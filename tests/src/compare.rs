use xust_tree::node::{self, Attribute, Element, NodeKind, Ref};

fn collect_atts<T: Ref>(e: Element<T>) -> Vec<Attribute<T>> {
    let mut atts: Vec<_> = e.attributes().collect();
    atts.sort_by(|a, b| {
        let a = a.node_name().to_ref();
        let b = b.node_name().to_ref();
        let nscmp = a.namespace().as_str().cmp(b.namespace().as_str());
        if nscmp == std::cmp::Ordering::Equal {
            a.local_name().as_str().cmp(b.local_name().as_str())
        } else {
            nscmp
        }
    });
    atts
}

pub fn compare_nodes<T: Ref, R: Ref>(a: &node::Node<T>, b: &node::Node<R>) -> Result<(), String> {
    match (a.node_kind(), b.node_kind()) {
        (NodeKind::Element, NodeKind::Element) => {
            let a = a.element().unwrap();
            let b = b.element().unwrap();
            let an = a.node_name();
            let bn = b.node_name();
            if an != bn {
                let ar = an.to_ref();
                let br = bn.to_ref();
                return Err(format!(
                    "Element 'Q{{{}}}{}' != 'Q{{{}}}{}'",
                    ar.namespace(),
                    ar.local_name(),
                    ar.namespace(),
                    br.local_name()
                ));
            }
            let a_atts = collect_atts(a);
            let b_atts = collect_atts(b);
            if a_atts.len() != b_atts.len() {
                return Err(format!(
                    "Element attribute counts are not equal: {} != {}.",
                    a_atts.len(),
                    b_atts.len()
                ));
            }
            for (a, b) in a_atts.iter().zip(b_atts) {
                let an = a.node_name();
                let bn = b.node_name();
                if an != bn {
                    let ar = an.to_ref();
                    let br = bn.to_ref();
                    return Err(format!(
                        "Attributes not equal: {:?} != {:?}.",
                        ar.local_name(),
                        br.local_name()
                    ));
                }
                if a.string_value() != b.string_value() {
                    return Err(format!(
                        "Attributes not equal: {:?} != {:?}.",
                        a.string_value(),
                        b.string_value()
                    ));
                }
            }
            Ok(())
        }
        (NodeKind::Document, NodeKind::Document) => Ok(()),
        (NodeKind::Namespace, NodeKind::Namespace) => Ok(()),
        (NodeKind::Text, NodeKind::Text) => {
            if a.string_value() != b.string_value() {
                return Err(format!(
                    "Text '{}' != '{}'",
                    a.string_value(),
                    b.string_value()
                ));
            }
            Ok(())
        }
        (NodeKind::ProcessingInstruction, NodeKind::ProcessingInstruction) => {
            if a.string_value() != b.string_value() {
                return Err(format!(
                    "ProcessingInstruction '{}' != '{}'",
                    a.string_value(),
                    b.string_value()
                ));
            }
            Ok(())
        }
        (a, b) => Err(format!("different node types: {:?} != {:?}", a, b)),
    }
}
