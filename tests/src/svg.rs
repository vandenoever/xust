#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
xust_xsd::use_xsd!();
use xust_tree::qnames::{NCName, Namespace, Prefix, QNameCollection};

type P = xust_xsd::parse::ParseEnv<QNames>;

pub struct QNames {
    qnames: QNameCollection,
    local: LocalQNames,
    svg: SvgQNames,
    xlink: XlinkQNames,
    xml: XmlQNames,
}
pub fn qnames(qnames: QNameCollection) -> QNames {
    QNames {
        local: LocalQNames::new(&qnames),
        svg: SvgQNames::new(&qnames),
        xlink: XlinkQNames::new(&qnames),
        xml: XmlQNames::new(&qnames),
        qnames,
    }
}
ns!(SvgQNames "http://www.w3.org/2000/svg" =>
    baseline_shift_value_type => "BaselineShiftValueType"
    class_list_type => "ClassListType"
    clip_fill_rule_type => "ClipFillRuleType"
    clip_path_value_type => "ClipPathValueType"
    clip_value_type => "ClipValueType"
    color_type => "ColorType"
    content_type_type => "ContentTypeType"
    coordinate_type => "CoordinateType"
    coordinates_type => "CoordinatesType"
    cursor_value_type => "CursorValueType"
    enable_background_value_type => "EnableBackgroundValueType"
    extension_list_type => "ExtensionListType"
    feature_list_type => "FeatureListType"
    filter_value_type => "FilterValueType"
    font_family_value_type => "FontFamilyValueType"
    font_size_adjust_value_type => "FontSizeAdjustValueType"
    font_size_value_type => "FontSizeValueType"
    glyph_orientation_horizontal_value_type => "GlyphOrientationHorizontalValueType"
    glyph_orientation_vertical_value_type => "GlyphOrientationVerticalValueType"
    kerning_value => "KerningValue"
    language_code_type => "LanguageCodeType"
    language_codes_type => "LanguageCodesType"
    length_type => "LengthType"
    lengths_type => "LengthsType"
    link_target_type => "LinkTargetType"
    marker_value_type => "MarkerValueType"
    mask_value_type => "MaskValueType"
    media_desc_type => "MediaDescType"
    number_optional_number_type => "NumberOptionalNumberType"
    number_or_percentage_type => "NumberOrPercentageType"
    numbers_type => "NumbersType"
    opacity_value_type => "OpacityValueType"
    paint_type => "PaintType"
    path_data_type => "PathDataType"
    points_type => "PointsType"
    preserve_aspect_ratio_spec_type => "PreserveAspectRatioSpecType"
    svg_color_type => "SVGColorType"
    script_type => "ScriptType"
    spacing_value_type => "SpacingValueType"
    stroke_dash_array_value_type => "StrokeDashArrayValueType"
    stroke_dash_offset_value_type => "StrokeDashOffsetValueType"
    stroke_miter_limit_value_type => "StrokeMiterLimitValueType"
    stroke_width_value_type => "StrokeWidthValueType"
    style_sheet_type => "StyleSheetType"
    text_decoration_value_type => "TextDecorationValueType"
    transform_list_type => "TransformListType"
    view_box_spec_type => "ViewBoxSpecType"
    a => "a"
    alt_glyph => "altGlyph"
    alt_glyph_def => "altGlyphDef"
    alt_glyph_item => "altGlyphItem"
    animate => "animate"
    animate_color => "animateColor"
    animate_motion => "animateMotion"
    animate_transform => "animateTransform"
    circle => "circle"
    clip_path => "clipPath"
    color_profile => "color-profile"
    cursor => "cursor"
    definition_src => "definition-src"
    defs => "defs"
    desc => "desc"
    ellipse => "ellipse"
    fe_blend => "feBlend"
    fe_color_matrix => "feColorMatrix"
    fe_component_transfer => "feComponentTransfer"
    fe_composite => "feComposite"
    fe_convolve_matrix => "feConvolveMatrix"
    fe_diffuse_lighting => "feDiffuseLighting"
    fe_displacement_map => "feDisplacementMap"
    fe_distant_light => "feDistantLight"
    fe_flood => "feFlood"
    fe_func_a => "feFuncA"
    fe_func_b => "feFuncB"
    fe_func_g => "feFuncG"
    fe_func_r => "feFuncR"
    fe_gaussian_blur => "feGaussianBlur"
    fe_image => "feImage"
    fe_merge => "feMerge"
    fe_merge_node => "feMergeNode"
    fe_morphology => "feMorphology"
    fe_offset => "feOffset"
    fe_point_light => "fePointLight"
    fe_specular_lighting => "feSpecularLighting"
    fe_spot_light => "feSpotLight"
    fe_tile => "feTile"
    fe_turbulence => "feTurbulence"
    filter => "filter"
    font => "font"
    font_face => "font-face"
    font_face_format => "font-face-format"
    font_face_name => "font-face-name"
    font_face_src => "font-face-src"
    font_face_uri => "font-face-uri"
    foreign_object => "foreignObject"
    g => "g"
    glyph => "glyph"
    glyph_ref => "glyphRef"
    hkern => "hkern"
    image => "image"
    line => "line"
    linear_gradient => "linearGradient"
    marker => "marker"
    mask => "mask"
    metadata => "metadata"
    missing_glyph => "missing-glyph"
    mpath => "mpath"
    path => "path"
    pattern => "pattern"
    polygon => "polygon"
    polyline => "polyline"
    radial_gradient => "radialGradient"
    rect => "rect"
    script => "script"
    set => "set"
    stop => "stop"
    style => "style"
    svg => "svg"
    switch => "switch"
    symbol => "symbol"
    text => "text"
    text_path => "textPath"
    title => "title"
    tref => "tref"
    tspan => "tspan"
    r#use => "use"
    view => "view"
    vkern => "vkern"
);
ns!(XlinkQNames "http://www.w3.org/1999/xlink" =>
    actuate => "actuate"
    actuate_type => "actuateType"
    arcrole => "arcrole"
    arcrole_type => "arcroleType"
    from => "from"
    from_type => "fromType"
    href => "href"
    href_type => "hrefType"
    label => "label"
    label_type => "labelType"
    role => "role"
    role_type => "roleType"
    show => "show"
    show_type => "showType"
    title => "title"
    title_attr_type => "titleAttrType"
    to => "to"
    to_type => "toType"
    r#type => "type"
    type_type => "typeType"
);
ns!(XmlQNames "http://www.w3.org/XML/1998/namespace" =>
    base => "base"
    lang => "lang"
    space => "space"
);
ns!(LocalQNames "" =>
    accent_height => "accent-height"
    accumulate => "accumulate"
    additive => "additive"
    alignment_baseline => "alignment-baseline"
    amplitude => "amplitude"
    arabic => "arabic"
    ascent => "ascent"
    attribute_name => "attributeName"
    attribute_type => "attributeType"
    azimuth => "azimuth"
    base_frequency => "baseFrequency"
    baseline => "baseline"
    baseline_shift => "baseline-shift"
    bbox => "bbox"
    begin => "begin"
    bias => "bias"
    by => "by"
    calc_mode => "calcMode"
    cap_height => "cap-height"
    centerline => "centerline"
    class => "class"
    clip => "clip"
    clip_path => "clip-path"
    clip_rule => "clip-rule"
    clip_path_units => "clipPathUnits"
    color => "color"
    color_interpolation => "color-interpolation"
    color_interpolation_filters => "color-interpolation-filters"
    color_profile => "color-profile"
    color_rendering => "color-rendering"
    content => "content"
    content_script_type => "contentScriptType"
    content_style_type => "contentStyleType"
    cursor => "cursor"
    cx => "cx"
    cy => "cy"
    d => "d"
    desc => "desc"
    descent => "descent"
    diffuse_constant => "diffuseConstant"
    direction => "direction"
    display => "display"
    divisor => "divisor"
    dominant_baseline => "dominant-baseline"
    dur => "dur"
    dx => "dx"
    dy => "dy"
    edge_mode => "edgeMode"
    elevation => "elevation"
    enable_background => "enable-background"
    end => "end"
    exponent => "exponent"
    external_resources_required => "externalResourcesRequired"
    fill => "fill"
    fill_opacity => "fill-opacity"
    fill_rule => "fill-rule"
    filter => "filter"
    filter_res => "filterRes"
    filter_units => "filterUnits"
    flood_color => "flood-color"
    flood_opacity => "flood-opacity"
    font_family => "font-family"
    font_size => "font-size"
    font_size_adjust => "font-size-adjust"
    font_stretch => "font-stretch"
    font_style => "font-style"
    font_variant => "font-variant"
    font_weight => "font-weight"
    format => "format"
    from => "from"
    fx => "fx"
    fy => "fy"
    g1 => "g1"
    g2 => "g2"
    glyph_name => "glyph-name"
    glyph_orientation_horizontal => "glyph-orientation-horizontal"
    glyph_orientation_vertical => "glyph-orientation-vertical"
    glyph_ref => "glyphRef"
    gradient_transform => "gradientTransform"
    gradient_units => "gradientUnits"
    han => "han"
    hanging => "hanging"
    height => "height"
    horiz_adv_x => "horiz-adv-x"
    horiz_origin_x => "horiz-origin-x"
    horiz_origin_y => "horiz-origin-y"
    id => "id"
    ideographic => "ideographic"
    image_rendering => "image-rendering"
    r#in => "in"
    in2 => "in2"
    intercept => "intercept"
    k => "k"
    k1 => "k1"
    k2 => "k2"
    k3 => "k3"
    k4 => "k4"
    kernel_matrix => "kernelMatrix"
    kernel_unit_length => "kernelUnitLength"
    key_points => "keyPoints"
    key_splines => "keySplines"
    key_times => "keyTimes"
    length_adjust => "lengthAdjust"
    letter_spacing => "letter-spacing"
    lighting_color => "lighting-color"
    limiting_cone_angle => "limitingConeAngle"
    local => "local"
    marker_end => "marker-end"
    marker_mid => "marker-mid"
    marker_start => "marker-start"
    marker_height => "markerHeight"
    marker_units => "markerUnits"
    marker_width => "markerWidth"
    mask => "mask"
    mask_units => "maskUnits"
    mathline => "mathline"
    max => "max"
    media => "media"
    metadata => "metadata"
    method => "method"
    min => "min"
    mode => "mode"
    name => "name"
    num_octaves => "numOctaves"
    offset => "offset"
    onabort => "onabort"
    onactivate => "onactivate"
    onbegin => "onbegin"
    onclick => "onclick"
    onend => "onend"
    onerror => "onerror"
    onfocusin => "onfocusin"
    onfocusout => "onfocusout"
    onload => "onload"
    onmousedown => "onmousedown"
    onmousemove => "onmousemove"
    onmouseout => "onmouseout"
    onmouseover => "onmouseover"
    onmouseup => "onmouseup"
    onrepeat => "onrepeat"
    onresize => "onresize"
    onscroll => "onscroll"
    onunload => "onunload"
    onzoom => "onzoom"
    opacity => "opacity"
    operator => "operator"
    order => "order"
    orient => "orient"
    origin => "origin"
    overflow => "overflow"
    overline_position => "overline-position"
    overline_thickness => "overline-thickness"
    panose_1 => "panose-1"
    path => "path"
    path_length => "pathLength"
    pattern_transform => "patternTransform"
    pattern_units => "patternUnits"
    pointer_events => "pointer-events"
    points => "points"
    points_at_x => "pointsAtX"
    points_at_y => "pointsAtY"
    points_at_z => "pointsAtZ"
    preserve_alpha => "preserveAlpha"
    preserve_aspect_ratio => "preserveAspectRatio"
    primitive_units => "primitiveUnits"
    r => "r"
    radius => "radius"
    ref_x => "refX"
    ref_y => "refY"
    rendering_intent => "rendering-intent"
    repeat_count => "repeatCount"
    repeat_dur => "repeatDur"
    required_extensions => "requiredExtensions"
    required_features => "requiredFeatures"
    restart => "restart"
    result => "result"
    rotate => "rotate"
    rx => "rx"
    ry => "ry"
    scale => "scale"
    seed => "seed"
    shape_rendering => "shape-rendering"
    slope => "slope"
    spacing => "spacing"
    specular_constant => "specularConstant"
    specular_exponent => "specularExponent"
    spread_method => "spreadMethod"
    start_offset => "startOffset"
    std_deviation => "stdDeviation"
    stemh => "stemh"
    stemv => "stemv"
    stitch_tiles => "stitchTiles"
    stop_color => "stop-color"
    stop_opacity => "stop-opacity"
    strikethrough_position => "strikethrough-position"
    strikethrough_thickness => "strikethrough-thickness"
    string => "string"
    stroke => "stroke"
    stroke_dasharray => "stroke-dasharray"
    stroke_dashoffset => "stroke-dashoffset"
    stroke_linecap => "stroke-linecap"
    stroke_linejoin => "stroke-linejoin"
    stroke_miterlimit => "stroke-miterlimit"
    stroke_opacity => "stroke-opacity"
    stroke_width => "stroke-width"
    style => "style"
    surface_scale => "surfaceScale"
    system_language => "systemLanguage"
    table_values => "tableValues"
    target => "target"
    target_x => "targetX"
    target_y => "targetY"
    text_anchor => "text-anchor"
    text_decoration => "text-decoration"
    text_rendering => "text-rendering"
    text_length => "textLength"
    title => "title"
    to => "to"
    topline => "topline"
    transform => "transform"
    r#type => "type"
    u1 => "u1"
    u2 => "u2"
    underline_position => "underline-position"
    underline_thickness => "underline-thickness"
    unicode => "unicode"
    unicode_bidi => "unicode-bidi"
    unicode_range => "unicode-range"
    units_per_em => "units-per-em"
    values => "values"
    vert_adv_y => "vert-adv-y"
    vert_origin_x => "vert-origin-x"
    vert_origin_y => "vert-origin-y"
    vert_text_orient => "vert-text-orient"
    view_box => "viewBox"
    view_target => "viewTarget"
    visibility => "visibility"
    width => "width"
    widths => "widths"
    word_spacing => "word-spacing"
    writing_mode => "writing-mode"
    x => "x"
    x_height => "x-height"
    x1 => "x1"
    x2 => "x2"
    x_channel_selector => "xChannelSelector"
    y => "y"
    y1 => "y1"
    y2 => "y2"
    y_channel_selector => "yChannelSelector"
    z => "z"
    zoom_and_pan => "zoomAndPan"
);
pub mod svg {
    pub mod local {
        xust_xsd::use_xsd!();
        choice!(missing_glyph MissingGlyph -
            Desc(super::ct::DescType): r svg desc super::ct::desc_type,
            Title(super::ct::TitleType): r svg title super::ct::title_type,
            Metadata(super::ct::MetadataType): r svg metadata super::ct::metadata_type,
            Defs(super::ct::DefsType): r svg defs super::ct::defs_type,
            Path(super::ct::PathType): r svg path super::ct::path_type,
            Text(super::ct::TextType): r svg text super::ct::text_type,
            Rect(super::ct::RectType): r svg rect super::ct::rect_type,
            Circle(super::ct::CircleType): r svg circle super::ct::circle_type,
            Ellipse(super::ct::EllipseType): r svg ellipse super::ct::ellipse_type,
            Line(super::ct::LineType): r svg line super::ct::line_type,
            Polyline(super::ct::PolylineType): r svg polyline super::ct::polyline_type,
            Polygon(super::ct::PolygonType): r svg polygon super::ct::polygon_type,
            Use(super::ct::UseType): r svg r#use super::ct::use_type,
            Image(super::ct::ImageType): r svg image super::ct::image_type,
            Svg(super::ct::SvgType): r svg svg super::ct::svg_type,
            G(super::ct::GType): r svg g super::ct::g_type,
            View(super::ct::ViewType): r svg view super::ct::view_type,
            Switch(super::ct::SwitchType): r svg switch super::ct::switch_type,
            A(super::ct::AType): r svg a super::ct::a_type,
            AltGlyphDef(super::ct::AltGlyphDefType): r svg alt_glyph_def super::ct::alt_glyph_def_type,
            Script(super::ct::ScriptType): r svg script super::ct::script_type,
            Style(super::ct::StyleType): r svg style super::ct::style_type,
            Symbol(super::ct::SymbolType): r svg symbol super::ct::symbol_type,
            Marker(super::ct::MarkerType): r svg marker super::ct::marker_type,
            ClipPath(super::ct::ClipPathType): r svg clip_path super::ct::clip_path_type,
            Mask(super::ct::MaskType): r svg mask super::ct::mask_type,
            LinearGradient(super::ct::LinearGradientType): r svg linear_gradient super::ct::linear_gradient_type,
            RadialGradient(super::ct::RadialGradientType): r svg radial_gradient super::ct::radial_gradient_type,
            Pattern(super::ct::PatternType): r svg pattern super::ct::pattern_type,
            Filter(super::ct::FilterType): r svg filter super::ct::filter_type,
            Cursor(super::ct::CursorType): r svg cursor super::ct::cursor_type,
            Font(super::ct::FontType): r svg font super::ct::font_type,
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateMotion(super::ct::AnimateMotionType): r svg animate_motion super::ct::animate_motion_type,
            AnimateColor(super::ct::AnimateColorType): r svg animate_color super::ct::animate_color_type,
            AnimateTransform(super::ct::AnimateTransformType): r svg animate_transform super::ct::animate_transform_type,
            ColorProfile(super::ct::ColorProfileType): r svg color_profile super::ct::color_profile_type,
            FontFace(super::ct::FontFaceType): r svg font_face super::ct::font_face_type
        );
        choice!(polygon1 Polygon1 -
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateMotion(super::ct::AnimateMotionType): r svg animate_motion super::ct::animate_motion_type,
            AnimateColor(super::ct::AnimateColorType): r svg animate_color super::ct::animate_color_type,
            AnimateTransform(super::ct::AnimateTransformType): r svg animate_transform super::ct::animate_transform_type
        );
        choice!(switch1 Switch1 -
            Path(super::ct::PathType): r svg path super::ct::path_type,
            Text(super::ct::TextType): r svg text super::ct::text_type,
            Rect(super::ct::RectType): r svg rect super::ct::rect_type,
            Circle(super::ct::CircleType): r svg circle super::ct::circle_type,
            Ellipse(super::ct::EllipseType): r svg ellipse super::ct::ellipse_type,
            Line(super::ct::LineType): r svg line super::ct::line_type,
            Polyline(super::ct::PolylineType): r svg polyline super::ct::polyline_type,
            Polygon(super::ct::PolygonType): r svg polygon super::ct::polygon_type,
            Use(super::ct::UseType): r svg r#use super::ct::use_type,
            Image(super::ct::ImageType): r svg image super::ct::image_type,
            Svg(super::ct::SvgType): r svg svg super::ct::svg_type,
            G(super::ct::GType): r svg g super::ct::g_type,
            Switch(super::ct::SwitchType): r svg switch super::ct::switch_type,
            A(super::ct::AType): r svg a super::ct::a_type,
            ForeignObject(super::ct::ForeignObjectType): r svg foreign_object super::ct::foreign_object_type,
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateMotion(super::ct::AnimateMotionType): r svg animate_motion super::ct::animate_motion_type,
            AnimateColor(super::ct::AnimateColorType): r svg animate_color super::ct::animate_color_type,
            AnimateTransform(super::ct::AnimateTransformType): r svg animate_transform super::ct::animate_transform_type
        );
        choice!(text Text m
            Desc(super::ct::DescType): r svg desc super::ct::desc_type,
            Title(super::ct::TitleType): r svg title super::ct::title_type,
            Metadata(super::ct::MetadataType): r svg metadata super::ct::metadata_type,
            Tspan(super::ct::TspanType): r svg tspan super::ct::tspan_type,
            Tref(super::ct::TrefType): r svg tref super::ct::tref_type,
            TextPath(super::ct::TextPathType): r svg text_path super::ct::text_path_type,
            AltGlyph(super::ct::AltGlyphType): r svg alt_glyph super::ct::alt_glyph_type,
            A(super::ct::AType): r svg a super::ct::a_type,
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateMotion(super::ct::AnimateMotionType): r svg animate_motion super::ct::animate_motion_type,
            AnimateColor(super::ct::AnimateColorType): r svg animate_color super::ct::animate_color_type,
            AnimateTransform(super::ct::AnimateTransformType): r svg animate_transform super::ct::animate_transform_type
        );
        choice!(text_path TextPath m
            Desc(super::ct::DescType): r svg desc super::ct::desc_type,
            Title(super::ct::TitleType): r svg title super::ct::title_type,
            Metadata(super::ct::MetadataType): r svg metadata super::ct::metadata_type,
            Tspan(super::ct::TspanType): r svg tspan super::ct::tspan_type,
            Tref(super::ct::TrefType): r svg tref super::ct::tref_type,
            AltGlyph(super::ct::AltGlyphType): r svg alt_glyph super::ct::alt_glyph_type,
            A(super::ct::AType): r svg a super::ct::a_type,
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateColor(super::ct::AnimateColorType): r svg animate_color super::ct::animate_color_type
        );
        choice!(tref Tref -
            Desc(super::ct::DescType): r svg desc super::ct::desc_type,
            Title(super::ct::TitleType): r svg title super::ct::title_type,
            Metadata(super::ct::MetadataType): r svg metadata super::ct::metadata_type,
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateColor(super::ct::AnimateColorType): r svg animate_color super::ct::animate_color_type
        );
        choice!(alt_glyph_def AltGlyphDef -
            AltGlyphItem(super::ct::AltGlyphItemType): r svg alt_glyph_item super::ct::alt_glyph_item_type,
            GlyphRef(super::ct::GlyphRefType): r svg glyph_ref super::ct::glyph_ref_type
        );
        sequence!(alt_glyph_item AltGlyphItem -
            glyph_ref(super::ct::GlyphRefType): r svg glyph_ref super::ct::glyph_ref_type
        );
        choice!(radial_gradient1 RadialGradient1 -
            Stop(super::ct::StopType): r svg stop super::ct::stop_type,
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateTransform(super::ct::AnimateTransformType): r svg animate_transform super::ct::animate_transform_type
        );
        choice!(fe_specular_lighting2 FeSpecularLighting2 -
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateColor(super::ct::AnimateColorType): r svg animate_color super::ct::animate_color_type
        );
        choice!(clip_path1 ClipPath1 -
            Path(super::ct::PathType): r svg path super::ct::path_type,
            Text(super::ct::TextType): r svg text super::ct::text_type,
            Rect(super::ct::RectType): r svg rect super::ct::rect_type,
            Circle(super::ct::CircleType): r svg circle super::ct::circle_type,
            Ellipse(super::ct::EllipseType): r svg ellipse super::ct::ellipse_type,
            Line(super::ct::LineType): r svg line super::ct::line_type,
            Polyline(super::ct::PolylineType): r svg polyline super::ct::polyline_type,
            Polygon(super::ct::PolygonType): r svg polygon super::ct::polygon_type,
            Use(super::ct::UseType): r svg r#use super::ct::use_type,
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateMotion(super::ct::AnimateMotionType): r svg animate_motion super::ct::animate_motion_type,
            AnimateColor(super::ct::AnimateColorType): r svg animate_color super::ct::animate_color_type,
            AnimateTransform(super::ct::AnimateTransformType): r svg animate_transform super::ct::animate_transform_type
        );
        choice!(filter1 Filter1 -
            FeBlend(super::ct::FeBlendType): r svg fe_blend super::ct::fe_blend_type,
            FeFlood(super::ct::FeFloodType): r svg fe_flood super::ct::fe_flood_type,
            FeColorMatrix(super::ct::FeColorMatrixType): r svg fe_color_matrix super::ct::fe_color_matrix_type,
            FeComponentTransfer(super::ct::FeComponentTransferType): r svg fe_component_transfer super::ct::fe_component_transfer_type,
            FeComposite(super::ct::FeCompositeType): r svg fe_composite super::ct::fe_composite_type,
            FeConvolveMatrix(super::ct::FeConvolveMatrixType): r svg fe_convolve_matrix super::ct::fe_convolve_matrix_type,
            FeDiffuseLighting(super::ct::FeDiffuseLightingType): r svg fe_diffuse_lighting super::ct::fe_diffuse_lighting_type,
            FeDisplacementMap(super::ct::FeDisplacementMapType): r svg fe_displacement_map super::ct::fe_displacement_map_type,
            FeGaussianBlur(super::ct::FeGaussianBlurType): r svg fe_gaussian_blur super::ct::fe_gaussian_blur_type,
            FeImage(super::ct::FeImageType): r svg fe_image super::ct::fe_image_type,
            FeMerge(super::ct::FeMergeType): r svg fe_merge super::ct::fe_merge_type,
            FeMorphology(super::ct::FeMorphologyType): r svg fe_morphology super::ct::fe_morphology_type,
            FeOffset(super::ct::FeOffsetType): r svg fe_offset super::ct::fe_offset_type,
            FeSpecularLighting(super::ct::FeSpecularLightingType): r svg fe_specular_lighting super::ct::fe_specular_lighting_type,
            FeTile(super::ct::FeTileType): r svg fe_tile super::ct::fe_tile_type,
            FeTurbulence(super::ct::FeTurbulenceType): r svg fe_turbulence super::ct::fe_turbulence_type,
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type
        );
        choice!(fe_turbulence FeTurbulence -
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type
        );
        choice!(fe_specular_lighting1 FeSpecularLighting1 -
            FeDistantLight(super::ct::FeDistantLightType): r svg fe_distant_light super::ct::fe_distant_light_type,
            FePointLight(super::ct::FePointLightType): r svg fe_point_light super::ct::fe_point_light_type,
            FeSpotLight(super::ct::FeSpotLightType): r svg fe_spot_light super::ct::fe_spot_light_type
        );
        choice!(fe_image FeImage -
            Animate(super::ct::AnimateType): r svg animate super::ct::animate_type,
            Set(super::ct::SetType): r svg set super::ct::set_type,
            AnimateTransform(super::ct::AnimateTransformType): r svg animate_transform super::ct::animate_transform_type
        );
        sequence!(fe_merge FeMerge -
            fe_merge_node(super::ct::FeMergeNodeType): r svg fe_merge_node super::ct::fe_merge_node_type
        );
        choice!(font1 Font1 -
            Glyph(super::ct::GlyphType): r svg glyph super::ct::glyph_type,
            Hkern(super::ct::HkernType): r svg hkern super::ct::hkern_type,
            Vkern(super::ct::VkernType): r svg vkern super::ct::vkern_type
        );
        choice!(font_face_src FontFaceSrc -
            FontFaceUri(super::ct::FontFaceUriType): r svg font_face_uri super::ct::font_face_uri_type,
            FontFaceName(super::ct::FontFaceNameType): r svg font_face_name super::ct::font_face_name_type
        );
    }
    pub mod g {
        xust_xsd::use_xsd!();
        sequence!(desc_title_metadata DescTitleMetadata -);
    }
    pub mod ct {
        xust_xsd::use_xsd!();
        simpleType!(baseline_shift_value_type BaselineShiftValueType);
        simpleType!(class_list_type ClassListType);
        simpleType!(clip_value_type ClipValueType);
        simpleType!(clip_path_value_type ClipPathValueType);
        simpleType!(clip_fill_rule_type ClipFillRuleType);
        simpleType!(content_type_type ContentTypeType);
        simpleType!(coordinate_type CoordinateType);
        simpleType!(coordinates_type CoordinatesType);
        simpleType!(color_type ColorType);
        simpleType!(cursor_value_type CursorValueType);
        simpleType!(enable_background_value_type EnableBackgroundValueType);
        simpleType!(extension_list_type ExtensionListType);
        simpleType!(feature_list_type FeatureListType);
        simpleType!(filter_value_type FilterValueType);
        simpleType!(font_family_value_type FontFamilyValueType);
        simpleType!(font_size_value_type FontSizeValueType);
        simpleType!(font_size_adjust_value_type FontSizeAdjustValueType);
        simpleType!(glyph_orientation_horizontal_value_type GlyphOrientationHorizontalValueType);
        simpleType!(glyph_orientation_vertical_value_type GlyphOrientationVerticalValueType);
        simpleType!(kerning_value KerningValue);
        simpleType!(language_code_type LanguageCodeType);
        simpleType!(language_codes_type LanguageCodesType);
        simpleType!(length_type LengthType);
        simpleType!(lengths_type LengthsType);
        simpleType!(link_target_type LinkTargetType);
        simpleType!(marker_value_type MarkerValueType);
        simpleType!(mask_value_type MaskValueType);
        simpleType!(media_desc_type MediaDescType);
        simpleType!(number_optional_number_type NumberOptionalNumberType);
        simpleType!(number_or_percentage_type NumberOrPercentageType);
        simpleType!(numbers_type NumbersType);
        simpleType!(opacity_value_type OpacityValueType);
        simpleType!(paint_type PaintType);
        simpleType!(path_data_type PathDataType);
        simpleType!(points_type PointsType);
        simpleType!(preserve_aspect_ratio_spec_type PreserveAspectRatioSpecType);
        simpleType!(spacing_value_type SpacingValueType);
        simpleType!(stroke_dash_array_value_type StrokeDashArrayValueType);
        simpleType!(stroke_dash_offset_value_type StrokeDashOffsetValueType);
        simpleType!(stroke_miter_limit_value_type StrokeMiterLimitValueType);
        simpleType!(stroke_width_value_type StrokeWidthValueType);
        simpleType!(style_sheet_type StyleSheetType);
        simpleType!(svg_color_type SvgColorType);
        simpleType!(text_decoration_value_type TextDecorationValueType);
        simpleType!(transform_list_type TransformListType);
        simpleType!(view_box_spec_type ViewBoxSpecType);
        complexType!(svg_type SvgType 115 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local content_script_type content_script_type o "text/ecmascript" String,
                local content_style_type content_style_type o "text/css" String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local height height r - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onabort onabort o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onerror onerror o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local onresize onresize o - String,
                local onscroll onscroll o - String,
                local onunload onunload o - String,
                local onzoom onzoom o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local preserve_aspect_ratio preserve_aspect_ratio o "xMidYMid meet" String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local unicode_bidi unicode_bidi o - String,
                local view_box view_box o - String,
                local visibility visibility o - String,
                local width width r - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                local x x o - String,
                local y y o - String,
                local zoom_and_pan zoom_and_pan o "magnify" String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(g_type GType 116 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(defs_type DefsType 117 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(desc_type DescType 118 m
        attributes {
            local class class ov - String,
            local content content o - String,
            local id id o - String,
            local style style o - String,
            xml base base o - String,
            xml lang lang o - String,
            xml space space o - String
        });
        complexType!(title_type TitleType 119 m
        attributes {
            local class class ov - String,
            local content content o - String,
            local id id o - String,
            local style style o - String,
            xml base base o - String,
            xml lang lang o - String,
            xml space space o - String
        });
        complexType!(symbol_type SymbolType 120 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local preserve_aspect_ratio preserve_aspect_ratio o "xMidYMid meet" String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local unicode_bidi unicode_bidi o - String,
                local view_box view_box o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(use_type UseType 121 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local height height o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local width width o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                local x x o - String,
                local y y o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href r - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Polygon1): v + - super::local::polygon1
        );
        complexType!(image_type ImageType 122 -
            attributes {
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local display display o - String,
                local external_resources_required external_resources_required o - bool,
                local filter filter o - String,
                local height height r - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local visibility visibility o - String,
                local width width r - String,
                local x x o - String,
                local y y o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Polygon1): v + - super::local::polygon1
        );
        complexType!(switch_type SwitchType 123 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Switch1): v + - super::local::switch1
        );
        complexType!(style_type StyleType 124 m
        attributes {
            local id id o - String,
            local media media o - String,
            local title title o - String,
            local r#type r#type r - String,
            xml base base o - String,
            xml space space o - String
        });
        complexType!(path_type PathType 125 -
            attributes {
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local d d r - String,
                local display display o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local path_length path_length o - f64,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local visibility visibility o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Polygon1): v + - super::local::polygon1
        );
        complexType!(rect_type RectType 126 -
            attributes {
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local display display o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local height height r - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local rx rx o - String,
                local ry ry o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local visibility visibility o - String,
                local width width r - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Polygon1): v + - super::local::polygon1
        );
        complexType!(circle_type CircleType 127 -
            attributes {
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local cx cx o - String,
                local cy cy o - String,
                local display display o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local r r r - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local visibility visibility o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Polygon1): v + - super::local::polygon1
        );
        complexType!(ellipse_type EllipseType 128 -
            attributes {
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local cx cx o - String,
                local cy cy o - String,
                local display display o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local rx rx r - String,
                local ry ry r - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local visibility visibility o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Polygon1): v + - super::local::polygon1
        );
        complexType!(line_type LineType 129 -
            attributes {
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local display display o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local visibility visibility o - String,
                local x1 x1 o - String,
                local x2 x2 o - String,
                local y1 y1 o - String,
                local y2 y2 o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Polygon1): v + - super::local::polygon1
        );
        complexType!(polyline_type PolylineType 130 -
            attributes {
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local display display o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local points points r - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local visibility visibility o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Polygon1): v + - super::local::polygon1
        );
        complexType!(polygon_type PolygonType 131 -
            attributes {
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local display display o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local points points r - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local visibility visibility o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Polygon1): v + - super::local::polygon1
        );
        complexType!(text_type TextType 132 m
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local length_adjust length_adjust o - String,
                local letter_spacing letter_spacing o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local text_length text_length o - String,
                local transform transform o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::Text): v + - super::local::text
        );
        complexType!(tspan_type TspanType 133 m
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local dx dx o - String,
                local dy dy o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local length_adjust length_adjust o - String,
                local letter_spacing letter_spacing o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local rotate rotate o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local text_length text_length o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::TextPath): v + - super::local::text_path
        );
        complexType!(tref_type TrefType 134 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local dx dx o - String,
                local dy dy o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local length_adjust length_adjust o - String,
                local letter_spacing letter_spacing o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local rotate rotate o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local text_length text_length o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local x x o - String,
                local y y o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::Tref): v + - super::local::tref
        );
        complexType!(text_path_type TextPathType 135 m
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local length_adjust length_adjust o - String,
                local letter_spacing letter_spacing o - String,
                local mask mask o - String,
                local method method o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local spacing spacing o - String,
                local start_offset start_offset o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local text_length text_length o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::TextPath): v + - super::local::text_path
        );
        complexType!(alt_glyph_type AltGlyphType 136 m
        attributes {
            local alignment_baseline alignment_baseline o - String,
            local baseline_shift baseline_shift o - String,
            local class class ov - String,
            local clip_path clip_path o - String,
            local clip_rule clip_rule o - String,
            local color color o - String,
            local color_interpolation color_interpolation o - String,
            local color_rendering color_rendering o - String,
            local cursor cursor o - String,
            local direction direction o - String,
            local display display o - String,
            local dominant_baseline dominant_baseline o - String,
            local dx dx o - String,
            local dy dy o - String,
            local external_resources_required external_resources_required o - bool,
            local fill fill o - String,
            local fill_opacity fill_opacity o - String,
            local fill_rule fill_rule o - String,
            local filter filter o - String,
            local font_family font_family o - String,
            local font_size font_size o - String,
            local font_size_adjust font_size_adjust o - String,
            local font_stretch font_stretch o - String,
            local font_style font_style o - String,
            local font_variant font_variant o - String,
            local font_weight font_weight o - String,
            local format format o - String,
            local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
            local glyph_orientation_vertical glyph_orientation_vertical o - String,
            local glyph_ref glyph_ref o - String,
            local id id o - String,
            local image_rendering image_rendering o - String,
            local letter_spacing letter_spacing o - String,
            local mask mask o - String,
            local onactivate onactivate o - String,
            local onclick onclick o - String,
            local onfocusin onfocusin o - String,
            local onfocusout onfocusout o - String,
            local onload onload o - String,
            local onmousedown onmousedown o - String,
            local onmousemove onmousemove o - String,
            local onmouseout onmouseout o - String,
            local onmouseover onmouseover o - String,
            local onmouseup onmouseup o - String,
            local opacity opacity o - String,
            local pointer_events pointer_events o - String,
            local required_extensions required_extensions o - String,
            local required_features required_features o - String,
            local rotate rotate o - String,
            local shape_rendering shape_rendering o - String,
            local stroke stroke o - String,
            local stroke_dasharray stroke_dasharray o - String,
            local stroke_dashoffset stroke_dashoffset o - String,
            local stroke_linecap stroke_linecap o - String,
            local stroke_linejoin stroke_linejoin o - String,
            local stroke_miterlimit stroke_miterlimit o - String,
            local stroke_opacity stroke_opacity o - String,
            local stroke_width stroke_width o - String,
            local style style o - String,
            local system_language system_language o - String,
            local text_anchor text_anchor o - String,
            local text_decoration text_decoration o - String,
            local text_rendering text_rendering o - String,
            local unicode_bidi unicode_bidi o - String,
            local visibility visibility o - String,
            local word_spacing word_spacing o - String,
            local x x o - String,
            local y y o - String,
            xlink actuate actuate o - String,
            xlink arcrole arcrole o - String,
            xlink href href o - String,
            xlink role role o - String,
            xlink show show o - String,
            xlink title title o - String,
            xlink r#type r#type o - String,
            xml base base o - String,
            xml lang lang o - String,
            xml space space o - String
        });
        complexType!(alt_glyph_def_type AltGlyphDefType 137 -
            attributes {
                local id id o - String,
                xml base base o - String
            }

            choice(super::local::AltGlyphDef): v + - super::local::alt_glyph_def
        );
        complexType!(alt_glyph_item_type AltGlyphItemType 138 -
            attributes {
                local id id o - String,
                xml base base o - String
            }

            sequence(super::local::AltGlyphItem): v + - super::local::alt_glyph_item
        );
        complexType!(glyph_ref_type GlyphRefType 139 -
        attributes {
            local class class ov - String,
            local dx dx o - String,
            local dy dy o - String,
            local font_family font_family o - String,
            local font_size font_size o - String,
            local font_size_adjust font_size_adjust o - String,
            local font_stretch font_stretch o - String,
            local font_style font_style o - String,
            local font_variant font_variant o - String,
            local font_weight font_weight o - String,
            local format format r - String,
            local glyph_ref glyph_ref r - String,
            local id id o - String,
            local style style o - String,
            local x x o - String,
            local y y o - String,
            xlink actuate actuate o - String,
            xlink arcrole arcrole o - String,
            xlink href href o - String,
            xlink role role o - String,
            xlink show show o - String,
            xlink title title o - String,
            xlink r#type r#type o - String,
            xml base base o - String
        });
        complexType!(marker_type MarkerType 140 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local marker_height marker_height o - String,
                local marker_units marker_units o - String,
                local marker_width marker_width o - String,
                local mask mask o - String,
                local opacity opacity o - String,
                local orient orient o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local preserve_aspect_ratio preserve_aspect_ratio o "xMidYMid meet" String,
                local ref_x ref_x o - String,
                local ref_y ref_y o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local unicode_bidi unicode_bidi o - String,
                local view_box view_box o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(color_profile_type ColorProfileType 141 -
            attributes {
                local id id o - String,
                local local local o - String,
                local name name r - String,
                local rendering_intent rendering_intent o "auto" String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata
        );
        complexType!(linear_gradient_type LinearGradientType 142 -
            attributes {
                local class class ov - String,
                local external_resources_required external_resources_required o - bool,
                local gradient_transform gradient_transform o - String,
                local gradient_units gradient_units o - String,
                local id id o - String,
                local spread_method spread_method o "pad" String,
                local style style o - String,
                local x1 x1 o - String,
                local x2 x2 o - String,
                local y1 y1 o - String,
                local y2 y2 o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::RadialGradient1): v + - super::local::radial_gradient1
        );
        complexType!(radial_gradient_type RadialGradientType 143 -
            attributes {
                local cx cx o - String,
                local cy cy o - String,
                local external_resources_required external_resources_required o - bool,
                local fx fx o - String,
                local fy fy o - String,
                local gradient_transform gradient_transform o - String,
                local gradient_units gradient_units o - String,
                local id id o - String,
                local r r o - String,
                local spread_method spread_method o "pad" String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::RadialGradient1): v + - super::local::radial_gradient1
        );
        complexType!(stop_type StopType 144 -
            attributes {
                local class class ov - String,
                local id id o - String,
                local offset offset r - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local style style o - String,
                xml base base o - String
            }

            choice(super::local::FeSpecularLighting2): v + - super::local::fe_specular_lighting2
        );
        complexType!(pattern_type PatternType 145 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local height height r - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pattern_transform pattern_transform o - String,
                local pattern_units pattern_units o - String,
                local pointer_events pointer_events o - String,
                local preserve_aspect_ratio preserve_aspect_ratio o "xMidYMid meet" String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local unicode_bidi unicode_bidi o - String,
                local view_box view_box o - String,
                local visibility visibility o - String,
                local width width r - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                local x x o - String,
                local y y o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(clip_path_type ClipPathType 146 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local clip_path_units clip_path_units o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local mask mask o - String,
                local opacity opacity o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::ClipPath1): v + - super::local::clip_path1
        );
        complexType!(mask_type MaskType 147 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local height height o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local mask_units mask_units o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local width width o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(filter_type FilterType 148 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local filter_res filter_res o - String,
                local filter_units filter_units o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local height height o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local primitive_units primitive_units o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local width width o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                local x x o - String,
                local y y o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            choice(super::local::Filter1): v + - super::local::filter1
        );
        complexType!(fe_distant_light_type FeDistantLightType 149 -
            attributes {
                local azimuth azimuth o - f64,
                local elevation elevation o - f64,
                local id id o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_point_light_type FePointLightType 150 -
            attributes {
                local id id o - String,
                local x x o - f64,
                local y y o - f64,
                local z z o - f64,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_spot_light_type FeSpotLightType 151 -
            attributes {
                local id id o - String,
                local limiting_cone_angle limiting_cone_angle o - f64,
                local points_at_x points_at_x o - f64,
                local points_at_y points_at_y o - f64,
                local points_at_z points_at_z o - f64,
                local specular_exponent specular_exponent o - f64,
                local x x o - f64,
                local y y o - f64,
                local z z o - f64,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_blend_type FeBlendType 152 -
            attributes {
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local in2 in2 r - String,
                local mode mode o "normal" String,
                local result result o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_color_matrix_type FeColorMatrixType 153 -
            attributes {
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local result result o - String,
                local r#type r#type o "matrix" String,
                local values values o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_component_transfer_type FeComponentTransferType 154 -
            attributes {
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local result result o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            fe_func_r(FeFuncRType): o svg fe_func_r fe_func_r_type,
            fe_func_g(FeFuncGType): o svg fe_func_g fe_func_g_type,
            fe_func_b(FeFuncBType): o svg fe_func_b fe_func_b_type,
            fe_func_a(FeFuncAType): o svg fe_func_a fe_func_a_type
        );
        complexType!(fe_func_r_type FeFuncRType 155 -
            attributes {
                local amplitude amplitude o - f64,
                local exponent exponent o - f64,
                local id id o - String,
                local intercept intercept o - f64,
                local offset offset o - f64,
                local slope slope o - f64,
                local table_values table_values o - String,
                local r#type r#type r - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_func_g_type FeFuncGType 156 -
            attributes {
                local amplitude amplitude o - f64,
                local exponent exponent o - f64,
                local id id o - String,
                local intercept intercept o - f64,
                local offset offset o - f64,
                local slope slope o - f64,
                local table_values table_values o - String,
                local r#type r#type r - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_func_b_type FeFuncBType 157 -
            attributes {
                local amplitude amplitude o - f64,
                local exponent exponent o - f64,
                local id id o - String,
                local intercept intercept o - f64,
                local offset offset o - f64,
                local slope slope o - f64,
                local table_values table_values o - String,
                local r#type r#type r - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_func_a_type FeFuncAType 158 -
            attributes {
                local amplitude amplitude o - f64,
                local exponent exponent o - f64,
                local id id o - String,
                local intercept intercept o - f64,
                local offset offset o - f64,
                local slope slope o - f64,
                local table_values table_values o - String,
                local r#type r#type r - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_composite_type FeCompositeType 159 -
            attributes {
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local in2 in2 r - String,
                local k1 k1 o - f64,
                local k2 k2 o - f64,
                local k3 k3 o - f64,
                local k4 k4 o - f64,
                local operator operator o "over" String,
                local result result o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_convolve_matrix_type FeConvolveMatrixType 160 -
            attributes {
                local bias bias o - f64,
                local divisor divisor o - f64,
                local edge_mode edge_mode o "duplicate" String,
                local height height o - String,
                local r#in r#in o - String,
                local kernel_matrix kernel_matrix r - String,
                local kernel_unit_length kernel_unit_length o - String,
                local order order r - String,
                local preserve_alpha preserve_alpha o - bool,
                local result result o - String,
                local target_x target_x o - i64,
                local target_y target_y o - i64,
                local width width o - String,
                local x x o - String,
                local y y o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_diffuse_lighting_type FeDiffuseLightingType 161 -
            attributes {
                local class class ov - String,
                local diffuse_constant diffuse_constant o - f64,
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local lighting_color lighting_color o - String,
                local result result o - String,
                local style style o - String,
                local surface_scale surface_scale o - f64,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice1(super::local::FeSpecularLighting1): r + - super::local::fe_specular_lighting1,
            choice2(super::local::FeSpecularLighting2): v + - super::local::fe_specular_lighting2
        );
        complexType!(fe_displacement_map_type FeDisplacementMapType 162 -
            attributes {
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local in2 in2 r - String,
                local result result o - String,
                local scale scale o - f64,
                local width width o - String,
                local x x o - String,
                local x_channel_selector x_channel_selector o "A" String,
                local y y o - String,
                local y_channel_selector y_channel_selector o "A" String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_flood_type FeFloodType 163 -
            attributes {
                local class class ov - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local result result o - String,
                local style style o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice(super::local::FeSpecularLighting2): v + - super::local::fe_specular_lighting2
        );
        complexType!(fe_gaussian_blur_type FeGaussianBlurType 164 -
            attributes {
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local result result o - String,
                local std_deviation std_deviation o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_image_type FeImageType 165 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local height height o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local result result o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local width width o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                local x x o - String,
                local y y o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::FeImage): v + - super::local::fe_image
        );
        complexType!(fe_merge_type FeMergeType 166 -
            attributes {
                local height height o - String,
                local id id o - String,
                local result result o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            sequence(super::local::FeMerge): v + - super::local::fe_merge
        );
        complexType!(fe_merge_node_type FeMergeNodeType 167 -
            attributes {
                local id id o - String,
                local r#in r#in o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_morphology_type FeMorphologyType 168 -
            attributes {
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local operator operator o "erode" String,
                local radius radius o - String,
                local result result o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_offset_type FeOffsetType 169 -
            attributes {
                local dx dx o - String,
                local dy dy o - String,
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local result result o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_specular_lighting_type FeSpecularLightingType 170 -
            attributes {
                local class class ov - String,
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local lighting_color lighting_color o - String,
                local result result o - String,
                local specular_constant specular_constant o - f64,
                local specular_exponent specular_exponent o - f64,
                local style style o - String,
                local surface_scale surface_scale o - f64,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice1(super::local::FeSpecularLighting1): r + - super::local::fe_specular_lighting1,
            choice2(super::local::FeSpecularLighting2): v + - super::local::fe_specular_lighting2
        );
        complexType!(fe_tile_type FeTileType 171 -
            attributes {
                local height height o - String,
                local id id o - String,
                local r#in r#in o - String,
                local result result o - String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(fe_turbulence_type FeTurbulenceType 172 -
            attributes {
                local base_frequency base_frequency o - String,
                local height height o - String,
                local id id o - String,
                local num_octaves num_octaves o - i64,
                local result result o - String,
                local seed seed o - f64,
                local stitch_tiles stitch_tiles o "noStitch" String,
                local r#type r#type o "turbulence" String,
                local width width o - String,
                local x x o - String,
                local y y o - String,
                xml base base o - String
            }

            choice(super::local::FeTurbulence): v + - super::local::fe_turbulence
        );
        complexType!(cursor_type CursorType 173 -
            attributes {
                local external_resources_required external_resources_required o - bool,
                local id id o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local system_language system_language o - String,
                local x x o - String,
                local y y o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata
        );
        complexType!(a_type AType 174 m
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local onactivate onactivate o - String,
                local onclick onclick o - String,
                local onfocusin onfocusin o - String,
                local onfocusout onfocusout o - String,
                local onload onload o - String,
                local onmousedown onmousedown o - String,
                local onmousemove onmousemove o - String,
                local onmouseout onmouseout o - String,
                local onmouseover onmouseover o - String,
                local onmouseup onmouseup o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local system_language system_language o - String,
                local target target o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local transform transform o - String,
                local unicode_bidi unicode_bidi o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String,
                xml lang lang o - String,
                xml space space o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(view_type ViewType 175 -
            attributes {
                local external_resources_required external_resources_required o - bool,
                local id id o - String,
                local preserve_aspect_ratio preserve_aspect_ratio o "xMidYMid meet" String,
                local view_box view_box o - String,
                local view_target view_target o - String,
                local zoom_and_pan zoom_and_pan o "magnify" String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata
        );
        complexType!(script_type ScriptType 176 m
        attributes {
            local external_resources_required external_resources_required o - bool,
            local id id o - String,
            local r#type r#type r - String,
            xlink actuate actuate o - String,
            xlink arcrole arcrole o - String,
            xlink href href o - String,
            xlink role role o - String,
            xlink show show o - String,
            xlink title title o - String,
            xlink r#type xlink_type o - String,
            xml base base o - String
        });
        complexType!(animate_type AnimateType 177 -
            attributes {
                local accumulate accumulate o "none" String,
                local additive additive o "replace" String,
                local attribute_name attribute_name r - String,
                local attribute_type attribute_type o - String,
                local begin begin o - String,
                local by by o - String,
                local calc_mode calc_mode o "linear" String,
                local dur dur o - String,
                local end end o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o "remove" String,
                local from from o - String,
                local id id o - String,
                local key_splines key_splines o - String,
                local key_times key_times o - String,
                local max max o - String,
                local min min o - String,
                local onbegin onbegin o - String,
                local onend onend o - String,
                local onrepeat onrepeat o - String,
                local repeat_count repeat_count o - String,
                local repeat_dur repeat_dur o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local restart restart o "always" String,
                local system_language system_language o - String,
                local to to o - String,
                local values values o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata
        );
        complexType!(set_type SetType 178 -
            attributes {
                local attribute_name attribute_name r - String,
                local attribute_type attribute_type o - String,
                local begin begin o - String,
                local dur dur o - String,
                local end end o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o "remove" String,
                local id id o - String,
                local max max o - String,
                local min min o - String,
                local onbegin onbegin o - String,
                local onend onend o - String,
                local onrepeat onrepeat o - String,
                local repeat_count repeat_count o - String,
                local repeat_dur repeat_dur o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local restart restart o "always" String,
                local system_language system_language o - String,
                local to to o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata
        );
        complexType!(animate_motion_type AnimateMotionType 179 -
            attributes {
                local accumulate accumulate o "none" String,
                local additive additive o "replace" String,
                local begin begin o - String,
                local by by o - String,
                local calc_mode calc_mode o "linear" String,
                local dur dur o - String,
                local end end o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o "remove" String,
                local from from o - String,
                local id id o - String,
                local key_points key_points o - String,
                local key_splines key_splines o - String,
                local key_times key_times o - String,
                local max max o - String,
                local min min o - String,
                local onbegin onbegin o - String,
                local onend onend o - String,
                local onrepeat onrepeat o - String,
                local origin origin o - String,
                local path path o - String,
                local repeat_count repeat_count o - String,
                local repeat_dur repeat_dur o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local restart restart o "always" String,
                local rotate rotate o - String,
                local system_language system_language o - String,
                local to to o - String,
                local values values o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            mpath(MpathType): o svg mpath mpath_type
        );
        complexType!(mpath_type MpathType 180 -
            attributes {
                local external_resources_required external_resources_required o - bool,
                local id id o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata
        );
        complexType!(animate_color_type AnimateColorType 181 -
            attributes {
                local accumulate accumulate o "none" String,
                local additive additive o "replace" String,
                local attribute_name attribute_name r - String,
                local attribute_type attribute_type o - String,
                local begin begin o - String,
                local by by o - String,
                local calc_mode calc_mode o "linear" String,
                local dur dur o - String,
                local end end o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o "remove" String,
                local from from o - String,
                local id id o - String,
                local key_splines key_splines o - String,
                local key_times key_times o - String,
                local max max o - String,
                local min min o - String,
                local onbegin onbegin o - String,
                local onend onend o - String,
                local onrepeat onrepeat o - String,
                local repeat_count repeat_count o - String,
                local repeat_dur repeat_dur o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local restart restart o "always" String,
                local system_language system_language o - String,
                local to to o - String,
                local values values o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata
        );
        complexType!(animate_transform_type AnimateTransformType 182 -
            attributes {
                local accumulate accumulate o "none" String,
                local additive additive o "replace" String,
                local attribute_name attribute_name r - String,
                local attribute_type attribute_type o - String,
                local begin begin o - String,
                local by by o - String,
                local calc_mode calc_mode o "linear" String,
                local dur dur o - String,
                local end end o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o "remove" String,
                local from from o - String,
                local id id o - String,
                local key_splines key_splines o - String,
                local key_times key_times o - String,
                local max max o - String,
                local min min o - String,
                local onbegin onbegin o - String,
                local onend onend o - String,
                local onrepeat onrepeat o - String,
                local repeat_count repeat_count o - String,
                local repeat_dur repeat_dur o - String,
                local required_extensions required_extensions o - String,
                local required_features required_features o - String,
                local restart restart o "always" String,
                local system_language system_language o - String,
                local to to o - String,
                local r#type r#type o "translate" String,
                local values values o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type xlink_type o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata
        );
        complexType!(font_type FontType 183 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local external_resources_required external_resources_required o - bool,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local horiz_adv_x horiz_adv_x r - f64,
                local horiz_origin_x horiz_origin_x o - f64,
                local horiz_origin_y horiz_origin_y o - f64,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local unicode_bidi unicode_bidi o - String,
                local vert_adv_y vert_adv_y o - f64,
                local vert_origin_x vert_origin_x o - f64,
                local vert_origin_y vert_origin_y o - f64,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            font_face(FontFaceType): r svg font_face font_face_type,
            missing_glyph(MissingGlyphType): r svg missing_glyph missing_glyph_type,
            choice(super::local::Font1): v + - super::local::font1
        );
        complexType!(glyph_type GlyphType 184 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local arabic arabic o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local d d o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_name glyph_name o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local han han o - String,
                local horiz_adv_x horiz_adv_x o - f64,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local unicode unicode o - String,
                local unicode_bidi unicode_bidi o - String,
                local vert_adv_y vert_adv_y o - f64,
                local vert_text_orient vert_text_orient o - String,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xml base base o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(missing_glyph_type MissingGlyphType 185 -
            attributes {
                local alignment_baseline alignment_baseline o - String,
                local baseline_shift baseline_shift o - String,
                local class class ov - String,
                local clip clip o - String,
                local clip_path clip_path o - String,
                local clip_rule clip_rule o - String,
                local color color o - String,
                local color_interpolation color_interpolation o - String,
                local color_interpolation_filters color_interpolation_filters o - String,
                local color_profile color_profile o - String,
                local color_rendering color_rendering o - String,
                local cursor cursor o - String,
                local d d o - String,
                local direction direction o - String,
                local display display o - String,
                local dominant_baseline dominant_baseline o - String,
                local enable_background enable_background o - String,
                local fill fill o - String,
                local fill_opacity fill_opacity o - String,
                local fill_rule fill_rule o - String,
                local filter filter o - String,
                local flood_color flood_color o - String,
                local flood_opacity flood_opacity o - String,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_size_adjust font_size_adjust o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
                local glyph_orientation_vertical glyph_orientation_vertical o - String,
                local horiz_adv_x horiz_adv_x o - f64,
                local id id o - String,
                local image_rendering image_rendering o - String,
                local letter_spacing letter_spacing o - String,
                local lighting_color lighting_color o - String,
                local marker_end marker_end o - String,
                local marker_mid marker_mid o - String,
                local marker_start marker_start o - String,
                local mask mask o - String,
                local opacity opacity o - String,
                local overflow overflow o - String,
                local pointer_events pointer_events o - String,
                local shape_rendering shape_rendering o - String,
                local stop_color stop_color o - String,
                local stop_opacity stop_opacity o - String,
                local stroke stroke o - String,
                local stroke_dasharray stroke_dasharray o - String,
                local stroke_dashoffset stroke_dashoffset o - String,
                local stroke_linecap stroke_linecap o - String,
                local stroke_linejoin stroke_linejoin o - String,
                local stroke_miterlimit stroke_miterlimit o - String,
                local stroke_opacity stroke_opacity o - String,
                local stroke_width stroke_width o - String,
                local style style o - String,
                local text_anchor text_anchor o - String,
                local text_decoration text_decoration o - String,
                local text_rendering text_rendering o - String,
                local unicode_bidi unicode_bidi o - String,
                local vert_adv_y vert_adv_y o - f64,
                local visibility visibility o - String,
                local word_spacing word_spacing o - String,
                local writing_mode writing_mode o - String,
                xml base base o - String
            }

            choice(super::local::MissingGlyph): v + - super::local::missing_glyph
        );
        complexType!(hkern_type HkernType 186 -
        attributes {
            local g1 g1 o - String,
            local g2 g2 o - String,
            local id id o - String,
            local k k r - f64,
            local u1 u1 o - String,
            local u2 u2 o - String,
            xml base base o - String
        });
        complexType!(vkern_type VkernType 187 -
        attributes {
            local g1 g1 o - String,
            local g2 g2 o - String,
            local id id o - String,
            local k k r - f64,
            local u1 u1 o - String,
            local u2 u2 o - String,
            xml base base o - String
        });
        complexType!(font_face_type FontFaceType 188 -
            attributes {
                local accent_height accent_height o - f64,
                local ascent ascent o - f64,
                local baseline baseline o - f64,
                local bbox bbox o - String,
                local cap_height cap_height o - f64,
                local centerline centerline o - f64,
                local descent descent o - f64,
                local font_family font_family o - String,
                local font_size font_size o - String,
                local font_stretch font_stretch o - String,
                local font_style font_style o - String,
                local font_variant font_variant o - String,
                local font_weight font_weight o - String,
                local hanging hanging o - f64,
                local id id o - String,
                local ideographic ideographic o - f64,
                local mathline mathline o - f64,
                local overline_position overline_position o - f64,
                local overline_thickness overline_thickness o - f64,
                local panose_1 panose_1 o - String,
                local slope slope o - f64,
                local stemh stemh o - f64,
                local stemv stemv o - f64,
                local strikethrough_position strikethrough_position o - f64,
                local strikethrough_thickness strikethrough_thickness o - f64,
                local topline topline o - f64,
                local underline_position underline_position o - f64,
                local underline_thickness underline_thickness o - f64,
                local unicode_range unicode_range o - String,
                local units_per_em units_per_em o - f64,
                local widths widths o - String,
                local x_height x_height o - f64,
                xml base base o - String
            }

            desc_title_metadata(super::g::DescTitleMetadata): o + - super::g::desc_title_metadata,
            font_face_src(FontFaceSrcType): r svg font_face_src font_face_src_type,
            definition_src(DefinitionSrcType): r svg definition_src definition_src_type
        );
        complexType!(font_face_src_type FontFaceSrcType 189 -
            attributes {
                local id id o - String,
                xml base base o - String
            }

            choice(super::local::FontFaceSrc): v + - super::local::font_face_src
        );
        complexType!(font_face_uri_type FontFaceUriType 190 -
            attributes {
                local id id o - String,
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String,
                xml base base o - String
            }

            font_face_format(FontFaceFormatType): r svg font_face_format font_face_format_type
        );
        complexType!(font_face_format_type FontFaceFormatType 191 -
        attributes {
            local id id o - String,
            local string string o - String,
            xml base base o - String
        });
        complexType!(font_face_name_type FontFaceNameType 192 -
        attributes {
            local id id o - String,
            local name name o - String,
            xml base base o - String
        });
        complexType!(definition_src_type DefinitionSrcType 193 -
        attributes {
            local id id o - String,
            xlink actuate actuate o - String,
            xlink arcrole arcrole o - String,
            xlink href href o - String,
            xlink role role o - String,
            xlink show show o - String,
            xlink title title o - String,
            xlink r#type r#type o - String,
            xml base base o - String
        });
        complexType!(metadata_type MetadataType 194 m
        attributes {
            local id id o - String,
            xml base base o - String
        });
        complexType!(foreign_object_type ForeignObjectType 195 m
        attributes {
            local alignment_baseline alignment_baseline o - String,
            local baseline_shift baseline_shift o - String,
            local class class ov - String,
            local clip clip o - String,
            local clip_path clip_path o - String,
            local clip_rule clip_rule o - String,
            local color color o - String,
            local color_interpolation color_interpolation o - String,
            local color_interpolation_filters color_interpolation_filters o - String,
            local color_profile color_profile o - String,
            local color_rendering color_rendering o - String,
            local content content o - String,
            local cursor cursor o - String,
            local direction direction o - String,
            local display display o - String,
            local dominant_baseline dominant_baseline o - String,
            local enable_background enable_background o - String,
            local external_resources_required external_resources_required o - bool,
            local fill fill o - String,
            local fill_opacity fill_opacity o - String,
            local fill_rule fill_rule o - String,
            local filter filter o - String,
            local flood_color flood_color o - String,
            local flood_opacity flood_opacity o - String,
            local font_family font_family o - String,
            local font_size font_size o - String,
            local font_size_adjust font_size_adjust o - String,
            local font_stretch font_stretch o - String,
            local font_style font_style o - String,
            local font_variant font_variant o - String,
            local font_weight font_weight o - String,
            local glyph_orientation_horizontal glyph_orientation_horizontal o - String,
            local glyph_orientation_vertical glyph_orientation_vertical o - String,
            local height height r - String,
            local id id o - String,
            local image_rendering image_rendering o - String,
            local letter_spacing letter_spacing o - String,
            local lighting_color lighting_color o - String,
            local marker_end marker_end o - String,
            local marker_mid marker_mid o - String,
            local marker_start marker_start o - String,
            local mask mask o - String,
            local onactivate onactivate o - String,
            local onclick onclick o - String,
            local onfocusin onfocusin o - String,
            local onfocusout onfocusout o - String,
            local onload onload o - String,
            local onmousedown onmousedown o - String,
            local onmousemove onmousemove o - String,
            local onmouseout onmouseout o - String,
            local onmouseover onmouseover o - String,
            local onmouseup onmouseup o - String,
            local opacity opacity o - String,
            local overflow overflow o - String,
            local pointer_events pointer_events o - String,
            local required_extensions required_extensions o - String,
            local required_features required_features o - String,
            local shape_rendering shape_rendering o - String,
            local stop_color stop_color o - String,
            local stop_opacity stop_opacity o - String,
            local stroke stroke o - String,
            local stroke_dasharray stroke_dasharray o - String,
            local stroke_dashoffset stroke_dashoffset o - String,
            local stroke_linecap stroke_linecap o - String,
            local stroke_linejoin stroke_linejoin o - String,
            local stroke_miterlimit stroke_miterlimit o - String,
            local stroke_opacity stroke_opacity o - String,
            local stroke_width stroke_width o - String,
            local style style o - String,
            local system_language system_language o - String,
            local text_anchor text_anchor o - String,
            local text_decoration text_decoration o - String,
            local text_rendering text_rendering o - String,
            local transform transform o - String,
            local unicode_bidi unicode_bidi o - String,
            local visibility visibility o - String,
            local width width r - String,
            local word_spacing word_spacing o - String,
            local writing_mode writing_mode o - String,
            local x x o - String,
            local y y o - String,
            xml base base o - String,
            xml lang lang o - String,
            xml space space o - String
        });
    }
    pub mod e {
        xust_xsd::use_xsd!();

        element_type!(svg svg(super::ct::SvgType) super::ct::svg_type);
        element_type!(svg g(super::ct::GType) super::ct::g_type);
        element_type!(svg defs(super::ct::DefsType) super::ct::defs_type);
        element_type!(svg desc(super::ct::DescType) super::ct::desc_type);
        element_type!(svg title(super::ct::TitleType) super::ct::title_type);
        element_type!(svg symbol(super::ct::SymbolType) super::ct::symbol_type);
        element_type!(svg r#use(super::ct::UseType) super::ct::use_type);
        element_type!(svg image(super::ct::ImageType) super::ct::image_type);
        element_type!(svg switch(super::ct::SwitchType) super::ct::switch_type);
        element_type!(svg style(super::ct::StyleType) super::ct::style_type);
        element_type!(svg path(super::ct::PathType) super::ct::path_type);
        element_type!(svg rect(super::ct::RectType) super::ct::rect_type);
        element_type!(svg circle(super::ct::CircleType) super::ct::circle_type);
        element_type!(svg ellipse(super::ct::EllipseType) super::ct::ellipse_type);
        element_type!(svg line(super::ct::LineType) super::ct::line_type);
        element_type!(svg polyline(super::ct::PolylineType) super::ct::polyline_type);
        element_type!(svg polygon(super::ct::PolygonType) super::ct::polygon_type);
        element_type!(svg text(super::ct::TextType) super::ct::text_type);
        element_type!(svg tspan(super::ct::TspanType) super::ct::tspan_type);
        element_type!(svg tref(super::ct::TrefType) super::ct::tref_type);
        element_type!(svg text_path(super::ct::TextPathType) super::ct::text_path_type);
        element_type!(svg alt_glyph(super::ct::AltGlyphType) super::ct::alt_glyph_type);
        element_type!(svg alt_glyph_def(super::ct::AltGlyphDefType) super::ct::alt_glyph_def_type);
        element_type!(svg alt_glyph_item(super::ct::AltGlyphItemType) super::ct::alt_glyph_item_type);
        element_type!(svg glyph_ref(super::ct::GlyphRefType) super::ct::glyph_ref_type);
        element_type!(svg marker(super::ct::MarkerType) super::ct::marker_type);
        element_type!(svg color_profile(super::ct::ColorProfileType) super::ct::color_profile_type);
        element_type!(svg linear_gradient(super::ct::LinearGradientType) super::ct::linear_gradient_type);
        element_type!(svg radial_gradient(super::ct::RadialGradientType) super::ct::radial_gradient_type);
        element_type!(svg stop(super::ct::StopType) super::ct::stop_type);
        element_type!(svg pattern(super::ct::PatternType) super::ct::pattern_type);
        element_type!(svg clip_path(super::ct::ClipPathType) super::ct::clip_path_type);
        element_type!(svg mask(super::ct::MaskType) super::ct::mask_type);
        element_type!(svg filter(super::ct::FilterType) super::ct::filter_type);
        element_type!(svg fe_distant_light(super::ct::FeDistantLightType) super::ct::fe_distant_light_type);
        element_type!(svg fe_point_light(super::ct::FePointLightType) super::ct::fe_point_light_type);
        element_type!(svg fe_spot_light(super::ct::FeSpotLightType) super::ct::fe_spot_light_type);
        element_type!(svg fe_blend(super::ct::FeBlendType) super::ct::fe_blend_type);
        element_type!(svg fe_color_matrix(super::ct::FeColorMatrixType) super::ct::fe_color_matrix_type);
        element_type!(svg fe_component_transfer(super::ct::FeComponentTransferType) super::ct::fe_component_transfer_type);
        element_type!(svg fe_func_r(super::ct::FeFuncRType) super::ct::fe_func_r_type);
        element_type!(svg fe_func_g(super::ct::FeFuncGType) super::ct::fe_func_g_type);
        element_type!(svg fe_func_b(super::ct::FeFuncBType) super::ct::fe_func_b_type);
        element_type!(svg fe_func_a(super::ct::FeFuncAType) super::ct::fe_func_a_type);
        element_type!(svg fe_composite(super::ct::FeCompositeType) super::ct::fe_composite_type);
        element_type!(svg fe_convolve_matrix(super::ct::FeConvolveMatrixType) super::ct::fe_convolve_matrix_type);
        element_type!(svg fe_diffuse_lighting(super::ct::FeDiffuseLightingType) super::ct::fe_diffuse_lighting_type);
        element_type!(svg fe_displacement_map(super::ct::FeDisplacementMapType) super::ct::fe_displacement_map_type);
        element_type!(svg fe_flood(super::ct::FeFloodType) super::ct::fe_flood_type);
        element_type!(svg fe_gaussian_blur(super::ct::FeGaussianBlurType) super::ct::fe_gaussian_blur_type);
        element_type!(svg fe_image(super::ct::FeImageType) super::ct::fe_image_type);
        element_type!(svg fe_merge(super::ct::FeMergeType) super::ct::fe_merge_type);
        element_type!(svg fe_merge_node(super::ct::FeMergeNodeType) super::ct::fe_merge_node_type);
        element_type!(svg fe_morphology(super::ct::FeMorphologyType) super::ct::fe_morphology_type);
        element_type!(svg fe_offset(super::ct::FeOffsetType) super::ct::fe_offset_type);
        element_type!(svg fe_specular_lighting(super::ct::FeSpecularLightingType) super::ct::fe_specular_lighting_type);
        element_type!(svg fe_tile(super::ct::FeTileType) super::ct::fe_tile_type);
        element_type!(svg fe_turbulence(super::ct::FeTurbulenceType) super::ct::fe_turbulence_type);
        element_type!(svg cursor(super::ct::CursorType) super::ct::cursor_type);
        element_type!(svg a(super::ct::AType) super::ct::a_type);
        element_type!(svg view(super::ct::ViewType) super::ct::view_type);
        element_type!(svg script(super::ct::ScriptType) super::ct::script_type);
        element_type!(svg animate(super::ct::AnimateType) super::ct::animate_type);
        element_type!(svg set(super::ct::SetType) super::ct::set_type);
        element_type!(svg animate_motion(super::ct::AnimateMotionType) super::ct::animate_motion_type);
        element_type!(svg mpath(super::ct::MpathType) super::ct::mpath_type);
        element_type!(svg animate_color(super::ct::AnimateColorType) super::ct::animate_color_type);
        element_type!(svg animate_transform(super::ct::AnimateTransformType) super::ct::animate_transform_type);
        element_type!(svg font(super::ct::FontType) super::ct::font_type);
        element_type!(svg glyph(super::ct::GlyphType) super::ct::glyph_type);
        element_type!(svg missing_glyph(super::ct::MissingGlyphType) super::ct::missing_glyph_type);
        element_type!(svg hkern(super::ct::HkernType) super::ct::hkern_type);
        element_type!(svg vkern(super::ct::VkernType) super::ct::vkern_type);
        element_type!(svg font_face(super::ct::FontFaceType) super::ct::font_face_type);
        element_type!(svg font_face_src(super::ct::FontFaceSrcType) super::ct::font_face_src_type);
        element_type!(svg font_face_uri(super::ct::FontFaceUriType) super::ct::font_face_uri_type);
        element_type!(svg font_face_format(super::ct::FontFaceFormatType) super::ct::font_face_format_type);
        element_type!(svg font_face_name(super::ct::FontFaceNameType) super::ct::font_face_name_type);
        element_type!(svg definition_src(super::ct::DefinitionSrcType) super::ct::definition_src_type);
        element_type!(svg metadata(super::ct::MetadataType) super::ct::metadata_type);
        element_type!(svg foreign_object(super::ct::ForeignObjectType) super::ct::foreign_object_type);
    }
}
pub mod xlink {
    pub mod local {
        xust_xsd::use_xsd!();
        choice!(arc_model ArcModel -);
        choice!(extended_model1 ExtendedModel1 -);
        choice!(extended_model2 ExtendedModel2 -);
        choice!(extended_model3 ExtendedModel3 -);
    }
    pub mod g {
        xust_xsd::use_xsd!();
        sequence!(default simple_model SimpleModel m
            any(Unit): v - - unit
        );
        choice!(extended_model ExtendedModel -
            Choice1(super::local::ArcModel): r + - super::local::arc_model,
            Choice2(super::local::ExtendedModel1): r + - super::local::extended_model1,
            Choice3(super::local::ExtendedModel2): r + - super::local::extended_model2,
            Choice4(super::local::ExtendedModel3): r + - super::local::extended_model3
        );
        sequence!(default title_model TitleModel m
            any(Unit): v - - unit
        );
        sequence!(default resource_model ResourceModel m
            any(Unit): v - - unit
        );
        sequence!(default locator_model LocatorModel -
            choice(super::local::ArcModel): v + - super::local::arc_model
        );
        sequence!(default arc_model ArcModel -
            choice(super::local::ArcModel): v + - super::local::arc_model
        );
    }
    pub mod ct {
        xust_xsd::use_xsd!();
        simpleType!(type_type TypeType);
        simpleType!(href_type HrefType);
        simpleType!(role_type RoleType);
        simpleType!(arcrole_type ArcroleType);
        simpleType!(title_attr_type TitleAttrType);
        simpleType!(show_type ShowType);
        simpleType!(actuate_type ActuateType);
        simpleType!(label_type LabelType);
        simpleType!(from_type FromType);
        simpleType!(to_type ToType);
        complexType!(simple Simple 62 m
            attributes {
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String
            }

            any(Unit): v - - unit
        );
        complexType!(extended Extended 63 -
            attributes {
                xlink role role o - String,
                xlink title title o - String,
                xlink r#type r#type r - String
            }

            extended_model(super::g::ExtendedModel): v + - super::g::extended_model
        );
        complexType!(title_elt_type TitleEltType 64 m
            attributes {
                xlink r#type r#type r - String,
                xml lang lang o - String
            }

            any(Unit): v - - unit
        );
        complexType!(resource_type ResourceType 65 m
            attributes {
                xlink label label o - String,
                xlink role role o - String,
                xlink title title o - String,
                xlink r#type r#type r - String
            }

            any(Unit): v - - unit
        );
        complexType!(locator_type LocatorType 66 -
            attributes {
                xlink href href r - String,
                xlink label label o - String,
                xlink role role o - String,
                xlink title title o - String,
                xlink r#type r#type r - String
            }

            choice(super::local::ArcModel): v + - super::local::arc_model
        );
        complexType!(arc_type ArcType 67 -
            attributes {
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink from from o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink to to o - String,
                xlink r#type r#type r - String
            }

            choice(super::local::ArcModel): v + - super::local::arc_model
        );
    }
    pub mod e {
        xust_xsd::use_xsd!();
    }
}
pub mod xml {
    pub mod local {
        xust_xsd::use_xsd!();
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
    }
    pub mod e {
        xust_xsd::use_xsd!();
    }
}
