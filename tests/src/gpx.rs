#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
xust_xsd::use_xsd!();
use xust_tree::qnames::{NCName, Namespace, Prefix, QNameCollection};

type P = xust_xsd::parse::ParseEnv<QNames>;

pub struct QNames {
    qnames: QNameCollection,
    local: LocalQNames,
    default: DefaultQNames,
}
pub fn qnames(qnames: QNameCollection) -> QNames {
    QNames {
        local: LocalQNames::new(&qnames),
        default: DefaultQNames::new(&qnames),
        qnames,
    }
}
ns!(DefaultQNames "http://www.topografix.com/GPX/1/1" =>
    ageofdgpsdata => "ageofdgpsdata"
    author => "author"
    bounds => "bounds"
    cmt => "cmt"
    copyright => "copyright"
    degrees_type => "degreesType"
    desc => "desc"
    dgps_station_type => "dgpsStationType"
    dgpsid => "dgpsid"
    ele => "ele"
    email => "email"
    extensions => "extensions"
    fix => "fix"
    fix_type => "fixType"
    geoidheight => "geoidheight"
    gpx => "gpx"
    hdop => "hdop"
    keywords => "keywords"
    latitude_type => "latitudeType"
    license => "license"
    link => "link"
    longitude_type => "longitudeType"
    magvar => "magvar"
    metadata => "metadata"
    name => "name"
    number => "number"
    pdop => "pdop"
    pt => "pt"
    rte => "rte"
    rtept => "rtept"
    sat => "sat"
    src => "src"
    sym => "sym"
    text => "text"
    time => "time"
    trk => "trk"
    trkpt => "trkpt"
    trkseg => "trkseg"
    r#type => "type"
    vdop => "vdop"
    wpt => "wpt"
    year => "year"
);
ns!(LocalQNames "" =>
    author => "author"
    creator => "creator"
    domain => "domain"
    href => "href"
    id => "id"
    lat => "lat"
    lon => "lon"
    maxlat => "maxlat"
    maxlon => "maxlon"
    minlat => "minlat"
    minlon => "minlon"
    version => "version"
);
pub mod default {
    pub mod local {
        xust_xsd::use_xsd!();
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
        complexType!(gpx_type GpxType 52 -
            attributes {
                local creator creator r - String,
                local version version r - String
            }

            metadata(MetadataType): o default metadata metadata_type,
            wpt(WptType): v default wpt wpt_type,
            rte(RteType): v default rte rte_type,
            trk(TrkType): v default trk trk_type,
            extensions(ExtensionsType): o default extensions extensions_type
        );
        complexType!(metadata_type MetadataType 53 -
            name(ct::String): o default name ct::string,
            desc(ct::String): o default desc ct::string,
            author(PersonType): o default author person_type,
            copyright(CopyrightType): o default copyright copyright_type,
            link(LinkType): v default link link_type,
            time(ct::DateTime): o default time ct::date_time,
            keywords(ct::String): o default keywords ct::string,
            bounds(BoundsType): o default bounds bounds_type,
            extensions(ExtensionsType): o default extensions extensions_type
        );
        complexType!(wpt_type WptType 54 -
            attributes {
                local lat lat r - String,
                local lon lon r - String
            }

            ele(ct::Decimal): o default ele ct::decimal,
            time(ct::DateTime): o default time ct::date_time,
            magvar(DegreesType): o default magvar degrees_type,
            geoidheight(ct::Decimal): o default geoidheight ct::decimal,
            name(ct::String): o default name ct::string,
            cmt(ct::String): o default cmt ct::string,
            desc(ct::String): o default desc ct::string,
            src(ct::String): o default src ct::string,
            link(LinkType): v default link link_type,
            sym(ct::String): o default sym ct::string,
            r#type(ct::String): o default r#type ct::string,
            fix(FixType): o default fix fix_type,
            sat(ct::NonNegativeInteger): o default sat ct::non_negative_integer,
            hdop(ct::Decimal): o default hdop ct::decimal,
            vdop(ct::Decimal): o default vdop ct::decimal,
            pdop(ct::Decimal): o default pdop ct::decimal,
            ageofdgpsdata(ct::Decimal): o default ageofdgpsdata ct::decimal,
            dgpsid(DgpsStationType): o default dgpsid dgps_station_type,
            extensions(ExtensionsType): o default extensions extensions_type
        );
        complexType!(rte_type RteType 55 -
            name(ct::String): o default name ct::string,
            cmt(ct::String): o default cmt ct::string,
            desc(ct::String): o default desc ct::string,
            src(ct::String): o default src ct::string,
            link(LinkType): v default link link_type,
            number(ct::NonNegativeInteger): o default number ct::non_negative_integer,
            r#type(ct::String): o default r#type ct::string,
            extensions(ExtensionsType): o default extensions extensions_type,
            rtept(WptType): v default rtept wpt_type
        );
        complexType!(trk_type TrkType 56 -
            name(ct::String): o default name ct::string,
            cmt(ct::String): o default cmt ct::string,
            desc(ct::String): o default desc ct::string,
            src(ct::String): o default src ct::string,
            link(LinkType): v default link link_type,
            number(ct::NonNegativeInteger): o default number ct::non_negative_integer,
            r#type(ct::String): o default r#type ct::string,
            extensions(ExtensionsType): o default extensions extensions_type,
            trkseg(TrksegType): v default trkseg trkseg_type
        );
        complexType!(extensions_type ExtensionsType 57 -
            any(Unit): v - - unit
        );
        complexType!(trkseg_type TrksegType 58 -
            trkpt(WptType): v default trkpt wpt_type,
            extensions(ExtensionsType): o default extensions extensions_type
        );
        complexType!(copyright_type CopyrightType 59 -
            attributes {
                local author author r - String
            }

            year(ct::GYear): o default year ct::g_year,
            license(ct::AnyUri): o default license ct::any_uri
        );
        complexType!(link_type LinkType 60 -
            attributes {
                local href href r - String
            }

            text(ct::String): o default text ct::string,
            r#type(ct::String): o default r#type ct::string
        );
        complexType!(email_type EmailType 61 -
        attributes {
            local domain domain r - String,
            local id id r - String
        });
        complexType!(person_type PersonType 62 -
            name(ct::String): o default name ct::string,
            email(EmailType): o default email email_type,
            link(LinkType): o default link link_type
        );
        complexType!(pt_type PtType 63 -
            attributes {
                local lat lat r - String,
                local lon lon r - String
            }

            ele(ct::Decimal): o default ele ct::decimal,
            time(ct::DateTime): o default time ct::date_time
        );
        complexType!(ptseg_type PtsegType 64 -
            pt(PtType): v default pt pt_type
        );
        complexType!(bounds_type BoundsType 65 -
        attributes {
            local maxlat maxlat r - String,
            local maxlon maxlon r - String,
            local minlat minlat r - String,
            local minlon minlon r - String
        });
        simpleType!(latitude_type LatitudeType);
        simpleType!(longitude_type LongitudeType);
        simpleType!(degrees_type DegreesType);
        simpleType!(fix_type FixType);
        simpleType!(dgps_station_type DgpsStationType);
    }
    pub mod e {
        xust_xsd::use_xsd!();

        element_type!(default gpx(super::ct::GpxType) super::ct::gpx_type);
    }
}
