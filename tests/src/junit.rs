use anyhow::Result;
use xust_tree::{
    qnames::{NCName, QNameCollector},
    tree::builder::DocumentBuilder,
    typed_value::{TypeId, XS_UNTYPED, XS_UNTYPED_ATOMIC},
};
use xust_xml::write::{self, XmlOutputParameters};
use xust_xsd::atomic::Atomic;

// structures based on jenkins-junit.xsd and
// <https://www.ibm.com/support/knowledgecenter/SSQ2R2_9.1.1/com.ibm.rsar.analysis.codereview.cobol.doc/topics/cac_useresults_junit.html?view=embed>

pub struct TestSuites {
    pub name: String,
    pub tests: Option<usize>,
    pub failures: Option<usize>,
    pub time: Option<f64>,
    pub test_suites: Vec<TestSuite>,
}

pub struct TestSuite {
    pub name: String,
    pub tests: Option<usize>,
    pub failures: Option<usize>,
    pub time: Option<f64>,
    pub test_cases: Vec<TestCase>,
}

pub struct TestCase {
    pub name: String,
    pub time: Option<f64>,
    pub failure: Option<Failure>,
}

pub struct Failure {
    pub message: String,
    pub r#type: String,
    pub text: String,
}

impl TestSuites {
    pub fn add_test_case(&mut self, suite: &str, test_case: TestCase) {
        if let Some(test_suite) = self
            .test_suites
            .iter_mut()
            .find(|test_suite| test_suite.name == suite)
        {
            test_suite.test_cases.push(test_case);
        } else {
            self.test_suites.push(TestSuite {
                failures: None,
                name: suite.to_string(),
                test_cases: vec![test_case],
                tests: None,
                time: None,
            });
        }
    }
}

pub fn test_suites_to_xml(test_suites: &TestSuites) -> Result<String> {
    const UNTYPED: TypeId = XS_UNTYPED;
    const UNTYPED_ATOMIC: TypeId = XS_UNTYPED_ATOMIC;
    let qnames = QNameCollector::default();
    let e_testsuites = qnames.add_local_qname(NCName("testsuites"));
    let e_testsuite = qnames.add_local_qname(NCName("testsuite"));
    let e_testcase = qnames.add_local_qname(NCName("testcase"));
    let e_failure = qnames.add_local_qname(NCName("failure"));
    let a_message = qnames.add_local_qname(NCName("name"));
    let a_name = qnames.add_local_qname(NCName("name"));
    let a_tests = qnames.add_local_qname(NCName("tests"));
    let a_failures = qnames.add_local_qname(NCName("failures"));
    let a_time = qnames.add_local_qname(NCName("time"));
    let a_type = qnames.add_local_qname(NCName("type"));
    let mut builder = DocumentBuilder::<Atomic>::new();
    let mut eb = builder.add_element(e_testsuites, UNTYPED, &[]);
    eb.add_attribute_from_qname(a_name, UNTYPED_ATOMIC, &test_suites.name);
    if let Some(tests) = &test_suites.tests {
        eb.add_attribute_from_qname(a_tests, UNTYPED_ATOMIC, &format!("{}", tests));
    }
    if let Some(failures) = &test_suites.failures {
        eb.add_attribute_from_qname(a_failures, UNTYPED_ATOMIC, &format!("{}", failures));
    }
    if let Some(time) = &test_suites.time {
        eb.add_attribute_from_qname(a_time, UNTYPED_ATOMIC, &format!("{}", time));
    }
    {
        let mut eb = eb.add_children();
        for test_suite in &test_suites.test_suites {
            eb.push_str("\n\t");
            let mut e = eb.add_element(e_testsuite, UNTYPED, &[]);
            e.add_attribute_from_qname(a_name, UNTYPED_ATOMIC, &test_suite.name);
            if let Some(tests) = &test_suite.tests {
                e.add_attribute_from_qname(a_tests, UNTYPED_ATOMIC, &format!("{}", tests));
            }
            if let Some(failures) = &test_suite.failures {
                e.add_attribute_from_qname(a_failures, UNTYPED_ATOMIC, &format!("{}", failures));
            }
            if let Some(time) = &test_suite.time {
                e.add_attribute_from_qname(a_time, UNTYPED_ATOMIC, &format!("{}", time));
            }
            let mut e = e.add_children();
            for test_case in &test_suite.test_cases {
                e.push_str("\n\t\t");
                let mut e = e.add_element(e_testcase, UNTYPED, &[]);
                e.add_attribute_from_qname(a_name, UNTYPED_ATOMIC, &test_case.name);
                let mut e = e.add_children();
                if let Some(failure) = &test_case.failure {
                    let mut e = e.add_element(e_failure, UNTYPED, &[]);
                    if !failure.message.is_empty() {
                        e.add_attribute_from_qname(a_message, UNTYPED_ATOMIC, &failure.message);
                    }
                    if !failure.r#type.is_empty() {
                        e.add_attribute_from_qname(a_type, UNTYPED_ATOMIC, &failure.r#type);
                    }
                    let mut e = e.add_children();
                    e.push_str(&failure.text);
                }
            }
            e.push_str("\n\t");
        }
        eb.push_str("\n");
    }
    let tree = builder.build(qnames.collect());
    let config = XmlOutputParameters::default();
    Ok(write::tree_to_string(&tree, config)?)
}
