use crate::{
    assert::EvalResult,
    tests_model::{
        Assertion, Catalog, Code, Dependency, DependencyType, Environment, Module, Schema, Source,
        Spec, TestCase, TestCaseResult, TestSet, Validation,
    },
};
use anyhow::Result;
use enumflags2::BitFlags;
use std::collections::{BTreeMap, HashMap, HashSet};
use std::path::{Path, PathBuf};
use std::rc::Rc;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::Mutex;
use xust_eval::{
    eval::{
        atomize_sequence,
        context::{read_xml_arc, read_xml_rc, Context, ContextInit, GlobalContext},
        eval_xquery,
    },
    r#fn::FunctionDefinitions,
    Ref,
};
use xust_grammar::{parse, ParseInit, XQuery};
use xust_tree::{
    node::{Element, Node},
    qnames::{NCName, Namespace, Prefix, QNameId},
    tree,
};
use xust_xsd::{atomic::Atomic, load_validator, xpath_error::Error, xsd_validator::XsdValidator};

#[derive(PartialEq, Eq, PartialOrd, Ord)]
struct CacheKey {
    schemas: Vec<PathBuf>,
    path: PathBuf,
}

impl From<&Path> for CacheKey {
    fn from(path: &Path) -> Self {
        Self {
            schemas: Vec::new(),
            path: path.into(),
        }
    }
}

impl From<PathBuf> for CacheKey {
    fn from(path: PathBuf) -> Self {
        Self {
            schemas: Vec::new(),
            path,
        }
    }
}

#[derive(Clone)]
pub struct DomCache<T: Ref> {
    validator_cache: Rc<Mutex<BTreeMap<Vec<PathBuf>, Option<XsdValidator>>>>,
    dom_cache: Rc<Mutex<BTreeMap<CacheKey, Option<T>>>>,
    xml_reader: Rc<dyn Fn(&Path, Option<XsdValidator>) -> std::result::Result<T, Error>>,
}

fn read_validator<T: Ref>(
    dom_cache: &DomCache<T>,
    mut schemas: Vec<PathBuf>,
) -> Option<XsdValidator> {
    let mut cache = dom_cache.validator_cache.lock().unwrap();
    schemas.sort();
    if let Some(r) = cache.get(&schemas) {
        r.clone()
    } else {
        let r = match load_validator(&schemas, None) {
            Err(e) => {
                eprintln!("Parsing {:?}: {:?}", schemas, e);
                None
            }
            Ok(p) => Some(p),
        };
        cache.insert(schemas, r.clone());
        r
    }
}

fn read_cached_xml<T: Ref>(
    dom_cache: &DomCache<T>,
    path: &Path,
    schemas: Vec<PathBuf>,
) -> Option<T> {
    let mut cache = dom_cache.dom_cache.lock().unwrap();
    let key = CacheKey {
        path: path.into(),
        schemas,
    };
    if let Some(r) = cache.get(&key) {
        r.clone()
    } else {
        let validator = read_validator(dom_cache, key.schemas.clone());
        let r = match (dom_cache.xml_reader)(path, validator) {
            Err(e) => {
                eprintln!("Parsing {}: {}", path.display(), e);
                None
            }
            Ok(p) => Some(p),
        };
        cache.insert(key, r.clone());
        r
    }
}

fn attribute_value<T: Ref>(e: &Element<T>, node_name: NCName) -> Option<String> {
    e.attributes()
        .find(|a| a.node_name().to_ref().local_name() == node_name)
        .map(|a| a.string_value().to_string())
}

fn root_element<T: Ref>(node: &Node<T>) -> Option<Element<T>> {
    node.children().find_map(|e| e.element())
}

pub fn default_dom_cache() -> DomCache<Rc<tree::Tree<Atomic>>> {
    DomCache {
        validator_cache: Rc::new(Mutex::new(BTreeMap::default())),
        dom_cache: Rc::new(Mutex::new(BTreeMap::default())),
        xml_reader: Rc::new(read_xml_rc),
    }
}

pub fn arc_dom_cache() -> DomCache<Arc<tree::Tree<Atomic>>> {
    DomCache {
        validator_cache: Rc::new(Mutex::new(BTreeMap::default())),
        dom_cache: Rc::new(Mutex::new(BTreeMap::default())),
        xml_reader: Rc::new(read_xml_arc),
    }
}

pub fn parse_catalog<T: Ref>(
    context_init: &ContextInit<T>,
    cache: DomCache<T>,
    catalogue_path: &Path,
    test_filter: Option<&str>,
    ignored_tests: &HashSet<&str>,
) -> Result<Catalog<T>> {
    let catalogue_path = PathBuf::from(catalogue_path);
    let dir = catalogue_path.parent().unwrap();
    let document = read_cached_xml(&cache, &catalogue_path, Vec::new()).unwrap();
    let document = Node::root(document);
    let root_element = root_element(&document).unwrap();
    let mut catalog = Catalog::new();
    let mut test_sets = Vec::new();
    for child in root_element.children() {
        if let Some(e) = child.element() {
            let en = e.node_name().to_ref();
            match en.local_name().as_str() {
                "environment" => {
                    let e = parse_environment(
                        context_init,
                        &e,
                        dir,
                        &cache,
                        &BTreeMap::new(),
                        &BTreeMap::new(),
                    )?;
                    if let Some(name) = e.name.clone() {
                        catalog.environments.insert(name, e);
                    }
                }
                "test-set" => {
                    let name = attribute_value(&e, NCName("name")).unwrap().to_string();
                    let file = attribute_value(&e, NCName("file")).unwrap().to_string();
                    test_sets.push((name, file));
                }
                _ => {
                    eprintln!(
                        "child {:?} {:?}",
                        en.local_name(),
                        child.node_name().unwrap().to_ref().local_name()
                    );
                }
            }
        }
    }
    for ts in test_sets
        .iter()
        .map(|(name, file)| {
            parse_test_set(
                context_init,
                name,
                dir.join(file),
                cache.clone(),
                &catalog.environments,
                test_filter,
                ignored_tests,
            )
        })
        .collect::<Result<Vec<_>>>()?
    {
        catalog.test_sets.insert(ts.name.clone(), ts);
    }
    Ok(catalog)
}

fn parse_module<T: Ref>(element: &Element<T>, dir: &Path) -> Result<Module> {
    let uri = attribute_value(element, NCName("uri")).unwrap();
    let location = attribute_value(element, NCName("location"));
    let file = file(element, dir).unwrap();
    Ok(Module {
        uri,
        location,
        file,
    })
}

fn parse_environment<T: Ref>(
    context_init: &ContextInit<T>,
    element: &Element<T>,
    dir: &Path,
    cache: &DomCache<T>,
    env: &BTreeMap<String, Environment<T>>,
    env2: &BTreeMap<String, Environment<T>>,
) -> Result<Environment<T>> {
    let mut new_env = Environment::default();
    if let Ok(dir) = dir.canonicalize() {
        if let Ok(url) = url::Url::from_directory_path(dir) {
            new_env.static_base_uri = Some(url);
        }
    }
    if let Some(r) = attribute_value(element, NCName("ref")) {
        if let Some(e) = env.get(&*r) {
            new_env = e.clone();
        } else if let Some(e) = env2.get(&*r) {
            new_env = e.clone();
        }
    }
    let (schemas, validator, schema_paths) = parse_schemas(element, dir, cache);
    new_env.schemas = schemas;
    new_env.validator = validator;
    if let Some(source) = get_child(element, NCName("source"))
        .as_ref()
        .and_then(|e| parse_source(e, dir, cache, schema_paths))
    {
        new_env.source = Some(source);
    }
    for ns in element
        .children()
        .filter_map(|n| n.element())
        .filter(|e| e.node_name().to_ref().local_name() == NCName("namespace"))
    {
        new_env.namespaces.insert(
            attribute_value(&ns, NCName("prefix")).unwrap(),
            attribute_value(&ns, NCName("uri")).unwrap(),
        );
    }
    for ns in element
        .children()
        .filter_map(|n| n.element())
        .filter(|e| e.node_name().to_ref().local_name() == NCName("static-base-uri"))
    {
        if let Some(uri) = attribute_value(&ns, NCName("uri")) {
            new_env.static_base_uri = if uri == "#UNDEFINED" {
                None
            } else {
                Some(url::Url::parse(&uri)?)
            };
        }
    }
    for ns in element
        .children()
        .filter_map(|n| n.element())
        .filter(|e| e.node_name().to_ref().local_name() == NCName("param"))
    {
        if let (Some(name), Some(select)) = (
            attribute_value(&ns, NCName("name")),
            attribute_value(&ns, NCName("select")),
        ) {
            let qname = element
                .node()
                .qnames()
                .qname(Prefix(""), Namespace(""), NCName(&name));
            new_env
                .param
                .insert(qname, parse_expression::<T>(context_init, &None, &select));
        }
    }
    if let Some(name) = attribute_value(element, NCName("name")) {
        new_env.name = Some(name);
    }
    Ok(new_env)
}

fn get_specs<T: Ref>(n: &Element<T>) -> BitFlags<Spec> {
    let s = n
        .children()
        .filter_map(|c| c.element())
        .filter_map(|e| {
            if e.node_name().to_ref().local_name() == NCName("dependency")
                && attribute_value(&e, NCName("type")) == Some("spec".to_string())
            {
                return attribute_value(&e, NCName("value"));
            }
            None
        })
        .collect::<Vec<_>>();
    s.iter()
        .flat_map(|s| s.split_whitespace())
        .map(Spec::from_string)
        .fold(BitFlags::empty(), |acc, v| acc | v)
}
fn get_child<T: Ref>(element: &Element<T>, local_part: NCName) -> Option<Element<T>> {
    element
        .children()
        .filter_map(|n| n.element())
        .find(|e| e.node_name().to_ref().local_name() == local_part)
}
struct DependencyAttQNames {
    r#type: Option<QNameId>,
    value: Option<QNameId>,
    satisfied: Option<QNameId>,
}
fn parse_test_set<T: Ref>(
    context_init: &ContextInit<T>,
    name: &str,
    path: PathBuf,
    cache: DomCache<T>,
    env2: &BTreeMap<String, Environment<T>>,
    test_filter: Option<&str>,
    ignored_tests: &HashSet<&str>,
) -> Result<TestSet<T>> {
    let dir = path.parent().unwrap();
    let document = read_cached_xml(&cache, &path, Vec::new()).unwrap();
    let document = Node::root(document);
    let dependency_qnames = DependencyAttQNames {
        r#type: document.find_qname(Namespace(""), NCName("type")),
        value: document.find_qname(Namespace(""), NCName("value")),
        satisfied: document.find_qname(Namespace(""), NCName("satisfied")),
    };
    let root_element = root_element(&document).unwrap();
    let mut ts = TestSet {
        name: name.to_string(),
        test_cases: BTreeMap::new(),
        environments: BTreeMap::new(),
        dependencies: Vec::new(),
    };
    let root_children = root_element.children();
    ts.dependencies = parse_dependencies(&root_element, &dependency_qnames)?;
    let test_set_specs = get_specs(&root_element);
    for child in root_children {
        if let Some(e) = child.element() {
            if e.node_name().to_ref().local_name() == NCName("environment") {
                let e = parse_environment(context_init, &e, dir, &cache, env2, &BTreeMap::new())?;
                if let Some(name) = e.name.clone() {
                    ts.environments.insert(name, e);
                }
            } else if e.node_name().to_ref().local_name() == NCName("test-case") {
                let name = attribute_value(&e, NCName("name")).unwrap().to_string();
                if !test_filter.map(|f| name.contains(f)).unwrap_or(true) {
                    continue;
                }
                if ignored_tests.contains(name.as_str()) {
                    continue;
                }
                let test = get_child(&e, NCName("test")).unwrap();
                let specs = test_set_specs | get_specs(&e);
                let result = get_child(&e, NCName("result")).unwrap();
                let environment = get_child(&e, NCName("environment"));
                let environment: Option<Environment<T>> = if let Some(e) = environment {
                    Some(parse_environment(
                        context_init,
                        &e,
                        dir,
                        &cache,
                        &ts.environments,
                        env2,
                    )?)
                } else {
                    None
                };
                let module = e
                    .children()
                    .filter_map(|n| n.element())
                    .filter(|e| e.node_name().to_ref().local_name() == NCName("module"))
                    .map(|e| parse_module(&e, dir).unwrap())
                    .collect();
                let dependencies = parse_dependencies(&e, &dependency_qnames)?;
                let assertions = parse_assertions(context_init, &environment, &result);
                let result = TestCaseResult { assertions };
                let test: String = test.children().fold(String::new(), |mut a, e| {
                    a.push_str(&e.string_value());
                    a
                });

                let tc = TestCase {
                    name,
                    test,
                    specs,
                    result,
                    environment,
                    module,
                    dependencies,
                };
                ts.test_cases.insert(tc.name.clone(), tc);
            }
        }
    }
    Ok(ts)
}
fn parse_dependency<T: Ref>(
    element: &Element<T>,
    qnames: &DependencyAttQNames,
) -> Result<Dependency> {
    let satisfied =
        if let Some(satisfied) = qnames.satisfied.and_then(|s| element.node().attribute(s)) {
            bool::from_str(satisfied)?
        } else {
            true
        };
    Ok(Dependency {
        r#type: DependencyType::from_str(
            element.node().attribute(qnames.r#type.unwrap()).unwrap(),
        )?,
        value: element
            .node()
            .attribute(qnames.value.unwrap())
            .unwrap()
            .to_string(),
        satisfied,
    })
}
fn parse_dependencies<T: Ref>(
    element: &Element<T>,
    qnames: &DependencyAttQNames,
) -> Result<Vec<Dependency>> {
    if qnames.value.is_none() {
        return Ok(Vec::new());
    }
    element
        .children()
        .filter_map(|n| n.element())
        .filter(|e| e.node_name().to_ref().local_name() == NCName("dependency"))
        .map(|e| parse_dependency(&e, qnames))
        .collect()
}
fn parse_assertions<T: Ref>(
    context_init: &ContextInit<T>,
    env: &Option<Environment<T>>,
    e: &Element<T>,
) -> Vec<Assertion<T>> {
    let mut assertions = Vec::new();
    for child in e.children() {
        if let Some(e) = child.element() {
            assertions.push(parse_assertion(context_init, env, &e));
        }
    }
    assertions
}
fn parse_assertion<T: Ref>(
    context_init: &ContextInit<T>,
    env: &Option<Environment<T>>,
    e: &Element<T>,
) -> Assertion<T> {
    match e.node_name().to_ref().local_name().as_str() {
        "all-of" => Assertion::AllOf(parse_assertions(context_init, env, e)),
        "any-of" => Assertion::AnyOf(parse_assertions(context_init, env, e)),
        "assert" => Assertion::Assert(parse_expression(context_init, env, &e.string_value())),
        "assert-count" => Assertion::AssertCount(e.string_value().parse::<u64>().unwrap()),
        "assert-deep-eq" => Assertion::AssertDeepEq(
            parse_const_expression(context_init, env, &e.string_value())
                .unwrap()
                .seq,
        ),
        "assert-empty" => Assertion::AssertEmpty,
        "assert-eq" => Assertion::AssertEq({
            parse_const_expression(context_init, env, &e.string_value())
                .unwrap()
                .atoms
                .unwrap()
        }),
        "assert-false" => Assertion::AssertFalse,
        "assert-permutation" => Assertion::AssertPermutation(
            parse_const_expression(context_init, env, &e.string_value())
                .map_err(|err| {
                    eprintln!(
                        "Error parsing const expression '{}': {:?}",
                        &e.string_value(),
                        err
                    );
                    err
                })
                .unwrap()
                .seq,
        ),
        "serialization-matches" => Assertion::SerializationMatches {
            regex: e.string_value().to_string(),
            file: attribute_value(e, NCName("file")),
            flags: attribute_value(e, NCName("flags")),
        },
        "assert-serialization-error" => {
            Assertion::AssertSerializationError(e.string_value().to_string(), parse_code(e))
        }
        "not" => Assertion::Not(Box::new(
            parse_assertions(context_init, env, e).pop().unwrap(),
        )),
        "assert-string-value" => Assertion::AssertStringValue {
            value: e.string_value().to_string(),
            normalize_space: attribute_value(e, NCName("normalize-space"))
                .as_ref()
                .map(|a| bool::from_str(a).unwrap())
                .unwrap_or(false),
        },
        "assert-true" => Assertion::AssertTrue,
        "assert-type" => Assertion::AssertType(
            parse_sequence_type(env, &e.string_value(), context_init.fd()).unwrap(),
        ),
        "assert-xml" => Assertion::AssertXml {
            file: attribute_value(e, NCName("file")),
            xml: e.string_value().to_string(),
            ignore_prefixes: attribute_value(e, NCName("ignore_prefixes"))
                .as_ref()
                .map(|a| bool::from_str(a).unwrap())
                .unwrap_or(false),
        },
        "error" => Assertion::Error(parse_code(e)),
        n => panic!("unknown '{}'", n),
    }
}
fn parse_code<T: Ref>(e: &Element<T>) -> Code {
    match attribute_value(e, NCName("code")).unwrap() {
        c if c == "*" => Code::Any,
        c if c.starts_with('Q') => Code::EQName(c),
        c => Code::NCName(c.parse::<Error>().unwrap()),
    }
}
fn file<T: Ref>(element: &Element<T>, dir: &Path) -> Option<PathBuf> {
    attribute_value(element, NCName("file")).map(|file| dir.join(file).canonicalize().unwrap())
}
fn parse_schemas<T: Ref>(
    element: &Element<T>,
    dir: &Path,
    cache: &DomCache<T>,
) -> (Vec<Schema>, Option<XsdValidator>, Vec<PathBuf>) {
    let mut schema_paths = Vec::new();
    let schemas = Vec::new();
    for e in element
        .children()
        .filter_map(|n| n.element())
        .filter(|e| e.node_name().to_ref().local_name() == NCName("schema"))
    {
        let file = file(&e, dir);
        if let Some(file) = file {
            schema_paths.push(file);
        }
    }
    let validator = if !schema_paths.is_empty() {
        read_validator(cache, schema_paths.clone())
    } else {
        None
    };
    (schemas, validator, schema_paths)
}
fn parse_source<T: Ref>(
    e: &Element<T>,
    dir: &Path,
    cache: &DomCache<T>,
    schema_paths: Vec<PathBuf>,
) -> Option<Source<T>> {
    let file = file(e, dir).unwrap();
    let validation = match attribute_value(e, NCName("validation")).as_deref() {
        Some("strict") => Validation::Strict,
        Some("lax") => Validation::Lax,
        _ => Validation::Skip,
    };
    let schema_paths = if validation == Validation::Skip {
        Vec::new()
    } else {
        schema_paths
    };
    read_cached_xml(cache, &file, schema_paths).map(|dom| Source {
        file,
        dom,
        validation,
    })
}
/*
fn is_error<T: Ref>(e: Element<T>) -> bool {
    match e.local_name() {
        "assert" | "serialization-matches" => false,
        "error" => true,
        "all-of" | "any-of" => has_error(&e),
        "not" => !has_error(&e),
        lp if lp.starts_with("assert-") => false,
        lp => panic!("unknown {}", lp),
    }
}
fn has_error<T: Ref>(e: &Element<T>) -> bool {
    e.children().filter_map(|c| c.element()).any(is_error)
}
*/
fn parse_sequence_type<T: Ref>(
    env: &Option<Environment<T>>,
    sequence_type: &str,
    fd: &FunctionDefinitions<T>,
) -> Result<XQuery> {
    let xquery = format!(". instance of {}", sequence_type);
    let ns = HashMap::new();
    let ns = env.as_ref().map(|e| &e.namespaces).unwrap_or(&ns);
    let init = ParseInit {
        namespaces: ns,
        fd,
        ..Default::default()
    };
    Ok(parse(xquery.trim(), init)?)
}

fn parse_const_expression<T: Ref>(
    context_init: &ContextInit<T>,
    env: &Option<Environment<T>>,
    xpath: &str,
) -> Result<EvalResult<T>> {
    let ns = HashMap::new();
    let ns = env.as_ref().map(|e| &e.namespaces).unwrap_or(&ns);
    // temporary workaround for parsing "&"
    let xpath = if xpath == "\"&\"" { "\"&amp;\"" } else { xpath };
    let init = ParseInit {
        namespaces: ns,
        fd: context_init.fd(),
        ..Default::default()
    };
    let xquery = match parse(xpath.trim(), init) {
        Ok(o) => o,
        Err(e) => panic!("Could not parse '{}'. {}", xpath.trim(), e),
    };
    let context = GlobalContext::new(context_init, xquery);
    let mut context = Context::new(context)?;
    let seq = eval_xquery(&mut context)?;
    Ok(EvalResult {
        atoms: atomize_sequence(&seq, &context),
        seq,
    })
}
fn parse_expression<T: Ref>(
    context_init: &ContextInit<T>,
    env: &Option<Environment<T>>,
    xpath: &str,
) -> XQuery {
    let ns = HashMap::new();
    let ns = env.as_ref().map(|e| &e.namespaces).unwrap_or(&ns);
    let xpath = format!("declare variable $result external := (); {}", xpath);
    let init = ParseInit {
        namespaces: ns,
        fd: context_init.fd(),
        ..Default::default()
    };
    match parse(xpath.trim(), init) {
        Ok(expr) => expr,
        Err(e) => {
            panic!("Could not parse xpath {}: {}", xpath, e);
        }
    }
}
