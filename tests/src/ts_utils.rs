use std::{path::PathBuf, sync::Arc};

use crate::ts::ts::{
    self,
    e::{TestSet, TestSuite},
};
use anyhow::Context;
use tokio::runtime::Runtime;
use xust_xsd::{load_validator, xsd_validator::XsdValidator, Deserialize};

#[derive(Debug)]
pub struct TestGroup {
    pub set: String,
    pub dir: PathBuf,
    pub test: ts::e::TestGroup,
}

async fn get_tests(
    input_file: PathBuf,
    test_filter: Option<String>,
    ignored_tests: &[&str],
    validator: Arc<XsdValidator>,
) -> Result<Vec<TestGroup>, anyhow::Error> {
    let bytes = tokio::fs::read(&input_file).await?;
    let (_qnames, set) = TestSet::from_bytes(bytes, Some(&validator))
        .map_err(|e| anyhow::anyhow!("{}", e))
        .with_context(|| format!("Cannot read file {}", input_file.display()))?;
    let dir = input_file.parent().unwrap();
    let mut tests = Vec::new();
    for test in set
        .test_group
        .into_iter()
        .filter(|tg| {
            test_filter
                .as_ref()
                .map(|f| tg.name.contains(f))
                .unwrap_or(true)
        })
        .filter(|tg| !ignored_tests.contains(&tg.name.as_str()))
    {
        tests.push(TestGroup {
            set: set.name.clone(),
            dir: dir.to_path_buf(),
            test,
        });
    }
    Ok(tests)
}

pub async fn get_test_sets(
    input_file: PathBuf,
    test_filter: Option<String>,
    ignored_tests: &'static [&str],
) -> Result<Vec<TestGroup>, anyhow::Error> {
    let validator = load_validator(
        &[
            "xsd/xsd/misc/AnnotatedTSSchema.xsd".into(),
            "xsd/xsd/www.w3.org/XML/2008/06/xlink.xsd".into(),
            "xsd/xsd/www.w3.org/2001/xml.xsd".into(),
        ],
        None,
    )
    .map_err(|e| anyhow::anyhow!("{}", e))?;
    let bytes = tokio::fs::read(&input_file).await?;
    let (_qnames, suite) = TestSuite::from_bytes(bytes, Some(&validator))
        .map_err(|e| anyhow::anyhow!("{}", e))
        .with_context(|| format!("Cannot read file {}", input_file.display()))?;
    let validator = Arc::new(validator);
    let mut futures = Vec::new();
    for r in suite.test_set_ref {
        let validator = validator.clone();
        if let Some(href) = r.href {
            let test_filter = test_filter.clone();
            let parent = input_file.parent().unwrap().to_path_buf();
            futures.push(tokio::spawn(async move {
                get_tests(
                    parent.join(href),
                    test_filter,
                    ignored_tests,
                    validator.clone(),
                )
                .await
            }));
        }
    }
    let mut t = Vec::new();
    for r in futures::future::join_all(futures).await {
        t.extend(r??);
    }
    Ok(t)
}

pub fn runtime_with_threads(num_threads: usize) -> Result<Runtime, std::io::Error> {
    if num_threads == 1 {
        tokio::runtime::Builder::new_current_thread()
            .worker_threads(1)
            .build()
    } else if num_threads == 0 {
        tokio::runtime::Runtime::new()
    } else {
        tokio::runtime::Builder::new_multi_thread()
            .worker_threads(num_threads)
            .build()
    }
}
