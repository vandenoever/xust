/*
 * A tool to through qt3tests and try to convert the valid schemas in there
 * to rust and to compile them.
 */

use std::{error::Error, path::PathBuf};
use xust_tests::ts_utils::{get_test_sets, runtime_with_threads, TestGroup};
use xust_xsd::{load_validator, xsd2rs::xsd2rs, xsd_validator::XsdValidator};

const IGNORED_TESTS: [&str; 0] = [];

// get a usable validator: it should be listed as valid
// and it should load without error in the current xust
fn get_usable_validator(test: &TestGroup) -> Option<(Vec<PathBuf>, XsdValidator)> {
    let schema_test = test.test.schema_test.as_ref()?;
    if !schema_test.expected.iter().all(|e| e.validity == "valid") {
        return None;
    }
    let xsd_paths: Vec<_> = schema_test
        .schema_document
        .iter()
        .filter_map(|sd| sd.href.as_ref())
        .map(|sd| test.dir.join(sd))
        .collect();
    load_validator(&xsd_paths, None)
        .ok()
        .map(|v| (xsd_paths, v))
}

struct Generated {
    test: TestGroup,
    xsd_paths: Vec<PathBuf>,
    validator: XsdValidator,
    rust_code: String,
}

fn generate_code(test: TestGroup) -> Option<Generated> {
    let (xsd_paths, validator) = get_usable_validator(&test)?;
    let rust_code = xsd2rs(&xsd_paths, false).ok()?;
    Some(Generated {
        test,
        xsd_paths,
        validator,
        rust_code,
    })
}

async fn run_test_sets(
    input_file: PathBuf,
    test_filter: Option<String>,
) -> Result<(), anyhow::Error> {
    let tests = get_test_sets(input_file, test_filter, &IGNORED_TESTS).await?;
    let mut futures = Vec::new();
    for test in tests {
        futures.push(tokio::spawn(async move { generate_code(test) }));
    }
    let r = futures::future::join_all(futures).await;
    let mut tests_with_validators: Vec<Generated> = Vec::new();
    for r in r {
        if let Some(g) = r? {
            tests_with_validators.push(g);
        }
    }
    println!("tests: {}", tests_with_validators.len());
    Ok(())
}

struct Arguments {
    help: bool,
    test_filter: Option<String>,
    num_threads: usize,
    input_file: PathBuf,
}

fn parse_arguments() -> Result<Arguments, Box<dyn std::error::Error>> {
    let mut args = pico_args::Arguments::from_env();
    let help = args.contains(["-h", "--help"]);
    let test_filter = args.opt_value_from_str("--test-filter")?;
    let num_threads = args.value_from_str(["-j", "--threads"]).unwrap_or(0);
    let input_file = args.free_from_str::<PathBuf>()?;
    if !args.finish().is_empty() {
        return Err("Too many files provided.".to_string().into());
    }
    Ok(Arguments {
        help,
        test_filter,
        num_threads,
        input_file,
    })
}

fn print_help() {
    println!(
        "Usage: {} [OPTION]... xmlschema2006-11-06/suite.xml",
        std::env::args().next().unwrap()
    );
    println!("  -h, --help");
    println!("      --test-filter FILTER");
    println!("  -j, --threads N");
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = parse_arguments().map_err(|e| {
        print_help();
        e
    })?;
    if args.help {
        print_help();
        return Ok(());
    }
    let rt = runtime_with_threads(args.num_threads)?;
    rt.block_on(async {
        run_test_sets(args.input_file, args.test_filter).await?;
        Ok(())
    })
}
