use std::error::Error;
use std::io::Write;
use xust_xml::read::parse_xml_from_file;
use xust_xml::write::XmlOutputParameters;
use xust_xsd::atomic::Atomic;

fn main() -> Result<(), Box<dyn Error>> {
    for a in std::env::args().skip(1) {
        let path = std::path::PathBuf::from(a);
        let tree = parse_xml_from_file::<Atomic>(&path, None, None)?;
        let output_config = XmlOutputParameters::default();
        let xml = xust_xml::write::tree_to_string(&tree, output_config)?;
        std::io::stdout().write_all(xml.as_bytes())?;
    }
    Ok(())
}
