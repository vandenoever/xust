#![allow(clippy::upper_case_acronyms)]
use anyhow::Result;
use rayon::iter::ParallelBridge;
use rayon::prelude::ParallelIterator;
use std::path::Path;
use std::sync::mpsc::sync_channel;
use std::thread;
use xust_xml::{
    read::{default_entity_loader, normalize_xml, parse_xml, NormalizedXml, ParserConfig},
    write::XmlOutputParameters,
    XmlVersion,
};
use xust_xsd::atomic::Atomic;

// popular extensions for xml files
const EXTS: [&str; 20] = [
    "svg",
    "xml",
    "xsl",
    "ui",
    "lang",
    "rng",
    "policy",
    //"html",
    "inx",
    "kcfg",
    "operations",
    "xsd",
    "xslt",
    "sch",
    "pom",
    "srx",
    "qrc",
    "rdf",
    "xhtml",
    "junitxml",
    "fods",
];

enum Error {
    None,
    IO,
    XML,
}

#[derive(Default, Clone)]
struct Report {
    bytes: usize,
    ok: usize,
    io_error: usize,
    utf_error: usize,
    xml_error: usize,
}

impl std::iter::Sum for Report {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        let mut r = Report::default();
        for i in iter {
            r.bytes += i.bytes;
            r.ok += i.ok;
            r.io_error += i.io_error;
            r.utf_error += i.utf_error;
            r.xml_error += i.xml_error;
        }
        r
    }
}

fn parse(xml: &NormalizedXml, path: &Path) -> Result<xust_tree::tree::Tree<Atomic>> {
    let config = ParserConfig {
        base_url: Some(std::path::PathBuf::from(path)),
        ..Default::default()
    };
    let tree = parse_xml::<Atomic>(xml, Some(&config), None)?;
    let output_config = XmlOutputParameters::default();
    let s = xust_xml::write::tree_to_string(&tree, output_config)?;
    let s = normalize_xml(s);
    Ok(parse_xml(&s, Some(&config), None)?)
}

fn main() {
    // read all xml files that are supplied as arguments
    let (tx, rx) = sync_channel(1024);
    let child = thread::spawn(move || {
        for a in std::env::args().skip(1) {
            for path in walkdir::WalkDir::new(a)
                .into_iter()
                .filter_map(|p| p.ok())
                .filter(|p| {
                    p.file_type().is_file()
                        && p.metadata()
                            .map(|m| m.len() < 1_000_000_000)
                            .unwrap_or(false)
                        && p.path()
                            .extension()
                            .map(|e| EXTS.iter().any(|ext| e == *ext))
                            .unwrap_or(false)
                })
            {
                tx.send(path).unwrap();
            }
        }
    });
    let r = rx
        .into_iter()
        .par_bridge()
        .map(|p| {
            let xml = match default_entity_loader(None, p.path(), XmlVersion::Xml10) {
                Ok(xml) => xml.text,
                Err(_e) => {
                    return (0, Error::IO);
                }
            };
            match parse(&xml, p.path()) {
                Ok(_tree) => (xml.len(), Error::None),
                Err(e) => {
                    println!("{}: {}", p.path().display(), e);
                    (xml.len(), Error::XML)
                    /*
                    println!(
                        "{}\t{}\t{}\t{}\t{}\t{}",
                        n,
                        n_utf_err,
                        n_xml_err,
                        sum_size / 1_000_000,
                        p.path().display(),
                        e
                    );
                    */
                }
            }
        })
        .fold_with(Report::default(), |mut r, v| {
            r.bytes += v.0;
            match v.1 {
                Error::None => r.ok += 1,
                Error::IO => r.io_error += 1,
                Error::XML => r.xml_error += 1,
            }
            r
        })
        .sum::<Report>();
    println!(
        "bytes: {}, documents: {}, ok: {}, io error: {}, utf error: {}, xml error: {}",
        r.bytes,
        r.ok + r.io_error + r.utf_error + r.xml_error,
        r.ok,
        r.io_error,
        r.utf_error,
        r.xml_error
    );
    child.join().expect("oops! the child thread panicked");
}
