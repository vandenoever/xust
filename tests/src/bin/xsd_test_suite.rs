use anyhow::Context;
use std::collections::BTreeMap;
use std::error::Error;
use std::path::{Path, PathBuf};
use xust_tests::ts_utils::runtime_with_threads;
use xust_tests::{
    junit,
    ts::ts,
    ts_utils::{get_test_sets, TestGroup},
};
use xust_tree::{
    qnames::{NCName, Namespace, Prefix, QNameCollector},
    tree::builder::DocumentBuilder,
    typed_value::{XS_UNTYPED, XS_UNTYPED_ATOMIC},
};
use xust_xml::{
    read::{decode_bytes, NormalizedXml},
    write::{self, XmlOutputParameters},
};
use xust_xsd::{atomic::Atomic, load_validator};

enum TestOutcome {
    Valid,
    Invalid,
    // NotKnown,
    // RuntimeSchemaError,
}

impl From<TestOutcome> for String {
    fn from(val: TestOutcome) -> String {
        match val {
            TestOutcome::Valid => "valid",
            TestOutcome::Invalid => "invalid",
            // TestOutcome::NotKnown => "notKnown",
            // TestOutcome::RuntimeSchemaError => "runtime-schema-error",
        }
        .into()
    }
}

struct Arguments {
    help: bool,
    junit_output: Option<PathBuf>,
    output: Option<PathBuf>,
    test_filter: Option<String>,
    num_threads: usize,
    input_file: PathBuf,
}

fn parse_arguments() -> Result<Arguments, Box<dyn std::error::Error>> {
    let mut args = pico_args::Arguments::from_env();
    let help = args.contains(["-h", "--help"]);
    let junit_output = args.opt_value_from_str("--junit-output")?;
    let output = args.opt_value_from_str("--output")?;
    let test_filter = args.opt_value_from_str("--test-filter")?;
    let num_threads = args.value_from_str(["-j", "--threads"]).unwrap_or(0);
    let input_file = args.free_from_str::<PathBuf>()?;
    if !args.finish().is_empty() {
        return Err("Too many files provided.".to_string().into());
    }
    Ok(Arguments {
        help,
        junit_output,
        output,
        test_filter,
        num_threads,
        input_file,
    })
}

fn print_help() {
    println!(
        "Usage: {} [OPTION]... xmlschema2006-11-06/suite.xml",
        std::env::args().next().unwrap()
    );
    println!("  -h, --help");
    println!("      --output OUTPUT");
    println!("      --junit-output FILE");
    println!("      --test-filter FILTER");
    println!("  -j, --threads N");
}

async fn read_bytes(path: &Path) -> Result<NormalizedXml, anyhow::Error> {
    match tokio::fs::read(&path).await {
        Ok(xml) => Ok(decode_bytes(xml)?.1),
        Err(e) => Err(e).with_context(|| format!("Could not read {}", path.display())),
    }
}

async fn run_test(test: TestGroup) -> Vec<TestResult> {
    let mut results = Vec::new();
    let dir = &test.dir;
    let validator = if let Some(schema_test) = test.test.schema_test {
        let xsd_paths: Vec<_> = schema_test
            .schema_document
            .iter()
            .filter_map(|sd| sd.href.as_ref())
            .map(|sd| dir.join(sd))
            .collect();
        let mut result = TestResult {
            result: ts::e::TestResult {
                group: test.test.name.clone(),
                normalized_load: None,
                set: test.set.clone(),
                test: schema_test.name,
                annotation: Default::default(),
                validity: TestOutcome::Valid.into(),
            },
            pass: true,
        };
        let validator = if let Ok(validator) = load_validator(&xsd_paths, None) {
            result.pass = schema_test.expected.iter().all(|e| e.validity == "valid");
            results.push(result);
            validator
        } else {
            result.result.validity = TestOutcome::Invalid.into();
            result.pass = schema_test.expected.iter().all(|e| e.validity == "invalid");
            results.push(result);
            return results;
        };
        validator
    } else {
        return results;
        // TODO load_validator(&[]).unwrap()
    };
    for instance in &test.test.instance_test {
        if let Some(href) = &instance.instance_document.href {
            let path = test.dir.join(href);
            let xml = read_bytes(&path).await.unwrap();
            let validity = match validator.validate_to_tree(&xml, None) {
                Ok(_) => "valid",
                Err(_e) => {
                    // eprintln!("{}", e);
                    "invalid"
                }
            };
            results.push(TestResult {
                result: ts::e::TestResult {
                    group: test.test.name.clone(),
                    normalized_load: None,
                    set: test.set.clone(),
                    test: instance.name.clone(),
                    annotation: Default::default(),
                    validity: validity.into(),
                },
                pass: instance.expected.iter().all(|e| e.validity == validity),
            });
        } else {
            eprintln!("Missing href for instance document in {}.", test.test.name);
        }
    }
    results
}

struct TestResult {
    result: ts::e::TestResult,
    pass: bool,
}

const IGNORED_TESTS: [&str; 2] = ["elemZ031", "particlesZ033_a"];

async fn run_test_sets(
    input_file: PathBuf,
    test_filter: Option<String>,
) -> Result<Vec<TestResult>, anyhow::Error> {
    let tests = get_test_sets(input_file, test_filter, &IGNORED_TESTS).await?;
    let mut futures = Vec::new();
    for test in tests {
        futures.push(tokio::spawn(async move { run_test(test).await }));
    }
    let r = futures::future::join_all(futures).await;
    let mut results = Vec::new();
    for r in r.into_iter().flat_map(|t| t.into_iter()) {
        for result in r {
            results.push(result);
        }
    }
    Ok(results)
}

fn results_to_junit(results: Vec<TestResult>) -> junit::TestSuites {
    let suite_name = "XML Schema Test Suite";
    // set name becomes suite name
    // group name + test name becomes testcase name
    let mut suites: BTreeMap<String, Vec<_>> = BTreeMap::new();
    let tests = results.len();
    for r in results {
        let suite = suites.entry(r.result.set.clone()).or_default();
        suite.push(r);
    }
    let mut test_suites = Vec::new();
    for (name, mut suite) in suites {
        suite.sort_by(|a, b| {
            let cmp = a.result.group.cmp(&b.result.group);
            if cmp == std::cmp::Ordering::Equal {
                a.result.test.cmp(&b.result.test)
            } else {
                cmp
            }
        });
        let test_cases = suite
            .into_iter()
            .map(|s| {
                let failure = if s.pass {
                    None
                } else {
                    Some(junit::Failure {
                        message: String::new(),
                        text: String::new(),
                        r#type: s.result.validity,
                    })
                };
                let name = format!("{}-{}", s.result.group, s.result.test);
                junit::TestCase {
                    name,
                    failure,
                    time: None,
                }
            })
            .collect();
        test_suites.push(junit::TestSuite {
            name,
            tests: None,
            test_cases,
            failures: None,
            time: None,
        });
    }
    junit::TestSuites {
        failures: None,
        name: suite_name.to_string(),
        test_suites,
        tests: Some(tests),
        time: None,
    }
}

fn test_results_to_xml(test_result: &[TestResult]) -> Result<String, Box<dyn Error>> {
    let mut qnames = QNameCollector::default();
    qnames.start_namespace_context();
    let p = Prefix("ts");
    let ns = Namespace("http://www.w3.org/XML/2004/xml-schema-test-suite/");
    let namespaces = [qnames.define_namespace(p, ns)?];
    let e_test_suite_results = qnames.add_qname(p, ns, NCName("testSuiteResults"));
    let e_test_result = qnames.add_qname(p, ns, NCName("testResult"));
    let a_suite = qnames.add_local_qname(NCName("suite"));
    let a_processor = qnames.add_local_qname(NCName("processor"));
    let a_submit_date = qnames.add_local_qname(NCName("submitDate"));
    let a_group = qnames.add_local_qname(NCName("group"));
    let a_set = qnames.add_local_qname(NCName("set"));
    let a_validity = qnames.add_local_qname(NCName("validity"));
    let a_test = qnames.add_local_qname(NCName("test"));
    let mut builder = DocumentBuilder::<Atomic>::new();
    let mut eb = builder.add_element(e_test_suite_results, XS_UNTYPED, &namespaces);
    eb.add_attribute_from_qname(a_suite, XS_UNTYPED_ATOMIC, "TODO");
    eb.add_attribute_from_qname(a_processor, XS_UNTYPED_ATOMIC, "");
    eb.add_attribute_from_qname(a_submit_date, XS_UNTYPED_ATOMIC, "2000-01-01");
    let mut eb = eb.add_children();
    for r in test_result {
        eb.push_str("\n\t");
        let mut eb = eb.add_element(e_test_result, XS_UNTYPED, &[]);
        eb.add_attribute_from_qname(a_group, XS_UNTYPED_ATOMIC, &r.result.group);
        eb.add_attribute_from_qname(a_test, XS_UNTYPED_ATOMIC, &r.result.test);
        eb.add_attribute_from_qname(a_set, XS_UNTYPED_ATOMIC, &r.result.set);
        eb.add_attribute_from_qname(a_validity, XS_UNTYPED_ATOMIC, &r.result.validity);
    }
    eb.push_str("\n");
    drop(eb);
    let tree = builder.build(qnames.collect());
    let output_config = XmlOutputParameters::default();
    Ok(write::tree_to_string(&tree, output_config)?)
}

// Print the number of failing tests per set.
// The output is sorted by decreasing number of failing tests.
fn print_summary_by_set(results: &Vec<TestResult>) {
    struct Count {
        total: u16,
        failed: u16,
    }
    let mut by_set: BTreeMap<&str, Count> = BTreeMap::new();
    for r in results {
        let set = by_set.entry(&r.result.set).or_insert(Count {
            total: 0,
            failed: 0,
        });
        set.total += 1;
        if !r.pass {
            set.failed += 1;
        }
    }
    let mut set_names: Vec<_> = by_set.keys().collect();
    set_names.sort_unstable_by_key(|a| by_set.get(*a).map(|a| -(a.failed as i16)).unwrap_or(0));

    for s in &set_names {
        if let Some(set) = by_set.get(*s) {
            println!("{}: {}/{}", s, set.failed, set.total);
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = parse_arguments().map_err(|e| {
        print_help();
        e
    })?;
    if args.help {
        print_help();
        return Ok(());
    }
    let rt = runtime_with_threads(args.num_threads)?;
    rt.block_on(async {
        let results = run_test_sets(args.input_file, args.test_filter).await?;
        let pass_count = results.iter().filter(|r| r.pass).count();
        print_summary_by_set(&results);
        println!(
            "Ran {} tests. {} passed. {} failed.",
            results.len(),
            pass_count,
            results.len() - pass_count
        );
        if let Some(output) = &args.output {
            std::fs::write(output, test_results_to_xml(&results)?)?;
        }
        if let Some(junit_output) = &args.junit_output {
            let test_suite_results = results_to_junit(results);
            std::fs::write(
                junit_output,
                junit::test_suites_to_xml(&test_suite_results)?,
            )?;
        }
        Ok(())
    })
}
