use std::path::PathBuf;
use std::sync::Arc;
use xust_eval::{
    eval::{
        atomize_sequence,
        context::{default_tree_context_init, Context, ContextInit, GlobalContext},
        eval_xquery,
    },
    r#fn::function_definitions,
    xdm::Item,
    Ref,
};
use xust_grammar::{parse, ParseInit};
use xust_tests::{
    assert::{check_assertion, EvalResult},
    junit,
    parse_catalog::{default_dom_cache, parse_catalog},
    tests_model::{DependencyType, TestCase},
};
use xust_tree::{node::Node, qnames::QNameCollector};
use xust_xsd::xpath_error as error;

struct Arguments {
    help: bool,
    junit_output: Option<PathBuf>,
    no_features: bool,
    print_console: bool,
    sort_by_xquery_length: bool,
    stop_on_error: Option<usize>,
    test_filter: Option<String>,
    catalogue_path: PathBuf,
}

fn parse_arguments() -> Result<Arguments, Box<dyn std::error::Error>> {
    let mut args = pico_args::Arguments::from_env();
    let help = args.contains(["-h", "--help"]);
    let junit_output = args.opt_value_from_str("--junit-output")?;
    let no_features = args.contains("--no-features");
    let print_console = args.contains("--print-console");
    let sort_by_xquery_length = args.contains("--sort-by-xquery-length");
    let stop_on_error = args.opt_value_from_str("--stop-on-error")?;
    let test_filter = args.opt_value_from_str("--test-filter")?;
    let catalogue_path = args.free_from_str()?;
    if !args.finish().is_empty() {
        return Err("Too many files provided.".to_string().into());
    }
    Ok(Arguments {
        help,
        junit_output,
        no_features,
        print_console,
        sort_by_xquery_length,
        stop_on_error,
        test_filter,
        catalogue_path,
    })
}

fn print_help() {
    println!(
        "Usage: {} [OPTION]... CATALOG",
        std::env::args().next().unwrap()
    );
    println!("  -h, --help");
    println!("      --junit-output FILE");
    println!("      --print-console");
    println!("      --sort-by-xquery-length");
    println!("      --stop-on-error N");
    println!("      --no-features");
    println!("      --test-filter");
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = parse_arguments().map_err(|e| {
        print_help();
        e
    })?;
    if args.help {
        print_help();
        return Ok(());
    }

    let context_init =
        default_tree_context_init(QNameCollector::default().collect(), function_definitions());
    let dom_cache = default_dom_cache();
    let ignored_tests = vec![
        // out of range for this code
        "K2-Literals-4",
        "K2-Literals-5",
        "K2-Literals-6",
        "RangeExpr-409",
        "RangeExpr-409a",
        "RangeExpr-409b",
        "RangeExpr-409c",
        "RangeExpr-409d",
        "RangeExpr-409e",
        "RangeExpr-411",
        "RangeExpr-411a",
        "RangeExpr-411b",
        "RangeExpr-411c",
        "RangeExpr-411d",
        "RangeExpr-411e",
        "RangeExpr-411f",
    ];
    let ignored_tests = ignored_tests.into_iter().collect();
    let catalog = parse_catalog(
        &context_init,
        dom_cache,
        &args.catalogue_path,
        args.test_filter.as_deref(),
        &ignored_tests,
    )?;
    let mut tests: Vec<(String, _)> = catalog
        .test_sets
        .into_iter()
        .filter(|(_, ts)| {
            !args.no_features
                || !ts
                    .dependencies
                    .iter()
                    .any(|d| d.r#type == DependencyType::Feature)
        })
        .flat_map(|(name, ts)| ts.test_cases.into_values().map(move |v| (name.clone(), v)))
        .filter(|tc| !tc.1.test.is_empty())
        .filter(|tc| !ignored_tests.contains(&tc.1.name.as_str()))
        .filter(|(_, tc)| {
            !args.no_features
                || !tc
                    .dependencies
                    .iter()
                    .any(|d| d.r#type == DependencyType::Feature)
        })
        .filter(|(_, t)| {
            if let Some(filter) = &args.test_filter {
                t.name.contains(filter)
            } else {
                true
            }
        })
        // ignore all construction mode tests for now
        .filter(|(_, t)| !t.name.contains("constrmod"))
        .collect();
    if args.sort_by_xquery_length {
        tests.sort_by(|a, b| a.1.test.len().cmp(&b.1.test.len()));
    }
    let eval = false;
    let mut test_suite_results = junit::TestSuites {
        failures: None,
        name: "FOTS".to_string(),
        test_suites: Vec::new(),
        tests: None,
        time: None,
    };
    let mut total = 0;
    let mut error_count = 0;
    for (n, (ts, t)) in tests.iter().enumerate() {
        total += 1;
        if args.print_console {
            if eval {
                println!(
                    "{} {:?} {}: '{}' {:?}",
                    n, t.specs, t.name, t.test, t.result
                );
            } else {
                println!("{} {:?} {}: '{}'", n, t.specs, t.name, t.test);
            }
        }
        let r = run_test(&context_init, t);
        let r = evaluate_result(&context_init, r, t, args.print_console);
        if args.junit_output.is_some() {
            test_suite_results.add_test_case(
                ts,
                junit::TestCase {
                    name: t.name.clone(),
                    failure: if r.passed {
                        None
                    } else {
                        Some(junit::Failure {
                            message: String::new(),
                            text: String::new(),
                            r#type: String::new(),
                        })
                    },
                    time: None,
                },
            );
        }
        if !r.passed {
            error_count += 1;
            if Some(error_count) == args.stop_on_error {
                break;
            }
        }
    }
    if let Some(junit_output) = &args.junit_output {
        std::fs::write(
            junit_output,
            junit::test_suites_to_xml(&test_suite_results)?,
        )?;
    }
    println!(
        "Ran {} tests. {} passed. {} failed.",
        total,
        total - error_count,
        error_count
    );
    Ok(())
}

enum TestError {
    GrammarError(error::Error),
}

impl From<error::Error> for TestError {
    fn from(e: error::Error) -> Self {
        TestError::GrammarError(e)
    }
}

struct TestEvaluation {
    passed: bool,
}

fn run_test<T: Ref>(
    context_init: &ContextInit<T>,
    test: &TestCase<T>,
) -> Result<EvalResult<T>, TestError> {
    let query = test.test.trim();
    let module = test.module.clone();
    let init = ParseInit {
        fd: context_init.fd(),
        reader: Arc::new(move |ns, _, _| {
            if let Some(m) = module.iter().find(|m| m.uri == ns) {
                if let Ok(content) = std::fs::read_to_string(&m.file) {
                    return Ok((content, None));
                }
            }
            Err(error::Error::xqst(59))
        }),
        ..Default::default()
    };
    let xquery = parse(query, init)?;
    let source = test.environment.as_ref().and_then(|e| e.source.as_ref());
    let node = source.map(|s| Node::root(s.dom.clone()));
    let mut context = GlobalContext::new(context_init, xquery);
    if let Some(env) = &test.environment {
        for k in &env.param {
            let c = GlobalContext::new(context_init, k.1.clone());
            let mut c = Context::new(c)?;
            let s = eval_xquery(&mut c)?;
            context.set_var(k.0.clone(), s);
        }
        if let Some(uri) = env.static_base_uri.as_ref() {
            context.module.base_uri = Some(uri.clone());
        }
    }
    let mut context = Context::new(context)?;
    let context_item = node.map(|node| Item::Node(node));
    if let Some(context_item) = &context_item {
        context.set_only_item(context_item);
    }
    let seq = eval_xquery(&mut context)?;
    Ok(EvalResult {
        atoms: atomize_sequence(&seq, &context),
        seq,
    })
}

fn evaluate_result<T: Ref>(
    context_init: &ContextInit<T>,
    result: Result<EvalResult<T>, TestError>,
    test_case: &TestCase<T>,
    print_console: bool,
) -> TestEvaluation {
    let mut passed = true;
    let result = match result {
        Ok(o) => Ok(o),
        Err(TestError::GrammarError(e)) => Err(e),
    };
    for a in test_case.result.assertions.iter() {
        match check_assertion(context_init, a, &result) {
            Ok(true) => {}
            Ok(false) => {
                passed = false;
                if print_console {
                    let s = format!("{:?}", result)
                        .chars()
                        .take(190)
                        .collect::<String>();
                    eprintln!("Evaluation of assertion failed. {:?} {}", passed, s);
                }
            }
            Err(e) => {
                passed = false;
                if print_console {
                    eprintln!(
                        "Evaluation of error in assertion failed. {:?} {:?} {:?}",
                        a, result, e
                    );
                }
            }
        }
    }
    if !passed && print_console {
        let s = format!("{:?}", result)
            .chars()
            .take(190)
            .collect::<String>();
        println!(
            "Assertion {:?} failed with {}",
            test_case.result.assertions, s
        );
    }
    TestEvaluation { passed }
}
