#![allow(clippy::upper_case_acronyms)]
use anyhow::{anyhow, Result};
use std::collections::BTreeSet;
use std::error::Error;
use std::path::{Path, PathBuf};
use std::rc::Rc;
use std::str::FromStr;
use xust_tests::{compare::compare_nodes, junit};
use xust_tree::{
    node::{self, Node, NodeKind, Ref},
    qnames::{NCName, Namespace, QNameCollector, QNameId},
    tree::{builder::DocumentBuilder, Tree},
    typed_value::{TypeId, XS_UNTYPED, XS_UNTYPED_ATOMIC},
};
use xust_xml::{
    read::{default_entity_loader, normalize_xml, parse_xml, parse_xml_from_file, ParserConfig},
    write::{self, XmlOutputParameters},
    XmlVersion,
};

#[derive(Clone, Copy, PartialEq, Debug)]
enum TestResult {
    NotApplicable,
    Success,
    CouldNotLoad,
    CouldNotParse,
}

impl TestResult {
    fn as_str(&self) -> &str {
        match self {
            TestResult::NotApplicable => "not applicable",
            TestResult::Success => "success",
            TestResult::CouldNotLoad => "could-not-load",
            TestResult::CouldNotParse => "could-not-parse",
        }
    }
}

#[derive(PartialEq, Clone, Copy, Debug)]
enum TestType {
    Valid,
    Invalid,
    NotWF,
    Error,
}

impl TestType {
    fn as_str(&self) -> &str {
        match self {
            TestType::Valid => "valid",
            TestType::Invalid => "invalid",
            TestType::NotWF => "not-wf",
            TestType::Error => "error",
        }
    }
}

impl FromStr for TestType {
    type Err = anyhow::Error;
    fn from_str(str: &str) -> Result<TestType> {
        Ok(match str {
            "valid" => TestType::Valid,
            "invalid" => TestType::Invalid,
            "not-wf" => TestType::NotWF,
            "error" => TestType::Error,
            s => return Err(anyhow!("Unknown test type '{}'", s)),
        })
    }
}

#[derive(Debug)]
struct TestResultDetails {
    test_id: String,
    suite: String,
    test_type: TestType,
    with_validation: TestResult,
    without_validation: TestResult,
    comparison: Option<Result<(), String>>,
}

fn parse_test<T: Ref>(context: &Context, node: &Node<T>) -> Test {
    let test_id = node.attribute(context.qnames.r#id).unwrap().to_string();
    let test_type = match node.attribute(context.qnames.r#type) {
        Some(str) => {
            TestType::from_str(str).unwrap_or_else(|_| panic!("Unknown test type '{}'", str))
        }
        None => panic!("No TYPE attribute on test."),
    };
    let mut path = context.base_url.clone();
    let mut suite = String::new();
    // collect xml:base attribute values
    for e in node.ancestors() {
        if let Some(base) = e.attribute(context.qnames.xml_base) {
            path.push(base);
        }
        if e.node_name()
            .map(|e| e.to_ref().local_name() == NCName("TESTCASES"))
            == Some(true)
        {
            if let Some(profile) = e.attribute(context.qnames.profile) {
                if suite.is_empty() {
                    suite = profile.to_string();
                } else {
                    suite = format!("{}/{}", profile, suite);
                }
            }
        }
    }
    let entities = node.attribute(context.qnames.entities);
    let entities = entities.is_none() || entities != Some("none");
    let namespace = node.attribute(context.qnames.namespace);
    let namespace = namespace.is_none() || namespace != Some("no");
    let old_edition = node.attribute(context.qnames.edition);
    let old_edition = old_edition.map(|e| !e.contains('5')).unwrap_or(false);
    let is_xml11 = node
        .attribute(context.qnames.version)
        .map(|v| v == "1.1")
        .unwrap_or(false);
    let output = node.attribute(context.qnames.output).map(|o| path.join(o));
    if let Some(uri) = node.attribute(context.qnames.uri) {
        path.push(uri);
    } else {
        panic!("NO URI ON TEST {}", test_id);
    }
    let base = path.parent().unwrap().to_path_buf();
    let filename = PathBuf::from(path.file_name().unwrap());
    Test {
        test_id,
        test_type,
        suite,
        base,
        filename,
        entities,
        namespace,
        old_edition,
        output,
        is_xml11,
    }
}

struct Test {
    test_id: String,
    test_type: TestType,
    suite: String,
    base: PathBuf,
    filename: PathBuf,
    entities: bool,
    namespace: bool,
    old_edition: bool, // test refers to xml 1.0 edition < 5
    output: Option<PathBuf>,
    is_xml11: bool,
}

fn compare_output(id: &str, tree: &Tree<()>, output: &Path) -> Result<(), String> {
    let config = ParserConfig {
        // the reference files do not all validate against their DTD
        dtd_validation: false,
        ..Default::default()
    };
    let tree2 = parse_xml_from_file::<()>(output, Some(&config), None).unwrap();
    let root1 = node::Node::root(tree);
    let i1 = root1.descendant();
    let root2 = node::Node::root(&tree2);
    let mut i2 = root2.descendant();
    let mut n = 0;
    for a in i1.filter(|n| n.node_kind() != NodeKind::Comment) {
        let b = if let Some(n2) = i2.next() {
            n2
        } else {
            return Err(format!("Reference tree in {} has less nodes: {}.", id, n));
        };
        if let Err(e) = compare_nodes(&a, &b) {
            return Err(format!("In {}, {}th nodes are not equal: {}", id, n, e));
        }
        n += 1;
    }
    if let Some(b) = i2.next() {
        return Err(format!(
            "Reference tree in {} has more nodes: {} vs {}, first is {:?}.",
            id,
            n,
            n,
            b.node_kind()
        ));
    }
    Ok(())
}

fn run_test<T: Ref>(context: &Context, node: &Node<T>) -> TestResultDetails {
    let test = parse_test(context, node);
    let mut config = ParserConfig {
        base_url: Some(test.base.clone()),
        ..Default::default()
    };
    match default_entity_loader(Some(&test.base), &test.filename, XmlVersion::Xml10) {
        Err(_e) => TestResultDetails {
            test_id: test.test_id,
            test_type: test.test_type,
            with_validation: TestResult::CouldNotLoad,
            without_validation: TestResult::CouldNotLoad,
            suite: test.suite,
            comparison: None,
        },
        Ok(result) => {
            // config.encoding = result.encoding;
            config.dtd_validation = false;
            config.strip_comments = false;
            let without_validation = parse_xml::<()>(&result.text, Some(&config), None);
            let verbose = false;
            if verbose {
                if let Err(e) = &without_validation {
                    eprintln!("W error {:?}", e);
                }
            }
            config.require_dtd = true;
            config.dtd_validation = true;
            config.strip_comments = true; // reference files have no comments
            let with_validation = parse_xml(&result.text, Some(&config), None);
            let verbose = false;
            if verbose {
                if let Err(e) = &with_validation {
                    eprintln!("V error {:?}", e);
                }
            }
            let comparison = if let (Some(output), Ok(tree)) = (&test.output, &with_validation) {
                Some(compare_output(&test.test_id, tree, output))
            } else {
                None
            };
            if let Ok(tree) = &with_validation {
                config.require_dtd = false;
                config.dtd_validation = false;
                round_trip(tree, &config, &test.base.join(test.filename), test.is_xml11);
            }
            let without_validation = if test.entities || !test.namespace || test.old_edition {
                TestResult::NotApplicable
            } else if without_validation.is_err() {
                TestResult::CouldNotParse
            } else {
                TestResult::Success
            };
            let with_validation = if !test.namespace || test.old_edition {
                TestResult::NotApplicable
            } else if with_validation.is_err() {
                TestResult::CouldNotParse
            } else {
                TestResult::Success
            };
            TestResultDetails {
                test_id: test.test_id,
                test_type: test.test_type,
                without_validation,
                with_validation,
                comparison,
                suite: test.suite,
            }
        }
    }
}

fn round_trip(tree: &Tree<()>, config: &ParserConfig, file: &Path, is_xml11: bool) {
    // serialize to string and reparse
    let mut output_config = XmlOutputParameters::default();
    if is_xml11 {
        output_config.version = XmlVersion::Xml11;
    }
    let xml = crate::write::tree_to_string(tree, output_config).unwrap();
    let xml = normalize_xml(xml);
    let result: Result<Tree<()>, _> = parse_xml(&xml, Some(config), None);
    if let Err(e) = result {
        eprintln!("### {}: {}", file.display(), e);
        eprintln!("{}", xml.as_str());
    }
}

fn passed_with_validation(r#type: TestType, result: TestResult) -> bool {
    if result == TestResult::NotApplicable {
        return true;
    }
    match r#type {
        // All parsers must accept "valid" testcases.
        TestType::Valid => result == TestResult::Success,
        // This is a validating parser, so invalid tests are rejected
        TestType::Invalid => result != TestResult::Success,
        // The parser reads external entities, so it should not accept
        // not-wf tests
        TestType::NotWF => result != TestResult::Success,
        // Parsers are not required to report "errors".
        TestType::Error => true,
    }
}

fn passed_without_validation(r#type: TestType, result: TestResult) -> bool {
    if result == TestResult::NotApplicable {
        return true;
    }
    match r#type {
        // All parsers must accept "valid" testcases.
        TestType::Valid => result == TestResult::Success,
        // This is a nonvalidating parser, so invalid tests must be accepted
        TestType::Invalid => result == TestResult::Success,
        // The parser reads external entities, so it should not accept
        // not-wf tests
        TestType::NotWF => result != TestResult::Success,
        // Parsers are not required to report "errors".
        TestType::Error => true,
    }
}

fn results_to_xml(results: &[TestResultDetails]) -> Result<String> {
    let qnames = QNameCollector::default();
    let e_results = qnames.add_local_qname(NCName("results"));
    let e_result = qnames.add_local_qname(NCName("result"));
    let a_name = qnames.add_local_qname(NCName("name"));
    let a_type = qnames.add_local_qname(NCName("type"));
    let a_result_with_validation = qnames.add_local_qname(NCName("result_with_validation"));
    let a_result_without_validation = qnames.add_local_qname(NCName("result_without_validation"));
    let a_passed_with_validation = qnames.add_local_qname(NCName("passed_with_validation"));
    let a_passed_without_validation = qnames.add_local_qname(NCName("passed_without_validation"));
    let mut b = DocumentBuilder::<()>::new();
    let eb = b.add_element(e_results, XS_UNTYPED, &[]);
    let mut eb = eb.add_children();
    const UNTYPED: TypeId = XS_UNTYPED_ATOMIC;
    for r in results {
        let mut e = eb.add_element(e_result, XS_UNTYPED, &[]);
        e.add_attribute_from_qname(a_name, UNTYPED, &r.test_id);
        e.add_attribute_from_qname(a_type, UNTYPED, r.test_type.as_str());
        e.add_attribute_from_qname(
            a_result_with_validation,
            UNTYPED,
            r.with_validation.as_str(),
        );
        e.add_attribute_from_qname(
            a_result_without_validation,
            UNTYPED,
            r.without_validation.as_str(),
        );
        let passed_with_validation = passed_with_validation(r.test_type, r.with_validation);
        e.add_attribute_from_qname(
            a_passed_with_validation,
            UNTYPED,
            if passed_with_validation {
                "true"
            } else {
                "false"
            },
        );
        let passed_without_validation =
            passed_without_validation(r.test_type, r.without_validation);
        e.add_attribute_from_qname(
            a_passed_without_validation,
            UNTYPED,
            if passed_without_validation {
                "true"
            } else {
                "false"
            },
        );
        eb.push_str("\n");
    }
    drop(eb);
    let tree = b.build(qnames.collect());
    let output_config = XmlOutputParameters::default();
    Ok(write::tree_to_string(&tree, output_config)?)
}

struct QNames {
    test: QNameId,
    uri: QNameId,
    xml_base: QNameId,
    r#type: QNameId,
    r#id: QNameId,
    profile: QNameId,
    entities: QNameId,  // ENTITIES
    namespace: QNameId, // NAMESPACE
    edition: QNameId,   // EDITION
    output: QNameId,
    version: QNameId,
}

const XMLNS: Namespace = Namespace("http://www.w3.org/XML/1998/namespace");

struct Context {
    qnames: QNames,
    base_url: PathBuf,
}

impl Context {
    fn new<T: Ref>(base_url: PathBuf, document: &Node<T>) -> Context {
        Context {
            qnames: QNames {
                r#id: document.find_qname(Namespace(""), NCName("ID")).unwrap(),
                r#type: document.find_qname(Namespace(""), NCName("TYPE")).unwrap(),
                profile: document
                    .find_qname(Namespace(""), NCName("PROFILE"))
                    .unwrap(),
                test: document.find_qname(Namespace(""), NCName("TEST")).unwrap(),
                entities: document
                    .find_qname(Namespace(""), NCName("ENTITIES"))
                    .unwrap(),
                namespace: document
                    .find_qname(Namespace(""), NCName("NAMESPACE"))
                    .unwrap(),
                edition: document
                    .find_qname(Namespace(""), NCName("EDITION"))
                    .unwrap(),
                output: document
                    .find_qname(Namespace(""), NCName("OUTPUT"))
                    .unwrap(),
                uri: document.find_qname(Namespace(""), NCName("URI")).unwrap(),
                version: document
                    .find_qname(Namespace(""), NCName("VERSION"))
                    .unwrap(),
                xml_base: document.find_qname(XMLNS, NCName("base")).unwrap(),
            },
            base_url,
        }
    }
}

fn results_to_junit(suite_name: &str, results: &[TestResultDetails]) -> junit::TestSuites {
    let mut suites = BTreeSet::new();
    for r in results {
        suites.insert(r.suite.clone());
    }
    let mut test_suites = Vec::new();
    for group in &["wf", "validate", "output"] {
        for suite in &suites {
            let mut test_cases = Vec::new();
            for r in results {
                if r.suite == *suite {
                    if *group == "validate" {
                        let failure = if passed_with_validation(r.test_type, r.with_validation) {
                            None
                        } else {
                            Some(junit::Failure {
                                message: String::new(),
                                text: String::new(),
                                r#type: String::new(),
                            })
                        };
                        test_cases.push(junit::TestCase {
                            name: r.test_id.clone(),
                            failure,
                            time: None,
                        });
                    } else if *group == "wf" {
                        let failure =
                            if passed_without_validation(r.test_type, r.without_validation) {
                                None
                            } else {
                                Some(junit::Failure {
                                    message: String::new(),
                                    text: String::new(),
                                    r#type: String::new(),
                                })
                            };
                        test_cases.push(junit::TestCase {
                            name: r.test_id.clone(),
                            failure,
                            time: None,
                        });
                    } else if *group == "output" {
                        if let Some(comparison) = &r.comparison {
                            let failure = if let Err(e) = comparison {
                                Some(junit::Failure {
                                    message: String::new(),
                                    text: e.clone(),
                                    r#type: String::new(),
                                })
                            } else {
                                None
                            };
                            test_cases.push(junit::TestCase {
                                name: r.test_id.clone(),
                                failure,
                                time: None,
                            });
                        }
                    }
                }
            }
            if !test_cases.is_empty() {
                test_suites.push(junit::TestSuite {
                    failures: None,
                    name: format!("{}::{}", group, suite),
                    test_cases,
                    tests: None,
                    time: None,
                });
            }
        }
    }
    junit::TestSuites {
        failures: None,
        name: suite_name.to_string(),
        test_suites,
        tests: Some(results.len()),
        time: None,
    }
}

struct Arguments {
    help: bool,
    test_filter: Option<String>,
    input_path: PathBuf,
}

fn parse_arguments() -> Result<Arguments, Box<dyn std::error::Error>> {
    let mut args = pico_args::Arguments::from_env();
    let help = args.contains(["-h", "--help"]);
    let test_filter = args.opt_value_from_str("--test-filter")?;
    let input_path = args.free_from_str::<PathBuf>()?;
    if !args.finish().is_empty() {
        return Err("Too many files provided.".to_string().into());
    }
    Ok(Arguments {
        help,
        test_filter,
        input_path,
    })
}

fn print_help() {
    println!(
        "Usage: {} [OPTION]... xmlconf.xml",
        std::env::args().next().unwrap()
    );
    println!("  -h, --help");
    println!("      --test-filter");
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = parse_arguments().map_err(|e| {
        print_help();
        e
    })?;
    if args.help {
        print_help();
        return Ok(());
    }
    let xml_conf_xml = args.input_path;
    let filter = args.test_filter;
    let base_url = xml_conf_xml.parent().unwrap().to_path_buf();
    let tree = parse_xml_from_file::<()>(&xml_conf_xml, None, None)?;
    let document = Node::root(Rc::new(tree));
    let context = Context::new(base_url, &document);
    let mut element = None;
    for child in document.children() {
        if child.element().is_some() {
            element = Some(child);
        }
    }
    let suite_name = element
        .unwrap()
        .attribute(context.qnames.profile)
        .unwrap()
        .to_string();
    let mut results = Vec::new();
    let id_qname = document.find_qname(Namespace(""), NCName("ID")).unwrap();
    for node in document.descendant() {
        if let Some(element) = node.element() {
            if element.has_name(context.qnames.test) {
                let matches_filter =
                    if let (Some(filter), Some(id_qname)) = (&filter, &node.attribute(id_qname)) {
                        id_qname == filter
                    } else {
                        true
                    };
                if matches_filter {
                    results.push(run_test(&context, &node));
                }
            }
        }
    }
    let mut with_validation = 0;
    let mut without_validation = 0;
    let mut equal = 0;
    let mut equal_count = 0;
    for r in &results {
        if passed_with_validation(r.test_type, r.with_validation) {
            with_validation += 1;
        } else {
            // println!("{:#?}", r);
        }
        if passed_without_validation(r.test_type, r.without_validation) {
            without_validation += 1;
        } else {
            // println!("{:#?}", r);
        }
        if let Some(comparison) = &r.comparison {
            equal_count += 1;
            if comparison.is_ok() {
                equal += 1;
            }
        }
    }
    let result_xml = results_to_xml(&results)?;
    std::fs::write("results.xml", result_xml)?;
    let junit = results_to_junit(&suite_name, &results);
    std::fs::write("junit.xml", junit::test_suites_to_xml(&junit)?)?;

    println!(
        "with validation total: {}, ok: {}, bad: {}",
        results.len(),
        with_validation,
        results.len() - with_validation
    );
    println!(
        "without validation total: {}, ok: {}, bad: {}",
        results.len(),
        without_validation,
        results.len() - without_validation
    );
    println!(
        "tree comparison total: {}, ok: {}, bad: {}",
        equal_count,
        equal,
        equal_count - equal
    );
    Ok(())
}
