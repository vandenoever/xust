use std::error::Error;
use std::path::{Path, PathBuf};
use xust_tests::compare::compare_nodes;
use xust_tree::node::{self, Ref};
use xust_xml::{
    read::{decode_bytes, parse_xml, NormalizedXml, ParserConfig},
    write::XmlOutputParameters,
};
use xust_xsd::{atomic::Atomic, load_validator};

struct Arguments {
    help: bool,
    xml_files: Vec<PathBuf>,
    xsd_files: Vec<PathBuf>,
}

fn parse_arguments() -> Result<Arguments, Box<dyn std::error::Error>> {
    let mut args = pico_args::Arguments::from_env();
    Ok(Arguments {
        help: args.contains(["-h", "--help"]),
        xsd_files: args.values_from_str(["-s", "--xsd"])?,
        xml_files: args.finish().iter().map(PathBuf::from).collect(),
    })
}

fn print_help() {
    println!(
        "\
        Usage: {} [OPTION]... XML_FILES

        -s, --xsd XSD_FILE",
        std::env::args().next().unwrap()
    );
    println!("  -h, --help");
}

fn compare_with_unvalidated<T: Ref>(
    path: &Path,
    xml: &NormalizedXml,
    tree: T,
) -> Result<(), Box<dyn Error>> {
    let root1 = node::Node::root(tree);
    let config = ParserConfig {
        base_url: Some(std::path::PathBuf::from(path)),
        ..Default::default()
    };
    let tree2 = parse_xml::<Atomic>(xml, Some(&config), None)?;
    let root2 = node::Node::root(&tree2);
    if let Err(e) = compare_nodes(&root1, &root2)
        .map_err(|e| format!("Error comparing {}: {}", path.display(), e))
    {
        std::fs::write("a", xust_xml::write::node_to_string(&root1, false, true)?)?;
        let output_config = XmlOutputParameters::default();
        std::fs::write("b", xust_xml::write::tree_to_string(&tree2, output_config)?)?;
        return Err(e.into());
    }
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = parse_arguments()?;
    if args.help {
        print_help();
        return Ok(());
    }
    stable_eyre::install()?;
    let validator = load_validator(&args.xsd_files, None)?;
    for path in args.xml_files {
        let bytes = std::fs::read(&path)?;
        let xml = decode_bytes(bytes)?.1;
        let tree = validator.validate_to_tree(&xml, None)?;
        compare_with_unvalidated(&path, &xml, &tree)?;
    }
    Ok(())
}
