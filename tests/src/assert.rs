use crate::tests_model::{Assertion, Code};
use xust_eval::{
    deep_equal::DeepEqual,
    eval::{
        context::{Context, ContextInit, GlobalContext},
        eval_xquery,
    },
    xdm::{Item, Sequence},
    Ref,
};
use xust_grammar::XQuery;
use xust_tree::{
    node::Node,
    qnames::{NCName, Namespace, Prefix},
};
use xust_xsd::{
    atomic::{eq_literal, Atomic},
    xpath_error::{Error, Result},
};

fn ok_and<T: Ref, F: Fn(&EvalResult<T>) -> Result<bool>>(
    result: &Result<EvalResult<T>>,
    f: F,
) -> Result<bool> {
    if let Ok(r) = result {
        f(r)
    } else {
        Ok(false)
    }
}

fn ok_seq<T: Ref, F: Fn(&Sequence<T>) -> Result<bool>>(
    result: &Result<EvalResult<T>>,
    f: F,
) -> Result<bool> {
    if let Ok(r) = result {
        f(&r.seq)
    } else {
        Ok(false)
    }
}

#[derive(Debug)]
pub struct EvalResult<T: Ref> {
    pub seq: Sequence<T>,
    pub atoms: Result<Vec<Atomic>>,
}

pub fn check_assertion<T: Ref>(
    context_init: &ContextInit<T>,
    assertion: &Assertion<T>,
    r: &Result<EvalResult<T>>,
) -> Result<bool> {
    match assertion {
        Assertion::AnyOf(assertions) => {
            let a = assertions
                .iter()
                .map(|a| check_assertion(context_init, a, r))
                .collect::<Result<Vec<_>>>()?;
            Ok(a.iter().any(|a| *a))
        }
        Assertion::AllOf(assertions) => {
            let a: Vec<_> = assertions
                .iter()
                .map(|a| check_assertion(context_init, a, r))
                .collect::<Result<Vec<_>>>()?;
            Ok(a.iter().all(|a| *a))
        }
        Assertion::Not(assertion) => Ok(!check_assertion(context_init, assertion, r)?), // not
        Assertion::Assert(xpath) => ok_seq(r, |r| assert(context_init, xpath, r)),
        Assertion::AssertEq(reference) => ok_and(r, |r| assert_eq(reference, r)),
        Assertion::AssertCount(n) => ok_seq(r, |r| assert_count(*n, r)),
        Assertion::AssertDeepEq(sequence) => ok_seq(r, |r| assert_deep_eq(sequence, r)),
        Assertion::AssertPermutation(sequence) => ok_seq(r, |r| assert_permutation(sequence, r)),
        Assertion::AssertXml {
            xml,
            file,
            ignore_prefixes,
        } => ok_seq(r, |r| {
            assert_xml(context_init, xml, file, *ignore_prefixes, r)
        }),
        Assertion::SerializationMatches { regex, file, flags } => {
            ok_seq(r, |r| serialization_matches(regex, file, flags, r))
        }
        Assertion::AssertSerializationError(_, code) => {
            ok_seq(r, |r| assert_serialization_error(code, r))
        }
        Assertion::AssertEmpty => ok_seq(r, |r| Ok(assert_empty(r))),
        Assertion::AssertType(r#type) => ok_seq(r, |r| assert_type(context_init, r#type, r)),
        Assertion::AssertTrue => ok_seq(r, |r| Ok(assert_true(r))),
        Assertion::AssertFalse => ok_seq(r, |r| Ok(assert_false(r))),
        Assertion::AssertStringValue {
            value,
            normalize_space,
        } => ok_seq(r, |r| assert_string_value(value, *normalize_space, r)),
        Assertion::Error(code) => match r {
            Ok(_) => Ok(false),
            Err(e) => Ok(check_error(code, e)),
        },
    }
}

// return true if the error matches the code
fn check_error(code: &Code, error: &Error) -> bool {
    match code {
        Code::Any => true,
        Code::NCName(e) => e == error,
        Code::EQName(_) => false,
    }
}
fn assert<T: Ref>(
    context_init: &ContextInit<T>,
    expr: &XQuery,
    result: &Sequence<T>,
) -> Result<bool> {
    let mut context = GlobalContext::new(context_init, expr.clone());
    let qname = context_init
        .qnames
        .qname(Prefix(""), Namespace(""), NCName("result"));
    context.set_var(qname, result.clone());
    let mut context = Context::new(context)?;
    let r = eval_xquery(&mut context)?.is_true();
    Ok(r)
}
fn assert_eq<T: Ref>(reference: &[Atomic], r: &EvalResult<T>) -> Result<bool> {
    let atoms = r.atoms.as_ref().map_err(|e| e.clone())?;
    if reference.len() != atoms.len() {
        return Ok(false);
    }
    for (a, b) in reference.iter().zip(atoms) {
        if !eq_literal(a, b)? {
            return Ok(false);
        }
    }
    Ok(true)
}
fn assert_count<T: Ref>(n: u64, r: &Sequence<T>) -> Result<bool> {
    Ok(r.len() == n as usize)
}
fn assert_deep_eq<T: Ref>(seq: &Sequence<T>, r: &Sequence<T>) -> Result<bool> {
    seq.deep_equal(r, "")
}
fn assert_permutation<T: Ref>(seq: &Sequence<T>, r: &Sequence<T>) -> Result<bool> {
    if seq.len() != r.len() {
        return Ok(false);
    }
    let mut left = r.clone().into_vec();
    for i1 in seq {
        let mut position = None;
        for (pos, i) in left.iter().enumerate() {
            if i.deep_equal(i1, "")? {
                position = Some(pos);
                break;
            }
        }
        if let Some(pos) = position {
            left.swap_remove(pos);
        }
    }
    Ok(left.is_empty())
}
fn node_to_xml<T: Ref>(
    node: &Node<T>,
    s: &mut String,
    normalize_empty_elements: bool,
) -> Result<()> {
    s.push_str(
        &xust_xml::write::node_to_string(node, true, normalize_empty_elements)
            .map_err(|e| Error::other(&e.to_string()))?,
    );
    Ok(())
}
fn item_to_xml<T: Ref>(
    item: &Item<T>,
    s: &mut String,
    normalize_empty_elements: bool,
) -> Result<bool> {
    if let Item::Node(node) = item {
        node_to_xml(node, s, normalize_empty_elements)?;
    } else {
        return Ok(false);
    }
    Ok(true)
}
fn items_to_xml<T: Ref>(
    seq: &Sequence<T>,
    normalize_empty_elements: bool,
) -> Result<Option<String>> {
    let mut result_xml = String::new();
    for i in seq {
        if !item_to_xml(i, &mut result_xml, normalize_empty_elements)? {
            return Ok(None);
        }
    }
    Ok(Some(result_xml))
}
fn parse_xml_fragment<T: Ref>(context_init: &ContextInit<T>, xml: &str) -> Result<Sequence<T>> {
    let xml = format!("<w>{}</w>", xml);
    let tree = context_init.parse_xml(xml).unwrap();
    Ok(Sequence::many(
        Node::root(tree)
            .first_child()
            .unwrap()
            .children()
            .map(Item::Node)
            .collect(),
    ))
}
fn assert_xml<T: Ref>(
    context_init: &ContextInit<T>,
    xml: &str,
    _file: &Option<String>,
    _ignore_prefixes: bool,
    r: &Sequence<T>,
) -> Result<bool> {
    let normalize_empty_elements = true;
    let result_xml = match items_to_xml(r, normalize_empty_elements)? {
        Some(r) => r,
        None => return Ok(false),
    };
    if xml != result_xml {
        // try again with different serialization for empty elements
        let normalize_empty_elements = !normalize_empty_elements;
        let result_xml = match items_to_xml(r, normalize_empty_elements)? {
            Some(r) => r,
            None => return Ok(false),
        };
        if xml != result_xml {
            let reference: Sequence<T> = parse_xml_fragment(context_init, xml)?;
            let equal = r.deep_equal(&reference, "")?;
            if !equal {
                //                eprintln!("{} != {}", xml, result_xml);
            }
            return Ok(equal);
        }
    }
    Ok(true)
}
fn serialization_matches<T: Ref>(
    regex: &str,
    _file: &Option<String>,
    _flags: &Option<String>,
    r: &Sequence<T>,
) -> Result<bool> {
    let result_xml = match items_to_xml(r, true)? {
        Some(r) => r,
        None => return Ok(false),
    };
    if regex == result_xml {
        return Ok(true);
    }
    Err(Error::unimplemented())
}
fn assert_serialization_error<T: Ref>(_code: &Code, _r: &Sequence<T>) -> Result<bool> {
    Err(Error::unimplemented())
}
fn assert_empty<T: Ref>(r: &Sequence<T>) -> bool {
    r.is_empty()
}
fn assert_type<T: Ref>(
    context_init: &ContextInit<T>,
    seq_type: &XQuery,
    result: &Sequence<T>,
) -> Result<bool> {
    let context = GlobalContext::new(context_init, seq_type.clone());
    let mut context = Context::new(context)?;
    if !result.is_empty() {
        context.set_only_item(result.get(0));
    }
    let r = eval_xquery(&mut context)?;
    Ok(r.is_true())
}
fn assert_true<T: Ref>(r: &Sequence<T>) -> bool {
    r.is_true()
}
fn assert_false<T: Ref>(r: &Sequence<T>) -> bool {
    r.is_false()
}
fn assert_string_value<T: Ref>(
    value: &str,
    normalize_space: bool,
    r: &Sequence<T>,
) -> Result<bool> {
    use std::fmt::Write;
    if normalize_space {
        return Err(Error::unimplemented());
    }
    let mut s = String::new();
    let mut first = true;
    for i in r {
        if first {
            first = false;
        } else {
            write!(&mut s, " ").unwrap();
        }
        write!(&mut s, "{}", i).unwrap();
    }
    Ok(value == s)
}
