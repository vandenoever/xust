#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
xust_xsd::use_xsd!();
use xust_tree::qnames::{NCName, Namespace, Prefix, QNameCollection};

type P = xust_xsd::parse::ParseEnv<QNames>;

pub struct QNames {
    qnames: QNameCollection,
    local: LocalQNames,
    default: DefaultQNames,
    xml: XmlQNames,
}
pub fn qnames(qnames: QNameCollection) -> QNames {
    QNames {
        local: LocalQNames::new(&qnames),
        default: DefaultQNames::new(&qnames),
        xml: XmlQNames::new(&qnames),
        qnames,
    }
}
ns!(DefaultQNames "http://www.w3.org/2010/09/qt-fots-catalog" =>
    eq_name => "EQName"
    all_of => "all-of"
    any_of => "any-of"
    assert => "assert"
    assert_count => "assert-count"
    assert_deep_eq => "assert-deep-eq"
    assert_empty => "assert-empty"
    assert_eq => "assert-eq"
    assert_false => "assert-false"
    assert_permutation => "assert-permutation"
    assert_serialization_error => "assert-serialization-error"
    assert_string_value => "assert-string-value"
    assert_true => "assert-true"
    assert_type => "assert-type"
    assert_xml => "assert-xml"
    catalog => "catalog"
    collation => "collation"
    collection => "collection"
    context_item => "context-item"
    created => "created"
    decimal_format => "decimal-format"
    dependency => "dependency"
    dependency_enum_type => "dependencyEnumType"
    description => "description"
    encoding_type => "encodingType"
    environment => "environment"
    error => "error"
    function_library => "function-library"
    link => "link"
    modified => "modified"
    module => "module"
    namespace => "namespace"
    not => "not"
    one_char => "one-char"
    param => "param"
    query => "query"
    resource => "resource"
    result => "result"
    schema => "schema"
    serialization_matches => "serialization-matches"
    source => "source"
    static_base_uri => "static-base-uri"
    test => "test"
    test_case => "test-case"
    test_set => "test-set"
    validation_enum_type => "validationEnumType"
);
ns!(XmlQNames "http://www.w3.org/XML/1998/namespace" =>
    id => "id"
);
ns!(LocalQNames "" =>
    na_n => "NaN"
    r#as => "as"
    by => "by"
    change => "change"
    code => "code"
    covers => "covers"
    covers_30 => "covers-30"
    decimal_separator => "decimal-separator"
    declared => "declared"
    r#default => "default"
    digit => "digit"
    document => "document"
    encoding => "encoding"
    exponent_separator => "exponent-separator"
    file => "file"
    flags => "flags"
    grouping_separator => "grouping-separator"
    idref => "idref"
    ignore_prefixes => "ignore-prefixes"
    infinity => "infinity"
    location => "location"
    media_type => "media-type"
    minus_sign => "minus-sign"
    name => "name"
    normalize_space => "normalize-space"
    on => "on"
    pattern_separator => "pattern-separator"
    per_mille => "per-mille"
    percent => "percent"
    prefix => "prefix"
    r#ref => "ref"
    role => "role"
    satisfied => "satisfied"
    section_number => "section-number"
    select => "select"
    source => "source"
    test_suite => "test-suite"
    r#type => "type"
    uri => "uri"
    validation => "validation"
    value => "value"
    version => "version"
    xquery_location => "xquery-location"
    xsd_version => "xsd-version"
    xslt_location => "xslt-location"
    zero_digit => "zero-digit"
);
pub mod default {
    pub mod local {
        xust_xsd::use_xsd!();
        choice!(not Not -
            AnyOf(super::ct::SequenceOfAssertionsType): r default any_of super::ct::sequence_of_assertions_type,
            AllOf(super::ct::SequenceOfAssertionsType): r default all_of super::ct::sequence_of_assertions_type,
            Not(super::e::Not): r - - super::e::not,
            Assert(super::e::Assert): r - - super::e::assert,
            AssertEq(super::e::AssertEq): r - - super::e::assert_eq,
            AssertCount(super::e::AssertCount): r - - super::e::assert_count,
            AssertDeepEq(super::e::AssertDeepEq): r - - super::e::assert_deep_eq,
            AssertPermutation(super::e::AssertPermutation): r - - super::e::assert_permutation,
            AssertXml(super::e::AssertXml): r - - super::e::assert_xml,
            SerializationMatches(super::e::SerializationMatches): r - - super::e::serialization_matches,
            AssertSerializationError(super::e::AssertSerializationError): r - - super::e::assert_serialization_error,
            AssertEmpty(Unit): r - - unit,
            AssertType(super::e::AssertType): r - - super::e::assert_type,
            AssertTrue(Unit): r - - unit,
            AssertFalse(Unit): r - - unit,
            AssertStringValue(super::e::AssertStringValue): r - - super::e::assert_string_value,
            Error(super::e::Error): r - - super::e::error
        );
        choice!(environment Environment -
            Schema(super::ct::SchemaType): r default schema super::ct::schema_type,
            Source(super::ct::SourceType): r default source super::ct::source_type,
            Resource(super::ct::ResourceType): r default resource super::ct::resource_type,
            Param(super::e::Param): r - - super::e::param,
            ContextItem(super::e::ContextItem): r - - super::e::context_item,
            DecimalFormat(super::e::DecimalFormat): r - - super::e::decimal_format,
            Namespace(super::e::Namespace): r - - super::e::namespace,
            FunctionLibrary(super::e::FunctionLibrary): r - - super::e::function_library,
            Collection(super::e::Collection): r - - super::e::collection,
            StaticBaseUri(super::e::StaticBaseUri): r - - super::e::static_base_uri,
            Collation(super::e::Collation): r - - super::e::collation
        );
        choice!(test_set2 TestSet2 -
            Description(super::e::Description): r - - super::e::description,
            Link(super::e::Link): r - - super::e::link,
            Environment(super::e::Environment): r - - super::e::environment,
            Dependency(super::e::Dependency): r - - super::e::dependency
        );
        choice!(test_case1 TestCase1 -
            Environment(super::e::Environment): r - - super::e::environment,
            Module(super::e::Module): r - - super::e::module,
            Dependency(super::e::Dependency): r - - super::e::dependency
        );
        complexType!(test_set TestSet 62 -
        attributes {
            local file file o - String,
            local name name o - String,
            xml id id o - String
        });
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
        simpleType!(one_char OneChar);
        complexType!(base_type BaseType 53 -
        attributes {
            xml id id o - String
        });
        complexType!(schema_type SchemaType 54 -
            attributes {
                local file file o - String,
                local role role o - String,
                local uri uri o - String,
                local xsd_version xsd_version o "1.0" String,
                xml id id o - String
            }

            description(super::e::Description): o - - super::e::description,
            created(super::e::Created): o - - super::e::created,
            modified(super::e::Modified): v - - super::e::modified
        );
        complexType!(source_type SourceType 55 -
            attributes {
                local file file o - String,
                local role role o - String,
                local uri uri o - String,
                local validation validation o - String,
                xml id id o - String
            }

            description(super::e::Description): o - - super::e::description,
            created(super::e::Created): o - - super::e::created,
            modified(super::e::Modified): v - - super::e::modified
        );
        complexType!(resource_type ResourceType 56 -
            attributes {
                local encoding encoding o - String,
                local file file o - String,
                local media_type media_type o - String,
                local uri uri o - String,
                xml id id o - String
            }

            description(super::e::Description): o - - super::e::description,
            created(super::e::Created): o - - super::e::created,
            modified(super::e::Modified): v - - super::e::modified
        );
        complexType!(sequence_of_assertions_type SequenceOfAssertionsType 57 -
            choice(super::local::Not): v + - super::local::not
        );
        simpleType!(eq_name EqName);
        simpleType!(encoding_type EncodingType);
        simpleType!(dependency_enum_type DependencyEnumType);
        simpleType!(validation_enum_type ValidationEnumType);
    }
    pub mod e {
        xust_xsd::use_xsd!();

        element!(default catalog Catalog 63 -
            attributes {
                local test_suite test_suite o - String,
                local version version o - String
            }
            environment(Environment): v - - environment,
            test_set(super::local::TestSet): v default test_set super::local::test_set
        );
        element!(default environment Environment 64 -
            attributes {
                local name name o - String,
                local r#ref r#ref o - String,
                xml id id o - String
            }
            choice(super::local::Environment): v + - super::local::environment
        );
        element!(default static_base_uri StaticBaseUri 65 -
                attributes {
                    local uri uri o - String
                }
        );
        element!(default collation Collation 69 -
                attributes {
                    local r#default r#default o "false" bool,
                    local uri uri o - String
                }
        );
        element!(default decimal_format DecimalFormat 70 -
                attributes {
                    local na_n na_n o - String,
                    local decimal_separator decimal_separator o - String,
                    local digit digit o - String,
                    local exponent_separator exponent_separator o - String,
                    local grouping_separator grouping_separator o - String,
                    local infinity infinity o - String,
                    local minus_sign minus_sign o - String,
                    local name name o - QName,
                    local pattern_separator pattern_separator o - String,
                    local per_mille per_mille o - String,
                    local percent percent o - String,
                    local zero_digit zero_digit o - String
                }
        );
        element!(default param Param 71 -
                attributes {
                    local r#as r#as o - String,
                    local declared declared o "false" bool,
                    local name name r - QName,
                    local select select o - String,
                    local source source o - String
                }
        );
        element!(default context_item ContextItem 72 -
                attributes {
                    local select select o - String
                }
        );
        element!(default collection Collection 73 -
            attributes {
                local uri uri o - String
            }
            source(super::ct::SourceType): v default source super::ct::source_type,
            resource(super::ct::ResourceType): v default resource super::ct::resource_type,
            query(Unit): v - - unit
        );
        element!(default function_library FunctionLibrary 74 -
                attributes {
                    local name name o - String,
                    local xquery_location xquery_location o - String,
                    local xslt_location xslt_location o - String
                }
        );
        element!(default namespace Namespace 75 -
                attributes {
                    local prefix prefix o - String,
                    local uri uri o - String
                }
        );
        element_type!(default query(Option<Unit>) unit);
        element_type!(default schema(super::ct::SchemaType) super::ct::schema_type);
        element_type!(default source(super::ct::SourceType) super::ct::source_type);
        element_type!(default resource(super::ct::ResourceType) super::ct::resource_type);
        element!(default description Description 76 -);
        element!(default test_set TestSet 79 -
            attributes {
                local covers covers ov - String,
                local covers_30 covers_30 ov - String,
                local name name o - String
            }
            choice(super::local::TestSet2): v + - super::local::test_set2,
            test_case(TestCase): v - - test_case
        );
        element!(default dependency Dependency 80 -
                attributes {
                    local satisfied satisfied o "true" bool,
                    local r#type r#type o - String,
                    local value value o - String
                }
        );
        element!(default test_case TestCase 81 -
            attributes {
                local covers covers ov - String,
                local covers_30 covers_30 ov - String,
                local name name o - String
            }
            description(Description): r - - description,
            created(Created): r - - created,
            modified(Modified): v - - modified,
            choice(super::local::TestCase1): v + - super::local::test_case1,
            test(Test): r - - test,
            result(Result): r - - result
        );
        element!(default test Test 82 -
                attributes {
                    local file file o - String
                }
        );
        element!(default created Created 83 -
                attributes {
                    local by by o - String,
                    local on on o - String
                }
        );
        element!(default modified Modified 84 -
                attributes {
                    local by by o - String,
                    local change change o - String,
                    local on on o - String
                }
        );
        element!(default module Module 85 -
                attributes {
                    local file file o - String,
                    local location location o - String,
                    local uri uri o - String
                }
        );
        element!(default result Result 86 -
            choice(super::local::Not): r + - super::local::not
        );
        element_type!(default any_of(super::ct::SequenceOfAssertionsType) super::ct::sequence_of_assertions_type);
        element_type!(default all_of(super::ct::SequenceOfAssertionsType) super::ct::sequence_of_assertions_type);
        element!(default not Not 87 -
            choice(super::local::Not): v + - super::local::not
        );
        element!(default assert Assert 76 -);
        element!(default assert_eq AssertEq 76 -);
        element!(default assert_count AssertCount 88 -);
        element!(default assert_deep_eq AssertDeepEq 76 -);
        element!(default assert_permutation AssertPermutation 76 -);
        element!(default assert_xml AssertXml 89 -
                attributes {
                    local file file o - String,
                    local ignore_prefixes ignore_prefixes o - bool
                }
        );
        element!(default serialization_matches SerializationMatches 90 -
                attributes {
                    local file file o - String,
                    local flags flags o - String
                }
        );
        element!(default assert_serialization_error AssertSerializationError 93 -
                attributes {
                    local code code o - String
                }
        );
        element_type!(default assert_empty(Option<Unit>) unit);
        element!(default assert_type AssertType 76 -);
        element_type!(default assert_true(Option<Unit>) unit);
        element_type!(default assert_false(Option<Unit>) unit);
        element!(default assert_string_value AssertStringValue 94 -
                attributes {
                    local normalize_space normalize_space o "false" bool
                }
        );
        element!(default error Error 95 -
                attributes {
                    local code code o - String
                }
        );
        element!(default link Link 97 -
                attributes {
                    local document document o - String,
                    local idref idref o - String,
                    local section_number section_number o - String,
                    local r#type r#type o - String
                }
        );
    }
}
pub mod xml {
    pub mod local {
        xust_xsd::use_xsd!();
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
    }
    pub mod e {
        xust_xsd::use_xsd!();
    }
}
