#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
xust_xsd::use_xsd!();
use xust_tree::qnames::{NCName, Namespace, Prefix, QNameCollection};

type P = xust_xsd::parse::ParseEnv<QNames>;

pub struct QNames {
    qnames: QNameCollection,
    local: LocalQNames,
    kcfg: KcfgQNames,
}
pub fn qnames(qnames: QNameCollection) -> QNames {
    QNames {
        local: LocalQNames::new(&qnames),
        kcfg: KcfgQNames::new(&qnames),
        qnames,
    }
}
ns!(KcfgQNames "http://www.kde.org/standards/kcfg/1.0" =>
    argument => "argument"
    choice => "choice"
    choices => "choices"
    code => "code"
    datatype => "datatype"
    r#default => "default"
    emit => "emit"
    entry => "entry"
    group => "group"
    include => "include"
    kcfg => "kcfg"
    kcfgfile => "kcfgfile"
    label => "label"
    max => "max"
    min => "min"
    parameter => "parameter"
    signal => "signal"
    tooltip => "tooltip"
    value => "value"
    values => "values"
    whatsthis => "whatsthis"
);
ns!(LocalQNames "" =>
    arg => "arg"
    code => "code"
    context => "context"
    hidden => "hidden"
    key => "key"
    max => "max"
    name => "name"
    param => "param"
    parent_group_name => "parentGroupName"
    prefix => "prefix"
    signal => "signal"
    state_config => "stateConfig"
    r#type => "type"
    value => "value"
);
pub mod kcfg {
    pub mod local {
        xust_xsd::use_xsd!();
        sequence!(choice1 Choice1 -);
        choice!(default: Parameter entry1 Entry1 -
            Parameter(super::ct::Parameter): o kcfg parameter super::ct::parameter,
            Label(super::ct::TranslatableString): o kcfg label super::ct::translatable_string,
            Whatsthis(super::ct::TranslatableString): o kcfg whatsthis super::ct::translatable_string,
            Tooltip(super::ct::TranslatableString): o kcfg tooltip super::ct::translatable_string,
            Choices(Choices): o kcfg choices choices,
            Code(super::ct::Code): o kcfg code super::ct::code,
            Default(Default): v kcfg r#default r#default,
            Min(Max): o kcfg min max,
            Max(Max): o kcfg max max,
            Emit(Emit): o kcfg emit emit
        );
        complexType!(values Values 57 -
            value(ct::String): v kcfg value ct::string
        );
        complexType!(argument Argument 58 -
        attributes {
            local r#type r#type r - String
        });
        complexType!(kcfgfile Kcfgfile 59 -
            attributes {
                local arg arg o - bool,
                local name name o - String,
                local state_config state_config o - bool
            }

            parameter(super::ct::Parameter): v kcfg parameter super::ct::parameter
        );
        complexType!(choice Choice 60 -
            attributes {
                local name name r - String,
                local value value o - String
            }

            all(Choice1): r + - choice1
        );
        complexType!(choices Choices 61 -
            attributes {
                local name name o - String,
                local prefix prefix o - String
            }

            choice(Choice): v kcfg choice choice
        );
        complexType!(r#default Default 62 -
        attributes {
            local code code o - bool,
            local param param o - String
        });
        complexType!(max Max 63 -
        attributes {
            local code code o - bool
        });
        complexType!(emit Emit 64 -
        attributes {
            local signal signal r - String
        });
        complexType!(entry Entry 65 -
            attributes {
                local hidden hidden o - bool,
                local key key o - String,
                local name name o - String,
                local r#type r#type o - String
            }

            choice(Entry1): v + - entry1
        );
        complexType!(group Group 66 -
            attributes {
                local name name r - String,
                local parent_group_name parent_group_name o - String
            }

            entry(Entry): v kcfg entry entry
        );
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
        simpleType!(datatype Datatype);
        complexType!(parameter Parameter 53 -
            attributes {
                local max max o - usize,
                local name name r - String,
                local r#type r#type o - String
            }

            values(super::local::Values): o kcfg values super::local::values
        );
        complexType!(code Code 54 -);
        complexType!(signal Signal 55 -
            attributes {
                local name name r - String
            }

            label(ct::String): o kcfg label ct::string,
            argument(super::local::Argument): v kcfg argument super::local::argument
        );
        complexType!(translatable_string TranslatableString 56 -
        attributes {
            local context context o - String
        });
    }
    pub mod e {
        xust_xsd::use_xsd!();

        element!(kcfg kcfg Kcfg 67 -
            include(ct::String): v kcfg include ct::string,
            kcfgfile(super::local::Kcfgfile): o kcfg kcfgfile super::local::kcfgfile,
            signal(super::ct::Signal): v kcfg signal super::ct::signal,
            group(super::local::Group): v kcfg group super::local::group
        );
    }
}
