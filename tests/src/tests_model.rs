use anyhow::Result;
use enumflags2::{bitflags, BitFlags};
use std::collections::{BTreeMap, HashMap};
use std::fmt::Debug;
use std::path::PathBuf;
use std::str::FromStr;
use url::Url;
use xust_eval::{xdm::Sequence, Ref};
use xust_grammar::XQuery;
use xust_tree::qnames::QName;
use xust_xsd::{atomic::Atomic, xpath_error::Error, xsd_validator::XsdValidator};

#[derive(Debug)]
pub struct Catalog<T: Ref> {
    pub environments: BTreeMap<String, Environment<T>>,
    pub test_sets: BTreeMap<String, TestSet<T>>,
}
impl<T: Ref> Default for Catalog<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T: Ref> Catalog<T> {
    pub fn new() -> Catalog<T> {
        Catalog {
            environments: BTreeMap::new(),
            test_sets: BTreeMap::new(),
        }
    }
}
#[derive(Clone)]
pub struct Environment<T: Ref> {
    pub name: Option<String>,
    pub validator: Option<XsdValidator>,
    pub schemas: Vec<Schema>,
    pub source: Option<Source<T>>,
    pub static_base_uri: Option<Url>,
    pub namespaces: HashMap<String, String>,
    pub param: BTreeMap<QName, XQuery>,
}

impl<T: Ref> std::fmt::Debug for Environment<T> {
    fn fmt(&self, _f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        Ok(())
    }
}

impl<T: Ref> Default for Environment<T> {
    fn default() -> Self {
        Self {
            name: None,
            schemas: Vec::new(),
            validator: None,
            source: None,
            static_base_uri: None,
            namespaces: HashMap::default(),
            param: BTreeMap::default(),
        }
    }
}

#[derive(Debug)]
pub struct TestSet<T: Ref> {
    pub name: String,
    pub test_cases: BTreeMap<String, TestCase<T>>,
    pub environments: BTreeMap<String, Environment<T>>,
    pub dependencies: Vec<Dependency>,
}

#[derive(Debug)]
pub struct TestCase<T: Ref> {
    pub name: String,
    pub test: String,
    pub result: TestCaseResult<T>,
    pub specs: BitFlags<Spec>,
    pub environment: Option<Environment<T>>,
    pub module: Vec<Module>,
    pub dependencies: Vec<Dependency>,
}

#[derive(Debug, Clone)]
pub struct Module {
    pub uri: String,
    pub location: Option<String>,
    pub file: PathBuf,
}

#[derive(Debug)]
pub struct Dependency {
    pub satisfied: bool,
    pub r#type: DependencyType,
    pub value: String,
}

#[derive(Debug, PartialEq)]
pub enum DependencyType {
    Calendar,
    CollectionStability,
    DefaultLanguage,
    DirectoryAsCollectionUri,
    Feature,
    FormatIntegerSequence,
    Language,
    Limits,
    Spec,
    SchemaAware,
    UnicodeNormalizationForm,
    UnicodeVersion,
    XmlVersion,
    XsdVersion,
}

impl FromStr for DependencyType {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<DependencyType, Self::Err> {
        Ok(match s {
            "calendar" => Self::Calendar,
            "collection-stability" => Self::CollectionStability,
            "default-language" => Self::DefaultLanguage,
            "directory-as-collection-uri" => Self::DirectoryAsCollectionUri,
            "feature" => Self::Feature,
            "format-integer-sequence" => Self::FormatIntegerSequence,
            "language" => Self::Language,
            "limits" => Self::Limits,
            "spec" => Self::Spec,
            "schemaAware" => Self::SchemaAware,
            "unicode-normalization-form" => Self::UnicodeNormalizationForm,
            "unicode-version" => Self::UnicodeVersion,
            "xml-version" => Self::XmlVersion,
            "xsd-version" => Self::XsdVersion,
            _ => return Err(anyhow::anyhow!("Invalid value for 'type': '{}'", s)),
        })
    }
}

impl<T: Ref> TestCase<T> {
    /*
    pub fn has_static_error(&self) -> bool {
        self.result.assertions.iter().any(|a| a.has_static_error())
    }
    pub fn has_wildcard_error(&self) -> bool {
        self.result
            .assertions
            .iter()
            .any(|a| a.has_wildcard_error())
    }
    pub fn has_error(&self, e: &Error) -> bool {
        self.result.assertions.iter().any(|a| a.has_error(e))
    }*/
}

#[derive(Debug)]
pub struct TestCaseResult<T: Ref> {
    pub assertions: Vec<Assertion<T>>,
}

#[derive(Debug)]
pub enum Assertion<T: Ref> {
    AnyOf(Vec<Assertion<T>>), // any-of
    AllOf(Vec<Assertion<T>>), // all-of
    Not(Box<Assertion<T>>),   // not
    Assert(XQuery),
    AssertEq(Vec<Atomic>),
    AssertCount(u64),
    AssertDeepEq(Sequence<T>),
    AssertPermutation(Sequence<T>),
    AssertXml {
        xml: String,
        file: Option<String>,
        ignore_prefixes: bool,
    },
    SerializationMatches {
        regex: String,
        file: Option<String>,
        flags: Option<String>,
    },
    AssertSerializationError(String, Code),
    AssertEmpty,
    AssertType(XQuery),
    AssertTrue,
    AssertFalse,
    AssertStringValue {
        value: String,
        normalize_space: bool,
    },
    Error(Code),
}

impl<T: Ref> Assertion<T> {
    /*
    fn has_static_error(&self) -> bool {
        match self {
            Assertion::Error(e) | Assertion::AssertSerializationError(_, e) => {
                if let Code::NCName(e) = e {
                    e.is_static()
                } else {
                    false
                }
            }
            Assertion::AnyOf(a) => a.iter().any(|a| a.has_static_error()),
            _ => false,
        }
    }
    fn has_wildcard_error(&self) -> bool {
        match self {
            Assertion::Error(e) | Assertion::AssertSerializationError(_, e) => {
                if let Code::Any = e {
                    true
                } else {
                    false
                }
            }
            Assertion::AnyOf(a) => a.iter().any(|a| a.has_wildcard_error()),
            _ => false,
        }
    }
    pub fn has_error(&self, err: &Error) -> bool {
        match self {
            Assertion::Error(e) | Assertion::AssertSerializationError(_, e) => {
                if let Code::NCName(e) = e {
                    e == err
                } else {
                    false
                }
            }
            Assertion::AnyOf(a) => a.iter().any(|a| a.has_error(err)),
            _ => false,
        }
    }*/
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Validation {
    Strict,
    Lax,
    Skip,
}

#[derive(Debug, Clone)]
pub struct Source<T: Ref> {
    pub file: PathBuf,
    pub dom: T,
    pub validation: Validation,
}

#[derive(Debug, Clone)]
pub struct Schema {
    pub file: Option<PathBuf>,
    pub uri: Option<String>,
}

#[derive(Debug)]
pub enum Code {
    NCName(Error),
    EQName(String),
    Any,
}

#[bitflags]
#[derive(Copy, Clone, Debug)]
#[repr(u32)]
pub enum Spec {
    XP20 = 0b0000_0000_0000_0001,
    XP20plus = 0b0000_0000_0000_0010,
    XP30 = 0b0000_0000_0000_0100,
    XP30plus = 0b0000_0000_0000_1000,
    XP31 = 0b0000_0000_0001_0000,
    XP31plus = 0b0000_0000_0010_0000,
    XQ10 = 0b0000_0000_0100_0000,
    XQ10plus = 0b0000_0000_1000_0000,
    XQ30 = 0b0000_0001_0000_0000,
    XQ30plus = 0b0000_0010_0000_0000,
    XQ31 = 0b0000_0100_0000_0000,
    XQ31plus = 0b0000_1000_0000_0000,
    XT30plus = 0b0001_0000_0000_0000,
}

impl Spec {
    pub fn from_string(s: &str) -> Self {
        match s {
            "XP20" => Spec::XP20,
            "XP20+" => Spec::XP20plus,
            "XP30" => Spec::XP30,
            "XP30+" => Spec::XP30plus,
            "XP31" => Spec::XP31,
            "XP31+" => Spec::XP31plus,
            "XQ10" => Spec::XQ10,
            "XQ10+" => Spec::XQ10plus,
            "XQ30" => Spec::XQ30,
            "XQ30+" => Spec::XQ30plus,
            "XQ31" => Spec::XQ31,
            "XQ31+" => Spec::XQ31plus,
            "XT30+" => Spec::XT30plus,
            unknown => panic!("Unknown spec: '{}'", unknown),
        }
    }
}
