#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
xust_xsd::use_xsd!();
use xust_tree::qnames::{NCName, Namespace, Prefix, QNameCollection};

type P = xust_xsd::parse::ParseEnv<QNames>;

pub struct QNames {
    qnames: QNameCollection,
    local: LocalQNames,
    ts: TsQNames,
    xlink: XlinkQNames,
    xml: XmlQNames,
}
pub fn qnames(qnames: QNameCollection) -> QNames {
    QNames {
        local: LocalQNames::new(&qnames),
        ts: TsQNames::new(&qnames),
        xlink: XlinkQNames::new(&qnames),
        xml: XmlQNames::new(&qnames),
        qnames,
    }
}
ns!(TsQNames "http://www.w3.org/XML/2004/xml-schema-test-suite/" =>
    xdm_filtering => "XDM-filtering"
    annotation => "annotation"
    appinfo => "appinfo"
    bug_uri => "bugURI"
    current => "current"
    documentation => "documentation"
    documentation_reference => "documentationReference"
    expected => "expected"
    expected_outcome => "expected-outcome"
    instance_document => "instanceDocument"
    instance_test => "instanceTest"
    known_token => "known-token"
    known_xsd_version => "known-xsd-version"
    prior => "prior"
    runtime_schema_error => "runtime-schema-error"
    schema_document => "schemaDocument"
    schema_test => "schemaTest"
    status => "status"
    test_outcome => "test-outcome"
    test_group => "testGroup"
    test_result => "testResult"
    test_set => "testSet"
    test_set_ref => "testSetRef"
    test_suite => "testSuite"
    test_suite_results => "testSuiteResults"
    unicode_versions => "unicode-versions"
    version_info => "version-info"
    version_token => "version-token"
    xml_substrate => "xml-substrate"
    xpath_in_cta => "xpath-in-CTA"
    xsd_1_0_editions => "xsd-1.0-editions"
);
ns!(XlinkQNames "http://www.w3.org/1999/xlink" =>
    actuate => "actuate"
    actuate_type => "actuateType"
    arcrole => "arcrole"
    arcrole_type => "arcroleType"
    from => "from"
    from_type => "fromType"
    href => "href"
    href_type => "hrefType"
    label => "label"
    label_type => "labelType"
    role => "role"
    role_type => "roleType"
    show => "show"
    show_type => "showType"
    title => "title"
    title_attr_type => "titleAttrType"
    to => "to"
    to_type => "toType"
    r#type => "type"
    type_type => "typeType"
);
ns!(XmlQNames "http://www.w3.org/XML/1998/namespace" =>
    lang => "lang"
);
ns!(LocalQNames "" =>
    bugzilla => "bugzilla"
    contributor => "contributor"
    date => "date"
    group => "group"
    name => "name"
    normalized_load => "normalizedLoad"
    processor => "processor"
    publication_permission => "publicationPermission"
    release_date => "releaseDate"
    schema_version => "schemaVersion"
    set => "set"
    source => "source"
    status => "status"
    submit_date => "submitDate"
    suite => "suite"
    test => "test"
    validity => "validity"
    version => "version"
);
pub mod ts {
    pub mod local {
        xust_xsd::use_xsd!();
        choice!(annotation Annotation -
            Appinfo(super::e::Appinfo): r - - super::e::appinfo,
            Documentation(super::e::Documentation): r - - super::e::documentation
        );
        sequence!(documentation Documentation m
            any(Unit): r - - unit
        );
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
        complexType!(status_entry StatusEntry 68 -
            attributes {
                local bugzilla bugzilla o - String,
                local date date r - String,
                local status status r - String
            }

            annotation(super::e::Annotation): v - - super::e::annotation
        );
        simpleType!(status Status);
        simpleType!(bug_uri BugUri);
        simpleType!(test_outcome TestOutcome);
        simpleType!(expected_outcome ExpectedOutcome);
        complexType!(r#ref Ref 73 -
            attributes {
                xlink href href o - String,
                xlink r#type r#type o - String
            }

            annotation(super::e::Annotation): v - - super::e::annotation
        );
        simpleType!(version_token VersionToken);
        simpleType!(known_token KnownToken);
        simpleType!(known_xsd_version KnownXsdVersion);
        simpleType!(xsd_1_0_editions Xsd10Editions);
        simpleType!(xml_substrate XmlSubstrate);
        simpleType!(unicode_versions UnicodeVersions);
        simpleType!(runtime_schema_error RuntimeSchemaError);
        simpleType!(xpath_in_cta XpathInCta);
        simpleType!(xdm_filtering XdmFiltering);
        simpleType!(version_info VersionInfo);
    }
    pub mod e {
        xust_xsd::use_xsd!();

        element!(ts test_suite TestSuite 88 -
            attributes {
                local name name r - String,
                local release_date release_date r - String,
                local schema_version schema_version r - String,
                local version version ov - String
            }
            annotation(Annotation): v - - annotation,
            test_set_ref(super::ct::Ref): v ts test_set_ref super::ct::r#ref
        );
        element_type!(ts test_set_ref(super::ct::Ref) super::ct::r#ref);
        element!(ts test_set TestSet 89 -
            attributes {
                local contributor contributor r - String,
                local name name r - String,
                local version version ov - String
            }
            annotation(Annotation): v - - annotation,
            test_group(TestGroup): v - - test_group
        );
        element!(ts test_group TestGroup 90 -
            attributes {
                local name name r - String,
                local version version ov - String
            }
            annotation(Annotation): v - - annotation,
            documentation_reference(super::ct::Ref): v ts documentation_reference super::ct::r#ref,
            schema_test(SchemaTest): o - - schema_test,
            instance_test(InstanceTest): v - - instance_test
        );
        element!(ts schema_test SchemaTest 91 -
            attributes {
                local name name r - String,
                local version version ov - String
            }
            annotation(Annotation): v - - annotation,
            schema_document(super::ct::Ref): v ts schema_document super::ct::r#ref,
            expected(Expected): v - - expected,
            current(super::ct::StatusEntry): o ts current super::ct::status_entry,
            prior(super::ct::StatusEntry): v ts prior super::ct::status_entry
        );
        element!(ts instance_test InstanceTest 92 -
            attributes {
                local name name r - String,
                local version version ov - String
            }
            annotation(Annotation): v - - annotation,
            instance_document(super::ct::Ref): r ts instance_document super::ct::r#ref,
            expected(Expected): v - - expected,
            current(super::ct::StatusEntry): o ts current super::ct::status_entry,
            prior(super::ct::StatusEntry): v ts prior super::ct::status_entry
        );
        element_type!(ts schema_document(super::ct::Ref) super::ct::r#ref);
        element_type!(ts instance_document(super::ct::Ref) super::ct::r#ref);
        element_type!(ts current(super::ct::StatusEntry) super::ct::status_entry);
        element_type!(ts prior(super::ct::StatusEntry) super::ct::status_entry);
        element!(ts expected Expected 93 -
                attributes {
                    local validity validity r - String,
                    local version version ov - String
                }
        );
        element!(ts test_suite_results TestSuiteResults 95 -
            attributes {
                local processor processor r - String,
                local publication_permission publication_permission o - String,
                local submit_date submit_date r - String,
                local suite suite r - String
            }
            annotation(Annotation): v - - annotation,
            test_result(TestResult): v - - test_result
        );
        element!(ts test_result TestResult 96 -
            attributes {
                local group group r - String,
                local normalized_load normalized_load o - String,
                local set set r - String,
                local test test r - String,
                local validity validity r - String
            }
            annotation(Annotation): v - - annotation
        );
        element_type!(ts documentation_reference(super::ct::Ref) super::ct::r#ref);
        element!(ts annotation Annotation 97 -
            choice(super::local::Annotation): v + - super::local::annotation
        );
        element!(ts appinfo Appinfo 98 m
            attributes {
                local source source o - String
            }
            sequence(super::local::Documentation): v + - super::local::documentation
        );
        element!(ts documentation Documentation 99 m
            attributes {
                local source source o - String,
                xml lang lang o - String
            }
            sequence(super::local::Documentation): v + - super::local::documentation
        );
    }
}
pub mod xlink {
    pub mod local {
        xust_xsd::use_xsd!();
        choice!(arc_model ArcModel -);
        choice!(extended_model1 ExtendedModel1 -);
        choice!(extended_model2 ExtendedModel2 -);
        choice!(extended_model3 ExtendedModel3 -);
    }
    pub mod g {
        xust_xsd::use_xsd!();
        sequence!(default simple_model SimpleModel m
            any(Unit): v - - unit
        );
        choice!(extended_model ExtendedModel -
            Choice1(super::local::ArcModel): r + - super::local::arc_model,
            Choice2(super::local::ExtendedModel1): r + - super::local::extended_model1,
            Choice3(super::local::ExtendedModel2): r + - super::local::extended_model2,
            Choice4(super::local::ExtendedModel3): r + - super::local::extended_model3
        );
        sequence!(default title_model TitleModel m
            any(Unit): v - - unit
        );
        sequence!(default resource_model ResourceModel m
            any(Unit): v - - unit
        );
        sequence!(default locator_model LocatorModel -
            choice(super::local::ArcModel): v + - super::local::arc_model
        );
        sequence!(default arc_model ArcModel -
            choice(super::local::ArcModel): v + - super::local::arc_model
        );
    }
    pub mod ct {
        xust_xsd::use_xsd!();
        simpleType!(type_type TypeType);
        simpleType!(href_type HrefType);
        simpleType!(role_type RoleType);
        simpleType!(arcrole_type ArcroleType);
        simpleType!(title_attr_type TitleAttrType);
        simpleType!(show_type ShowType);
        simpleType!(actuate_type ActuateType);
        simpleType!(label_type LabelType);
        simpleType!(from_type FromType);
        simpleType!(to_type ToType);
        complexType!(simple Simple 62 m
            attributes {
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink href href o - String,
                xlink role role o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink r#type r#type o - String
            }

            any(Unit): v - - unit
        );
        complexType!(extended Extended 63 -
            attributes {
                xlink role role o - String,
                xlink title title o - String,
                xlink r#type r#type r - String
            }

            extended_model(super::g::ExtendedModel): v + - super::g::extended_model
        );
        complexType!(title_elt_type TitleEltType 64 m
            attributes {
                xlink r#type r#type r - String,
                xml lang lang o - String
            }

            any(Unit): v - - unit
        );
        complexType!(resource_type ResourceType 65 m
            attributes {
                xlink label label o - String,
                xlink role role o - String,
                xlink title title o - String,
                xlink r#type r#type r - String
            }

            any(Unit): v - - unit
        );
        complexType!(locator_type LocatorType 66 -
            attributes {
                xlink href href r - String,
                xlink label label o - String,
                xlink role role o - String,
                xlink title title o - String,
                xlink r#type r#type r - String
            }

            choice(super::local::ArcModel): v + - super::local::arc_model
        );
        complexType!(arc_type ArcType 67 -
            attributes {
                xlink actuate actuate o - String,
                xlink arcrole arcrole o - String,
                xlink from from o - String,
                xlink show show o - String,
                xlink title title o - String,
                xlink to to o - String,
                xlink r#type r#type r - String
            }

            choice(super::local::ArcModel): v + - super::local::arc_model
        );
    }
    pub mod e {
        xust_xsd::use_xsd!();
    }
}
pub mod xml {
    pub mod local {
        xust_xsd::use_xsd!();
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
    }
    pub mod e {
        xust_xsd::use_xsd!();
    }
}
