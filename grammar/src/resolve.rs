use xust_tree::qnames::{NCName, Namespace};

pub trait FunctionResolver {
    fn find(&self, namespace: Namespace, local_name: NCName, arity: usize) -> Option<usize>;
    fn len(&self) -> usize;
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

pub(crate) struct EmptyFunctionResolver;

impl FunctionResolver for EmptyFunctionResolver {
    fn find(&self, _namespace: Namespace, _local_name: NCName, _arity: usize) -> Option<usize> {
        None
    }
    fn len(&self) -> usize {
        0
    }
}

pub(crate) const EMPTY_FUNCTION_RESOLVER: EmptyFunctionResolver = EmptyFunctionResolver;
