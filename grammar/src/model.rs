#![allow(clippy::upper_case_acronyms)]
use rust_decimal::Decimal;
use std::borrow::Cow;
use std::collections::BTreeMap;
use xust_tree::{qnames::NCName, string_collection::StringId};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Expr {}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Module<'a> {
    pub version: Option<VersionDecl>,
    pub module: LibraryOrMain<'a>,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum LibraryOrMain<'a> {
    LibraryModule(ModuleDecl<'a>, Prolog),
    MainModule(Prolog),
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct VersionDecl {
    pub encoding: Option<String>,
    pub version: Option<String>,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ModuleDecl<'a> {
    pub name: NCName<'a>,
    pub uri: Cow<'a, str>,
}
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct Prolog {
    pub boundary_space_policy: Option<StripOrPreserve>,
    pub default_collation: Option<URILiteral>,
    pub base_uri: Option<URILiteral>,
    pub construction_mode: Option<StripOrPreserve>,
    pub ordering_mode: Option<OrderingMode>,
    pub empty_order: Option<GreatestOrLeast>,
    pub copy_namespaces_mode: Option<CopyNamespacesMode>,
    //pub decimal_formats: BTreeMap<Option<QName>, BTreeMap<DFPropertyName, String>>,
    pub schema_imports: BTreeMap<Option<StringId>, (URILiteral, Vec<URILiteral>)>,
    pub module_imports: BTreeMap<Option<StringId>, URILiteral>,
    pub default_element_namespaces: Option<URILiteral>,
    pub default_function_namespaces: Option<URILiteral>,
    // pub context_item: Option<ContextItem>,
    // pub functions: BTreeMap<QNameId, FunctionDecl>,
    // pub options: BTreeMap<QNameId, String>,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum StripOrPreserve {
    Strip,
    Preserve,
}
#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub enum OrderingMode {
    Ordered,
    Unordered,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum GreatestOrLeast {
    Greatest,
    Least,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CopyNamespacesMode {
    pub preserve: bool,
    pub inherit: bool,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum VarValueOrExternal {
    Value,
    External,
}
#[derive(Debug, Clone, PartialEq)]
pub struct Annotation {
    pub values: Vec<Literal>,
}
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum DFPropertyName {
    DecimalSeparator,
    GroupingSeparator,
    Infinity,
    MinusSign,
    NaN,
    Percent,
    PerMille,
    ZeroDigit,
    Digit,
    PatternSeparator,
    ExponentSeparator,
}
#[derive(Debug, Clone, PartialEq)]
pub enum Literal {
    String(StringId),
    AnyURI(StringId),
    Integer(I64),     // xs:integer
    Decimal(Decimal), // xs:decimal
    Float(f32),       // xs:float
    Double(F64),      // xs:double
}
#[repr(packed)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct I64(pub i64);
#[repr(packed)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct F64(pub f64);
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Axis {
    Child,
    Descendant,
    Attribute,
    Self_,
    DescendantOrSelf,
    FollowingSibling,
    Following,
    Parent,
    Ancestor,
    PrecedingSibling,
    Preceding,
    AncestorOrSelf,
    Namespace,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum SortDirection {
    Ascending,
    Descending,
}
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SomeOrEvery {
    Some,
    Every,
}
#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub enum Comp {
    Equal,       // =
    NotEqual,    // !=
    LessOrEqual, // <=
    Less,        // <
    Greater,     // >
    GreaterOrEqual,
    Eq,       // eq
    Ne,       // ne
    Lt,       // lt
    Le,       // le
    Gt,       // gt
    Ge,       // ge
    Is,       // is
    Precedes, // <<
    Follows,  // >>
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PlusMinus {
    Plus,
    Minus,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Multiplicative {
    Multiply,
    Div,
    IDiv,
    Mod,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum IntersectExcept {
    Intersect,
    Except,
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct URILiteral(pub StringId);
