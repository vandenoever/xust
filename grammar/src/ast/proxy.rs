#![allow(clippy::upper_case_acronyms)]
use crate::{
    ast::{self, KeySpecifier, Node, NodeId, Nodes, OccurrenceIndicator, SomeOrEvery},
    model::{Axis, OrderingMode, VarValueOrExternal},
};
use rust_decimal::Decimal;
use xust_tree::{
    qnames::{NCName, Namespace, Prefix, QName, QNameId, QNameRef},
    string_collection::StringId,
};
use xust_xsd::types::TypeId;

#[derive(Debug)]
#[cfg_attr(test, derive(PartialEq))]
pub enum Expr<'a> {
    AxisStep(AxisStep<'a>),
    BinaryOp(Binary<'a>),
    ContextItemExpr,
    Decimal(Decimal),
    Double(f64),
    FLWORExpr(FLWORExpr<'a>),
    OrderByClause(OrderByClause<'a>),
    WindowClause(WindowClause<'a>),
    If(If<'a>),
    Function(Function<'a>),
    FunctionCall(FunctionCall<'a>),
    ArrowFunctionCall(ArrowFunctionCall<'a>),
    NamedFunctionRef {
        id: usize,
        arity: u32,
    },
    Integer(i64),
    Negate(Negate<'a>),
    String(String<'a>),
    SequenceType(SequenceType<'a>),
    VarRef(VarRef<'a>),
    VarDecl,
    ContextItem,
    SquareArrayConstructor(ExprIterator<'a>),
    CurlyArrayConstructor(ExprIterator<'a>),
    Postfix(Postfix<'a>),
    OrderedExpr(OrderingMode, ExprIterator<'a>),
    Empty,
    CompDocConstructor(ExprIterator<'a>),
    CompElemConstructor(CompConstructor<'a>),
    CompAttrConstructor(CompConstructor<'a>),
    CompPIConstructor(CompPIConstructor<'a>),
    CompTextConstructor(ExprIterator<'a>),
    CompCommentConstructor(ExprIterator<'a>),
    DirElemConstructor(DirElemConstructor<'a>),
    DirAttribute(DirAttribute<'a>),
    DirCommentConstructor(DirCommentConstructor<'a>),
    DirPIConstructor(DirPIConstructor<'a>),
    StringConstructor(ExprIterator<'a>),
    MapConstructor,
    QName(QNameId),
    SingleType {
        optional: bool,
        single_type: QNameId,
    },
    QuantifiedExpr(SomeOrEvery),
    ArgumentPlaceholder,
    UnaryLookup,
}

impl<'a> Expr<'a> {
    pub(super) fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        let n = nodes.node(pos);
        match n.1 {
            Node::FLWORExpr(return_clause) => {
                Expr::FLWORExpr(FLWORExpr::new(nodes, pos, return_clause))
            }
            Node::QueryBody => ExprIterator::new(nodes, pos).last().unwrap(),
            Node::String(id) => Expr::String(String { nodes, id }),
            Node::BinaryOp(op) => Expr::BinaryOp(Binary {
                op,
                nodes,
                first: pos + 1,
            }),
            Node::Integer(i) => Expr::Integer(i.0),
            Node::Decimal(i) => Expr::Decimal(i),
            Node::Double(i) => Expr::Double(i.0),
            Node::FunctionCall { id, arity, dynamic } => Expr::FunctionCall(FunctionCall {
                nodes,
                pos,
                id,
                arity,
                dynamic,
            }),
            Node::ArrowFunctionCall { id, arity } => {
                Expr::ArrowFunctionCall(ArrowFunctionCall::new(nodes, pos, id, arity))
            }
            Node::NamedFunctionRef { id, arity } => Expr::NamedFunctionRef { id, arity },
            Node::ParenthesizedExpr if n.0 == 1 => Expr::Empty,
            Node::ParenthesizedExpr => Expr::new(nodes, pos + 1),
            Node::ContextItemExpr => Expr::ContextItemExpr,
            Node::AxisStep(axis, node_test) => {
                Expr::AxisStep(AxisStep::new(nodes, pos, axis, node_test))
            }
            Node::Negate => Expr::Negate(Negate::new(nodes, pos)),
            Node::SequenceType {
                occurrence_indicator,
            } => Expr::SequenceType(SequenceType::new(nodes, pos, occurrence_indicator)),
            Node::VarRef(v) => Expr::VarRef(VarRef::new(nodes, v)),
            Node::SquareArrayConstructor => {
                Expr::SquareArrayConstructor(ExprIterator::new(nodes, pos))
            }
            Node::CurlyArrayConstructor => {
                Expr::CurlyArrayConstructor(ExprIterator::new(nodes, pos))
            }
            Node::Postfix => Expr::Postfix(Postfix::new(nodes, pos)),
            Node::CompDocConstructor => Expr::CompDocConstructor(ExprIterator::new(nodes, pos)),
            Node::CompElemConstructor(qname) => {
                Expr::CompElemConstructor(CompConstructor::new(qname, nodes, pos))
            }
            Node::CompAttrConstructor(qname) => {
                Expr::CompAttrConstructor(CompConstructor::new(qname, nodes, pos))
            }
            Node::CompPIConstructor(ncname) => {
                Expr::CompPIConstructor(CompPIConstructor::new(ncname, nodes, pos))
            }
            Node::CompTextConstructor => Expr::CompTextConstructor(ExprIterator::new(nodes, pos)),
            Node::CompCommentConstructor => {
                Expr::CompCommentConstructor(ExprIterator::new(nodes, pos))
            }
            Node::DirElemConstructor(qname, self_close) => {
                Expr::DirElemConstructor(DirElemConstructor::new(qname, self_close, nodes, pos))
            }
            Node::DirCommentConstructor(comment) => {
                Expr::DirCommentConstructor(DirCommentConstructor::new(comment, nodes))
            }
            Node::DirPIConstructor(target, content) => {
                Expr::DirPIConstructor(DirPIConstructor::new(target, content, nodes))
            }
            Node::DirAttribute(qname) => Expr::DirAttribute(DirAttribute::new(qname, nodes, pos)),
            Node::StringConstructor => Expr::StringConstructor(ExprIterator::new(nodes, pos)),
            Node::MapConstructor => Expr::MapConstructor,
            Node::SingleType {
                optional,
                single_type,
            } => Expr::SingleType {
                optional,
                single_type,
            },
            Node::QuantifiedExpr(some_or_every) => Expr::QuantifiedExpr(some_or_every),
            Node::ArgumentPlaceholder => Expr::ArgumentPlaceholder,
            Node::Ordering(ordering_mode) => {
                Expr::OrderedExpr(ordering_mode, ExprIterator::new(nodes, pos))
            }
            Node::Lookup(_) => Expr::UnaryLookup,
            Node::WindowClause(wc) => Expr::WindowClause(WindowClause::new(nodes, pos, wc)),
            Node::OrderByClause { stable } => {
                Expr::OrderByClause(OrderByClause::new(nodes, pos, stable))
            }
            Node::VarDecl { .. } => Expr::VarDecl,
            Node::ContextItem => Expr::ContextItem,
            Node::Function { .. } => Expr::Function(Function::new(nodes, pos)),
            Node::IfExpr => Expr::If(If::new(nodes, pos)),
            Node::TypeswitchExpr(_) => Expr::Integer(0),
            Node::SwitchExpr => Expr::Integer(0),
            Node::TryCatchExpr => Expr::Integer(0),
            ref n => {
                nodes.print_nodes();
                panic!("{:#?}", n);
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct ExprRef<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
}

impl<'a> ExprRef<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        Self { nodes, pos }
    }
    pub fn expr(&self) -> Expr {
        Expr::new(self.nodes, self.pos)
    }
}

#[cfg(test)]
impl<'a> PartialEq for ExprRef<'a> {
    fn eq(&self, b: &ExprRef<'a>) -> bool {
        node_eq(self.nodes, self.pos, b.nodes, b.pos) || unimplemented!()
    }
}

#[derive(Debug, Clone)]
pub struct ExprIterator<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    end: NodeId,
}

impl<'a> ExprIterator<'a> {
    pub(super) fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        let end = pos + nodes.node(pos).0;
        let pos = pos + 1;
        Self { nodes, pos, end }
    }
    pub fn is_empty(&self) -> bool {
        self.pos == self.end
    }
    pub fn user_function(&self, id: usize) -> Option<Function<'a>> {
        user_function(id, self.nodes)
    }
}

impl<'a> Iterator for ExprIterator<'a> {
    type Item = Expr<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.pos != self.end {
            let next = Some(Expr::new(self.nodes, self.pos));
            self.pos += self.nodes.node(self.pos).0;
            next
        } else {
            None
        }
    }
}

impl<'a> IntoIterator for &ExprIterator<'a> {
    type Item = Expr<'a>;
    type IntoIter = ExprIterator<'a>;
    fn into_iter(self) -> Self::IntoIter {
        self.clone()
    }
}

#[cfg(test)]
impl<'a> PartialEq for ExprIterator<'a> {
    fn eq(&self, b: &Self) -> bool {
        self.end == b.end && node_eq(self.nodes, self.pos, b.nodes, b.pos) || unimplemented!()
    }
}

pub enum PostfixItem<'a> {
    LookupWildcard,
    LookupInteger(u32),
    Lookup(ExprRef<'a>),
    LookupNCName(NCName<'a>),
    FunctionArgs(ExprIterator<'a>),
    Predicate(ExprIterator<'a>),
}

impl<'a> PostfixItem<'a> {
    pub(super) fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        let n = nodes.node(pos);
        match n.1 {
            Node::Lookup(KeySpecifier::Wildcard) => PostfixItem::LookupWildcard,
            Node::Lookup(KeySpecifier::Integer(i)) => PostfixItem::LookupInteger(i),
            Node::Lookup(KeySpecifier::Expr) => PostfixItem::Lookup(ExprRef::new(nodes, pos + 1)),
            Node::Lookup(KeySpecifier::NCName(ncname)) => {
                PostfixItem::LookupNCName(NCName(nodes.string(ncname)))
            }
            Node::PostfixArgumentList => PostfixItem::FunctionArgs(ExprIterator::new(nodes, pos)),
            Node::Predicate => PostfixItem::Predicate(ExprIterator::new(nodes, pos)),
            ref n => panic!("{:#?}", n),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Postfix<'a> {
    nodes: &'a Nodes,
    expr_pos: NodeId,
    pos: NodeId,
    end: NodeId,
}

impl<'a> Postfix<'a> {
    pub(super) fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        let end = pos + nodes.node(pos).0;
        let expr_pos = pos + 1;
        let pos = expr_pos + nodes.node(expr_pos).0;
        Self {
            nodes,
            expr_pos,
            pos,
            end,
        }
    }
    pub fn is_empty(&self) -> bool {
        self.pos == self.end
    }
    pub fn expr(&self) -> Expr<'a> {
        Expr::new(self.nodes, self.expr_pos)
    }
}

impl<'a> Iterator for Postfix<'a> {
    type Item = PostfixItem<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.pos != self.end {
            let next = Some(PostfixItem::new(self.nodes, self.pos));
            self.pos += self.nodes.node(self.pos).0;
            next
        } else {
            None
        }
    }
}

impl<'a> IntoIterator for &Postfix<'a> {
    type Item = PostfixItem<'a>;
    type IntoIter = Postfix<'a>;
    fn into_iter(self) -> Self::IntoIter {
        self.clone()
    }
}

#[cfg(test)]
impl<'a> PartialEq for Postfix<'a> {
    fn eq(&self, b: &Self) -> bool {
        self.end == b.end && node_eq(self.nodes, self.pos, b.nodes, b.pos) || unimplemented!()
    }
}

#[cfg(test)]
fn node_eq<T: PartialEq>(a: &Nodes, an: T, b: &Nodes, bn: T) -> bool {
    let ap: *const Nodes = a;
    let bp: *const Nodes = b;
    an == bn && ap == bp
}

#[derive(Debug)]
pub struct String<'a> {
    nodes: &'a Nodes,
    id: StringId,
}

#[cfg(test)]
impl<'a> PartialEq for String<'a> {
    fn eq(&self, b: &Self) -> bool {
        node_eq(self.nodes, self.id, b.nodes, b.id) || self.str() == b.str()
    }
}

impl<'a> String<'a> {
    pub fn str(&self) -> &'a str {
        self.nodes.strings.get(self.id)
    }
}

#[derive(Debug)]
pub struct Binary<'a> {
    op: ast::BinaryOp,
    nodes: &'a Nodes,
    first: NodeId,
}

impl<'a> Binary<'a> {
    pub fn op(&self) -> ast::BinaryOp {
        self.op
    }
    pub fn first(&self) -> Expr<'a> {
        Expr::new(self.nodes, self.first)
    }
    pub fn second(&self) -> Expr<'a> {
        Expr::new(self.nodes, self.first + self.nodes.node(self.first).0)
    }
}

#[cfg(test)]
impl<'a> PartialEq for Binary<'a> {
    fn eq(&self, b: &Self) -> bool {
        self.op == b.op
            && (node_eq(self.nodes, self.first, b.nodes, b.first)
                || self.first() == b.first() && self.second() == b.second())
    }
}

#[derive(Debug)]
pub struct FunctionCall<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    id: usize,
    arity: u32,
    dynamic: bool,
}

fn user_function(id: usize, nodes: &Nodes) -> Option<Function> {
    if id < nodes.static_functions {
        return None;
    }
    Some(Function::new(
        nodes,
        NodeId::from(id - nodes.static_functions),
    ))
}

impl<'a> FunctionCall<'a> {
    pub fn arity(&self) -> usize {
        self.arity as usize
    }
    pub fn id(&self) -> usize {
        self.id
    }
    pub fn user_function(&self) -> Option<Function<'a>> {
        user_function(self.id, self.nodes)
    }
    pub fn args(&self) -> ExprIterator<'a> {
        ExprIterator::new(self.nodes, self.pos)
    }
}

#[cfg(test)]
impl<'a> PartialEq for FunctionCall<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct ArrowFunctionCall<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    id: Option<usize>,
    arity: u32,
}

impl<'a> ArrowFunctionCall<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId, id: usize, arity: u32) -> Self {
        let id = if id > 0 { Some(id) } else { None };
        Self {
            nodes,
            pos,
            id,
            arity,
        }
    }
    pub fn user_function(&self) -> Option<Function<'a>> {
        self.id.and_then(|id| user_function(id, self.nodes))
    }
    pub fn arity(&self) -> usize {
        self.arity as usize
    }
    pub fn id(&self) -> Option<usize> {
        self.id
    }
    pub fn args(&self) -> ExprIterator<'a> {
        // todo: fix passing of args, currently only passing first arg
        ExprIterator::new(self.nodes, self.pos)
    }
}

#[cfg(test)]
impl<'a> PartialEq for ArrowFunctionCall<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct FLWORExpr<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    return_clause: NodeId,
}

impl<'a> FLWORExpr<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId, _bad_return_clause: Option<NodeId>) -> Self {
        let size = nodes.node(pos).0;
        let end = pos + size;
        let mut p = pos + 1;
        let mut return_clause = p;
        while p != end {
            return_clause = p;
            p += nodes.node(p).0;
        }
        /*
        if bad_return_clause != return_clause {
            println!("{:?} != {:?}", bad_return_clause, return_clause);
        }
        */
        Self {
            nodes,
            pos,
            return_clause,
        }
    }
    pub fn clauses(&self) -> ClauseIterator<'a> {
        ClauseIterator {
            nodes: self.nodes,
            pos: self.pos + 1,
            end: self.return_clause,
        }
    }
    pub fn return_clause(&self) -> Expr<'a> {
        Expr::new(self.nodes, self.return_clause)
    }
}

#[derive(Clone)]
pub struct ClauseIterator<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    end: NodeId,
}

impl<'a> Iterator for ClauseIterator<'a> {
    type Item = Clause<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.pos >= self.end {
            return None;
        }
        let pos = self.pos;
        let (size, node) = self.nodes.node(pos);
        self.pos += *size;
        Some(Clause::new(self.nodes, pos, *size, node))
    }
}

#[derive(Debug)]
pub enum Clause<'a> {
    For(ForBinding<'a>),
    Let(LetBinding<'a>),
    Window(WindowClause<'a>),
    Where(Expr<'a>),
    Count(u16),
    Other,
}

impl<'a> Clause<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId, _size: u32, node: &Node) -> Self {
        match node {
            Node::ForBinding(for_binding) => Clause::For(ForBinding::new(nodes, pos, for_binding)),
            Node::LetBinding(name, has_type_declaration) => {
                Clause::Let(LetBinding::new(nodes, pos, *name, *has_type_declaration))
            }
            Node::WhereClause => Clause::Where(Expr::new(nodes, pos + 1)),
            Node::CountClause(varid) => Clause::Count(*varid),

            // Node::WindowClause(w) => panic!("window {:?}", w),
            // n => panic!("{:?}", n),
            _ => Clause::Other,
        }
    }
}

#[cfg(test)]
impl<'a> PartialEq for FLWORExpr<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct ForBinding<'a> {
    nodes: &'a Nodes,
    var: u16,
    type_declaration: Option<NodeId>,
    allowing_empty: bool,
    positional_var: Option<u16>,
    expr: NodeId,
}

impl<'a> ForBinding<'a> {
    fn new(nodes: &'a Nodes, mut pos: NodeId, fb: &ast::ForBinding) -> Self {
        pos += 1;
        let type_declaration = if fb.has_type_declaration { None } else { None };
        Self {
            nodes,
            var: fb.var,
            allowing_empty: fb.allowing_empty,
            type_declaration,
            positional_var: fb.positional_var,
            expr: pos,
        }
    }
    pub fn varid(&self) -> u16 {
        self.var
    }
    pub fn type_declaration(&self) -> Option<SequenceType<'a>> {
        unimplemented!()
        /*
        self.type_declaration.map(|pos| SequenceType {
            nodes: self.nodes,
            pos,j
            occurrence_indicator: {
                unimplemented!()
            }
        })*/
    }
    pub fn allowing_empty(&self) -> bool {
        self.allowing_empty
    }
    pub fn positional_var(&self) -> Option<u16> {
        self.positional_var
    }
    pub fn expr(&self) -> Expr<'a> {
        Expr::new(self.nodes, self.expr)
    }
}

#[derive(Debug)]
pub struct LetBinding<'a> {
    nodes: &'a Nodes,
    varid: u16,
    type_declaration: Option<NodeId>,
    expr: NodeId,
}

impl<'a> LetBinding<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId, varid: u16, has_type_declaration: bool) -> Self {
        let (type_declaration, expr) = if has_type_declaration {
            let (size, _n) = nodes.node(pos + 1);
            (Some(pos + 1), pos + 1 + *size)
        } else {
            (None, pos + 1)
        };
        Self {
            nodes,
            varid,
            type_declaration,
            expr,
        }
    }
    pub fn varid(&self) -> u16 {
        self.varid
    }
    pub fn type_declaration(&self) {}
    pub fn expr(&self) -> Expr<'a> {
        Expr::new(self.nodes, self.expr)
    }
}

#[derive(Debug)]
pub struct OrderByClause<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
}

impl<'a> OrderByClause<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId, _stable: bool) -> Self {
        Self { nodes, pos }
    }
}

#[cfg(test)]
impl<'a> PartialEq for OrderByClause<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct WindowClause<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
}

impl<'a> WindowClause<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId, _wc: ast::WindowClause) -> Self {
        Self { nodes, pos }
    }
}

#[cfg(test)]
impl<'a> PartialEq for WindowClause<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct If<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
}

impl<'a> If<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        Self { nodes, pos }
    }
    pub fn r#if(&self) -> Expr<'a> {
        Expr::new(self.nodes, self.pos + 1)
    }
    pub fn r#then(&self) -> Expr<'a> {
        let pos = self.pos + 1;
        let pos = pos + self.nodes.node(pos).0;
        Expr::new(self.nodes, pos)
    }
    pub fn r#else(&self) -> Expr<'a> {
        let pos = self.pos + 1;
        let pos = pos + self.nodes.node(pos).0;
        let pos = pos + self.nodes.node(pos).0;
        Expr::new(self.nodes, pos)
    }
}

#[cfg(test)]
impl<'a> PartialEq for If<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct Function<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
}

impl<'a> Function<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        Self { nodes, pos }
    }
    pub fn stack_size(&self) -> u16 {
        if let Node::Function { stack_size, .. } = self.nodes.node(self.pos).1 {
            stack_size
        } else {
            panic!()
        }
    }
    pub fn arity(&self) -> u32 {
        if let Node::Function { arity, .. } = self.nodes.node(self.pos).1 {
            arity
        } else {
            panic!()
        }
    }
    pub fn print(&self) {
        if let (size, Node::Function { .. }) = self.nodes.node(self.pos) {
            let end = self.pos + *size;
            let mut n = self.pos;
            while n != end {
                let node = self.nodes.node(n);
                println!("{:?}", node);
                n += 1;
            }
        }
    }
    pub fn expr(&self) -> Expr {
        if let (_, Node::Function { .. }) = self.nodes.node(self.pos) {
            Expr::new(self.nodes, self.pos + 1)
        } else {
            panic!()
        }
    }
}

#[cfg(test)]
impl<'a> PartialEq for Function<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct SequenceType<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    occurrence_indicator: OccurrenceIndicator,
}

impl<'a> SequenceType<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId, occurrence_indicator: OccurrenceIndicator) -> Self {
        Self {
            nodes,
            pos,
            occurrence_indicator,
        }
    }
    // do not call this if occurrence_indicator is OccurrenceIndicator::Zero!
    pub fn item_type(&self) -> ItemType<'a> {
        assert!(self.occurrence_indicator != OccurrenceIndicator::Zero);
        ItemType::new(self.nodes, self.pos + 1)
    }
    pub fn occurrence_indicator(&self) -> OccurrenceIndicator {
        self.occurrence_indicator
    }
}

#[cfg(test)]
impl<'a> PartialEq for SequenceType<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub enum ItemType<'a> {
    KindTest(KindTest<'a>),
    ArrayTest(ArrayTest<'a>),
    AtomicOrUnionType(AtomicOrUnionType<'a>),
    Item,
    MapTest,
    AnyMapTest,
    FunctionTest,
}
impl<'a> ItemType<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        let item_type = if let Node::ItemType(item_type) = &nodes.node(pos).1 {
            item_type
        } else {
            panic!("{:#?} {:#?}", nodes.node(pos).1, nodes)
        };
        match item_type {
            ast::ItemType::KindTest => ItemType::KindTest(KindTest::new(nodes, pos + 1)),
            ast::ItemType::ArrayTest => ItemType::ArrayTest(ArrayTest::new(nodes, pos)),
            ast::ItemType::AtomicOrUnionType(type_id) => ItemType::AtomicOrUnionType(
                AtomicOrUnionType::new(nodes, nodes.types.to_type_id(*type_id)),
            ),
            ast::ItemType::Item => ItemType::Item,
            ast::ItemType::MapTest(_) => ItemType::MapTest,
            ast::ItemType::AnyMapTest => ItemType::AnyMapTest,
            ast::ItemType::FunctionTest => ItemType::FunctionTest,
        }
    }
}

#[derive(Debug)]
pub enum KindTest<'a> {
    DocumentTestNone,
    DocumentTestElementTest(ElementTest<'a>),
    ElementTest(ElementTest<'a>),
    AttributeTest(ElementTest<'a>),
    SchemaElementTest(QNameId),
    SchemaAttributeTest(QNameId),
    TextTest,
    AnyKindTest,
    CommentTest,
    PITestNone,
    PITestNCName(StringId),
    PITestString(StringId),
    NamespaceNodeTest,
}

impl<'a> KindTest<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        let kind_test = if let Node::KindTest(kind_test) = &nodes.node(pos).1 {
            kind_test
        } else {
            panic!("{:#?}", nodes.node(pos).1)
        };
        match kind_test {
            ast::KindTest::DocumentTestNone => KindTest::DocumentTestNone,
            ast::KindTest::DocumentTestElementTest {
                match_nilled,
                match_name,
                match_type,
            } => KindTest::DocumentTestElementTest(ElementTest::new(
                nodes,
                pos + 1,
                *match_nilled,
                *match_name,
                *match_type,
            )),
            ast::KindTest::ElementTest {
                match_nilled,
                match_name,
                match_type,
            } => KindTest::ElementTest(ElementTest::new(
                nodes,
                pos + 1,
                *match_nilled,
                *match_name,
                *match_type,
            )),
            ast::KindTest::AttributeTest {
                match_name,
                match_type,
            } => KindTest::AttributeTest(ElementTest::new(
                nodes,
                pos + 1,
                false,
                *match_name,
                *match_type,
            )),
            ast::KindTest::SchemaElementTest(q) => KindTest::SchemaElementTest(*q),
            ast::KindTest::SchemaAttributeTest(q) => KindTest::SchemaAttributeTest(*q),
            ast::KindTest::TextTest => KindTest::TextTest,
            ast::KindTest::AnyKindTest => KindTest::AnyKindTest,
            ast::KindTest::CommentTest => KindTest::CommentTest,
            ast::KindTest::PITestNone => KindTest::PITestNone,
            ast::KindTest::PITestNCName(string) => KindTest::PITestNCName(*string),
            ast::KindTest::PITestString(string) => KindTest::PITestString(*string),
            ast::KindTest::NamespaceNodeTest => KindTest::NamespaceNodeTest,
            ref n => unimplemented!("{:#?}", n),
        }
    }
}

#[derive(Debug)]
pub struct ElementTest<'a> {
    nodes: &'a Nodes,
    name: Option<QNameId>,
    type_name: Option<QNameId>,
    match_nilled: bool,
}

impl<'a> ElementTest<'a> {
    fn new(
        nodes: &'a Nodes,
        pos: NodeId,
        match_nilled: bool,
        match_name: bool,
        match_type: bool,
    ) -> Self {
        let (name, type_name) = if match_name || match_type {
            if let Node::ElementOrAttributeTest(test) = &nodes.node(pos).1 {
                (
                    if match_name { Some(test.name) } else { None },
                    if match_type { Some(test.r#type) } else { None },
                )
            } else {
                panic!("{:#?}", nodes.node(pos).1)
            }
        } else {
            (None, None)
        };
        Self {
            nodes,
            name,
            type_name,
            match_nilled,
        }
    }
    pub fn match_nilled(&self) -> bool {
        self.match_nilled
    }
    pub fn name(&self) -> Option<QNameRef<'a>> {
        self.name.map(|q| self.nodes.qname(q))
    }
    pub fn type_name(&self) -> Option<QNameRef<'a>> {
        self.type_name.map(|q| self.nodes.qname(q))
    }
}

#[derive(Debug)]
pub struct ArrayTest<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
}

impl<'a> ArrayTest<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        Self { nodes, pos }
    }
    pub fn sequence_type(&self) -> Option<SequenceType<'a>> {
        if self.nodes.node(self.pos).0 == 1 {
            None
        } else {
            Some(
                if let Node::SequenceType {
                    occurrence_indicator,
                } = self.nodes.node(self.pos + 1).1
                {
                    SequenceType::new(self.nodes, self.pos + 1, occurrence_indicator)
                } else {
                    panic!()
                },
            )
        }
    }
}

#[derive(Debug)]
pub struct AtomicOrUnionType<'a> {
    nodes: &'a Nodes,
    r#type: TypeId,
}

impl<'a> AtomicOrUnionType<'a> {
    fn new(nodes: &'a Nodes, r#type: TypeId) -> Self {
        Self { nodes, r#type }
    }
    pub fn type_id(&self) -> TypeId {
        self.r#type
    }
}

#[derive(Debug)]
pub struct AxisStep<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    axis: Axis,
    node_test: ast::NodeTest,
}

impl<'a> AxisStep<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId, axis: Axis, node_test: ast::NodeTest) -> Self {
        Self {
            nodes,
            pos,
            axis,
            node_test,
        }
    }
    pub fn axis(&self) -> Axis {
        self.axis
    }
    pub fn node_test(&self) -> NodeTest {
        match self.node_test {
            ast::NodeTest::KindTest => NodeTest::KindTest(KindTest::new(self.nodes, self.pos + 1)),
            ast::NodeTest::NS(id) => NodeTest::NS(Namespace(self.nodes.strings.get(id))),
            // ast::NodeTest::NSPrefix(id) => NodeTest::NSPrefix(self.nodes.qnames.prefix_id(id)),
            ast::NodeTest::Localname(id) => NodeTest::Localname(NCName(self.nodes.strings.get(id))),
            ast::NodeTest::Wildcard => NodeTest::Wildcard,
            ast::NodeTest::QName(id) => NodeTest::QName(self.nodes.qnames.get(id)),
        }
    }
}

#[cfg(test)]
impl<'a> PartialEq for AxisStep<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub enum NodeTest<'a> {
    KindTest(KindTest<'a>),
    NS(Namespace<'a>),
    NSPrefix(Prefix<'a>),
    Localname(NCName<'a>),
    Wildcard,
    QName(QNameRef<'a>),
}

#[derive(Debug)]
pub struct Negate<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
}

impl<'a> Negate<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId) -> Self {
        Self { nodes, pos }
    }
    pub fn expr(&self) -> Expr<'a> {
        Expr::new(self.nodes, self.pos + 1)
    }
}

#[cfg(test)]
impl<'a> PartialEq for Negate<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.pos == b.pos) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct VarRef<'a> {
    nodes: &'a Nodes,
    varid: u16,
}

impl<'a> VarRef<'a> {
    fn new(nodes: &'a Nodes, varid: u16) -> Self {
        Self { nodes, varid }
    }
    pub fn varid(&self) -> u16 {
        self.varid
    }
}

#[cfg(test)]
impl<'a> PartialEq for VarRef<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.varid == b.varid) || unimplemented!()
    }
}

pub enum NameOrExpression<'a, T> {
    Name(T),
    Expression(Expr<'a>),
}

#[derive(Debug)]
pub struct CompConstructor<'a> {
    qname: Option<QNameId>,
    nodes: &'a Nodes,
    pos: NodeId,
}

impl<'a> CompConstructor<'a> {
    fn new(qname: Option<QNameId>, nodes: &'a Nodes, pos: NodeId) -> Self {
        Self { qname, nodes, pos }
    }
    pub fn qname(&self) -> NameOrExpression<QNameRef<'a>> {
        if let Some(qname) = self.qname {
            NameOrExpression::Name(self.nodes.qname(qname))
        } else {
            NameOrExpression::Expression(Expr::new(self.nodes, self.pos + 1))
        }
    }
    pub fn expr(&self) -> Option<Expr<'a>> {
        let size = self.nodes.node(self.pos).0;
        let name_size = if self.qname.is_some() {
            0
        } else {
            self.nodes.node(self.pos + 1).0
        };
        let has_expr = size > name_size + 1;
        if has_expr {
            let pos = self.pos + 1 + name_size;
            Some(Expr::new(self.nodes, pos))
        } else {
            None
        }
    }
}

#[cfg(test)]
impl<'a> PartialEq for CompConstructor<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct CompPIConstructor<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    ncname: StringId,
}

impl<'a> CompPIConstructor<'a> {
    fn new(ncname: StringId, nodes: &'a Nodes, pos: NodeId) -> Self {
        Self { nodes, pos, ncname }
    }
    pub fn target(&self) -> NameOrExpression<&'a str> {
        let ncname = self.nodes.string(self.ncname);
        if ncname.is_empty() {
            NameOrExpression::Expression(Expr::new(self.nodes, self.pos + 1))
        } else {
            NameOrExpression::Name(ncname)
        }
    }
    pub fn content(&self) -> Option<Expr<'a>> {
        let size = self.nodes.node(self.pos).0;
        let offset = if size > 1 {
            let name_size = self.nodes.node(self.pos + 1).0;
            if size > name_size + 1 {
                1 + name_size
            } else {
                0
            }
        } else {
            0
        };
        if offset > 0 {
            Some(Expr::new(self.nodes, self.pos + offset))
        } else {
            None
        }
    }
}

#[cfg(test)]
impl<'a> PartialEq for CompPIConstructor<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct DirElemConstructor<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    qname: QNameId,
    self_close: bool,
}

impl<'a> DirElemConstructor<'a> {
    fn new(qname: QNameId, self_close: bool, nodes: &'a Nodes, pos: NodeId) -> Self {
        Self {
            nodes,
            pos,
            qname,
            self_close,
        }
    }
    pub fn qname(&self) -> QNameRef<'a> {
        self.nodes.qname(self.qname)
    }
    pub fn self_close(&self) -> bool {
        self.self_close
    }
    pub fn attributes(&self) -> ExprIterator {
        ExprIterator::new(self.nodes, self.pos)
    }
    pub fn contents(&self) -> ExprIterator {
        ExprIterator::new(self.nodes, self.pos)
    }
}

#[cfg(test)]
impl<'a> PartialEq for DirElemConstructor<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.qname == b.qname) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct DirAttribute<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    qname: QNameId,
}

impl<'a> DirAttribute<'a> {
    fn new(qname: QNameId, nodes: &'a Nodes, pos: NodeId) -> Self {
        Self { nodes, pos, qname }
    }
    pub fn qname(&self) -> QNameRef<'a> {
        self.nodes.qname(self.qname)
    }
    pub fn contents(&self) -> ExprIterator {
        ExprIterator::new(self.nodes, self.pos)
    }
}

#[cfg(test)]
impl<'a> PartialEq for DirAttribute<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.qname == b.qname) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct DirCommentConstructor<'a> {
    nodes: &'a Nodes,
    comment: StringId,
}

impl<'a> DirCommentConstructor<'a> {
    fn new(comment: StringId, nodes: &'a Nodes) -> Self {
        Self { nodes, comment }
    }
    pub fn comment(&self) -> &str {
        self.nodes.string(self.comment)
    }
}

#[cfg(test)]
impl<'a> PartialEq for DirCommentConstructor<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.comment == b.comment) || unimplemented!()
    }
}

#[derive(Debug)]
pub struct DirPIConstructor<'a> {
    nodes: &'a Nodes,
    target: StringId,
    content: StringId,
}

impl<'a> DirPIConstructor<'a> {
    fn new(target: StringId, content: StringId, nodes: &'a Nodes) -> Self {
        Self {
            nodes,
            target,
            content,
        }
    }
    pub fn target(&self) -> QName {
        self.nodes.qnames.qname(
            Prefix(""),
            Namespace(""),
            NCName(self.nodes.string(self.target)),
        )
    }
    pub fn content(&self) -> &str {
        self.nodes.string(self.content)
    }
}

#[cfg(test)]
impl<'a> PartialEq for DirPIConstructor<'a> {
    fn eq(&self, b: &Self) -> bool {
        let ap: *const Nodes = self.nodes;
        let bp: *const Nodes = b.nodes;
        (ap == bp && self.target == b.target && self.content == b.content) || unimplemented!()
    }
}

pub struct VarIterator<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    end: NodeId,
    n: u16,
}

impl<'a> VarIterator<'a> {
    pub(crate) fn new(nodes: &'a Nodes) -> Self {
        let pos = NodeId(1);
        let end = NodeId(0) + nodes.node(NodeId(0)).0;
        Self {
            nodes,
            pos,
            end,
            n: 0,
        }
    }
}

impl<'a> Iterator for VarIterator<'a> {
    type Item = VarDecl<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        while self.pos != self.end {
            let (size, node) = self.nodes.node(self.pos);
            let pos = self.pos;
            self.pos += *size;
            if let Node::VarDecl { external } = node {
                let n = self.n;
                self.n += 1;
                return Some(VarDecl::new(
                    self.nodes,
                    pos,
                    n,
                    *external == VarValueOrExternal::External,
                ));
            }
        }
        None
    }
}

pub struct VarDecl<'a> {
    nodes: &'a Nodes,
    pos: NodeId,
    varid: u16,
    external: bool,
}

impl<'a> VarDecl<'a> {
    fn new(nodes: &'a Nodes, pos: NodeId, varid: u16, external: bool) -> Self {
        Self {
            nodes,
            pos,
            varid,
            external,
        }
    }
    pub fn varid(&self) -> u16 {
        self.varid
    }
    pub fn external(&self) -> bool {
        self.external
    }
    pub fn expr(&self) -> Option<Expr> {
        let total_size = self.nodes.node(self.pos).0;
        if total_size == 0 {
            return None;
        }
        let (size, node) = self.nodes.node(self.pos + 1);
        if matches!(node, Node::SequenceType { .. }) {
            if total_size == size + 1 {
                return None;
            }
            return Some(Expr::new(self.nodes, self.pos + 1 + *size));
        }
        return Some(Expr::new(self.nodes, self.pos + 1));
    }
}
