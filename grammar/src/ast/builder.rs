use crate::{ast::*, grammar::ParsedQName, resolve::FunctionResolver, var_collector::VarCollector};
use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::Arc;
use xust_tree::{
    qnames::{Prefix, PrefixId, QNameCollector, QNameId},
    string_collection::{StringCollection, StringId},
};
use xust_xsd::xpath_error::Error;

struct Module {
    namespace: String,
    base: Option<String>,
    locations: Vec<String>,
    loaded: bool,
}

pub(crate) struct Builder {
    pub(crate) base: Option<String>,
    default_element_namespace: StringId,
    default_function_namespace: StringId,
    num_expressions: usize,
    nodes: Vec<(u32, Node)>,
    stack: Vec<usize>,
    strings: StringCollection,
    errors: Vec<Error>,
    modules: Vec<Module>,
    schema_paths: Vec<PathBuf>,
    pub vars: VarCollector,
    pub qnames: QNameCollector,
    pub reader:
        Arc<dyn Fn(&str, Option<&str>, &[String]) -> Result<(String, Option<String>), Error>>,
}

impl Builder {
    pub fn start_node(&mut self) {
        self.stack.push(self.nodes.len());
        self.nodes.push((0, Node::Nothing));
    }
    pub fn add_node(&mut self, node: Node) {
        self.nodes.push((1, node));
        if self.stack.is_empty() {
            self.num_expressions += 1;
        }
    }
    pub fn add_error(&mut self, error: Error) {
        self.errors.push(error);
    }
    pub fn build(mut self, fd: &dyn FunctionResolver) -> Result<Nodes, Error> {
        assert_eq!(self.stack.len(), 0);
        if let Some(error) = self.errors.first() {
            //eprintln!("Error in build: {:#?}", error);
            return Err(error.clone());
        }
        if self.strings.str().contains('\0') {
            return Err(Error::xqst(90));
        }
        let (translates, strings) = self.strings.sort_and_deduplicate();
        Builder::translate_strings(&mut self.nodes, &translates);

        let namespaces = self.qnames.namespace_context().to_vec();
        let (types, qnames) =
            xust_xsd::load_types(&self.schema_paths, self.qnames).map_err(|_| Error::xxxx(33))?;
        // TODO create constructor functions from simple types from schema

        let functions = collect_functions(&self.nodes);

        resolve_functions(&mut self.nodes, &qnames, fd, &functions)?;
        resolve_global_variables(&qnames, &mut self.nodes, &mut self.vars)?;

        resolve_types(&mut self.nodes, &types)?;
        let var_names = self.vars.var_names();

        Ok(Nodes {
            nodes: self.nodes,
            qnames,
            strings,
            num_expressions: self.num_expressions,
            namespaces,
            var_names,
            main_stack_size: 200,
            static_functions: fd.len(),
            functions,
            types,
        })
    }
    fn translate_strings(nodes: &mut [(u32, Node)], translation: &[StringId]) {
        for n in nodes {
            if let Node::String(ref mut id) = n.1 {
                *id = translation[id.id as usize];
            }
            if let Node::CompPIConstructor(ref mut id) = n.1 {
                *id = translation[id.id as usize];
            }
            if let Node::DirPIConstructor(ref mut a, ref mut b) = n.1 {
                *a = translation[a.id as usize];
                *b = translation[b.id as usize];
            }
            if let Node::DirCommentConstructor(ref mut a) = n.1 {
                *a = translation[a.id as usize];
            }
            if let Node::Lookup(KeySpecifier::NCName(ref mut id)) = n.1 {
                *id = translation[id.id as usize];
            }
            if let Node::AxisStep(_, NodeTest::Localname(ref mut id)) = n.1 {
                *id = translation[id.id as usize];
            }
        }
    }
    // Wrap the expressions from the given position to the end in a new
    // expression with the given node.
    // What does this to to the state of the NodeBuilder?
    //
    pub fn wrap_tail(&mut self, pos: usize, node: Node) {
        self.nodes
            .insert(pos, ((self.nodes.len() - pos + 1) as u32, node));
    }
    pub fn end_node(&mut self, node: Node) {
        let start = self.stack.pop().unwrap();
        self.nodes[start] = ((self.nodes.len() - start) as u32, node);
        if self.stack.is_empty() {
            self.num_expressions += 1;
        }
    }
    pub fn cancel_node(&mut self) {
        self.nodes
            .resize(self.stack.pop().unwrap(), (0, Node::Nothing));
    }
    pub fn next_node(&self) -> NodeId {
        NodeId(self.nodes.len() as u32)
    }
    pub fn eqname(&mut self, namespace: Namespace, prefix: Prefix, local_name: NCName) -> QNameId {
        if namespace == Namespace("http://www.w3.org/2000/xmlns/") {
            self.errors.push(Error::xqst(70));
        }
        self.qnames.add_qname(prefix, namespace, local_name)
    }
    // workaround for declaring and variables until static and dynamic
    // namespaces are separated
    pub fn qname_var(&mut self, q: ParsedQName) -> QNameId {
        match q {
            ParsedQName::Full {
                namespace,
                local_name,
            } => self.eqname(Namespace(&namespace), Prefix(""), local_name),
            ParsedQName::Prefix { prefix, local_name } => {
                if prefix == Prefix("") {
                    self.eqname(Namespace(""), Prefix(""), local_name)
                } else {
                    self.qname(prefix, local_name)
                }
            }
        }
    }
    pub fn qname_(&mut self, q: ParsedQName) -> QNameId {
        match q {
            ParsedQName::Full {
                namespace,
                local_name,
            } => self.eqname(Namespace(&namespace), Prefix(""), local_name),
            ParsedQName::Prefix { prefix, local_name } => self.qname(prefix, local_name),
        }
    }
    pub fn qname_function(&mut self, prefix: Prefix, local_name: NCName) -> QNameId {
        if prefix.is_empty() {
            self.qnames
                .add_qname_by_namespace(self.default_function_namespace, local_name)
        } else {
            self.qname(prefix, local_name)
        }
    }
    pub fn qname_element(&mut self, prefix: Prefix, local_name: NCName) -> QNameId {
        if prefix.is_empty() {
            self.qnames
                .add_qname_by_namespace(self.default_element_namespace, local_name)
        } else {
            self.qname(prefix, local_name)
        }
    }
    /// create a QName, if the prefix is empty, use no namespace
    pub fn qname(&mut self, prefix: Prefix, local_name: NCName) -> QNameId {
        if let Ok(qname) = self.qnames.add_qname_by_prefix(prefix, local_name) {
            return qname;
        }
        if !prefix.is_empty() {
            // if a prefix is not known, log the problem, but do not return
            // an error
            // eprintln!("Namespace unknown for {}:{}", prefix, local_name);
            self.errors.push(Error::xpst(81));
        }
        self.qnames.add_local_qname(local_name)
    }
    pub fn prefix(&mut self, prefix: Prefix) -> PrefixId {
        if let Some(nd) = self.qnames.get_namespace(prefix) {
            nd.prefix
        } else {
            // if a prefix is not known, log the problem, but do not return
            // an error
            self.errors.push(Error::xpst(81));
            self.qnames.empty_prefix()
        }
    }
    pub fn string(&mut self, s: &str) -> StringId {
        self.strings.add_string(s)
    }
    pub fn len(&self) -> usize {
        self.nodes.len()
    }
    pub fn new(ns: &HashMap<String, String>, base_url: Option<String>) -> Result<Self, Error> {
        let mut b = Self {
            base: base_url,
            ..Default::default()
        };
        b.qnames.start_namespace_context();
        for e in ns {
            if e.0 == "xmlns" {
                return Err(Error::xqst(70));
            }
            b.qnames
                .define_namespace(Prefix(e.0), Namespace(e.1))
                .map_err(|_e| Error::xqst(70))?;
        }
        b.qnames.finish_namespace_context();
        Ok(b)
    }
    pub fn add_module_import(&mut self, namespace: String, locations: Vec<String>) {
        if !self.modules.iter().any(|m| m.namespace == namespace) {
            self.modules.push(Module {
                namespace,
                base: self.base.clone(),
                locations,
                loaded: false,
            });
        }
    }
    pub fn get_unloaded_module(&mut self) -> Option<(String, Option<String>, String)> {
        if let Some(m) = self.modules.iter_mut().find(|m| !m.loaded) {
            m.loaded = true;
            match (self.reader)(&m.namespace, m.base.as_deref(), &m.locations) {
                Ok((content, base)) => {
                    return Some((m.namespace.clone(), base, content));
                }
                Err(e) => {
                    self.add_error(e);
                }
            }
        }
        None
    }
    pub fn set_loaded_module(&mut self, namespace: String) {
        if let Some(_m) = self.modules.iter_mut().find(|m| m.namespace == namespace) {}
    }
}

impl Default for Builder {
    fn default() -> Self {
        thread_local! {
            static NS: QNameCollector = {
                let mut q = QNameCollector::no_namespaces();
                q.define_namespace(Prefix(""), Namespace("")).unwrap();
                q.define_namespace(Prefix("xml"), Namespace("http://www.w3.org/XML/1998/namespace")).unwrap();
                q.define_namespace(Prefix("xs"), Namespace("http://www.w3.org/2001/XMLSchema")).unwrap();
                q.define_namespace(Prefix("xsi"), Namespace("http://www.w3.org/2001/XMLSchema-instance")).unwrap();
                q.define_namespace(Prefix("fn"), Namespace("http://www.w3.org/2005/xpath-functions")).unwrap();
                q.define_namespace(Prefix("local"), Namespace("http://www.w3.org/2005/xquery-local-functions")).unwrap();
                q.define_namespace(Prefix("array"), Namespace("http://www.w3.org/2005/xpath-functions/array")).unwrap();
                q.define_namespace(Prefix("map"), Namespace("http://www.w3.org/2005/xpath-functions/map")).unwrap();
                q.define_namespace(Prefix("math"), Namespace("http://www.w3.org/2005/xpath-functions/math")).unwrap();
                q
            };
        }
        let qnames = NS.with(|f| f.clone());
        let empty = qnames.get_namespace(Prefix("")).unwrap().namespace;
        assert_eq!(qnames.get_namespace(Prefix("xmlns")), None);
        Self {
            base: None,
            default_function_namespace: qnames.get_namespace(Prefix("fn")).unwrap().namespace,
            default_element_namespace: empty,
            num_expressions: 0,
            nodes: Vec::new(),
            stack: Vec::new(),
            strings: StringCollection::with_capacity(1024),
            errors: Vec::new(),
            qnames,
            vars: VarCollector::default(),
            reader: Arc::new(default_reader),
            schema_paths: Vec::new(),
            modules: Vec::new(),
        }
    }
}

pub(crate) fn default_reader(
    _uri: &str,
    base: Option<&str>,
    locations: &[String],
) -> Result<(String, Option<String>), Error> {
    let base: Option<PathBuf> = base.map(|b| b.into());
    for l in locations {
        let l = if let Some(base) = &base {
            base.join(l)
        } else {
            PathBuf::from(l)
        };
        if let Ok(content) = std::fs::read_to_string(&l) {
            return Ok((content, l.parent().map(|l| format!("{}", l.display()))));
        }
    }
    Err(Error::xqst(59))
}

#[derive(Debug, Clone)]
pub struct UserFunction {
    qname: QNameId,
    arity: u32,
    node: NodeId,
}

impl UserFunction {
    pub fn qname(&self) -> QNameId {
        self.qname
    }
    pub fn node(&self) -> NodeId {
        self.node
    }
}

fn collect_functions(nodes: &[(u32, Node)]) -> Vec<UserFunction> {
    let mut uf = Vec::new();
    for (i, (_, n)) in nodes.iter().enumerate() {
        if let Node::Function { name, arity, .. } = n {
            uf.push(UserFunction {
                qname: *name,
                arity: *arity,
                node: i.into(),
            })
        }
    }
    uf
}

fn find_function(
    id: usize,
    arity: u32,
    qnames: &QNameCollection,
    fd: &dyn FunctionResolver,
    user_fd: &[UserFunction],
) -> Result<(usize, bool), Error> {
    let name = QNameId::from_usize(id);
    let qname = qnames.to_ref(name);
    let namespace = qname.namespace();
    let local_name = qname.local_name();
    //let check_dynamic = true;
    if let Some(f) = fd.find(namespace, local_name, arity as usize) {
        Ok((f, false))
    } else if let Some(uf) = user_fd
        .iter()
        .find(|fd| fd.qname == name && fd.arity == arity)
    {
        Ok((usize::from(uf.node) + fd.len(), false))
    } else {
        /*
        eprintln!(
            "Cannot find function Q{{{}}}{}#{}",
            namespace, local_name, arity
        );
        */
        Err(Error::xpst(17))
        /*
        let has_dynamic = !check_dynamic || unimplemented!();
        if !has_dynamic {
            return Err(Error::xpst(0017));
        }
        */
    }
}

fn resolve_functions(
    nodes: &mut [(u32, Node)],
    qnames: &QNameCollection,
    fd: &dyn FunctionResolver,
    user_fd: &[UserFunction],
) -> Result<(), Error> {
    for node in nodes {
        match node.1 {
            Node::FunctionCall {
                arity,
                ref mut id,
                ref mut dynamic,
            } => {
                let r = find_function(*id, arity, qnames, fd, user_fd)?;
                *id = r.0;
                *dynamic = r.1;
            }
            Node::ArrowFunctionCall { arity, ref mut id } => {
                // If id == 0, it's dynamic, substract 1 to get back in range
                // unf no Option<u32> was possible, because it bloats Node.
                if *id > 0 {
                    *id = find_function(*id - 1, arity, qnames, fd, user_fd)?.0;
                }
            }
            Node::NamedFunctionRef { arity, ref mut id } => {
                let r = find_function(*id, arity, qnames, fd, user_fd)?;
                *id = r.0;
            }
            _ => {}
        }
    }
    Ok(())
}

fn resolve_global_variables(
    qnames: &QNameCollection,
    nodes: &mut [(u32, Node)],
    vars: &mut VarCollector,
) -> Result<(), Error> {
    for node in nodes {
        match &mut node.1 {
            Node::VarRef(v) => {
                vars.resolve_ref(qnames, v)?;
            }
            _ => {}
        }
    }
    Ok(())
}

fn resolve_types(nodes: &mut [(u32, Node)], types: &Types) -> Result<(), Error> {
    for node in nodes {
        match &mut node.1 {
            Node::ItemType(ItemType::AtomicOrUnionType(v)) => {
                if let Some(id) = types.find_type(QNameId::from_usize(*v)) {
                    *v = usize::from(id);
                } else {
                    return Err(Error::xpst(51));
                }
            }
            _ => {}
        }
    }
    Ok(())
}

#[test]
fn test_empty() {
    let builder = Builder::default();
    let nodes = builder
        .build(&crate::resolve::EMPTY_FUNCTION_RESOLVER)
        .unwrap();
    assert_eq!(nodes.len(), 0);
}

#[test]
fn test_flwor() {
    let mut builder = Builder::default();
    builder.start_node();
    builder.end_node(Node::FLWORExpr(None));
    let nodes = builder
        .build(&crate::resolve::EMPTY_FUNCTION_RESOLVER)
        .unwrap();
    assert_eq!(nodes.len(), 1);
}

#[test]
fn test_flwor_cancel() {
    let mut builder = Builder::default();
    builder.start_node();
    builder.cancel_node();
    let nodes = builder
        .build(&crate::resolve::EMPTY_FUNCTION_RESOLVER)
        .unwrap();
    assert_eq!(nodes.len(), 0);
}
