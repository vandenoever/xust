use xust_tree::qnames::{QNameCollection, QNameCollector, QNameId};
use xust_xsd::xpath_error::Error;
/**
 * VarCollector can register variables in nested scopes.
 * Each registered variable is assigned an increasing number. During execution
 * of the code, this number indicates the position in the stack where
 * the value of the variable is stored.
 *
 * Since variable names are not used during execution, they are collected in
 * a separate QNameCollector.
 */

#[derive(Default)]
pub struct VarCollector {
    global_names: Vec<QNameId>,
    names: Vec<QNameId>,
    unknowns: Vec<QNameId>,
    scopes: Vec<usize>,
}

impl VarCollector {
    pub fn declare_global_variable(
        &mut self,
        qname: QNameId,
        qnames: &QNameCollector,
    ) -> Result<u16, Error> {
        if self.global_names.iter().any(|n| *n == qname) {
            let qname = qnames.to_ref(qname);
            eprintln!(
                "Variable {}:{} is already defined.",
                qname.prefix(),
                qname.local_name(),
            );
            return Err(Error::xqst(49));
        }
        let n = self.global_names.len() as u16;
        self.global_names.push(qname);
        Ok(n)
    }
    pub fn declare_variable(&mut self, qname: QNameId) -> u16 {
        let n = self.names.len() as u16;
        self.names.push(qname);
        n
    }
    fn find(&self, qname: QNameId) -> Option<u16> {
        self.names
            .iter()
            .position(|n| *n == qname)
            .map(|p| p as u16)
    }
    // Return the stack position for the variable. If the variable is not yet
    // known, a special value is returned that should be resolved later.
    // If multiple variables with the same name were registered, return the
    // position of the one that was added last.
    // The request for the variable is recorded, so when the scope ends,
    // it is known what variables are used within a scope.
    pub fn reference_variable(&mut self, qname: QNameId) -> u16 {
        if let Some(pos) = self.find(qname) {
            // already defined
            return pos;
        }
        if let Some(pos) = self
            .unknowns
            .iter()
            .position(|n| *n == qname)
            .map(|p| p as u16)
        {
            // already unknown
            return u16::MAX - pos;
        }
        // new unknown
        let v = u16::MAX - self.unknowns.len() as u16;
        self.unknowns.push(qname);
        v
    }
    pub fn resolve_ref(&mut self, _qnames: &QNameCollection, v: &mut u16) -> Result<(), Error> {
        let offset = (u16::MAX - *v) as usize;
        if offset < self.unknowns.len() {
            let qname = self.unknowns[offset];
            if let Some(pos) = self
                .global_names
                .iter()
                .position(|n| *n == qname)
                .map(|p| p as u16)
            {
                *v = pos;
            } else {
                /*
                let qname = qnames.to_ref(qname);
                eprintln!(
                    "Error resolving variable {}Q{{{}}}:{}.",
                    qname.prefix(),
                    qname.namespace(),
                    qname.local_name()
                );
                */
                return Err(Error::xqst(8));
            }
        } else {
            *v += self.global_names.len() as u16;
        }
        Ok(())
    }
    pub fn start_scope(&mut self) -> ScopeMarker {
        let sm = ScopeMarker(self.scopes.len());
        self.scopes.push(self.names.len());
        sm
    }
    // Leave the scope and return the size of the stack needed for the variables
    // in the scope.
    pub fn leave_scope(&mut self, marker: ScopeMarker) -> u16 {
        self.scopes.pop().unwrap();
        let remaining = self.scopes.last().copied().unwrap_or(0);
        assert_eq!(self.scopes.len(), marker.0);
        let old_len = self.names.len();
        self.names.truncate(remaining);
        (old_len - remaining) as u16
    }
    pub(crate) fn var_names(self) -> Vec<QNameId> {
        self.global_names
    }
}
#[derive(PartialEq, Eq)]
pub struct ScopeMarker(usize);
