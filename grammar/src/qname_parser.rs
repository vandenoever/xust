use crate::grammar::{parse_qname, parse_qname_str, ParsedQName};
use xust_tree::qnames::{Namespace, NamespaceDefinition, Prefix, QName, QNameCollection};
use xust_xsd::xpath_error as error;

pub trait QNameParser {
    fn create_qname(&self, qname: &str, namespaces: &[NamespaceDefinition])
        -> error::Result<QName>;
}

impl QNameParser for QNameCollection {
    fn create_qname(
        &self,
        qname: &str,
        namespaces: &[NamespaceDefinition],
    ) -> error::Result<QName> {
        match parse_qname(qname) {
            None => Err(error::Error::xqdy(74)),
            Some(ParsedQName::Full {
                namespace,
                local_name,
            }) => Ok(self.qname(Prefix(""), Namespace(&namespace), local_name)),
            Some(ParsedQName::Prefix { prefix, local_name }) => {
                if let Some(qname) = self.qname_from_prefix(prefix, local_name, namespaces) {
                    Ok(qname)
                } else {
                    // prefix was not defined
                    Err(error::Error::xqdy(74))
                }
            }
        }
    }
}

pub fn create_qname(
    qnames: &QNameCollection,
    namespace: Namespace,
    qname: &str,
) -> error::Result<QName> {
    let (prefix, local_name) = parse_qname_str(qname)?;
    Ok(qnames.qname(prefix, namespace, local_name))
}
