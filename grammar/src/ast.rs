#![allow(clippy::upper_case_acronyms, clippy::enum_variant_names)]
pub mod builder;
pub mod proxy;

use crate::model::{
    Axis, Comp, GreatestOrLeast, OrderingMode, SomeOrEvery, SortDirection, VarValueOrExternal, F64,
    I64,
};
use proxy::{Expr, ExprIterator};
use rust_decimal::Decimal;
use xust_tree::{
    qnames::{NCName, Namespace, NamespaceDefinition, QNameCollection, QNameId, QNameRef},
    string_collection::{StringCollection, StringId},
};
use xust_xsd::types::Types;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd)]
pub struct NodeId(u32);

impl std::ops::Add<u32> for NodeId {
    type Output = NodeId;
    fn add(self, n: u32) -> Self {
        NodeId(self.0 + n)
    }
}

impl std::ops::AddAssign<u32> for NodeId {
    fn add_assign(&mut self, n: u32) {
        self.0 += n;
    }
}

impl From<NodeId> for usize {
    fn from(n: NodeId) -> usize {
        n.0 as usize
    }
}

impl From<usize> for NodeId {
    fn from(n: usize) -> NodeId {
        NodeId(n as u32)
    }
}

// Rules for the proxy structures: stepping through is lazy, but accessing
// resolves any fields up to the iterators.
// Indexing into node does bounds checks, so it's good limit doing that.

#[derive(Debug, Clone)]
pub(crate) struct Nodes {
    nodes: Vec<(u32, Node)>,
    qnames: QNameCollection,
    strings: StringCollection,
    num_expressions: usize,
    namespaces: Vec<NamespaceDefinition>,
    var_names: Vec<QNameId>,
    pub main_stack_size: u16,
    static_functions: usize,
    functions: Vec<builder::UserFunction>,
    pub types: Types,
}

impl Nodes {
    fn start(&self) -> NodeId {
        NodeId(0)
    }
    fn node(&self, pos: NodeId) -> &(u32, Node) {
        if self.nodes.len() <= pos.0 as usize {
            println!("{:#?}", self);
        }
        &self.nodes[pos.0 as usize]
    }
    pub fn qname(&self, name: QNameId) -> QNameRef {
        self.qnames.get(name)
    }
    pub fn qnames(&self) -> &QNameCollection {
        &self.qnames
    }
    pub fn expr(&self) -> proxy::Expr {
        proxy::Expr::new(self, NodeId(0))
    }
    fn string(&self, id: StringId) -> &str {
        self.strings.get(id)
    }
    pub fn namespace_definitions(&self) -> &[NamespaceDefinition] {
        &self.namespaces
    }
    #[cfg(test)]
    pub fn len(&self) -> usize {
        self.num_expressions
    }
    pub fn var_names(&self) -> &Vec<QNameId> {
        &self.var_names
    }
    pub fn print_nodes(&self) {
        eprintln!("{:#?}", self.nodes);
    }
}

impl<'a> IntoIterator for &'a Nodes {
    type Item = Expr<'a>;
    type IntoIter = ExprIterator<'a>;
    fn into_iter(self) -> ExprIterator<'a> {
        ExprIterator::new(self, self.start())
    }
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum Node {
    QueryBody,
    VarDecl {
        external: VarValueOrExternal,
    },
    ContextItem,
    Nothing,
    // Expr
    BinaryOp(BinaryOp),
    FLWORExpr(Option<NodeId>),
    LetBinding(u16, bool),
    WindowClause(WindowClause),
    WindowEndCondition,
    WindowConditionCurrentItem(u16),
    WindowConditionPositionalVar(u16),
    WindowConditionPreviousItem(u16),
    WindowConditionNextItem(u16),
    WhereClause,
    GroupByClause,
    GroupingSpec {
        variable: u16,
        collation: Option<StringId>,
    },
    OrderByClause {
        stable: bool,
    },
    OrderSpec(OrderSpec),
    CountClause(u16),
    ReturnClause,
    QuantifiedExpr(SomeOrEvery),
    QuantifiedExprLine(u16),
    SwitchExpr,                  // switch, cases, default
    SwitchCaseClause,            // { values, value }))
    TypeswitchExpr(Option<u16>), // switch, cases, varname, default,
    CaseClause(Option<u16>),     // varname, types, value
    IfExpr,                      // if, then, else
    TryCatchExpr,
    CatchClause,
    Function {
        name: QNameId,
        arity: u32,
        stack_size: u16,
    },
    FunctionCall {
        id: usize,
        arity: u32,
        dynamic: bool,
    },
    ArrowFunctionCall {
        // defined when arrow function is static
        id: usize,
        arity: u32,
    },
    NamedFunctionRef {
        id: usize,
        arity: u32,
    },
    VarRef(u16),
    ContextItemExpr,
    ParenthesizedExpr,
    Negate,
    Postfix,
    // Literal(Literal),
    String(StringId),
    AnyURI(StringId),
    Integer(I64),     // xs:integer
    Decimal(Decimal), // xs:decimal
    Float(f32),       // xs:float
    Double(F64),      // xs:double
    // Other
    ForBinding(ForBinding),
    SequenceType {
        occurrence_indicator: OccurrenceIndicator,
    },
    ItemType(ItemType),
    SingleType {
        optional: bool,
        single_type: QNameId,
    },
    ElementOrAttributeTest(ElementOrAttributeTest),
    ArgumentPlaceholder,
    AxisStep(Axis, NodeTest),
    KindTest(KindTest),
    Annotation(QNameId),
    Lookup(KeySpecifier),
    Predicate,
    PostfixArgumentList,
    SquareArrayConstructor,
    CurlyArrayConstructor,
    CompDocConstructor,
    CompTextConstructor,
    CompCommentConstructor,
    CompPIConstructor(StringId),
    CompElemConstructor(Option<QNameId>),
    CompAttrConstructor(Option<QNameId>),
    DirElemConstructor(QNameId, bool),
    DirCommentConstructor(StringId),
    DirPIConstructor(StringId, StringId),
    DirAttribute(QNameId),
    StringConstructor,
    MapConstructor,
    Ordering(OrderingMode),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BinaryOp {
    Comma,
    Or,
    And,
    Comparison(Comp),
    StringConcat,
    Range,
    Add,
    Substract,
    Multiply,
    Div,
    IDiv,
    Mod,
    Union,
    Intersect,
    Except,
    SimpleMap,
    Step,
    Instanceof,
    Treat,
    Castable,
    Cast,
}

#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub enum OccurrenceIndicator {
    Zero,       // empty-sequence()
    One,        // ''
    ZeroOrOne,  // ?
    ZeroOrMore, // *
    OneOrMore,  // +
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum ItemType {
    KindTest, //(KindTest),
    Item,
    FunctionTest, /*(
                      Vec<Annotation>,
                      Option<(Vec<SequenceType>, Box<SequenceType>)>,
                  )*/
    AnyMapTest,
    MapTest(QNameId), //(Option<(QName, Box<SequenceType>)>),
    ArrayTest,        //(Option<Box<SequenceType>>),
    AtomicOrUnionType(usize),
    //ParenthesizedItemType, //(Box<ItemType>),
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct ForBinding {
    pub var: u16,
    pub allowing_empty: bool,
    pub has_type_declaration: bool,
    pub positional_var: Option<u16>,
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum KindTest {
    DocumentTestNone,
    DocumentTestElementTest {
        match_nilled: bool,
        match_name: bool,
        match_type: bool,
    },
    DocumentTestSchemaElementTest(QNameId),
    ElementTest {
        match_nilled: bool,
        match_name: bool,
        match_type: bool,
    },
    AttributeTest {
        match_name: bool,
        match_type: bool,
    },
    SchemaElementTest(QNameId),
    SchemaAttributeTest(QNameId),
    PITestNone,
    PITestNCName(StringId),
    PITestString(StringId),
    CommentTest,
    TextTest,
    NamespaceNodeTest,
    AnyKindTest,
}
#[derive(Debug, Clone, PartialEq)]
pub(crate) struct ElementOrAttributeTest {
    pub name: QNameId,
    pub r#type: QNameId,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum NodeTest {
    KindTest,
    NS(StringId),
    // NSPrefix(PrefixId),
    Localname(StringId),
    Wildcard,
    QName(QNameId),
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum KeySpecifier {
    NCName(StringId),
    Integer(u32),
    Wildcard,
    Expr,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) struct WindowClause {
    pub varid: u16,
    pub tumbling: bool,
    pub has_type_declaration: bool,
    pub has_end: bool,
    pub only: bool,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct OrderSpec {
    pub direction: Option<SortDirection>,
    pub empty: Option<GreatestOrLeast>,
    pub collation: Option<StringId>,
}

#[cfg(test)]
macro_rules! print_sizes {
    ( $( $t:ty )* ) => {
        $( println!("sizeof {}\t{}", stringify!($t), size_of::<$t>()); )*
    }
}
#[test]
fn print_a_sizes() {
    use crate::error::{Error, Result};
    use std::mem::size_of;
    use std::rc::Rc;
    struct T(u32, Node);
    struct T3(T, T, T);
    println!("===");
    print_sizes!(
        T3 T Node ElementOrAttributeTest ItemType KindTest Expr
        String Vec<u8> Box<str> Box<[u8]> Rc<u8> Box<u8> usize
        Error Result<()>
    );
    println!("===");
    assert!(size_of::<Node>() <= 24);
    assert!(size_of::<Expr>() <= 40);
    assert!(size_of::<Error>() <= 24);
}
