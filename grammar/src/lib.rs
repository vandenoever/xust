#![allow(dead_code)]
pub mod ast;
pub mod grammar;
mod model;
pub mod qname_parser;
pub mod resolve;
mod var_collector;
use xust_xsd::xpath_error as error;

use ast::{builder::Builder, Node, Nodes};
use grammar::main_module;
use std::collections::HashMap;
use std::sync::Arc;
use xust_tree::qnames::{NamespaceDefinition, QNameCollection, QNameId};
use xust_xsd::types::Types;

pub use ast::{
    proxy::{
        ArrowFunctionCall, AxisStep, Binary, CompConstructor, CompPIConstructor,
        DirCommentConstructor, DirElemConstructor, DirPIConstructor, ElementTest, Expr,
        ExprIterator, ExprRef, Function, FunctionCall, If, ItemType, KindTest, NameOrExpression,
        NodeTest, Postfix, PostfixItem, SequenceType, String as XString, VarRef,
    },
    BinaryOp, OccurrenceIndicator,
};
pub use model::{Axis, Comp};
pub use resolve::FunctionResolver;

#[derive(Debug, Clone)]
pub struct XQuery {
    nodes: Arc<Nodes>,
}

impl XQuery {
    pub fn query_body(&self) -> Expr {
        self.nodes.expr()
    }
    pub fn qnames(&self) -> &QNameCollection {
        self.nodes.qnames()
    }
    pub fn namespace_definitions(&self) -> &[NamespaceDefinition] {
        self.nodes.namespace_definitions()
    }
    pub fn global_variables(&self) -> ast::proxy::VarIterator {
        // self.nodes.print_nodes();
        ast::proxy::VarIterator::new(&self.nodes)
    }
    pub fn var_names(&self) -> &[QNameId] {
        self.nodes.var_names()
    }
    pub fn types(&self) -> &Types {
        &self.nodes.types
    }
    pub fn main_stack_size(&self) -> usize {
        self.nodes.main_stack_size as usize
    }
}

pub struct ParseInit<'a> {
    pub fd: &'a dyn FunctionResolver,
    pub namespaces: &'a HashMap<String, String>,
    pub reader: Arc<
        dyn Fn(&str, Option<&str>, &[String]) -> Result<(String, Option<String>), error::Error>,
    >,
    pub print_position: bool,
    pub base_url: Option<String>,
}

impl<'a> Default for ParseInit<'a> {
    fn default() -> Self {
        lazy_static::lazy_static! {
            static ref EMPTY_NAMESPACES: HashMap<String, String> = HashMap::new();
        }
        Self {
            fd: &crate::resolve::EMPTY_FUNCTION_RESOLVER,
            namespaces: &EMPTY_NAMESPACES,
            reader: Arc::new(ast::builder::default_reader),
            print_position: false,
            base_url: None,
        }
    }
}

pub fn parse(s: &str, init: ParseInit) -> error::Result<XQuery> {
    let mut builder =
        Builder::new(init.namespaces, init.base_url).map_err(|_| error::Error::xqst(70))?;
    builder.reader = init.reader.clone();

    // skip prolog for now
    builder.start_node();
    let print_position = init.print_position;
    let (i, _module) = main_module(&mut builder, s).map_err(|e| {
        if print_position {
            print_error_position(s, &e);
        }
        error::Error::xpst(3)
    })?;
    builder.end_node(Node::QueryBody);

    if !i.is_empty() {
        // eprintln!("Not all of the query was parsed. Remaining: '{}'.", i);
        return Err(error::Error::xpst(3));
    }
    Ok(XQuery {
        nodes: Arc::new(builder.build(init.fd)?),
    })
}

fn print_error_position(s: &str, e: &nom::Err<nom::error::Error<&str>>) {
    if let Some((line, column, fragment)) = get_error_position(s, e) {
        eprintln!("Error parsing {}:{}:\n  {}", line, column, fragment);
        eprintln!("{:indent$}^", "", indent = column + 2);
    }
}

fn get_error_position<'a>(
    all: &'a str,
    error: &nom::Err<nom::error::Error<&str>>,
) -> Option<(usize, usize, &'a str)> {
    let after_error = match error {
        nom::Err::Incomplete(_) => return None,
        nom::Err::Error(e) | nom::Err::Failure(e) => e.input,
    };
    let before_error = &all[..all.len() - after_error.len()];
    let line = 1 + before_error
        .as_bytes()
        .iter()
        .filter(|&&c| c == b'\n')
        .count();
    let line_start = before_error.rfind('\n').unwrap_or_default();
    let line_end = after_error.find('\n').unwrap_or(after_error.len());
    let column = before_error.len() - line_start;
    let start = usize::min(all.len(), line_start + 1);
    let end = usize::min(all.len(), before_error.len() + line_end);
    let fragment = &all[start..end];
    Some((line, column, fragment))
}

#[cfg(test)]
struct FunctionDefinition<'a> {
    namespace: xust_tree::qnames::Namespace<'a>,
    local_name: xust_tree::qnames::NCName<'a>,
    min_arity: usize,
    max_arity: usize,
}

#[cfg(test)]
impl<'a> FunctionResolver for Vec<FunctionDefinition<'a>> {
    fn find(
        &self,
        namespace: xust_tree::qnames::Namespace,
        local_name: xust_tree::qnames::NCName,
        arity: usize,
    ) -> Option<usize> {
        self.iter().position(|f| {
            f.min_arity <= arity
                && arity <= f.max_arity
                && local_name == f.local_name
                && namespace == f.namespace
        })
    }
    fn len(&self) -> usize {
        self.len()
    }
}

#[cfg(test)]
fn parse_test(s: &str) -> XQuery {
    use xust_tree::qnames::{NCName, Namespace};
    let mut init = ParseInit::default();
    let fd = vec![FunctionDefinition {
        namespace: Namespace("http://www.w3.org/2005/xpath-functions"),
        local_name: NCName("root"),
        min_arity: 0,
        max_arity: 1,
    }];
    init.fd = &fd;
    parse(s, init).unwrap()
}
#[test]
fn zero() {
    let _xquery = parse_test("0");
    //    println!("{:#?}", xquery);
}
#[test]
fn addition() {
    let _xquery = parse_test("1+2");
    //    println!("{:#?}", xquery);
    let _xquery = parse_test("1+2+3");
    //    println!("{:#?}", xquery);
}
#[test]
fn add_and_multiply() {
    use ast::BinaryOp;
    let xquery = parse_test("1+2*3");
    assert_eq!(xquery.nodes.len(), 1);
    let body = xquery.nodes.expr();
    if let Expr::BinaryOp(o) = body {
        assert_eq!(o.op(), BinaryOp::Add);
        assert_eq!(o.first(), Expr::Integer(1));
        if let Expr::BinaryOp(o) = o.second() {
            assert_eq!(o.op(), BinaryOp::Multiply);
            assert_eq!(o.first(), Expr::Integer(2));
            assert_eq!(o.second(), Expr::Integer(3));
        } else {
            panic!()
        }
    } else {
        panic!();
    }
    let _xquery = parse_test("1*2+3");
    //    println!("{:#?}", xquery);
}
#[test]
fn string() {
    let xquery = parse_test("'abc'");
    assert_eq!(xquery.nodes.len(), 1);
    let body = xquery.nodes.expr();
    if let Expr::String(s) = body {
        assert_eq!(s.str(), "abc");
    } else {
        panic!();
    }
}
#[test]
fn root() {
    let xquery = parse_test("/.");
    assert_eq!(xquery.nodes.len(), 1);
    let body = xquery.nodes.expr();
    if let Expr::BinaryOp(op) = body {
        assert_eq!(op.op(), BinaryOp::Step);
    } else {
        panic!("{:?}", body);
    }
}
#[test]
fn test_square_array_constructor() {
    let xquery = parse_test("[]");
    assert_eq!(xquery.nodes.len(), 1);
    let body = xquery.nodes.expr();
    if let Expr::SquareArrayConstructor(c) = body {
        assert!(c.is_empty());
    } else {
        panic!("{:?}", body);
    }
}
#[test]
fn test_curly_array_constructor() {
    let xquery = parse_test("array{}");
    assert_eq!(xquery.nodes.len(), 1);
    let body = xquery.nodes.expr();
    if let Expr::CurlyArrayConstructor(c) = body {
        assert!(c.is_empty());
    } else {
        panic!("{:?}", body);
    }
}
#[test]
fn test_element_test() {
    let xquery = parse_test("element(Root)");
    assert_eq!(xquery.nodes.len(), 1);
    let _body = xquery.nodes.expr();
    println!("{:#?}", xquery);
}
#[test]
fn test_xs_integer_instance() {
    let xquery = parse_test(". instance of xs:integer");
    assert_eq!(xquery.nodes.len(), 1);
    let _body = xquery.nodes.expr();
    println!("{:#?}", xquery);
}
