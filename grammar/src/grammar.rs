use super::{
    ast::{
        builder::Builder as B, BinaryOp, ElementOrAttributeTest, ForBinding, ItemType,
        KeySpecifier, KindTest, Node, NodeTest, OccurrenceIndicator, OrderSpec, WindowClause,
    },
    model::{
        Annotation, Axis, Comp, CopyNamespacesMode, DFPropertyName, GreatestOrLeast,
        IntersectExcept, LibraryOrMain, Module, ModuleDecl, Multiplicative, OrderingMode,
        PlusMinus, Prolog, SomeOrEvery, SortDirection, StripOrPreserve, URILiteral,
        VarValueOrExternal, VersionDecl, F64, I64,
    },
};
use nom::{
    branch::alt,
    bytes::complete::{
        tag, tag_no_case, take, take_till, take_until, take_while, take_while1, take_while_m_n,
    },
    character::complete::{anychar, char, digit0, digit1, hex_digit1, multispace0, one_of},
    combinator::{map, map_res, not, opt, recognize},
    multi::{fold_many0, many0, separated_list1},
    sequence::{delimited, preceded, terminated, tuple},
};
use std::borrow::Cow;
use std::collections::BTreeMap;
use xust_tree::{
    qnames::{NCName, Namespace, Prefix, QNameId},
    string_collection::StringId,
};
use xust_xsd::xpath_error::Error;

type IResult<'a, T> = nom::IResult<&'a str, T>;
type R<'a> = Result<&'a str, nom::Err<nom::error::Error<&'a str>>>;

trait CancelNode<T> {
    fn or_cancel(self, b: &mut B) -> T;
}

impl<'a, T> CancelNode<IResult<'a, T>> for IResult<'a, T> {
    fn or_cancel(self, b: &mut B) -> IResult<'a, T> {
        self.map_err(|e| {
            b.cancel_node();
            e
        })
    }
}

impl<'a> CancelNode<R<'a>> for R<'a> {
    fn or_cancel(self, b: &mut B) -> R<'a> {
        self.map_err(|e| {
            b.cancel_node();
            e
        })
    }
}

fn close_comment(mut i: &str) -> R {
    let mut depth = 1usize;
    loop {
        let (j, _) = take_while(|c| c != ':' && c != '(')(i)?;
        i = if let Ok((j, _)) = tag::<_, _, ()>("(:")(j) {
            depth += 1;
            j
        } else if let Ok((j, _)) = tag::<_, _, ()>(":)")(j) {
            depth -= 1;
            if depth == 0 {
                return Ok(j);
            }
            j
        } else {
            let (j, _) = take(1usize)(j)?;
            j
        }
    }
}

/// whitespace that may contain nested comments
fn tws(mut i: &str) -> IResult<()> {
    loop {
        // remove " \t\n\r"
        let (j, _) = multispace0(i)?;
        if let Ok((j, _)) = tag::<_, _, ()>("(:")(j) {
            i = close_comment(j)?;
        } else {
            return Ok((j, ()));
        }
    }
}

fn s(i: &str) -> IResult<()> {
    // remove " \t\n\r"
    let i = multispace0(i)?.0;
    Ok((i, ()))
}

// [1]    	Module 	   ::=    	VersionDecl? (LibraryModule | MainModule)
// [3]    	MainModule 	   ::=    	Prolog QueryBody
pub(crate) fn main_module<'a>(b: &mut B, i: &'a str) -> IResult<'a, Module<'a>> {
    let (i, (version, _)) = tuple((opt(version_decl), tws))(i)?;
    let (i, module) = {
        if module_decl(i).is_ok() {
            // error: this is a library module, not a main module
            return Err(nom::Err::Error(nom::error::make_error(
                "",
                nom::error::ErrorKind::Fail,
            )));
        }
        let ns = b.qnames.start_namespace_context();
        let (i, prolog) = prolog(b, i).map_err(|e| {
            b.qnames.leave_namespace_context(ns);
            e
        })?;
        library_modules(b).map_err(|e| {
            b.qnames.leave_namespace_context(ns);
            e
        })?;
        let r = expr(b, i);
        b.qnames.leave_namespace_context(ns);
        let i = r?;
        (i, LibraryOrMain::MainModule(prolog))
    };
    Ok((i, Module { version, module }))
}
// [4]    	LibraryModule 	   ::=    	ModuleDecl Prolog
fn library_modules<'a>(b: &mut B) -> R<'a> {
    while let Some((ns, base, text)) = b.get_unloaded_module() {
        b.base = base;
        match library_module(b, &text) {
            Ok(_) => {
                b.set_loaded_module(ns);
            }
            Err(e) => {
                super::print_error_position(&text, &e);
                return Err(nom::Err::Error(nom::error::make_error(
                    "",
                    nom::error::ErrorKind::Fail,
                )));
            }
        }
    }
    Ok("")
}
fn library_module<'a>(b: &mut B, i: &'a str) -> IResult<'a, Module<'a>> {
    let (i, (version, _)) = tuple((opt(version_decl), tws))(i)?;
    let (i, decl) = module_decl(i)?;
    let ns = b.qnames.start_namespace_context();
    define_namespace(b, decl.name, &decl.uri);
    let r = prolog(b, i);
    b.qnames.leave_namespace_context(ns);
    let (i, prolog) = r?;
    let module = LibraryOrMain::LibraryModule(decl, prolog);
    Ok((i, Module { version, module }))
}

fn tag_s<'a>(t: &str, i: &'a str) -> R<'a> {
    Ok(tuple((tag(t), tws))(i)?.0)
}

fn tag_s_tag_s<'a>(t1: &str, t2: &str, i: &'a str) -> R<'a> {
    Ok(tuple((tag(t1), tws, tag(t2), tws))(i)?.0)
}

fn s_tag_s<'a>(t: &str, i: &'a str) -> R<'a> {
    Ok(tuple((tws, tag(t), tws))(i)?.0)
}

fn char_s(c: char, i: &str) -> R {
    Ok(tuple((char(c), tws))(i)?.0)
}

fn s_char(c: char, i: &str) -> R {
    Ok(tuple((tws, char(c)))(i)?.0)
}

fn s_char_s(c: char, i: &str) -> R {
    Ok(tuple((tws, char(c), tws))(i)?.0)
}

// [2]    	VersionDecl 	   ::=    	"xquery" (("encoding" StringLiteral) | ("version" StringLiteral ("encoding" StringLiteral)?)) Separator
fn version_decl(i: &str) -> IResult<VersionDecl> {
    let i = tag_s("xquery", i)?;
    let (i, version) = opt(preceded(tuple((tag("version"), tws)), string_literal))(i)?;
    let i = tws(i)?.0;
    let (i, encoding) = opt(preceded(tuple((tag("encoding"), tws)), string_literal))(i)?;
    let i = s_char(';', i)?;
    Ok((
        i,
        VersionDecl {
            version: version.map(|v| v.to_string()),
            encoding: encoding.map(|v| v.to_string()),
        },
    ))
}

fn parse_module<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, (_version, _)) = tuple((opt(version_decl), tws))(i)?;
    let i = module_decl(i)?.0;
    if !i.is_empty() {
        eprintln!("Not all of the query was parsed. Remaining: '{}'.", i);
        b.add_error(Error::xpst(3));
    }
    Ok(i)
}

// [5]    	ModuleDecl 	   ::=    	"module" "namespace" NCName "=" URILiteral Separator
fn module_decl(code: &str) -> IResult<ModuleDecl> {
    let i = tag_s_tag_s("module", "namespace", code)?;
    let (i, name) = ncname_str(i)?;
    let i = s_char_s('=', i)?;
    let (i, uri) = string_literal(i)?;
    let i = tws(i)?.0;
    let i = char_s(';', i)?;
    Ok((i, ModuleDecl { name, uri }))
}

// [6]    	Prolog 	   ::=    	((DefaultNamespaceDecl | Setter | NamespaceDecl | Import) Separator)* ((ContextItemDecl | AnnotatedDecl | OptionDecl) Separator)*
pub(crate) fn prolog<'a>(b: &mut B, mut i: &'a str) -> IResult<'a, Prolog> {
    let mut prolog = Prolog::default();
    loop {
        i = if let Ok((j, (_, _, t, _))) = tuple((
            tag("declare"),
            tws,
            take_while1(|c: char| c.is_ascii_lowercase() || c == '-'),
            tws,
        ))(i)
        {
            // "default" ("element" | "function" | "collation" | "order"
            //   | "decimal-format")
            let j = if t == "default" {
                let (j, t) = terminated(
                    take_while1(|c: char| c.is_ascii_lowercase() || c == '-'),
                    tws,
                )(j)?;
                if t == "element" {
                    let j = tag_s("namespace", j)?;
                    let (j, ns) = string_literal(j)?;
                    prolog.default_element_namespaces = Some(URILiteral(b.string(&ns)));
                    j
                } else if t == "function" {
                    let j = tag_s("namespace", j)?;
                    let (j, ns) = string_literal(j)?;
                    prolog.default_function_namespaces = Some(URILiteral(b.string(&ns)));
                    j
                } else if t == "collation" {
                    let (j, ns) = string_literal(j)?;
                    prolog.default_collation = Some(URILiteral(b.string(&ns)));
                    j
                } else if t == "order" {
                    let j = tag_s("empty", j)?;
                    let (j, gl) = greatest_or_least(j)?;
                    prolog.empty_order = Some(gl);
                    j
                } else if t == "decimal-format" {
                    let (j, _df) = decimal_format_decl(j)?;
                    // prolog.decimal_formats.insert(None, df);
                    j
                } else {
                    break;
                }
            } else if t == "boundary-space" {
                // "boundary-space" | "base-uri" | "construction" | "ordering"
                // "copy-namespaces" | "decimal-format" | "namespace"
                let (j, bs) = strip_or_preserve(j)?;
                prolog.boundary_space_policy = Some(bs);
                j
            } else if t == "base-uri" {
                let (j, _) = tws(j)?;
                let (j, base_uri) = string_literal(j)?;
                prolog.base_uri = Some(URILiteral(b.string(&base_uri)));
                j
            } else if t == "construction" {
                let (j, c) = strip_or_preserve(j)?;
                prolog.construction_mode = Some(c);
                j
            } else if t == "ordering" {
                let (j, om) = ordering_mode(j)?;
                prolog.ordering_mode = Some(om);
                j
            } else if t == "copy-namespaces" {
                let (j, cn) = copy_namespaces_decl(j)?;
                prolog.copy_namespaces_mode = Some(cn);
                j
            } else if t == "decimal-format" {
                let (j, _name) = eqname(b, j)?;
                let (j, _) = tws(j)?;
                let (j, _df) = decimal_format_decl(j)?;
                // prolog.decimal_formats.insert(Some(name), df);
                j
            } else if t == "namespace" {
                namespace_decl(b, j)?
            } else {
                break;
            };
            s_char_s(';', j)?
        } else if let Ok((j, _)) = tuple((tag("import"), tws))(i) {
            let j = if let Ok((j, (ns, si))) = schema_import(b, j) {
                prolog.schema_imports.insert(ns, si);
                j
            } else {
                let (j, (ns, mi)) = module_import(b, j)?;
                prolog.module_imports.insert(ns, mi);
                j
            };
            s_char_s(';', j)?
        } else {
            break;
        };
    }
    b.qnames.finish_namespace_context();
    loop {
        i = if let Ok((j, _)) = tuple((tag("declare"), tws))(i) {
            let j = annotations(b, j)?;
            let annotations = Vec::new();
            let j = if let Ok((j, _)) = tag::<_, _, ()>("function")(j) {
                function_decl(b, j, annotations)?
                // prolog.functions.insert(name, f);
            } else if let Ok((j, _)) = tag::<_, _, ()>("variable")(j) {
                var_decl(b, j, annotations)?
            } else if let Ok((j, _)) = tag::<_, _, ()>("option")(j) {
                // let (j, (_, n, _, v)) = tuple((tws, eqname, tws, string_literal))(j)?;
                // prolog.options.insert(n, v);
                j
            } else {
                let (j, _) = tag("context")(j)?;
                // prolog.context_item = Some(ci);
                context_item_decl(b, j)?
            };
            let j = s_char_s(';', j)?;
            if j.is_empty() {
                i = j;
                break;
            }
            j
        } else {
            break;
        };
    }
    Ok((i, prolog))
}

macro_rules! tag_enum {
    ( $enum:ident $fn:ident $s1:expr => $v1:ident $( $s:expr => $v:ident )*) => {
        fn $fn(i: &str) -> IResult<$enum> {
            $(
                if let Ok((i, _)) = tag::<_,_,()>($s)(i) {
                    Ok((i, $enum::$v))
                } else
            )*
            {map(tag($s1),|_| $enum::$v1)(i)}
        }
    };
}
tag_enum!(StripOrPreserve strip_or_preserve
    "strip" => Strip
    "preserve" => Preserve
);
tag_enum!(OrderingMode ordering_mode
    "ordered" => Ordered
    "unordered" => Unordered
);
tag_enum!(GreatestOrLeast greatest_or_least
    "greatest" => Greatest
    "least" => Least
);
tag_enum!(DFPropertyName dfproperty_name
    "decimal-separator"=> DecimalSeparator
    "grouping-separator"=> GroupingSeparator
    "infinity"=> Infinity
    "minus-sign"=> MinusSign
    "NaN"=> NaN
    "percent"=> Percent
    "per-mille"=> PerMille
    "zero-digit"=> ZeroDigit
    "digit"=> Digit
    "pattern-separator"=> PatternSeparator
    "exponent-separator" => ExponentSeparator
);

// [15]    	CopyNamespacesDecl 	   ::=    	"declare" "copy-namespaces" PreserveMode "," InheritMode
// [16]    	PreserveMode 	   ::=    	"preserve" | "no-preserve"
// [17]    	InheritMode 	   ::=    	"inherit" | "no-inherit"
fn copy_namespaces_decl(i: &str) -> IResult<CopyNamespacesMode> {
    let (i, preserve) = if let Ok((i, _)) = tag::<_, _, ()>("preserve")(i) {
        (i, true)
    } else {
        let (i, _) = tag("no-preserve")(i)?;
        (i, false)
    };
    let i = s_char_s(',', i)?;
    let (i, inherit) = if let Ok((i, _)) = tag::<_, _, ()>("inherit")(i) {
        (i, true)
    } else {
        let (i, _) = tag("no-inherit")(i)?;
        (i, false)
    };
    Ok((i, CopyNamespacesMode { preserve, inherit }))
}
// [18]    	DecimalFormatDecl 	   ::=    	"declare" (("decimal-format" EQName) | ("default" "decimal-format")) (DFPropertyName "=" StringLiteral)*
fn decimal_format_decl(i: &str) -> IResult<BTreeMap<DFPropertyName, String>> {
    let (i, v) = many0(map(
        tuple((dfproperty_name, tws, char('='), tws, string_literal, tws)),
        |t| (t.0, t.4),
    ))(i)?;
    Ok((i, v.into_iter().map(|(k, v)| (k, v.to_string())).collect()))
}
// [21]    	SchemaImport 	   ::=    	"import" "schema" SchemaPrefix? URILiteral ("at" URILiteral ("," URILiteral)*)?
fn schema_import<'a>(
    b: &mut B,
    i: &'a str,
) -> IResult<'a, (Option<StringId>, (URILiteral, Vec<URILiteral>))> {
    let (i, _) = tuple((tag("schema"), tws))(i)?;
    let (i, prefix) = opt(schema_prefix)(i)?;
    let prefix = prefix.flatten().map(|p| b.string(p.0));
    let (i, _) = tws(i)?;
    let (i, uri) = string_literal(i)?;
    let (i, locations) = opt(preceded(
        tuple((tws, tag("at"), tws)),
        separated_list1(
            tuple((tws, char(','), tws)),
            map(string_literal, |s| URILiteral(b.string(&s))),
        ),
    ))(i)?;
    Ok((
        i,
        (
            prefix,
            (URILiteral(b.string(&uri)), locations.unwrap_or_default()),
        ),
    ))
}
// [22]    	SchemaPrefix 	   ::=    	("namespace" NCName "=") | ("default" "element" "namespace")
fn schema_prefix(i: &str) -> IResult<Option<NCName>> {
    alt((
        map(
            tuple((tag("namespace"), tws, ncname_str, tws, char('='))),
            |t| Some(t.2),
        ),
        map(
            tuple((tag("default"), tws, tag("element"), tws, tag("namespace"))),
            |_| None,
        ),
    ))(i)
}
// [23]    	ModuleImport 	   ::=    	"import" "module" ("namespace" NCName "=")? URILiteral ("at" URILiteral ("," URILiteral)*)?
fn module_import<'a>(b: &mut B, i: &'a str) -> IResult<'a, (Option<StringId>, URILiteral)> {
    let (i, _) = tuple((tag("module"), tws))(i)?;
    let (i, prefix) = opt(map(
        tuple((tag("namespace"), tws, ncname_str, tws, char('='), tws)),
        |t| t.2,
    ))(i)?;
    let (i, uri) = string_literal(i)?;
    let (i, _) = tws(i)?;
    let (i, locations) = opt(preceded(tag("at"), many0(preceded(tws, string_literal))))(i)?;
    let locs = locations.iter().flatten().map(|s| s.to_string()).collect();
    b.add_module_import(uri.to_string(), locs);
    if let Some(prefix) = prefix {
        if let Err(e) = b.qnames.define_namespace(Prefix(prefix.0), Namespace(&uri)) {
            let e = match e {
                xust_tree::qnames::Error::AlreadyDefinedPrefix => Error::xqst(33),
                _ => Error::xxxx(43),
            };
            b.add_error(e);
        }
    }
    let prefix = prefix.map(|p| b.string(p.0));
    Ok((i, (prefix, URILiteral(b.string(&uri)))))
}
// [24]    	NamespaceDecl 	   ::=    	"declare" "namespace" NCName "=" URILiteral
fn namespace_decl<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, n) = ncname_str(i)?;
    let i = s_char_s('=', i)?;
    let (i, v) = string_literal(i)?;
    define_namespace(b, NCName(n.0), &v);
    Ok(i)
}
fn define_namespace(b: &mut B, prefix: NCName, namespace: &str) {
    if let Err(e) = b
        .qnames
        .define_namespace(Prefix(prefix.0), Namespace(namespace))
    {
        let e = match e {
            xust_tree::qnames::Error::AlreadyDefinedPrefix => Error::xqst(33),
            _ => Error::xxxx(43),
        };
        b.add_error(e);
    }
}
// [27]    	Annotation 	   ::=    	"%" EQName ("(" Literal ("," Literal)* ")")?
fn annotations<'a>(b: &mut B, mut i: &'a str) -> R<'a> {
    while let Ok(j) = annotation(b, i) {
        i = tws(j)?.0;
    }
    Ok(i)
}
fn annotation<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = char_s('%', i)?;
    let (i, _name) = eqname(b, i)?;
    let i = tws(i)?.0;
    // b.start_node();
    let i = if let Ok((i, _)) = char::<_, ()>('(')(i) {
        // TODO: cancel node on parse error
        let (mut i, _) = tws(i)?;
        while let Ok((j, _)) = literal(b, i) {
            i = char_s(',', j)?
        }
        char_s(')', i)?
    } else {
        i
    };
    // b.end_node(Node::Annotation(name));
    Ok(i)
}
// [28]    	VarDecl 	   ::=    	"variable" "$" VarName TypeDeclaration? ((":=" VarValue) | ("external" (":=" VarDefaultValue)?))
fn var_decl<'a>(b: &mut B, i: &'a str, _annotations: Vec<Annotation>) -> R<'a> {
    let i = tws(i)?.0;
    let i = char_s('$', i)?;
    b.start_node();
    let (i, qname) = eqname(b, i)?;
    if let Err(e) = b.vars.declare_global_variable(qname, &b.qnames) {
        b.add_error(e);
    }
    let i = tws(i)?.0;
    let i = if let Ok(j) = type_declaration(b, i) {
        j
    } else {
        i
    };
    let (i, _) = tws(i)?;
    let (i, external) = var_value_or_external(b, i)?;
    b.end_node(Node::VarDecl { external });
    Ok(i)
}
fn var_value<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s(":=", i)?;
    let i = expr_single(b, i)?;
    Ok(i)
}
fn var_value_or_external<'a>(b: &mut B, i: &'a str) -> IResult<'a, VarValueOrExternal> {
    Ok(if let Ok(i) = tag_s("external", i) {
        (
            if let Ok(i) = var_value(b, i) { i } else { i },
            VarValueOrExternal::External,
        )
    } else {
        (var_value(b, i)?, VarValueOrExternal::Value)
    })
}
// [31]    	ContextItemDecl 	   ::=    	"declare" "context" "item" ("as" ItemType)? ((":=" VarValue) | ("external" (":=" VarDefaultValue)?))
fn context_item_decl<'a>(b: &mut B, i: &'a str) -> R<'a> {
    b.start_node();
    let i = s_tag_s("item", i)?;
    let i = if let Ok(i) = tag_s("as", i) {
        let i = item_type(b, i)?;
        tws(i)?.0
    } else {
        i
    };
    let (i, _value) = var_value_or_external(b, i)?;
    b.end_node(Node::ContextItem);
    Ok(i)
}
// [32]    	FunctionDecl 	   ::=    	"function" EQName "(" ParamList? ")" ("as" SequenceType)? (FunctionBody | "external") 	/* xgc: reserved-function-names */
fn function_decl<'a>(b: &mut B, i: &'a str, _annotations: Vec<Annotation>) -> R<'a> {
    let i = tws(i)?.0;
    let (i, name) = eqname(b, i)?;
    let i = tws(i)?.0;
    b.start_node();
    let scope = b.vars.start_scope();
    let (i, arity) = param_list(b, i)?;
    let i = tws(i)?.0;
    let i = if let Ok(i) = type_declaration(b, i) {
        i
    } else {
        i
    };
    let i = tws(i)?.0;
    let (i, _external) = if let Ok((i, _)) = tag::<_, _, ()>("external")(i) {
        (i, true)
    } else {
        let (i, _found) = enclosed_expr(b, i)?;
        (i, false)
    };
    let stack_size = b.vars.leave_scope(scope);
    b.end_node(Node::Function {
        name,
        arity,
        stack_size,
    });
    Ok(i)
}
// [33]    	ParamList 	   ::=    	Param ("," Param)*
fn param_list<'a>(b: &mut B, i: &'a str) -> IResult<'a, u32> {
    let i = char_s('(', i)?;
    let (i, arity) = if let Ok((mut i, _)) = param(b, i) {
        let mut arity = 1;
        while let Ok(j) = char_s(',', i) {
            let (j, _varid) = param(b, j)?;
            arity += 1;
            i = tws(j)?.0;
        }
        (i, arity)
    } else {
        (i, 0)
    };
    let i = s_char(')', i)?;
    Ok((i, arity))
}
// [34]    	Param 	   ::=    	"$" EQName TypeDeclaration?
fn param<'a>(b: &mut B, i: &'a str) -> IResult<'a, u16> {
    let (i, varid) = var_declaration(b, i)?;
    let i = tws(i)?.0;
    let i = if let Ok(i) = type_declaration(b, i) {
        i
    } else {
        i
    };
    Ok((i, varid))
}
// [36]    	EnclosedExpr 	   ::=    	"{" Expr? "}"
// return true if an expression was found
fn enclosed_expr<'a>(b: &mut B, i: &'a str) -> IResult<'a, bool> {
    let i = char_s('{', i)?;
    let (i, got_expression) = expr0(b, i)?;
    let i = s_char('}', i)?;
    Ok((i, got_expression))
}

/*
 *
 * How will we deal with Binary Operators? That is, expressions that do not
 * have a unique start. Expressions with binary operators all start with an
 * expression and that expression can turn out the be an expression that was
 * nested in another expression. For example:
 *    1 + 2 * 3   becomes   (1 + (2 * 3))
 * In this case it's fine that there is only on bracket. But in this case:
 *    1 * 2 + 3   becomes   ((1 * 2) + 3)
 * there is a bracket extra. This bracket can only be added later.
 * This can be done with Vec::insert.
 *
 * So we pass the builder to a parse function. When that returns with success,
 * we can see if there is a binary operator. If so, have to wrap the expression
 * from the point that we saw initially and add 'bracket' there.
 *
 * It's safest to do this after the second expression has passed.
 *
 * Best is to make this into a few tests before proceeding with the grammar.
 */

fn binary<'a>(
    b: &mut B,
    i: &'a str,
    prefix: &mut dyn FnMut(&'a str) -> IResult<BinaryOp>,
    sub: fn(&mut B, &'a str) -> R<'a>,
    just_one: bool,
) -> R<'a> {
    let start = b.len();
    let mut i = sub(b, i)?;
    while let Ok((j, op)) = prefix(i) {
        match sub(b, j) {
            Ok(j) => {
                b.wrap_tail(start, Node::BinaryOp(op));
                i = j;
            }
            _ => break,
        }
        if just_one {
            break;
        }
    }
    Ok(i)
}

fn binary_char<'a>(
    b: &mut B,
    i: &'a str,
    op: BinaryOp,
    c: char,
    sub: fn(&mut B, &'a str) -> R<'a>,
) -> R<'a> {
    binary(
        b,
        i,
        &mut map(tuple((tws, char(c), tws)), |_| op),
        sub,
        false,
    )
}

fn binary_tag<'a>(
    b: &mut B,
    i: &'a str,
    op: BinaryOp,
    t: &str,
    sub: fn(&mut B, &'a str) -> R<'a>,
    just_one: bool,
) -> R<'a> {
    binary(
        b,
        i,
        &mut map(tuple((tws, tag(t), tws)), |_| op),
        sub,
        just_one,
    )
}

// [39]    	Expr 	   ::=    	ExprSingle ("," ExprSingle)*
fn expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary_char(b, i, BinaryOp::Comma, ',', expr_single)
}
/*
#[test]
fn test_expr() {
    // literal("\"&\"").unwrap();
    // expr("\"&\"").unwrap();
}
*/
// return true if an expression was found
fn expr0<'a>(b: &mut B, i: &'a str) -> IResult<'a, bool> {
    expr(b, i).map(|i| (i, true)).or(Ok((i, false)))
}

type Fbi = for<'a> fn(b: &mut B, i: &'a str) -> R<'a>;

fn or<'a>(b: &mut B, i: &'a str, f: &'static [Fbi]) -> R<'a> {
    for f in &f[0..f.len() - 1] {
        if let Ok(i) = f(b, i) {
            return Ok(i);
        }
    }
    f.last().unwrap()(b, i)
}

// [40]    	ExprSingle 	   ::=    	FLWORExpr
// | QuantifiedExpr
// | SwitchExpr
// | TypeswitchExpr
// | IfExpr
// | TryCatchExpr
// | OrExpr
fn expr_single<'a>(b: &mut B, i: &'a str) -> R<'a> {
    or(
        b,
        i,
        &[
            flwor_expr,
            quantified_expr,
            switch_expr,
            typeswitch_expr,
            if_expr,
            try_catch_expr,
            or_expr,
        ],
    )
}
// [41]    	FLWORExpr 	   ::=    	InitialClause IntermediateClause* ReturnClause
fn flwor_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    b.start_node();
    let mut i = initial_clause(b, i).or_cancel(b)?;
    while let Ok(j) = intermediate_clause(b, i) {
        i = tws(j)?.0;
    }
    let i = tag("return")(i)?.0;
    let i = tws(i)?.0;
    let return_clause = b.next_node();
    let i = expr_single(b, i)?;
    let return_clause = if return_clause == b.next_node() {
        None
    } else {
        Some(return_clause)
    };
    b.end_node(Node::FLWORExpr(return_clause));
    Ok(i)
}
// [42]    	InitialClause 	   ::=    	ForClause | LetClause | WindowClause
fn initial_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    or(b, i, &[for_clause, let_clause, window_clause])
}
// [43]    	IntermediateClause 	   ::=    	InitialClause | WhereClause | GroupByClause | OrderByClause | CountClause
fn intermediate_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    or(
        b,
        i,
        &[
            for_clause,
            let_clause,
            window_clause,
            where_clause,
            group_by_clause,
            order_by_clause,
            count_clause,
        ],
    )
}
// [44]    	ForClause 	   ::=    	"for" ForBinding ("," ForBinding)*
fn for_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("for", i)?;
    let mut i = for_binding(b, i)?;
    while let Ok(j) = s_char_s(',', i) {
        i = for_binding(b, j)?;
    }
    Ok(i)
}
// [45]    	ForBinding 	   ::=    	"$" VarName TypeDeclaration? AllowingEmpty? PositionalVar? "in" ExprSingle
fn for_binding<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, varid) = var_declaration(b, i)?;
    b.start_node();
    let (i, has_type_declaration) = if let Ok(j) = s_tag_s("as", i) {
        (sequence_type(b, j)?, true)
    } else {
        (i, false)
    };
    let i = tws(i)?.0;
    let (i, allowing_empty) = opt(tuple((tag("allowing"), tws, tag("empty"), tws)))(i)?;
    let (i, positional_var) = if let Ok((i, v)) = positional_var(b, i) {
        (i, Some(v))
    } else {
        (i, None)
    };
    let i = tws(i)?.0;
    let i = tag("in")(i)?.0;
    let i = tws(i)?.0;
    let i = expr_single(b, i)?;
    b.end_node(Node::ForBinding(ForBinding {
        var: varid,
        allowing_empty: allowing_empty.is_some(),
        positional_var,
        has_type_declaration,
    }));
    Ok(i)
}
// [47]    	PositionalVar 	   ::=    	"at" "$" VarName
fn positional_var<'a>(b: &mut B, i: &'a str) -> IResult<'a, u16> {
    let i = tag_s("at", i)?;
    var_declaration(b, i)
}
// [48]    	LetClause 	   ::=    	"let" LetBinding ("," LetBinding)*
fn let_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("let", i)?;
    let mut i = let_binding(b, i)?;
    while let Ok(j) = s_char_s(',', i) {
        i = let_binding(b, j)?;
    }
    Ok(i)
}
// [49]    	LetBinding 	   ::=    	"$" VarName TypeDeclaration? ":=" ExprSingle
fn let_binding<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, varname) = var_declaration(b, i)?;
    b.start_node();
    let (i, has_type_declaration) = if let Ok(j) = s_tag_s("as", i) {
        (sequence_type(b, j)?, true)
    } else {
        (i, false)
    };
    let i = s_tag_s(":=", i)?;
    let i = expr_single(b, i)?;
    b.end_node(Node::LetBinding(varname, has_type_declaration));
    Ok(i)
}
// [50]    	WindowClause 	   ::=    	"for" (TumblingWindowClause | SlidingWindowClause)
// [51]    	TumblingWindowClause 	   ::=    	"tumbling" "window" "$" VarName TypeDeclaration? "in" ExprSingle WindowStartCondition WindowEndCondition?
// [52]    	SlidingWindowClause 	   ::=    	"sliding" "window" "$" VarName TypeDeclaration? "in" ExprSingle WindowStartCondition WindowEndCondition
// [53]    	WindowStartCondition 	   ::=    	"start" WindowVars "when" ExprSingle
// [54]    	WindowEndCondition 	   ::=    	"only"? "end" WindowVars "when" ExprSingle
fn window_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("for", i)?;
    let (i, tumbling) = if let Ok(i) = tag_s("tumbling", i) {
        (i, true)
    } else {
        let i = tag_s("sliding", i)?;
        (i, false)
    };
    let i = tag_s("window", i)?;
    let (i, varid) = var_declaration(b, i)?;
    b.start_node();
    let i = tws(i)?.0;
    let (i, has_type_declaration) = if let Ok(j) = type_declaration(b, i) {
        let i = tws(j)?.0;
        (i, true)
    } else {
        (i, false)
    };
    let i = tag_s("in", i)?;
    let i = expr_single(b, i)?;
    let i = s_tag_s("start", i)?;
    let i = window_vars(b, i)?;
    let (i, only) = opt(tuple((tws, tag("only"))))(i)?;
    let (i, has_end) = if tumbling {
        if let Ok(i) = s_tag_s("end", i) {
            (window_vars(b, i)?, true)
        } else {
            (i, false)
        }
    } else {
        let i = s_tag_s("end", i)?;
        let i = window_vars(b, i)?;
        (i, true)
    };
    b.end_node(Node::WindowClause(WindowClause {
        varid,
        tumbling,
        has_type_declaration,
        only: only.is_some(),
        has_end,
    }));
    Ok(i)
}
fn optional_var_name<'a, O, F: FnMut(&'a str) -> IResult<O>>(
    b: &mut B,
    i: &'a str,
    mut prefix: F,
) -> IResult<'a, Option<u16>> {
    Ok(if let Ok((i, _)) = prefix(i) {
        let (i, varid) = var_declaration(b, i)?;
        let i = tws(i)?.0;
        (i, Some(varid))
    } else {
        (i, None)
    })
}
// [55]    	WindowVars 	   ::=    	("$" CurrentItem)? PositionalVar? ("previous" "$" PreviousItem)? ("next" "$" NextItem)?
fn window_vars<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, current_item) = optional_var_name(b, i, tuple((char('$'), tws)))?;
    if let Some(item) = current_item {
        b.add_node(Node::WindowConditionCurrentItem(item));
    }
    let (i, positional_var) = if let Ok((i, positional_var)) = positional_var(b, i) {
        let i = tws(i)?.0;
        (i, Some(positional_var))
    } else {
        (i, None)
    };
    if let Some(item) = positional_var {
        b.add_node(Node::WindowConditionPositionalVar(item));
    }
    let (i, previous_item) =
        optional_var_name(b, i, tuple((tag("previous"), tws, char('$'), tws)))?;
    if let Some(item) = previous_item {
        b.add_node(Node::WindowConditionPreviousItem(item));
    }
    let (i, next_item) = optional_var_name(b, i, tuple((tag("next"), tws, char('$'), tws)))?;
    if let Some(item) = next_item {
        b.add_node(Node::WindowConditionNextItem(item));
    }
    let i = tag_s("when", i)?;
    let i = expr_single(b, i)?;
    Ok(i)
}
// [59]    	CountClause 	   ::=    	"count" "$" VarName
fn count_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("count", i)?;
    let (i, varid) = var_declaration(b, i)?;
    b.add_node(Node::CountClause(varid));
    Ok(i)
}
// [60]    	WhereClause 	   ::=    	"where" ExprSingle
fn where_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("where", i)?;
    b.start_node();
    let i = expr_single(b, i)?;
    b.end_node(Node::WhereClause);
    Ok(i)
}
// [61]    	GroupByClause 	   ::=    	"group" "by" GroupingSpecList
// [62]    	GroupingSpecList 	   ::=    	GroupingSpec ("," GroupingSpec)*
// [63]    	GroupingSpec 	   ::=    	GroupingVariable (TypeDeclaration? ":=" ExprSingle)? ("collation" URILiteral)?
// [64]    	GroupingVariable 	   ::=    	"$" VarName
fn group_by_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s_tag_s("group", "by", i)?;
    b.start_node();
    let mut i = grouping_spec(b, i)?;
    while let Ok(j) = s_char_s(',', i) {
        i = grouping_spec(b, j)?;
    }
    b.end_node(Node::GroupByClause);
    Ok(i)
}
fn grouping_spec<'a>(b: &mut B, i: &'a str) -> R<'a> {
    // if the grouping spec has an expression, then it's a declaration,
    // otherwise it is a reference. So initially we only check if there is a
    // variable and declare or reference it after the potential assignment.
    let (i, variable_str) = recognize(tuple((char('$'), tws, eqname_str)))(i)?;
    let i = tws(i)?.0;
    b.start_node();
    let (i, has_type_declaration) = if let Ok(i) = type_declaration(b, i) {
        (tws(i)?.0, true)
    } else {
        (i, false)
    };
    let (i, has_assignment) = if let Ok(i) = s_tag_s(":=", i) {
        (expr_single(b, i)?, true)
    } else {
        (i, false)
    };
    let variable = if has_assignment {
        var_declaration(b, variable_str)
    } else {
        if has_type_declaration {
            // todo: there may only be a type declaration if there is an assignment
        }
        var_reference(b, variable_str)
    }?
    .1;
    let i = tws(i)?.0;
    let (i, collation) = opt(preceded(tuple((tag("collation"), tws)), string_literal))(i)?;
    let collation = collation.map(|c| b.string(&c));
    b.end_node(Node::GroupingSpec {
        variable,
        collation,
    });
    Ok(i)
}
// [65]    	OrderByClause 	   ::=    	(("order" "by") | ("stable" "order" "by")) OrderSpecList
fn order_by_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, stable) = opt(tag("stable"))(i)?;
    let i = tws(i)?.0;
    let i = tag_s_tag_s("order", "by", i)?;
    b.start_node();
    let mut i = order_spec(b, i)?;
    while let Ok(j) = s_char_s(',', i) {
        i = order_spec(b, j)?;
    }
    b.end_node(Node::OrderByClause {
        stable: stable.is_some(),
    });
    Ok(i)
}
// [66]    	OrderSpecList 	   ::=    	OrderSpec ("," OrderSpec)*
// [67]    	OrderSpec 	   ::=    	ExprSingle OrderModifier
// [68]    	OrderModifier 	   ::=    	("ascending" | "descending")? ("empty" ("greatest" | "least"))? ("collation" URILiteral)?
fn order_spec<'a>(b: &mut B, i: &'a str) -> R<'a> {
    b.start_node();
    let i = expr_single(b, i)?;
    let i = tws(i)?.0;
    let (i, direction) = opt(sort_direction)(i)?;
    let (i, empty) = opt(preceded(tuple((tws, tag("empty"), tws)), greatest_or_least))(i)?;
    let i = tws(i)?.0;
    let (i, collation) = opt(preceded(tuple((tag("collation"), tws)), string_literal))(i)?;
    let collation = collation.map(|c| b.string(&c));
    b.end_node(Node::OrderSpec(OrderSpec {
        direction,
        empty,
        collation,
    }));
    Ok(i)
}
tag_enum!(SortDirection sort_direction
    "ascending" => Ascending
    "descending" => Descending
);
// [70]    	QuantifiedExpr 	   ::=    	("some" | "every") "$" VarName TypeDeclaration? "in" ExprSingle ("," "$" VarName TypeDeclaration? "in" ExprSingle)* "satisfies" ExprSingle
tag_enum!(SomeOrEvery some_or_every
    "some" => Some
    "every" => Every
);
fn quantified_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, some_or_every) = some_or_every(i)?;
    let (i, _) = tws(i)?;
    b.start_node();
    let mut i = quantified_expr_line(b, i).or_cancel(b)?;
    while let Ok(j) = s_char_s(',', i) {
        i = quantified_expr_line(b, j).or_cancel(b)?;
    }
    i = s_tag_s("satisfies", i).or_cancel(b)?;
    i = expr_single(b, i).or_cancel(b)?;
    b.end_node(Node::QuantifiedExpr(some_or_every));
    Ok(i)
}
fn quantified_expr_line<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, name) = var_declaration(b, i)?;
    let (mut i, _) = tws(i)?;
    b.start_node();
    if let Ok(j) = tag_s("as", i) {
        i = sequence_type(b, j).or_cancel(b)?;
    }
    let i = s_tag_s("in", i).or_cancel(b)?;
    let i = expr_single(b, i).or_cancel(b)?;
    b.end_node(Node::QuantifiedExprLine(name));
    Ok(i)
}
// [71]    	SwitchExpr 	   ::=    	"switch" "(" Expr ")" SwitchCaseClause+ "default" "return" ExprSingle
fn switch_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("switch", i)?;
    let i = char_s('(', i)?;
    b.start_node();
    let i = expr(b, i)?;
    let i = s_char_s(')', i)?;
    let i = switch_case_clause(b, i).or_cancel(b)?;
    let mut i = tws(i)?.0;
    while let Ok(j) = switch_case_clause(b, i) {
        i = tws(j)?.0;
    }
    let i = tag_s_tag_s("default", "return", i)?;
    let i = expr_single(b, i)?;
    b.end_node(Node::SwitchExpr);
    Ok(i)
}
// [72]    	SwitchCaseClause 	   ::=    	("case" SwitchCaseOperand)+ "return" ExprSingle
fn switch_case_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("case", i)?;
    b.start_node();
    let mut i = expr_single(b, i).or_cancel(b)?;
    while let Ok(j) = s_tag_s("case", i) {
        i = expr_single(b, j).or_cancel(b)?;
    }
    let i = s_tag_s("return", i)?;
    let i = expr_single(b, i)?;
    b.end_node(Node::SwitchCaseClause);
    Ok(i)
}
// [74]    	TypeswitchExpr 	   ::=    	"typeswitch" "(" Expr ")" CaseClause+ "default" ("$" VarName)? "return" ExprSingle
fn typeswitch_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("typeswitch", i)?;
    let i = char_s('(', i)?;
    b.start_node();
    let i = expr(b, i)?;
    let i = s_char_s(')', i)?;
    let i = case_clause(b, i)?;
    let mut i = tws(i)?.0;
    while let Ok(j) = case_clause(b, i) {
        i = tws(j)?.0;
    }
    let i = tag_s("default", i)?;
    let (i, varname) = if let Ok((i, varname)) = var_declaration(b, i) {
        let i = tws(i)?.0;
        (i, Some(varname))
    } else {
        (i, None)
    };
    let i = s_tag_s("return", i)?;
    let i = expr_single(b, i)?;
    b.end_node(Node::TypeswitchExpr(varname));
    Ok(i)
}
// [75]    	CaseClause 	   ::=    	"case" ("$" VarName "as")? SequenceTypeUnion "return" ExprSingle
// [76]    	SequenceTypeUnion 	   ::=    	SequenceType ("|" SequenceType)*
fn case_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("case", i)?;
    b.start_node();
    let (i, varname) = if let Ok((i, varname)) = var_declaration(b, i) {
        let i = s_tag_s("as", i)?;
        (i, Some(varname))
    } else {
        (i, None)
    };
    let mut i = sequence_type(b, i)?;
    while let Ok(j) = s_char_s('|', i) {
        i = sequence_type(b, j)?;
    }
    let i = s_tag_s("return", i)?;
    let i = expr_single(b, i)?;
    b.end_node(Node::CaseClause(varname));
    Ok(i)
}
// [77]    	IfExpr 	   ::=    	"if" "(" Expr ")" "then" ExprSingle "else" ExprSingle
fn if_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("if", i)?;
    b.start_node();
    let i = expr_single(b, i)?;
    let i = s_tag_s("then", i)?;
    let i = expr_single(b, i)?;
    let i = s_tag_s("else", i)?;
    let i = expr_single(b, i)?;
    b.end_node(Node::IfExpr);
    Ok(i)
}
// [78]    	TryCatchExpr 	   ::=    	TryClause CatchClause+
// [79]    	TryClause 	   ::=    	"try" EnclosedTryTargetExpr
// [80]    	EnclosedTryTargetExpr 	   ::=    	EnclosedExpr
fn try_catch_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("try", i)?;
    b.start_node();
    let i = enclosed_expr(b, i)?.0;
    let mut i = tws(i)?.0;
    while let Ok(j) = catch_clause(b, i) {
        i = tws(j)?.0;
    }
    b.end_node(Node::TryCatchExpr);
    Ok(i)
}
// [81]    	CatchClause 	   ::=    	"catch" CatchErrorList EnclosedExpr
// [82]    	CatchErrorList 	   ::=    	NameTest ("|" NameTest)*
fn catch_clause<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("catch", i)?;
    b.start_node();
    let mut i = name_test(b, i)?.0;
    while let Ok(j) = s_char_s('|', i) {
        i = name_test(b, j)?.0;
    }
    let i = tws(i)?.0;
    for ln in &[
        "code",
        "description",
        "value",
        "module",
        "line-number",
        "column-number",
        "additional",
    ] {
        let qname = b.eqname(
            Namespace("http://www.w3.org/2005/xqt-errors"),
            Prefix("err"),
            NCName(ln),
        );
        b.vars.declare_variable(qname);
    }
    let i = enclosed_expr(b, i)?.0;
    b.end_node(Node::CatchClause);
    Ok(i)
}
// [83]    	OrExpr 	   ::=    	AndExpr ( "or" AndExpr )*
fn or_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary(
        b,
        i,
        &mut map(tuple((tws, not(tag("order")), tag("or"), tws)), |_| {
            BinaryOp::Or
        }),
        and_expr,
        false,
    )
}
// [150]    	DirCommentContents 	   ::=    	((Char - '-') | ('-' (Char - '-')))* 	/* ws:
// explicit */
// [84]    	AndExpr 	   ::=    	ComparisonExpr ( "and" ComparisonExpr )*
fn and_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary_tag(b, i, BinaryOp::And, "and", comparison_expr, false)
}
// [85]    	ComparisonExpr 	   ::=    	StringConcatExpr ( (ValueComp
// | GeneralComp
// | NodeComp) StringConcatExpr )?
fn comparison_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary(
        b,
        i,
        &mut map(tuple((tws, not(tag("let")), comp, tws)), |t| {
            BinaryOp::Comparison(t.2)
        }),
        string_concat_expr,
        true,
    )
}
/*
#[cfg(test)]
fn create_literal_expr(integer: i64) -> Expr {
    Expr::literal(Literal::Integer(integer))
}
#[test]
fn test_comparison_expr() {
    let one = create_literal_expr(1);
    assert_eq!(
        comparison_expr("1 = 1"),
        Ok((
            "",
            Expr::binary_expr(one.clone(), BinaryOp::Comparison(Comp::Equal), one)
        ))
    )
}
*/
// [86]    	StringConcatExpr 	   ::=    	RangeExpr ( "||" RangeExpr )*
fn string_concat_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary_tag(b, i, BinaryOp::StringConcat, "||", range_expr, false)
}
// [87]    	RangeExpr 	   ::=    	AdditiveExpr ( "to" AdditiveExpr )?
fn range_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary_tag(b, i, BinaryOp::Range, "to", additive_expr, true)
}
// [88]    	AdditiveExpr 	   ::=    	MultiplicativeExpr ( ("+" | "-") MultiplicativeExpr )*
fn additive_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary(
        b,
        i,
        &mut map(tuple((tws, plus_min, tws)), |t| match t.1 {
            PlusMinus::Plus => BinaryOp::Add,
            PlusMinus::Minus => BinaryOp::Substract,
        }),
        multiplicative_expr,
        false,
    )
}
fn plus_min(i: &str) -> IResult<PlusMinus> {
    map(one_of("+-"), |c| {
        if c == '+' {
            PlusMinus::Plus
        } else {
            PlusMinus::Minus
        }
    })(i)
}
// [89]    	MultiplicativeExpr 	   ::=    	UnionExpr ( ("*" | "div" | "idiv" | "mod") UnionExpr )*
tag_enum!(Multiplicative multiplicative
    "*" => Multiply
    "div" => Div
    "idiv" => IDiv
    "mod" => Mod
);
fn multiplicative_expr<'a>(b: &mut B, h: &'a str) -> R<'a> {
    let start = b.len();
    let mut i = union_expr(b, h)?;
    while let Ok((j, (ws1, op, ws2))) = tuple((recognize(tws), multiplicative, recognize(tws)))(i) {
        // We need a non-digit before and after the operator, e.g. 3div4 is not
        // allowed.
        if op != Multiplicative::Multiply {
            if ws1.is_empty() {
                not(digit1)(&h[h.len() - i.len() - 1..])?;
            }
            if ws2.is_empty() {
                not(digit1)(j)?;
            }
        }
        let op = match op {
            Multiplicative::Multiply => BinaryOp::Multiply,
            Multiplicative::Div => BinaryOp::Div,
            Multiplicative::IDiv => BinaryOp::IDiv,
            Multiplicative::Mod => BinaryOp::Mod,
        };
        match union_expr(b, j) {
            Ok(j) => {
                b.wrap_tail(start, Node::BinaryOp(op));
                i = j;
            }
            _ => break,
        }
    }
    Ok(i)
}
// [90]    	UnionExpr 	   ::=    	IntersectExceptExpr ( ("union" | "|") IntersectExceptExpr )*
fn union_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary(
        b,
        i,
        &mut map(tuple((tws, alt((tag("|"), tag("union"))), tws)), |_| {
            BinaryOp::Union
        }),
        intersect_except_expr,
        false,
    )
}
// [91]    	IntersectExceptExpr 	   ::=    	InstanceofExpr ( ("intersect" | "except") InstanceofExpr )*
tag_enum!(IntersectExcept intersect_except
    "intersect" => Intersect
    "except" => Except
);
fn intersect_except_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary(
        b,
        i,
        &mut map(tuple((tws, intersect_except, tws)), |t| match t.1 {
            IntersectExcept::Intersect => BinaryOp::Intersect,
            IntersectExcept::Except => BinaryOp::Except,
        }),
        instanceof_expr,
        true,
    )
}
// [92]    	InstanceofExpr 	   ::=    	TreatExpr ( "instance" "of" SequenceType )?
fn instanceof_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let start = b.len();
    let mut i = treat_expr(b, i)?;
    if let Ok(j) = tag_s_tag_s("instance", "of", i) {
        i = sequence_type(b, j)?;
        b.wrap_tail(start, Node::BinaryOp(BinaryOp::Instanceof));
    }
    Ok(i)
}
// [93]    	TreatExpr 	   ::=    	CastableExpr ( "treat" "as" SequenceType )?
fn treat_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let start = b.len();
    let mut i = castable_expr(b, i)?;
    if let Ok(j) = tag_s_tag_s("treat", "as", i) {
        i = sequence_type(b, j)?;
        b.wrap_tail(start, Node::BinaryOp(BinaryOp::Treat));
    }
    Ok(i)
}
// [94]    	CastableExpr 	   ::=    	CastExpr ( "castable" "as" SingleType )?
fn castable_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let start = b.len();
    let mut i = cast_expr(b, i)?;
    if let Ok(j) = tag_s_tag_s("castable", "as", i) {
        i = single_type(b, j)?;
        b.wrap_tail(start, Node::BinaryOp(BinaryOp::Castable));
    }
    Ok(i)
}
// [95]    	CastExpr 	   ::=    	ArrowExpr ( "cast" "as" SingleType )?
fn cast_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let start = b.len();
    let mut i = arrow_expr(b, i)?;
    if let Ok(j) = tag_s_tag_s("cast", "as", i) {
        let j = single_type(b, j)?;
        b.wrap_tail(start, Node::BinaryOp(BinaryOp::Cast));
        i = j;
    }
    Ok(i)
}
// [96]    	ArrowExpr 	   ::=    	UnaryExpr ( "=>" ArrowFunctionSpecifier ArgumentList )*
fn arrow_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let start = b.len();
    let mut i = unary_expr(b, i)?;
    while let Ok(j) = s_tag_s("=>", i) {
        let (j, id) = arrow_function_specifier(b, j)?;
        let j = tws(j)?.0;
        let (j, arity) = argument_list(b, j)?;
        i = j;
        b.wrap_tail(
            start,
            Node::ArrowFunctionCall {
                id: if let Some(id) = id { id + 1 } else { 0 },
                arity: arity + 1,
            },
        );
    }
    Ok(i)
}
// [97]    	UnaryExpr 	   ::=    	("-" | "+")* ValueExpr
fn positive(i: &str) -> IResult<bool> {
    fold_many0(
        one_of("-+"),
        || true,
        |acc, item| if item == '-' { !acc } else { acc },
    )(i)
}
fn unary_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, positive) = positive(i)?;
    if positive {
        value_expr(b, i)
    } else {
        b.start_node();
        let i = value_expr(b, i).or_cancel(b)?;
        b.end_node(Node::Negate);
        Ok(i)
    }
}
// [98]    	ValueExpr 	   ::=    	ValidateExpr | ExtensionExpr | SimpleMapExpr
fn value_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    simple_map_expr(b, i)
    //alt((validate_expr, simple_map_expr, extension_expr))(i)
}
// [99]    	GeneralComp 	   ::=    	"=" | "!=" | "<" | "<=" | ">" | ">="
// [100]    	ValueComp 	   ::=    	"eq" | "ne" | "lt" | "le" | "gt" | "ge"
// [101]    	NodeComp 	   ::=    	"is" | "<<" | ">>"
tag_enum!(Comp comp
    "=" => Equal
    "!=" => NotEqual
    "<=" => LessOrEqual
    ">=" => GreaterOrEqual
    "eq" => Eq
    "ne" =>  Ne
    "lt" => Lt
    "le" => Le
    "gt" => Gt
    "ge" => Ge
    "is" => Is
    "<<" => Precedes
    ">>" => Follows
    "<" => Less
    ">" => Greater
);
/*
#[test]
fn test_compo() {
    let s = "<elem/> let $b";
    let _r = expr_single(s).unwrap();
    assert_eq!(comp("="), Ok(("", Comp::Equal)))
}
// [102]    	ValidateExpr 	   ::=    	"validate" (ValidationMode | ("type" TypeName))? "{" Expr "}"
fn validate_expr(i: &str) -> Result<Expr> {
    let (i, _) = tag("validate")(i)?;
    let (i, _) = tws(i)?;
    let (i, m) = opt(alt((
        map(tag("lax"), |_| ValidationMode::Lax),
        map(tag("strict"), |_| ValidationMode::Strict),
        map(tuple((tag("type"), tws, eqname)), |t| {
            ValidationMode::Type(t.2)
        }),
    )))(i)?;
    let (i, _) = tws(i)?;
    let (i, e) = enclosed_expr(i)?;
    Ok((
        i,
        Expr::validate_expr(
            e.unwrap_or(Expr::empty_sequence()),
            m.unwrap_or(ValidationMode::Strict),
        ),
    ))
}
// [104]    	ExtensionExpr 	   ::=    	Pragma+ "{" Expr? "}"
fn extension_expr(i: &str) -> Result<Expr> {
    let (i, pragmas) = many1(terminated(pragma, tws))(i)?;
    let (i, expr) = enclosed_expr(i)?;
    Ok((i, Expr::extension_expr(expr, pragmas)))
}
// [105]    	Pragma 	   ::=    	"(#" S? EQName (S PragmaContents)? "#)" 	/* ws: explicit */
fn pragma(i: &str) -> Result<(QName, String)> {
    let (i, _) = tag("(#")(i)?;
    let (i, _) = s(i)?;
    let (i, name) = eqname(i)?;
    let (i, _) = s(i)?;
    let (i, contents) = take_until("#)")(i)?;
    let (i, _) = tag("#)")(i)?;
    Ok((i, (name, contents.into())))
}
*/
// [106]    	PragmaContents 	   ::=    	(Char* - (Char* '#)' Char*))
// [107]    	SimpleMapExpr 	   ::=    	PathExpr ("!" PathExpr)*
fn simple_map_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    binary_char(b, i, BinaryOp::SimpleMap, '!', path_expr)
}

// expand '/' to fn:root(self::node()) treat as document-node()
fn root(b: &mut B) {
    b.start_node();
    b.start_node();
    b.start_node();
    b.add_node(Node::KindTest(KindTest::AnyKindTest));
    b.end_node(Node::AxisStep(Axis::Self_, NodeTest::KindTest));
    let id = b
        .eqname(
            Namespace("http://www.w3.org/2005/xpath-functions"),
            Prefix("fn"),
            NCName("root"),
        )
        .to_usize();
    b.end_node(Node::FunctionCall {
        id,
        arity: 1,
        dynamic: false,
    });
    b.start_node();
    b.start_node();
    b.add_node(Node::KindTest(KindTest::DocumentTestNone));
    b.end_node(Node::ItemType(ItemType::KindTest));
    b.end_node(Node::SequenceType {
        occurrence_indicator: OccurrenceIndicator::One,
    });
    b.end_node(Node::BinaryOp(BinaryOp::Treat));
}

// expand '//' to descendant-or-self::node()
fn double_slash(b: &mut B) {
    b.start_node();
    b.add_node(Node::KindTest(KindTest::AnyKindTest));
    b.end_node(Node::AxisStep(Axis::DescendantOrSelf, NodeTest::KindTest));
}

// [108]    	PathExpr 	   ::=    	("/" RelativePathExpr?)
// | ("//" RelativePathExpr)
// | RelativePathExpr
fn path_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Some(i) = i.strip_prefix("//") {
        let i = tws(i)?.0;
        b.start_node();
        root(b);
        b.start_node();
        double_slash(b);
        match relative_path_expr(b, i) {
            Ok(i) => {
                b.end_node(Node::BinaryOp(BinaryOp::Step));
                b.end_node(Node::BinaryOp(BinaryOp::Step));
                Ok(i)
            }
            Err(e) => {
                b.cancel_node();
                b.cancel_node();
                Err(e)
            }
        }
    } else if let Some(i) = i.strip_prefix('/') {
        let i = tws(i)?.0;
        let start = b.len();
        root(b);
        let i = if let Ok(i) = relative_path_expr(b, i) {
            b.wrap_tail(start, Node::BinaryOp(BinaryOp::Step));
            i
        } else {
            i
        };
        Ok(i)
    } else {
        relative_path_expr(b, i)
    }
}
// [109]    	RelativePathExpr 	   ::=    	StepExpr (("/" | "//") StepExpr)*
fn relative_path_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let start = b.len();
    let mut i = step_expr(b, i)?;
    loop {
        let (mut j, _) = tws(i)?;
        if j.starts_with("//") {
            // expand '//' to /descendant-or-self::node()/
            b.start_node();
            b.add_node(Node::KindTest(KindTest::AnyKindTest));
            b.end_node(Node::AxisStep(Axis::DescendantOrSelf, NodeTest::KindTest));
            b.wrap_tail(start, Node::BinaryOp(BinaryOp::Step));
            j = &j[2..];
        } else if let Ok(k) = char_s('/', i) {
            j = k;
        } else {
            break;
        }
        match step_expr(b, j) {
            Ok(j) => {
                b.wrap_tail(start, Node::BinaryOp(BinaryOp::Step));
                i = j;
            }
            Err(_) => {
                break;
            }
        }
    }
    let (i, _) = tws(i)?;
    Ok(i)
}
// [110]    	StepExpr 	   ::=    	PostfixExpr | AxisStep
fn step_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Ok(ok) = postfix_expr(b, i) {
        return Ok(ok);
    }
    axis_step(b, i)
}
// [111]    	AxisStep 	   ::=    	(ReverseStep | ForwardStep) PredicateList
fn axis_step<'a>(b: &mut B, i: &'a str) -> R<'a> {
    b.start_node();
    // step() starts a node that should be ended here
    let (i, node_test) = step(b, i).or_cancel(b)?;
    let (mut i, _) = tws(i)?;
    while let Ok(j) = predicate(b, i) {
        i = tws(j)?.0;
    }
    b.end_node(node_test);
    Ok(i)
}

tag_enum!(Axis axis
    "child" => Child
    "descendant-or-self" => DescendantOrSelf
    "descendant" => Descendant
    "attribute" => Attribute
    "self" => Self_
    "following-sibling" => FollowingSibling
    "following" => Following
    "parent" => Parent
    "ancestor-or-self" => AncestorOrSelf
    "ancestor" => Ancestor
    "preceding-sibling" => PrecedingSibling
    "preceding" => Preceding
);
fn step<'a>(b: &mut B, i: &'a str) -> IResult<'a, Node> {
    if let Ok((i, _)) = tag::<_, _, ()>("..")(i) {
        Ok((i, Node::AxisStep(Axis::Parent, NodeTest::Wildcard)))
    } else if let Some(i) = i.strip_prefix('@') {
        let (i, _) = tws(i)?;
        let (i, node_test) = node_test(b, i)?;
        Ok((i, Node::AxisStep(Axis::Attribute, node_test)))
    } else {
        let (i, axis) = opt(terminated(axis, tuple((tws, tag("::")))))(i)?;
        let (i, node_test) = node_test(b, i)?;
        Ok((i, Node::AxisStep(axis.unwrap_or(Axis::Child), node_test)))
    }
}
// [118]    	NodeTest 	   ::=    	KindTest | NameTest
fn node_test<'a>(b: &mut B, i: &'a str) -> IResult<'a, NodeTest> {
    if let Ok((i, _)) = kind_test(b, i) {
        Ok((i, NodeTest::KindTest))
    } else {
        name_test(b, i)
    }
}

// [119]    	NameTest 	   ::=    	EQName | Wildcard
fn name_test<'a>(b: &mut B, i: &'a str) -> IResult<'a, NodeTest> {
    if let Ok((i, ncname)) = preceded(tag("*:"), ncname_str)(i) {
        Ok((i, NodeTest::Localname(b.string(ncname.as_str()))))
    } else if let Ok((i, _)) = char::<_, ()>('*')(i) {
        Ok((i, NodeTest::Wildcard))
    } else if let Ok((i, ns)) = terminated(braced_uri_literal, char('*'))(i) {
        Ok((i, NodeTest::NS(b.string(&ns))))
    //    } else if let Ok((i, prefix)) = terminated(ncname_str, tag(":*"))(i) {
    //        Ok((i, NodeTest::NSPrefix(b.prefix(prefix.as_prefix()))))
    } else {
        let (i, qname) = eqname_element(b, i)?;
        Ok((i, NodeTest::QName(qname)))
    }
}
// [121]    	PostfixExpr 	   ::=    	PrimaryExpr (Predicate | ArgumentList | Lookup)*
fn postfix<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Ok((i, _)) = char::<_, ()>('?')(i) {
        let (i, _) = tws(i)?;
        let i = key_specifier(b, i)?;
        return Ok(i);
    }
    let start = b.len();
    if let Ok((i, _)) = argument_list(b, i) {
        b.wrap_tail(start, Node::PostfixArgumentList);
        return Ok(i);
    }
    let i = predicate(b, i)?;
    b.wrap_tail(start, Node::Predicate);
    Ok(i)
}
fn postfix_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let start = b.len();
    let mut i = primary_expr(b, i)?;
    let mut has_postfix = false;
    while let Ok(j) = postfix(b, i) {
        has_postfix = true;
        i = tws(j)?.0;
    }
    if has_postfix {
        b.wrap_tail(start, Node::Postfix);
    }
    Ok(i)
}
// [122]    	ArgumentList 	   ::=    	"(" (Argument ("," Argument)*)? ")"
fn argument_list<'a>(b: &mut B, i: &'a str) -> IResult<'a, u32> {
    let mut nargs = 0;
    let mut i = char_s('(', i)?;
    while !i.starts_with(')') {
        let j = if let Ok((i, _)) = char::<_, ()>('?')(i) {
            b.add_node(Node::ArgumentPlaceholder);
            i
        } else {
            expr_single(b, i)?
        };
        nargs += 1;
        match char_s(',', j) {
            Ok(j) => i = j,
            Err(_) => {
                i = j;
                break;
            }
        };
    }
    let i = char_s(')', i)?;
    Ok((i, nargs))
}
// [124]    	Predicate 	   ::=    	"[" Expr "]"
fn predicate<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = char_s('[', i)?;
    let i = expr(b, i)?;
    let i = s_char(']', i)?;
    Ok(i)
}
// [126]    	KeySpecifier 	   ::=    	NCName | Integer | ParenthesizedExpr | "*"
fn key_specifier<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Ok((i, integer)) = integer_literal(i) {
        b.add_node(Node::Lookup(KeySpecifier::Integer(integer as u32)));
        Ok(i)
    } else if let Ok((i, ncname)) = ncname_str(i) {
        let ncname = b.string(ncname.as_str());
        b.add_node(Node::Lookup(KeySpecifier::NCName(ncname)));
        Ok(i)
    } else if let Ok((i, _)) = char::<_, ()>('*')(i) {
        b.add_node(Node::Lookup(KeySpecifier::Wildcard));
        Ok(i)
    } else {
        b.start_node();
        let i = parenthesized_expr(b, i).or_cancel(b)?;
        b.end_node(Node::Lookup(KeySpecifier::Expr));
        Ok(i)
    }
}

// [127]    	ArrowFunctionSpecifier 	   ::=    	EQName | VarRef | ParenthesizedExpr
fn arrow_function_specifier<'a>(b: &mut B, i: &'a str) -> IResult<'a, Option<usize>> {
    eqname_function(b, i)
        .map(|(i, name)| (i, Some(name.to_usize())))
        .or_else(|_| {
            var_ref(b, i)
                .or_else(|_| parenthesized_expr(b, i))
                .map(|i| (i, None))
        })
}

// [128]    	PrimaryExpr 	   ::=    	Literal
// | VarRef
// | ParenthesizedExpr
// | ContextItemExpr
// | FunctionCall
// | OrderedExpr
// | UnorderedExpr
// | NodeConstructor
// | FunctionItemExpr
// | MapConstructor
// | ArrayConstructor
// | StringConstructor
// | UnaryLookup
fn primary_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Ok((i, l)) = literal(b, i) {
        b.add_node(l);
        return Ok(i);
    }
    if let Ok(ok) = parenthesized_expr(b, i) {
        return Ok(ok);
    }
    if let Ok(ok) = array_constructor(b, i) {
        return Ok(ok);
    }
    if let Ok((i, _)) = char::<_, ()>('?')(i) {
        return key_specifier(b, i);
    }
    if let Ok(ok) = function_call(b, i) {
        return Ok(ok);
    }
    if let Ok(ok) = node_constructor(b, i) {
        return Ok(ok);
    }
    if let Ok(ok) = map_constructor(b, i) {
        return Ok(ok);
    }
    if let Ok(ok) = named_function_ref(b, i) {
        return Ok(ok);
    }
    if let Ok(ok) = inline_function_expr(b, i) {
        return Ok(ok);
    }
    if let Ok(ok) = string_constructor(b, i) {
        return Ok(ok);
    }
    if let Ok(ok) = order_expr(b, i) {
        return Ok(ok);
    }
    if let Ok(i) = var_ref(b, i) {
        Ok(i)
    } else {
        // if this is '..' do not match '.'
        if tag::<_, _, ()>("..")(i).is_ok() {
            tag("proceed")(i).map(|(i, _)| i)
        } else {
            let i = char('.')(i)?.0;
            b.add_node(Node::ContextItemExpr);
            Ok(i)
        }
    }
}
// [129]    	Literal 	   ::=    	Literal | String
fn literal<'a>(b: &mut B, i: &'a str) -> IResult<'a, Node> {
    if let Ok((i, nl)) = numeric_literal(i) {
        Ok((i, nl))
    } else {
        let (i, sl) = string_literal(i)?;
        Ok((i, Node::String(b.string(&sl))))
    }
}
// [130]    	Literal 	   ::=    	Integer | Decimal | Double
// [220]    	Decimal 	   ::=    	("." Digits) | (Digits "." [0-9]*) 	/* ws: explicit */
// [221]    	Double 	   ::=    	(("." Digits) | (Digits ("." [0-9]*)?)) [eE] [+-]? Digits 	/* ws: explicit */
fn numeric_literal(s: &str) -> IResult<Node> {
    if let Ok((i, _)) = digit1::<_, ()>(s) {
        if let Ok((i, _)) = char::<_, ()>('.')(i) {
            let i = digit0(i)?.0;
            if let Ok((i, _)) = one_of::<_, _, ()>("eE")(i) {
                return double(s, i);
            } else {
                return decimal(s, i);
            }
        } else if let Ok((i, _)) = one_of::<_, _, ()>("eE")(i) {
            return double(s, i);
        } else {
            // integer
            let s = &s[0..s.len() - i.len()];
            let v = if let Ok(v) = str::parse::<i64>(s) {
                v
            } else {
                // try to parse into a decimal, it can take larger integers
                if let Ok(v) = std::str::FromStr::from_str(s) {
                    return Ok((i, Node::Decimal(v)));
                }
                return Err(nom::Err::Error(nom::error::make_error(
                    i,
                    nom::error::ErrorKind::MapRes,
                )));
            };
            return Ok((i, Node::Integer(I64(v))));
        }
    }
    let i = char('.')(s)?.0;
    let i = digit1(i)?.0;
    if let Ok((i, _)) = one_of::<_, _, ()>("eE")(i) {
        return double(s, i);
    }
    decimal(s, i)
}
fn double<'a>(s: &'a str, exponent: &'a str) -> IResult<'a, Node> {
    let i = opt(one_of("+-"))(exponent)?.0;
    let i = digit1(i)?.0;
    let s = &s[0..s.len() - i.len()];
    let v = map(nom::number::complete::double, |i| Node::Double(F64(i)))(s)?.1;
    Ok((i, v))
}
fn decimal<'a>(s: &'a str, i: &'a str) -> IResult<'a, Node> {
    let s = &s[0..s.len() - i.len()];
    let v = std::str::FromStr::from_str(s)
        .map_err(|_| nom::Err::Error(nom::error::make_error(s, nom::error::ErrorKind::Digit)))?;
    Ok((i, Node::Decimal(v)))
}
// [131]    	VarRef 	   ::=    	"$" VarName
fn var_ref<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, varid) = var_reference(b, i)?;
    b.add_node(Node::VarRef(varid));
    Ok(i)
}
#[cfg(test)]
fn count_exprs_and_nodes(_s: &str, _f: ()) -> (usize, usize) {
    use super::ast::builder::Builder;
    let _b = Builder::default();
    //    let mut b = b.
    (0, 0)
}
#[test]
fn test_var_ref() {}
// [133]    	ParenthesizedExpr 	   ::=    	"(" Expr? ")"
fn parenthesized_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = char_s('(', i)?;
    b.start_node();
    let i = expr0(b, i).or_cancel(b)?.0;
    let i = s_char(')', i).or_cancel(b)?;
    b.end_node(Node::ParenthesizedExpr);
    Ok(i)
}
// [135]    	OrderedExpr 	   ::=    	"ordered" EnclosedExpr
// [136]    	UnorderedExpr 	   ::=    	"unordered" EnclosedExpr
fn order_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, (ordering_mode, _)) = tuple((ordering_mode, tws))(i)?;
    b.start_node();
    let i = enclosed_expr(b, i).or_cancel(b)?.0;
    b.end_node(Node::Ordering(ordering_mode));
    Ok(i)
}

const RESERVED_FUNCTION_NAMES: [&str; 18] = [
    "array",
    "attribute",
    "comment",
    "document-node",
    "element",
    "empty-sequence",
    "function",
    "if",
    "item",
    "map",
    "namespace-node",
    "node",
    "processing-instruction",
    "schema-attribute",
    "schema-element",
    "switch",
    "text",
    "typeswitch",
];
// [137]    	FunctionCall 	   ::=    	EQName ArgumentList
fn function_call<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, name) = eqname_function(b, i)?;
    let i = tws(i)?.0;
    b.start_node();
    let (i, arity) = argument_list(b, i).or_cancel(b)?;
    b.end_node(Node::FunctionCall {
        id: name.to_usize(),
        arity,
        dynamic: false,
    });
    /*
    let mut fc = Expr::function_call(eqname.into());
    let (i, _) = tws(i)?;
    let (i, arguments) = argument_list(i)?;
    for arg in arguments.0 {
        if let Some(exp) = arg {
            fc.function_call_arg(exp);
        } else {
            fc.function_call_placeholder();
        }
    }
    */
    Ok(i)
}
/*
#[test]
fn test_function_call() {
    let zero = create_literal_expr(0);
    assert_eq!(
        function_call("not( 0)"),
        Ok((
            "",
            Expr::function_call(EQName(String::from("not")), ArgumentList(vec![Some(zero)]))
        ))
    )
}
*/
// [140]    	NodeConstructor 	   ::=    	DirectConstructor
// | ComputedConstructor
fn node_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Ok(i) = direct_constructor(b, i) {
        return Ok(i);
    }
    computed_constructor(b, i)
    /*
    alt((
        map(direct_constructor, Expr::direct_constructor),
        computed_constructor,
    ))(i)
    */
}
// DirectConstructor 	   ::=    	DirElemConstructor
// | DirCommentConstructor
// | DirPIConstructor
fn direct_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Ok(r) = dir_elem_constructor(b, i) {
        return Ok(r);
    }
    if let Ok(r) = dir_comment_constructor(b, i) {
        return Ok(r);
    }
    dir_pi_constructor(b, i)
}
// [142]    	DirElemConstructor 	   ::=    	"<" QName DirAttributeList ("/>" | (">" DirElemContent* "</" QName S? ">")) 	/* ws: explicit */
fn dir_elem_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = char('<')(i)?.0;
    let tagname = recognize(qname_str)(i)?.1;
    let (i, (prefix, local_name)) = qname_str(i)?;
    let i = s(i)?.0;
    b.start_node();
    let ns = b.qnames.start_namespace_context();
    let r = dir_attribute_list_ns(b, i);
    b.qnames.finish_namespace_context();
    r.or_cancel(b).map_err(|e| {
        b.qnames.leave_namespace_context(ns);
        e
    })?;
    let r = dir_elem_constructor_inner(b, i, tagname, prefix, local_name);
    b.qnames.leave_namespace_context(ns);
    let (i, node) = r?;
    b.end_node(node);
    Ok(i)
}
fn dir_elem_constructor_inner<'a>(
    b: &mut B,
    i: &'a str,
    tagname: &str,
    prefix: Prefix,
    local_name: NCName,
) -> IResult<'a, Node> {
    let i = dir_attribute_list(b, i).or_cancel(b)?;
    let qname = b.qname_element(prefix, local_name);
    let mut i = s(i).or_cancel(b)?.0;
    let self_close;
    if let Ok((mut j, _)) = char::<_, ()>('>')(i) {
        self_close = false;
        while let Ok(k) = dir_elem_content(b, j) {
            j = k;
        }
        let (j, (_, endtag, _, _)) =
            tuple((tag("</"), recognize(qname_str), tws, char('>')))(j).or_cancel(b)?;
        if tagname != endtag {
            b.add_error(Error::xqst(118))
        }
        i = j;
    } else {
        self_close = true;
        let j = tuple((s, tag("/>")))(i).or_cancel(b)?.0;
        i = j;
    }
    Ok((i, Node::DirElemConstructor(qname, self_close)))
}
#[derive(Clone, Copy, Debug)]
enum AttributeTarget<'a> {
    None,
    Attribute,
    Namespace(Prefix<'a>),
}
fn attribute_target<'a>(prefix: Prefix<'a>, local_name: NCName<'a>) -> AttributeTarget<'a> {
    if prefix == Prefix("xmlns") {
        AttributeTarget::Namespace(local_name.as_prefix())
    } else if prefix == Prefix("") && local_name == NCName("xmlns") {
        AttributeTarget::Namespace(Prefix(""))
    } else {
        AttributeTarget::None
    }
}
// [143]    	DirAttributeList 	   ::=    	(S (QName S? "=" S? StringConstructor)?)* 	/* ws: explicit */
fn dir_attribute_list_ns<'a>(b: &mut B, mut i: &'a str) -> R<'a> {
    while let Ok((j, (_, (prefix, local_name), _, _, _))) =
        tuple((s, qname_str, s, char('='), s))(i)
    {
        let target = attribute_target(prefix, local_name);
        if let AttributeTarget::Namespace(_) = target {
            i = dir_attribute_value(b, target, true, j)?;
        } else {
            let (j, delim) = one_of("\"'")(j)?;
            i = skip_attribute_value(j, delim)?;
        }
    }
    Ok(i)
}
fn skip_attribute_value(mut s: &str, delim: char) -> R {
    loop {
        let i = take_till(|c| c == delim || c == '{')(s)?.0;
        let (i, c) = anychar(i)?;
        let d = anychar(i)?.1; // peek
        if c == d {
            // '', "", or {{ are escapes: continue
            s = anychar(i)?.0;
            continue;
        }
        if c == delim {
            return Ok(i);
        }
        s = skip_attribute_enclosed_expr(i)?;
    }
}
fn skip_attribute_enclosed_expr(mut s: &str) -> R {
    loop {
        let i = take_till(|c| c == '}' || c == '{' || c == '"' || c == '\'')(s)?.0;
        let (i, c) = anychar(i)?;
        match c {
            '"' | '\'' => s = skip_attribute_value(i, c)?,
            '{' => {
                let c = anychar(i)?.1;
                if c != '{' {
                    s = skip_attribute_enclosed_expr(i)?;
                }
            }
            _ => {
                // '}'
                let (j, c) = anychar(i)?;
                if c != '}' {
                    return Ok(i);
                }
                s = j;
            }
        }
    }
}
fn dir_attribute_list<'a>(b: &mut B, mut i: &'a str) -> R<'a> {
    while let Ok((j, (_, (prefix, local_name), _, _, _))) =
        tuple((s, qname_str, s, char('='), s))(i)
    {
        if !(prefix == Prefix("xmlns") || (prefix == Prefix("") && local_name == NCName("xmlns"))) {
            b.start_node();
            i = dir_attribute_value(b, AttributeTarget::Attribute, false, j).or_cancel(b)?;
            let qname = b.qname_element(prefix, local_name);
            b.end_node(Node::DirAttribute(qname));
        } else {
            i = dir_attribute_value(b, AttributeTarget::None, false, j)?;
        }
    }
    Ok(i)
}
// [144]    	StringConstructor 	   ::=    	('"' (EscapeQuot | QuotAttrValueContent)* '"')
// | ("'" (EscapeApos | AposAttrValueContent)* "'")
fn dir_attribute_value<'a>(
    b: &mut B,
    ns_prefix: AttributeTarget<'a>,
    parsing_namespaces: bool,
    i: &'a str,
) -> R<'a> {
    if let Ok(i) = dir_attribute_value_q('"', "\"\"", b, ns_prefix, parsing_namespaces, i) {
        return Ok(i);
    }
    dir_attribute_value_q('\'', "''", b, ns_prefix, parsing_namespaces, i)
}
fn dir_attribute_value_q_cow<'a>(i: &'a str, qc: char, q: &str) -> IResult<'a, Cow<'a, str>> {
    if let Ok((i, s)) =
        take_while1::<_, _, ()>(move |c| c != qc && c != '{' && c != '}' && c != '<' && c != '&')(i)
    {
        Ok((i, Cow::Borrowed(s)))
    } else if let Ok((i, s)) = common_content_chars(i) {
        Ok((i, s))
    } else {
        map(tag(q), |s: &str| Cow::Borrowed(&s[..1]))(i)
    }
}
fn dir_attribute_value_q<'a>(
    qc: char,
    q: &str,
    b: &mut B,
    ns_prefix: AttributeTarget<'a>,
    parsing_namespaces: bool,
    i: &'a str,
) -> R<'a> {
    let mut i = char(qc)(i)?.0;
    loop {
        if let Ok((j, s)) = dir_attribute_value_q_cow(i, qc, q) {
            match ns_prefix {
                AttributeTarget::None => {}
                AttributeTarget::Attribute => {
                    let s = b.string(&s);
                    b.add_node(Node::String(s));
                }
                AttributeTarget::Namespace(ns_prefix) => {
                    if let Err(_e) = b.qnames.define_namespace(ns_prefix, Namespace(&s)) {
                        b.add_error(Error::xqst(70));
                    }
                }
            }
            i = j;
        } else if i.starts_with('{') {
            if parsing_namespaces {
                b.add_error(Error::xqst(22));
                break;
            }
            i = enclosed_expr(b, i)?.0;
        } else {
            break;
        }
    }
    let i = char(qc)(i)?.0;
    Ok(i)
}
// [147]    	DirElemContent 	   ::=    	DirectConstructor
// | CDataSection
// | CommonContent
// | ElementContentChar
fn dir_elem_content<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Ok((i, s)) = element_content_chars(i) {
        let s = b.string(s);
        b.add_node(Node::String(s));
        Ok(i)
    } else if let Ok((i, _)) = enclosed_expr(b, i) {
        Ok(i)
    } else if let Ok((i, cc)) = common_content_chars(i) {
        let s = b.string(&cc);
        b.add_node(Node::String(s));
        Ok(i)
    } else if let Ok((i, cds)) = cdata_section(i) {
        let s = b.string(cds);
        b.add_node(Node::String(s));
        Ok(i)
    } else {
        direct_constructor(b, i)
    }
}
// [148]    	CommonContent 	   ::=    	PredefinedEntityRef | CharRef | "{{" | "}}" | EnclosedExpr
fn common_content_chars(i: &str) -> IResult<Cow<str>> {
    alt((
        predefined_entity_ref,
        char_ref,
        map(tag("{{"), |_| Cow::Borrowed("{")),
        map(tag("}}"), |_| Cow::Borrowed("}")),
    ))(i)
}
// [149]    	DirCommentConstructor 	   ::=    	"<!--" DirCommentContents "-->" 	/* ws: explicit */
// [150]    	DirCommentContents 	   ::=    	((Char - '-') | ('-' (Char - '-')))* 	/* ws: explicit */
fn dir_comment_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, comment) = preceded(tag("<!--"), take_until("-->"))(i)?;
    if comment.contains("--") || comment == "-" {
        return Err(nom::Err::Error(nom::error::make_error(
            comment,
            nom::error::ErrorKind::Tag,
        )));
    }
    let comment = b.string(comment);
    b.add_node(Node::DirCommentConstructor(comment));
    let i = &i[3..];
    Ok(i)
}
// [151]    	DirPIConstructor 	   ::=    	"<?" PITarget (S DirPIContents)? "?>" 	/* ws: explicit */
// [152]    	DirPIContents 	   ::=    	(Char* - (Char* '?>' Char*)) 	/* ws: explicit */
fn dir_pi_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    // TODO: use PITarget instead of QName
    let (i, target) = preceded(tag("<?"), recognize(qname_str))(i)?;
    not(tag_no_case("xml"))(target)?;
    let (i, _) = tws(i)?;
    let (i, content) = take_until("?>")(i)?;
    let (i, _) = tag("?>")(i)?;
    let target = b.string(target);
    let content = b.string(content);
    b.add_node(Node::DirPIConstructor(target, content));
    Ok(i)
}
// [153] CDataSection 	   ::=    	"<![CDATA[" CDataSectionContents "]]>" 	/* ws: explicit */
fn cdata_section(i: &str) -> IResult<&str> {
    preceded(tag("<![CDATA["), take_until("]]>"))(i)
}
// [155]    	ComputedConstructor 	   ::=    	CompDocConstructor
// | CompElemConstructor
// | CompAttrConstructor
// | CompNamespaceConstructor
// | CompTextConstructor
// | CompCommentConstructor
// | CompPIConstructor
fn computed_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    or(
        b,
        i,
        &[
            comp_doc_constructor,
            comp_elem_constructor,
            comp_attr_constructor,
            // comp_namespace_constructor,
            comp_text_constructor,
            comp_comment_constructor,
            comp_pi_constructor,
        ],
    )
}
// [156]    	CompDocConstructor 	   ::=    	"document" EnclosedExpr
fn comp_doc_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("document", i)?;
    b.start_node();
    let i = enclosed_expr(b, i).or_cancel(b)?.0;
    b.end_node(Node::CompDocConstructor);
    Ok(i)
}
// "{" Expr "}"
fn enclosed_required_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = char_s('{', i)?;
    let i = expr(b, i)?;
    let i = s_char('}', i)?;
    Ok(i)
}
// [157]    	CompElemConstructor 	   ::=    	"element" (EQName | ("{" Expr "}")) EnclosedContentExpr
fn comp_elem_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("element", i)?;
    b.start_node();
    let (i, eqname) = if let Ok((i, eqname)) = eqname(b, i) {
        (i, Some(eqname))
    } else {
        let i = enclosed_required_expr(b, i).or_cancel(b)?;
        (i, None)
    };
    let i = tws(i)?.0;
    let i = enclosed_expr(b, i).or_cancel(b)?.0;
    b.end_node(Node::CompElemConstructor(eqname));
    Ok(i)
}
// [159]    	CompAttrConstructor 	   ::=    	"attribute" (EQName | ("{" Expr "}")) EnclosedExpr
fn comp_attr_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("attribute", i)?;
    b.start_node();
    let (i, eqname) = if let Ok((i, eqname)) = eqname(b, i) {
        (i, Some(eqname))
    } else {
        let i = enclosed_required_expr(b, i).or_cancel(b)?;
        (i, None)
    };
    let i = tws(i).or_cancel(b)?.0;
    let i = enclosed_expr(b, i).or_cancel(b)?.0;
    b.end_node(Node::CompAttrConstructor(eqname));
    Ok(i)
}
/*
// [160]    	CompNamespaceConstructor 	   ::=    	"namespace" (Prefix | EnclosedPrefixExpr) EnclosedURIExpr
fn comp_namespace_constructor(i: &str) -> Result<Expr> {
    let (i, _) = tag("namespace")(i)?;
    let (i, _) = tws(i)?;
    let (i, n) = alt((
        map(ncname, CompNCName::Name),
        map(enclosed_expr, CompNCName::NameExpr),
    ))(i)?;
    let (i, _) = tws(i)?;
    let (i, e) = enclosed_expr(i)?;
    Ok((i, Expr::comp_namespace_constructor(n, e)))
}
*/
// [164]    	CompTextConstructor 	   ::=    	"text" EnclosedExpr
fn comp_text_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("text", i)?;
    b.start_node();
    let i = enclosed_expr(b, i).or_cancel(b)?.0;
    b.end_node(Node::CompTextConstructor);
    Ok(i)
}
// [165]    	CompCommentConstructor 	   ::=    	"comment" EnclosedExpr
fn comp_comment_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("comment", i)?;
    b.start_node();
    let i = enclosed_expr(b, i).or_cancel(b)?.0;
    b.end_node(Node::CompCommentConstructor);
    Ok(i)
}
// [166]    	CompPIConstructor 	   ::=    	"processing-instruction" (NCName | ("{" Expr "}")) EnclosedExpr
fn comp_pi_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("processing-instruction", i)?;
    b.start_node();
    let (i, ncname) = if let Ok((i, ncname)) = ncname_str(i) {
        let ncname_id = b.string(ncname.as_str());
        (i, ncname_id)
    } else {
        let i = enclosed_required_expr(b, i).or_cancel(b)?;
        let ncname = b.string("");
        (i, ncname)
    };
    let i = tws(i).or_cancel(b)?.0;
    let i = enclosed_expr(b, i).or_cancel(b)?.0;
    b.end_node(Node::CompPIConstructor(ncname));
    Ok(i)
}
// [168]    	NamedFunctionRef 	   ::=    	EQName "#" IntegerLiteral
fn named_function_ref<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, eqname) = eqname_function(b, i)?;
    let i = s_char('#', i)?;
    b.start_node();
    let i = tws(i).or_cancel(b)?.0;
    let (i, arity) = integer_literal(i).or_cancel(b)?;
    b.end_node(Node::NamedFunctionRef {
        id: eqname.to_usize(),
        arity: arity as u32,
    });
    Ok(i)
}
// [169]    	InlineFunctionExpr 	   ::=    	Annotation* "function" "(" ParamList? ")" ("as" SequenceType)? FunctionBody
fn inline_function_expr<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = annotations(b, i)?;
    let i = tag_s("function", i)?;
    let (i, _arity) = param_list(b, i)?;
    let i = tws(i)?.0;
    let i = if let Ok(i) = type_declaration(b, i) {
        i
    } else {
        i
    };
    let i = tws(i)?.0;
    let (i, _found) = enclosed_expr(b, i)?;
    Ok(i)
}
// [170]    	MapConstructor 	   ::=    	"map" "{" (MapConstructorEntry ("," MapConstructorEntry)*)? "}"
fn map_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("map", i)?;
    let mut i = char_s('{', i)?;
    b.start_node();
    while let Ok(j) = expr_single(b, i) {
        let j = s_char_s(':', j)?;
        let j = expr_single(b, j)?;
        let j = tws(j)?.0;
        match char_s(',', j) {
            Ok(j) => i = j,
            Err(_) => {
                i = j;
                break;
            }
        }
    }
    b.end_node(Node::MapConstructor);
    let i = char('}')(i)?.0;
    Ok(i)
}
// [174]    	ArrayConstructor 	   ::=    	SquareArrayConstructor | CurlyArrayConstructor
// [175]    	SquareArrayConstructor 	   ::=    	"[" (ExprSingle ("," ExprSingle)*)? "]"
// [176]    	CurlyArrayConstructor 	   ::=    	"array" EnclosedExpr
fn array_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Ok(mut i) = char_s('[', i) {
        b.start_node();
        while let Ok(j) = expr_single(b, i) {
            i = j;
            match s_char_s(',', i) {
                Ok(j) => {
                    i = j;
                }
                _ => break,
            }
        }
        let i = s_char(']', i).or_cancel(b)?;
        b.end_node(Node::SquareArrayConstructor);
        return Ok(i);
    }
    let i = tag_s("array", i)?;
    b.start_node();
    let i = enclosed_expr(b, i).or_cancel(b)?.0;
    b.end_node(Node::CurlyArrayConstructor);
    Ok(i)
}
// [177]    	StringConstructor 	   ::=    	"``[" StringConstructorContent "]``" 	/* ws: explicit */
// [178]    	StringConstructorContent 	   ::=    	StringConstructorChars (StringConstructorInterpolation StringConstructorChars)* 	/* ws: explicit */
fn string_constructor<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag("``[")(i)?.0;
    b.start_node();
    let mut i = string_constructor_chars(b, i).or_cancel(b)?;
    loop {
        i = if let Ok(j) = string_constructor_interpolation(b, i) {
            string_constructor_chars(b, j).or_cancel(b)?
        } else {
            break;
        }
    }
    let i = tag("]``")(i).or_cancel(b)?.0;
    b.end_node(Node::StringConstructor);
    Ok(i)
}
// [179]    	StringConstructorChars 	   ::=    	(Char* - (Char* ('`{' | ']``') Char*)) 	/* ws: explicit */
fn string_constructor_chars<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (ia, sa) = take_until("]``")(i)?;
    // look for `{ until ]``
    if let Ok((_, sb)) = take_until::<_, _, ()>("`{")(&i[..sa.len()]) {
        let string = b.string(sb);
        b.add_node(Node::String(string));
        Ok(&i[sb.len()..])
    } else {
        let string = b.string(sa);
        b.add_node(Node::String(string));
        Ok(ia)
    }
}
// [180]    	StringConstructorInterpolation 	   ::=    	"`{" Expr? "}`"
fn string_constructor_interpolation<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("`{", i)?;
    let i = expr0(b, i)?.0;
    let i = tuple((tws, tag("}`")))(i)?.0;
    Ok(i)
}
/*
// [181]    	UnaryLookup 	   ::=    	"?" KeySpecifier
fn lookup(i: &str) -> Result<KeySpecifier> {
    preceded(tuple((char('?'), tws)), key_specifier)(i)
}
*/
// [182]    	SingleType 	   ::=    	SimpleTypeName "?"?
fn single_type<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, name) = eqname(b, i)?;
    let i = tws(i)?.0;
    let (i, optional) = opt(char('?'))(i)?;
    b.add_node(Node::SingleType {
        optional: optional.is_some(),
        single_type: name,
    });
    Ok(i)
}
// [183]    	TypeDeclaration 	   ::=    	"as" SequenceType
fn type_declaration<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tag_s("as", i)?;
    sequence_type(b, i)
}
// [184]    	SequenceType 	   ::=    	("empty-sequence" "(" ")")
// | (ItemType OccurrenceIndicator?
fn sequence_type<'a>(b: &mut B, i: &'a str) -> R<'a> {
    if let Ok((i, _)) = tuple((tag("empty-sequence"), tws, char('('), tws, char(')')))(i) {
        b.add_node(Node::SequenceType {
            occurrence_indicator: OccurrenceIndicator::Zero,
        });
        Ok(i)
    } else {
        // ItemType is child node of SequenceType
        b.start_node();
        let i = item_type(b, i).or_cancel(b)?;
        let (i, (_, oi, _)) = tuple((tws, occurrence_indicator, tws))(i).or_cancel(b)?;
        b.end_node(Node::SequenceType {
            occurrence_indicator: oi,
        });
        Ok(i)
    }
}
// [185]    	OccurrenceIndicator 	   ::=    	"?" | "*" | "+" 	/* xgc: occurrence-indicators */
#[allow(clippy::unnecessary_wraps)]
fn occurrence_indicator(i: &str) -> IResult<OccurrenceIndicator> {
    if let Ok((i, _)) = char::<_, ()>('?')(i) {
        Ok((i, OccurrenceIndicator::ZeroOrOne))
    } else if let Ok((i, _)) = char::<_, ()>('*')(i) {
        Ok((i, OccurrenceIndicator::ZeroOrMore))
    } else if let Ok((i, _)) = char::<_, ()>('+')(i) {
        Ok((i, OccurrenceIndicator::OneOrMore))
    } else {
        Ok((i, OccurrenceIndicator::One))
    }
}
// [186]    	ItemType 	   ::=    	KindTest | ("item" "(" ")") | FunctionTest | MapTest | ArrayTest | AtomicOrUnionType | ParenthesizedItemType
fn item_type<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let start = b.len();
    if let Ok((i, _)) = kind_test(b, i) {
        b.wrap_tail(start, Node::ItemType(ItemType::KindTest));
        return Ok(i);
    }
    if let Ok((i, _)) = tuple((tag("item"), tws, char('('), tws, char(')')))(i) {
        b.add_node(Node::ItemType(ItemType::Item));
        return Ok(i);
    }
    if let Ok(i) = function_test(b, i) {
        return Ok(i);
    }
    if let Ok(i) = map_test(b, i) {
        return Ok(i);
    }
    if let Ok(i) = array_test(b, i) {
        return Ok(i);
    }
    if let Ok((i, name)) = eqname_element(b, i) {
        b.add_node(Node::ItemType(ItemType::AtomicOrUnionType(name.to_usize())));
        return Ok(i);
    }
    let i = char_s('(', i)?;
    let i = item_type(b, i)?;
    let i = s_char(')', i)?;
    Ok(i)
    /*
    eprintln!("Cannot parse '{}'.", i);
    unimplemented!()
    map(tuple((char('('), tws, item_type, tws, char(')'))), |t| {
        ItemType::ParenthesizedItemType(Box::new(t.2))
    })(i)
    */
}
// [188]    	KindTest 	   ::=    	DocumentTest
// | ElementTest
// | AttributeTest
// | SchemaElementTest
// | SchemaAttributeTest
// | PITest
// | CommentTest
// | TextTest
// | NamespaceNodeTest
// | AnyKindTest
fn kind_test<'a>(b: &mut B, i: &'a str) -> IResult<'a, NodeTest> {
    if let Ok(i) = document_test(b, i) {
        return Ok((i, NodeTest::KindTest));
    }
    if let Ok(i) = element_test(b, i) {
        return Ok((i, NodeTest::KindTest));
    }
    if let Ok(i) = attribute_test(b, i) {
        return Ok((i, NodeTest::KindTest));
    }
    if let Ok(i) = tag_s("schema-element", i) {
        let i = char_s('(', i)?;
        let (i, name) = eqname(b, i)?;
        let i = s_char(')', i)?;
        b.add_node(Node::KindTest(KindTest::SchemaElementTest(name)));
        return Ok((i, NodeTest::KindTest));
    }
    if let Ok(i) = tag_s("schema-attribute", i) {
        let i = char_s('(', i)?;
        let (i, name) = eqname(b, i)?;
        let i = s_char(')', i)?;
        b.add_node(Node::KindTest(KindTest::SchemaAttributeTest(name)));
        return Ok((i, NodeTest::KindTest));
    }
    if let Ok(i) = pi_test(b, i) {
        return Ok((i, NodeTest::KindTest));
    }
    if let Ok((i, _)) = tuple((tag("comment"), tws, char('('), tws, char(')')))(i) {
        b.add_node(Node::KindTest(KindTest::CommentTest));
        return Ok((i, NodeTest::KindTest));
    }
    if let Ok((i, _)) = tuple((tag("text"), tws, char('('), tws, char(')')))(i) {
        b.add_node(Node::KindTest(KindTest::TextTest));
        return Ok((i, NodeTest::KindTest));
    }
    if let Ok((i, _)) = tuple((tag("namespace-node"), tws, char('('), tws, char(')')))(i) {
        b.add_node(Node::KindTest(KindTest::NamespaceNodeTest));
        return Ok((i, NodeTest::KindTest));
    }
    let (i, _) = tuple((tag("node"), tws, char('('), tws, char(')')))(i)?;
    b.add_node(Node::KindTest(KindTest::AnyKindTest));
    Ok((i, NodeTest::KindTest))
}
// [190]    	DocumentTest 	   ::=    	"document-node" "(" (ElementTest | SchemaElementTest)? ")"
fn document_test<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tuple((tag("document-node"), tws, char('('), tws))(i)?.0;
    b.start_node();
    let (i, kt) = if let Ok((i, _)) = tuple((tag("element"), tws, char('('), tws))(i) {
        let (i, et) = element_test_end(b, i).or_cancel(b)?;
        (
            i,
            KindTest::DocumentTestElementTest {
                match_nilled: et.match_nilled,
                match_name: et.match_name,
                match_type: et.match_type,
            },
        )
    }
    /*
    let (i, t) = opt(alt((
        map(element_test, DocumentTest::ElementTest),
        map(schema_element_test, DocumentTest::SchemaElementTest),
    )))(i)?;
    */
    else {
        (i, KindTest::DocumentTestNone)
    };
    let i = s_char(')', i).or_cancel(b)?;
    b.end_node(Node::KindTest(kt));
    Ok(i)
}
// [194]    	PITest 	   ::=    	"processing-instruction" "(" (NCName | StringLiteral)? ")"
fn pi_test<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, _) = tuple((tag("processing-instruction"), tws, char('('), tws))(i)?;
    let (i, node) = if let Ok((i, name)) = ncname_str(i) {
        let name = b.string(name.as_str());
        (i, KindTest::PITestNCName(name))
    } else if let Ok((i, s)) = string_literal(i) {
        let s = b.string(&s);
        (i, KindTest::PITestString(s))
    } else {
        (i, KindTest::PITestNone)
    };
    let i = s_char(')', i)?;
    b.add_node(Node::KindTest(node));
    Ok(i)
}
// [199]    	ElementTest 	   ::=    	"element" "(" (ElementNameOrWildcard ("," TypeName "?"?)?)? ")"
fn name_or_wildcard<'a>(b: &mut B, i: &'a str) -> IResult<'a, Option<QNameId>> {
    if let Ok((i, eqname)) = eqname_element(b, i) {
        Ok((i, Some(eqname)))
    } else {
        map(char('*'), |_| None)(i)
    }
}
struct ElementTest {
    match_nilled: bool,
    match_name: bool,
    match_type: bool,
}
struct AttributeTest {
    match_name: bool,
    match_type: bool,
}
fn element_test<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tuple((tag("element"), tws, char('('), tws))(i)?.0;
    b.start_node();
    let (i, et) = element_test_end(b, i).or_cancel(b)?;
    b.end_node(Node::KindTest(KindTest::ElementTest {
        match_nilled: et.match_nilled,
        match_name: et.match_name,
        match_type: et.match_type,
    }));
    Ok(i)
}
fn element_test_end<'a>(b: &mut B, i: &'a str) -> IResult<'a, ElementTest> {
    let (i, name, type_name, match_nilled) = if let Ok((i, name)) = name_or_wildcard(b, i) {
        let (i, type_name, match_nilled) = if let Ok(i) = char_s(',', i) {
            let (i, type_name) = eqname_element(b, i)?;
            let i = tws(i)?.0;
            let (i, t) = opt(char('?'))(i)?;
            let match_nilled = t.is_some();
            (i, Some(type_name), match_nilled)
        } else {
            (i, None, false)
        };
        (i, name, type_name, match_nilled)
    } else {
        (i, None, None, false)
    };
    let i = s_char(')', i)?;
    if name.is_some() || type_name.is_some() {
        b.add_node(Node::ElementOrAttributeTest(ElementOrAttributeTest {
            name: name.unwrap_or_else(|| QNameId::from_usize(0)),
            r#type: type_name.unwrap_or_else(|| QNameId::from_usize(0)),
        }));
    }
    Ok((
        i,
        ElementTest {
            match_nilled,
            match_name: name.is_some(),
            match_type: type_name.is_some(),
        },
    ))
}
// [195]    	AttributeTest 	   ::=    	"attribute" "(" (AttribNameOrWildcard ("," TypeName)?)? ")"
fn attribute_test<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let i = tuple((tag("attribute"), tws, char('('), tws))(i)?.0;
    b.start_node();
    let (i, at) = attribute_test_end(b, i).or_cancel(b)?;
    b.end_node(Node::KindTest(KindTest::AttributeTest {
        match_name: at.match_name,
        match_type: at.match_type,
    }));
    Ok(i)
}
fn attribute_test_end<'a>(b: &mut B, i: &'a str) -> IResult<'a, AttributeTest> {
    let (i, name, type_name) = if let Ok((i, name)) = name_or_wildcard(b, i) {
        let (i, type_name) = if let Ok(i) = char_s(',', i) {
            let (i, type_name) = eqname_element(b, i)?;
            (i, Some(type_name))
        } else {
            (i, None)
        };
        (i, name, type_name)
    } else {
        (i, None, None)
    };
    let i = s_char(')', i)?;
    if name.is_some() || type_name.is_some() {
        b.add_node(Node::ElementOrAttributeTest(ElementOrAttributeTest {
            name: name.unwrap_or_else(|| QNameId::from_usize(0)),
            r#type: type_name.unwrap_or_else(|| QNameId::from_usize(0)),
        }));
    }
    Ok((
        i,
        AttributeTest {
            match_name: name.is_some(),
            match_type: type_name.is_some(),
        },
    ))
}
// [201]    	SchemaElementTest 	   ::=    	"schema-element" "(" ElementDeclaration ")"
// [207]    	FunctionTest 	   ::=    	Annotation* (AnyFunctionTest
// | TypedFunctionTest)
fn function_test<'a>(b: &mut B, mut i: &'a str) -> R<'a> {
    b.start_node();
    while let Ok(j) = annotation(b, i) {
        i = j;
    }
    let mut i = tuple((tag("function"), tws, char('('), tws))(i)
        .or_cancel(b)?
        .0;
    if let Ok((i, _)) = char::<_, ()>('*')(i) {
        let i = s_char(')', i).or_cancel(b)?;
        b.end_node(Node::ItemType(ItemType::FunctionTest));
        Ok(i)
    } else {
        while !i.starts_with(')') {
            let j = sequence_type(b, i).or_cancel(b)?;
            if let Ok(j) = s_char_s(',', j) {
                i = j;
            } else {
                i = j;
                break;
            }
        }
        let i = tuple((tws, char(')'), tws, tag("as"), tws))(i)
            .or_cancel(b)?
            .0;
        let i = sequence_type(b, i).or_cancel(b)?;
        b.end_node(Node::ItemType(ItemType::FunctionTest));
        Ok(i)
    }
}
// [210]    	MapTest 	   ::=    	AnyMapTest | TypedMapTest
// [211]    	AnyMapTest 	   ::=    	"map" "(" "*" ")"
// [212]    	TypedMapTest 	   ::=    	"map" "(" AtomicOrUnionType "," SequenceType ")"
fn map_test<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, _) = tuple((tag("map"), tws, char('('), tws))(i)?;
    if let Ok((i, _)) = tuple((char('*'), tws, char(')')))(i) {
        b.add_node(Node::ItemType(ItemType::AnyMapTest));
        return Ok(i);
    }
    let i = tws(i)?.0;
    let (i, name) = eqname_element(b, i)?;
    let i = s_char_s(',', i)?;
    b.start_node();
    let i = sequence_type(b, i)
        .and_then(|i| s_char(')', i))
        .or_cancel(b)?;
    b.end_node(Node::ItemType(ItemType::MapTest(name)));
    Ok(i)
}
// [213]    	ArrayTest 	   ::=    	AnyArrayTest | TypedArrayTest
// [214]    	AnyArrayTest 	   ::=    	"array" "(" "*" ")"
// [215]    	TypedArrayTest 	   ::=    	"array" "(" SequenceType ")"
fn array_test<'a>(b: &mut B, i: &'a str) -> R<'a> {
    let (i, _) = tuple((tag("array"), tws, char('('), tws))(i)?;
    if let Ok((i, _)) = tuple((char('*'), tws, char(')')))(i) {
        b.add_node(Node::ItemType(ItemType::ArrayTest));
        return Ok(i);
    }
    let i = tws(i)?.0;
    b.start_node();
    let i = sequence_type(b, i)
        .and_then(|i| s_char(')', i))
        .or_cancel(b)?;
    b.end_node(Node::ItemType(ItemType::ArrayTest));
    Ok(i)
}
fn eqname_function<'a>(b: &mut B, i: &'a str) -> IResult<'a, QNameId> {
    if let Ok((i, (ns, local_name))) = tuple((braced_uri_literal, ncname_str))(i) {
        Ok((i, b.eqname(Namespace(&ns), Prefix(""), local_name)))
    } else {
        let (i, (prefix, local_name)) = qname_str(i)?;
        if prefix == Prefix("") && RESERVED_FUNCTION_NAMES.contains(&local_name.as_str()) {
            // error
            tag("RESERVED_FUNCTION_NAMES")(i)?;
        }
        Ok((i, b.qname_function(prefix, local_name)))
    }
}

pub(crate) enum ParsedQName<'a> {
    Full {
        namespace: Cow<'a, str>,
        local_name: NCName<'a>,
    },
    Prefix {
        prefix: Prefix<'a>,
        local_name: NCName<'a>,
    },
}

fn parse_qname_str_(i: &str) -> IResult<ParsedQName> {
    if let Ok((i, (namespace, local_name))) = tuple((braced_uri_literal, ncname_str))(i) {
        Ok((
            i,
            ParsedQName::Full {
                namespace,
                local_name,
            },
        ))
    } else {
        let (i, (prefix, local_name)) = qname_str(i)?;
        Ok((i, ParsedQName::Prefix { prefix, local_name }))
    }
}

pub(crate) fn parse_qname(i: &str) -> Option<ParsedQName> {
    parse_qname_str_(i).ok().map(|t| t.1)
}
// [218]    	EQName 	   ::=    	QName | URIQualifiedName
fn var_declaration<'a>(b: &mut B, i: &'a str) -> IResult<'a, u16> {
    let i = char_s('$', i)?;
    let (i, qname) = eqname_var(b, i)?;
    let id = b.vars.declare_variable(qname);
    Ok((i, id))
}
fn var_reference<'a>(b: &mut B, i: &'a str) -> IResult<'a, u16> {
    let i = char_s('$', i)?;
    let (i, qname) = eqname_var(b, i)?;
    let id = b.vars.reference_variable(qname);
    Ok((i, id))
}
fn eqname_str(i: &str) -> IResult<&str> {
    recognize(parse_qname_str_)(i)
}
fn eqname_var<'a>(b: &mut B, i: &'a str) -> IResult<'a, QNameId> {
    parse_qname_str_(i).map(|(i, q)| (i, b.qname_var(q)))
}
fn eqname<'a>(b: &mut B, i: &'a str) -> IResult<'a, QNameId> {
    parse_qname_str_(i).map(|(i, q)| (i, b.qname_(q)))
}
fn eqname_element<'a>(b: &mut B, i: &'a str) -> IResult<'a, QNameId> {
    if let Ok((i, (ns, local_name))) = tuple((braced_uri_literal, ncname_str))(i) {
        Ok((i, b.eqname(Namespace(&ns), Prefix(""), local_name)))
    } else {
        let (i, (prefix, local_name)) = qname_str(i)?;
        Ok((i, b.qname_element(prefix, local_name)))
    }
}

pub(crate) fn parse_qname_str(qname: &str) -> Result<(Prefix, NCName), Error> {
    match qname_str(qname) {
        Ok((i, (prefix, local_name))) if i.is_empty() => Ok((prefix, local_name)),
        _ => Err(Error::foca(2)),
    }
}

pub(crate) fn qname_str(i: &str) -> IResult<(Prefix, NCName)> {
    map(
        tuple((ncname_str, opt(tuple((char(':'), ncname_str))))),
        |t| {
            if let Some(n) = t.1 {
                (t.0.as_prefix(), n.1)
            } else {
                (Prefix(""), t.0)
            }
        },
    )(i)
}
fn ncname_str(i: &str) -> IResult<NCName> {
    let (j, _) = take_while_m_n(1, 1, is_nc_name_start_char)(i)?;
    let (j, _) = take_while(is_nc_name_char)(j)?;
    let i = &i[..i.len() - j.len()];
    Ok((j, NCName(i)))
}
// [219]    	Integer 	   ::=    	Digits
fn integer_literal(i: &str) -> IResult<i64> {
    map_res(recognize(digit1), str::parse)(i)
}
// [222]    	String 	   ::=    	('"' (PredefinedEntityRef | CharRef | EscapeQuot | [^"&])* '"') | ("'" (PredefinedEntityRef | CharRef | EscapeApos | [^'&])* "'")
fn string_literal(i: &str) -> IResult<Cow<str>> {
    alt((
        delimited(
            char('"'),
            fold_many0(
                alt((
                    map(take_while1(|c| c != '"' && c != '&'), Cow::Borrowed),
                    predefined_entity_ref,
                    char_ref,
                    map(tag("&;"), Cow::Borrowed), // not in spec but in tests
                    map(tag("\"\""), |_| Cow::Borrowed("\"")),
                )),
                || Cow::Borrowed(""),
                join_cow,
            ),
            char('"'),
        ),
        delimited(
            char('\''),
            fold_many0(
                alt((
                    map(take_while1(|c| c != '\'' && c != '&'), Cow::Borrowed),
                    predefined_entity_ref,
                    char_ref,
                    map(tag("&;"), Cow::Borrowed), // not in spec but in tests
                    map(tag("''"), |_| Cow::Borrowed("'")),
                )),
                || Cow::Borrowed(""),
                join_cow,
            ),
            char('\''),
        ),
    ))(i)
}
#[test]
fn t() {
    assert_eq!(string_literal("\"\""), Ok(("", "".into())));
}
fn join_cow<'a>(mut a: Cow<'a, str>, b: Cow<'a, str>) -> Cow<'a, str> {
    if a.is_empty() {
        return b;
    }
    if b.is_empty() {
        return a;
    }
    a.to_mut().push_str(&b);
    a
}
// [224]    	BracedURILiteral 	   ::=    	"Q" "{" (PredefinedEntityRef | CharRef | [^&{}])* "}"
fn braced_uri_literal(i: &str) -> IResult<Cow<str>> {
    let (i, (_, b, _)) = tuple((
        tag("Q{"),
        fold_many0(
            alt((
                map(take_while1(|c| c != '&' && c != '{' && c != '}'), |s| {
                    Cow::Borrowed(s)
                }),
                predefined_entity_ref,
                char_ref,
            )),
            || Cow::Borrowed(""),
            join_cow,
        ),
        char('}'),
    ))(i)?;
    Ok((i, b))
}
// [225]    	PredefinedEntityRef 	   ::=    	"&" ("lt" | "gt" | "amp" | "quot" | "apos") ";" 	/* ws: explicit */
fn predefined_entity_ref(i: &str) -> IResult<Cow<str>> {
    delimited(
        char('&'),
        alt((
            map(tag("lt"), |_| Cow::Borrowed("<")),
            map(tag("gt"), |_| Cow::Borrowed(">")),
            map(tag("amp"), |_| Cow::Borrowed("&")),
            map(tag("quot"), |_| Cow::Borrowed("\"")),
            map(tag("apos"), |_| Cow::Borrowed("'")),
        )),
        char(';'),
    )(i)
}
// [228]    	ElementContentChar 	   ::=    	(Char - [{}<&])
fn element_content_chars(i: &str) -> IResult<&str> {
    take_while1(|c: char| !"{}<&".contains(c))(i)
}
// [233]    	CharRef 	   ::=    	[http://www.w3.org/TR/REC-xml#NT-CharRef]XML 	/* xgc: xml-version */
// [66]   	CharRef	   ::=   	'&#' [0-9]+ ';'
//			| '&#x' [0-9a-fA-F]+ ';'	[WFC: Legal Character]
fn char_ref(i: &str) -> IResult<Cow<str>> {
    fn d(i: &str, radix: u32) -> Result<Cow<str>, ()> {
        let mut s = String::new();
        match u32::from_str_radix(i, radix) {
            Ok(v) => {
                if let Ok(c) = char::try_from(v) {
                    s.push(c);
                } else {
                    return Err(());
                }
            }
            Err(_e) => return Err(()),
        }
        Ok(Cow::Owned(s))
    }
    terminated(
        alt((
            preceded(tag("&#"), map_res(digit1, |s| d(s, 10))),
            preceded(tag("&#x"), map_res(hex_digit1, |s| d(s, 16))),
        )),
        char(';'),
    )(i)
}
// [238]    	Digits 	   ::=    	[0-9]+
//
//
// XML
fn is_nc_name_start_char(c: char) -> bool {
    matches!(c, 'a'..='z' | 'A'..='Z' | '_' | 'À'..='Ö' | 'Ø'..='ö' | 'ø'..='\u{2ff}' | 'Ų'..='\u{37d}' | '\u{37f}'..='\u{1fff}')
    // TODO
    //        [#xC0-#xD6] | [#xD8-#xF6] | [#xF8-#x2FF] | [#x370-#x37D] | [#x37F-#x1FFF] | [#x200C-#x200D] | [#x2070-#x218F] | [#x2C00-#x2FEF] | [#x3001-#xD7FF] | [#xF900-#xFDCF] | [#xFDF0-#xFFFD] | [#x10000-#xEFFFF]
}
fn is_nc_name_char(c: char) -> bool {
    match c {
        '-' | '.' | '0'..='9' | '·' => true,
        '\u{0300}'..='\u{036f}' | '\u{203f}'..='\u{2040}' => true,
        c => is_nc_name_start_char(c),
    }
}
