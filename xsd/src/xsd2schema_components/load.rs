use super::parsed2mods;
use crate::{
    add_includes, adjust_schema,
    schema_components::SchemaComponents,
    tree_to_schema,
    xsd::xs::{e, g},
    xsd_validator::{self, XsdValidator},
    TreeToSchema,
};
use std::{
    collections::{btree_map::Entry, hash_map, BTreeMap, HashMap, HashSet},
    path::{Path, PathBuf},
    sync::Arc,
};
use xust_tree::qnames::QNameCollector;
use xust_xml::{
    read::{normalize_xml, parse_xml_from_file, EntityLoaderResult, ParserConfig},
    XmlVersion,
};

fn load_schema(
    input_file: &Path,
    qnames: QNameCollector,
    validator: Option<&XsdValidator>,
    config: Option<&ParserConfig>,
) -> std::result::Result<TreeToSchema, Box<dyn std::error::Error>> {
    let tree = if let Some(validator) = validator {
        let mut config = if let Some(config) = config {
            (*config).clone()
        } else {
            ParserConfig::default()
        };
        config.base_url = Some(input_file.into());
        let xml = (config.entity_loader)(None, input_file, XmlVersion::Xml10)?.text;
        validator.validate_to_tree(&xml, Some(&config))?
    } else {
        parse_xml_from_file(input_file, config, Some(qnames))?
    };
    let mut r = tree_to_schema(input_file, tree, validator.is_some())?;
    let mut todo = Vec::new();
    add_includes(&mut todo, input_file, &r.schemas);
    let mut schemas = HashSet::new();
    while let Some(input_file) = todo.pop() {
        if schemas.contains(&input_file) {
            continue;
        }
        let qnames = QNameCollector::from_qname_collection(r.qnames.clone());
        let tree = parse_xml_from_file(&input_file, None, Some(qnames))?;
        let s = tree_to_schema(&input_file, tree, validator.is_some())?;
        add_includes(&mut todo, &input_file, &s.schemas);
        for (k, v) in s.rev_prefixes {
            r.rev_prefixes.entry(k).or_insert(v);
        }
        for s in s.schemas {
            let s = adjust_schema(&r.ns, s)?;
            r.schemas.push(s);
        }
        schemas.insert(input_file);
    }
    Ok(r)
}

pub fn load_validator(
    xsd_paths: &[PathBuf],
    config: Option<&ParserConfig>,
) -> Result<xsd_validator::XsdValidator, Box<dyn std::error::Error>> {
    let validator = get_xsd_validator();
    let model = xsd2schema_components(xsd_paths, Some(&validator), config)?;
    xsd_validator::XsdValidator::build(&model.components)
}

fn xsd_bootstrap_entity_loader(
    _base_url: Option<&Path>,
    path: &Path,
    _parent_version: XmlVersion,
) -> xust_xml::error::Result<EntityLoaderResult> {
    let xsd_xsd = std::include_str!("../../xsd/www.w3.org/2001/XMLSchema/xsd.xsd");
    let xml_xsd = std::include_str!("../../xsd/www.w3.org/2001/xml.xsd");
    let xsd_dtd = std::include_str!("../../xsd/www.w3.org/2001/XMLSchema/XMLSchema.dtd");
    let datatypes_dtd = std::include_str!("../../xsd/www.w3.org/2001/XMLSchema/datatypes.dtd");
    let text = if path.as_os_str() == "xsd.xsd" {
        xsd_xsd
    } else if path.as_os_str() == "xml.xsd" {
        xml_xsd
    } else if path.as_os_str() == "XMLSchema.dtd" {
        xsd_dtd
    } else if path.as_os_str() == "datatypes.dtd" {
        datatypes_dtd
    } else {
        panic!("{} is not expected in the xsd bootstrap.", path.display());
    };
    Ok(EntityLoaderResult {
        base: None,
        encoding: None,
        text: normalize_xml(text.to_string()),
    })
}

// A validator for XSD files is built in.
pub(crate) fn get_xsd_validator() -> XsdValidator {
    lazy_static::lazy_static! {
        static ref VALIDATOR: XsdValidator = load_xsd_validator();
    }
    VALIDATOR.clone()
}

fn load_xsd_validator() -> XsdValidator {
    // a temporary function for loading a validator
    let xsd_paths = &["xsd.xsd".into(), "xml.xsd".into()];
    let config = ParserConfig {
        entity_loader: Arc::new(xsd_bootstrap_entity_loader),
        ..Default::default()
    };
    let model =
        xsd2schema_components_qnames(xsd_paths, QNameCollector::default(), Some(&config), None)
            .unwrap();
    let validator = xsd_validator::XsdValidator::build(&model.components)
        .expect("An error while loading a validator for xsd files is a fundamental problem.");
    let model = xsd2schema_components(xsd_paths, Some(&validator), Some(&config)).unwrap();
    xsd_validator::XsdValidator::build(&model.components).unwrap()
}

pub(super) struct Schemas {
    pub prefixes: HashMap<String, String>,
    pub schemas: BTreeMap<String, Vec<e::Schema>>,
    pub qnames: QNameCollector,
    // an unreliable set of namespace definitions, currently needed to
    // resolve QNames in facets
    pub namespace_definitions: HashMap<String, String>,
}

pub(super) fn load_schemas(
    input_files: &[PathBuf],
    mut qnames: QNameCollector,
    config: Option<&ParserConfig>,
    validator: Option<&XsdValidator>,
) -> Result<Schemas, Box<dyn std::error::Error>> {
    let mut prefixes = HashMap::new();
    let mut schemas = BTreeMap::new();
    let mut input_files: Vec<_> = input_files.into();
    let mut pos = 0;
    let mut nds = HashMap::new();
    let mut possible_schema_locations = HashMap::new();
    while pos < input_files.len() {
        let input_file = &input_files[pos];
        let mut r = load_schema(input_file, qnames, validator, config)?;
        for n in r.rev_prefixes {
            if let hash_map::Entry::Vacant(e) = nds.entry(n.0.clone()) {
                e.insert(n.1);
            }
        }
        qnames = QNameCollector::from_qname_collection(r.qnames);
        prefixes.insert(r.ns.clone(), r.prefix);
        let input_file = input_file.parent().unwrap().to_path_buf();
        for c in r.schemas.iter().flat_map(|s| &s.composition) {
            if let g::Composition::Import(import) = c {
                let import_ns = import.namespace.as_deref().unwrap_or("");
                if !schemas.contains_key(import_ns) {
                    if let Some(location) = &import.schema_location {
                        let input_file = input_file.join(location);
                        if input_file.exists() {
                            input_files.push(input_file);
                        } else {
                            possible_schema_locations
                                .insert(import_ns.to_owned(), location.to_owned());
                        }
                    }
                }
            }
        }
        match schemas.entry(r.ns) {
            Entry::Vacant(e) => {
                e.insert(r.schemas);
            }
            Entry::Occupied(mut e) => {
                e.get_mut().append(&mut r.schemas);
            }
        }
        pos += 1;
    }
    for schema in schemas.values().flatten() {
        for c in &schema.composition {
            if let g::Composition::Import(import) = c {
                let import_ns = import.namespace.as_deref().unwrap_or("");
                if !schemas.contains_key(import_ns) {
                    if let Some(location) = possible_schema_locations.get(import_ns) {
                        return Err(format!(
                            "No schema was provided for namespace '{}'. {} cannot be read. Please provide a local file.",
                            import_ns, location
                        )
                        .into());
                    } else {
                        return Err(format!(
                            "No schema was provided for namespace '{}'.",
                            import_ns
                        )
                        .into());
                    }
                }
            }
        }
    }
    Ok(Schemas {
        prefixes,
        schemas,
        qnames,
        namespace_definitions: nds,
    })
}

pub(crate) struct PrefixesAndSchemaComponents {
    pub prefixes: HashMap<String, String>,
    pub components: SchemaComponents,
}

pub(crate) fn xsd2schema_components(
    input_files: &[PathBuf],
    validator: Option<&XsdValidator>,
    config: Option<&ParserConfig>,
) -> Result<PrefixesAndSchemaComponents, Box<dyn std::error::Error>> {
    let collector = if let Some(validator) = validator {
        QNameCollector::from_qname_collection(validator.qnames().clone())
    } else {
        QNameCollector::default()
    };
    xsd2schema_components_qnames(input_files, collector, config, validator)
}

pub(crate) fn xsd2schema_components_qnames(
    input_files: &[PathBuf],
    qnames: QNameCollector,
    config: Option<&ParserConfig>,
    validator: Option<&XsdValidator>,
) -> Result<PrefixesAndSchemaComponents, Box<dyn std::error::Error>> {
    let mut schemas = load_schemas(input_files, qnames, config, validator)?;
    for prefix in schemas.prefixes.values_mut() {
        if prefix.is_empty() {
            *prefix = "default".to_string();
        }
    }
    Ok(parsed2mods(schemas)?)
}
