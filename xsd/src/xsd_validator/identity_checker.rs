use super::Error;
use crate::{
    identity_constraint_grammar::{Field, FieldPath, NameTest, SelectorPath},
    schema_components::{
        identity_constraints::{IdentityConstraintId, IdentityConstraints},
        IdentityConstraint, IdentityConstraintCategory,
    },
    Atomic,
};
use std::sync::Arc;
use xust_tree::qnames::QNameId;

pub(crate) type Result<T> = std::result::Result<T, Error>;

pub(crate) struct IdentityChecker {
    constraints: Arc<IdentityConstraints>,
    states: Vec<State>,
}

impl IdentityChecker {
    pub fn new(constraints: Arc<IdentityConstraints>) -> Self {
        Self {
            constraints,
            states: Vec::new(),
        }
    }
    /**
     * Each element in the document is passed to the IdentityChecker.
     */
    pub fn start_element(
        &mut self,
        qname: QNameId,
        constraints: &[IdentityConstraintId],
        is_simple: bool,
        errors: &mut Vec<Error>,
    ) -> Result<()> {
        // process the new qname in all the current states
        for s in &mut self.states {
            s.start_element(qname, false, is_simple, errors);
        }
        // add the new constraints as states
        for ic in constraints {
            let mut state = State::new(*ic, &self.constraints[*ic]);
            state.start_element(qname, true, is_simple, errors);
            self.states.push(state);
        }
        Ok(())
    }
    pub fn end_element(&mut self, errors: &mut Vec<Error>) {
        for s in &mut self.states {
            s.end_element(errors);
        }
        for state in &self.states {
            if state.depth == 0 && !state.keys.is_empty() {
                if let IdentityConstraintCategory::Keyref(key) = &state.constraint.category {
                    self.check_keyrefs(*key, &state.keys, errors);
                }
            }
        }
        // TODO: check for errors
        self.states.retain(|s| s.depth > 0);
    }
    fn check_keyrefs(&self, key: IdentityConstraintId, refs: &[Key], errors: &mut Vec<Error>) {
        let keys = if let Some(k) = self.states.iter().find(|s| s.id == key) {
            k
        } else {
            errors.push(Error::IdentityError {
                id: key,
                msg: format!("Cannot find key definition for {:?}.", key),
            });
            return;
        };
        for r in refs {
            if !keys.keys.contains(r) {
                errors.push(Error::IdentityError {
                    id: key,
                    msg: format!("Cannot find key '{:?}', only {:?}.", r, keys.keys),
                });
            }
        }
    }
    pub fn start_attribute(&mut self, qname: QNameId) {
        for s in &mut self.states {
            s.start_attribute(qname);
        }
    }
    pub fn end_attribute(&mut self) {
        for s in &mut self.states {
            s.end_attribute();
        }
    }
    pub fn add_value(&mut self, value: &Atomic) {
        for s in &mut self.states {
            s.add_value(value);
        }
    }
}

// state of on xpath expression
// The state comes into existence when an element starts in which identity
// constraints are defined.
// The identity contraint has two xpath expressions that work consecutively.
// A selector xpath may start with .// which indicates that the match can start
// in any child element. It might even start inside another child element.
// The matching only really starts once the first name test passes. When it
// does, a new SelectorState is created.
//
// A selector xpath can have multiple matches. Both of these may match
// initially.
//
// When an element matches the last step in the selector xpath, that element
// must contain one node for each field. For each field, a FieldState is
// created immediately.
//
// State progression for selectors.
#[derive(Debug)]
struct State {
    id: IdentityConstraintId,
    constraint: IdentityConstraint,
    selector_states: Vec<SelectorState>,
    // depth relative to the creation of this state
    depth: usize,
    keys: Vec<Key>,
}

#[derive(Debug, PartialEq)]
struct Key(Vec<Vec<Atomic>>);

impl State {
    fn new(id: IdentityConstraintId, constraint: &IdentityConstraint) -> Self {
        let mut selector_states = Vec::new();
        for (n, selector) in constraint.selector.0.iter().enumerate() {
            if selector.steps.is_empty() {
                let state = SelectorState::new(id, n, selector, &constraint.fields);
                selector_states.push(state);
            }
        }
        Self {
            id,
            constraint: constraint.clone(),
            selector_states,
            depth: 0,
            keys: Default::default(),
        }
    }
    fn start_element(
        &mut self,
        qname: QNameId,
        _lowest_level: bool,
        is_simple: bool,
        errors: &mut Vec<Error>,
    ) {
        self.depth += 1;
        for s in &mut self.selector_states {
            s.start_element(self.id, &self.constraint, qname, is_simple, errors);
        }
        // start a new selector state if the qname matches
        for (n, selector) in self.constraint.selector.0.iter().enumerate() {
            if selector_matches(selector, qname, self.depth) {
                let state = SelectorState::new(self.id, n, selector, &self.constraint.fields);
                self.selector_states.push(state);
            }
        }
    }
    fn end_element(&mut self, errors: &mut Vec<Error>) {
        self.depth -= 1;
        for s in &mut self.selector_states {
            s.end_element();
        }
        let keys = &mut self.keys;
        let keyref = matches!(
            self.constraint.category,
            IdentityConstraintCategory::Keyref(_)
        );
        self.selector_states.retain(|s| {
            if s.done() {
                if let Some(key) = s.get_key(keyref, errors) {
                    // add the new key
                    if keys.iter().any(|k| &key == k) {
                        if !keyref {
                            // disabled for now because it thinks xsd.xsd is invalid
                            /*
                            errors.push(Error::IdentityError(format!("Duplicate key {:?}", key)));
                            */
                        }
                    } else {
                        keys.push(key);
                    }
                }
            }
            !s.done()
        });
    }
    fn start_attribute(&mut self, qname: QNameId) {
        for s in &mut self.selector_states {
            s.start_attribute(&self.constraint.fields, qname);
        }
    }
    fn end_attribute(&mut self) {
        for s in &mut self.selector_states {
            s.end_attribute();
        }
    }
    fn add_value(&mut self, value: &Atomic) {
        for s in &mut self.selector_states {
            s.add_value(value);
        }
    }
}

fn selector_matches(path: &SelectorPath, qname: QNameId, depth: usize) -> bool {
    // If the selector does not start with './/' and this is not the lowest
    // level, there can be no match.
    if !(depth <= 2 || path.initial_double_slash) {
        return false;
    }
    let first = if let Some(first) = path.steps.first() {
        first
    } else {
        // the selector has no steps and was activated when it came into scope.
        return false;
    };
    name_test_matches(first, qname)
}

fn field_matches(path: &FieldPath, qname: QNameId, depth: usize, step: usize) -> bool {
    // If the selector does not start with './/' and this is not the lowest
    // level, there can be no match.
    if !(depth <= 2 || path.initial_double_slash) {
        return false;
    }
    let test = &path.steps[step];
    name_test_matches(test, qname)
}

fn name_test_matches(name_test: &NameTest, qname: QNameId) -> bool {
    match name_test {
        NameTest::Any => true,
        NameTest::QName(q) => *q == qname,
        NameTest::Namespace(ns) => qname.compare_namespace(*ns),
    }
}

#[derive(Debug)]
enum SelectorState {
    Started {
        selector_xpath: usize,
        step: usize,
        depth: usize,
    },
    Collecting {
        field_states: Vec<FieldState>,
        depth: usize,
    },
    NoMatch {
        selector_xpath: usize,
        // location where the non-match happened.
        // this is where matching kan pick up again
        step: usize,
        depth: usize,
    },
}

impl SelectorState {
    // Create a SelectorState in the Collecting state.
    fn collecting(id: IdentityConstraintId, fields: &[Field], depth: usize) -> Self {
        let mut field_states = Vec::with_capacity(fields.len());
        for f in fields {
            let state = FieldState::new(id, f);
            field_states.push(state);
        }
        Self::Collecting {
            field_states,
            depth,
        }
    }
    fn new(
        id: IdentityConstraintId,
        selector_xpath_pos: usize,
        selector_xpath: &SelectorPath,
        fields: &[Field],
    ) -> Self {
        // if the selector_xpath has only one step, the state is Collecting
        if selector_xpath.steps.len() < 2 {
            Self::collecting(id, fields, 1)
        } else {
            Self::Started {
                selector_xpath: selector_xpath_pos,
                step: 1,
                depth: 1,
            }
        }
    }
    // check if the selector continues to match
    // if all steps in the selector xpath have matched, report it
    fn start_element(
        &mut self,
        id: IdentityConstraintId,
        constraints: &IdentityConstraint,
        qname: QNameId,
        is_simple: bool,
        errors: &mut Vec<Error>,
    ) {
        match self {
            Self::Started {
                selector_xpath,
                step,
                depth,
            } => {
                *depth += 1;
                let selector = &constraints.selector.0[*selector_xpath];
                let test = &selector.steps[*step];
                if name_test_matches(test, qname) {
                    *step += 1;
                    if *step == constraints.selector.0.len() {
                        *self = Self::collecting(id, &constraints.fields, *depth);
                    }
                } else {
                    *self = Self::NoMatch {
                        selector_xpath: *selector_xpath,
                        step: *step,
                        depth: *depth,
                    }
                }
            }
            Self::Collecting {
                field_states,
                depth,
            } => {
                *depth += 1;
                for (n, f) in field_states.iter_mut().enumerate() {
                    f.start_element(&constraints.fields[n], qname, is_simple, errors);
                }
            }
            Self::NoMatch { depth, .. } => *depth += 1,
        }
    }
    fn end_element(&mut self) {
        match self {
            Self::Started { step, depth, .. } => {
                *depth -= 1;
                if step > depth {
                    // possible roll back the match
                    *step -= 1;
                }
            }
            Self::Collecting {
                field_states,
                depth,
            } => {
                *depth -= 1;
                for f in field_states {
                    f.end_element();
                }
            }
            Self::NoMatch {
                selector_xpath,
                step,
                depth,
            } => {
                *depth -= 1;
                if step >= depth {
                    *step = *depth;
                    // continue matching
                    *self = Self::Started {
                        selector_xpath: *selector_xpath,
                        step: *step,
                        depth: *depth,
                    };
                }
            }
        }
    }
    fn done(&self) -> bool {
        match self {
            Self::Started { depth, .. } => *depth == 0,
            Self::Collecting { depth, .. } => *depth == 0,
            Self::NoMatch { depth, .. } => *depth == 0,
        }
    }
    fn get_key(&self, is_keyref: bool, errors: &mut Vec<Error>) -> Option<Key> {
        if let Self::Collecting { field_states, .. } = self {
            let mut key = Vec::with_capacity(field_states.len());
            for v in field_states {
                match v.get_value(is_keyref) {
                    Ok(v) => {
                        if v.is_empty() && is_keyref {
                            // no full key found
                            return None;
                        }
                        key.push(v);
                    }
                    Err(e) => {
                        errors.push(e);
                    }
                }
            }
            Some(Key(key))
        } else {
            None
        }
    }
    fn start_attribute(&mut self, fields: &[Field], qname: QNameId) {
        if let Self::Collecting { field_states, .. } = self {
            for f in field_states.iter_mut().zip(fields) {
                f.0.start_attribute(f.1, qname);
            }
        }
    }
    fn end_attribute(&mut self) {
        if let Self::Collecting { field_states, .. } = self {
            for f in field_states {
                f.end_attribute();
            }
        }
    }
    fn add_value(&mut self, value: &Atomic) {
        if let Self::Collecting { field_states, .. } = self {
            for f in field_states.iter_mut() {
                f.add_value(value);
            }
        }
    }
}

#[derive(Debug)]
enum FieldStateState {
    Started,
    CollectingAttribute(Vec<Atomic>),
    CollectingElement(Vec<Atomic>),
}

#[derive(Debug)]
struct FieldState {
    id: IdentityConstraintId,
    states: Vec<FieldPathState>,
    value: Vec<Atomic>,
    state: FieldStateState,
    number_of_matches: usize,
    depth: usize,
    in_attribute: bool,
}

impl FieldState {
    fn new(id: IdentityConstraintId, f: &Field) -> Self {
        let mut number_of_matches = 0;
        let mut states = Vec::new();
        let mut collecting = false;
        for (path, f) in f.0.iter().enumerate() {
            if f.steps.is_empty() {
                collecting = true;
                number_of_matches = 1;
                // path is '.'
                states.push(FieldPathState {
                    path,
                    depth: 1,
                    step: 0,
                    matching: true,
                })
            }
        }
        let state = if collecting {
            number_of_matches = 1;
            FieldStateState::CollectingElement(Vec::new())
        } else {
            FieldStateState::Started
        };
        FieldState {
            id,
            states,
            state,
            value: Vec::new(),
            number_of_matches,
            depth: 1,
            in_attribute: false,
        }
    }
    // check if the selector continues to match
    // if all step in the selector xpath have matched, report it
    fn start_element(
        &mut self,
        field: &Field,
        qname: QNameId,
        is_simple: bool,
        errors: &mut Vec<Error>,
    ) {
        self.depth += 1;
        if let FieldStateState::Started = self.state {
            for state in &mut self.states {
                if state.start_element(field, qname) {
                    if !is_simple {
                        errors.push(Error::IdentityError {
                            id: self.id,
                            msg: "Complex field node.".into(),
                        });
                    }
                    self.number_of_matches += 1;
                    self.state = FieldStateState::CollectingElement(Vec::new());
                }
            }
            // start a new field state if the qname matches
            for (n, path) in field.0.iter().enumerate() {
                if field_matches(path, qname, self.depth, 0) {
                    if path.steps.len() == 1 {
                        if !is_simple {
                            errors.push(Error::IdentityError {
                                id: self.id,
                                msg: "Complex field node.".into(),
                            });
                        }
                        self.number_of_matches += 1;
                        self.state = FieldStateState::CollectingElement(Vec::new());
                        return;
                    } else {
                        self.states.push(FieldPathState {
                            path: n,
                            depth: 1,
                            step: 0,
                            matching: true,
                        })
                    }
                }
            }
        }
    }
    fn end_element(&mut self) {
        if self.depth == 0 {
            return;
        }
        self.depth -= 1;
        if let FieldStateState::Started = self.state {
            for state in &mut self.states {
                state.end_element();
            }
            self.states.retain(|s| s.depth > 0);
        }
    }
    fn start_attribute(&mut self, field: &Field, qname: QNameId) {
        self.in_attribute = true;
        // in the right circumstances, switch to Collecting
        for state in &mut self.states {
            if state.start_attribute(field, qname) {
                self.number_of_matches += 1;
                self.state = FieldStateState::CollectingAttribute(Vec::new());
            }
        }
        // start collecting if a field matches just an attribute
        for path in &field.0 {
            if path.is_attribute
                && path.steps.len() == 1
                && field_matches(path, qname, self.depth, 0)
            {
                self.number_of_matches += 1;
                self.state = FieldStateState::CollectingAttribute(Vec::new());
                return;
            }
        }
    }
    fn end_attribute(&mut self) {
        self.in_attribute = false;
        if let FieldStateState::CollectingAttribute(v) = &mut self.state {
            std::mem::swap(&mut self.value, v);
            self.state = FieldStateState::Started;
        }
    }
    fn add_value(&mut self, value: &Atomic) {
        match (self.in_attribute, &mut self.state) {
            (true, FieldStateState::CollectingAttribute(v)) => v.push(value.clone()),

            (false, FieldStateState::CollectingElement(v)) => v.push(value.clone()),
            _ => {}
        }
    }
    fn get_value(&self, is_keyref: bool) -> Result<Vec<Atomic>> {
        if self.number_of_matches == 0 {
            if is_keyref {
                return Ok(Vec::new());
            }
            return Err(Error::IdentityError {
                id: self.id,
                msg: "No matches.".into(),
            });
        }
        if self.number_of_matches > 1 {
            // disabled for now because it thinks xsd.xsd is invalid
            /*
            return Err(Error::IdentityError {
                id: self.id,
                msg: format!("Too many matches ({}).", self.number_of_matches),
            });
            */
        }
        if let FieldStateState::CollectingAttribute(v) = &self.state {
            return Ok(v.clone());
        }
        if let FieldStateState::CollectingElement(v) = &self.state {
            return Ok(v.clone());
        }
        Ok(self.value.clone())
    }
}

#[derive(Debug)]
struct FieldPathState {
    path: usize,
    matching: bool,
    depth: usize,
    step: usize,
}

impl FieldPathState {
    // return true when a match is complete
    fn start_element(&mut self, field: &Field, qname: QNameId) -> bool {
        if self.matching {
            let path = &field.0[self.path];
            if path.is_attribute && self.step + 1 == path.steps.len() {
                return false;
            }
            if field_matches(path, qname, self.depth, self.step) {
                self.step += 1;
                if self.step == path.steps.len() {
                    return true;
                }
            } else {
                self.matching = false;
            }
        }
        false
    }
    fn end_element(&mut self) {
        if self.step > self.depth {
            // possible roll back the match
            self.step = self.depth;
            self.matching = true;
        }
    }
    fn start_attribute(&mut self, field: &Field, qname: QNameId) -> bool {
        let path = &field.0[self.path];
        if path.is_attribute && self.step + 1 == path.steps.len() {
            return field_matches(path, qname, self.depth, self.step);
        }
        false
    }
}
