use xust_tree::{
    builder::{Builder, TypedBuilder},
    qnames::{NamespaceDefinition, QNameCollection, QNameId},
    tree::Tree,
    typed_value::TypeId,
    TreeBuilder,
};

pub(crate) struct DummyBuilder;

impl<T> Builder<Tree<T>> for DummyBuilder {
    fn init_document(&mut self) {}
    fn init_element(
        &mut self,
        _qname: QNameId,
        _type: TypeId,
        _namespace_definitions: &[NamespaceDefinition],
    ) {
    }
    fn init_top_attribute(&mut self, _qname: QNameId, _type: TypeId, _content: &str) {}
    fn init_top_text(&mut self, _content: &str) {}
    fn init_top_comment(&mut self, _content: &str) {}
    fn init_top_processing_instruction(&mut self, _target: QNameId, _content: &str) {}
    fn end_comment(&mut self) {}
    fn end_top_text(&mut self) {}
    fn end_processing_instruction(&mut self) {}
    fn push_str(&mut self, _s: &str) {}
    fn push(&mut self, _c: char) {}
    fn start_element(
        &mut self,
        _parent: usize,
        _qname: QNameId,
        _type: TypeId,
        _namespace_definitions: &[NamespaceDefinition],
    ) {
    }
    fn end_element(&mut self, _node_id: usize) {}
    fn start_attribute(&mut self, _qname: QNameId, _type: TypeId, _value: &str) {}
    fn process_element_open_tag(&mut self) -> usize {
        0
    }
    fn start_comment(&mut self, _node_id: usize) {}
    fn add_comment(&mut self, _node_id: usize, _content: &str) {}
    fn start_processing_instruction(&mut self, _node_id: usize, _target: QNameId) {}
    fn end_document(&mut self) {}
    fn build(self: Box<Self>, qnames: QNameCollection) -> Tree<T> {
        let mut builder = TreeBuilder::empty();
        builder.init_top_comment("dummy");
        Box::new(builder).build(qnames)
    }
    fn has_errors(&self) -> bool {
        false
    }
    fn errors(&self, _qnames: QNameCollection) -> Vec<String> {
        Vec::new()
    }
}

impl<T> TypedBuilder<Tree<T>, T> for DummyBuilder {
    fn push_typed_element(&mut self, _element: usize, _typed_value: T, _last: bool) {}
    fn push_typed_attribute(&mut self, _typed_value: T, _last: bool) {}
}
