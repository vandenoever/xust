use super::{Error, ValidatorState};
use crate::{
    fsm::{
        dfa::{All, Edge, Label, Node, DFA},
        Element, FSM,
    },
    parse::MaxOccurs,
    schema_components::{
        identity_constraints::IdentityConstraintId, Attribute, ProcessContents, Wildcard,
        WildcardId, Wildcards,
    },
    types::TypeId,
};
use std::sync::Arc;
use xust_tree::qnames::{QNameCollection, QNameId};

struct StackItem {
    dfa: usize,
    node_id: usize,
    tag: Option<QNameId>,
    // currently active sub-dfa, or none if not inside a sub-dfa
    all_dfa: Option<usize>,
    // how often has the <all/> been seen?
    all_count: usize,
    // how often has each top-level dfa been see in the current particle?
    all_dfa_count: Vec<usize>,
    r#default: Option<String>,
    // The type that follows from validation, it might be overridden by
    // xsi:type.
    actual_type: Option<TypeId>,
}

pub(super) struct Stack {
    qnames: QNameCollection,
    stack: Vec<StackItem>,
    dfas: Arc<Vec<Element>>,
    wildcards: Arc<Wildcards>,
}

impl Stack {
    pub fn new(fsm: &FSM) -> Self {
        Self {
            qnames: fsm.qnames().clone(),
            dfas: fsm.elements().clone(),
            wildcards: fsm.wildcards.clone(),
            stack: Vec::new(),
        }
    }
    pub fn reset(&mut self) {
        self.stack.clear();
        let start_element = self.dfas.len() - 1;
        if let DFA::Children { start_node, .. } = &self.dfas[start_element].dfa {
            self.stack.push(StackItem {
                dfa: start_element,
                node_id: *start_node,
                tag: None,
                all_dfa: None,
                all_count: 0,
                all_dfa_count: Vec::new(),
                r#default: None,
                actual_type: None,
            });
        }
    }
    pub fn get_state(&self) -> ValidatorState {
        if let Some(stack_item) = self.stack.last() {
            match &self.dfas[stack_item.dfa].dfa {
                DFA::Children { mixed, .. } | DFA::All(All { mixed, .. }) => {
                    if *mixed {
                        ValidatorState::ElementMixed
                    } else {
                        ValidatorState::ElementNotMixed
                    }
                }
                DFA::SimpleType(st) => {
                    ValidatorState::ElementSimpleType(*st, stack_item.actual_type)
                }
                DFA::Empty => ValidatorState::ElementNotMixed,
                DFA::Any(_) => ValidatorState::ElementMixed,
            }
        } else {
            panic!()
        }
    }
    fn dfa(&self) -> usize {
        if let Some(stack_item) = self.stack.last() {
            return stack_item.dfa;
        }
        panic!()
    }
    pub fn simple_type_default(&mut self) -> Option<String> {
        self.stack.last_mut().and_then(|s| s.r#default.take())
    }
    pub fn attributes(&self) -> (&[Attribute], &Option<WildcardId>) {
        let e = &self.dfas[self.dfa()];
        (&e.atts, &e.attribute_wildcard)
    }
    pub fn tag(&self) -> Option<QNameId> {
        if let Some(stack_item) = self.stack.last() {
            return stack_item.tag;
        }
        panic!()
    }
    pub fn push(
        &mut self,
        qname: QNameId,
        xsi_type: Option<usize>,
    ) -> Result<(&[Attribute], &[IdentityConstraintId], bool, Option<TypeId>), Error> {
        // check if element is allowed to start
        let stack_item = if let Some(stack_item) = self.stack.last_mut() {
            stack_item
        } else {
            return Err(Error::UnexpectedEvent);
        };
        let element = &self.dfas[stack_item.dfa];
        let (dfa, ics, r#default) = match &element.dfa {
            // no child element is allowed
            DFA::Empty | DFA::SimpleType(_) => {
                return Err(Error::UnexpectedElement(qname));
            }
            DFA::Any(w) => {
                let w = &self.wildcards[*w];
                let (a, b) = handle_any(qname, stack_item.dfa, w, &self.dfas)?;
                (a, b, &None)
            }
            DFA::Children { nodes, .. } => push_children(
                qname,
                stack_item,
                nodes,
                &self.dfas,
                &self.wildcards,
                &self.qnames,
            )?,
            DFA::All(all) => push_all(
                qname,
                stack_item,
                all,
                &self.dfas,
                &self.wildcards,
                &self.qnames,
            )?,
        };
        let mut actual_type = self.dfas[dfa].r#type;
        // TODO: check that the xsi_type is allowed
        let dfa = if let Some(xsi_type) = xsi_type {
            if let DFA::SimpleType(at) = &self.dfas[dfa].dfa {
                if let DFA::SimpleType(_) = &self.dfas[xsi_type].dfa {
                    actual_type = Some(*at);
                } else {
                    return Err(Error::InvalidXsiType);
                }
            }
            xsi_type
        } else {
            dfa
        };
        let e = &self.dfas[dfa];
        // put new element on the stack
        self.stack.push(StackItem {
            dfa,
            node_id: e.dfa.start_node(),
            tag: Some(qname),
            all_dfa: None,
            all_count: 0,
            all_dfa_count: Vec::new(),
            r#default: r#default.clone(),
            actual_type,
        });
        let is_simple = matches!(e.dfa, DFA::SimpleType(_));
        Ok((&e.atts, ics, is_simple, actual_type))
    }
    pub fn pop(&mut self) -> Result<(), Error> {
        let mut stack_item = if let Some(stack_item) = self.stack.pop() {
            stack_item
        } else {
            return Err(Error::UnexpectedEvent);
        };
        let dfa = &self.dfas[stack_item.dfa].dfa;
        if let DFA::Children { nodes, .. } = dfa {
            if !nodes[stack_item.node_id].end_node {
                return Err(Error::UnexpectedElementEnd(stack_item.tag.unwrap()));
            }
        } else if let DFA::All(all) = dfa {
            // is current dfa finished?
            if let Some(all_dfa) = stack_item.all_dfa {
                if let DFA::Children { nodes, .. } = &all.particles[all_dfa].dfa {
                    if !nodes[stack_item.node_id].end_node {
                        return Err(Error::UnexpectedElementEnd(stack_item.tag.unwrap()));
                    }
                }
                if is_all_finished(&stack_item, all) {
                    stack_item.all_count += 1;
                }
            }
            if stack_item.all_dfa_count.is_empty() || stack_item.all_count < all.min_occurs {
                return Err(Error::UnexpectedElementEnd(stack_item.tag.unwrap()));
            }
            for (i, p) in all.particles.iter().enumerate() {
                if stack_item.all_dfa_count[i] < p.min_occurs {
                    return Err(Error::UnexpectedElementEnd(stack_item.tag.unwrap()));
                }
            }
        }
        Ok(())
    }
    pub fn len(&self) -> usize {
        self.stack.len()
    }
}

// find the DFA edge labelled with the qname and use the destination
// as the next node. if there is no such edge find an <any/> edge
// return the index of the dfa corresponding to the element
fn push_children<'a>(
    qname: QNameId,
    stack_item: &mut StackItem,
    nodes: &'a [Node],
    dfas: &'a [Element],
    wildcards: &Wildcards,
    qnames: &QNameCollection,
) -> Result<(usize, &'a [IdentityConstraintId], &'a Option<String>), Error> {
    let edges = &nodes[stack_item.node_id].edges;
    if let Some(edge) = edges.iter().find(|e| e.label == Label::Tag(qname)) {
        // change dfa according to element
        stack_item.node_id = edge.destination;
        Ok((
            edge.dfa,
            edge.identity_constraints.as_ref(),
            &edge.r#default,
        ))
    } else if let Some(edge) = edges.iter().find(|e| e.label == Label::Any) {
        stack_item.node_id = edge.destination;
        if let DFA::Any(w) = dfas[edge.dfa].dfa {
            let w = &wildcards[w];
            let (a, b) = handle_any(qname, edge.dfa, w, dfas)?;
            Ok((a, b, &None))
        } else {
            panic!()
        }
    } else {
        let expected = list_tags(qnames, &nodes[stack_item.node_id].edges);
        Err(Error::UnexpectedElementWithExpected(qname, expected))
    }
}

fn is_all_finished(stack_item: &StackItem, all: &All) -> bool {
    for (i, p) in all.particles.iter().enumerate() {
        if stack_item.all_dfa_count[i] < p.min_occurs {
            return false;
        }
    }
    true
}

fn push_all<'a>(
    qname: QNameId,
    stack_item: &mut StackItem,
    all: &'a All,
    dfas: &'a [Element],
    wildcards: &Wildcards,
    qnames: &QNameCollection,
) -> Result<(usize, &'a [IdentityConstraintId], &'a Option<String>), Error> {
    // finish any open dfa
    if let Some(all_dfa) = stack_item.all_dfa {
        let dfa = &all.particles[all_dfa].dfa;
        let r = match_all_dfa(qname, stack_item, dfa, dfas, wildcards, qnames);
        if let Ok(dfa) = r {
            return Ok(dfa);
        }
        // error means this particle is done
        stack_item.all_dfa = None;
        if is_all_finished(stack_item, all) {
            stack_item.all_count += 1;
            for count in stack_item.all_dfa_count.iter_mut() {
                *count = 0;
            }
        }
    } else {
        stack_item.all_dfa_count.resize(all.particles.len(), 0);
    }
    // should another particle be parsed?
    if MaxOccurs::Bounded(stack_item.all_count) >= all.max_occurs {
        return Err(Error::TooMany);
    }
    for (i, dfa) in all.particles.iter().enumerate() {
        if MaxOccurs::Bounded(stack_item.all_dfa_count[i]) < dfa.max_occurs {
            stack_item.node_id = dfa.dfa.start_node();
            let r = match_all_dfa(qname, stack_item, &dfa.dfa, dfas, wildcards, qnames);
            if let Ok(dfa) = r {
                stack_item.all_dfa = Some(i);
                stack_item.all_dfa_count[i] += 1;
                return Ok(dfa);
            }
        }
    }
    Err(Error::TooMany)
}

fn match_all_dfa<'a>(
    qname: QNameId,
    stack_item: &mut StackItem,
    dfa: &'a DFA,
    dfas: &'a [Element],
    wildcards: &Wildcards,
    qnames: &QNameCollection,
) -> Result<(usize, &'a [IdentityConstraintId], &'a Option<String>), Error> {
    Ok(match &dfa {
        // skip validation on this child element and its contents
        DFA::Any(w) => {
            let w = &wildcards[*w];
            let (a, b) = handle_any(qname, stack_item.dfa, w, dfas)?;
            (a, b, &None)
        }
        DFA::Children { nodes, .. } => {
            push_children(qname, stack_item, nodes, dfas, wildcards, qnames)?
        }
        _ => panic!(),
    })
}

fn handle_any<'a>(
    qname: QNameId,
    dfa: usize,
    w: &Wildcard,
    dfas: &'a [Element],
) -> Result<(usize, &'a [IdentityConstraintId]), Error> {
    Ok(if w.process_contents == ProcessContents::Skip {
        // stay on the current <any/>
        (dfa, &[])
    } else {
        // validate according to top-level definition if the qname
        // is known, otherwise skip validation on this child element
        let def = find_global_element_dfa(dfas, qname);
        if let Some(def) = def {
            def
        } else if w.process_contents == ProcessContents::Strict {
            return Err(Error::UnexpectedElement(qname));
        } else {
            // Lax
            (dfa, &[])
        }
    })
}

//  1. reset to start dfa (the one with all top level elements
//  2. get the type of current element: mixed, elements, simpleType
//  3. get attributes of element at the top of the stack
//  4. get tag of element at the top of the stack
//  5. push a new state on the stack based on an element qnameid (possible error)
//  6. pop a state from the stack (possible error)

fn find_global_element_dfa(
    dfas: &[Element],
    qname: QNameId,
) -> Option<(usize, &[IdentityConstraintId])> {
    // if there is a definition for qname, use that otherwise
    // stay in this definition
    if let DFA::Children { nodes, .. } = &dfas.last().unwrap().dfa {
        nodes[0]
            .edges
            .iter()
            .find(|e| e.label == Label::Tag(qname))
            .map(|edge| (edge.dfa, edge.identity_constraints.as_ref()))
    } else {
        panic!()
    }
}

fn list_tags(qnames: &QNameCollection, edges: &[Edge]) -> String {
    let mut s = String::new();
    for edge in edges {
        use std::fmt::Write;
        if let Label::Tag(tag) = edge.label {
            write!(s, "{:?}, ", qnames.get(tag)).unwrap();
        } else {
            write!(s, "*, ").unwrap();
        }
    }
    s
}
