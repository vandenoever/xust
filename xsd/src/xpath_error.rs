#![allow(clippy::upper_case_acronyms)]
use backtrace::Backtrace;
use std::fmt::{Debug, Display};

#[derive(Clone)]
pub struct ShortBacktrace(Option<Box<Backtrace>>);

impl ShortBacktrace {
    fn new() -> Self {
        Self(Some(Box::new(Backtrace::new_unresolved())))
    }
}

#[derive(Clone, Debug)]
pub struct ErrorCode {
    code: u16,
    #[cfg(debug_assertions)]
    #[allow(dead_code)]
    debug: ShortBacktrace,
}

impl From<u16> for ErrorCode {
    fn from(code: u16) -> Self {
        Self {
            code,
            #[cfg(debug_assertions)]
            debug: ShortBacktrace::new(),
        }
    }
}

impl PartialEq for ErrorCode {
    fn eq(&self, o: &Self) -> bool {
        self.code == o.code
    }
}

impl Display for ErrorCode {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.code)?;
        Ok(())
    }
}

impl Debug for ShortBacktrace {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if let Some(backtrace) = &self.0 {
            let mut backtrace = backtrace.clone();
            backtrace.resolve();
            let cwd = std::env::current_dir().ok();
            let mut print_path =
                move |fmt: &mut std::fmt::Formatter<'_>, path: backtrace::BytesOrWideString<'_>| {
                    let cwd = cwd.clone();
                    let path = path.into_path_buf();
                    let path = if let Some(d) = cwd.and_then(|d| path.strip_prefix(d).ok()) {
                        d
                    } else {
                        &path
                    };
                    write!(fmt, "{} ", path.display())
                };
            for frame in backtrace
                .frames()
                .iter()
                .filter(|f| {
                    f.symbols().iter().filter_map(|s| s.filename()).all(|f| {
                        let p = f.to_string_lossy();
                        !p.contains("grammar/src/error.rs")
                    })
                })
                .take(1)
            {
                let mut fmt =
                    backtrace::BacktraceFmt::new(f, backtrace::PrintFmt::Short, &mut print_path);
                let mut frame_fmt = fmt.frame();
                frame_fmt.backtrace_frame(frame)?;
                drop(frame_fmt);
                fmt.finish()?;
            }
        }
        Ok(())
    }
}

#[non_exhaustive]
#[derive(Debug, Clone)]
pub enum Error {
    FOAP(ErrorCode),
    FOAR(ErrorCode),
    FOAY(ErrorCode),
    FOCA(ErrorCode),
    FOCH(ErrorCode),
    FODC(ErrorCode),
    FODF(ErrorCode),
    FODT(ErrorCode),
    FOER(ErrorCode),
    FOFD(ErrorCode),
    FOJS(ErrorCode),
    FONS(ErrorCode),
    FOQM(ErrorCode),
    FORG(ErrorCode),
    FORX(ErrorCode),
    FOTY(ErrorCode),
    FOUT(ErrorCode),
    FOXT(ErrorCode),
    SENR(ErrorCode),
    SEPM(ErrorCode),
    SERE(ErrorCode),
    SESU(ErrorCode),
    SXXP(ErrorCode),
    XPDY(ErrorCode),
    XPST(ErrorCode),
    XPTY(ErrorCode),
    XQDY(ErrorCode),
    XQST(ErrorCode),
    XQTY(ErrorCode),
    XTDE(ErrorCode),
    XTSE(ErrorCode),
    XXXX(ErrorCode),
    Unimplemented(Box<(String, ShortBacktrace)>),
    ImplementationError(ShortBacktrace),
    Other(Box<(String, ShortBacktrace)>),
}

impl Error {
    pub fn foap(code: u16) -> Self {
        Self::FOAP(code.into())
    }
    pub fn foar(code: u16) -> Self {
        Self::FOAR(code.into())
    }
    pub fn foay(code: u16) -> Self {
        Self::FOAY(code.into())
    }
    pub fn foca(code: u16) -> Self {
        Self::FOCA(code.into())
    }
    pub fn foch(code: u16) -> Self {
        Self::FOCH(code.into())
    }
    pub fn fodc(code: u16) -> Self {
        Self::FODC(code.into())
    }
    pub fn fodf(code: u16) -> Self {
        Self::FODF(code.into())
    }
    pub fn fodt(code: u16) -> Self {
        Self::FODT(code.into())
    }
    pub fn foer(code: u16) -> Self {
        Self::FOER(code.into())
    }
    pub fn fofd(code: u16) -> Self {
        Self::FOFD(code.into())
    }
    pub fn fojs(code: u16) -> Self {
        Self::FOJS(code.into())
    }
    pub fn fons(code: u16) -> Self {
        Self::FONS(code.into())
    }
    pub fn foqm(code: u16) -> Self {
        Self::FOQM(code.into())
    }
    pub fn forg(code: u16) -> Self {
        Self::FORG(code.into())
    }
    pub fn forx(code: u16) -> Self {
        Self::FORX(code.into())
    }
    pub fn foty(code: u16) -> Self {
        Self::FOTY(code.into())
    }
    pub fn fout(code: u16) -> Self {
        Self::FOUT(code.into())
    }
    pub fn foxt(code: u16) -> Self {
        Self::FOXT(code.into())
    }
    pub fn senr(code: u16) -> Self {
        Self::SENR(code.into())
    }
    pub fn sepm(code: u16) -> Self {
        Self::SEPM(code.into())
    }
    pub fn sere(code: u16) -> Self {
        Self::SERE(code.into())
    }
    pub fn sesu(code: u16) -> Self {
        Self::SESU(code.into())
    }
    pub fn sxxp(code: u16) -> Self {
        Self::SXXP(code.into())
    }
    pub fn xpdy(code: u16) -> Self {
        Self::XPDY(code.into())
    }
    pub fn xpst(code: u16) -> Self {
        Self::XPST(code.into())
    }
    pub fn xpty(code: u16) -> Self {
        Self::XPTY(code.into())
    }
    pub fn xqdy(code: u16) -> Self {
        Self::XQDY(code.into())
    }
    pub fn xqst(code: u16) -> Self {
        Self::XQST(code.into())
    }
    pub fn xqty(code: u16) -> Self {
        Self::XQTY(code.into())
    }
    pub fn xtde(code: u16) -> Self {
        Self::XTDE(code.into())
    }
    pub fn xtse(code: u16) -> Self {
        Self::XTSE(code.into())
    }
    pub fn xxxx(code: u16) -> Self {
        Self::XXXX(code.into())
    }
    pub fn is_static(&self) -> bool {
        matches!(self, Error::XPST(_) | Error::XQST(_))
    }
    pub fn unimplemented() -> Self {
        Self::Unimplemented(Box::new((String::new(), ShortBacktrace::new())))
    }
    pub fn unimplemented_msg(m: String) -> Self {
        Self::Unimplemented(Box::new((m, ShortBacktrace::new())))
    }
    pub fn implementation_error() -> Self {
        Self::ImplementationError(ShortBacktrace::new())
    }
    pub fn other(e: &dyn Display) -> Self {
        Self::Other(Box::new((e.to_string(), ShortBacktrace::new())))
    }
}

impl std::error::Error for Error {}
impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::FOAP(n) => write!(f, "FOAP{}", n),
            Self::FOAR(n) => write!(f, "FOAR{}", n),
            Self::FOAY(n) => write!(f, "FOAY{}", n),
            Self::FOCA(n) => write!(f, "FOCA{}", n),
            Self::FOCH(n) => write!(f, "FOCH{}", n),
            Self::FODC(n) => write!(f, "FODC{}", n),
            Self::FODF(n) => write!(f, "FODF{}", n),
            Self::FODT(n) => write!(f, "FODT{}", n),
            Self::FOER(n) => write!(f, "FOER{}", n),
            Self::FOFD(n) => write!(f, "FOFD{}", n),
            Self::FOJS(n) => write!(f, "FOJS{}", n),
            Self::FONS(n) => write!(f, "FONS{}", n),
            Self::FOQM(n) => write!(f, "FOQM{}", n),
            Self::FORG(n) => write!(f, "FORG{}", n),
            Self::FORX(n) => write!(f, "FORX{}", n),
            Self::FOTY(n) => write!(f, "FOTY{}", n),
            Self::FOUT(n) => write!(f, "FOUT{}", n),
            Self::FOXT(n) => write!(f, "FOXT{}", n),
            Self::SENR(n) => write!(f, "SENR{}", n),
            Self::SEPM(n) => write!(f, "SEPM{}", n),
            Self::SERE(n) => write!(f, "SERE{}", n),
            Self::SESU(n) => write!(f, "SESU{}", n),
            Self::SXXP(n) => write!(f, "SXXP{}", n),
            Self::XPDY(n) => write!(f, "XPDY{}", n),
            Self::XPST(n) => write!(f, "XPST{}", n),
            Self::XPTY(n) => write!(f, "XPTY{}", n),
            Self::XQDY(n) => write!(f, "XQDY{}", n),
            Self::XQST(n) => write!(f, "XQST{}", n),
            Self::XQTY(n) => write!(f, "XQTY{}", n),
            Self::XTDE(n) => write!(f, "XTDE{}", n),
            Self::XTSE(n) => write!(f, "XTSE{}", n),
            Self::XXXX(n) => write!(f, "XXXX{}", n),
            Self::Unimplemented(_) => write!(f, "Unimplemented"),
            Self::ImplementationError(_) => write!(f, "Implementation Error"),
            Self::Other(o) => write!(f, "{}", o.0),
        }
    }
}
impl std::str::FromStr for Error {
    type Err = String;
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        if s.len() != 8 {
            return Err(format!("Error code '{}' is not 8 characters.", s));
        }
        let n = s[4..]
            .parse::<u16>()
            .map_err(|e| format!("Cannot parse error number in '{}': {}", s, e))?;
        Ok(match &s[..4] {
            "FOAP" => Self::FOAP(n.into()),
            "FOAR" => Self::FOAR(n.into()),
            "FOAY" => Self::FOAY(n.into()),
            "FOCA" => Self::FOCA(n.into()),
            "FOCH" => Self::FOCH(n.into()),
            "FODC" => Self::FODC(n.into()),
            "FODF" => Self::FODF(n.into()),
            "FODT" => Self::FODT(n.into()),
            "FOER" => Self::FOER(n.into()),
            "FOFD" => Self::FOFD(n.into()),
            "FOJS" => Self::FOJS(n.into()),
            "FONS" => Self::FONS(n.into()),
            "FOQM" => Self::FOQM(n.into()),
            "FORG" => Self::FORG(n.into()),
            "FORX" => Self::FORX(n.into()),
            "FOTY" => Self::FOTY(n.into()),
            "FOUT" => Self::FOUT(n.into()),
            "FOXT" => Self::FOXT(n.into()),
            "SENR" => Self::SENR(n.into()),
            "SEPM" => Self::SEPM(n.into()),
            "SERE" => Self::SERE(n.into()),
            "SESU" => Self::SESU(n.into()),
            "SXXP" => Self::SXXP(n.into()),
            "XPDY" => Self::XPDY(n.into()),
            "XPST" => Self::XPST(n.into()),
            "XPTY" => Self::XPTY(n.into()),
            "XQDY" => Self::XQDY(n.into()),
            "XQST" => Self::XQST(n.into()),
            "XQTY" => Self::XQTY(n.into()),
            "XTDE" => Self::XTDE(n.into()),
            "XTSE" => Self::XTSE(n.into()),
            "XXXX" => Self::XXXX(n.into()),
            _ => return Err(format!("Unknown error code '{}'.", s)),
        })
    }
}
impl PartialEq for Error {
    fn eq(&self, o: &Self) -> bool {
        match (self, o) {
            (Self::FOAP(a), Self::FOAP(b)) => a == b,
            (Self::FOAR(a), Self::FOAR(b)) => a == b,
            (Self::FOAY(a), Self::FOAY(b)) => a == b,
            (Self::FOCA(a), Self::FOCA(b)) => a == b,
            (Self::FOCH(a), Self::FOCH(b)) => a == b,
            (Self::FODC(a), Self::FODC(b)) => a == b,
            (Self::FODF(a), Self::FODF(b)) => a == b,
            (Self::FODT(a), Self::FODT(b)) => a == b,
            (Self::FOER(a), Self::FOER(b)) => a == b,
            (Self::FOFD(a), Self::FOFD(b)) => a == b,
            (Self::FOJS(a), Self::FOJS(b)) => a == b,
            (Self::FONS(a), Self::FONS(b)) => a == b,
            (Self::FOQM(a), Self::FOQM(b)) => a == b,
            (Self::FORG(a), Self::FORG(b)) => a == b,
            (Self::FORX(a), Self::FORX(b)) => a == b,
            (Self::FOTY(a), Self::FOTY(b)) => a == b,
            (Self::FOUT(a), Self::FOUT(b)) => a == b,
            (Self::FOXT(a), Self::FOXT(b)) => a == b,
            (Self::SENR(a), Self::SENR(b)) => a == b,
            (Self::SEPM(a), Self::SEPM(b)) => a == b,
            (Self::SERE(a), Self::SERE(b)) => a == b,
            (Self::SESU(a), Self::SESU(b)) => a == b,
            (Self::SXXP(a), Self::SXXP(b)) => a == b,
            (Self::XPDY(a), Self::XPDY(b)) => a == b,
            (Self::XPST(a), Self::XPST(b)) => a == b,
            (Self::XPTY(a), Self::XPTY(b)) => a == b,
            (Self::XQDY(a), Self::XQDY(b)) => a == b,
            (Self::XQST(a), Self::XQST(b)) => a == b,
            (Self::XQTY(a), Self::XQTY(b)) => a == b,
            (Self::XTDE(a), Self::XTDE(b)) => a == b,
            (Self::XTSE(a), Self::XTSE(b)) => a == b,
            (Self::XXXX(a), Self::XXXX(b)) => a == b,
            _ => false,
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;
