use std::error::Error;
use std::fs::write;
use std::path::PathBuf;
use std::process::exit;
use xust_xsd::xsd2rs::xsd2rs;

struct Arguments {
    help: bool,
    input_files: Vec<PathBuf>,
    output_file: PathBuf,
    internal: bool,
}

fn parse_arguments() -> Result<Arguments, Box<dyn std::error::Error>> {
    let mut args = pico_args::Arguments::from_env();
    Ok(Arguments {
        help: args.contains(["-h", "--help"]),
        internal: args.contains("--internal"),
        output_file: args.value_from_str(["-o", "--output"])?,
        input_files: args.finish().iter().map(PathBuf::from).collect(),
    })
}

fn print_help() {
    println!(
        "\
        Usage: {} [OPTION]... -o OUTPUT_FILE INPUT_FILES",
        std::env::args().next().unwrap()
    );
    println!("  -h, --help");
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = parse_arguments()?;
    if args.help {
        print_help();
        return Ok(());
    }
    if args.input_files.is_empty() {
        print_help();
        exit(1);
    }
    stable_eyre::install()?;
    let rs = xsd2rs(&args.input_files, args.internal)?;
    write(&args.output_file, rs)
        .map_err(|e| format!("Cannot write to {}: {}", args.output_file.display(), e))?;
    Ok(())
}
