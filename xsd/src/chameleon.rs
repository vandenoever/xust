use xust_tree::qnames::{Namespace, QName};

use super::xsd::xs::{ct, e, g, local};

fn fix_opt_qname(tns: Namespace, qname: &mut Option<QName>) {
    if let Some(qname) = qname {
        if qname.to_ref().namespace().is_empty() {
            *qname = qname.change_namespace(tns);
        }
    }
}

fn nested_particle(tns: Namespace, np: &mut g::NestedParticle) {
    if let g::NestedParticle::Element(e) = np {
        fix_opt_qname(tns, &mut e.r#ref);
    }
}

fn simple_explicit_group(tns: Namespace, g: &mut ct::SimpleExplicitGroup) {
    for np in &mut g.nested_particle {
        nested_particle(tns, np);
    }
}

pub(crate) fn chameleon_transform(tns: Namespace, b: &mut e::Schema) {
    if !tns.0.is_empty() {
        b.target_namespace = Some(tns.0.to_string());
    }
    for s in &mut b.sequence2 {
        match &mut s.schema_top {
            g::SchemaTop::Element(e) => {
                fix_opt_qname(tns, &mut e.r#type);
            }
            g::SchemaTop::Attribute(a) => {
                fix_opt_qname(tns, &mut a.r#type);
            }
            g::SchemaTop::Redefinable(g::Redefinable::ComplexType(_ct)) => {}
            g::SchemaTop::Redefinable(g::Redefinable::Group(g)) => {
                if let local::Group3::Sequence(s) = &mut g.choice {
                    simple_explicit_group(tns, s);
                }
            }
            g::SchemaTop::Redefinable(g::Redefinable::AttributeGroup(g)) => {
                for ad in &mut g.attr_decls.choice {
                    match ad {
                        local::AttrDecls::Attribute(a) => {
                            fix_opt_qname(tns, &mut a.r#ref);
                            fix_opt_qname(tns, &mut a.r#type);
                        }
                        local::AttrDecls::AttributeGroup(_ag) => {}
                    }
                }
            }
            _ => {}
        }
    }
}
