#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
crate::use_xsd!();
use xust_tree::qnames::{NCName, Namespace, Prefix, QNameCollection};

type P = crate::parse::ParseEnv<QNames>;

pub struct QNames {
    qnames: QNameCollection,
    local: LocalQNames,
    xml: XmlQNames,
    xs: XsQNames,
}
pub fn qnames(qnames: QNameCollection) -> QNames {
    QNames {
        local: LocalQNames::new(&qnames),
        xml: XmlQNames::new(&qnames),
        xs: XsQNames::new(&qnames),
        qnames,
    }
}
ns!(XmlQNames "http://www.w3.org/XML/1998/namespace" =>
    lang => "lang"
);
ns!(XsQNames "http://www.w3.org/2001/XMLSchema" =>
    entities => "ENTITIES"
    entity => "ENTITY"
    id => "ID"
    idref => "IDREF"
    idrefs => "IDREFS"
    nc_name => "NCName"
    nmtoken => "NMTOKEN"
    nmtokens => "NMTOKENS"
    name => "Name"
    q_name => "QName"
    all => "all"
    all_nni => "allNNI"
    alternative => "alternative"
    annotation => "annotation"
    any => "any"
    any_atomic_type => "anyAtomicType"
    any_attribute => "anyAttribute"
    any_simple_type => "anySimpleType"
    any_uri => "anyURI"
    appinfo => "appinfo"
    assert => "assert"
    assertion => "assertion"
    attribute => "attribute"
    attribute_group => "attributeGroup"
    base64_binary => "base64Binary"
    basic_namespace_list => "basicNamespaceList"
    block_set => "blockSet"
    boolean => "boolean"
    byte => "byte"
    choice => "choice"
    complex_content => "complexContent"
    complex_type => "complexType"
    date => "date"
    date_time => "dateTime"
    day_time_duration => "dayTimeDuration"
    decimal => "decimal"
    default_open_content => "defaultOpenContent"
    derivation_control => "derivationControl"
    derivation_set => "derivationSet"
    documentation => "documentation"
    double => "double"
    duration => "duration"
    element => "element"
    enumeration => "enumeration"
    error => "error"
    explicit_timezone => "explicitTimezone"
    extension => "extension"
    field => "field"
    float => "float"
    form_choice => "formChoice"
    fraction_digits => "fractionDigits"
    full_derivation_set => "fullDerivationSet"
    g_day => "gDay"
    g_month => "gMonth"
    g_month_day => "gMonthDay"
    g_year => "gYear"
    g_year_month => "gYearMonth"
    group => "group"
    hex_binary => "hexBinary"
    import => "import"
    include => "include"
    int => "int"
    integer => "integer"
    key => "key"
    keyref => "keyref"
    language => "language"
    length => "length"
    list => "list"
    long => "long"
    max_exclusive => "maxExclusive"
    max_inclusive => "maxInclusive"
    max_length => "maxLength"
    min_exclusive => "minExclusive"
    min_inclusive => "minInclusive"
    min_length => "minLength"
    namespace_list => "namespaceList"
    negative_integer => "negativeInteger"
    non_negative_integer => "nonNegativeInteger"
    non_positive_integer => "nonPositiveInteger"
    normalized_string => "normalizedString"
    notation => "notation"
    open_content => "openContent"
    r#override => "override"
    pattern => "pattern"
    positive_integer => "positiveInteger"
    public => "public"
    qname_list => "qnameList"
    qname_list_a => "qnameListA"
    redefine => "redefine"
    reduced_derivation_control => "reducedDerivationControl"
    restriction => "restriction"
    schema => "schema"
    selector => "selector"
    sequence => "sequence"
    short => "short"
    simple_content => "simpleContent"
    simple_derivation_set => "simpleDerivationSet"
    simple_type => "simpleType"
    special_namespace_list => "specialNamespaceList"
    string => "string"
    time => "time"
    token => "token"
    total_digits => "totalDigits"
    type_derivation_control => "typeDerivationControl"
    union => "union"
    unique => "unique"
    unsigned_byte => "unsignedByte"
    unsigned_int => "unsignedInt"
    unsigned_long => "unsignedLong"
    unsigned_short => "unsignedShort"
    untyped => "untyped"
    untyped_atomic => "untypedAtomic"
    white_space => "whiteSpace"
    xpath_default_namespace => "xpathDefaultNamespace"
    year_month_duration => "yearMonthDuration"
);
ns!(LocalQNames "" =>
    r#abstract => "abstract"
    applies_to_empty => "appliesToEmpty"
    attribute_form_default => "attributeFormDefault"
    base => "base"
    block => "block"
    block_default => "blockDefault"
    r#default => "default"
    default_attributes => "defaultAttributes"
    default_attributes_apply => "defaultAttributesApply"
    element_form_default => "elementFormDefault"
    r#final => "final"
    final_default => "finalDefault"
    fixed => "fixed"
    form => "form"
    id => "id"
    inheritable => "inheritable"
    item_type => "itemType"
    max_occurs => "maxOccurs"
    member_types => "memberTypes"
    min_occurs => "minOccurs"
    mixed => "mixed"
    mode => "mode"
    name => "name"
    namespace => "namespace"
    nillable => "nillable"
    not_namespace => "notNamespace"
    not_q_name => "notQName"
    process_contents => "processContents"
    public => "public"
    r#ref => "ref"
    refer => "refer"
    schema_location => "schemaLocation"
    source => "source"
    substitution_group => "substitutionGroup"
    system => "system"
    target_namespace => "targetNamespace"
    test => "test"
    r#type => "type"
    r#use => "use"
    value => "value"
    version => "version"
    xpath => "xpath"
    xpath_default_namespace => "xpathDefaultNamespace"
);
pub mod xml {
    pub mod local {
        crate::use_xsd!();
    }
    pub mod g {
        crate::use_xsd!();
    }
    pub mod ct {
        crate::use_xsd!();
    }
    pub mod e {
        crate::use_xsd!();
    }
}
pub mod xs {
    pub mod local {
        crate::use_xsd!();
        choice!(simple_restriction_model1 SimpleRestrictionModel1 -
            MinExclusive(super::ct::Facet): r xs min_exclusive super::ct::facet,
            MinInclusive(super::ct::Facet): r xs min_inclusive super::ct::facet,
            MaxExclusive(super::ct::Facet): r xs max_exclusive super::ct::facet,
            MaxInclusive(super::ct::Facet): r xs max_inclusive super::ct::facet,
            TotalDigits(super::e::TotalDigits): r - - super::e::total_digits,
            FractionDigits(super::ct::NumFacet): r xs fraction_digits super::ct::num_facet,
            Length(super::ct::NumFacet): r xs length super::ct::num_facet,
            MinLength(super::ct::NumFacet): r xs min_length super::ct::num_facet,
            MaxLength(super::ct::NumFacet): r xs max_length super::ct::num_facet,
            Enumeration(super::ct::NoFixedFacet): r xs enumeration super::ct::no_fixed_facet,
            WhiteSpace(super::e::WhiteSpace): r - - super::e::white_space,
            Pattern(super::e::Pattern): r - - super::e::pattern,
            Assertion(super::ct::Assertion): r xs assertion super::ct::assertion,
            ExplicitTimezone(super::e::ExplicitTimezone): r - - super::e::explicit_timezone
        );
        choice!(attr_decls AttrDecls -
            Attribute(super::ct::Attribute): r xs attribute super::ct::attribute,
            AttributeGroup(super::ct::AttributeGroupRef): r xs attribute_group super::ct::attribute_group_ref
        );
        sequence!(default complex_type_model ComplexTypeModel -
            open_content(super::e::OpenContent): o - - super::e::open_content,
            type_def_particle(super::g::TypeDefParticle): o + - super::g::type_def_particle,
            attr_decls(super::g::AttrDecls): d + - super::g::attr_decls,
            assertions(super::g::Assertions): d + - super::g::assertions
        );
        choice!(real_group1 RealGroup1 -
            All(super::ct::All): r xs all super::ct::all,
            Choice(super::ct::ExplicitGroup): r xs choice super::ct::explicit_group,
            Sequence(super::ct::ExplicitGroup): r xs sequence super::ct::explicit_group
        );
        choice!(all_model AllModel -
            Element(super::ct::LocalElement): r xs element super::ct::local_element,
            Any(super::e::Any): r - - super::e::any,
            Group(Group): r xs group group
        );
        choice!(simple_restriction_model SimpleRestrictionModel -
            Choice(SimpleRestrictionModel1): r + - simple_restriction_model1,
            Any(Unit): r - - unit
        );
        sequence!(complex_restriction_type2 ComplexRestrictionType2 -
            open_content(super::e::OpenContent): o - - super::e::open_content,
            type_def_particle(super::g::TypeDefParticle): r + - super::g::type_def_particle
        );
        choice!(default: SimpleRestrictionModel restriction_type1 RestrictionType1 -
            Sequence(ComplexRestrictionType2): r + - complex_restriction_type2,
            SimpleRestrictionModel(super::g::SimpleRestrictionModel): d + - super::g::simple_restriction_model
        );
        choice!(complex_restriction_type1 ComplexRestrictionType1 -
            Sequence(ComplexRestrictionType2): r + - complex_restriction_type2
        );
        choice!(default: SimpleRestrictionModel simple_restriction_type1 SimpleRestrictionType1 -
            SimpleRestrictionModel(super::g::SimpleRestrictionModel): d + - super::g::simple_restriction_model
        );
        choice!(element3 Element3 -
            SimpleType(super::ct::LocalSimpleType): r xs simple_type super::ct::local_simple_type,
            ComplexType(super::ct::LocalComplexType): r xs complex_type super::ct::local_complex_type
        );
        choice!(group3 Group3 -
            All(All1): r xs all all1,
            Choice(super::ct::SimpleExplicitGroup): r xs choice super::ct::simple_explicit_group,
            Sequence(super::ct::SimpleExplicitGroup): r xs sequence super::ct::simple_explicit_group
        );
        sequence!(keyref1 Keyref1 -
            selector(super::e::Selector): r - - super::e::selector,
            field(super::e::Field): v - - super::e::field
        );
        sequence!(schema1 Schema1 -
            default_open_content(super::e::DefaultOpenContent): r - - super::e::default_open_content,
            annotation(super::e::Annotation): v - - super::e::annotation
        );
        sequence!(schema2 Schema2 -
            schema_top(super::g::SchemaTop): r + - super::g::schema_top,
            annotation(super::e::Annotation): v - - super::e::annotation
        );
        choice!(complex_content1 ComplexContent1 -
            Restriction(super::ct::ComplexRestrictionType): r xs restriction super::ct::complex_restriction_type,
            Extension(super::ct::ExtensionType): r xs extension super::ct::extension_type
        );
        choice!(simple_content1 SimpleContent1 -
            Restriction(super::ct::SimpleRestrictionType): r xs restriction super::ct::simple_restriction_type,
            Extension(super::ct::SimpleExtensionType): r xs extension super::ct::simple_extension_type
        );
        choice!(redefine Redefine -
            Annotation(super::e::Annotation): r - - super::e::annotation,
            Redefinable(super::g::Redefinable): r + - super::g::redefinable
        );
        sequence!(documentation Documentation m
            any(Unit): r - - unit
        );
        choice!(annotation Annotation -
            Appinfo(super::e::Appinfo): r - - super::e::appinfo,
            Documentation(super::e::Documentation): r - - super::e::documentation
        );
        complexType!(group Group 105 -
            attributes {
                local id id o - String,
                local max_occurs max_occurs o - MaxOccurs,
                local min_occurs min_occurs o - usize,
                local r#ref r#ref r - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        complexType!(all1 All1 116 -
            attributes {
                local id id o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(AllModel): v + - all_model
        );
    }
    pub mod g {
        crate::use_xsd!();
        sequence!(default any_type AnyType m
            any(Unit): v - - unit
        );
        choice!(composition Composition -
            Include(super::e::Include): r - - super::e::include,
            Import(super::e::Import): r - - super::e::import,
            Redefine(super::e::Redefine): r - - super::e::redefine,
            Override(super::e::Override): r - - super::e::r#override,
            Annotation(super::e::Annotation): r - - super::e::annotation
        );
        choice!(schema_top SchemaTop -
            Redefinable(Redefinable): r + - redefinable,
            Element(super::ct::TopLevelElement): r xs element super::ct::top_level_element,
            Attribute(super::ct::TopLevelAttribute): r xs attribute super::ct::top_level_attribute,
            Notation(super::e::Notation): r - - super::e::notation
        );
        choice!(redefinable Redefinable -
            SimpleType(super::ct::TopLevelSimpleType): r xs simple_type super::ct::top_level_simple_type,
            ComplexType(super::ct::TopLevelComplexType): r xs complex_type super::ct::top_level_complex_type,
            Group(super::ct::NamedGroup): r xs group super::ct::named_group,
            AttributeGroup(super::ct::NamedAttributeGroup): r xs attribute_group super::ct::named_attribute_group
        );
        choice!(type_def_particle TypeDefParticle -
            Group(super::ct::GroupRef): r xs group super::ct::group_ref,
            All(super::ct::All): r xs all super::ct::all,
            Choice(super::ct::ExplicitGroup): r xs choice super::ct::explicit_group,
            Sequence(super::ct::ExplicitGroup): r xs sequence super::ct::explicit_group
        );
        choice!(nested_particle NestedParticle -
            Element(super::ct::LocalElement): r xs element super::ct::local_element,
            Group(super::ct::GroupRef): r xs group super::ct::group_ref,
            Choice(super::ct::ExplicitGroup): r xs choice super::ct::explicit_group,
            Sequence(super::ct::ExplicitGroup): r xs sequence super::ct::explicit_group,
            Any(super::e::Any): r - - super::e::any
        );
        choice!(particle Particle -
            Element(super::ct::LocalElement): r xs element super::ct::local_element,
            Group(super::ct::GroupRef): r xs group super::ct::group_ref,
            All(super::ct::All): r xs all super::ct::all,
            Choice(super::ct::ExplicitGroup): r xs choice super::ct::explicit_group,
            Sequence(super::ct::ExplicitGroup): r xs sequence super::ct::explicit_group,
            Any(super::e::Any): r - - super::e::any
        );
        sequence!(default attr_decls AttrDecls -
            choice(super::local::AttrDecls): v + - super::local::attr_decls,
            any_attribute(super::e::AnyAttribute): o - - super::e::any_attribute
        );
        sequence!(default assertions Assertions -
            assert(super::ct::Assertion): v xs assert super::ct::assertion
        );
        choice!(default: Sequence complex_type_model ComplexTypeModel -
            SimpleContent(super::e::SimpleContent): r - - super::e::simple_content,
            ComplexContent(super::e::ComplexContent): r - - super::e::complex_content,
            Sequence(super::local::ComplexTypeModel): d + - super::local::complex_type_model
        );
        sequence!(default all_model AllModel -
            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::AllModel): v + - super::local::all_model
        );
        choice!(identity_constraint IdentityConstraint -
            Unique(super::ct::Keybase): r xs unique super::ct::keybase,
            Key(super::ct::Keybase): r xs key super::ct::keybase,
            Keyref(super::e::Keyref): r - - super::e::keyref
        );
        choice!(simple_derivation SimpleDerivation -
            Restriction(super::e::Restriction): r - - super::e::restriction,
            List(super::e::List): r - - super::e::list,
            Union(super::e::Union): r - - super::e::union
        );
        sequence!(default simple_restriction_model SimpleRestrictionModel -
            simple_type(super::ct::LocalSimpleType): o xs simple_type super::ct::local_simple_type,
            choice(super::local::SimpleRestrictionModel): v + - super::local::simple_restriction_model
        );
    }
    pub mod ct {
        crate::use_xsd!();
        simpleType!(any_simple_type AnySimpleType);
        simpleType!(any_atomic_type AnyAtomicType);
        simpleType!(idrefs Idrefs);
        simpleType!(entities Entities);
        simpleType!(nmtokens Nmtokens);
        complexType!(open_attrs OpenAttrs 52 -);
        complexType!(annotated Annotated 53 -
            attributes {
                local id id o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        simpleType!(form_choice FormChoice);
        simpleType!(derivation_set DerivationSet);
        simpleType!(full_derivation_set FullDerivationSet);
        simpleType!(all_nni AllNni);
        complexType!(attribute Attribute 58 -
            attributes {
                local r#default r#default o - String,
                local fixed fixed o - String,
                local form form o - String,
                local id id o - String,
                local inheritable inheritable o - bool,
                local name name o - String,
                local r#ref r#ref o - QName,
                local target_namespace target_namespace o - String,
                local r#type r#type o - QName,
                local r#use r#use o "optional" String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            simple_type(LocalSimpleType): o xs simple_type local_simple_type
        );
        complexType!(top_level_attribute TopLevelAttribute 59 -
            attributes {
                local r#default r#default o - String,
                local fixed fixed o - String,
                local id id o - String,
                local inheritable inheritable o - bool,
                local name name r - String,
                local r#type r#type o - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            simple_type(LocalSimpleType): o xs simple_type local_simple_type
        );
        complexType!(assertion Assertion 60 -
            attributes {
                local id id o - String,
                local test test o - String,
                local xpath_default_namespace xpath_default_namespace o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        complexType!(complex_type ComplexType 61 -
            attributes {
                local r#abstract r#abstract o "false" bool,
                local block block o - String,
                local default_attributes_apply default_attributes_apply o "true" bool,
                local r#final r#final o - String,
                local id id o - String,
                local mixed mixed o - bool,
                local name name o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            complex_type_model(super::g::ComplexTypeModel): d + - super::g::complex_type_model
        );
        complexType!(top_level_complex_type TopLevelComplexType 62 -
            attributes {
                local r#abstract r#abstract o "false" bool,
                local block block o - String,
                local default_attributes_apply default_attributes_apply o "true" bool,
                local r#final r#final o - String,
                local id id o - String,
                local mixed mixed o - bool,
                local name name r - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            complex_type_model(super::g::ComplexTypeModel): d + - super::g::complex_type_model
        );
        complexType!(local_complex_type LocalComplexType 63 -
            attributes {
                local default_attributes_apply default_attributes_apply o "true" bool,
                local id id o - String,
                local mixed mixed o - bool
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            complex_type_model(super::g::ComplexTypeModel): d + - super::g::complex_type_model
        );
        complexType!(restriction_type RestrictionType 64 -
            attributes {
                local base base r - QName,
                local id id o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::RestrictionType1): o + - super::local::restriction_type1,
            attr_decls(super::g::AttrDecls): d + - super::g::attr_decls,
            assertions(super::g::Assertions): d + - super::g::assertions
        );
        complexType!(complex_restriction_type ComplexRestrictionType 65 -
            attributes {
                local base base r - QName,
                local id id o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::ComplexRestrictionType1): o + - super::local::complex_restriction_type1,
            attr_decls(super::g::AttrDecls): d + - super::g::attr_decls,
            assertions(super::g::Assertions): d + - super::g::assertions
        );
        complexType!(extension_type ExtensionType 66 -
            attributes {
                local base base r - QName,
                local id id o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            open_content(super::e::OpenContent): o - - super::e::open_content,
            type_def_particle(super::g::TypeDefParticle): o + - super::g::type_def_particle,
            attr_decls(super::g::AttrDecls): d + - super::g::attr_decls,
            assertions(super::g::Assertions): d + - super::g::assertions
        );
        complexType!(simple_restriction_type SimpleRestrictionType 67 -
            attributes {
                local base base r - QName,
                local id id o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::SimpleRestrictionType1): o + - super::local::simple_restriction_type1,
            attr_decls(super::g::AttrDecls): d + - super::g::attr_decls,
            assertions(super::g::Assertions): d + - super::g::assertions
        );
        complexType!(simple_extension_type SimpleExtensionType 68 -
            attributes {
                local base base r - QName,
                local id id o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            attr_decls(super::g::AttrDecls): d + - super::g::attr_decls,
            assertions(super::g::Assertions): d + - super::g::assertions
        );
        simpleType!(block_set BlockSet);
        complexType!(element Element 70 -
            attributes {
                local r#abstract r#abstract o "false" bool,
                local block block o - String,
                local r#default r#default o - String,
                local r#final r#final o - String,
                local fixed fixed o - String,
                local form form o - String,
                local id id o - String,
                local max_occurs max_occurs o "1" MaxOccurs,
                local min_occurs min_occurs o "1" usize,
                local name name o - String,
                local nillable nillable o - bool,
                local r#ref r#ref o - QName,
                local substitution_group substitution_group ov - QName,
                local target_namespace target_namespace o - String,
                local r#type r#type o - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::Element3): o + - super::local::element3,
            alternative(AltType): v xs alternative alt_type,
            identity_constraint(super::g::IdentityConstraint): v + - super::g::identity_constraint
        );
        complexType!(top_level_element TopLevelElement 71 -
            attributes {
                local r#abstract r#abstract o "false" bool,
                local block block o - String,
                local r#default r#default o - String,
                local r#final r#final o - String,
                local fixed fixed o - String,
                local id id o - String,
                local name name r - String,
                local nillable nillable o - bool,
                local substitution_group substitution_group ov - QName,
                local r#type r#type o - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::Element3): o + - super::local::element3,
            alternative(AltType): v xs alternative alt_type,
            identity_constraint(super::g::IdentityConstraint): v + - super::g::identity_constraint
        );
        complexType!(local_element LocalElement 72 -
            attributes {
                local block block o - String,
                local r#default r#default o - String,
                local fixed fixed o - String,
                local form form o - String,
                local id id o - String,
                local max_occurs max_occurs o "1" MaxOccurs,
                local min_occurs min_occurs o "1" usize,
                local name name o - String,
                local nillable nillable o - bool,
                local r#ref r#ref o - QName,
                local target_namespace target_namespace o - String,
                local r#type r#type o - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::Element3): o + - super::local::element3,
            alternative(AltType): v xs alternative alt_type,
            identity_constraint(super::g::IdentityConstraint): v + - super::g::identity_constraint
        );
        complexType!(alt_type AltType 73 -
            attributes {
                local id id o - String,
                local test test o - String,
                local r#type r#type o - QName,
                local xpath_default_namespace xpath_default_namespace o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::Element3): o + - super::local::element3
        );
        complexType!(group Group 74 -
            attributes {
                local id id o - String,
                local max_occurs max_occurs o "1" MaxOccurs,
                local min_occurs min_occurs o "1" usize,
                local name name o - String,
                local r#ref r#ref o - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            particle(super::g::Particle): v + - super::g::particle
        );
        complexType!(real_group RealGroup 75 -
            attributes {
                local id id o - String,
                local max_occurs max_occurs o "1" MaxOccurs,
                local min_occurs min_occurs o "1" usize,
                local name name o - String,
                local r#ref r#ref o - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::RealGroup1): o + - super::local::real_group1
        );
        complexType!(named_group NamedGroup 76 -
            attributes {
                local id id o - String,
                local name name r - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::Group3): r + - super::local::group3
        );
        complexType!(group_ref GroupRef 77 -
            attributes {
                local id id o - String,
                local max_occurs max_occurs o "1" MaxOccurs,
                local min_occurs min_occurs o "1" usize,
                local r#ref r#ref r - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        complexType!(explicit_group ExplicitGroup 78 -
            attributes {
                local id id o - String,
                local max_occurs max_occurs o "1" MaxOccurs,
                local min_occurs min_occurs o "1" usize
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            nested_particle(super::g::NestedParticle): v + - super::g::nested_particle
        );
        complexType!(simple_explicit_group SimpleExplicitGroup 79 -
            attributes {
                local id id o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            nested_particle(super::g::NestedParticle): v + - super::g::nested_particle
        );
        complexType!(all All 80 -
            attributes {
                local id id o - String,
                local max_occurs max_occurs o "1" MaxOccurs,
                local min_occurs min_occurs o "1" usize
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            choice(super::local::AllModel): v + - super::local::all_model
        );
        complexType!(wildcard Wildcard 81 -
            attributes {
                local id id o - String,
                local namespace namespace o - String,
                local not_namespace not_namespace ov - String,
                local process_contents process_contents o "strict" String
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        simpleType!(namespace_list NamespaceList);
        simpleType!(basic_namespace_list BasicNamespaceList);
        simpleType!(special_namespace_list SpecialNamespaceList);
        simpleType!(qname_list QnameList);
        simpleType!(qname_list_a QnameListA);
        simpleType!(xpath_default_namespace XpathDefaultNamespace);
        complexType!(attribute_group AttributeGroup 88 -
            attributes {
                local id id o - String,
                local name name o - String,
                local r#ref r#ref o - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            attr_decls(super::g::AttrDecls): d + - super::g::attr_decls
        );
        complexType!(named_attribute_group NamedAttributeGroup 89 -
            attributes {
                local id id o - String,
                local name name r - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            attr_decls(super::g::AttrDecls): d + - super::g::attr_decls
        );
        complexType!(attribute_group_ref AttributeGroupRef 90 -
            attributes {
                local id id o - String,
                local r#ref r#ref r - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        complexType!(keybase Keybase 91 -
            attributes {
                local id id o - String,
                local name name o - String,
                local r#ref r#ref o - QName
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            sequence(super::local::Keyref1): o + - super::local::keyref1
        );
        simpleType!(public Public);
        simpleType!(derivation_control DerivationControl);
        simpleType!(simple_derivation_set SimpleDerivationSet);
        complexType!(simple_type SimpleType 96 -
            attributes {
                local r#final r#final o - String,
                local id id o - String,
                local name name o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            simple_derivation(super::g::SimpleDerivation): r + - super::g::simple_derivation
        );
        complexType!(top_level_simple_type TopLevelSimpleType 97 -
            attributes {
                local r#final r#final o - String,
                local id id o - String,
                local name name r - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            simple_derivation(super::g::SimpleDerivation): v + - super::g::simple_derivation
        );
        complexType!(local_simple_type LocalSimpleType 98 -
            attributes {
                local id id o - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation,
            simple_derivation(super::g::SimpleDerivation): v + - super::g::simple_derivation
        );
        complexType!(facet Facet 99 -
            attributes {
                local fixed fixed o "false" bool,
                local id id o - String,
                local value value r - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        complexType!(no_fixed_facet NoFixedFacet 100 -
            attributes {
                local id id o - String,
                local value value r - String
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        complexType!(num_facet NumFacet 101 -
            attributes {
                local fixed fixed o "false" bool,
                local id id o - String,
                local value value r - usize
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        complexType!(int_facet IntFacet 102 -
            attributes {
                local fixed fixed o "false" bool,
                local id id o - String,
                local value value r - i64
            }

            annotation(super::e::Annotation): o - - super::e::annotation
        );
        simpleType!(reduced_derivation_control ReducedDerivationControl);
        simpleType!(type_derivation_control TypeDerivationControl);
    }
    pub mod e {
        crate::use_xsd!();

        element!(xs schema Schema 128 -
            attributes {
                local attribute_form_default attribute_form_default o "unqualified" String,
                local block_default block_default o "" String,
                local default_attributes default_attributes o - QName,
                local element_form_default element_form_default o "unqualified" String,
                local final_default final_default o "" String,
                local id id o - String,
                local target_namespace target_namespace o - String,
                local version version o - String,
                local xpath_default_namespace xpath_default_namespace o "##local" String,
                xml lang lang o - String
            }
            composition(super::g::Composition): v + - super::g::composition,
            sequence1(super::local::Schema1): o + - super::local::schema1,
            sequence2(super::local::Schema2): v + - super::local::schema2
        );
        element!(xs any_attribute AnyAttribute 129 -
            attributes {
                local id id o - String,
                local namespace namespace o - String,
                local not_namespace not_namespace ov - String,
                local not_q_name not_q_name ov - String,
                local process_contents process_contents o "strict" String
            }
            annotation(Annotation): o - - annotation
        );
        element!(xs complex_content ComplexContent 130 -
            attributes {
                local id id o - String,
                local mixed mixed o - bool
            }
            annotation(Annotation): o - - annotation,
            choice(super::local::ComplexContent1): r + - super::local::complex_content1
        );
        element!(xs open_content OpenContent 132 -
            attributes {
                local id id o - String,
                local mode mode o "interleave" String
            }
            annotation(Annotation): o - - annotation,
            any(super::ct::Wildcard): o xs any super::ct::wildcard
        );
        element!(xs default_open_content DefaultOpenContent 134 -
            attributes {
                local applies_to_empty applies_to_empty o "false" bool,
                local id id o - String,
                local mode mode o "interleave" String
            }
            annotation(Annotation): o - - annotation,
            any(super::ct::Wildcard): r xs any super::ct::wildcard
        );
        element!(xs simple_content SimpleContent 135 -
            attributes {
                local id id o - String
            }
            annotation(Annotation): o - - annotation,
            choice(super::local::SimpleContent1): r + - super::local::simple_content1
        );
        element_type!(xs complex_type(super::ct::TopLevelComplexType) super::ct::top_level_complex_type);
        element_type!(xs element(super::ct::TopLevelElement) super::ct::top_level_element);
        element_type!(xs all(super::ct::All) super::ct::all);
        element_type!(xs choice(super::ct::ExplicitGroup) super::ct::explicit_group);
        element_type!(xs sequence(super::ct::ExplicitGroup) super::ct::explicit_group);
        element_type!(xs group(super::ct::NamedGroup) super::ct::named_group);
        element!(xs any Any 136 -
            attributes {
                local id id o - String,
                local max_occurs max_occurs o "1" MaxOccurs,
                local min_occurs min_occurs o "1" usize,
                local namespace namespace o - String,
                local not_namespace not_namespace ov - String,
                local not_q_name not_q_name ov - String,
                local process_contents process_contents o "strict" String
            }
            annotation(Annotation): o - - annotation
        );
        element_type!(xs attribute(super::ct::TopLevelAttribute) super::ct::top_level_attribute);
        element_type!(xs attribute_group(super::ct::NamedAttributeGroup) super::ct::named_attribute_group);
        element!(xs include Include 137 -
            attributes {
                local id id o - String,
                local schema_location schema_location r - String
            }
            annotation(Annotation): o - - annotation
        );
        element!(xs redefine Redefine 138 -
            attributes {
                local id id o - String,
                local schema_location schema_location r - String
            }
            choice(super::local::Redefine): v + - super::local::redefine
        );
        element!(xs r#override Override 139 -
            attributes {
                local id id o - String,
                local schema_location schema_location r - String
            }
            annotation(Annotation): o - - annotation,
            schema_top(super::g::SchemaTop): v + - super::g::schema_top
        );
        element!(xs import Import 140 -
            attributes {
                local id id o - String,
                local namespace namespace o - String,
                local schema_location schema_location o - String
            }
            annotation(Annotation): o - - annotation
        );
        element!(xs selector Selector 142 -
            attributes {
                local id id o - String,
                local xpath xpath r - String,
                local xpath_default_namespace xpath_default_namespace o - String
            }
            annotation(Annotation): o - - annotation
        );
        element!(xs field Field 142 -
            attributes {
                local id id o - String,
                local xpath xpath r - String,
                local xpath_default_namespace xpath_default_namespace o - String
            }
            annotation(Annotation): o - - annotation
        );
        element_type!(xs unique(super::ct::Keybase) super::ct::keybase);
        element_type!(xs key(super::ct::Keybase) super::ct::keybase);
        element!(xs keyref Keyref 143 -
            attributes {
                local id id o - String,
                local name name o - String,
                local r#ref r#ref o - QName,
                local refer refer o - QName
            }
            annotation(Annotation): o - - annotation,
            sequence(super::local::Keyref1): o + - super::local::keyref1
        );
        element!(xs notation Notation 144 -
            attributes {
                local id id o - String,
                local name name r - String,
                local public public o - String,
                local system system o - String
            }
            annotation(Annotation): o - - annotation
        );
        element!(xs appinfo Appinfo 145 m
            attributes {
                local source source o - String
            }
            sequence(super::local::Documentation): v + - super::local::documentation
        );
        element!(xs documentation Documentation 146 m
            attributes {
                local source source o - String,
                xml lang lang o - String
            }
            sequence(super::local::Documentation): v + - super::local::documentation
        );
        element!(xs annotation Annotation 147 -
            attributes {
                local id id o - String
            }
            choice(super::local::Annotation): v + - super::local::annotation
        );
        element_type!(xs simple_type(super::ct::TopLevelSimpleType) super::ct::top_level_simple_type);
        element!(xs restriction Restriction 148 -
            attributes {
                local base base o - QName,
                local id id o - String
            }
            annotation(Annotation): o - - annotation,
            simple_restriction_model(super::g::SimpleRestrictionModel): d + - super::g::simple_restriction_model
        );
        element!(xs list List 149 -
            attributes {
                local id id o - String,
                local item_type item_type o - QName
            }
            annotation(Annotation): o - - annotation,
            simple_type(super::ct::LocalSimpleType): o xs simple_type super::ct::local_simple_type
        );
        element!(xs union Union 150 -
            attributes {
                local id id o - String,
                local member_types member_types ov - QName
            }
            annotation(Annotation): o - - annotation,
            simple_type(super::ct::LocalSimpleType): v xs simple_type super::ct::local_simple_type
        );
        element_type!(xs min_exclusive(super::ct::Facet) super::ct::facet);
        element_type!(xs min_inclusive(super::ct::Facet) super::ct::facet);
        element_type!(xs max_exclusive(super::ct::Facet) super::ct::facet);
        element_type!(xs max_inclusive(super::ct::Facet) super::ct::facet);
        element!(xs total_digits TotalDigits 151 -
            attributes {
                local fixed fixed o "false" bool,
                local id id o - String,
                local value value r - usize
            }
            annotation(Annotation): o - - annotation
        );
        element_type!(xs fraction_digits(super::ct::NumFacet) super::ct::num_facet);
        element_type!(xs length(super::ct::NumFacet) super::ct::num_facet);
        element_type!(xs min_length(super::ct::NumFacet) super::ct::num_facet);
        element_type!(xs max_length(super::ct::NumFacet) super::ct::num_facet);
        element_type!(xs enumeration(super::ct::NoFixedFacet) super::ct::no_fixed_facet);
        element!(xs white_space WhiteSpace 153 -
            attributes {
                local fixed fixed o "false" bool,
                local id id o - String,
                local value value r - String
            }
            annotation(Annotation): o - - annotation
        );
        element!(xs pattern Pattern 154 -
            attributes {
                local id id o - String,
                local value value r - String
            }
            annotation(Annotation): o - - annotation
        );
        element_type!(xs assertion(super::ct::Assertion) super::ct::assertion);
        element!(xs explicit_timezone ExplicitTimezone 156 -
            attributes {
                local fixed fixed o "false" bool,
                local id id o - String,
                local value value r - String
            }
            annotation(Annotation): o - - annotation
        );
    }
}
