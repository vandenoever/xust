use crate::{atomic::par_ser::*, facets::Facets, schema_components::XS_NS, types::TypeId};
use xust_tree::qnames::{QNameCollection, QNameId};

pub const XS_ANY_TYPE: TypeId = TypeId(0);
pub const XS_ANY_SIMPLE_TYPE: TypeId = TypeId(1);
pub const XS_ANY_ATOMIC_TYPE: TypeId = TypeId(2);
// primitive datatypes
pub const XS_STRING: TypeId = TypeId(3);
pub const XS_BOOLEAN: TypeId = TypeId(4);
pub const XS_DECIMAL: TypeId = TypeId(5);
pub const XS_FLOAT: TypeId = TypeId(6);
pub const XS_DOUBLE: TypeId = TypeId(7);
pub const XS_DURATION: TypeId = TypeId(8);
pub const XS_DATE_TIME: TypeId = TypeId(9);
pub const XS_TIME: TypeId = TypeId(10);
pub const XS_DATE: TypeId = TypeId(11);
pub const XS_G_YEAR_MONTH: TypeId = TypeId(12);
pub const XS_G_YEAR: TypeId = TypeId(13);
pub const XS_G_MONTH_DAY: TypeId = TypeId(14);
pub const XS_G_DAY: TypeId = TypeId(15);
pub const XS_G_MONTH: TypeId = TypeId(16);
pub const XS_HEX_BINARY: TypeId = TypeId(17);
pub const XS_BASE64_BINARY: TypeId = TypeId(18);
pub const XS_ANY_URI: TypeId = TypeId(19);
pub const XS_QNAME: TypeId = TypeId(20);
pub const XS_NOTATION: TypeId = TypeId(21);
// derived atomics
pub const XS_NORMALIZED_STRING: TypeId = TypeId(22);
pub const XS_TOKEN: TypeId = TypeId(23);
pub const XS_LANGUAGE: TypeId = TypeId(24);
pub const XS_NAME: TypeId = TypeId(25);
pub const XS_NCNAME: TypeId = TypeId(26);
pub const XS_ID: TypeId = TypeId(27);
pub const XS_IDREF: TypeId = TypeId(28);
pub const XS_ENTITY: TypeId = TypeId(29);
pub const XS_NMTOKEN: TypeId = TypeId(30);
pub const XS_INTEGER: TypeId = TypeId(31);
pub const XS_NON_POSITIVE_INTEGER: TypeId = TypeId(32);
pub const XS_NEGATIVE_INTEGER: TypeId = TypeId(33);
pub const XS_LONG: TypeId = TypeId(34);
pub const XS_INT: TypeId = TypeId(35);
pub const XS_SHORT: TypeId = TypeId(36);
pub const XS_BYTE: TypeId = TypeId(37);
pub const XS_NON_NEGATIVE_INTEGER: TypeId = TypeId(38);
pub const XS_UNSIGNED_LONG: TypeId = TypeId(39);
pub const XS_UNSIGNED_INT: TypeId = TypeId(40);
pub const XS_UNSIGNED_SHORT: TypeId = TypeId(41);
pub const XS_UNSIGNED_BYTE: TypeId = TypeId(42);
pub const XS_POSITIVE_INTEGER: TypeId = TypeId(43);
pub const XS_DAY_TIME_DURATION: TypeId = TypeId(44);
pub const XS_YEAR_MONTH_DURATION: TypeId = TypeId(45);
// derived for XDM
pub const XS_UNTYPED: TypeId = TypeId(46);
pub const XS_UNTYPED_ATOMIC: TypeId = TypeId(47);
pub const XS_ERROR: TypeId = TypeId(48);
// lists
pub const XS_IDREFS: TypeId = TypeId(49);
pub const XS_ENTITIES: TypeId = TypeId(50);
pub const XS_NMTOKENS: TypeId = TypeId(51);

const EMPTY_FACETS: fn() -> Facets = Facets::default;

const BUILT_INS: &[(&str, Option<TypeId>, PrimitiveParSer, fn() -> Facets)] = &[
    // primitive types
    ("string", None, STRING_PAR_SER, Facets::string),
    (
        "boolean",
        None,
        BOOLEAN_PAR_SER,
        Facets::white_space_collapse,
    ),
    (
        "decimal",
        None,
        DECIMAL_PAR_SER,
        Facets::white_space_collapse,
    ),
    ("float", None, FLOAT_PAR_SER, Facets::white_space_collapse),
    ("double", None, DOUBLE_PAR_SER, Facets::white_space_collapse),
    (
        "duration",
        None,
        DURATION_PAR_SER,
        Facets::white_space_collapse,
    ),
    ("dateTime", None, DATE_TIME_PAR_SER, Facets::date_time),
    ("time", None, TIME_PAR_SER, Facets::date_time),
    ("date", None, DATE_PAR_SER, Facets::date_time),
    ("gYearMonth", None, G_YEAR_MONTH_PAR_SER, Facets::date_time),
    ("gYear", None, G_YEAR_PAR_SER, Facets::date_time),
    ("gMonthDay", None, G_MONTH_DAY_PAR_SER, Facets::date_time),
    ("gDay", None, G_DAY_PAR_SER, Facets::date_time),
    ("gMonth", None, G_MONTH_PAR_SER, Facets::date_time),
    (
        "hexBinary",
        None,
        HEX_BINARY_PAR_SER,
        Facets::white_space_collapse,
    ),
    (
        "base64Binary",
        None,
        BASE64_BINARY_PAR_SER,
        Facets::white_space_collapse,
    ),
    (
        "anyURI",
        None,
        ANY_URI_PAR_SER,
        Facets::white_space_collapse,
    ),
    ("QName", None, QNAME_PAR_SER, Facets::white_space_collapse),
    (
        "NOTATION",
        None,
        NOTATION_PAR_SER,
        Facets::white_space_collapse,
    ),
    // derived types
    // strings
    (
        "normalizedString",
        Some(XS_STRING),
        STRING_PAR_SER,
        Facets::normalized_string,
    ),
    (
        "token",
        Some(XS_NORMALIZED_STRING),
        STRING_PAR_SER,
        Facets::token,
    ),
    ("language", Some(XS_TOKEN), STRING_PAR_SER, Facets::language),
    ("Name", Some(XS_TOKEN), NAME_PAR_SER, Facets::name),
    ("NCName", Some(XS_NAME), NCNAME_PAR_SER, Facets::ncname),
    ("ID", Some(XS_NCNAME), NCNAME_PAR_SER, Facets::ncname),
    ("IDREF", Some(XS_NCNAME), NCNAME_PAR_SER, Facets::ncname),
    ("ENTITY", Some(XS_NCNAME), NCNAME_PAR_SER, Facets::ncname),
    ("NMTOKEN", Some(XS_TOKEN), NMTOKEN_PAR_SER, Facets::nmtoken),
    // ints
    (
        "integer",
        Some(XS_DECIMAL),
        DECIMAL_PAR_SER,
        Facets::integer,
    ),
    (
        "nonPositiveInteger",
        Some(XS_INTEGER),
        DECIMAL_PAR_SER,
        Facets::non_positive_integer,
    ),
    (
        "negativeInteger",
        Some(XS_NON_POSITIVE_INTEGER),
        DECIMAL_PAR_SER,
        Facets::negative_integer,
    ),
    ("long", Some(XS_INTEGER), DECIMAL_PAR_SER, Facets::long),
    ("int", Some(XS_LONG), DECIMAL_PAR_SER, Facets::int),
    ("short", Some(XS_INT), DECIMAL_PAR_SER, Facets::short),
    ("byte", Some(XS_SHORT), DECIMAL_PAR_SER, Facets::byte),
    (
        "nonNegativeInteger",
        Some(XS_INTEGER),
        DECIMAL_PAR_SER,
        Facets::non_negative_integer,
    ),
    (
        "unsignedLong",
        Some(XS_NON_NEGATIVE_INTEGER),
        DECIMAL_PAR_SER,
        Facets::unsigned_long,
    ),
    (
        "unsignedInt",
        Some(XS_UNSIGNED_LONG),
        DECIMAL_PAR_SER,
        Facets::unsigned_int,
    ),
    (
        "unsignedShort",
        Some(XS_UNSIGNED_INT),
        DECIMAL_PAR_SER,
        Facets::unsigned_short,
    ),
    (
        "unsignedByte",
        Some(XS_UNSIGNED_SHORT),
        DECIMAL_PAR_SER,
        Facets::unsigned_byte,
    ),
    (
        "positiveInteger",
        Some(XS_NON_NEGATIVE_INTEGER),
        DECIMAL_PAR_SER,
        Facets::positive_integer,
    ),
    // durations
    (
        "dayTimeDuration",
        Some(XS_DURATION),
        DURATION_PAR_SER,
        Facets::white_space_collapse,
    ),
    (
        "yearMonthDuration",
        Some(XS_DURATION),
        DURATION_PAR_SER,
        Facets::white_space_collapse,
    ),
    // XQuery types
    ("untyped", None, ERROR_PAR_SER, EMPTY_FACETS),
    ("untypedAtomic", None, STRING_PAR_SER, EMPTY_FACETS),
    ("error", None, ERROR_PAR_SER, EMPTY_FACETS),
];

pub(crate) fn is_built_in(name: Option<QNameId>, qnames: &QNameCollection) -> bool {
    if let Some(name) = name {
        let qname = qnames.to_ref(name);
        if qname.namespace().as_str() != XS_NS {
            return false;
        }
        BUILT_INS
            .iter()
            .any(|(v, _, _, _)| v == &qname.local_name().as_str())
    } else {
        false
    }
}

pub(crate) fn built_ins() -> &'static [(
    &'static str,
    Option<TypeId>,
    PrimitiveParSer,
    fn() -> Facets,
)] {
    BUILT_INS
}

pub(crate) const BUILT_IN_AMOUNT: usize = 52;

#[test]
fn check_static_type_ids() {
    let schema_components =
        crate::xsd2schema_components::load::xsd2schema_components(&[], None, None)
            .unwrap()
            .components;
    let types = &schema_components.types;
    assert_eq!(types.len(), BUILT_IN_AMOUNT);
    for t in types.iter() {
        if let Some(name) = t.1.name {
            println!("{:?} {}", t.0, schema_components.qnames.get(name));
        }
    }
    for (t, ln) in &[
        (XS_ANY_TYPE, "anyType"),
        (XS_ANY_SIMPLE_TYPE, "anySimpleType"),
        (XS_ANY_ATOMIC_TYPE, "anyAtomicType"),
        (XS_DURATION, "duration"),
        (XS_DATE_TIME, "dateTime"),
        (XS_TIME, "time"),
        (XS_DATE, "date"),
        (XS_G_YEAR_MONTH, "gYearMonth"),
        (XS_G_YEAR, "gYear"),
        (XS_G_MONTH_DAY, "gMonthDay"),
        (XS_G_DAY, "gDay"),
        (XS_G_MONTH, "gMonth"),
        (XS_BOOLEAN, "boolean"),
        (XS_BASE64_BINARY, "base64Binary"),
        (XS_HEX_BINARY, "hexBinary"),
        (XS_FLOAT, "float"),
        (XS_DECIMAL, "decimal"),
        (XS_DOUBLE, "double"),
        (XS_ANY_URI, "anyURI"),
        (XS_QNAME, "QName"),
        (XS_NOTATION, "NOTATION"),
        (XS_STRING, "string"),
        (XS_INTEGER, "integer"),
        (XS_INT, "int"),
        (XS_SHORT, "short"),
        (XS_BYTE, "byte"),
        (XS_TOKEN, "token"),
        (XS_NMTOKEN, "NMTOKEN"),
        (XS_NCNAME, "NCName"),
        (XS_IDREF, "IDREF"),
        (XS_ENTITY, "ENTITY"),
        (XS_UNTYPED_ATOMIC, "untypedAtomic"),
        (XS_DAY_TIME_DURATION, "dayTimeDuration"),
        (XS_YEAR_MONTH_DURATION, "yearMonthDuration"),
        (XS_NMTOKENS, "NMTOKENS"),
    ] {
        let name = schema_components.qnames.get(types[*t].get_name().unwrap());
        assert_eq!(name.to_ref().local_name().0, *ln);
    }
}
