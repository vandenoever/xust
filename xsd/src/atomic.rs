pub(crate) mod atomic_value;
mod num;
pub(crate) mod par_ser;

use crate::{
    facets::Facets,
    qname_parser::QNameParser,
    types::{built_ins::*, TypeId},
    xpath_error::{Error, Result},
};
use atomic_value::AtomicValue;
use num::SafeOps;
use par_ser::PrimitiveParSer;
use rust_decimal::{
    prelude::{FromPrimitive, ToPrimitive},
    Decimal,
};
use std::fmt::{Display, Formatter};
use xust_tree::qnames::QName;

#[derive(Clone, Debug, PartialEq)]
pub struct Atomic {
    pub(crate) value: AtomicValue,
    pub(crate) r#type: TypeId,
}

impl Atomic {
    pub fn double(v: f64) -> Self {
        Self {
            value: AtomicValue::Double(v),
            r#type: XS_DOUBLE,
        }
    }
    pub fn float(v: f32) -> Self {
        Self {
            value: AtomicValue::Float(v),
            r#type: XS_FLOAT,
        }
    }
    pub fn decimal(v: Decimal) -> Self {
        Self {
            value: AtomicValue::Decimal(v),
            r#type: XS_DECIMAL,
        }
    }
    pub fn integer(v: i64) -> Self {
        Self {
            value: AtomicValue::Decimal(v.into()),
            r#type: XS_DECIMAL,
        }
    }
    pub fn string<S: Into<Box<str>>>(v: S) -> Self {
        Self {
            value: AtomicValue::String(v.into()),
            r#type: XS_STRING,
        }
    }
    pub fn any_uri<S: Into<Box<str>>>(v: S) -> Self {
        Self {
            value: AtomicValue::AnyURI(v.into()),
            r#type: XS_ANY_URI,
        }
    }
    pub fn qname(v: QName) -> Self {
        Self {
            value: AtomicValue::QName(v),
            r#type: XS_QNAME,
        }
    }
    pub fn boolean(v: bool) -> Self {
        Self {
            value: AtomicValue::Boolean(v),
            r#type: XS_BOOLEAN,
        }
    }
    pub fn type_id(&self) -> TypeId {
        self.r#type
    }
    pub fn is_true(&self) -> bool {
        if let AtomicValue::Boolean(b) = self.value {
            b
        } else {
            false
        }
    }
    pub fn is_false(&self) -> bool {
        if let AtomicValue::Boolean(b) = self.value {
            !b
        } else {
            false
        }
    }
    pub fn negate(&self) -> Result<Self> {
        Ok(Atomic {
            value: match &self.value {
                AtomicValue::Decimal(v) => AtomicValue::Decimal(-v),
                AtomicValue::Double(v) => AtomicValue::Double(-v),
                AtomicValue::Float(v) => AtomicValue::Float(-v),
                _e => return Err(Error::xpty(4)),
            },
            r#type: self.r#type,
        })
    }
    pub fn abs(&self) -> Result<Self> {
        Ok(match &self.value {
            AtomicValue::Double(v) => Atomic::double(v.abs()),
            AtomicValue::Decimal(v) => Atomic::decimal(v.abs()),
            AtomicValue::Float(v) => Atomic::float(v.abs()),
            _e => return Err(Error::xpty(4)),
        })
    }
    pub fn ceil(&self) -> Result<Self> {
        Ok(match &self.value {
            AtomicValue::Double(v) => Atomic::double(v.ceil()),
            AtomicValue::Decimal(v) => Atomic::decimal(v.ceil()),
            AtomicValue::Float(v) => Atomic::float(v.ceil()),
            _e => return Err(Error::xpty(4)),
        })
    }
    pub fn floor(&self) -> Result<Self> {
        Ok(match &self.value {
            AtomicValue::Double(v) => Atomic::double(v.floor()),
            AtomicValue::Decimal(v) => Atomic::decimal(v.floor()),
            AtomicValue::Float(v) => Atomic::float(v.floor()),
            _e => return Err(Error::xpty(4)),
        })
    }
    pub fn round(&self) -> Result<Self> {
        Ok(match &self.value {
            AtomicValue::Double(v) => Atomic::double(round_f64(*v)),
            AtomicValue::Decimal(v) => Atomic::decimal(v.round()),
            AtomicValue::Float(v) => Atomic::float(round_f32(*v)),
            _e => return Err(Error::xpty(4)),
        })
    }
    pub fn round_2(&self, precision: i64) -> Result<Self> {
        Ok(match &self.value {
            AtomicValue::Double(v) => {
                let n = 10_f64.powi(precision as i32);
                Atomic::double(round_f64(n * v) / n)
            }
            AtomicValue::Decimal(v) => Atomic::decimal(v.round_dp(precision as u32)),
            AtomicValue::Float(v) => {
                let n = 10_f32.powi(precision as i32);
                Atomic::float(round_f32(n * v) / n)
            }
            _e => return Err(Error::xpty(4)),
        })
    }
    pub fn as_i64(&self) -> Option<i64> {
        match &self.value {
            AtomicValue::Decimal(s) => d2i64(*s).ok(),
            _ => None,
        }
    }
    pub fn as_double(&self) -> Option<f64> {
        Some(match &self.value {
            AtomicValue::Double(i) => *i,
            AtomicValue::Float(i) => *i as f64,
            AtomicValue::Decimal(i) => return d2f64(*i).ok(),
            _ => return None,
        })
    }
    pub fn as_str(&self) -> Option<&str> {
        match &self.value {
            AtomicValue::String(s) => Some(s),
            _ => None,
        }
    }
    pub fn as_qname(&self) -> Option<&QName> {
        match &self.value {
            AtomicValue::QName(q) => Some(q),
            _ => None,
        }
    }
    pub fn eq_numeric_singleton(&self, v: usize) -> bool {
        match self.value {
            AtomicValue::Decimal(i) => Some(v) == i.to_usize(),
            AtomicValue::Double(i) => v as f64 == i,
            AtomicValue::Float(i) => v as f32 == i,
            // AtomicValue::Decimal(i) => v == i,
            _ => false,
        }
    }
    // Convert atomic to boolean as described in fn:boolean.
    // This is different from xs:boolean.
    pub fn to_boolean(&self) -> Result<bool> {
        Ok(match &self.value {
            AtomicValue::String(s) => !s.is_empty(),
            AtomicValue::Boolean(b) => *b,
            AtomicValue::Decimal(d) => !d.is_zero(),
            AtomicValue::Float(d) => *d != 0. && !d.is_nan(),
            AtomicValue::Double(d) => *d != 0. && !d.is_nan(),
            _ => return Err(Error::xpty(4)),
        })
    }
    /*
    fn try_untyped_to_double(self) -> Result<Atomic> {
        if self.r#type != XS_UNTYPED_ATOMIC {
            return Ok(self);
        }
        if let AtomicValue::String(s) = self.value {
            if let Ok(value) = parse_primitive::double(s.as_ref()) {
                return Ok(Atomic {
                    value: AtomicValue::Double(value),
                    r#type: XS_DOUBLE,
                });
            }
        }
        Err(Error::forg(1))
    }
    */
}

impl Display for Atomic {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

pub fn safe_add(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_add(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_add(b)?),
        PromotedNumericPair::Decimal(a, b) => Atomic::decimal(a.safe_add(b)?),
    })
}
pub fn safe_sub(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_sub(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_sub(b)?),
        PromotedNumericPair::Decimal(a, b) => {
            Atomic::decimal(a.checked_sub(b).ok_or_else(|| Error::foar(1))?)
        }
    })
}
pub fn safe_div(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_div(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_div(b)?),
        PromotedNumericPair::Decimal(a, b) => {
            Atomic::decimal(a.checked_div(b).ok_or_else(|| Error::foar(1))?)
        }
    })
}
pub fn safe_idiv(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::integer(a.safe_idiv(b)? as i64),
        PromotedNumericPair::Float(a, b) => Atomic::integer(a.safe_idiv(b)? as i64),
        PromotedNumericPair::Decimal(a, b) => Atomic::integer(d2i64(a.safe_idiv(b)?)?),
    })
}
pub fn safe_mul(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_mul(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_mul(b)?),
        PromotedNumericPair::Decimal(a, b) => Atomic::decimal(a.safe_mul(b)?),
    })
}
pub fn safe_mod(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_mod(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_mod(b)?),
        PromotedNumericPair::Decimal(a, b) => Atomic::decimal(a.safe_mod(b)?),
    })
}

// round negative numbers that are equally near to integers upwards
pub fn round_f64(v: f64) -> f64 {
    let r = v.round();
    if v < 0. && v - r == 0.5 {
        r + 1.
    } else {
        r
    }
}

fn round_f32(v: f32) -> f32 {
    let r = v.round();
    if v < 0. && v - r == 0.5 {
        r + 1.
    } else {
        r
    }
}

/// Convert the source `Atomic` to `Value` such that the parser gets a string
/// to parse or the result `Value` that would come out of the parsing.
/// Follows the table in 19.1 Casting from primitive types to primitive types.
/// Any information loss, e.g. when going from dateTime to date should occur
/// here, even if the `Value` can hold that information.
///
/// TODO: This is part of <https://www.w3.org/TR/xpath-functions-31/>
pub(crate) fn cast_to_atomic(
    source: &Atomic,
    r#type: TypeId,
    target: PrimitiveParSer,
    facets: &Facets,
    qname_parser: &QNameParser,
) -> Result<Atomic> {
    Ok(Atomic {
        value: cast_to_atomic_value(source, target, facets, qname_parser)?,
        r#type,
    })
}

pub(crate) fn cast_to_atomic_value(
    source: &Atomic,
    target: PrimitiveParSer,
    facets: &Facets,
    qname_parser: &QNameParser,
) -> Result<AtomicValue> {
    if let AtomicValue::String(s) = &source.value {
        return target
            .parse(s, facets, qname_parser)
            .map_err(|_| Error::fodt(1));
    }
    let v = match target.primitive() {
        XS_STRING => {
            if let AtomicValue::String(s) | AtomicValue::Notation(s) = &source.value {
                AtomicValue::String(s.as_ref().into())
            } else {
                AtomicValue::String(format!("{}", source.value).into())
            }
        }
        XS_FLOAT => match &source.value {
            AtomicValue::Float(v) => AtomicValue::Float(*v),
            AtomicValue::Double(v) => AtomicValue::Float(*v as f32),
            AtomicValue::Decimal(v) => AtomicValue::Float(d2f32(*v)?),
            AtomicValue::Boolean(v) => AtomicValue::Float(if *v { 1. } else { 0. }),
            _ => return Err(Error::fodt(1)),
        },
        XS_DOUBLE => match &source.value {
            AtomicValue::Float(v) => AtomicValue::Double(*v as f64),
            AtomicValue::Double(v) => AtomicValue::Double(*v),
            AtomicValue::Decimal(v) => AtomicValue::Double(d2f64(*v)?),
            AtomicValue::Boolean(v) => AtomicValue::Double(if *v { 1. } else { 0. }),
            _ => return Err(Error::fodt(1)),
        },
        XS_DECIMAL => match &source.value {
            AtomicValue::Float(v) => AtomicValue::Decimal(f322d(*v)?),
            AtomicValue::Double(v) => AtomicValue::Decimal(f642d(*v)?),
            AtomicValue::Decimal(v) => AtomicValue::Decimal(*v),
            AtomicValue::Boolean(v) => {
                AtomicValue::Decimal(Decimal::new(if *v { 1 } else { 0 }, 0))
            }
            _ => return Err(Error::fodt(1)),
        },
        XS_BOOLEAN => match &source.value {
            AtomicValue::Float(v) => AtomicValue::Boolean(*v != 0.),
            AtomicValue::Double(v) => AtomicValue::Boolean(*v != 0.),
            AtomicValue::Decimal(v) => AtomicValue::Boolean(!v.is_zero()),
            AtomicValue::Boolean(v) => AtomicValue::Boolean(*v),
            _ => return Err(Error::fodt(1)),
        },
        XS_DURATION => match &source.value {
            AtomicValue::Duration(v) => AtomicValue::Duration(*v),
            _ => return Err(Error::fodt(1)),
        },
        XS_BASE64_BINARY => match &source.value {
            AtomicValue::HexBinary(v) | AtomicValue::Base64Binary(v) => {
                AtomicValue::Base64Binary(v.clone())
            }
            _ => return Err(Error::fodt(1)),
        },
        XS_HEX_BINARY => match &source.value {
            AtomicValue::HexBinary(v) | AtomicValue::Base64Binary(v) => {
                AtomicValue::HexBinary(v.clone())
            }
            _ => return Err(Error::fodt(1)),
        },
        XS_DATE => match &source.value {
            AtomicValue::Date(v) => AtomicValue::Date(*v),
            AtomicValue::DateTime(v) => AtomicValue::Date(v.to_date()),
            _ => return Err(Error::fodt(1)),
        },
        XS_TIME => match &source.value {
            AtomicValue::Time(v) => AtomicValue::Time(*v),
            AtomicValue::DateTime(v) => AtomicValue::Time(v.to_time()),
            _ => return Err(Error::fodt(1)),
        },
        XS_DATE_TIME => match &source.value {
            AtomicValue::Date(v) => AtomicValue::DateTime(v.to_date_time()),
            AtomicValue::DateTime(v) => AtomicValue::DateTime(*v),
            _ => return Err(Error::fodt(1)),
        },
        XS_G_YEAR => match &source.value {
            AtomicValue::GYear(v) => AtomicValue::GYear(*v),
            AtomicValue::DateTime(v) => AtomicValue::GYear(v.to_date().to_year()),
            AtomicValue::Date(v) => AtomicValue::GYear(v.to_year()),
            _ => return Err(Error::fodt(1)),
        },
        XS_G_MONTH => match &source.value {
            AtomicValue::GMonth(v) => AtomicValue::GMonth(*v),
            AtomicValue::DateTime(v) => AtomicValue::GMonth(v.to_date().to_month()),
            AtomicValue::Date(v) => AtomicValue::GMonth(v.to_month()),
            _ => return Err(Error::fodt(1)),
        },
        XS_G_DAY => match &source.value {
            AtomicValue::GDay(v) => AtomicValue::GDay(*v),
            AtomicValue::DateTime(v) => AtomicValue::GDay(v.to_date().to_day()),
            AtomicValue::Date(v) => AtomicValue::GDay(v.to_day()),
            _ => return Err(Error::fodt(1)),
        },
        XS_G_YEAR_MONTH => match &source.value {
            AtomicValue::GYearMonth(v) => AtomicValue::GYearMonth(*v),
            AtomicValue::DateTime(v) => AtomicValue::GYearMonth(v.to_date().to_year_month()),
            AtomicValue::Date(v) => AtomicValue::GYearMonth(v.to_year_month()),
            _ => return Err(Error::fodt(1)),
        },
        XS_G_MONTH_DAY => match &source.value {
            AtomicValue::GMonthDay(v) => AtomicValue::GMonthDay(*v),
            AtomicValue::DateTime(v) => AtomicValue::GMonthDay(v.to_date().to_month_day()),
            AtomicValue::Date(v) => AtomicValue::GMonthDay(v.to_month_day()),
            _ => return Err(Error::fodt(1)),
        },
        v => panic!("{}", v), //return Err(Error::xxxx(101)), // TODO
    };
    Ok(v)
}

pub fn atomic_to_integer(atomic: &Atomic) -> Result<i64> {
    use std::str::FromStr;
    Ok(match &atomic.value {
        AtomicValue::Boolean(b) => {
            if *b {
                1
            } else {
                0
            }
        }
        AtomicValue::String(s) => i64::from_str(s).map_err(|_e| Error::forg(1))?,
        AtomicValue::Decimal(i) => d2i64(*i)?,
        AtomicValue::Float(f) => *f as i64,
        AtomicValue::Double(d) => *d as i64,
        _ => return Err(Error::xpty(4)),
    })
}

pub fn deep_equal(a: &Atomic, b: &Atomic, _collation: &str) -> Result<bool> {
    Ok(match (&a.value, &b.value) {
        (AtomicValue::Double(a), AtomicValue::Double(b)) => {
            if a.is_nan() && b.is_nan() {
                true
            } else {
                a == b
            }
        }
        (AtomicValue::Float(a), AtomicValue::Float(b)) => {
            if a.is_nan() && b.is_nan() {
                true
            } else {
                a == b
            }
        }
        (_, _) => eq_literal(a, b)?,
    })
}
/*
impl Atomic {
    pub fn local_name(&self) -> NCName {
        // use type id to get local_name from type register
        todo!()
    }
}
*/
#[derive(Debug)]
enum PromotedNumericPair {
    Double(f64, f64),
    Float(f32, f32),
    Decimal(Decimal, Decimal),
}

fn same_primitive(a: &AtomicValue, b: &AtomicValue) -> bool {
    std::mem::discriminant(a) == std::mem::discriminant(b)
}

// promote values so that they are of equal numeric type
fn numeric_type_promotion(a: &AtomicValue, b: &AtomicValue) -> Result<PromotedNumericPair> {
    Ok(match (a, b) {
        (AtomicValue::Double(a), b) => PromotedNumericPair::Double(*a, b.promote_to_double()?),
        (a, AtomicValue::Double(b)) => PromotedNumericPair::Double(a.promote_to_double()?, *b),
        (AtomicValue::Float(a), b) => PromotedNumericPair::Float(*a, b.promote_to_float()?),
        (a, AtomicValue::Float(b)) => PromotedNumericPair::Float(a.promote_to_float()?, *b),
        (AtomicValue::Decimal(a), b) => PromotedNumericPair::Decimal(*a, b.promote_to_decimal()?),
        (a, AtomicValue::Decimal(b)) => PromotedNumericPair::Decimal(a.promote_to_decimal()?, *b),
        _ => return Err(Error::xpty(4)),
    })
}

// fn promoted_primitive_pair(a: &AtomicValue, b: &AtomicValue)

impl AtomicValue {
    fn promote_to_double(&self) -> Result<f64> {
        Ok(match self {
            AtomicValue::Double(v) => *v,
            AtomicValue::Float(v) => *v as f64,
            AtomicValue::Decimal(v) => v.to_f64().ok_or_else(|| Error::xpty(4))?,
            _ => return Err(Error::xpty(4)),
        })
    }
    fn promote_to_float(&self) -> Result<f32> {
        Ok(match self {
            AtomicValue::Float(v) => *v,
            AtomicValue::Decimal(v) => v.to_f32().ok_or_else(|| Error::xpty(4))?,
            _ => return Err(Error::xpty(4)),
        })
    }
    fn promote_to_decimal(&self) -> Result<Decimal> {
        Ok(match self {
            AtomicValue::Decimal(v) => *v,
            _ => return Err(Error::xpty(4)),
        })
    }
}
/*
fn write_float<T: num_traits::float::FloatCore + Display + Default>(
    v: &T,
    f: &mut std::fmt::Formatter,
) -> std::fmt::Result {
    if v.is_infinite() {
        if v.is_sign_negative() {
            write!(f, "-")?;
        }
        write!(f, "INF")
    } else if *v == T::default() && v.is_sign_negative() {
        write!(f, "-0")
    } else {
        write!(f, "{}", v)
    }
}

impl std::fmt::Display for Atomic {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self.value {
            AtomicValue::Integer(v) => write(v, f),
            AtomicValue::Decimal(v) => write(v, f),
            AtomicValue::Double(v) => write_float(v, f),
            AtomicValue::Float(v) => write_float(v, f),
            AtomicValue::String(v) => write(v, f),
            AtomicValue::Boolean(v) => write(v, f),
            AtomicValue::QName(v) => write_qname(v, f),
            AtomicValue::Time(v) => write(v, f),
            AtomicValue::TimeTZ(v) => write_time_tz(v, f),
            AtomicValue::DateTime(v) => write(v, f),
            AtomicValue::DateTimeTZ(v) => write_date_time_tz(v, f),
            AtomicValue::Date(v) => write(v, f),
            AtomicValue::DateTZ(v) => write_date_tz(v, f),
            AtomicValue::GYear(v) => write(v, f),
            e => unimplemented!("{:#?}", e),
        }
    }
}
*/
pub fn value_comparison(
    f: fn(&Atomic, &Atomic) -> Result<bool>,
    mut a: Atomic,
    mut b: Atomic,
) -> Result<bool> {
    if a.r#type == XS_UNTYPED_ATOMIC {
        a.r#type = XS_STRING
    };
    if b.r#type == XS_UNTYPED_ATOMIC {
        b.r#type = XS_STRING
    };
    f(&a, &b)
}

macro_rules! compare_fn {
    ($n:ident $f:ident) => {
        pub fn $n(a: &Atomic, b: &Atomic) -> Result<bool> {
            $f(&a.value, &b.value)
        }
    };
}

fn d2f32(d: Decimal) -> Result<f32> {
    d.to_f32().ok_or_else(|| Error::xpty(4))
}

fn d2f64(d: Decimal) -> Result<f64> {
    d.to_f64().ok_or_else(|| Error::xpty(4))
}

fn d2i64(d: Decimal) -> Result<i64> {
    d.to_i64().ok_or_else(|| Error::xpty(4))
}

fn f322d(v: f32) -> Result<Decimal> {
    Decimal::from_f32(v).ok_or_else(|| Error::fodt(1))
}

fn f642d(v: f64) -> Result<Decimal> {
    Decimal::from_f64(v).ok_or_else(|| Error::fodt(1))
}

pub fn eq_literal(a: &Atomic, b: &Atomic) -> Result<bool> {
    let (a, b) = (&a.value, &b.value);
    if same_primitive(a, b) {
        return Ok(a == b);
    }
    Ok(match numeric_type_promotion(a, b)? {
        PromotedNumericPair::Double(a, b) => a == b,
        PromotedNumericPair::Float(a, b) => a == b,
        PromotedNumericPair::Decimal(a, b) => a == b,
    })
}

fn greater_atomic(a: &AtomicValue, b: &AtomicValue) -> Result<bool> {
    if same_primitive(a, b) {
        return Ok(a > b);
    }
    Ok(match numeric_type_promotion(a, b)? {
        PromotedNumericPair::Double(a, b) => a > b,
        PromotedNumericPair::Float(a, b) => a > b,
        PromotedNumericPair::Decimal(a, b) => a > b,
    })
}

fn greater_or_equal_atomic(a: &AtomicValue, b: &AtomicValue) -> Result<bool> {
    if same_primitive(a, b) {
        return Ok(a >= b);
    }
    Ok(match numeric_type_promotion(a, b)? {
        PromotedNumericPair::Double(a, b) => a >= b,
        PromotedNumericPair::Float(a, b) => a >= b,
        PromotedNumericPair::Decimal(a, b) => a >= b,
    })
}

pub fn not_eq_literal(a: &Atomic, b: &Atomic) -> Result<bool> {
    Ok(!eq_literal(a, b)?)
}

pub fn less_literal(a: &Atomic, b: &Atomic) -> Result<bool> {
    greater_literal(b, a)
}

pub fn less_or_equal_literal(a: &Atomic, b: &Atomic) -> Result<bool> {
    greater_or_equal_literal(b, a)
}

compare_fn!(greater_literal greater_atomic);
compare_fn!(greater_or_equal_literal greater_or_equal_atomic);
