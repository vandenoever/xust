#![allow(dead_code)]
use std::fmt::Debug;
use std::num::ParseIntError;
use thiserror::Error;
use xust_tree::qnames::QName;

#[derive(Error, Debug)]
pub enum XsdError {
    #[error("Node is not an element.")]
    NotAnElement,
    #[error("This element '{0}' was not expected.")]
    UnexpectedElement(String, eyre::Report),
    #[error("An element is needed here.")]
    MissingElement(eyre::Report),
    #[error("An attribute, '{0}' is missing.")]
    MissingAttribute(QName, eyre::Report),
    #[error("QName '{0}' cannot be resolved.")]
    UnresolvedQName(String, eyre::Report),
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error("Could not parse string ({0}) as boolean")]
    ParseBoolError(String),
}

pub struct ErrorCollector<T> {
    max_errors: usize,
    errors: Vec<T>,
}

impl<T: Debug> ErrorCollector<T> {
    pub fn new(max_errors: usize) -> Self {
        Self {
            max_errors,
            errors: Vec::new(),
        }
    }
    pub fn add(&mut self, error: T) -> Result<(), CollectedErrors<T>> {
        self.errors.push(error);
        if self.errors.len() == self.max_errors {
            let errors = std::mem::take(&mut self.errors);
            Err(CollectedErrors(errors))
        } else {
            Ok(())
        }
    }
    pub fn into_errors(self) -> Result<(), CollectedErrors<T>> {
        if self.errors.is_empty() {
            Ok(())
        } else {
            Err(CollectedErrors(self.errors))
        }
    }
}

#[derive(Error, Debug)]
#[error("{0:#?}")]
pub struct CollectedErrors<T: Debug>(Vec<T>);
