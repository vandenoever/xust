mod dummy;
mod identity_checker;
mod stack;

use crate::{
    atomic::Atomic,
    fsm::{self, FSM},
    qname_parser::QNameParser,
    schema_components::{
        identity_constraints::{IdentityConstraintId, IdentityConstraints},
        AttributeDeclaration, ProcessContents, SchemaComponents, SimpleType,
    },
    types::{self, built_ins::XS_ANY_SIMPLE_TYPE, parse_all},
};
use identity_checker::IdentityChecker;
use stack::Stack;
use xust_tree::{
    builder::{Builder, TypedBuilder},
    qnames::{NCName, Namespace, NamespaceDefinition, Prefix, QNameCollection, QNameId},
    stream::IteratorToTree,
    string_stream::{ItemStreamBuilder, StringNamesToQNames},
    tree::{builder::TreeBuilder, Tree},
    typed_value::{TypeId, XS_UNTYPED_ATOMIC},
};
use xust_xml::read::{parse_to_stream, NormalizedXml, ParserConfig};

pub(crate) enum Error {
    MissingRequiredAttribute {
        attribute: QNameId,
        element: QNameId,
    },
    TextOutsideDocumentElement,
    NoTextAllowedInElement(String),
    UnexpectedElement(QNameId),
    UnexpectedElementWithExpected(QNameId, String),
    UnexpectedElementEnd(QNameId),
    InvalidElementTextContent(String, QNameId),
    UnexpectedAttribute(QNameId),
    InvalidAttribute(QNameId, String),
    InvalidXsiType,
    TooMany,
    UnexpectedEvent,
    IdentityError {
        id: IdentityConstraintId,
        msg: String,
    },
}

impl Error {
    fn to_string(
        &self,
        qnames: &QNameCollection,
        identity_constraints: &IdentityConstraints,
    ) -> String {
        match self {
            Error::MissingRequiredAttribute { attribute, element } => {
                format!(
                    "Missing required attribute {} in element {}.",
                    qnames.get(*attribute),
                    qnames.get(*element)
                )
            }
            Error::UnexpectedAttribute(a) => {
                format!("Unexpected attribute {}.", qnames.get(*a))
            }
            Error::InvalidAttribute(a, v) => {
                format!("Invalid attribute {}: '{}'.", qnames.get(*a), v)
            }
            Error::TextOutsideDocumentElement => {
                "Found text outside the document element.".to_string()
            }
            Error::NoTextAllowedInElement(s) => {
                format!("Found unexpected text '{}' in element.", s)
            }
            Error::UnexpectedElement(e) => {
                format!("Found unexpected element {}.", qnames.get(*e))
            }
            Error::UnexpectedElementWithExpected(e, string) => {
                format!(
                    "Found unexpected element {}, expected one of {}.",
                    qnames.get(*e),
                    string
                )
            }
            Error::UnexpectedElementEnd(e) => {
                format!("Found unexpected end of element {}.", qnames.get(*e))
            }
            Error::InvalidElementTextContent(t, e) => {
                format!(
                    "Invalid text content '{}' in element {}.",
                    t,
                    qnames.get(*e)
                )
            }
            Error::InvalidXsiType => "Invalid xsi:type".to_string(),
            Error::TooMany => "Too many occurances of particle".to_string(),
            Error::UnexpectedEvent => "Unexpected event.".to_string(),
            Error::IdentityError { id, msg } => {
                let ic = &identity_constraints[*id];
                let q = qnames.get(ic.name);
                format!("Identity error for key {:?} ({:#?}: {}", q, ic, msg)
            }
        }
    }
}

#[derive(Debug, PartialEq)]
enum ValidatorState {
    Document,
    ElementNotMixed,
    ElementMixed,
    ElementSimpleType(types::TypeId, Option<types::TypeId>),
    Attribute,
    Comment,
    ProcessingInstruction,
    Done,
}

#[derive(Clone, Copy)]
enum CurrentAttribute {
    Predefined(usize),
    TopLevel(QNameId),
    XsiType,
    Untyped,
}

struct Attribute {
    qname: QNameId,
    r#type: TypeId,
    start: usize,
}

struct SubA {
    defined_attributes: Vec<bool>,
    stream: Box<dyn TypedBuilder<Tree<Atomic>, Atomic> + Send + Sync>,
    identity_checker: IdentityChecker,
    errors: Vec<Error>,
}

struct SubB {
    fsm: FSM,
    attributes: Vec<Attribute>,
    // text buffer for receiving attribute values
    buffer: String,
    // every attributes that was seen is set to true
    // the index corresponds to the index of the attribute in the elementdefinition
    stack: Stack,
    qname_parser: QNameParser,
    xsi_schema_location: QNameId,
    xsi_no_namespace_schema_location: QNameId,
    xsi_type: QNameId,
}

struct Validator {
    state: ValidatorState,
    start_element: Option<(usize, QNameId, TypeId)>,
    a: SubA,
    b: SubB,
}

impl Validator {
    fn end_checks(&mut self) {}
    pub fn reset(&mut self) {
        self.b.stack.reset();
    }
    fn set_element_state(&mut self) {
        self.state = self.b.stack.get_state();
    }
    fn check_element_simple_type(
        &mut self,
        tag: QNameId,
        element: usize,
        r#type: types::TypeId,
        actual_type: Option<types::TypeId>,
        simple_type_default: Option<String>,
    ) {
        let stream = &mut self.a.stream;
        let identity_checker = &mut self.a.identity_checker;
        let mut f = |value, last| {
            identity_checker.add_value(&value);
            stream.push_typed_element(element, value, last);
        };
        let value = if self.b.buffer.is_empty() {
            simple_type_default.as_deref().unwrap_or("")
        } else {
            &self.b.buffer
        };
        if parse_all(
            &self.b.fsm.simple_types,
            r#type,
            value,
            &mut f,
            &self.b.qname_parser,
        )
        .is_err()
        {
            self.error(Error::InvalidElementTextContent(self.b.buffer.clone(), tag));
        } else if let Some(actual_type) = actual_type {
            // If the actual type is different from the r#type, the r#type was
            // set by xsi:type. Just checking the xsi:type is not enough. The
            // actual type can have additional facets.
            if actual_type != r#type && actual_type != XS_ANY_SIMPLE_TYPE {
                let mut dummy = |_value, _last| {};
                if parse_all(
                    &self.b.fsm.simple_types,
                    actual_type,
                    value,
                    &mut dummy,
                    &self.b.qname_parser,
                )
                .is_err()
                {
                    self.error(Error::InvalidElementTextContent(self.b.buffer.clone(), tag));
                }
            }
        }
    }
    fn check_attribute_presence(&mut self) {
        let atts = self.b.stack.attributes().0;
        let mut missing = Vec::new();
        for (i, a) in atts.iter().enumerate() {
            if !self.a.defined_attributes[i] {
                if let Some(v) = a.ad.fixed.as_ref().or(a.ad.r#default.as_ref()) {
                    self.b.attributes.clear();
                    self.b.buffer.clear();
                    self.b.buffer.push_str(v);
                    self.b.attributes.push(Attribute {
                        qname: a.name,
                        start: 0,
                        r#type: XS_UNTYPED_ATOMIC,
                    });
                    if let Err(e) = handle_attribute(&mut self.a, &self.b, 0) {
                        self.a.errors.push(e);
                    }
                } else if a.required {
                    missing.push(a.name);
                }
            }
        }
        let tag = self.b.stack.tag().unwrap();
        for a in missing {
            self.error(Error::MissingRequiredAttribute {
                attribute: a,
                element: tag,
            });
        }
    }
    fn error(&mut self, err: Error) {
        self.a.errors.push(err);
    }
}
fn handle_attribute(a: &mut SubA, b: &SubB, att: usize) -> Result<(), Error> {
    let value = attribute_value(att, &b.attributes, &b.buffer);
    let att = &b.attributes[att];
    let qname = att.qname;
    let r#type = att.r#type;
    let (atts, attribute_wildcard) = b.stack.attributes();
    let attribute = if qname == b.xsi_type {
        CurrentAttribute::XsiType
    } else if qname == b.xsi_no_namespace_schema_location || qname == b.xsi_schema_location {
        CurrentAttribute::Untyped
    } else if let Some(att) = atts.iter().position(|a| a.name == qname) {
        a.defined_attributes[att] = true;
        CurrentAttribute::Predefined(att)
    } else if let Some(w) = attribute_wildcard {
        let q = b.qname_parser.qnames().to_ref(qname);
        let w = &b.fsm.wildcards[*w];
        if w.allowed_namespace(q.namespace().0) {
            match (w.process_contents, b.fsm.attributes.contains_key(&qname)) {
                (ProcessContents::Strict, true) | (ProcessContents::Lax, true) => {
                    CurrentAttribute::TopLevel(qname)
                }
                (ProcessContents::Strict, false) => {
                    return Err(Error::UnexpectedAttribute(qname));
                }
                (ProcessContents::Skip, _) | (ProcessContents::Lax, false) => {
                    CurrentAttribute::Untyped
                }
            }
        } else {
            return Err(Error::UnexpectedAttribute(qname));
        }
    } else {
        return Err(Error::UnexpectedAttribute(qname));
    };
    a.stream.start_attribute(qname, r#type, value);
    match attribute {
        CurrentAttribute::Predefined(att) => {
            // write typed value(s)
            let att = b.stack.attributes().0[att].clone();
            let valid = handle_typed_attribute(
                att.name,
                &att.ad,
                &mut a.stream,
                &b.fsm.simple_types,
                value,
                &b.qname_parser,
                &mut a.identity_checker,
            );
            if !valid {
                return Err(Error::InvalidAttribute(att.name, value.to_string()));
            }
        }
        CurrentAttribute::TopLevel(qname) => {
            if let Some(ad) = b.fsm.attributes.get(&qname) {
                let valid = handle_typed_attribute(
                    qname,
                    ad,
                    &mut a.stream,
                    &b.fsm.simple_types,
                    value,
                    &b.qname_parser,
                    &mut a.identity_checker,
                );
                if !valid {
                    return Err(Error::InvalidAttribute(qname, value.to_string()));
                }
            } else {
                a.stream.push_str(value);
            }
        }
        CurrentAttribute::XsiType => {
            if let Some(_xsi_type) = b.qname_parser.parse_qname(value) {
                // the type of this element is overridden
            } else {
                a.errors.push(Error::InvalidXsiType);
            }
        }
        CurrentAttribute::Untyped => {
            a.stream.push_str(value);
        }
    };
    Ok(())
}
fn attribute_value<'a>(a: usize, attributes: &[Attribute], buffer: &'a str) -> &'a str {
    let start = attributes[a].start;
    let end = if let Some(a) = attributes.get(a + 1) {
        a.start
    } else {
        buffer.len()
    };
    &buffer[start..end]
}
fn handle_typed_attribute(
    qname: QNameId,
    ad: &AttributeDeclaration,
    stream: &mut Box<(dyn TypedBuilder<Tree<Atomic>, Atomic> + Send + Sync + 'static)>,
    types: &[SimpleType],
    value: &str,
    qname_parser: &QNameParser,
    identity_checker: &mut IdentityChecker,
) -> bool {
    let mut valid = true;
    if let Some(fixed) = &ad.fixed {
        valid = value == *fixed;
    }
    identity_checker.start_attribute(qname);
    let mut f = |value, last| {
        identity_checker.add_value(&value);
        stream.push_typed_attribute(value, last);
    };
    if parse_all(types, ad.content, value, &mut f, qname_parser).is_err() {
        valid = false;
    }
    identity_checker.end_attribute();
    valid
}

impl Builder<Tree<Atomic>> for Validator {
    fn init_document(&mut self) {
        self.reset();
        self.state = ValidatorState::Document;
        self.a.stream.init_document()
    }
    fn init_element(
        &mut self,
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    ) {
        self.b
            .qname_parser
            .push_namespace_definitions(namespace_definitions);
        self.a
            .stream
            .init_element(qname, r#type, namespace_definitions)
    }
    fn init_top_attribute(&mut self, qname: QNameId, r#type: TypeId, content: &str) {
        self.state = ValidatorState::Attribute;
        self.a.stream.init_top_attribute(qname, r#type, content)
    }
    fn init_top_text(&mut self, content: &str) {
        self.state = ValidatorState::ElementMixed;
        self.a.stream.init_top_text(content)
    }
    fn init_top_comment(&mut self, content: &str) {
        self.state = ValidatorState::Comment;
        self.a.stream.init_top_comment(content)
    }
    fn init_top_processing_instruction(&mut self, target: QNameId, content: &str) {
        self.state = ValidatorState::ProcessingInstruction;
        self.a
            .stream
            .init_top_processing_instruction(target, content)
    }
    fn end_comment(&mut self) {
        self.set_element_state();
        self.a.stream.end_comment()
    }
    fn end_top_text(&mut self) {
        self.state = ValidatorState::Done;
        self.a.stream.end_top_text()
    }
    fn end_processing_instruction(&mut self) {
        self.set_element_state();
        self.a.stream.end_processing_instruction()
    }
    fn push_str(&mut self, s: &str) {
        match self.state {
            ValidatorState::Document => {
                if !s.trim().is_empty() {
                    self.error(Error::TextOutsideDocumentElement);
                }
            }
            ValidatorState::ElementNotMixed => {
                if !s.trim().is_empty() {
                    self.error(Error::NoTextAllowedInElement(s.to_string()));
                }
            }
            ValidatorState::Attribute | ValidatorState::ElementSimpleType(_, _) => {
                self.b.buffer.push_str(s);
            }
            _ => self.a.stream.push_str(s),
        }
    }
    fn push(&mut self, c: char) {
        let s = String::from(c);
        self.push_str(&s);
    }
    fn start_element(
        &mut self,
        parent: usize,
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    ) {
        // The type is still XS_UNTYPED, the validator will determine a more
        // precise type.
        self.b
            .qname_parser
            .push_namespace_definitions(namespace_definitions);
        self.start_element = Some((parent, qname, r#type));
    }
    fn end_element(&mut self, node_id: usize) {
        let qname = if let Some(qname) = self.b.stack.tag() {
            qname
        } else {
            return;
        };
        let simple_type_default = self.b.stack.simple_type_default();
        if let Err(err) = self.b.stack.pop() {
            self.error(err);
        }
        if self.b.stack.len() == 1 {
            self.end_checks();
        }
        if let ValidatorState::ElementSimpleType(st, xsi_type) = self.state {
            // typed value in element should also be present as a text node
            self.a.stream.push_str(&self.b.buffer);
            self.check_element_simple_type(qname, node_id, st, xsi_type, simple_type_default);
        }
        self.set_element_state();
        self.a.identity_checker.end_element(&mut self.a.errors);
        self.a.stream.end_element(node_id);
        self.b.qname_parser.pop_namespace_definitions();
    }
    fn start_attribute(&mut self, qname: QNameId, r#type: TypeId, value: &str) {
        self.state = ValidatorState::Attribute;
        self.b.attributes.push(Attribute {
            qname,
            r#type,
            start: self.b.buffer.len(),
        });
        self.b.buffer.push_str(value);
    }
    fn process_element_open_tag(&mut self) -> usize {
        let (parent, qname, r#type) = self.start_element.take().unwrap();
        let mut xsi_type = None;
        for a in 0..self.b.attributes.len() {
            if self.b.attributes[a].qname == self.b.xsi_type {
                let value = attribute_value(a, &self.b.attributes, &self.b.buffer);
                if let Some(xt) = self.b.qname_parser.parse_qnameid(value) {
                    if let Some(t) = self.b.fsm.elements().iter().position(|e| {
                        if let Some(name) = e.qname {
                            name == xt
                        } else {
                            false
                        }
                    }) {
                        xsi_type = Some(t);
                    } else {
                        self.a.errors.push(Error::InvalidXsiType);
                    }
                    // the type of this element is overridden
                } else {
                    self.a.errors.push(Error::InvalidXsiType);
                }
                // break;
            }
        }
        let (atts, constraints, is_simple, actual_type) = match self.b.stack.push(qname, xsi_type) {
            Ok(r) => r,
            Err(e) => {
                self.error(e);
                return 0;
            }
        };
        self.a.stream.start_element(
            parent,
            qname,
            actual_type.map(|t| t.into()).unwrap_or(r#type),
            self.b.qname_parser.namespace_definitions(),
        );
        if let Err(e) =
            self.a
                .identity_checker
                .start_element(qname, constraints, is_simple, &mut self.a.errors)
        {
            self.a.errors.push(e);
        }
        // handle all in self.attributes
        self.a.defined_attributes.clear();
        self.a.defined_attributes.resize(atts.len(), false);
        self.state = ValidatorState::Attribute;
        for att in 0..self.b.attributes.len() {
            if let Err(e) = handle_attribute(&mut self.a, &self.b, att) {
                self.a.errors.push(e);
            }
        }
        self.b.buffer.clear();
        self.check_attribute_presence();
        self.b.attributes.clear();
        self.set_element_state();
        self.b.buffer.clear();
        self.a.stream.process_element_open_tag()
    }
    fn start_comment(&mut self, node_id: usize) {
        self.state = ValidatorState::Comment;
        self.a.stream.start_comment(node_id)
    }
    fn add_comment(&mut self, node_id: usize, content: &str) {
        self.a.stream.add_comment(node_id, content)
    }
    fn start_processing_instruction(&mut self, node_id: usize, target: QNameId) {
        self.state = ValidatorState::ProcessingInstruction;
        self.a.stream.start_processing_instruction(node_id, target)
    }
    fn end_document(&mut self) {
        self.state = ValidatorState::Done;
        self.a.stream.end_document()
    }
    fn build(self: Box<Self>, qnames: QNameCollection) -> Tree<Atomic> {
        self.a.stream.build(qnames)
    }
    fn has_errors(&self) -> bool {
        !self.a.errors.is_empty()
    }
    fn errors(&self, qnames: QNameCollection) -> Vec<String> {
        self.a
            .errors
            .iter()
            .map(|e| e.to_string(&qnames, &self.b.fsm.identity_constraints))
            .collect()
    }
}

#[derive(Clone)]
pub struct XsdValidator {
    fsm: FSM,
}

impl XsdValidator {
    fn new(fsm: FSM) -> Self {
        XsdValidator { fsm }
    }
    pub fn qnames(&self) -> &QNameCollection {
        self.fsm.qnames()
    }
    pub(crate) fn build(
        schema_components: &SchemaComponents,
    ) -> Result<XsdValidator, Box<dyn std::error::Error>> {
        Ok(XsdValidator::new(fsm::build(schema_components)?))
    }
    fn validate_to_stream(
        &self,
        xml: &NormalizedXml,
        stream: Box<dyn TypedBuilder<Tree<Atomic>, Atomic> + Send + Sync>,
        config: Option<&ParserConfig>,
    ) -> Result<Tree<Atomic>, Box<dyn std::error::Error>> {
        let fsm = self.fsm.clone();
        let qname_collector = fsm.qname_collector();
        let default_config = ParserConfig {
            normalize_attribute_whitespace: false,
            ..Default::default()
        };
        let config = config.unwrap_or(&default_config);
        let qname_parser = QNameParser::new(fsm.qnames().clone());
        let xsi_schema_location = qname_parser.qnames().add_qname(
            Prefix("xsi"),
            Namespace("http://www.w3.org/2001/XMLSchema-instance"),
            NCName("schemaLocation"),
        );
        let xsi_no_namespace_schema_location = qname_parser.qnames().add_qname(
            Prefix("xsi"),
            Namespace("http://www.w3.org/2001/XMLSchema-instance"),
            NCName("noNamespaceSchemaLocation"),
        );
        let xsi_type = qname_parser.qnames().add_qname(
            Prefix("xsi"),
            Namespace("http://www.w3.org/2001/XMLSchema-instance"),
            NCName("type"),
        );
        let validator = Box::new(Validator {
            a: SubA {
                defined_attributes: Vec::new(),
                errors: Vec::new(),
                stream,
                identity_checker: IdentityChecker::new(fsm.identity_constraints.clone()),
            },
            b: SubB {
                attributes: Vec::new(),
                buffer: String::new(),
                stack: Stack::new(&fsm),
                qname_parser,
                xsi_schema_location,
                xsi_no_namespace_schema_location,
                xsi_type,
                fsm,
            },
            state: ValidatorState::Document,
            start_element: None,
        });
        let mut stream: StringNamesToQNames<Tree<Atomic>, _, _> =
            StringNamesToQNames::from_qname_collector(
                IteratorToTree::new(validator),
                qname_collector,
            );
        if let Err(e) = parse_to_stream(xml, config, &mut stream) {
            return Err(e)
                .map_err(|e| format!("Document is invalid: {:?} {}", stream.errors(), e).into());
        }
        if stream.has_errors() {
            let errors = stream.errors();
            // eprintln!("{:?}", errors);
            return Err(format!("Document is invalid: {:?}", errors).into());
        }
        Ok(stream.build()?)
    }
    pub fn validate_to_tree(
        &self,
        xml: &NormalizedXml,
        config: Option<&ParserConfig>,
    ) -> Result<Tree<Atomic>, Box<dyn std::error::Error>> {
        self.validate_to_stream(xml, Box::new(TreeBuilder::empty()), config)
    }
}
