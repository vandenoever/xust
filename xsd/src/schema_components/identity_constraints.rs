use super::IdentityConstraint;

#[derive(Debug, Copy, Clone, PartialEq, Default, Eq, PartialOrd, Ord)]
pub struct IdentityConstraintId(usize);

#[derive(Debug, Clone, Default)]
pub(crate) struct IdentityConstraints(Vec<IdentityConstraint>);

impl IdentityConstraints {
    pub fn new() -> Self {
        Self(Vec::new())
    }
    pub fn iter(&self) -> impl Iterator<Item = (IdentityConstraintId, &IdentityConstraint)> {
        self.0
            .iter()
            .enumerate()
            .map(|(id, t)| (IdentityConstraintId(id), t))
    }
    pub fn iter_mut(
        &mut self,
    ) -> impl Iterator<Item = (IdentityConstraintId, &mut IdentityConstraint)> {
        self.0
            .iter_mut()
            .enumerate()
            .map(|(id, t)| (IdentityConstraintId(id), t))
    }
    pub fn push(&mut self, t: IdentityConstraint) -> IdentityConstraintId {
        self.0.push(t);
        IdentityConstraintId(self.0.len() - 1)
    }
}

impl std::ops::Index<IdentityConstraintId> for IdentityConstraints {
    type Output = IdentityConstraint;
    fn index(&self, id: IdentityConstraintId) -> &Self::Output {
        &self.0[id.0]
    }
}

impl std::ops::IndexMut<IdentityConstraintId> for IdentityConstraints {
    fn index_mut(&mut self, id: IdentityConstraintId) -> &mut Self::Output {
        &mut self.0[id.0]
    }
}
