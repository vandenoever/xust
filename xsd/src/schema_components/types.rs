use super::{ContentType, Particle, Type, TypeDef};
use crate::types::{built_ins::BUILT_IN_AMOUNT, TypeId};
use std::collections::BTreeMap;
use xust_tree::qnames::QNameId;

#[derive(Debug, Clone, Default)]
pub(crate) struct Types(Vec<Type>);

impl Types {
    pub fn new() -> Self {
        Self(Vec::new())
    }
    pub fn iter(&self) -> impl Iterator<Item = (TypeId, &Type)> {
        self.0
            .iter()
            .enumerate()
            .map(|(id, t)| (TypeId(id as u16), t))
    }
    pub fn particles(&self) -> impl Iterator<Item = (&Particle, bool)> {
        self.iter()
            .filter_map(|(id, t)| match &t.def {
                TypeDef::ComplexType(ct) => Some((id, ct)),
                _ => None,
            })
            .filter_map(|(_, ct)| {
                if let ContentType::ComplexType { particle, mixed } = &ct.content_type {
                    Some((particle, *mixed))
                } else {
                    None
                }
            })
    }
    pub fn type_ids(&self) -> impl Iterator<Item = TypeId> {
        (0..self.0.len()).map(|v| TypeId(v as u16))
    }
    pub fn len(&self) -> usize {
        self.0.len()
    }
    pub fn push(&mut self, t: Type) -> TypeId {
        self.0.push(t);
        TypeId((self.0.len() - 1) as u16)
    }
    // call this function after declaring all top level simple types, but
    // before defining them or other simple types
    #[must_use]
    pub fn sort_by_depencency(
        &mut self,
        simple_type_dependencies: &BTreeMap<QNameId, QNameId>,
    ) -> bool {
        let mut nv = Vec::with_capacity(self.0.len());
        std::mem::swap(&mut nv, &mut self.0);
        let mut todo = Vec::with_capacity(simple_type_dependencies.len());
        for (pos, t) in nv.drain(..).enumerate() {
            if pos < BUILT_IN_AMOUNT {
                self.0.push(t);
                continue;
            }
            let name = t.name.unwrap();
            let done = if let Some(dep) = simple_type_dependencies.get(&name) {
                self.0.iter().any(|t| t.name == Some(*dep))
            } else {
                true
            };
            if done {
                self.0.push(t);
            } else {
                todo.push(t);
            }
        }
        let mut todo_len = todo.len();
        while !todo.is_empty() {
            std::mem::swap(&mut nv, &mut todo);
            for t in nv.drain(..) {
                let name = t.name.unwrap();
                let done = if let Some(dep) = simple_type_dependencies.get(&name) {
                    self.0.iter().any(|t| t.name == Some(*dep))
                } else {
                    true
                };
                if done {
                    self.0.push(t);
                } else {
                    todo.push(t);
                }
            }
            if todo_len == todo.len() {
                // there must be a circular dependency
                return false;
            }
            todo_len = todo.len();
        }
        true
    }
}

impl std::ops::Index<TypeId> for Types {
    type Output = Type;
    fn index(&self, id: TypeId) -> &Self::Output {
        &self.0[id.0 as usize]
    }
}

impl std::ops::IndexMut<TypeId> for Types {
    fn index_mut(&mut self, id: TypeId) -> &mut Self::Output {
        &mut self.0[id.0 as usize]
    }
}
