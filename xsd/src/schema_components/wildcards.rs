use super::Wildcard;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct WildcardId(usize);

impl WildcardId {
    pub fn any_type() -> Self {
        WildcardId(0)
    }
}

impl From<WildcardId> for usize {
    fn from(t: WildcardId) -> usize {
        t.0
    }
}

impl<'a> From<&'a WildcardId> for usize {
    fn from(t: &WildcardId) -> usize {
        t.0
    }
}

impl std::fmt::Display for WildcardId {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, Default)]
pub(crate) struct Wildcards(Vec<Wildcard>);

impl Wildcards {
    pub fn new() -> Self {
        Self(vec![Wildcard::any_type()])
    }
    pub fn iter(&self) -> impl Iterator<Item = (WildcardId, &Wildcard)> {
        self.0.iter().enumerate().map(|(id, t)| (WildcardId(id), t))
    }
    pub fn add(&mut self, t: Wildcard) -> WildcardId {
        if let Some(pos) = self.0.iter().position(|w| &t == w) {
            WildcardId(pos)
        } else {
            self.0.push(t);
            WildcardId(self.0.len() - 1)
        }
    }
}

impl std::ops::Index<WildcardId> for Wildcards {
    type Output = Wildcard;
    fn index(&self, id: WildcardId) -> &Self::Output {
        &self.0[id.0]
    }
}
