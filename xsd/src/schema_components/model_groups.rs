use super::ModelGroup;

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ModelGroupId(usize);

impl From<ModelGroupId> for usize {
    fn from(t: ModelGroupId) -> usize {
        t.0
    }
}

impl<'a> From<&'a ModelGroupId> for usize {
    fn from(t: &ModelGroupId) -> usize {
        t.0
    }
}

impl std::fmt::Display for ModelGroupId {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, Default)]
pub(crate) struct ModelGroups(Vec<ModelGroup>);

impl ModelGroups {
    pub fn new() -> Self {
        Self(vec![])
    }
    pub fn iter(&self) -> impl Iterator<Item = (ModelGroupId, &ModelGroup)> {
        self.0
            .iter()
            .enumerate()
            .map(|(id, t)| (ModelGroupId(id), t))
    }
    pub fn push(&mut self, t: ModelGroup) -> ModelGroupId {
        self.0.push(t);
        ModelGroupId(self.0.len() - 1)
    }
}

impl std::ops::Index<ModelGroupId> for ModelGroups {
    type Output = ModelGroup;
    fn index(&self, id: ModelGroupId) -> &Self::Output {
        &self.0[id.0]
    }
}

impl std::ops::IndexMut<ModelGroupId> for ModelGroups {
    fn index_mut(&mut self, id: ModelGroupId) -> &mut Self::Output {
        &mut self.0[id.0]
    }
}
