use std::{
    collections::BTreeSet,
    fmt::{Display, Write},
};
use xust_tree::{
    qnames::{NamespaceDefinition, QNameCollection, QNameId},
    stream::{Item, ItemStream},
    typed_value::XS_UNTYPED,
};
use xust_xml::write::{stream::StreamWriter, XmlOutputParameters};

use crate::{AsStream, Deserialize, Stream};

pub trait Serializable<E>: AsStream<E> + Deserialize {}

impl<E, R> Serializable<E> for R where R: Deserialize + AsStream<E> {}

pub fn serialize<E>(v: &dyn Serializable<E, QNames = E>, names: &E) -> String {
    let qnames = v.qnames(names).clone();
    let mut qname_collector = NamespaceCollector {
        qnames: &qnames,
        namespaces: BTreeSet::new(),
    };
    let namespace_definitions = Vec::new();
    v.serialize_to_stream(&mut qname_collector, names);
    let mut xml = String::new();
    let mut serializer = Serializer {
        writer: StreamWriter::new(&mut xml, XmlOutputParameters::default(), qnames),
        namespace_definitions: &namespace_definitions,
        buffer: String::new(),
    };
    v.serialize_to_stream(&mut serializer, names);
    xml
}

struct NamespaceCollector<'a> {
    qnames: &'a QNameCollection,
    namespaces: BTreeSet<QNameId>,
}

impl NamespaceCollector<'_> {
    fn add(&mut self, qname: QNameId) {
        let q = self.qnames.to_ref(qname);
        let _prefix = q.prefix();
        let _namespace = q.namespace();
        self.namespaces.insert(qname);
    }
}

impl Stream for NamespaceCollector<'_> {
    fn start_element(&mut self, qname: QNameId) {
        self.add(qname);
    }
    fn end_element(&mut self) {}
    fn start_attribute(&mut self, qname: QNameId) {
        self.add(qname);
    }
    fn write_att(&mut self, _: &dyn Display) {}
    fn end_attribute(&mut self) {}
    fn write_str(&mut self, _str: &str) {}
    fn write_element(&mut self, _element: &crate::Element) {}
}

struct Serializer<'a, W: Write> {
    writer: StreamWriter<W>,
    namespace_definitions: &'a [NamespaceDefinition],
    buffer: String,
}

impl<'a, W: Write> Stream for Serializer<'a, W> {
    fn start_element(&mut self, qname: xust_tree::qnames::QNameId) {
        self.writer.next(Item::StartElement {
            qname,
            r#type: XS_UNTYPED,
            namespace_definitions: self.namespace_definitions,
        });
        self.namespace_definitions = &[];
    }
    fn end_element(&mut self) {
        self.writer.next(Item::EndElement)
    }
    fn start_attribute(&mut self, qname: QNameId) {
        self.writer.next(Item::StartAttribute {
            qname,
            r#type: XS_UNTYPED,
        });
        self.buffer.clear();
    }
    fn write_att(&mut self, v: &dyn Display) {
        let _ = write!(&mut self.buffer, "{}", v);
    }
    fn end_attribute(&mut self) {
        self.writer.write_str(&self.buffer);
        self.writer.next(Item::EndAttribute)
    }
    fn write_str(&mut self, str: &str) {
        self.writer.next(Item::Text(str));
    }
    fn write_element(&mut self, element: &crate::Element) {
        self.writer.write_element(element);
    }
}
