use lazy_static::lazy_static;
use nom::{
    branch::alt,
    bytes::complete::{tag, take_until1},
    character::complete::{char, digit1, none_of, one_of},
    combinator::{opt, recognize},
    sequence::tuple,
    IResult,
};
use regex::Regex;
use std::sync::Arc;

fn add_class(s: &mut String, c: &str, include: bool) {
    s.push('[');
    if !include {
        s.push('^');
    }
    s.push_str(c);
    s.push(']');
}

const I_CLASS: &str = ":A-Za-z_\\xC0-\\xD6\\xD8-\\xF6\\xF8-\\u02FF\\u0370-\\u037D\\u037F-\\u1FFF\\u200C-\\u200D\\u2070-\\u218F\\u2C00-\\u2FEF\\u3001-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFFD\\U00010000-\\U000EFFFF";
const C_CLASS: &str = ":A-Za-z_\\xC0-\\xD6\\xD8-\\xF6\\xF8-\\u02FF\\u0370-\\u037D\\u037F-\\u1FFF\\u200C-\\u200D\\u2070-\\u218F\\u2C00-\\u2FEF\\u3001-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFFD\\U00010000-\\U000EFFFF\\-.0-9\\xB7\\u0300-\\u036F\\u203F-\\u2040";
const W_CLASS: &str = "\\p{P}\\p{Z}\\p{C}";
// this could also be the XML Schema pattern [\i-[:]][\c-[:]]*
const NCNAME: &str = "^[A-Za-z_\\xC0-\\xD6\\xD8-\\xF6\\xF8-\\u02FF\\u0370-\\u037D\\u037F-\\u1FFF\\u200C-\\u200D\\u2070-\\u218F\\u2C00-\\u2FEF\\u3001-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFFD\\U00010000-\\U000EFFFF][A-Za-z_\\xC0-\\xD6\\xD8-\\xF6\\xF8-\\u02FF\\u0370-\\u037D\\u037F-\\u1FFF\\u200C-\\u200D\\u2070-\\u218F\\u2C00-\\u2FEF\\u3001-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFFD\\U00010000-\\U000EFFFF\\-.0-9\\xB7\\u0300-\\u036F\\u203F-\\u2040]*$";

// [64]   	regExp	   ::=   	branch ( '|' branch )*
fn reg_exp<'a>(s: &mut String, i: &'a str) -> nom::IResult<&'a str, ()> {
    let mut i = branch(s, i)?.0;
    while let Ok((j, _)) = char::<_, ()>('|')(i) {
        s.push('|');
        i = branch(s, j)?.0;
    }
    Ok((i, ()))
}

// [65]   	branch	   ::=   	piece*
fn branch<'a>(s: &mut String, mut i: &'a str) -> IResult<&'a str, ()> {
    while let Ok((j, _)) = piece(s, i) {
        i = j;
    }
    Ok((i, ()))
}

// [66]   	piece	   ::=   	atom quantifier?
fn piece<'a>(s: &mut String, i: &'a str) -> IResult<&'a str, ()> {
    let i = atom(s, i)?.0;
    if let Ok((i, q)) = quantifier(i) {
        s.push_str(q);
        Ok((i, ()))
    } else {
        Ok((i, ()))
    }
}

// [67]   	quantifier	   ::=   	[?*+] | ( '{' quantity '}' )
fn quantifier(i: &str) -> IResult<&str, &str> {
    alt((
        recognize(one_of("?*+")),
        recognize(tuple((char('{'), quantity, char('}')))),
    ))(i)
}

// [68]   	quantity	   ::=   	quantRange | quantMin | QuantExact
// [69]   	quantRange	   ::=   	QuantExact ',' QuantExact
// [70]   	quantMin	   ::=   	QuantExact ','
// [71]   	QuantExact	   ::=   	[0-9]+
fn quantity(i: &str) -> IResult<&str, &str> {
    recognize(tuple((digit1, opt(tuple((char(','), opt(digit1)))))))(i)
}

// [72]   	atom	   ::=   	NormalChar | charClass | ( '(' regExp ')' )
fn atom<'a>(s: &mut String, i: &'a str) -> IResult<&'a str, ()> {
    if let Ok(r) = normal_char(s, i) {
        Ok(r)
    } else if let Ok(r) = char_class(s, i) {
        Ok(r)
    } else {
        let i = char('(')(i)?.0;
        s.push('(');
        let i = reg_exp(s, i)?.0;
        let i = char(')')(i)?.0;
        s.push(')');
        Ok((i, ()))
    }
}

// [73]   	NormalChar	   ::=   	[^.\?*+{}()|#x5B#x5D]	/*  N.B.:  #x5B = '[', #x5D = ']'  */
fn normal_char<'a>(s: &mut String, i: &'a str) -> IResult<&'a str, ()> {
    let (i, c) = none_of(".\\?*+{}()|[]")(i)?;
    s.push(c);
    Ok((i, ()))
}

struct ClassExpr<'a> {
    // the positive or negative group
    group: Vec<CharClass<'a>>,
    // indicate if the group is positive or negative
    pos: bool,
    // characters that are subtracted from the group
    subtract: Option<Box<ClassExpr<'a>>>,
}

impl<'a> ClassExpr<'a> {
    fn new() -> Self {
        Self {
            group: Vec::new(),
            pos: true,
            subtract: None,
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum SingleChar {
    CharEsc(char),
    CharNoEsc(char),
}

#[derive(Clone, Copy, Debug)]
enum CharClass<'a> {
    MultiCharEsc(char),
    CatEsc(&'a str),
    ComplEsc(&'a str),
    SingleChar(SingleChar),
    Range(SingleChar, SingleChar),
}

fn push_char(s: &mut String, c: SingleChar) {
    match c {
        SingleChar::CharEsc(c) => {
            s.push('\\');
            s.push(c);
        }
        SingleChar::CharNoEsc(c) => {
            s.push(c);
        }
    }
}

fn push_char_class(s: &mut String, c: CharClass) {
    match c {
        CharClass::MultiCharEsc(c) => match c {
            'i' => add_class(s, I_CLASS, true),
            'I' => add_class(s, I_CLASS, false),
            'c' => add_class(s, C_CLASS, true),
            'C' => add_class(s, C_CLASS, false),
            'w' => add_class(s, W_CLASS, false),
            'W' => add_class(s, W_CLASS, true),
            c => {
                s.push('\\');
                s.push(c)
            }
        },
        CharClass::CatEsc(c) => {
            s.push_str("\\p{");
            s.push_str(c);
            s.push('}');
        }
        CharClass::ComplEsc(c) => {
            s.push_str("\\P{");
            s.push_str(c);
            s.push('}');
        }
        CharClass::SingleChar(c) => push_char(s, c),
        CharClass::Range(a, b) => {
            push_char(s, a);
            s.push('-');
            push_char(s, b)
        }
    }
}

fn push_class_expr(s: &mut String, c: &ClassExpr) {
    s.push('[');
    if !c.pos {
        s.push('^');
    }
    for c in &c.group {
        push_char_class(s, *c);
    }
    if let Some(subtract) = &c.subtract {
        s.push_str("--");
        push_class_expr(s, subtract);
    }
    s.push(']');
}

// [74]   	charClass	   ::=   	SingleCharEsc | charClassEsc | charClassExpr | WildcardEsc
fn char_class<'a>(s: &mut String, i: &'a str) -> IResult<&'a str, ()> {
    let mut class_expr = ClassExpr::new();
    let i = if let Ok((i, c)) = single_char_esc(i) {
        push_char(s, c);
        i
    } else if let Ok((i, c)) = char_class_esc(i) {
        push_char_class(s, c);
        i
    } else if let Ok((i, _)) = char_class_expr(&mut class_expr, i) {
        push_class_expr(s, &class_expr);
        i
    } else {
        // [98]   	WildcardEsc	   ::=   	'.'
        let i = char('.')(i)?.0;
        s.push('.');
        i
    };
    Ok((i, ()))
}

// [75]   	charClassExpr	   ::=   	'[' charGroup ']'
fn char_class_expr<'a>(class_expr: &mut ClassExpr<'a>, i: &'a str) -> IResult<&'a str, ()> {
    let i = char('[')(i)?.0;
    let i = char_group(class_expr, i)?.0;
    let i = char(']')(i)?.0;
    Ok((i, ()))
}

// [76]   	charGroup	   ::=   	( posCharGroup | negCharGroup ) ( '-' charClassExpr )?
fn char_group<'a>(class_expr: &mut ClassExpr<'a>, i: &'a str) -> IResult<&'a str, ()> {
    let i = if let Ok((i, _)) = pos_char_group(class_expr, i) {
        i
    } else {
        neg_char_group(class_expr, i)?.0
    };
    if let Ok((i, _)) = char::<_, ()>('-')(i) {
        let mut subtract = ClassExpr::new();
        let i = char_class_expr(&mut subtract, i)?.0;
        class_expr.subtract = Some(Box::new(subtract));
        Ok((i, ()))
    } else {
        Ok((i, ()))
    }
}

// [77]   	posCharGroup	   ::=   	( charGroupPart )+
fn pos_char_group<'a>(class_expr: &mut ClassExpr<'a>, i: &'a str) -> IResult<&'a str, ()> {
    let (mut i, g) = char_group_part(i)?;
    class_expr.group.push(g);
    while let Ok((j, g)) = char_group_part(i) {
        class_expr.group.push(g);
        i = j;
    }
    Ok((i, ()))
}

// [78]   	negCharGroup	   ::=   	'^' posCharGroup
fn neg_char_group<'a>(class_expr: &mut ClassExpr<'a>, i: &'a str) -> IResult<&'a str, ()> {
    let i = char('^')(i)?.0;
    class_expr.pos = false;
    pos_char_group(class_expr, i)
}

// [79]   	charGroupPart	   ::=   	singleChar | charRange | charClassEsc
fn char_group_part(i: &str) -> IResult<&str, CharClass> {
    Ok(if let Ok(r) = char_class_esc(i) {
        r
    } else if let Ok((i, c)) = single_char(i) {
        (i, CharClass::SingleChar(c))
    } else {
        char_range(i)?
    })
}

// [80]   	singleChar	   ::=   	SingleCharEsc | SingleCharNoEsc
fn single_char(i: &str) -> IResult<&str, SingleChar> {
    let (i, c) = if let Ok(r) = single_char_esc(i) {
        r
    } else {
        single_char_no_esc(i)?
    };
    Ok((i, c))
}

// [81]   	charRange	   ::=   	singleChar '-' singleChar
fn char_range(i: &str) -> IResult<&str, CharClass> {
    let (i, c1) = single_char(i)?;
    let i = char('-')(i)?.0;
    let (i, c2) = single_char(i)?;
    Ok((i, CharClass::Range(c1, c2)))
}

// [82]   	SingleCharNoEsc	   ::=   	[^\#x5B#x5D] 	/*  N.B.:  #x5B = '[', #x5D = ']'  */
fn single_char_no_esc(i: &str) -> IResult<&str, SingleChar> {
    let (j, c) = none_of("[]")(i)?;
    if j.starts_with('[') {
        return Err(nom::Err::Error(nom::error::make_error(
            "",
            nom::error::ErrorKind::Fail,
        )));
    }
    Ok((j, SingleChar::CharNoEsc(c)))
}

// [83]   	charClassEsc	   ::=   	( MultiCharEsc | catEsc | complEsc )
fn char_class_esc(i: &str) -> IResult<&str, CharClass> {
    if let Ok(r) = multi_char_esc(i) {
        return Ok(r);
    }
    if let Ok(r) = cat_esc(i) {
        return Ok(r);
    }
    compl_esc(i)
}

// [84]   	SingleCharEsc	   ::=   	'\' [nrt\|.?*+(){}#x2D#x5B#x5D#x5E]	/* N.B.:  #x2D = '-', #x5B = '[', #x5D = ']', #x5E = '^' */
fn single_char_esc(i: &str) -> IResult<&str, SingleChar> {
    let i = char('\\')(i)?.0;
    let (i, c) = one_of("nrt\\|.?*+(){}-[]^")(i)?;
    Ok((i, SingleChar::CharEsc(c)))
}

// [85]   	catEsc	   ::=   	'\p{' charProp '}'
fn cat_esc(i: &str) -> IResult<&str, CharClass> {
    let i = tag("\\p{")(i)?.0;
    let (i, p) = take_until1("}")(i)?;
    let i = tag("}")(i)?.0;
    Ok((i, CharClass::CatEsc(p)))
}

// [86]   	complEsc	   ::=   	'\P{' charProp '}'
fn compl_esc(i: &str) -> IResult<&str, CharClass> {
    let i = tag("\\P{")(i)?.0;
    let (i, p) = take_until1("}")(i)?;
    let i = tag("}")(i)?.0;
    Ok((i, CharClass::ComplEsc(p)))
}

// [97]   	MultiCharEsc	   ::=   	'\' [sSiIcCdDwW]
fn multi_char_esc(i: &str) -> IResult<&str, CharClass> {
    let i = char('\\')(i)?.0;
    let (i, c) = one_of("sSiIcCdDwW")(i)?;
    Ok((i, CharClass::MultiCharEsc(c)))
}

fn translate_pattern(v: &str) -> Result<String, ()> {
    let mut s = String::with_capacity(2 * v.len());
    s.push('^');
    let i = reg_exp(&mut s, v).map_err(|_| ())?.0;
    if !i.is_empty() {
        return Err(());
    }
    s.push('$');
    Ok(s)
}

#[test]
fn test_translate_pattern() {
    let v = "([\\i-[:]][\\c-[:]]*:)?[\\i-[:]][\\c-[:]]{33}";
    let s = translate_pattern(v).unwrap();
    assert_eq!(
        s,
        format!(
            "^([[{}]--[:]][[{}]--[:]]*:)?[[{}]--[:]][[{}]--[:]]{{33}}$",
            I_CLASS, C_CLASS, I_CLASS, C_CLASS
        )
    );
}
#[test]
fn test_translate_pattern2() {
    let v = "[\\C]+";
    let s = translate_pattern(v).unwrap();
    assert_eq!(s, format!("^[[^{}]]+$", C_CLASS));
}

#[derive(Debug, Clone)]
struct PatternInner {
    patterns: Vec<Regex>,
    base: Option<Arc<PatternInner>>,
}

impl PatternInner {
    fn test(&self, v: &str) -> bool {
        /*
        println!(
            "checking '{}' against '{}': {}",
            v,
            self.0.as_str(),
            self.0.is_match(v)
        );
        */
        (self.patterns.is_empty() || self.patterns.iter().any(|p| p.is_match(v)))
            && self.base.as_ref().map(|b| b.test(v)).unwrap_or(true)
    }
}

lazy_static! {
    pub static ref EMPTY: Pattern = Pattern(Arc::new(PatternInner {
        patterns: Vec::new(),
        base: None,
    }));
}

#[derive(Debug, Clone, PartialEq)]
pub struct Pattern(Arc<PatternInner>);

impl Pattern {
    pub fn empty() -> Self {
        EMPTY.clone()
    }
    pub fn new(patterns: &[&str]) -> Result<Self, ()> {
        let patterns = patterns
            .iter()
            .map(|s| Self::regex(s))
            .collect::<Result<_, _>>()?;
        Ok(Pattern(Arc::new(PatternInner {
            patterns,
            base: None,
        })))
    }
    pub fn derive(patterns: &[&str], base: &Pattern) -> Result<Self, ()> {
        let patterns = patterns
            .iter()
            .map(|s| Self::regex(s))
            .collect::<Result<_, _>>()?;
        Ok(Pattern(Arc::new(PatternInner {
            patterns,
            base: Some(base.0.clone()),
        })))
    }
    fn regex(rg: &str) -> Result<Regex, ()> {
        // implement https://www.w3.org/TR/xmlschema11-2/#dt-regex
        // especially \c and \i https://www.w3.org/TR/xmlschema11-2/#cces-mce
        let re = translate_pattern(rg);
        /*
        if let Ok(re) = &re {
            println!("'{}' -> '{}': {}", rg, re, Regex::new(&re).is_ok());
        } else {
            println!("'{}' -> ###", rg);
        }
        */
        let re = re?;
        Regex::new(&re).map_err(|_e| {
            // dbg!(rg, re, e);
        })
    }
    pub fn test(&self, v: &str) -> bool {
        self.0.test(v)
    }
    pub fn ncname() -> Self {
        Self(Arc::new(PatternInner {
            patterns: vec![Regex::new(NCNAME).unwrap()],
            base: None,
        }))
    }
}

impl PartialEq for PatternInner {
    fn eq(&self, o: &Self) -> bool {
        self.patterns.len() == o.patterns.len()
            && self
                .patterns
                .iter()
                .zip(o.patterns.iter())
                .all(|(a, b)| a.as_str() == b.as_str())
            && self.base == o.base
    }
}
