use crate::{
    parse::MaxOccurs,
    schema_components::{
        identity_constraints::IdentityConstraints, Attribute, AttributeDeclaration, Compositor,
        ContentType, ElementDeclaration, ModelGroup, Particle, SchemaComponents, SimpleType, Term,
        TypeDef, WildcardId, Wildcards,
    },
    TypeId,
};
use std::collections::BTreeMap;
use std::sync::Arc;
use xust_tree::qnames::{QNameCollection, QNameCollector, QNameId};

#[derive(Debug)]
pub(crate) struct Element {
    pub qname: Option<QNameId>,
    pub r#type: Option<TypeId>,
    pub dfa: dfa::DFA,
    pub atts: Vec<Attribute>,
    pub attribute_wildcard: Option<WildcardId>,
}

#[derive(Clone)]
pub(crate) struct FSM {
    qnames: QNameCollection,
    elements: Arc<Vec<Element>>,
    pub attributes: Arc<BTreeMap<QNameId, AttributeDeclaration>>,
    pub simple_types: Arc<[SimpleType]>,
    pub wildcards: Arc<Wildcards>,
    pub identity_constraints: Arc<IdentityConstraints>,
}

impl FSM {
    pub fn qname_collector(&self) -> QNameCollector {
        QNameCollector::from_qname_collection(self.qnames.clone())
    }
    pub fn qnames(&self) -> &QNameCollection {
        &self.qnames
    }
    pub fn elements(&self) -> &Arc<Vec<Element>> {
        &self.elements
    }
}

pub(crate) mod dfa {
    use crate::{
        parse::MaxOccurs,
        schema_components::{identity_constraints::IdentityConstraintId, WildcardId},
    };
    use xust_tree::qnames::QNameId;
    #[derive(Debug)]
    pub(crate) struct AllParticle {
        pub dfa: DFA,
        pub min_occurs: usize,
        pub max_occurs: MaxOccurs,
    }
    #[derive(Debug)]
    pub(crate) struct All {
        pub particles: Vec<AllParticle>,
        pub mixed: bool,
        pub min_occurs: usize,
        pub max_occurs: MaxOccurs,
    }
    #[derive(Debug)]
    pub(crate) enum DFA {
        Empty,
        Any(WildcardId),
        Children {
            start_node: usize,
            nodes: Vec<Node>,
            mixed: bool,
        },
        All(All),
        SimpleType(crate::types::TypeId),
    }
    impl DFA {
        pub fn start_node(&self) -> usize {
            if let DFA::Children { start_node, .. } = self {
                *start_node
            } else {
                0
            }
        }
    }
    #[derive(Debug)]
    pub struct Node {
        pub end_node: bool,
        pub edges: Vec<Edge>,
    }
    #[derive(Debug, Clone)]
    pub struct Edge {
        pub destination: usize,
        pub label: Label,
        pub dfa: usize,
        pub identity_constraints: Vec<IdentityConstraintId>,
        pub r#default: Option<String>,
    }
    #[derive(Clone, Copy, PartialEq, Eq, Debug)]
    pub enum Label {
        Tag(QNameId),
        Any,
    }
}

#[derive(Default)]
struct NFA {
    nodes: Vec<Node>,
}

impl NFA {
    fn new() -> Self {
        Self {
            nodes: vec![Node::end_node()],
        }
    }
    fn add_node(&mut self) -> usize {
        let node_id = self.nodes.len();
        self.nodes.push(Node::new());
        node_id
    }
    fn add_edge(&mut self, source: usize, destination: usize, label: Label) {
        self.nodes[source].edges.push(Edge { label, destination });
    }
}

struct Node {
    edges: Vec<Edge>,
    end_node: bool,
}

impl Node {
    fn end_node() -> Self {
        Self {
            edges: Vec::new(),
            end_node: true,
        }
    }
    fn new() -> Self {
        Self {
            edges: Vec::new(),
            end_node: false,
        }
    }
}

#[derive(PartialEq, PartialOrd, Eq, Ord)]
enum Label {
    Epsilon,
    Any(WildcardId),
    Name(ElementDeclaration),
}

struct Edge {
    label: Label,
    destination: usize,
}

fn convert_all_to_dfa(
    model: &SchemaComponents,
    particle: &Particle,
    group: &ModelGroup,
    mixed: bool,
    any_offset: usize,
) -> Result<dfa::DFA, Box<dyn std::error::Error>> {
    let mut dfas = Vec::with_capacity(group.particles.len());
    for particle in &group.particles {
        let mut nfa = NFA::new();
        let start_node = translate_particle(&mut nfa, particle, 0, model, 0)?;
        let dfa = convert_to_dfa(nfa, start_node, any_offset, mixed, &model.qnames)?;
        dfas.push(dfa::AllParticle {
            dfa,
            min_occurs: particle.min_occurs,
            max_occurs: particle.max_occurs,
        });
    }
    Ok(dfa::DFA::All(dfa::All {
        particles: dfas,
        min_occurs: particle.min_occurs,
        max_occurs: particle.max_occurs,
        mixed,
    }))
}

pub(crate) fn build(model: &SchemaComponents) -> Result<FSM, Box<dyn std::error::Error>> {
    if model.is_empty() {
        return Err("The model is empty.".into());
    }
    let mut dfas = Vec::with_capacity(model.types.len() + 2);
    let any_offset = model.types.len();
    // root node
    for (id, t) in model.types.iter() {
        let element = match &t.def {
            TypeDef::ComplexType(ec) => {
                let dfa = match &ec.content_type {
                    ContentType::Empty => dfa::DFA::Empty,
                    ContentType::SimpleType(st) => dfa::DFA::SimpleType(*st),
                    ContentType::ComplexType { mixed, particle } => {
                        let all_dfa = if let Term::Group(g) = &particle.term {
                            let group = model.get_group(*g);
                            if group.compositor == Compositor::All {
                                Some(convert_all_to_dfa(
                                    model, particle, group, *mixed, any_offset,
                                )?)
                            } else {
                                None
                            }
                        } else {
                            None
                        };
                        if let Some(dfa) = all_dfa {
                            dfa
                        } else {
                            let mut nfa = NFA::new();
                            let start_node = translate_particle(&mut nfa, particle, 0, model, 0)?;
                            convert_to_dfa(nfa, start_node, any_offset, *mixed, &model.qnames)?
                        }
                    }
                };
                let atts = ec.attribute_uses.clone();
                let attribute_wildcard = ec.attribute_wildcard;
                Element {
                    qname: t.name,
                    r#type: Some(id),
                    dfa,
                    atts,
                    attribute_wildcard,
                }
            }
            TypeDef::SimpleType(_st) => Element {
                qname: t.name,
                r#type: Some(id),
                dfa: dfa::DFA::SimpleType(id),
                atts: Vec::new(),
                attribute_wildcard: Some(WildcardId::any_type()),
            },
        };
        dfas.push(element);
    }
    for (w, _) in model.wildcards.iter() {
        dfas.push(Element {
            qname: None,
            r#type: None,
            dfa: dfa::DFA::Any(w),
            atts: Vec::new(),
            attribute_wildcard: Some(w),
        });
    }
    dfas.push(Element {
        qname: None,
        r#type: None,
        dfa: dfa::DFA::Children {
            start_node: 0,
            mixed: false,
            nodes: vec![
                dfa::Node {
                    end_node: false,
                    edges: model
                        .elements()
                        .map(|ed| dfa::Edge {
                            destination: 1,
                            label: dfa::Label::Tag(ed.tag),
                            dfa: ed.content.into(),
                            identity_constraints: ed.identity_constraints.clone(),
                            r#default: ed.r#default.clone(),
                        })
                        .collect(),
                },
                dfa::Node {
                    end_node: true,
                    edges: Vec::new(),
                },
            ],
        },
        atts: Vec::new(),
        attribute_wildcard: None,
    });
    let fsm = FSM {
        qnames: model.qnames.clone(),
        elements: Arc::new(dfas),
        simple_types: model.simple_types.clone(),
        attributes: model.attributes.clone(),
        wildcards: model.wildcards.clone(),
        identity_constraints: model.identity_constraints.clone(),
    };
    Ok(fsm)
}

fn translate_particle_1(
    nfa: &mut NFA,
    cp: &Particle,
    next: usize,
    model: &SchemaComponents,
    depth: usize,
) -> Result<usize, Box<dyn std::error::Error>> {
    let node = nfa.add_node();
    match &cp.term {
        Term::TopLevelElement(e) => {
            let e = &model.elements[*e];
            nfa.add_edge(node, next, Label::Name(e.clone()));
        }
        Term::LocalElement(e) => {
            nfa.add_edge(node, next, Label::Name(e.clone()));
        }
        Term::Group(g) => {
            let group = model.get_group(*g);
            match &group.compositor {
                Compositor::Sequence => {
                    let mut n = next;
                    for cp in group.particles.iter().rev() {
                        n = translate_particle(nfa, cp, n, model, depth + 1)?;
                    }
                    nfa.add_edge(node, n, Label::Epsilon);
                }
                Compositor::Choice => {
                    for cp in &group.particles {
                        let n = translate_particle(nfa, cp, next, model, depth + 1)?;
                        nfa.add_edge(node, n, Label::Epsilon);
                    }
                }
                Compositor::All => {
                    return Err("<all/> is not allowed here.".into());
                }
            }
        }
        Term::Any(a) => {
            nfa.add_edge(node, next, Label::Any(*a));
        }
    }
    Ok(node)
}

fn translate_particle(
    nfa: &mut NFA,
    cp: &Particle,
    next: usize,
    model: &SchemaComponents,
    depth: usize,
) -> Result<usize, Box<dyn std::error::Error>> {
    if depth > 100 {
        return Err("Recursion too deep.".into());
    }
    Ok(match (cp.min_occurs, cp.max_occurs) {
        (1, MaxOccurs::Bounded(1)) => translate_particle_1(nfa, cp, next, model, depth)?,
        (0, MaxOccurs::Bounded(1)) => {
            let node1 = nfa.add_node();
            let sub = translate_particle_1(nfa, cp, next, model, depth)?;
            nfa.add_edge(node1, sub, Label::Epsilon);
            nfa.add_edge(node1, next, Label::Epsilon);
            node1
        }
        (0, MaxOccurs::Unbounded) => {
            let node1 = nfa.add_node();
            let sub = translate_particle_1(nfa, cp, node1, model, depth)?;
            nfa.add_edge(node1, sub, Label::Epsilon);
            nfa.add_edge(node1, next, Label::Epsilon);
            node1
        }
        (1, MaxOccurs::Unbounded) => {
            let node1 = nfa.add_node();
            let node2 = nfa.add_node();
            let sub = translate_particle_1(nfa, cp, node2, model, depth)?;
            nfa.add_edge(node1, sub, Label::Epsilon);
            nfa.add_edge(node2, sub, Label::Epsilon);
            nfa.add_edge(node2, next, Label::Epsilon);
            node1
        }
        (min, _) if min > 50 => {
            return Err("minOccurs is larger than supported.".into());
        }
        (_, MaxOccurs::Bounded(max)) if max > 50 => {
            return Err("maxOccurs is larger than supported.".into());
        }
        (min, MaxOccurs::Bounded(max)) if min > max => {
            return Err("minOccurs is larger than maxOccurs.".into());
        }
        (min, max) => {
            // see https://www.cogsci.ed.ac.uk/~ht/XML_Europe_2003.html
            let (mut b, _t) = if let MaxOccurs::Bounded(max) = max {
                let mut t = next;
                for _ in min..max {
                    let b = translate_particle_1(nfa, cp, t, model, depth)?;
                    nfa.add_edge(b, t, Label::Epsilon);
                    t = b;
                }
                (t, t)
            } else {
                let t = nfa.add_node();
                let b = translate_particle_1(nfa, cp, t, model, depth)?;
                nfa.add_edge(t, b, Label::Epsilon);
                nfa.add_edge(b, next, Label::Epsilon);
                (b, t)
            };
            for _ in 0..min {
                b = translate_particle_1(nfa, cp, b, model, depth)?;
            }
            b
        }
    })
}

fn get_epsilon_closure(nodes: &[Node], start: usize) -> Vec<usize> {
    let mut closure = vec![start];
    expand_epsilon_closure(nodes, &mut closure);
    closure
}

fn expand_epsilon_closure(nodes: &[Node], closure: &mut Vec<usize>) {
    let mut pos = 0;
    loop {
        let node = &nodes[closure[pos]];
        for e in &node.edges {
            if e.label == Label::Epsilon && !closure.contains(&e.destination) {
                closure.push(e.destination);
            }
        }
        pos += 1;
        if pos == closure.len() {
            closure.sort_unstable();
            return;
        }
    }
}

fn get_unique_edges<'a>(epsilon: &[usize], nodes: &'a [Node]) -> BTreeMap<&'a Label, Vec<usize>> {
    let mut map: BTreeMap<&Label, Vec<usize>> = BTreeMap::new();
    for e in epsilon
        .iter()
        .map(|e| &nodes[*e])
        .flat_map(|node| &node.edges)
        .filter(|e| e.label != Label::Epsilon)
    {
        let entry = map.entry(&e.label).or_default();
        if !entry.contains(&e.destination) {
            entry.push(e.destination);
        }
    }
    for v in &mut map.values_mut() {
        expand_epsilon_closure(nodes, v);
    }
    map
}

fn is_end_node(nfa: &NFA, nodes: &[usize]) -> bool {
    for p in nodes {
        if nfa.nodes[*p].end_node {
            return true;
        }
    }
    false
}

// convert an NFA to a DFA
// The NFA consists of epsilon closure. Those are groups of nodes that can be
// reached via epilon edges. Such groups are the new states in the DFA.
//
// 1) find the epsilon closure for the start_node in the NFA
// 2) get all the unique non-epsilon edges and the closures that they point to

fn convert_to_dfa(
    nfa: NFA,
    start_node: usize,
    any_offset: usize,
    mixed: bool,
    _qnames: &QNameCollection,
) -> Result<dfa::DFA, Box<dyn std::error::Error>> {
    // print_nfa(_qnames, &nfa, start_node);
    // https://www.javatpoint.com/automata-conversion-from-nfa-with-null-to-dfa
    let mut states = vec![get_epsilon_closure(&nfa.nodes, start_node)];
    let mut nodes = vec![dfa::Node {
        edges: Vec::new(),
        end_node: is_end_node(&nfa, &states[0]),
    }];
    let mut pos = 0;
    loop {
        let r = get_unique_edges(&states[pos], &nfa.nodes);
        for (k, epsilon) in r {
            let epos = if let Some(epos) = states.iter().position(|s| *s == epsilon) {
                epos
            } else {
                nodes.push(dfa::Node {
                    edges: Vec::new(),
                    end_node: is_end_node(&nfa, &epsilon),
                });
                states.push(epsilon);
                states.len() - 1
            };
            nodes[pos].edges.push(convert_edge(k, epos, any_offset)?);
        }
        pos += 1;
        if pos == states.len() {
            break;
        }
    }
    // print_dfa(_qnames, &nodes, 0);
    Ok(dfa::DFA::Children {
        start_node: 0,
        mixed,
        nodes,
    })
}

fn convert_edge(
    label: &Label,
    destination: usize,
    any_offset: usize,
) -> Result<dfa::Edge, Box<dyn std::error::Error>> {
    let edge = match label {
        Label::Name(ed) => dfa::Edge {
            destination,
            label: dfa::Label::Tag(ed.tag),
            dfa: ed.content.into(),
            identity_constraints: ed.identity_constraints.clone(),
            r#default: ed.r#default.clone(),
        },
        Label::Any(w) => dfa::Edge {
            destination,
            label: dfa::Label::Any,
            dfa: any_offset + usize::from(w),
            identity_constraints: Vec::new(),
            r#default: None,
        },
        Label::Epsilon => {
            return Err("Edge has non-name label.".into());
        }
    };
    Ok(edge)
}

#[allow(dead_code)]
fn check_dfa(nodes: &[dfa::Node]) {
    for node in nodes {
        let edges = &node.edges;
        if !edges.is_empty() {
            for a in 0..edges.len() - 1 {
                for b in a + 1..edges.len() {
                    if edges[a].label == edges[b].label {
                        println!("oops! {} {}", a, b);
                    }
                }
            }
        }
    }
}

fn print_nfa_label(qnames: &QNameCollection, label: &Label) {
    match label {
        Label::Epsilon => print!("ɛ"),
        Label::Any(_) => print!("*"),
        Label::Name(ed) => print!("<{}/>", qnames.to_ref(ed.tag).local_name()),
    }
}

fn print_dfa_label(qnames: &QNameCollection, label: &dfa::Label) {
    match label {
        dfa::Label::Any => print!("*"),
        dfa::Label::Tag(tag) => print!("<{}/>", qnames.to_ref(*tag).local_name()),
    }
}

#[allow(dead_code)]
fn print_nfa(qnames: &QNameCollection, nfa: &NFA, start_node: usize) {
    println!("digraph NFA {{\n\trankdir=LR;");
    print!("\tnode [shape = doublecircle]");
    for (pos, node) in nfa.nodes.iter().enumerate() {
        if node.end_node {
            print!(" {}", pos);
        }
    }
    println!(";\n\tnode [shape = circle];\n\tinit [shape = point];");
    println!("\tinit -> {};", start_node);
    for (pos, node) in nfa.nodes.iter().enumerate() {
        for edge in &node.edges {
            print!("\t{} -> {} [label=\"", pos, edge.destination);
            print_nfa_label(qnames, &edge.label);
            println!("\"];");
        }
    }
    println!("}}");
}

#[allow(dead_code)]
fn print_dfa(qnames: &QNameCollection, nodes: &[dfa::Node], start_node: usize) {
    println!("digraph DFA {{\n\trankdir=LR;");
    print!("\tnode [shape = doublecircle]");
    for (pos, node) in nodes.iter().enumerate() {
        if node.end_node {
            print!(" {}", pos);
        }
    }
    println!(";\n\tnode [shape = circle];\n\tinit [shape = point];");
    println!("\tinit -> {};", start_node);
    for (pos, node) in nodes.iter().enumerate() {
        for edge in &node.edges {
            print!("\t{} -> {} [label=\"", pos, edge.destination);
            print_dfa_label(qnames, &edge.label);
            println!("\"];");
        }
    }
    println!("}}");
}
