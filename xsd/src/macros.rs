#[macro_export]
macro_rules! use_xsd {
    () => {
        use xust_tree::qnames::QName;
        #[allow(unused_imports)]
        use $crate::{
            choice, choice_member_parse, complexType, element, element_type, member_decl, ns,
            parse::xsd::ct, seq_member_parse, sequence, serialize_member, simpleType, use_xsd,
        };
    };
}

#[macro_export]
macro_rules! ns {
    (
        $struct:ident $ns:expr => $( $name:tt => $str:tt )*
    ) => {
        #[allow(dead_code)]
        pub struct $struct {
            $($name: xust_tree::qnames::QNameId),*
        }
        impl $struct {
            #[allow(unused_variables)]
            fn new(qnames: &QNameCollection) -> Self {
                let ns = Namespace($ns);
                Self {
                    $( $name: qnames.add_qname(Prefix(""), ns, NCName($str)) ),*
                }
            }
        }
    };
}

#[macro_export]
macro_rules! constructor {
    ($group:tt - $c:path) => {
        $c
    };
    ($m:tt $t:tt $c:path) => {
        $crate::parse::name_and_type(
            |env: &mut super::super::P| env.names.qnames.get(env.names.$m.$t),
            $c,
        )
    };
}

#[macro_export]
macro_rules! member_decl {
    (v + $mixed:tt $type:path) => { Vec<$type> };
    (o + $mixed:tt $type:path) => { Option<$type> };
    ($mode:tt + $mixed:tt $type:path) => { $type };
    (v $mod:tt m $type:path) => { Vec<($type, String)> };
    (o $mod:tt m $type:path) => { Option<($type, String)> };
    ($mode:tt $mod:tt m $type:path) => { ($type, String) };
    (v $mod:tt $mixed:tt $type:path) => { Vec<$type> };
    (o $mod:tt $mixed:tt $type:path) => { Option<$type> };
    ($mode:tt $mod:tt $mixed:tt $type:path) => { $type };
}

#[macro_export]
macro_rules! element_member_parse {
    ($env:ident $e:ident o m $mod:tt $c:tt) => {
        if let Some(e) = $e {
            let r = $c($env, e.clone())?;
            let s = $crate::parse::mixed_text(e);
            (r.0, r.1.map(|v| (v, s)))
        } else {
            (None, None)
        }
    };
    ($env:ident $e:ident o $mixed:tt $mod:tt $c:tt) => {
        if let Some(e) = $e {
            $c($env, e)?
        } else {
            (None, None)
        }
    };
    ($env:ident $e:ident r m $mod:tt $c:tt) => {
        $crate::parse::req_mixed($c)($env, $e)?
    };
    ($env:ident $e:ident r $mixed:tt $mod:tt $c:tt) => {
        $crate::parse::req($c)($env, $e)?
    };
    ($env:ident $e:ident d m $mod:tt $c:tt) => {
        $crate::parse::req_or_default_mixed($c)($env, $e)?
    };
    ($env:ident $e:ident d $mixed:tt $mod:tt $c:tt) => {
        $crate::parse::req_or_default($c)($env, $e)?
    };
    ($env:ident $e:ident v m - $c:tt) => {
        $crate::parse::many0_mixed($c)($env, $e)?
    };
    ($env:ident $e:ident v $mixed:tt $mod:tt $c:tt) => {
        $crate::parse::many0($c)($env, $e)?
    };
}

#[macro_export]
macro_rules! element {
    ( $mod:tt $name:ident $struct:ident $id:tt $mixed:tt $( $member:ident($type:path): $mode:tt $m:tt $t:tt $c:path ),*) => {
        element!($mod $name $struct $id $mixed attributes{} $( $member($type): $mode $m $t $c ),* );
    };
    ( $mod:tt $name:ident $struct:ident $id:tt $mixed:tt attributes { $( $amod:tt $a:ident $b:ident $amode:tt $def:tt $atype:path ),* } $( $member:ident($type:path): $mode:tt $m:tt $t:tt $c:path ),*) => {
        element!(declare $struct $id $mixed attributes { $( $a $b $amode $def $atype ),* } $( $member($type): $mode $m ),*);
        pub fn $name<'a>(env: &mut super::super::P, element: $crate::Element) -> $crate::parse::EResult<'a, $struct> {
            if !$crate::parse::check_name(env.names.qnames.get(env.names.$mod.$name), &element) {
                return Ok((Some(element), None));
            }
            use $crate::xsd2rs::traits::Introspection;
            let v;
            element!(parse v env element $struct $mixed attributes { $( $amod $a $b $amode $def $atype ),* } $( $member $mode $m $t $c ),*);
            return Ok(($crate::parse::next_element(&element), Some(v)));
        }
        impl $crate::Deserialize for $struct {
            type QNames = super::super::QNames;
            fn from_bytes(xml: Vec<u8>, validator: Option<&$crate::xsd_validator::XsdValidator>) -> std::result::Result<(super::super::QNames, Self), Box<dyn std::error::Error>> {
                let tree = $crate::parse::bytes_to_tree(xml, validator)?;
                let root = xust_tree::node::Node::root(tree);
                let document_element = $crate::parse::next_element_sibling(root.first_child())
                    .ok_or_else(|| "No document element found.")?;
                Self::from_document_element(document_element, validator.is_some())
            }
            fn from_document_element(document_element: Element, validate: bool) -> std::result::Result<(super::super::QNames, Self), Box<dyn std::error::Error>> {
                let qnames = super::super::qnames(document_element.qnames().clone());
                let mut env = $crate::parse::ParseEnv {
                    names: qnames,
                    errors: $crate::error::ErrorCollector::new(1),
                    validate
                };
                let instance = {
                    let (left, instance) =
                        $name(&mut env, document_element).map_err(|e| format!("{}", e))?;
                    if let Some(left) = left {
                        return Err(format!("There was trailing data in the file: {:?}", left.node_name()).into());
                    }
                    instance.ok_or_else(||"Could not parse")?
                };
                Ok((env.names, instance))
            }
            fn qnames<'a>(&self, qnames: &'a super::super::QNames) -> &'a xust_tree::qnames::QNameCollection {
                &qnames.qnames
            }
        }
        impl $crate::AsStream<super::super::QNames>  for $struct {
            fn serialize_to_stream(&self, out: &mut dyn $crate::Stream, qnames: &super::super::QNames) {
                out.start_element(qnames.$mod.$name);
                $(
                    let a = &self.$b;
                    let qname = qnames.$amod.$a;
                    element!(serialize_attribute out $amode $def a qname);
                )*
                element!(serialize_first_text out self $mixed);
                $(
                    serialize_member!((self.$member) $mode $mixed $m $t out qnames);
                )*
                out.end_element();
            }
        }
    };
    (declare $struct:ident $id:tt $mixed:tt attributes { $( $a:ident $b:ident $amode:tt $def:tt $atype:path ),* } $( $member:ident($type:path): $mode:tt $mod:tt ),*) => {
        #[allow(unused_imports)]
        use $crate::parse::*;
        element!($struct $mixed attributes { $( $b $amode $def $atype ),* } $( $member($type): $mode $mod ),*);
        impl $crate::xsd2rs::traits::Introspection for $struct {
            const TYPE_ID: $crate::TypeId = $crate::TypeId::from($id);
        }
    };
    ($struct:ident - attributes { $( $b:ident $amode:tt $def:tt $atype:path ),* } $( $member:ident($type:path): $mode:tt $mod:tt ),* ) => {
        #[derive(Debug)]
        pub struct $struct {
            $( pub $b: element!(attr_decl $amode $def $atype), )*
            $( pub $member: member_decl!($mode $mod - $type), )*
        }
    };
    ($struct:ident m attributes { $( $b:ident $amode:tt $def:tt $atype:path ),* } $( $member:ident($type:path): $mode:tt $mod:tt ),* ) => {
        #[derive(Debug)]
        pub struct $struct {
            $( pub $b: element!(attr_decl $amode $def $atype), )*
            pub first_text: String,
            $( pub $member: member_decl!($mode $mod m $type), )*
        }
    };
    (parse $v:ident $env:ident $element:ident $struct:ident $mixed:tt attributes { $( $amod:tt $a:ident $b:ident $amode:tt $def:tt $atype:path ),* } $( $member:ident $mode:tt $m:tt $t:tt $c:path ),*) => {
        $crate::parse::check_element_type($env.validate, $struct::TYPE_ID, &$element)?;
        $(
            let def: Option<&str> = $element!(attr_def $def);
            let $b = <element!(attr_decl $amode $def $atype)>::parse_attribute($env.names.qnames.get($env.names.$amod.$a), &$element, def)?;
        )*
        let child;
        element!(parse first_text child first_text $element $mixed);
        let child = $crate::parse::next_element_sibling(child);
        $(
            let (child, $member) = $crate::element_member_parse!($env child $mode $mixed $m ($crate::constructor!($m $t $c)));
        )*
        $crate::parse::check_done(child)?;
        element!($v first_text $struct $mixed attributes { $( $b ),* } $( $member ),*);
    };
    ($v:ident $first_text:ident $struct:ident - attributes { $( $b:ident ),* } $( $member:ident ),*) => {
        $v = $struct {
            $( $b, )*
            $( $member, )*
        };
    };
    ($v:ident $first_text:ident $struct:ident m attributes { $( $b:ident ),* } $( $member:ident ),*) => {
        $v = $struct {
            $( $b, )*
            first_text: $first_text,
            $( $member, )*
        };
    };
    (parse first_text $child:ident $first_text:ident $element:ident -) => {
        $child = $element.first_child();
    };
    (parse first_text $child:ident $first_text:ident $element:ident m) => {
        let $first_text;
        $child = first_child_if_text(&$element)?;
        if let Some(n) = &$child {
            $first_text = n.string_value().into();
        } else {
            $first_text = String::new();
        }
    };
    (serialize_first_text $out:ident $self:ident -) => {};
    (serialize_first_text $out:ident $self:ident m) => {
        $out.write_str(&$self.first_text);
    };
    (serialize_attribute $out:ident o - $a:ident $qname:ident) => {
        if let Some(a) = $a {
            $out.start_attribute($qname);
            $out.write_att(a);
            $out.end_attribute();
        }
    };
    (serialize_attribute $out:ident ov - $a:ident $qname:ident) => {
        if let Some(a) = $a {
            $out.start_attribute($qname);
            for a in a {
                $out.write_att(a);
            }
            $out.end_attribute();
        }
    };
    (serialize_attribute $out:ident v - $a:ident $qname:ident) => {
        $out.start_attribute($qname);
        for a in $a {
            $out.write_att(a);
        }
        $out.end_attribute();
    };
    (serialize_attribute $out:ident $amode:tt $def:tt $a:ident $qname:ident) => {
        $out.start_attribute($qname);
        $out.write_att($a);
        $out.end_attribute();
    };
    (attr_decl o - $atype:path) => { Option<$atype> };
    (attr_decl ov - $atype:path) => { Option<Vec<$atype>> };
    (attr_decl v - $atype:path) => { Vec<$atype> };
    (attr_decl $amode:tt $def:tt $atype:path) => { $atype };
    (attr_def -) => { None };
    (attr_def $def:tt) => { Some($def) };
}

#[macro_export]
macro_rules! serialize_member {
    (($member:expr) v $mixed:tt $m:tt $t:tt $out:ident $qnames:ident) => {
        for m in $member.iter() {
            serialize_member!((m) r $mixed $m $t $out $qnames);
        }
    };
    (($member:expr) o $mixed:tt $m:tt $t:tt $out:ident $qnames:ident ) => {
        if let Some(m) = &$member {
            serialize_member!((m) r $mixed $m $t $out $qnames);
        }
    };
    (($member:expr) r $mixed:tt $m:tt - $out:ident $qnames:ident ) => {
        $member.serialize_to_stream($out, $qnames);
    };
    (($member:expr) r m $m:tt $t:tt $out:ident $qnames:ident ) => {
        $out.start_element($qnames.$m.$t);
        $member.0.serialize_to_stream($out, $qnames);
        $out.end_element();
        $member.1.serialize_to_stream($out, $qnames);
    };
    (($member:expr) r $mixed:tt $m:tt $t:tt $out:ident $qnames:ident ) => {
        $out.start_element($qnames.$m.$t);
        $member.serialize_to_stream($out, $qnames);
        $out.end_element();
    };
    (($member:expr) d $mixed:tt $m:tt $t:tt $out:ident $qnames:ident ) => {
        serialize_member!(($member) r $mixed $m $t $out $qnames);
    };
}

#[macro_export]
macro_rules! simpleType {
    ( $name:ident $struct:ident ) => {
        pub type $struct = String;
        pub fn $name<'a, P>(
            _env: &mut P,
            e: $crate::Element,
        ) -> $crate::parse::CtResult<'a, String> {
            Ok((
                $crate::parse::next_element(&e),
                e.string_value().to_string(),
            ))
        }
    };
}

#[macro_export]
macro_rules! complexType {
    ( $name:ident $struct:ident $id:tt $mixed:tt $( $member:ident($type:path): $mode:tt $mod:tt $t:tt $c:path ),*) => {
        complexType!($name $struct $id $mixed attributes{} $( $member($type): $mode $mod $t $c ),* );
    };
    ( $name:ident $struct:ident $id:tt $mixed:tt attributes { $( $amod:tt $a:ident $b:ident $amode:tt $def:tt $atype:path ),* } $( $member:ident($type:path): $mode:tt $m:tt $t:tt $c:path ),*) => {
        element!(declare $struct $id $mixed attributes { $( $a $b $amode $def $atype ),* } $( $member($type): $mode $m ),*);
        pub fn $name<'a>(env: &mut super::super::P, element: $crate::Element) -> $crate::parse::CtResult<'a, $struct> {
            use $crate::xsd2rs::traits::Introspection;
            let v;
            element!(parse v env element $struct $mixed attributes { $( $amod $a $b $amode $def $atype ),* } $( $member $mode $m $t $c ),*);
            Ok(($crate::parse::next_element(&element), v))
        }
        impl $crate::AsStream<super::super::QNames>  for $struct {
            #[allow(unused_variables)]
            fn serialize_to_stream(&self, out: &mut dyn $crate::Stream, qnames: &super::super::QNames) {
                $(
                    let a = &self.$b;
                    let qname = qnames.$amod.$a;
                    element!(serialize_attribute out $amode $def a qname);
                )*
                element!(serialize_first_text out self $mixed);
                $(
                    serialize_member!((self.$member) $mode $mixed $m $t out qnames);
                )*
            }
        }
    }
}

#[macro_export]
macro_rules! element_type {
    ( $mod:tt $name:ident($struct:path) $c:path ) => {
        pub fn $name<'a>(
            env: &mut super::super::P,
            element: $crate::Element,
        ) -> $crate::parse::EResult<'a, $struct> {
            if !$crate::parse::check_name(env.names.qnames.get(env.names.$mod.$name), &element) {
                return Ok((Some(element), None));
            }
            let (next, v) = $c(env, element)?;
            Ok((next, Some(v)))
        }
    };
    ( $mod:tt $name:ident($struct:path) $id:tt $c:path ) => {
        element_type!($mod $name($struct) $c);
    };
}

#[macro_export]
macro_rules! choice {
    (
        $name:ident $enum:ident $mixed:tt $( $choice:ident($type:path): $mode:ident $mod:tt $t:tt $f:path ),*
    ) => {
        choice!(internal $name $enum $mixed $( $choice($type): $mode $mod $t $f ),*);
    };
    (
        default: $default:ident $name:ident $enum:ident $mixed:tt $( $choice:ident($type:path): $mode:ident $mod:tt $t:tt $f:path ),*
    ) => {
        choice!(internal $name $enum $mixed $( $choice($type): $mode $mod $t $f ),*);
        impl std::default::Default for $enum {
            fn default() -> $enum {
                $enum::$default(std::default::Default::default())
            }
        }
    };
    (
        internal $name:ident $enum:ident $mixed:tt $( $choice:ident($type:path): $mode:ident $mod:tt $t:tt $f:path ),*
    ) => {
        #[derive(Debug)]
        pub enum $enum {
            $(
                $choice(member_decl!($mode $mod $mixed $type))
            ),*
        }
        pub fn $name<'a>(env: &mut super::super::P, e: $crate::Element) -> $crate::parse::EResult<'a, $enum> {
            $( choice_member_parse!(env e $mode $mixed $mod $t $enum $choice $f); )*
            #[allow(unreachable_code)]
            Ok((Some(e), None))
        }
        impl $crate::AsStream<super::super::QNames>  for $enum {
            #[allow(unreachable_patterns)]
            fn serialize_to_stream(&self, out: &mut dyn $crate::Stream, qnames: &super::super::QNames) {
                match self {
                    $(
                        Self::$choice(c) => {
                            serialize_member!((c) $mode $mixed $mod $t out qnames);
                        }
                    ),*
                    _ => { }
                }
            }
        }
    };
}

#[macro_export]
macro_rules! choice_member_parse {
    // parse a non-element choice option
    ($env:ident $e:ident r $mixed:tt $mod:tt - $enum:ident $choice:ident $f:path) => {
        let (e, v) = $f($env, $e.clone())?;
        if let Some(v) = v {
            return Ok((e, Some(<$enum>::$choice(v))));
        }
    };
    // parse a choice option or use a default value for it
    ($env:ident $e:ident d $mixed:tt $mod:tt $t:tt $enum:ident $choice:ident $f:path) => {
        let (e, v) = $f($env, $e.clone())?;
        return Ok((
            e,
            if let Some(v) = v {
                Some(<$enum>::$choice(v))
            } else {
                Default::default()
            },
        ));
    };
    // parse a choice option that is an element
    ($env:ident $e:ident r m $mod:tt $t:tt $enum:ident $choice:ident $f:path) => {
        if $crate::parse::check_name($env.names.qnames.get($env.names.$mod.$t), &$e) {
            let (e2, v) = $f($env, $e.clone())?;
            let s = $crate::parse::mixed_text($e);
            return Ok((e2, Some(<$enum>::$choice((v, s)))));
        }
    };
    ($env:ident $e:ident r $mixed:tt $mod:tt $t:tt $enum:ident $choice:ident $f:path) => {
        if $crate::parse::check_name($env.names.qnames.get($env.names.$mod.$t), &$e) {
            let (e, v) = $f($env, $e)?;
            return Ok((e, Some(<$enum>::$choice(v))));
        }
    };
    // parse a choice option that is an optional element
    ($env:ident $e:ident o $mixed:tt $mod:tt $t:tt $enum:ident $choice:ident $f:path) => {
        if $crate::parse::check_name($env.names.qnames.get($env.names.$mod.$t), &$e) {
            let (e, v) = $f($env, $e)?;
            return Ok((e, Some(<$enum>::$choice(Some(v)))));
        }
    };
    // parse a choice option that contains a vector
    // it should have at least one item
    ($env:ident $e:ident v $mixed:tt $mod:tt $t:tt $enum:ident $choice:ident $f:path) => {
        let mut acc = Vec::new();
        let mut me = Some($e.clone());
        while let Some(e) = me {
            if $crate::parse::check_name($env.names.qnames.get($env.names.$mod.$t), &$e) {
                let (e, value) = $f($env, e)?;
                acc.push(value);
                me = e;
            } else {
                me = Some(e);
                break;
            }
        }
        if !acc.is_empty() {
            return Ok((me, Some(<$enum>::$choice(acc))));
        }
    };
}

#[macro_export]
macro_rules! seq_member_parse {
    ($env:ident $e:ident $at_start:ident o $mixed:tt $c:tt) => {
        $crate::parse::opt($c)($env, $e, &mut $at_start)?
    };
    ($env:ident $e:ident $at_start:ident r m $c:tt) => {
        {
            let e_clone = $e.clone();
            let (e2, v) = seq_member_parse!($env $e $at_start r - $c);
            let s = e_clone.map(|e| $crate::parse::mixed_text(e)).unwrap_or_default();
            (e2, (v, s))
        }
    };
    ($env:ident $e:ident $at_start:ident r $mixed:tt $c:tt) => {
        if let Some(e) = $e {
            let (e, v) = $c($env, e)?;
            if let Some(v) = v {
                $at_start = false;
                (e, v)
            } else if $at_start {
                return Ok((e, None));
            } else {
                return Err($crate::error::XsdError::MissingElement(
                    eyre::eyre!(stringify!($c)),
                ));
            }
        } else if $at_start {
            return Ok((None, None));
        } else {
            return Err($crate::error::XsdError::MissingElement(
                eyre::eyre!(stringify!($c)),
            ));
        }
    };
    ($env:ident $e:ident $at_start:ident d $mixed:tt $c:tt) => {
        if let Some(e) = $e {
            let (e, v) = $c($env, e)?;
            if let Some(v) = v {
                $at_start = false;
                (e, v)
            } else if $at_start {
                return Ok((e, None));
            } else {
                return Err($crate::error::XsdError::MissingElement(
                    eyre::eyre!("a"),
                ));
            }
        } else {
            (None, Default::default())
        }
    };
    ($env:ident $e:ident $at_start:ident v m $c:tt) => {{
        let (e, m) = $crate::parse::many0_mixed($c)($env, $e)?;
        if !m.is_empty() {
            $at_start = false;
        }
        (e, m)
    }};
    ($env:ident $e:ident $at_start:ident v $mixed:tt $c:tt) => {{
        let (e, m) = $crate::parse::many0($c)($env, $e)?;
        if !m.is_empty() {
            $at_start = false;
        }
        (e, m)
    }};
    ($env:ident $e:ident $at_start:ident - $mixed:tt ()) => {
        ($e, xust_tree::tree::Element)
    };
}

#[macro_export]
macro_rules! sequence {
    ( default $name:ident $struct:ident $mixed:tt $( $member:ident($type:path): $mode:tt $mod:tt $t:tt $c:path ),*) => {
        sequence!(internal derive(Debug, Default) $name $struct $mixed $( $member($type): $mode $mod $t $c ),*);
    };
    ( $name:ident $struct:ident $mixed:tt $( $member:ident($type:path): $mode:tt $mod:tt $t:tt $c:path ),*) => {
        sequence!(internal derive(Debug) $name $struct $mixed $( $member($type): $mode $mod $t $c ),*);
    };
    ( internal $derive:meta $name:ident $struct:ident $mixed:tt $( $member:ident($type:path): $mode:tt $mod:tt $t:tt $c:path ),*) => {
        use $crate::parse::*;
        #[$derive]
        pub struct $struct {
            $( pub $member: member_decl!($mode $mod $mixed $type), )*
        }
        #[allow(unused_variables, unused_assignments)]
        pub fn $name<'a>(env: &mut super::super::P, element: $crate::Element) -> $crate::parse::EResult<'a, $struct> {
            #[allow(unused_mut)]
            let mut at_start = true;
            let e = Some(element);
            $(
                let (e, $member) = $crate::seq_member_parse!(env e at_start $mode $mixed ($crate::constructor!($mod $t $c)));
            )*
            Ok((e, Some($struct {
                $( $member, )*
            })))
        }
        impl $crate::AsStream<super::super::QNames>  for $struct {
            fn serialize_to_stream(&self, out: &mut dyn $crate::Stream, qnames: &super::super::QNames) {
                $(
                    serialize_member!( (self.$member) $mode $mixed $mod $t out qnames);
                )*
            }
        }
    };
}
