pub(crate) mod identity_constraints;
mod model_groups;
pub(crate) mod types;
mod wildcards;

use crate::{
    atomic::par_ser::*,
    facets::Facets,
    identity_constraint_grammar::{Field, Selector},
    parse::MaxOccurs,
    types::TypeId,
};
use bitflags::bitflags;
use identity_constraints::{IdentityConstraintId, IdentityConstraints};
pub(crate) use model_groups::{ModelGroupId, ModelGroups};
use std::collections::BTreeMap;
use std::sync::Arc;
pub(crate) use types::Types;
pub(crate) use wildcards::{WildcardId, Wildcards};
use xust_tree::qnames::{QNameCollection, QNameId};

// 2.2.1 Type Definition Components
#[derive(Debug, PartialEq, Clone)]
pub(crate) enum TypeDef {
    SimpleType(SimpleType),
    ComplexType(ComplexType),
}

#[derive(Debug, PartialEq, Clone)]
pub(crate) struct Type {
    pub name: Option<QNameId>,
    pub base: TypeId,
    pub def: TypeDef,
}

impl Type {
    pub(crate) fn get_name(&self) -> Option<QNameId> {
        self.name
    }
    pub(crate) fn has_name(&self, name: QNameId) -> bool {
        self.get_name() == Some(name)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum Term {
    TopLevelElement(usize),
    LocalElement(ElementDeclaration),
    Group(ModelGroupId),
    Any(WildcardId),
}

// 3.3.1 The Element Declaration Schema Component
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) struct ElementDeclaration {
    pub tag: QNameId,
    pub content: TypeId,
    pub identity_constraints: Vec<IdentityConstraintId>,
    pub r#default: Option<String>,
    pub fixed: Option<String>,
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct AttributeDeclaration {
    pub content: TypeId,
    pub r#default: Option<String>,
    pub fixed: Option<String>,
}

// 3.4.1 The Complex Type Definition Schema Component
#[derive(Debug, Clone, PartialEq)]
pub(crate) struct ComplexType {
    pub r#final: ComplexTypeFinal,
    pub derivation_method: DerivationMethod,
    pub r#abstract: bool,
    pub attribute_uses: Vec<Attribute>,
    pub attribute_wildcard: Option<WildcardId>,
    pub content_type: ContentType,
}

#[derive(Debug, PartialEq)]
pub(crate) enum Compositor {
    Choice,
    Sequence,
    All,
}

/// 3.8.1 The Model Group Schema Component
#[derive(Debug, PartialEq)]
pub(crate) struct ModelGroup {
    pub compositor: Compositor,
    pub name: Option<QNameId>,
    pub particles: Vec<Particle>,
    pub mixed: bool,
}

impl ModelGroup {
    pub(crate) fn get_choice_default(&self, model: &SchemaComponents) -> Option<usize> {
        if self.compositor == Compositor::Choice {
            self.particles
                .iter()
                .position(|m| m.min_occurs == 0 || m.has_default(model))
        } else {
            None
        }
    }
    pub(crate) fn has_default(&self, model: &SchemaComponents) -> bool {
        match self.compositor {
            Compositor::Choice => self
                .particles
                .iter()
                .any(|m| m.min_occurs == 0 || m.has_default(model)),
            Compositor::Sequence => self
                .particles
                .iter()
                .all(|m| m.min_occurs == 0 || m.has_default(model)),
            Compositor::All => false,
        }
    }
}

impl Default for ModelGroup {
    fn default() -> Self {
        Self {
            compositor: Compositor::Choice,
            name: None,
            particles: Vec::new(),
            mixed: false,
        }
    }
}

// 3.9.1 The Particle Schema Component
#[derive(Debug, Clone, PartialEq)]
pub(crate) struct Particle {
    pub term: Term,
    pub min_occurs: usize,
    pub max_occurs: MaxOccurs,
}

impl Particle {
    pub(crate) fn has_default(&self, model: &SchemaComponents) -> bool {
        if self.min_occurs == 0 {
            return true;
        }
        match self.term {
            Term::TopLevelElement(_) => false,
            Term::LocalElement { .. } => false,
            Term::Group(groupid) => model.groups[groupid].has_default(model),
            Term::Any(_) => false,
        }
    }
}

// 3.10.1 The Wildcard Schema Component
#[derive(Debug, Clone, PartialEq)]
pub(crate) struct Wildcard {
    pub process_contents: ProcessContents,
    pub namespace_constraints: NamespaceConstraints,
}

impl Wildcard {
    pub fn any_type() -> Self {
        Self {
            // the ur-type has lax contents processing
            process_contents: ProcessContents::Lax,
            // no namespace constraints (any)
            namespace_constraints: NamespaceConstraints::any_type(),
        }
    }
    pub fn allowed_namespace(&self, namespace: &str) -> bool {
        let nc = &self.namespace_constraints;
        match nc.variety {
            NamespaceConstraintsVariety::Any => true,
            NamespaceConstraintsVariety::Enumeration => {
                nc.namespaces.iter().any(|ns| ns == namespace)
            }
            NamespaceConstraintsVariety::Not => !nc.namespaces.iter().any(|ns| ns == namespace),
        }
    }
    // 3.6.2.2 Common Rules for Attribute Wildcards
    pub fn intersect(mut self, other: Wildcard) -> Wildcard {
        self.namespace_constraints = self
            .namespace_constraints
            .intersect(other.namespace_constraints);
        self
    }
}

// 3.11.1 The Identity-constraint Definition Schema Component
#[derive(Debug, Clone, PartialEq)]
pub(crate) struct IdentityConstraint {
    pub name: QNameId,
    pub category: IdentityConstraintCategory,
    pub selector: Selector,
    pub selector_xpath: String,
    pub fields: Vec<Field>,
    // during loading fields_xpath may have value [String::new()] to indicate
    // that it was referenced by a keyref and the category can be either key or
    // unique
    pub fields_xpath: Vec<String>,
}

// An identity constraint is either <key/>, <keyref/> or <unique/>.
#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) enum IdentityConstraintCategory {
    Key,
    Keyref(IdentityConstraintId),
    Unique,
}

impl IdentityConstraint {
    pub(crate) fn is_keyref(&self) -> bool {
        std::mem::discriminant(&self.category)
            == std::mem::discriminant(&IdentityConstraintCategory::Keyref(
                IdentityConstraintId::default(),
            ))
    }
}

// 3.16.1 The Simple Type Definition Schema Component
#[derive(Debug, Clone, PartialEq)]
pub(crate) struct SimpleType {
    pub r#final: SimpleTypeFinal,
    pub variety: SimpleTypeVariety,
    pub facets: Facets,
}

impl Default for SimpleType {
    fn default() -> Self {
        Self {
            r#final: SimpleTypeFinal::default(),
            variety: SimpleTypeVariety::Atomic(STRING_PAR_SER),
            facets: Facets::default(),
        }
    }
}

// 3.17.1 The Schema Itself
#[derive(Debug)]
pub(crate) struct SchemaComponents {
    pub qnames: QNameCollection,
    pub elements: Vec<ElementDeclaration>,
    pub attributes: Arc<BTreeMap<QNameId, AttributeDeclaration>>,
    pub types: Types,
    pub groups: ModelGroups,
    pub wildcards: Arc<Wildcards>,
    pub identity_constraints: Arc<IdentityConstraints>,
    pub simple_types: Arc<[SimpleType]>,
}

impl SchemaComponents {
    pub fn is_empty(&self) -> bool {
        self.elements.is_empty()
    }
    pub fn elements(&self) -> impl Iterator<Item = &ElementDeclaration> {
        self.elements.iter()
    }
    pub fn get_group(&self, group: ModelGroupId) -> &ModelGroup {
        &self.groups[group]
    }
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct Attribute {
    pub name: QNameId,
    pub ad: AttributeDeclaration,
    pub required: bool,
}

bitflags! {
    #[derive(Default)]
    pub(crate) struct SimpleTypeFinal: u8 {
        const EXTENSION = 1;
        const LIST = 2;
        const RESTRICTION = 4;
        const UNION = 8;
    }
    #[derive(Default)]
    pub(crate) struct ComplexTypeFinal: u8 {
        const EXTENSION = 1;
        const RESTRICTION = 2;
    }
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct List {
    pub item_type: TypeId,
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum SimpleTypeVariety {
    Atomic(PrimitiveParSer),
    List(List),
    Union(Vec<TypeId>),
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum ContentType {
    Empty,
    SimpleType(TypeId),
    ComplexType { mixed: bool, particle: Particle },
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum DerivationMethod {
    Extension,
    Restriction,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) enum ProcessContents {
    // only elements with a definition are allowed
    Strict,
    // No constraints at all: the item must simply be well-formed XML.
    Skip,
    // If the element has a definition, validate it
    Lax,
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct NamespaceConstraints {
    pub variety: NamespaceConstraintsVariety,
    pub namespaces: Vec<String>,
    pub disallowed: Vec<QNameId>,
}

impl NamespaceConstraints {
    fn any_type() -> Self {
        Self {
            variety: NamespaceConstraintsVariety::Any,
            namespaces: Vec::new(),
            disallowed: Vec::new(),
        }
    }
    // 3.10.6.4 Attribute Wildcard Intersection
    pub fn intersect(mut self, mut other: NamespaceConstraints) -> NamespaceConstraints {
        match (self.variety, other.variety) {
            (NamespaceConstraintsVariety::Any, _) => other,
            (_, NamespaceConstraintsVariety::Any) => self,
            (
                NamespaceConstraintsVariety::Enumeration,
                NamespaceConstraintsVariety::Enumeration,
            ) => {
                self.namespaces.retain(|n| other.namespaces.contains(n));
                NamespaceConstraints {
                    variety: NamespaceConstraintsVariety::Enumeration,
                    namespaces: self.namespaces,
                    disallowed: Vec::new(),
                }
            }
            (NamespaceConstraintsVariety::Not, NamespaceConstraintsVariety::Not) => {
                for ns in other.namespaces {
                    if !self.namespaces.contains(&ns) {
                        self.namespaces.push(ns);
                    }
                }
                NamespaceConstraints {
                    variety: NamespaceConstraintsVariety::Not,
                    namespaces: self.namespaces,
                    disallowed: Vec::new(),
                }
            }
            (NamespaceConstraintsVariety::Enumeration, NamespaceConstraintsVariety::Not) => {
                self.namespaces.retain(|n| !other.namespaces.contains(n));
                NamespaceConstraints {
                    variety: NamespaceConstraintsVariety::Enumeration,
                    namespaces: self.namespaces,
                    disallowed: Vec::new(),
                }
            }
            (NamespaceConstraintsVariety::Not, NamespaceConstraintsVariety::Enumeration) => {
                other.namespaces.retain(|n| !self.namespaces.contains(n));
                NamespaceConstraints {
                    variety: NamespaceConstraintsVariety::Enumeration,
                    namespaces: other.namespaces,
                    disallowed: Vec::new(),
                }
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) enum NamespaceConstraintsVariety {
    Any,
    Enumeration,
    Not,
}

pub const XS_NS: &str = "http://www.w3.org/2001/XMLSchema";
