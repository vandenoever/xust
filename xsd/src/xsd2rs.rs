mod local_name_collector;
pub mod traits;

use self::local_name_collector::LocalName;
use crate::{
    parse::MaxOccurs,
    schema_components::{
        Attribute, ComplexType, Compositor, ContentType, ElementDeclaration, ModelGroupId,
        Particle, SchemaComponents, SimpleType, SimpleTypeVariety, Term, Type, TypeDef, Types,
    },
    types::{built_ins::*, TypeId},
    xsd2rs::local_name_collector::LocalNameCollector,
    xsd2schema_components::{
        get_particles, is_only_used_directly_in_element,
        load::{get_xsd_validator, xsd2schema_components, PrefixesAndSchemaComponents},
    },
};
use heck::{ToSnakeCase, ToUpperCamelCase};
use std::cmp::Ordering;
use std::collections::{BTreeMap, BTreeSet};
use std::fmt::{self, Write};
use std::path::PathBuf;
use xust_tree::qnames::{self, QNameCollection, QNameId};

type FError = Result<(), fmt::Error>;

#[derive(Debug, PartialEq, Clone, Copy)]
enum StructType {
    G,
    Ct,
    E,
    Local,
}

impl StructType {
    fn prefix(&self) -> &'static str {
        match self {
            Self::G => "g",
            Self::Ct => "ct",
            Self::E => "e",
            Self::Local => "local",
        }
    }
}

struct Context<'a> {
    prefix: &'a str,
    r#mod: StructType,
    model: &'a SchemaComponents,
    f: &'a mut dyn Write,
    local_groups: &'a BTreeMap<ModelGroupId, LocalName>,
    local_types: &'a BTreeMap<TypeId, LocalName>,
}

impl Context<'_> {
    fn get_group_name(&self, groupid: ModelGroupId) -> (StructType, String) {
        let group = &self.model.groups[groupid];
        if let Some(name) = group.name {
            (
                StructType::G,
                self.model.qnames.to_ref(name).local_name().to_string(),
            )
        } else {
            (StructType::Local, self.local_groups[&groupid].name.clone())
        }
    }
}

fn esc(s: String) -> String {
    match s.as_str() {
        "abstract" => "r#abstract",
        "as" => "r#as",
        "default" => "r#default",
        "final" => "r#final",
        "in" => "r#in",
        "override" => "r#override",
        "ref" => "r#ref",
        "type" => "r#type",
        "use" => "r#use",
        s => return s.replace('.', "_"),
    }
    .to_string()
}

fn get_type(c: &mut Context, term: &Term) -> Option<(StructType, bool, String, String)> {
    let mut is_group = false;
    let r#type = match term {
        Term::TopLevelElement(e) => {
            let e = &c.model.elements[*e];
            if e.content == TypeId::any_type() {
                None
            } else if let Some(name) = c.local_types.get(&e.content) {
                return Some((
                    StructType::Local,
                    false,
                    name.prefix.to_string(),
                    name.name.clone(),
                ));
            } else if let Some(name) = c.model.types[e.content].get_name() {
                Some((StructType::Ct, name))
            } else {
                Some((StructType::E, e.tag))
            }
        }
        Term::LocalElement(ElementDeclaration { tag, content, .. }) => {
            if *content == TypeId::any_type() {
                None
            } else if let Some(name) = c.local_types.get(content) {
                return Some((
                    StructType::Local,
                    false,
                    name.prefix.to_string(),
                    name.name.clone(),
                ));
            } else if let Some(name) = c.model.types[*content].get_name() {
                Some((StructType::Ct, name))
            } else {
                Some((StructType::E, *tag))
            }
        }
        Term::Group(groupid) => {
            let group = &c.model.groups[*groupid];
            if let Some(r#type) = group.name {
                is_group = true;
                Some((StructType::G, r#type))
            } else {
                let r = c.get_group_name(*groupid);
                return Some((r.0, true, c.prefix.to_string(), r.1));
            }
        }
        Term::Any(_) => {
            return None;
        }
    };
    if let Some((n, r#type)) = r#type {
        let r#type = c.model.qnames.get(r#type).to_ref();
        let prefix = r#type.prefix().to_string();
        let local_name = r#type.local_name().to_string();
        Some((n, is_group, prefix, local_name))
    } else {
        None
    }
}

fn member2rs(c: &mut Context, m: &Particle, name: &str, choice: bool) -> FError {
    let mode = get_mode(c.model, m);
    let name = if choice {
        name.to_upper_camel_case()
    } else {
        name.to_snake_case()
    };
    let (type_mod, is_group, type_prefix, type_local_name) = if let Some(r) = get_type(c, &m.term) {
        r
    } else {
        let r#type = "Unit".to_string();
        write!(c.f, "{}({}): {} - - unit", name, r#type, mode)?;
        return Ok(());
    };
    let constructor = mod_name(
        &type_prefix,
        c.prefix,
        c.r#mod,
        type_mod,
        &esc(type_local_name.to_snake_case()),
    );
    let r#type = mod_name(
        &type_prefix,
        c.prefix,
        c.r#mod,
        type_mod,
        &type_local_name.to_upper_camel_case(),
    );
    let mut t = (if is_group { "+" } else { "-" }, None);
    if type_mod != StructType::E {
        if let Term::TopLevelElement(e) = m.term {
            let e = &c.model.elements[e];
            t.0 = c.prefix;
            t.1 = Some(esc(c
                .model
                .qnames
                .to_ref(e.tag)
                .local_name()
                .as_str()
                .to_snake_case()));
        }
        if let Term::LocalElement(ElementDeclaration { tag, .. }) = m.term {
            t.0 = c.prefix;
            t.1 = Some(esc(c
                .model
                .qnames
                .to_ref(tag)
                .local_name()
                .as_str()
                .to_snake_case()));
        }
    }
    write!(
        c.f,
        "{}({}): {} {} {} {}",
        esc(name),
        r#type,
        mode,
        t.0,
        t.1.as_deref().unwrap_or("-"),
        constructor
    )
}

fn mod_name(
    prefix: &str,
    parent_prefix: &str,
    r#mod: StructType,
    st: StructType,
    name: &str,
) -> String {
    if prefix == parent_prefix || prefix.is_empty() {
        if r#mod == st {
            name.to_string()
        } else {
            format!("super::{}::{}", st.prefix(), name)
        }
    } else if prefix == "xs" {
        // TODO: clean check
        format!("{}::{}", st.prefix(), name)
    } else {
        let prefix = if prefix.is_empty() { "default" } else { prefix };
        format!("super::super::{}::{}::{}", prefix, st.prefix(), name)
    }
}

fn get_mode(model: &SchemaComponents, sm: &Particle) -> char {
    let group_with_default_value = sm.has_default(model);
    if sm.max_occurs != MaxOccurs::Bounded(1) {
        'v'
    } else if sm.min_occurs == 0 {
        'o'
    } else if group_with_default_value {
        'd'
    } else {
        'r'
    }
}

fn attribute2rs(
    a: qnames::Ref,
    r#type: Option<TypeId>,
    is_list: bool,
    att: &Attribute,
    member: &str,
    f: &mut dyn Write,
) -> FError {
    let def;
    let mut r#default = att.ad.r#default.as_deref();
    if let Some(d) = r#default {
        def = format!("\"{}\"", d);
        r#default = Some(&def);
    }
    let mut mode = if att.required { "r" } else { "o" };
    if is_list {
        if att.required {
            mode = "v";
        } else {
            mode = "ov";
        }
    }
    let atype = match a.local_name().as_str() {
        "maxOccurs" => "MaxOccurs",
        _ => match r#type {
            Some(XS_QNAME) => "QName",
            Some(XS_BOOLEAN) => "bool",
            Some(XS_DOUBLE) => "f64",
            Some(XS_FLOAT) => "f32",
            Some(XS_INTEGER) => "i64",
            Some(XS_BYTE) => "i8",
            Some(XS_SHORT) => "i16",
            Some(XS_INT) => "i32",
            Some(XS_LONG) => "i64",
            Some(XS_UNSIGNED_BYTE) => "u8",
            Some(XS_UNSIGNED_SHORT) => "u16",
            Some(XS_UNSIGNED_INT) => "u32",
            Some(XS_UNSIGNED_LONG) => "u64",
            Some(XS_NON_NEGATIVE_INTEGER) => "usize",
            Some(XS_POSITIVE_INTEGER) => "usize",
            _ => "String",
        },
    };
    let prefix = if a.namespace().is_empty() {
        "local".to_string()
    } else {
        esc(a.prefix().to_string())
    };
    write!(
        f,
        "{} {} {} {} {} {}",
        prefix,
        esc(a.local_name().as_str().to_snake_case()),
        member,
        mode,
        r#default.unwrap_or("-"),
        atype
    )
}

fn attributes2rs(
    model: &SchemaComponents,
    atts: &[Attribute],
    f: &mut dyn Write,
) -> Result<bool, fmt::Error> {
    if atts.is_empty() {
        return Ok(false);
    }
    let mut atts = Vec::from(atts);
    let q = &model.qnames;
    atts.sort_by(|a, b| {
        let a = q.to_ref(a.name);
        let b = q.to_ref(b.name);
        let ord = a.namespace().0.cmp(b.namespace().0);
        if ord == Ordering::Equal {
            a.local_name().0.cmp(b.local_name().0)
        } else {
            ord
        }
    });
    writeln!(f)?;
    writeln!(f, "        attributes {{")?;
    let mut particles = BTreeSet::new();
    let mut first = true;
    for a in &atts {
        if !first {
            writeln!(f, ",")?;
        }
        write!(f, "            ")?;
        first = false;
        let qname = q.to_ref(a.name);
        let local_name = qname.local_name().as_str();
        let mut member = esc(local_name.to_snake_case());
        if particles.contains(&member) {
            member = format!("{}_{}", qname.prefix(), local_name);
        }
        let mut is_list = false;
        let r#type;
        if let Some(list_type) = get_list_item_type(&model.types[a.ad.content]) {
            is_list = true;
            r#type = Some(list_type);
        } else {
            r#type = get_primitive_type_and_derived_atomic_parent(&model.types, a.ad.content);
        }
        attribute2rs(qname, r#type, is_list, a, &member, f)?;
        particles.insert(member);
    }
    writeln!(f)?;
    write!(f, "        }}")?;
    Ok(true)
}

fn get_list_item_type(t: &Type) -> Option<TypeId> {
    if let TypeDef::SimpleType(st) = &t.def {
        if let SimpleTypeVariety::List(l) = &st.variety {
            return Some(l.item_type);
        }
    }
    None
}

fn is_union(t: &Type) -> bool {
    if let TypeDef::SimpleType(st) = &t.def {
        return matches!(st.variety, SimpleTypeVariety::Union(_));
    }
    false
}

fn get_primitive_type_and_derived_atomic_parent(types: &Types, mut t: TypeId) -> Option<TypeId> {
    while t != TypeId::any_type() {
        if t.0 >= 3 && t.0 <= 45 {
            return Some(t);
        }
        if is_union(&types[t]) {
            return None;
        }
        t = types[t].base;
    }
    None
}

fn make_member_names(c: &Context, particles: &[Particle]) -> Vec<String> {
    let mut names: Vec<(String, usize, String)> = Vec::new();
    for particle in particles {
        let name = match particle.term {
            Term::TopLevelElement(e) => {
                let e = &c.model.elements[e];
                c.model.qnames.to_ref(e.tag).local_name().to_string()
            }
            Term::LocalElement(ElementDeclaration { tag, .. }) => {
                c.model.qnames.to_ref(tag).local_name().to_string()
            }
            Term::Group(gid) => {
                let g = &c.model.groups[gid];
                if let Some(name) = g.name {
                    c.model.qnames.to_ref(name).local_name().to_string()
                } else {
                    match g.compositor {
                        Compositor::Choice => "choice",
                        Compositor::Sequence => "sequence",
                        Compositor::All => "all",
                    }
                    .to_string()
                }
            }
            Term::Any(_) => "any".to_string(),
        };
        let mut c = 1;
        if let Some(n) = names.iter_mut().find(|(_, _, original)| *original == name) {
            n.0 = format!("{}{}", n.2, 1);
            n.1 += 1;
            c = n.1;
        }
        let member_name = if c == 1 {
            name.to_string()
        } else {
            format!("{}{}", name, c)
        };
        names.push((member_name, c, name.to_string()));
    }
    names.into_iter().map(|(n, _, _)| n).collect()
}

fn choice2rs(c: &mut Context, groupid: ModelGroupId) -> FError {
    let g = &c.model.groups[groupid];
    let member_names = make_member_names(c, &g.particles);
    let r#default = if let Some(position) = g.get_choice_default(c.model) {
        format!("default: {} ", member_names[position].to_upper_camel_case())
    } else {
        String::new()
    };
    let (_, name) = c.get_group_name(groupid);
    write!(
        c.f,
        "    choice!({}{} {} {}",
        r#default,
        name.to_snake_case(),
        name.to_upper_camel_case(),
        if g.mixed { "m" } else { "-" }
    )?;
    members2rs(c, &g.particles, &member_names, true)
}

fn sequence2rs(c: &mut Context, groupid: ModelGroupId) -> FError {
    let s = &c.model.groups[groupid];
    if is_only_used_directly_in_element(&c.model.types, groupid, s) {
        return Ok(());
    }
    let member_names = make_member_names(c, &s.particles);
    let (_, name) = c.get_group_name(groupid);
    write!(c.f, "    sequence!(")?;
    if s.has_default(c.model) {
        write!(c.f, "default ")?;
    }
    write!(
        c.f,
        "{} {} {}",
        esc(name.to_snake_case()),
        name.to_upper_camel_case(),
        if s.mixed { "m" } else { "-" }
    )?;
    members2rs(c, &s.particles, &member_names, false)
}

fn members2rs(
    c: &mut Context,
    particles: &[Particle],
    member_names: &[String],
    choice: bool,
) -> FError {
    if particles.is_empty() {
        return writeln!(c.f, ");");
    }
    writeln!(c.f)?;
    write!(c.f, "        ")?;
    let mut first = true;
    for (pos, m) in particles.iter().enumerate() {
        if !first {
            writeln!(c.f, ",")?;
            write!(c.f, "        ")?;
        }
        first = false;
        member2rs(c, m, &member_names[pos], choice)?;
    }
    writeln!(c.f)?;
    writeln!(c.f, "    );")
}

fn simple_type2rs(c: &mut Context, qname: Option<QNameId>, _simple_type: &SimpleType) -> FError {
    if let Some(name) = qname {
        let name = c.model.qnames.to_ref(name);
        if name.prefix().0 == c.prefix {
            let name = name.local_name().as_str();
            if name == "ScriptType" {
                // workaround for SVG.xsd
                return Ok(());
            }
            if !is_built_in(qname, &c.model.qnames) {
                writeln!(
                    c.f,
                    "simpleType!({} {});",
                    esc(name.to_snake_case()),
                    esc(name.to_upper_camel_case())
                )?;
            }
        }
    }
    Ok(())
}

fn complex_type2rs(
    c: &mut Context,
    id: TypeId,
    qname: Option<QNameId>,
    ct: &ComplexType,
) -> FError {
    let qname = qname.map(|n| c.model.qnames.to_ref(n));
    let name = if let Some(name) = &qname {
        name.local_name().as_str()
    } else {
        c.local_types.get(&id).unwrap().name.as_str()
    };
    if name == "anyType" {
        return Ok(());
    }
    write!(
        c.f,
        "    complexType!({} {} {} {}",
        esc(name.to_snake_case()),
        name.to_upper_camel_case(),
        id.0,
        if is_mixed(ct) { "m" } else { "-" }
    )?;
    let wrote_attributes = attributes2rs(c.model, &ct.attribute_uses, c.f)?;
    let particles = get_particles(&c.model.groups, ct);
    if wrote_attributes && !particles.is_empty() {
        writeln!(c.f)?;
    }
    let member_names = make_member_names(c, &particles);
    members2rs(c, &particles, &member_names, false)
}

fn is_mixed(ct: &ComplexType) -> bool {
    if let ContentType::ComplexType { mixed, .. } = &ct.content_type {
        *mixed
    } else {
        false
    }
}

fn element2rs(c: &mut Context, name: QNameId, ct: &ComplexType, id: TypeId) -> FError {
    let particles = get_particles(&c.model.groups, ct);
    let name = c.model.qnames.to_ref(name);
    let local_name = name.local_name().as_str();
    write!(
        c.f,
        "    element!({} {} {} {} {}",
        c.prefix,
        esc(local_name.to_snake_case()),
        esc(local_name.to_upper_camel_case()),
        id,
        if is_mixed(ct) { "m" } else { "-" }
    )?;
    let wrote_attributes = attributes2rs(c.model, &ct.attribute_uses, c.f)?;
    if wrote_attributes && particles.is_empty() {
        writeln!(c.f)?;
    }
    let member_names = make_member_names(c, &particles);
    members2rs(c, &particles, &member_names, false)
}

fn element_type2rs(c: &mut Context, tag: QNameId, r#type: QNameId, id: TypeId) -> FError {
    write!(
        c.f,
        "    element_type!({} {}",
        c.prefix,
        esc(c
            .model
            .qnames
            .to_ref(tag)
            .local_name()
            .as_str()
            .to_snake_case())
    )?;
    if id == TypeId::any_type() {
        writeln!(c.f, "(Option<Unit>) unit);")
    } else {
        let r#type = c.model.qnames.to_ref(r#type);
        let r#type = r#type.local_name().as_str();
        writeln!(
            c.f,
            "(super::ct::{}) super::ct::{});",
            esc(r#type.to_upper_camel_case()),
            esc(r#type.to_snake_case())
        )
    }
}

fn g2rs(c: &mut Context, groupid: ModelGroupId) -> FError {
    let group = &c.model.groups[groupid];
    match group.compositor {
        Compositor::Choice => choice2rs(c, groupid),
        Compositor::Sequence => sequence2rs(c, groupid),
        Compositor::All => {
            let (_, n) = c.get_group_name(groupid);
            writeln!(
                c.f,
                "sequence!({} {} {});",
                esc(n.to_snake_case()),
                n.to_upper_camel_case(),
                if group.mixed { "m" } else { "-" }
            )
        }
    }
}

fn schema_components2string(
    c: &mut Context,
    crate_mod: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    writeln!(
        c.f,
        "pub mod local {{
    {}::use_xsd!();",
        crate_mod
    )?;
    c.r#mod = StructType::Local;
    for (id, g) in c.model.groups.iter() {
        if g.name.is_none()
            && c.local_groups
                .get(&id)
                .map(|g| g.prefix == c.prefix)
                .unwrap_or(false)
        {
            g2rs(c, id)?;
        }
    }
    for (id, t) in c.model.types.iter() {
        if c.local_types.contains_key(&id) && c.local_types.get(&id).unwrap().prefix == c.prefix {
            if let TypeDef::ComplexType(ct) = &t.def {
                complex_type2rs(c, id, t.name, ct)?;
            } else {
                return Err("Creating simple types as rust is not yet implemented.".into());
            }
        }
    }
    writeln!(
        c.f,
        "
    }}
    pub mod g {{
    {}::use_xsd!();",
        crate_mod
    )?;
    c.r#mod = StructType::G;
    for (id, g) in c.model.groups.iter() {
        if let Some(name) = &g.name {
            if c.model.qnames.to_ref(*name).prefix().0 == c.prefix {
                g2rs(c, id)?;
            }
        }
    }
    writeln!(
        c.f,
        "
    }}
    pub mod ct {{
        {}::use_xsd!();",
        crate_mod
    )?;
    c.r#mod = StructType::Ct;
    for (id, ec) in c.model.types.iter() {
        match &ec.def {
            TypeDef::ComplexType(t) => {
                if let Some(name) = ec.name {
                    if c.model.qnames.to_ref(name).prefix().0 == c.prefix {
                        complex_type2rs(c, id, ec.name, t)?;
                    }
                }
            }
            TypeDef::SimpleType(t) => {
                simple_type2rs(c, ec.name, t)?;
            }
        }
    }
    writeln!(
        c.f,
        "
    }}
    pub mod e {{
        {}::use_xsd!();
    ",
        crate_mod
    )?;
    c.r#mod = StructType::E;
    for ed in &c.model.elements {
        if c.model.qnames.to_ref(ed.tag).prefix().0 == c.prefix {
            let t = &c.model.types[ed.content];
            if let TypeDef::ComplexType(content) = &t.def {
                if let Some(r#type) = t.name {
                    element_type2rs(c, ed.tag, r#type, ed.content)?;
                } else {
                    element2rs(c, ed.tag, content, ed.content)?;
                }
            } else {
                return Err("Elements with simple contents are not yet implemented".into());
                // todo!()
            }
        }
    }
    Ok(writeln!(c.f, "}}")?)
}

fn rustfmt(rs: &str) -> Result<String, Box<dyn std::error::Error>> {
    use std::io::Write;
    use std::process::{Command, Stdio};
    let mut child = Command::new("rustfmt")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;
    {
        let stdin = child.stdin.as_mut().expect("failed to get stdin");
        stdin
            .write_all(rs.as_bytes())
            .expect("failed to write to stdin");
    }
    let output = child.wait_with_output().expect("failed to wait on child");
    if output.status.success() {
        Ok(String::from_utf8(output.stdout)?)
    } else {
        Err(String::from_utf8_lossy(&output.stderr).into())
    }
}

fn write_ns_literals(
    code: &mut String,
    prefix: &str,
    namespace: &str,
    qnames: &QNameCollection,
    values: &BTreeSet<QNameId>,
) -> Result<(), Box<dyn std::error::Error>> {
    writeln!(
        code,
        "ns!({}QNames \"{}\" =>",
        prefix.to_upper_camel_case(),
        namespace
    )?;
    let mut values: Vec<_> = values
        .iter()
        .filter(|v| namespace == qnames.to_ref(**v).namespace().as_str())
        .map(|v| qnames.to_ref(*v))
        .filter(|v| v.local_name().as_str() != "NOTATION") // TODO
        .collect();
    values.sort_unstable_by(|a, b| a.local_name().0.cmp(b.local_name().0));
    for value in values {
        writeln!(
            code,
            "\t{} => \"{}\"",
            esc(value.local_name().as_str().to_snake_case()),
            value.local_name()
        )?;
    }
    writeln!(code, ");")?;
    Ok(())
}

fn collect_literals(model: &SchemaComponents) -> BTreeSet<QNameId> {
    let mut qnames = BTreeSet::new();
    qnames.extend(model.elements.iter().map(|ed| ed.tag));
    for (_, t) in model.types.iter() {
        match &t.def {
            TypeDef::ComplexType(t) => {
                for a in &t.attribute_uses {
                    qnames.insert(a.name);
                }
                if let ContentType::ComplexType { particle, .. } = &t.content_type {
                    if let Term::LocalElement(ElementDeclaration { tag, .. }) = &particle.term {
                        qnames.insert(*tag);
                    }
                }
            }
            TypeDef::SimpleType(_) => {
                if let Some(name) = t.name {
                    qnames.insert(name);
                }
            }
        }
    }
    for (_, g) in model.groups.iter() {
        for m in &g.particles {
            if let Term::LocalElement(ElementDeclaration { tag, .. }) = &m.term {
                qnames.insert(*tag);
            }
        }
    }
    for e in &model.elements {
        qnames.insert(e.tag);
    }
    qnames
}

fn schema_components2rs(
    model: &PrefixesAndSchemaComponents,
    internal: bool,
) -> Result<String, Box<dyn std::error::Error>> {
    let prefixes = &model.prefixes;
    let model = &model.components;
    let prefix: BTreeSet<&String> = prefixes.values().collect();
    let crate_mod = if internal { "crate" } else { "xust_xsd" };
    let mut code = String::new();
    writeln!(
        code,
        "#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
{}::use_xsd!();
use xust_tree::qnames::{{NCName, Namespace, Prefix, QNameCollection}};

type P = crate::parse::ParseEnv<QNames>;

pub struct QNames {{
    qnames: QNameCollection,
    local: LocalQNames,",
        crate_mod
    )?;
    for prefix in &prefix {
        writeln!(
            &mut code,
            "\t{}: {}QNames,",
            prefix.to_snake_case(),
            prefix.to_upper_camel_case()
        )?;
    }
    writeln!(
        code,
        "}}
             pub fn qnames(qnames: QNameCollection) -> QNames {{
                 QNames {{
                     local: LocalQNames::new(&qnames),"
    )?;
    for prefix in &prefix {
        writeln!(
            &mut code,
            "\t{}: {}QNames::new(&qnames),",
            prefix.to_snake_case(),
            prefix.to_upper_camel_case()
        )?;
    }
    writeln!(code, "qnames}}}}")?;
    let qnames = collect_literals(model);
    for prefix in &prefix {
        let ns = prefixes.iter().find(|(_, p)| prefix == p).unwrap().0;
        write_ns_literals(&mut code, prefix, ns, &model.qnames, &qnames)?;
    }
    write_ns_literals(&mut code, "local", "", &model.qnames, &qnames)?;

    let mut names = LocalNameCollector::new(model);
    for (_id, g) in model.groups.iter() {
        if let Some(name) = &g.name {
            let name = LocalName::new(model.qnames.to_ref(*name));
            names.invent_names(&name, &g.particles);
        }
    }
    for (_id, t) in model.types.iter() {
        if let Some(name) = &t.name {
            let name = LocalName::new(model.qnames.to_ref(*name));
            names.invent_names_for_type(&name, t);
        }
    }
    for e in model.elements() {
        let name = LocalName::new(model.qnames.to_ref(e.tag));
        let t = &model.types[e.content];
        names.invent_names_for_type(&name, t);
    }

    for prefix in &prefix {
        write!(&mut code, "pub mod {} {{", prefix)?;
        let mut c = Context {
            prefix,
            r#mod: StructType::Local,
            model,
            f: &mut code,
            local_groups: &names.local_groups,
            local_types: &names.local_types,
        };
        schema_components2string(&mut c, crate_mod)?;
        writeln!(&mut code, "}}")?;
    }
    if !internal {
        code = code.replace("crate::parse", "xust_xsd::parse");
    }
    rustfmt(&code)
}

pub fn xsd2rs(
    input_files: &[PathBuf],
    internal: bool,
) -> std::result::Result<String, Box<dyn std::error::Error>> {
    let validator = get_xsd_validator();
    let model = xsd2schema_components(input_files, Some(&validator), None)?;
    schema_components2rs(&model, internal)
}
