#![allow(dead_code)]
use chrono::{Datelike, FixedOffset, NaiveDate, NaiveDateTime, NaiveTime, TimeZone};
use rust_decimal::Decimal;
use std::fmt::{Display, Formatter};
use xust_tree::qnames::QName;

// The 19 primitive datatypes of XML Schema are separate enum values, except
// for anyURI which is the same as String.
// Decimal and Integer are different represenations of xs:decimal.

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub(crate) enum AtomicValue {
    String(Box<str>),
    Boolean(bool),
    Decimal(Decimal),
    Float(f32),
    Double(f64),
    Duration(Duration),
    DateTime(DateTime),
    Time(Time),
    Date(Date),
    GYearMonth(GYearMonth),
    GYear(GYear),
    GMonthDay(GMonthDay),
    GDay(GDay),
    GMonth(GMonth),
    HexBinary(Box<[u8]>),
    Base64Binary(Box<[u8]>),
    AnyURI(Box<str>),
    QName(QName),
    Notation(Box<str>),
}

impl Display for AtomicValue {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            AtomicValue::String(v) => write!(f, "{}", v),
            AtomicValue::Boolean(v) => write!(f, "{}", v),
            AtomicValue::Decimal(v) => write!(f, "{}", v),
            AtomicValue::Float(v) => write!(f, "{}", v),
            AtomicValue::Double(v) => write!(f, "{}", v),
            AtomicValue::Duration(v) => write!(f, "{}", v),
            AtomicValue::DateTime(v) => write!(f, "{}", v),
            AtomicValue::Time(v) => write!(f, "{}", v),
            AtomicValue::Date(v) => write!(f, "{}", v),
            AtomicValue::GYearMonth(v) => write!(f, "{}", v),
            AtomicValue::GYear(v) => write!(f, "{}", v),
            AtomicValue::GMonthDay(v) => write!(f, "{}", v),
            AtomicValue::GDay(v) => write!(f, "{}", v),
            AtomicValue::GMonth(v) => write!(f, "{}", v),
            AtomicValue::HexBinary(v) => write!(f, "{:X?}", v),
            AtomicValue::Base64Binary(v) => write!(
                f,
                "{}",
                base64::display::Base64Display::new(v, &base64::engine::general_purpose::STANDARD)
            ),
            AtomicValue::AnyURI(v) => write!(f, "{}", v),
            AtomicValue::QName(v) => write!(f, "{}", v),
            AtomicValue::Notation(v) => write!(f, "{}", v),
        }
    }
}

// see https://www.w3.org/TR/xmlschema11-2/#duration
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd)]
pub struct Duration {
    months: u32,
    seconds: Decimal,
}

impl Duration {
    pub(crate) fn new(months: u32, seconds: Decimal) -> Self {
        Self { months, seconds }
    }
}

impl Display for Duration {
    fn fmt(&self, _f: &mut Formatter) -> std::fmt::Result {
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct DateTime {
    date_time: NaiveDateTime,
    time_zone: Option<FixedOffset>,
}

impl DateTime {
    pub(crate) fn new(date_time: NaiveDateTime, time_zone: Option<FixedOffset>) -> Self {
        Self {
            date_time,
            time_zone,
        }
    }
    pub(crate) fn to_date(self) -> Date {
        Date {
            date: self.date_time.date(),
            time_zone: self.time_zone,
        }
    }
    pub(crate) fn to_time(self) -> Time {
        Time {
            time: self.date_time.time(),
            time_zone: self.time_zone,
        }
    }
}

impl Display for DateTime {
    fn fmt(&self, _f: &mut Formatter) -> std::fmt::Result {
        Ok(())
    }
}

impl PartialOrd for DateTime {
    fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering> {
        match (self.time_zone, rhs.time_zone) {
            (None, None) => Some(self.date_time.cmp(&rhs.date_time)),
            (Some(self_tz), Some(rhs_tz)) => self_tz
                .from_utc_datetime(&self.date_time)
                .partial_cmp(&rhs_tz.from_utc_datetime(&rhs.date_time)),

            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Time {
    time: NaiveTime,
    time_zone: Option<FixedOffset>,
}

impl Time {
    pub(crate) fn new(time: NaiveTime, time_zone: Option<FixedOffset>) -> Self {
        Self { time, time_zone }
    }
}
impl Display for Time {
    fn fmt(&self, _f: &mut Formatter) -> std::fmt::Result {
        Ok(())
    }
}
impl PartialOrd for Time {
    fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering> {
        match (self.time_zone, rhs.time_zone) {
            (None, None) => self.time.partial_cmp(&rhs.time),
            (Some(self_tz), Some(rhs_tz)) => {
                (self.time - self_tz).partial_cmp(&(rhs.time - rhs_tz))
            }
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Date {
    date: NaiveDate,
    time_zone: Option<FixedOffset>,
}

impl Date {
    pub(crate) fn new(date: NaiveDate, time_zone: Option<FixedOffset>) -> Self {
        Self { date, time_zone }
    }
    pub(crate) fn to_date_time(self) -> DateTime {
        DateTime {
            date_time: self.date.and_time(chrono::NaiveTime::default()),
            time_zone: self.time_zone,
        }
    }
    pub(crate) fn to_year(self) -> GYear {
        GYear::new(self.date.year(), self.time_zone).unwrap()
    }
    pub(crate) fn to_month(self) -> GMonth {
        GMonth::new(self.date.month(), self.time_zone).unwrap()
    }
    pub(crate) fn to_day(self) -> GDay {
        GDay::new(self.date.day(), self.time_zone).unwrap()
    }
    pub(crate) fn to_year_month(self) -> GYearMonth {
        GYearMonth::new(self.date.year(), self.date.month(), self.time_zone).unwrap()
    }
    pub(crate) fn to_month_day(self) -> GMonthDay {
        GMonthDay::new(self.date.month(), self.date.day(), self.time_zone).unwrap()
    }
}
impl Display for Date {
    fn fmt(&self, _f: &mut Formatter) -> std::fmt::Result {
        Ok(())
    }
}
fn to_date_time(date: chrono::NaiveDate, tz: chrono::FixedOffset) -> chrono::DateTime<FixedOffset> {
    tz.from_utc_datetime(&date.and_time(chrono::NaiveTime::default()))
}
impl PartialOrd for Date {
    fn partial_cmp(&self, rhs: &Self) -> Option<std::cmp::Ordering> {
        match (self.time_zone, rhs.time_zone) {
            (None, None) => self.date.partial_cmp(&rhs.date),
            (Some(lhs_tz), Some(rhs_tz)) => {
                let lhs = to_date_time(self.date, lhs_tz);
                let rhs = to_date_time(rhs.date, rhs_tz);
                lhs.partial_cmp(&rhs)
            }
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd)]
pub struct GYearMonth(Date);

impl GYearMonth {
    pub(crate) fn new(year: i32, month: u32, time_zone: Option<FixedOffset>) -> Option<Self> {
        NaiveDate::from_ymd_opt(year, month, 1).map(|date| Self(Date { date, time_zone }))
    }
}

impl Display for GYearMonth {
    fn fmt(&self, _f: &mut Formatter) -> std::fmt::Result {
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd)]
pub struct GYear(Date);
impl GYear {
    pub(crate) fn new(year: i32, time_zone: Option<FixedOffset>) -> Option<Self> {
        NaiveDate::from_ymd_opt(year, 1, 1).map(|date| Self(Date { date, time_zone }))
    }
}
impl Display for GYear {
    fn fmt(&self, _f: &mut Formatter) -> std::fmt::Result {
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd)]
pub struct GMonthDay(Date);
impl GMonthDay {
    pub(crate) fn new(month: u32, day: u32, time_zone: Option<FixedOffset>) -> Option<Self> {
        NaiveDate::from_ymd_opt(0, month, day).map(|date| Self(Date { date, time_zone }))
    }
}
impl Display for GMonthDay {
    fn fmt(&self, _f: &mut Formatter) -> std::fmt::Result {
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd)]
pub struct GDay(Date);
impl GDay {
    pub(crate) fn new(day: u32, time_zone: Option<FixedOffset>) -> Option<Self> {
        NaiveDate::from_ymd_opt(0, 1, day).map(|date| Self(Date { date, time_zone }))
    }
}
impl Display for GDay {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0.date.day())?;
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd)]
pub struct GMonth(Date);
impl GMonth {
    pub(crate) fn new(month: u32, time_zone: Option<FixedOffset>) -> Option<Self> {
        NaiveDate::from_ymd_opt(0, month, 1).map(|date| Self(Date { date, time_zone }))
    }
}
impl Display for GMonth {
    fn fmt(&self, _f: &mut Formatter) -> std::fmt::Result {
        Ok(())
    }
}
