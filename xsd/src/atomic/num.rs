use rust_decimal::Decimal;

use crate::xpath_error::{Error, Result};
use std::ops::Add;
use std::ops::Div;
use std::ops::Mul;
use std::ops::Rem;
use std::ops::Sub;

pub(crate) trait Inf: Sized {
    fn inf() -> Result<Self>;
    fn min_inf() -> Result<Self>;
    fn nan() -> Result<Self>;
    fn is_nan(&self) -> bool;
    fn is_normal(&self) -> bool;
    fn is_sign_positive(&self) -> bool;
}

impl Inf for i64 {
    fn inf() -> Result<Self> {
        Err(Error::foar(1))
    }
    fn min_inf() -> Result<Self> {
        Err(Error::foar(1))
    }
    fn nan() -> Result<Self> {
        Err(Error::foar(1))
    }
    fn is_nan(&self) -> bool {
        false
    }
    fn is_normal(&self) -> bool {
        true
    }
    fn is_sign_positive(&self) -> bool {
        i64::is_positive(*self)
    }
}

impl Inf for Decimal {
    fn inf() -> Result<Self> {
        Err(Error::foar(1))
    }
    fn min_inf() -> Result<Self> {
        Err(Error::foar(1))
    }
    fn nan() -> Result<Self> {
        Err(Error::foar(1))
    }
    fn is_nan(&self) -> bool {
        false
    }
    fn is_normal(&self) -> bool {
        true
    }
    fn is_sign_positive(&self) -> bool {
        Decimal::is_sign_positive(self)
    }
}

impl Inf for f64 {
    fn inf() -> Result<Self> {
        Ok(f64::INFINITY)
    }
    fn min_inf() -> Result<Self> {
        Ok(-f64::INFINITY)
    }
    fn nan() -> Result<Self> {
        Ok(f64::NAN)
    }
    fn is_nan(&self) -> bool {
        f64::is_nan(*self)
    }
    fn is_normal(&self) -> bool {
        f64::is_normal(*self)
    }
    fn is_sign_positive(&self) -> bool {
        f64::is_sign_positive(*self)
    }
}

impl Inf for f32 {
    fn inf() -> Result<Self> {
        Ok(f32::INFINITY)
    }
    fn min_inf() -> Result<Self> {
        Ok(-f32::INFINITY)
    }
    fn nan() -> Result<Self> {
        Ok(f32::NAN)
    }
    fn is_nan(&self) -> bool {
        f32::is_nan(*self)
    }
    fn is_normal(&self) -> bool {
        f32::is_normal(*self)
    }
    fn is_sign_positive(&self) -> bool {
        f32::is_sign_positive(*self)
    }
}

pub(crate) trait SafeOps:
    Sized + Add + Div + Mul + Sub + Rem + PartialEq + PartialOrd + Default + std::fmt::Display + Inf
{
    fn safe_add(self, b: Self) -> Result<<Self as Add>::Output> {
        Ok(self.add(b))
    }
    fn safe_sub(self, b: Self) -> Result<<Self as Sub>::Output> {
        Ok(self.sub(b))
    }
    fn safe_div(self, b: Self) -> Result<<Self as Div>::Output>
    where
        <Self as Div>::Output: Inf,
    {
        if b == Self::default() {
            if self > Self::default() {
                if b.is_sign_positive() {
                    <Self as Div>::Output::inf()
                } else {
                    <Self as Div>::Output::min_inf()
                }
            } else if self < Self::default() {
                if b.is_sign_positive() {
                    <Self as Div>::Output::min_inf()
                } else {
                    <Self as Div>::Output::inf()
                }
            } else {
                <Self as Div>::Output::nan()
            }
        } else {
            Ok(self.div(b))
        }
    }
    fn safe_idiv(self, b: Self) -> Result<<Self as Div>::Output>
    where
        <Self as Div>::Output: Inf,
    {
        if b == Self::default() {
            Err(Error::foar(1))
        } else if !self.is_normal() || b.is_nan() {
            Err(Error::foar(2))
        } else {
            Ok(self.div(b))
        }
    }
    fn safe_mul(self, b: Self) -> Result<<Self as Mul>::Output> {
        Ok(self.mul(b))
    }
    fn safe_mod(self, b: Self) -> Result<<Self as Rem>::Output>
    where
        <Self as Rem>::Output: Inf,
    {
        if b == Self::default() {
            if self > Self::default() {
                <Self as Rem>::Output::inf()
            } else if self < Self::default() {
                <Self as Rem>::Output::min_inf()
            } else {
                <Self as Rem>::Output::nan()
            }
        } else {
            Ok(self.rem(b))
        }
    }
}

impl<
        A: Add + Div + Mul + Sub + Rem + PartialEq + PartialOrd + Default + std::fmt::Display + Inf,
    > SafeOps for A
{
}
