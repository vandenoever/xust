pub mod write;

use crate::{
    atomic::atomic_value::{
        AtomicValue, Date, DateTime, Duration, GDay, GMonth, GMonthDay, GYear, GYearMonth, Time,
    },
    facets::{Facets, WhiteSpace},
    qname_parser::QNameParser,
    types::{built_ins::*, TypeId},
};
use chrono::{FixedOffset, NaiveDate, NaiveDateTime, NaiveTime};
use lazy_static::lazy_static;
use nom::{
    bytes::complete::tag,
    character::complete::{self, char, digit1},
    combinator::{map, map_res, opt, recognize},
    sequence::tuple,
};
use regex::Regex;
use rust_decimal::Decimal;
use std::fmt;
use std::str::FromStr;
use xust_xml::{is_name_char, name, ncname};

type R<'a> = Result<AtomicValue, ()>;
type ParseFn = for<'a> fn(&'a str, &Facets, _: &QNameParser) -> R<'a>;

#[derive(Clone, Copy)]
pub(crate) struct PrimitiveParSer {
    parser: ParseFn,
    primitive: TypeId,
}

impl PrimitiveParSer {
    pub fn primitive(&self) -> TypeId {
        self.primitive
    }
    pub fn parse<'a>(&self, s: &'a str, facets: &Facets, qname_parser: &QNameParser) -> R<'a> {
        facets.check_lexical(s)?;
        let v = (self.parser)(s, facets, qname_parser)?;
        facets.check_value(&v, s)?;
        Ok(v)
    }
}

impl PartialEq for PrimitiveParSer {
    fn eq(&self, rhs: &Self) -> bool {
        self.parser as usize == rhs.parser as usize && self.primitive == rhs.primitive
    }
}

impl fmt::Debug for PrimitiveParSer {
    fn fmt(&self, _f: &mut fmt::Formatter) -> fmt::Result {
        Ok(())
    }
}

pub(crate) const ERROR_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: error_par,
    primitive: XS_ERROR,
};
fn error_par<'a>(_v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    Err(())
}

pub(crate) const DECIMAL_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: decimal_par,
    primitive: XS_DECIMAL,
};
fn decimal_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    v.parse::<Decimal>()
        .map_err(|_| ())
        .map(AtomicValue::Decimal)
}

pub(crate) const DOUBLE_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: double_par,
    primitive: XS_DOUBLE,
};
fn double_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    v.parse::<f64>().map_err(|_| ()).map(AtomicValue::Double)
}

pub(crate) const FLOAT_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: float_par,
    primitive: XS_FLOAT,
};
fn float_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    v.parse::<f32>().map_err(|_| ()).map(AtomicValue::Float)
}

pub(crate) const DURATION_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: duration_par,
    primitive: XS_DURATION,
};
fn duration_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    let (v, _negative) = if let Some(v) = v.strip_prefix('-') {
        (v, true)
    } else {
        (v, false)
    };
    let part = |c: char| map(opt(tuple((digit1, char(c)))), |v| v.map(|v| v.0));
    let r = tuple((
        char('P'),
        part('Y'),
        part('M'),
        part('D'),
        opt(tuple((
            char('T'),
            part('H'),
            part('M'),
            opt(tuple((
                recognize(tuple((digit1, opt(tuple((char('.'), digit1)))))),
                char('S'),
            ))),
        ))),
    ))(v)
    .map_err(|_: nom::Err<()>| ())?;
    if !r.0.is_empty() {
        return Err(());
    }
    let r = r.1;
    let year = if let Some(v) = r.1 {
        u32::from_str(v).map_err(|_| ())?
    } else {
        0
    };
    let month = if let Some(v) = r.2 {
        u32::from_str(v).map_err(|_| ())?
    } else {
        0
    };
    let day = if let Some(v) = r.3 {
        Decimal::from_str(v).map_err(|_| ())?
    } else {
        Decimal::ZERO
    };
    let mut hour = Decimal::ZERO;
    let mut minute = Decimal::ZERO;
    let mut second = Decimal::ZERO;
    if let Some(r) = r.4 {
        if let Some(v) = r.1 {
            hour = Decimal::from_str(v).map_err(|_| ())?;
        }
        if let Some(v) = r.2 {
            minute = Decimal::from_str(v).map_err(|_| ())?;
        }
        if let Some((v, _)) = r.3 {
            second = Decimal::from_str(v).map_err(|_| ())?;
        }
    }
    let year_month = year * 12 + month;
    let day_time = Decimal::from(24 * 3600) * day
        + Decimal::from(3600) * hour
        + Decimal::from(60) * minute
        + second;
    Ok(AtomicValue::Duration(Duration::new(year_month, day_time)))
}

pub(crate) const DATE_TIME_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: date_time_par,
    primitive: XS_DATE_TIME,
};
fn date_time_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    let (date_time, offset) = if let Ok(v) = chrono::DateTime::parse_from_rfc3339(v) {
        (v.naive_utc(), Some(*v.offset()))
    } else if let Ok(v) = NaiveDateTime::parse_from_str(v, "%Y-%m-%dT%H:%M:%S") {
        (v, None)
    } else {
        return Err(());
    };
    Ok(AtomicValue::DateTime(DateTime::new(date_time, offset)))
}

pub(crate) const TIME_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: time_par,
    primitive: XS_TIME,
};
fn time_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    use chrono::format::{Fixed, Item};
    let b = v.as_bytes();
    if b.len() < 8 || b[2] != b':' || b[5] != b':' {
        return Err(());
    }
    let hour = u32::from_str(&v[0..2]).map_err(|_| ())?;
    let minute = u32::from_str(&v[3..5]).map_err(|_| ())?;
    let second = u32::from_str(&v[6..8]).map_err(|_| ())?;
    let mut nano = 0;
    let mut parsed = chrono::format::Parsed::new();
    let (remaining, nano_str) =
        opt::<_, _, (), _>(recognize(tuple((char('.'), digit1))))(&v[8..]).map_err(|_| ())?;
    if let Some(nano_str) = nano_str {
        const FORMAT: [chrono::format::Item; 1] = [Item::Fixed(Fixed::Nanosecond)];
        chrono::format::parse(&mut parsed, nano_str, FORMAT.iter()).map_err(|_| ())?;
        nano = parsed.nanosecond.unwrap_or(0);
    }
    let timezone = parse_timezone(remaining)?;
    let time = NaiveTime::from_hms_nano_opt(hour, minute, second, nano).ok_or(())?;
    Ok(AtomicValue::Time(Time::new(time, timezone)))
}

fn parse_timezone(v: &str) -> Result<Option<FixedOffset>, ()> {
    use chrono::format::{Fixed, Item};
    let mut parsed = chrono::format::Parsed::new();
    let mut timezone = None;
    const FORMAT_TZ: [chrono::format::Item; 1] = [Item::Fixed(Fixed::TimezoneOffsetColonZ)];
    if !v.is_empty() {
        chrono::format::parse(&mut parsed, v, FORMAT_TZ.iter()).map_err(|_| ())?;
        timezone = parsed.offset.and_then(FixedOffset::east_opt);
    };
    Ok(timezone)
}

pub(crate) const DATE_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: date_par,
    primitive: XS_DATE,
};
fn date_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    let (v, year) = year_frag(v).map_err(|_| ())?;
    let b = v.as_bytes();
    if b.len() < 6 || b[0] != b'-' || b[3] != b'-' {
        return Err(());
    }
    let month = u32::from_str(&v[1..3]).map_err(|_| ())?;
    let day = u32::from_str(&v[4..6]).map_err(|_| ())?;
    let timezone = parse_timezone(&v[6..])?;
    let date = NaiveDate::from_ymd_opt(year, month, day).ok_or(())?;
    Ok(AtomicValue::Date(Date::new(date, timezone)))
}

pub(crate) const G_YEAR_MONTH_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: g_year_month_par,
    primitive: XS_G_YEAR_MONTH,
};
fn g_year_month_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    let r = tuple((complete::i32, char('-'), month, opt(timezone)))(v).map_err(|_| ())?;
    if !r.0.is_empty() {
        return Err(());
    }
    GYearMonth::new(r.1 .0, r.1 .2 as u32, r.1 .3)
        .map(AtomicValue::GYearMonth)
        .ok_or(())
}

pub(crate) const G_YEAR_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: g_year_par,
    primitive: XS_G_YEAR,
};
fn g_year_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    let r = tuple((year_frag, opt(timezone)))(v).map_err(|_| ())?;
    if !r.0.is_empty() {
        return Err(());
    }
    GYear::new(r.1 .0, r.1 .1).map(AtomicValue::GYear).ok_or(())
}

pub(crate) const G_MONTH_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: g_month_par,
    primitive: XS_G_MONTH,
};
fn g_month_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    // trailing -- after month is a typo from
    // <https://www.w3.org/TR/2001/REC-xmlschema-2-20010502> and earlier
    let r = tuple((tag("--"), month, opt(tag("--")), opt(timezone)))(v).map_err(|_| ())?;
    if !r.0.is_empty() {
        return Err(());
    }
    GMonth::new(r.1 .1 as u32, r.1 .3)
        .map(AtomicValue::GMonth)
        .ok_or(())
}

#[test]
fn test_g_month_par() {
    fn to_atomic(m: Option<GMonth>) -> Result<AtomicValue, ()> {
        m.map(AtomicValue::GMonth).ok_or(())
    }
    assert_eq!(
        g_month_par("--01", &Facets::default(), &QNameParser::default()),
        to_atomic(GMonth::new(1, None))
    );
    assert_eq!(
        g_month_par("--01--", &Facets::default(), &QNameParser::default()),
        to_atomic(GMonth::new(1, None))
    );
    assert_eq!(
        g_month_par("--01Z", &Facets::default(), &QNameParser::default()),
        to_atomic(GMonth::new(1, Some(FixedOffset::east_opt(0).unwrap())))
    );
    assert_eq!(
        g_month_par("--01+10:00", &Facets::default(), &QNameParser::default()),
        to_atomic(GMonth::new(
            1,
            Some(FixedOffset::east_opt(10 * 3600).unwrap())
        ))
    );
}

fn two_digits(v: &str) -> nom::IResult<&str, u8> {
    let r = complete::u8(v)?;
    if v.len() - r.0.len() == 2 {
        return Ok(r);
    }
    Err(nom::Err::Error(nom::error::make_error(
        v,
        nom::error::ErrorKind::Fail,
    )))
}

fn month(v: &str) -> nom::IResult<&str, u8> {
    map_res(two_digits, |v| {
        if (1..=12).contains(&v) {
            Ok(v)
        } else {
            Err(())
        }
    })(v)
}
fn day(v: &str) -> nom::IResult<&str, u8> {
    map_res(two_digits, |v| {
        if (1..=31).contains(&v) {
            Ok(v)
        } else {
            Err(())
        }
    })(v)
}

fn timezone(v: &str) -> nom::IResult<&str, FixedOffset> {
    use chrono::format::{Fixed, Item};
    const FORMAT_TZ: [chrono::format::Item; 1] = [Item::Fixed(Fixed::TimezoneOffsetColonZ)];
    let mut parsed = chrono::format::Parsed::new();
    let mut offset = None;
    if chrono::format::parse(&mut parsed, v, FORMAT_TZ.iter()).is_ok() {
        offset = FixedOffset::east_opt(parsed.offset.unwrap());
    }
    if let Some(offset) = offset {
        Ok(("", offset))
    } else {
        Err(nom::Err::Error(nom::error::make_error(
            "",
            nom::error::ErrorKind::Fail,
        )))
    }
}

fn year_frag(v: &str) -> nom::IResult<&str, i32> {
    let digits = v.strip_prefix('-').unwrap_or(v);
    let (remaining, year) = complete::i32(v)?;
    let len = digits.len() - remaining.len();
    if (v.starts_with('0') && len != 4) || len < 3 {
        // number must be at least 4 digits
        return Err(nom::Err::Error(nom::error::make_error(
            "",
            nom::error::ErrorKind::Fail,
        )));
    }
    Ok((remaining, year))
}

pub(crate) const G_DAY_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: g_day_par,
    primitive: XS_G_DAY,
};
fn g_day_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    let r = tuple((tag("---"), day, opt(timezone)))(v).map_err(|_| ())?;
    if !r.0.is_empty() {
        return Err(());
    }
    GDay::new(r.1 .1 as u32, r.1 .2)
        .map(AtomicValue::GDay)
        .ok_or(())
}
pub(crate) const G_MONTH_DAY_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: g_month_day_par,
    primitive: XS_G_MONTH_DAY,
};
fn g_month_day_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    let r = tuple((tag("--"), month, char('-'), day, opt(timezone)))(v).map_err(|_| ())?;
    if !r.0.is_empty() {
        return Err(());
    }
    GMonthDay::new(r.1 .1 as u32, r.1 .3 as u32, r.1 .4)
        .map(AtomicValue::GMonthDay)
        .ok_or(())
}

pub(crate) const BOOLEAN_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: boolean_par,
    primitive: XS_BOOLEAN,
};
fn boolean_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    if v == "true" || v == "1" {
        Ok(AtomicValue::Boolean(true))
    } else if v == "false" || v == "0" {
        Ok(AtomicValue::Boolean(false))
    } else {
        Err(())
    }
}

pub(crate) const STRING_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: string_par,
    primitive: XS_STRING,
};
fn string_par<'a>(v: &'a str, facets: &Facets, _: &QNameParser) -> R<'a> {
    lazy_static! {
        static ref REPLACE: regex::Regex = Regex::new("[\t\r\n]").unwrap();
        static ref COLLAPSE: regex::Regex = Regex::new("[\t\r\n]+").unwrap();
    }
    let v: Box<str> = if facets.white_space == WhiteSpace::Replace {
        REPLACE.replace_all(v, " ").into()
    } else if facets.white_space == WhiteSpace::Collapse {
        COLLAPSE.replace_all(v.trim(), " ").into()
    } else {
        v.into()
    };
    facets.check_length(&v)?;
    Ok(AtomicValue::String(v))
}

pub(crate) const NMTOKEN_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: nmtoken_par,
    primitive: XS_STRING,
};
fn nmtoken_par<'a>(v: &'a str, facets: &Facets, _: &QNameParser) -> R<'a> {
    if v.chars().all(is_name_char) {
        facets.check_length(v)?;
        Ok(AtomicValue::String(v.into()))
    } else {
        Err(())
    }
}
pub(crate) const NAME_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: name_par,
    primitive: XS_STRING,
};
fn name_par<'a>(v: &'a str, facets: &Facets, _: &QNameParser) -> R<'a> {
    if let Ok(("", _)) = name(v) {
        facets.check_length(v)?;
        Ok(AtomicValue::String(v.into()))
    } else {
        Err(())
    }
}
pub(crate) const NCNAME_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: ncname_par,
    primitive: XS_STRING,
};
fn ncname_par<'a>(v: &'a str, facets: &Facets, _: &QNameParser) -> R<'a> {
    if let Ok(("", _)) = ncname(v) {
        facets.check_length(v)?;
        Ok(AtomicValue::String(v.into()))
    } else {
        Err(())
    }
}

pub(crate) const ANY_URI_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: any_uri_par,
    primitive: XS_ANY_URI,
};
fn any_uri_par<'a>(v: &'a str, facets: &Facets, _: &QNameParser) -> R<'a> {
    facets.check_length(v)?;
    Ok(AtomicValue::AnyURI(v.into()))
}
pub(crate) const QNAME_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: qname_par,
    primitive: XS_QNAME,
};
fn qname_par<'a>(v: &'a str, _facets: &Facets, qname_parser: &QNameParser) -> R<'a> {
    if let Some(qname) = qname_parser.parse_qname(v) {
        // println!("Parsing     '{}'", v);
        Ok(AtomicValue::QName(qname))
    } else {
        // println!("Parsing bad '{}'", v);
        Err(())
    }
}
pub(crate) const BASE64_BINARY_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: base64_par,
    primitive: XS_BASE64_BINARY,
};
fn base64_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    use base64::Engine;
    let v = base64::engine::general_purpose::STANDARD
        .decode(v)
        .map_err(|_| ())?;
    Ok(AtomicValue::Base64Binary(v.into()))
}
pub(crate) const HEX_BINARY_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: hex_par,
    primitive: XS_HEX_BINARY,
};
fn hex_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    let v = hex::decode(v).map_err(|_| ())?;
    Ok(AtomicValue::Base64Binary(v.into()))
}
pub(crate) const NOTATION_PAR_SER: PrimitiveParSer = PrimitiveParSer {
    parser: notation_par,
    primitive: XS_NOTATION,
};
fn notation_par<'a>(v: &'a str, _facets: &Facets, _: &QNameParser) -> R<'a> {
    Ok(AtomicValue::Notation(v.into()))
}
