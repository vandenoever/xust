/*
use chrono::{Date, DateTime, FixedOffset};
use std::fmt::{Display, Formatter, Result};
use xust_tree::qnames::QName;

pub fn write<T: Display>(v: &T, f: &mut Formatter) -> Result {
    write!(f, "{}", v)
}
pub fn write_qname(v: &QName, f: &mut Formatter) -> Result {
    write!(f, "Q{{{}}}{}", v.namespace(), v.local_name())
}
pub fn write_time_tz(v: &DateTime<FixedOffset>, f: &mut Formatter) -> Result {
    write!(f, "{}", v.time())?;
    write_timezone(f, v.timezone())
}
pub fn write_date_time_tz(v: &DateTime<FixedOffset>, f: &mut Formatter) -> Result {
    write!(f, "{}T{}", v.date(), v.time())?;
    write_timezone(f, v.timezone())
}
pub fn write_date_tz(v: &Date<FixedOffset>, f: &mut Formatter) -> Result {
    write!(f, "{}", v.naive_utc())?;
    write_timezone(f, v.timezone())
}

fn write_timezone(f: &mut Formatter, i: chrono::FixedOffset) -> Result {
    if i == chrono::FixedOffset::west(0) {
        write!(f, "Z")
    } else {
        write!(f, "{}", i)
    }
}
*/
