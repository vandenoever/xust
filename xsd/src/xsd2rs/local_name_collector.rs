use xust_tree::qnames::Ref;

use crate::{
    schema_components::{
        ContentType, ModelGroupId, Particle, SchemaComponents, Term, Type, TypeDef,
    },
    types::TypeId,
};
use std::collections::{BTreeMap, HashSet};

/// This module gives names to groups and types that do not yet have a name.

pub(super) struct LocalNameCollector<'a> {
    local_names: HashSet<LocalName>,
    model: &'a SchemaComponents,
    pub local_groups: BTreeMap<ModelGroupId, LocalName>,
    pub local_types: BTreeMap<TypeId, LocalName>,
}

impl<'a> LocalNameCollector<'a> {
    pub fn new(model: &'a SchemaComponents) -> Self {
        Self {
            model,
            local_names: HashSet::default(),
            local_groups: BTreeMap::default(),
            local_types: BTreeMap::default(),
        }
    }
    pub fn invent_names(&mut self, base: &LocalName, particles: &[Particle]) {
        for p in particles {
            self.invent_name(base, &p.term);
        }
    }
    pub fn invent_name(&mut self, base: &LocalName, term: &Term) {
        match term {
            Term::TopLevelElement(_) => {}
            Term::LocalElement(e) => {
                let name = LocalName::new(self.model.qnames.to_ref(e.tag));
                self.invent_names_in_element(&name, e.content);
            }
            Term::Group(g) => {
                let group = self.model.get_group(*g);
                if group.name.is_none() {
                    self.add_group(*g, base);
                    self.invent_names(base, &group.particles);
                }
            }
            Term::Any(_) => {}
        }
    }
    fn invent_names_in_element(&mut self, base: &LocalName, type_id: TypeId) {
        let t = &self.model.types[type_id];
        if t.name.is_none() {
            self.add_type(type_id, base);
            self.invent_names_for_type(base, t);
        }
    }
    pub fn invent_names_for_type(&mut self, base: &LocalName, t: &Type) {
        match &t.def {
            TypeDef::ComplexType(ct) => match &ct.content_type {
                ContentType::ComplexType { particle, .. } => {
                    self.invent_name(base, &particle.term);
                }
                _ => {}
            },
            TypeDef::SimpleType(_) => {}
        }
    }
    fn add_type(&mut self, t: TypeId, base: &LocalName) {
        let name = self.add_name(base);
        self.local_types.insert(t, name);
    }
    fn add_group(&mut self, group: ModelGroupId, base: &LocalName) {
        let name = self.add_name(base);
        self.local_groups.insert(group, name);
    }
    fn add_name(&mut self, base: &LocalName) -> LocalName {
        let mut name = base.clone();
        let mut i = 1;
        while self.local_names.contains(&name) {
            name.name = format!("{}{}", base.name, i);
            i += 1;
        }
        self.local_names.insert(name.clone());
        name
    }
}

#[derive(Clone, PartialEq, Eq, Hash)]
pub(super) struct LocalName {
    pub prefix: String,
    pub name: String,
}

impl LocalName {
    pub fn new(qname: Ref) -> Self {
        Self {
            name: qname.local_name().to_string(),
            prefix: qname.prefix().to_string(),
        }
    }
}
