use crate::types::TypeId;

pub trait Introspection {
    const TYPE_ID: TypeId;
}
