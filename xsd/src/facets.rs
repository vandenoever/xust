pub(crate) mod pattern;
use crate::atomic::atomic_value::AtomicValue;
use bitflags::bitflags;
use lazy_static::lazy_static;
use pattern::Pattern;
use rust_decimal::Decimal;

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct Facets {
    pub white_space: WhiteSpace,
    pub min_length: usize,
    pub max_length: Option<usize>,
    pub min_inclusive: Option<AtomicValue>,
    pub max_inclusive: Option<AtomicValue>,
    pub min_exclusive: Option<AtomicValue>,
    pub max_exclusive: Option<AtomicValue>,
    pub total_digits: Option<u32>,
    pub fraction_digits: Option<u32>,
    pub patterns: Pattern,
    pub enumeration: Vec<AtomicValue>,
    pub explicit_timezone: Option<ExplicitTimezone>,
    pub fixed: Fixed,
}

lazy_static! {
    pub(crate) static ref EMPTY: Facets = Facets::default();
}

impl Facets {
    pub fn default() -> Self {
        Self {
            white_space: WhiteSpace::Preserve,
            min_length: 0,
            max_length: None,
            min_inclusive: None,
            max_inclusive: None,
            min_exclusive: None,
            max_exclusive: None,
            total_digits: None,
            fraction_digits: None,
            patterns: Pattern::empty(),
            enumeration: Vec::new(),
            explicit_timezone: None,
            fixed: Fixed::empty(),
        }
    }
    pub fn string() -> Self {
        let mut s = Self::default();
        s.white_space = WhiteSpace::Preserve;
        s
    }
    pub fn white_space_collapse() -> Self {
        let mut s = Self::default();
        s.white_space = WhiteSpace::Trim;
        s.fixed = Fixed::WHITE_SPACE;
        s
    }
    pub fn date_time() -> Self {
        let mut s = Self::default();
        s.white_space = WhiteSpace::Trim;
        s.explicit_timezone = Some(ExplicitTimezone::Optional);
        s.fixed = Fixed::WHITE_SPACE;
        s
    }
    pub fn normalized_string() -> Self {
        let mut s = Self::default();
        s.white_space = WhiteSpace::Replace;
        s
    }
    pub fn token() -> Self {
        let mut s = Self::default();
        s.white_space = WhiteSpace::Collapse;
        s
    }
    pub fn language() -> Self {
        Self {
            patterns: Pattern::new(&["[a-zA-Z]{1,8}(-[a-zA-Z0-9]{1,8})*"]).unwrap(),
            white_space: WhiteSpace::Trim,
            ..Self::default()
        }
    }
    pub fn nmtoken() -> Self {
        Self {
            patterns: Pattern::new(&["\\c+"]).unwrap(),
            white_space: WhiteSpace::Trim,
            ..Self::default()
        }
    }
    pub fn name() -> Self {
        Self {
            patterns: Pattern::new(&["\\i\\c*"]).unwrap(),
            white_space: WhiteSpace::Trim,
            ..Self::default()
        }
    }
    pub fn ncname() -> Self {
        Self {
            patterns: Pattern::ncname(),
            white_space: WhiteSpace::Trim,
            ..Self::default()
        }
    }
    pub fn non_empty() -> Self {
        let mut s = Self::default();
        s.min_length = 1;
        s.white_space = WhiteSpace::Trim;
        s
    }
    pub fn integer() -> Self {
        let mut s = Self::default();
        s.fraction_digits = Some(0);
        s.white_space = WhiteSpace::Trim;
        s.fixed = Fixed::FRACTION_DIGITS.union(Fixed::WHITE_SPACE);
        s
    }
    pub fn non_positive_integer() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::ZERO));
        s
    }
    pub fn non_negative_integer() -> Self {
        let mut s = Self::integer();
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::ZERO));
        s
    }
    pub fn positive_integer() -> Self {
        let mut s = Self::integer();
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::ONE));
        s
    }
    pub fn negative_integer() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::NEGATIVE_ONE));
        s
    }
    pub fn long() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::from(i64::MAX)));
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::from(i64::MIN)));
        s
    }
    pub fn int() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::from(i32::MAX)));
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::from(i32::MIN)));
        s
    }
    pub fn short() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::from(i16::MAX)));
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::from(i16::MIN)));
        s
    }
    pub fn byte() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::from(i8::MAX)));
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::from(i8::MIN)));
        s
    }
    pub fn unsigned_long() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::from(u64::MAX)));
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::from(u64::MIN)));
        s
    }
    pub fn unsigned_int() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::from(u32::MAX)));
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::from(u32::MIN)));
        s
    }
    pub fn unsigned_short() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::from(u16::MAX)));
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::from(u16::MIN)));
        s
    }
    pub fn unsigned_byte() -> Self {
        let mut s = Self::integer();
        s.max_inclusive = Some(AtomicValue::Decimal(Decimal::from(u8::MAX)));
        s.min_inclusive = Some(AtomicValue::Decimal(Decimal::from(u8::MIN)));
        s
    }
    pub fn check_lexical(&self, v: &str) -> Result<(), ()> {
        // match any of the pattern restrictions
        if !self.patterns.test(v) {
            return Err(());
        }
        Ok(())
    }
    pub fn check_enumeration(&self, v: &str) -> Result<(), ()> {
        if !self.enumeration.is_empty()
            && !self.enumeration.iter().any(|e| {
                if let AtomicValue::String(e) = e {
                    e.as_ref() == v
                } else {
                    false
                }
            })
        {
            return Err(());
        }
        Ok(())
    }
    pub fn check_value(&self, v: &AtomicValue, s: &str) -> Result<(), ()> {
        if !self.enumeration.is_empty() && !self.enumeration.iter().any(|e| e == v) {
            // eprintln!("{} not in {:?}", v, self.enumeration);
            return Err(());
        }
        if let Some(min_inclusive) = &self.min_inclusive {
            if v < min_inclusive {
                return Err(());
            }
        }
        if let Some(max_inclusive) = &self.max_inclusive {
            if v > max_inclusive {
                return Err(());
            }
        }
        if let Some(min_exclusive) = &self.min_exclusive {
            if v <= min_exclusive {
                return Err(());
            }
        }
        if let Some(max_exclusive) = &self.max_exclusive {
            if v >= max_exclusive {
                return Err(());
            }
        }
        if let Some(total_digits) = self.total_digits {
            if let AtomicValue::Decimal(_) = v {
                let count = s.bytes().filter(|b| b.is_ascii_digit()).count();
                if count > total_digits as usize {
                    return Err(());
                }
            }
        }
        if let Some(fraction_digits) = self.fraction_digits {
            if let AtomicValue::Decimal(d) = v {
                if d.scale() > fraction_digits {
                    return Err(());
                }
            }
        }
        Ok(())
    }
    pub fn check_length(&self, v: &str) -> Result<(), ()> {
        if v.len() < self.min_length {
            return Err(());
        }
        if let Some(max) = self.max_length {
            if v.len() > max {
                return Err(());
            }
        }
        Ok(())
    }
    pub fn inherit(&mut self, base: &Facets) -> Result<(), String> {
        if let Some(base_min_inclusive) = &base.min_inclusive {
            if let Some(min_exclusive) = &self.min_exclusive {
                if *min_exclusive < *base_min_inclusive {
                    return Err(format!(
                        "Restricted min_exclusive must be larger than the base value. {} vs {}",
                        min_exclusive, base_min_inclusive
                    ));
                }
            } else if self.min_inclusive.is_none() {
                self.min_inclusive = Some(base_min_inclusive.clone());
            }
            if let Some(max_exclusive) = &self.max_exclusive {
                if max_exclusive < base_min_inclusive {
                    return Err(format!(
                        "Restricted max_exclusive must be larger than the base value. {} vs {}",
                        max_exclusive, base_min_inclusive
                    ));
                }
            }
        } else if let Some(base_min_exclusive) = &base.min_exclusive {
            if let Some(min_exclusive) = &self.min_exclusive {
                if min_exclusive > base_min_exclusive {
                    return Err(format!(
                        "Restricted min_exclusive must be larger than the base value. {} vs {}",
                        min_exclusive, base_min_exclusive
                    ));
                }
            } else if self.min_exclusive.is_none() {
                self.min_exclusive = Some(base_min_exclusive.clone());
            }
            if let Some(max_exclusive) = &self.max_exclusive {
                if max_exclusive >= base_min_exclusive {
                    return Err(format!(
                        "Restricted max_exclusive must be larger than the base value. {} vs {}",
                        max_exclusive, base_min_exclusive
                    ));
                }
            }
        }
        if let Some(base_max_inclusive) = &base.max_inclusive {
            if let Some(max_exclusive) = &self.max_exclusive {
                if max_exclusive > base_max_inclusive {
                    return Err(format!(
                        "Restricted max_exclusive must be smaller than the base value. {} vs {}",
                        max_exclusive, base_max_inclusive
                    ));
                }
            } else if self.max_inclusive.is_none() {
                self.max_inclusive = Some(base_max_inclusive.clone());
            }
            if let Some(min_exclusive) = &self.min_exclusive {
                if min_exclusive > base_max_inclusive {
                    return Err(format!(
                        "Restricted min_exclusive must be smaller than the base value. {} vs {}",
                        min_exclusive, base_max_inclusive
                    ));
                }
            }
        } else if let Some(base_max_exclusive) = &base.max_exclusive {
            if let Some(max_exclusive) = &self.max_exclusive {
                if max_exclusive > base_max_exclusive {
                    return Err(format!(
                        "Restricted max_exclusive must be smaller than the base value. {} vs {}",
                        max_exclusive, base_max_exclusive
                    ));
                }
            } else if self.max_exclusive.is_none() {
                self.max_exclusive = Some(base_max_exclusive.clone());
            }
            if let Some(min_exclusive) = &self.min_exclusive {
                if min_exclusive >= base_max_exclusive {
                    return Err(format!(
                        "Restricted min_exclusive must be smaller than the base value. {} vs {}",
                        min_exclusive, base_max_exclusive
                    ));
                }
            }
        }
        match (self.total_digits, base.total_digits) {
            (None, v) => self.total_digits = v,
            (Some(s), Some(v)) => self.total_digits = Some(s.min(v)),
            _ => {}
        }
        match (self.fraction_digits, base.fraction_digits) {
            (None, v) => self.fraction_digits = v,
            (Some(s), Some(v)) => self.fraction_digits = Some(s.min(v)),
            _ => {}
        }
        if self.enumeration.is_empty() {
            self.enumeration = base.enumeration.clone();
        }
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) enum WhiteSpace {
    Preserve,
    Replace,
    Collapse,
    // Trim is like collapse, but the lexical space has no whitespace, so just
    // trimming will do. This helps to avoid doing work that is not needed.
    // Nearly all simple types are trim. Only xs:string, xs:normalized_string,
    // xs:token and their derivatives are Preserve, Replace, and Collapse
    // respectively. But it's expensive to figure which of these is the
    // ancestor, so we use this special value.
    Trim,
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum ExplicitTimezone {
    Required,
    Prohibited,
    Optional,
}

bitflags! {
    #[derive(Default)]
    pub(crate) struct Fixed: u16 {
        const LENGTH = 1;
        const MIN_LENGTH = 2;
        const MAX_LENGTH = 4;
        const WHITE_SPACE = 8;
        const MAX_INCLUSIVE = 16;
        const MAX_EXCLUSIVE = 32;
        const MIN_EXCLUSIVE = 64;
        const MIN_INCLUSIVE = 128;
        const TOTAL_DIGITS = 256;
        const FRACTION_DIGITS = 512;
        const EXPLICIT_TIMEZONE = 1024;
    }
}
