#![recursion_limit = "256"]
pub mod error;
#[macro_use]
pub mod macros;
pub mod atomic;
mod chameleon;
mod facets;
mod fsm;
mod identity_constraint_grammar;
pub mod parse;
pub mod qname_parser;
mod schema_components;
pub mod types;
pub mod write;
pub mod xpath_error;
mod xsd;
pub mod xsd2rs;
pub mod xsd2schema_components;
pub mod xsd_validator;

pub use crate::types::TypeId;
pub use crate::xsd2schema_components::load::load_validator;
use atomic::Atomic;
use std::collections::HashMap;
use std::fmt::Display;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use xsd::xs::{e, g};
use xsd2schema_components::load::xsd2schema_components_qnames;
use xsd_validator::XsdValidator;
use xust_tree::{
    node,
    qnames::{Namespace, QNameCollection, QNameCollector, QNameId},
    tree::Tree,
};

type Node = node::Node<Arc<Tree<Atomic>>>;
pub type Element = node::Element<Arc<Tree<Atomic>>>;

pub trait Deserialize {
    type QNames;
    fn from_bytes(
        xml: Vec<u8>,
        validator: Option<&XsdValidator>,
    ) -> Result<(Self::QNames, Self), Box<dyn std::error::Error>>
    where
        Self: Sized;
    fn from_document_element(
        document_element: Element,
        validate: bool,
    ) -> Result<(Self::QNames, Self), Box<dyn std::error::Error>>
    where
        Self: Sized;
    fn qnames<'a>(&self, qnames: &'a Self::QNames) -> &'a QNameCollection;
}

pub trait Stream {
    fn start_element(&mut self, qname: QNameId);
    fn end_element(&mut self);
    fn start_attribute(&mut self, qname: QNameId);
    fn write_att(&mut self, _: &dyn Display);
    fn end_attribute(&mut self);
    fn write_str(&mut self, str: &str);
    fn write_element(&mut self, element: &Element);
}

pub trait AsStream<E> {
    fn serialize_to_stream(&self, out: &mut dyn Stream, env: &E);
}

impl<E, T> AsStream<E> for Option<T>
where
    T: AsStream<E>,
{
    fn serialize_to_stream(&self, out: &mut dyn Stream, env: &E) {
        println!("OPTION {}", self.is_some());
        if let Some(v) = self {
            v.serialize_to_stream(out, env)
        }
    }
}

impl<E> AsStream<E> for () {
    fn serialize_to_stream(&self, _out: &mut dyn Stream, _env: &E) {}
}

impl<E> AsStream<E> for String {
    fn serialize_to_stream(&self, out: &mut dyn Stream, _env: &E) {
        out.write_str(self)
    }
}

impl<E> AsStream<E> for Element {
    fn serialize_to_stream(&self, out: &mut dyn Stream, _env: &E) {
        out.write_element(self);
    }
}

// (T, String) is used for mixed elements
impl<T: AsStream<E>, E> AsStream<E> for (T, String) {
    fn serialize_to_stream(&self, out: &mut dyn Stream, env: &E) {
        self.0.serialize_to_stream(out, env);
        self.1.serialize_to_stream(out, env);
    }
}

fn add_includes(todo: &mut Vec<PathBuf>, input_file: &Path, schemas: &[e::Schema]) {
    for c in schemas.iter().flat_map(|s| &s.composition) {
        if let g::Composition::Include(include) = c {
            if let Some(parent) = input_file.parent() {
                todo.push(parent.join(&include.schema_location));
            }
        }
    }
}

fn adjust_schema(
    tns: &str,
    mut b: e::Schema,
) -> std::result::Result<e::Schema, Box<dyn std::error::Error>> {
    if let Some(ns) = &b.target_namespace {
        if ns != tns {
            return Err(format!("Differing namespaces via include: '{}' vs '{}'.", tns, ns).into());
        }
        return Ok(b);
    }
    chameleon::chameleon_transform(Namespace(tns), &mut b);
    Ok(b)
}

pub fn load_types(
    xsd_paths: &[PathBuf],
    qnames: QNameCollector,
) -> Result<(types::Types, QNameCollection), Box<dyn std::error::Error>> {
    let model = xsd2schema_components_qnames(xsd_paths, qnames, None, None)?.components;
    let types = types::Types::from_model_types(model.types);
    let qnames = model.qnames;
    Ok((types, qnames))
}

struct TreeToSchema {
    prefix: String,
    ns: String,
    qnames: QNameCollection,
    schemas: Vec<e::Schema>,
    rev_prefixes: HashMap<std::string::String, std::string::String>,
}

fn tree_to_schema(
    input_file: &Path,
    tree: Tree<Atomic>,
    validate: bool,
) -> std::result::Result<TreeToSchema, Box<dyn std::error::Error>> {
    let root = Node::root(Arc::new(tree));
    let document_element = parse::next_element_sibling(root.first_child()).unwrap();
    let mut prefixes = HashMap::new();
    let mut rev_prefixes = HashMap::new();
    for n in document_element.namespaces() {
        prefixes.insert(n.namespace().to_string(), n.prefix().to_string());
        rev_prefixes.insert(n.prefix().to_string(), n.namespace().to_string());
    }
    let schema = e::Schema::from_document_element(document_element, validate)
        .map_err(|e| format!("Cannot parse {}: {}.", input_file.display(), e))?;
    let ns = schema.1.target_namespace.clone().unwrap_or_default();
    Ok(TreeToSchema {
        prefix: prefixes
            .get(ns.as_str())
            .map(|p| p.to_string())
            .unwrap_or_else(|| "prefix".to_string()),
        ns,
        qnames: root.qnames().clone(),
        schemas: vec![schema.1],
        rev_prefixes,
    })
}
