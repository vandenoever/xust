pub(crate) mod load;

use crate::{
    atomic::{atomic_value::AtomicValue, par_ser::PrimitiveParSer},
    facets::{self, pattern::Pattern, ExplicitTimezone, Facets, Fixed, WhiteSpace},
    identity_constraint_grammar::{parse_field_xpath, parse_selector_xpath, Field, Selector},
    parse::MaxOccurs,
    qname_parser::QNameParser,
    schema_components::{
        identity_constraints::{IdentityConstraintId, IdentityConstraints},
        Attribute, AttributeDeclaration, ComplexType, ComplexTypeFinal, Compositor, ContentType,
        DerivationMethod, ElementDeclaration, IdentityConstraint, IdentityConstraintCategory, List,
        ModelGroup, ModelGroupId, ModelGroups, NamespaceConstraints, NamespaceConstraintsVariety,
        Particle, ProcessContents, SchemaComponents, SimpleType, SimpleTypeFinal,
        SimpleTypeVariety, Term, Type, TypeDef, Types, Wildcard, WildcardId, Wildcards, XS_NS,
    },
    types::{
        built_ins::{
            built_ins, XS_ANY_ATOMIC_TYPE, XS_ANY_SIMPLE_TYPE, XS_ANY_TYPE, XS_DECIMAL, XS_ENTITY,
            XS_IDREF, XS_NMTOKEN, XS_STRING,
        },
        parse_all, TypeId,
    },
    xsd::xs::{ct, e, g, local},
};
use std::str::FromStr;
use std::sync::Arc;
use std::{
    collections::{btree_map::Entry, BTreeMap, BTreeSet, HashMap},
    fmt::{Display, Formatter},
};
use xust_tree::qnames::{
    NCName, Namespace, Prefix, QName, QNameCollection, QNameCollector, QNameId,
};

use self::load::{PrefixesAndSchemaComponents, Schemas};

type Result<T> = std::result::Result<T, String>;

#[derive(Default)]
struct ModelCollector {
    qnames: QNameCollection,
    substitution_groups: BTreeMap<QNameId, ModelGroupId>,
    // types that an element gets from its first substituonGroup, to be
    // enacted when all elements have been parsed
    substitution_group_types: Vec<(usize, usize)>,
    elements: Vec<ElementDeclaration>,
    attributes: BTreeMap<QNameId, AttributeDeclaration>,
    undefined_types: BTreeSet<QNameId>,
    types_under_construction: BTreeSet<QNameId>,
    types: Types,
    groups: ModelGroups,
    wildcards: Wildcards,
    simple_type_dependencies: BTreeMap<QNameId, QNameId>,
    identity_constraints: IdentityConstraints,
}

#[derive(Clone, Copy, PartialEq, Debug)]
enum Form {
    Qualified,
    Unqualified,
}

impl Display for Form {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Self::Qualified => write!(f, "qualified"),
            Self::Unqualified => write!(f, "unqualified"),
        }
    }
}

impl FromStr for Form {
    type Err = String;
    fn from_str(s: &str) -> Result<Self> {
        match s {
            "qualified" => Ok(Self::Qualified),
            "unqualified" => Ok(Self::Unqualified),
            s => Err(format!("Value '{}' is not allowed for attribute form.", s)),
        }
    }
}

struct Global<'a> {
    prefix: String,
    namespace: String,
    qnames: &'a mut QNameCollector,
    qname_parser: &'a QNameParser,
    element_form_default: Form,
    attribute_form_default: Form,
    schema: &'a e::Schema,
    prefixes: &'a HashMap<String, String>,
    schemas: &'a BTreeMap<String, Vec<e::Schema>>,
    mc: ModelCollector,
}

impl<'a> Global<'a> {
    fn add(&mut self, qname: &QName) -> QNameId {
        self.qnames.add_qname_by_ref(qname.as_ref())
    }
    fn qname(&mut self, name: &str) -> QNameId {
        self.qnames.add_qname(
            Prefix(&self.prefix),
            Namespace(&self.namespace),
            NCName(name),
        )
    }
    fn resolve_attribute_name(&mut self, name: &str, form: &Option<String>) -> Result<QNameId> {
        resolve_qname(
            self.qnames,
            &self.prefix,
            &self.namespace,
            name,
            form,
            self.attribute_form_default,
        )
    }
    fn resolve_element_name(&mut self, name: &str, form: &Option<String>) -> Result<QNameId> {
        resolve_qname(
            self.qnames,
            &self.prefix,
            &self.namespace,
            name,
            form,
            self.element_form_default,
        )
    }
}

fn resolve_qname(
    qnames: &mut QNameCollector,
    prefix: &str,
    namespace: &str,
    name: &str,
    form: &Option<String>,
    form_default: Form,
) -> Result<QNameId> {
    let form = if let Some(form) = form {
        FromStr::from_str(form)?
    } else {
        form_default
    };
    Ok(if form == Form::Qualified {
        qnames.add_qname(Prefix(prefix), Namespace(namespace), NCName(name))
    } else {
        qnames.add_local_qname(NCName(name))
    })
}

fn xs_name(qnames: &mut QNameCollector, local_name: &str) -> QNameId {
    qnames.add_qname(Prefix("xs"), Namespace(XS_NS), NCName(local_name))
}

// add xs:anyType to the default types
fn add_any_type(
    qnames: &mut QNameCollector,
    types: &mut Types,
    groups: &mut ModelGroups,
) -> TypeId {
    let group = ModelGroup {
        name: Some(xs_name(qnames, "anyType")),
        compositor: Compositor::Sequence,
        particles: vec![Particle {
            term: Term::Any(WildcardId::any_type()),
            min_occurs: 0,
            max_occurs: MaxOccurs::Unbounded,
        }],
        mixed: true,
    };
    let ct = ComplexType {
        r#final: ComplexTypeFinal::default(),
        derivation_method: DerivationMethod::Restriction,
        r#abstract: false,
        attribute_uses: Vec::new(),
        attribute_wildcard: Some(WildcardId::any_type()),
        content_type: ContentType::ComplexType {
            mixed: true,
            particle: Particle {
                term: Term::Group(groups.push(group)),
                min_occurs: 1,
                max_occurs: MaxOccurs::Bounded(1),
            },
        },
    };
    types.push(Type {
        name: Some(xs_name(qnames, "anyType")),
        base: TypeId::any_type(),
        def: TypeDef::ComplexType(ct),
    })
}

fn add_built_in_types(qnames: &mut QNameCollector, types: &mut Types, groups: &mut ModelGroups) {
    add_any_type(qnames, types, groups);
    types.push(Type {
        name: Some(xs_name(qnames, "anySimpleType")),
        base: XS_ANY_TYPE,
        def: TypeDef::SimpleType(SimpleType::default()),
    });
    types.push(Type {
        name: Some(xs_name(qnames, "anyAtomicType")),
        base: XS_ANY_SIMPLE_TYPE,
        def: TypeDef::SimpleType(SimpleType::default()),
    });
    add_primitive_types(qnames, types);
}

fn add_primitive_types(qnames: &mut QNameCollector, types: &mut Types) {
    for (local_name, base, par_ser, facets) in built_ins().iter() {
        let base = if let Some(base) = base {
            *base
        } else {
            XS_ANY_ATOMIC_TYPE
        };
        types.push(Type {
            name: Some(xs_name(qnames, local_name)),
            base,
            def: TypeDef::SimpleType(SimpleType {
                r#final: SimpleTypeFinal::default(),
                variety: SimpleTypeVariety::Atomic(*par_ser),
                facets: facets(),
            }),
        });
    }
    add_built_in_list_types(qnames, types);
}
fn add_built_in_list_types(qnames: &mut QNameCollector, types: &mut Types) {
    // IDREFS, ENTITIES, NMTOKENS
    types.push(Type {
        name: Some(xs_name(qnames, "IDREFS")),
        base: XS_ANY_SIMPLE_TYPE,
        def: TypeDef::SimpleType(SimpleType {
            r#final: SimpleTypeFinal::default(),
            variety: SimpleTypeVariety::List(List {
                item_type: XS_IDREF,
            }),
            facets: Facets::non_empty(),
        }),
    });
    types.push(Type {
        name: Some(xs_name(qnames, "ENTITIES")),
        base: XS_ANY_SIMPLE_TYPE,
        def: TypeDef::SimpleType(SimpleType {
            r#final: SimpleTypeFinal::default(),
            variety: SimpleTypeVariety::List(List {
                item_type: XS_ENTITY,
            }),
            facets: Facets::non_empty(),
        }),
    });
    types.push(Type {
        name: Some(xs_name(qnames, "NMTOKENS")),
        base: XS_ANY_SIMPLE_TYPE,
        def: TypeDef::SimpleType(SimpleType {
            r#final: SimpleTypeFinal::default(),
            variety: SimpleTypeVariety::List(List {
                item_type: XS_NMTOKEN,
            }),
            facets: Facets::non_empty(),
        }),
    });
}

impl ModelCollector {
    fn new(qnames: &mut QNameCollector) -> Self {
        let mut types = Types::new();
        let mut groups = ModelGroups::new();
        add_built_in_types(qnames, &mut types, &mut groups);
        Self {
            qnames: qnames.qname_collection(),
            substitution_groups: BTreeMap::new(),
            substitution_group_types: Vec::new(),
            undefined_types: BTreeSet::new(),
            types_under_construction: BTreeSet::new(),
            elements: Vec::new(),
            attributes: BTreeMap::new(),
            types,
            groups,
            wildcards: Wildcards::new(),
            simple_type_dependencies: BTreeMap::new(),
            identity_constraints: IdentityConstraints::new(),
        }
    }
    fn declare_simple_type(&mut self, simple_type: QNameId, dep: Option<QNameId>) {
        if let Some(dep) = dep {
            self.simple_type_dependencies.insert(simple_type, dep);
        }
        self.undefined_types.insert(simple_type);
        self.types.push(Type {
            name: Some(simple_type),
            base: XS_ANY_SIMPLE_TYPE,
            def: TypeDef::SimpleType(SimpleType::default()),
        });
    }
    fn declare_complex_type(&mut self, complex_type: QNameId) {
        self.undefined_types.insert(complex_type);
        let mut t = self.types[TypeId::any_type()].clone();
        t.name = Some(complex_type);
        self.types.push(t);
    }
    #[must_use]
    fn sort_types_by_dependency(&mut self) -> bool {
        self.types
            .sort_by_depencency(&self.simple_type_dependencies)
    }
    fn declare_group(&mut self, group: QNameId) {
        self.groups.push(ModelGroup {
            name: Some(group),
            compositor: Compositor::Choice,
            particles: Vec::new(),
            mixed: false,
        });
    }
    fn declare_attribute(&mut self, name: QNameId, simple_type: TypeId) -> Result<()> {
        if self.attributes.contains_key(&name) {
            return Err(format!("{} was already declared.", self.qnames.get(name)));
        }
        self.attributes.insert(
            name,
            AttributeDeclaration {
                content: simple_type,
                r#default: None,
                fixed: None,
            },
        );
        Ok(())
    }
    fn define_attribute(
        &mut self,
        name: QNameId,
        simple_type: TypeId,
        fixed: &Option<String>,
        r#default: &Option<String>,
    ) {
        assert_eq!(XS_ANY_SIMPLE_TYPE, self.attributes[&name].content);
        self.attributes.insert(
            name,
            AttributeDeclaration {
                content: simple_type,
                fixed: fixed.clone(),
                r#default: r#default.clone(),
            },
        );
    }
    fn declare_substitution_group(&mut self, element: QNameId) {
        if let Entry::Vacant(entry) = self.substitution_groups.entry(element) {
            let group = self.groups.push(ModelGroup {
                compositor: Compositor::Choice,
                particles: Vec::new(),
                // do not use element name in a group namespace
                name: None,
                mixed: false,
            });
            entry.insert(group);
        }
    }
    fn add_substitution_group_instance(
        &mut self,
        name: String,
        substitution_group: QNameId,
        term: Term,
    ) -> Result<()> {
        let substitution_group =
            if let Some(substitution_group) = self.substitution_groups.get(&substitution_group) {
                substitution_group
            } else {
                return Err(format!("Substitution group {} was not found.", name));
            };
        self.groups[*substitution_group].particles.push(Particle {
            term,
            min_occurs: 1,
            max_occurs: MaxOccurs::Bounded(1),
        });
        Ok(())
    }
    fn add_substitution_group_element(
        &mut self,
        name: String,
        substitution_group: QNameId,
        instance_tag: QNameId,
    ) -> Result<()> {
        let pos = if let Some(pos) = self.elements.iter().position(|e| e.tag == instance_tag) {
            pos
        } else {
            return Err(format!(
                "Content for substitution group {} with instance {} was not found.",
                self.qnames.get(substitution_group),
                self.qnames.get(instance_tag)
            ));
        };
        self.add_substitution_group_instance(name, substitution_group, Term::TopLevelElement(pos))
    }
    fn add_substitution_group_group(
        &mut self,
        name: String,
        substitution_group: QNameId,
        group: QNameId,
    ) -> Result<()> {
        let group = if let Some(group) = self.substitution_groups.get(&group) {
            *group
        } else {
            return Err(format!(
                "Content for substitution group {} with instance {} was not found.",
                self.qnames.get(substitution_group),
                self.qnames.get(group)
            ));
        };
        self.add_substitution_group_instance(name, substitution_group, Term::Group(group))
    }
    fn declare_element(
        &mut self,
        element: QNameId,
        r#type: Option<QNameId>,
        r#default: Option<String>,
        fixed: Option<String>,
    ) -> Result<()> {
        let content = if let Some(r#type) = r#type {
            if let Some(r#type) = self
                .types
                .iter()
                .find(|(_, t)| t.has_name(r#type))
                .map(|t| t.0)
            {
                r#type
            } else {
                return Err(format!(
                    "Cannot find type named {}.",
                    self.qnames.get(r#type)
                ));
            }
        } else {
            TypeId::any_type()
        };
        self.elements.push(ElementDeclaration {
            tag: element,
            content,
            identity_constraints: Vec::new(),
            r#default,
            fixed,
        });
        Ok(())
    }
    fn define_element(
        &mut self,
        element: QNameId,
        r#type: TypeId,
        identity_constraints: Vec<IdentityConstraintId>,
    ) {
        let ed = self
            .elements
            .iter_mut()
            .find(|ed| ed.tag == element)
            .unwrap();
        ed.content = r#type;
        ed.identity_constraints = identity_constraints;
    }
    fn define_simple_type(&mut self, simple_type: Type) -> Result<TypeId> {
        assert!(matches!(simple_type.def, TypeDef::SimpleType(_)));
        assert!(simple_type.name.is_some());
        self.undefined_types.remove(&simple_type.name.unwrap());
        // if the type has already been declared, add it at the reserved position
        let id = self
            .types
            .iter()
            .find(|(_, t)| match &t.def {
                TypeDef::SimpleType(_i) => t.name == simple_type.name,
                _ => false,
            })
            .map(|(id, _)| id);
        if let Some(id) = id {
            self.types[id] = simple_type;
            Ok(id)
        } else {
            Err(format!(
                "Simple type with name {} was not declared.",
                self.qnames.get(simple_type.name.unwrap())
            ))
        }
    }
    fn get_element_pos(&self, tag: QNameId) -> Option<usize> {
        self.elements.iter().position(|ed| ed.tag == tag)
    }
    fn define_complex_type(&mut self, complex_type: Type) -> Result<TypeId> {
        assert!(matches!(complex_type.def, TypeDef::ComplexType(_)));
        assert!(complex_type.name.is_some());
        self.undefined_types.remove(&complex_type.name.unwrap());
        // the type has already been declared, add it at the reserved position
        let id = self
            .types
            .iter()
            .find(|(_, t)| match &t.def {
                TypeDef::ComplexType(_i) => t.name == complex_type.name,
                _ => false,
            })
            .map(|(id, _)| id);
        if let Some(id) = id {
            self.types[id] = complex_type;
            Ok(id)
        } else {
            Err(format!(
                "Complex type with name {} was not declared.",
                self.qnames.get(complex_type.name.unwrap())
            ))
        }
    }
    fn add_simple_type(&mut self, simple_type: Type) -> TypeId {
        assert!(matches!(simple_type.def, TypeDef::SimpleType(_)));
        assert!(simple_type.name.is_none());
        if let Some((id, _)) = self.types.iter().find(|(_id, t)| *t == &simple_type) {
            return id;
        }
        self.types.push(simple_type)
    }
    fn add_complex_type(&mut self, complex_type: Type) -> TypeId {
        assert!(matches!(complex_type.def, TypeDef::ComplexType(_)));
        assert!(complex_type.name.is_none());
        if let Some((id, _)) = self.types.iter().find(|(_id, t)| *t == &complex_type) {
            return id;
        }
        self.types.push(complex_type)
    }
    fn get_type_id(&mut self, id: QNameId, qname: &QName) -> Result<TypeId> {
        if let Some((id, _)) = self.types.iter().find(|(_, t)| t.has_name(id)) {
            return Ok(id);
        }
        Err(format!("Type {} was not declared.", qname))
    }
    fn get_type(&mut self, id: QNameId) -> Result<Option<(TypeId, &Type)>> {
        if self.undefined_types.contains(&id) {
            Ok(None)
        } else if let Some((id, t)) = self.types.iter().find(|(_, t)| t.has_name(id)) {
            Ok(Some((id, t)))
        } else {
            Err(format!("Type {} was not declared.", self.qnames.get(id)))
        }
    }
    fn define_group(&mut self, group: ModelGroup) -> Result<()> {
        assert!(group.name.is_some());
        // if the group has already been declared, add it at the reserved position
        let id = if let Some((id, _)) = self.groups.iter().find(|(_, i)| i.name == group.name) {
            id
        } else {
            return Err(format!(
                "Group with name {} was not declared.",
                self.qnames.get(group.name.unwrap())
            ));
        };
        self.groups[id] = group;
        Ok(())
    }
    fn add_group(&mut self, group: ModelGroup) -> ModelGroupId {
        assert!(group.name.is_none());
        if let Some((id, _)) = self.groups.iter().find(|(_id, g)| *g == &group) {
            return id;
        }
        self.groups.push(group)
    }
    fn get_group(&mut self, id: QNameId) -> Result<ModelGroupId> {
        if let Some((id, _)) = self.groups.iter().find(|(_, t)| t.name == Some(id)) {
            return Ok(id);
        }
        Err(format!("Group {} was not declared.", self.qnames.get(id)))
    }
    fn add_wildcard(&mut self, wildcard: Wildcard) -> WildcardId {
        self.wildcards.add(wildcard)
    }
    fn add_identity_constraint(&mut self, ic: IdentityConstraint) -> Result<IdentityConstraintId> {
        if let Some((id, i)) = self
            .identity_constraints
            .iter_mut()
            .find(|i| i.1.name == ic.name)
        {
            // if fields is empty, the ic was only referenced, but not yet
            // defined
            if i.fields.is_empty() {
                // if fields_xpath is not empty, the discriminant should not be Keyref, otherwise
                // it must match exactly
                if (!i.fields_xpath.is_empty() && !i.is_keyref())
                    || std::mem::discriminant(&i.category) == std::mem::discriminant(&ic.category)
                {
                    *i = ic;
                } else {
                    return Err(format!(
                        "Referred Identity Constraint {} is has different category than {}.",
                        self.qnames.get(i.name),
                        self.qnames.get(ic.name)
                    ));
                }
                return Ok(id);
            } else {
                return Err(format!(
                    "Identity Constraint {} is already defined.",
                    self.qnames.get(ic.name)
                ));
            }
        }
        Ok(self.identity_constraints.push(ic))
    }
    // get the identity constraint for a particular qname
    fn get_identity_constraint(
        &mut self,
        name: QNameId,
        category: IdentityConstraintCategory,
    ) -> Result<IdentityConstraintId> {
        if let Some((id, i)) = self
            .identity_constraints
            .iter_mut()
            .find(|i| i.1.name == name)
        {
            if std::mem::discriminant(&i.category) == std::mem::discriminant(&category) {
                return Ok(id);
            } else {
                return Err(format!(
                    "Defined identity constraint {} has a different category: {:?} vs {:?}.",
                    self.qnames.get(name),
                    i.category,
                    category
                ));
            }
        }
        Ok(self.identity_constraints.push(IdentityConstraint {
            name,
            category,
            selector: Selector::default(),
            selector_xpath: String::new(),
            fields: Vec::new(),
            fields_xpath: Vec::new(),
        }))
    }
    fn get_identity_constraint_for_keyref(
        &mut self,
        name: QNameId,
    ) -> Result<IdentityConstraintId> {
        if let Some((id, i)) = self
            .identity_constraints
            .iter_mut()
            .find(|i| i.1.name == name)
        {
            if !i.is_keyref() {
                return Ok(id);
            } else {
                return Err(format!(
                    "Defined identity constraint {} has keyref category.",
                    self.qnames.get(name),
                ));
            }
        }
        Ok(self.identity_constraints.push(IdentityConstraint {
            name,
            category: IdentityConstraintCategory::Key,
            selector: Selector::default(),
            selector_xpath: String::new(),
            fields: Vec::new(),
            // special value to indicate that this was referenced from a
            // keyref and category can be either key or unique
            fields_xpath: vec![String::new()],
        }))
    }
}

fn parse_particles(
    global: &mut Global,
    parent: Option<&str>,
    nested_particle: &[g::NestedParticle],
) -> Result<Vec<Particle>> {
    let mut particles = Vec::new();
    for p in nested_particle {
        if let Some(particle) = parse_particle(global, parent, p)? {
            particles.push(particle);
        }
    }
    Ok(particles)
}

// get the base type or item type for the simple type
fn get_required_type(st: &ct::TopLevelSimpleType) -> Option<QName> {
    match st.simple_derivation.first() {
        Some(g::SimpleDerivation::Restriction(r)) => r.base.clone(),
        Some(g::SimpleDerivation::List(l)) => l.item_type.clone(),
        Some(g::SimpleDerivation::Union(_u)) => None, // optional todo
        None => None,
    }
}

fn set_fixed(facet: Fixed, f: bool, fixed: &mut Fixed) -> Result<()> {
    if f {
        fixed.insert(facet);
    } else if fixed.contains(facet) {
        return Err(format!("Cannot unset fixed for {:?}.", facet));
    }
    Ok(())
}

fn parse_facet<'a>(
    global: &Global,
    parser: Option<&PrimitiveParSer>,
    base_facets: &Facets,
    g14: &'a local::SimpleRestrictionModel1,
    patterns: &mut Vec<&'a str>,
    facets: &mut Facets,
) -> Result<()> {
    match g14 {
        local::SimpleRestrictionModel1::Pattern(p) => {
            patterns.push(p.value.as_str());
        }
        local::SimpleRestrictionModel1::Enumeration(e) => {
            let value = parse_atomic(parser, &e.value, base_facets, global.qname_parser)?;
            facets.enumeration.push(value);
        }
        local::SimpleRestrictionModel1::MinInclusive(e) => {
            set_fixed(Fixed::MIN_INCLUSIVE, e.fixed, &mut facets.fixed)?;
            let value = parse_atomic(parser, &e.value, base_facets, global.qname_parser)?;
            facets.min_inclusive = Some(value);
        }
        local::SimpleRestrictionModel1::MaxInclusive(e) => {
            set_fixed(Fixed::MAX_INCLUSIVE, e.fixed, &mut facets.fixed)?;
            let value = parse_atomic(parser, &e.value, base_facets, global.qname_parser)?;
            facets.max_inclusive = Some(value);
        }
        local::SimpleRestrictionModel1::MinExclusive(e) => {
            set_fixed(Fixed::MIN_EXCLUSIVE, e.fixed, &mut facets.fixed)?;
            let value = parse_atomic(parser, &e.value, &facets::EMPTY, global.qname_parser)?;
            facets.min_exclusive = Some(value);
        }
        local::SimpleRestrictionModel1::MaxExclusive(e) => {
            set_fixed(Fixed::MAX_EXCLUSIVE, e.fixed, &mut facets.fixed)?;
            let value = parse_atomic(parser, &e.value, &facets::EMPTY, global.qname_parser)?;
            facets.max_exclusive = Some(value);
        }
        local::SimpleRestrictionModel1::TotalDigits(e) => {
            set_fixed(Fixed::TOTAL_DIGITS, e.fixed, &mut facets.fixed)?;
            if e.value == 0 {
                return Err("Value 0 for totalDigits is not allowed.".into());
            }
            facets.total_digits = Some(e.value as u32);
        }
        local::SimpleRestrictionModel1::FractionDigits(e) => {
            set_fixed(Fixed::FRACTION_DIGITS, e.fixed, &mut facets.fixed)?;
            facets.fraction_digits = Some(e.value as u32);
        }
        local::SimpleRestrictionModel1::WhiteSpace(e) => {
            set_fixed(Fixed::WHITE_SPACE, e.fixed, &mut facets.fixed)?;
            let mut value = match e.value.as_str() {
                "preserve" => WhiteSpace::Preserve,
                "replace" => WhiteSpace::Replace,
                "collapse" => WhiteSpace::Collapse,
                _ => return Err(format!("Invalid whitespace value: '{}'.", e.value)),
            };
            if value == WhiteSpace::Collapse && facets.white_space == WhiteSpace::Trim {
                value = WhiteSpace::Trim;
            }
            if base_facets.fixed.contains(Fixed::WHITE_SPACE) && value != base_facets.white_space {
                return Err(format!(
                    "Cannot set whiteSpace to {:?}, it is fixed to {:?}.",
                    value, base_facets.white_space
                ));
            }
            if (value == WhiteSpace::Preserve && facets.white_space != WhiteSpace::Preserve)
                || (value == WhiteSpace::Replace
                    && (facets.white_space == WhiteSpace::Collapse
                        || facets.white_space == WhiteSpace::Trim))
            {
                return Err(format!(
                    "Cannot give whiteSpace a larger value space with {:?}.",
                    value
                ));
            }
            facets.white_space = value;
        }
        local::SimpleRestrictionModel1::MinLength(e) => {
            set_fixed(Fixed::MIN_LENGTH, e.fixed, &mut facets.fixed)?;
            if e.value < facets.min_length {
                return Err(format!(
                    "Cannot make min_length smaller than base type: {} vs {}",
                    e.value, facets.min_length
                ));
            } else if let Some(max_length) = facets.max_length {
                if e.value > max_length {
                    return Err(format!(
                        "Cannot make minLength larger than maxLength: {} vs {}",
                        e.value, max_length
                    ));
                }
            }
            if base_facets.fixed.contains(Fixed::MIN_LENGTH) && e.value != facets.min_length {
                return Err(format!(
                    "minLength is fixed. Change from {} to {} is not allowed.",
                    facets.min_length, e.value
                ));
            }
            facets.min_length = e.value;
        }
        local::SimpleRestrictionModel1::MaxLength(e) => {
            set_fixed(Fixed::MAX_LENGTH, e.fixed, &mut facets.fixed)?;
            if e.value < facets.min_length {
                return Err(format!(
                    "Cannot make maxLength smaller than minLength: {} vs {}",
                    e.value, facets.min_length
                ));
            } else if let Some(max_length) = facets.max_length {
                if e.value > max_length {
                    return Err(format!(
                        "Cannot make maxLength larger than base type: {} vs {}",
                        e.value, max_length
                    ));
                }
            }
            if base_facets.fixed.contains(Fixed::MAX_LENGTH) && Some(e.value) != facets.max_length {
                return Err(format!(
                    "maxLength is fixed. Change from {:?} to {} is not allowed.",
                    facets.max_length, e.value
                ));
            }
            facets.max_length = Some(e.value);
        }
        local::SimpleRestrictionModel1::Length(e) => {
            set_fixed(Fixed::LENGTH, e.fixed, &mut facets.fixed)?;
            if base_facets.fixed.contains(Fixed::LENGTH) && e.value != facets.min_length {
                return Err(format!(
                    "length is fixed. Change from {:?} to {} is not allowed.",
                    facets.min_length, e.value
                ));
            }
            facets.min_length = e.value;
            facets.max_length = Some(e.value);
        }
        local::SimpleRestrictionModel1::ExplicitTimezone(e) => {
            set_fixed(Fixed::EXPLICIT_TIMEZONE, e.fixed, &mut facets.fixed)?;
            facets.explicit_timezone = Some(match e.value.as_str() {
                "required" => ExplicitTimezone::Required,
                "prohibited" => ExplicitTimezone::Prohibited,
                "optional" => ExplicitTimezone::Optional,
                _ => return Err(format!("Invalid explicitTimezone value: '{}'.", e.value)),
            })
        }
        local::SimpleRestrictionModel1::Assertion(_) => {}
    }
    Ok(())
}

fn parse_simple_type_restriction(
    global: &mut Global,
    r: &e::Restriction,
    r#final: SimpleTypeFinal,
) -> Result<Type> {
    let base = if let Some(base) = &r.base {
        get_type(global, base)?
    } else if let Some(simple_type) = &r.simple_restriction_model.simple_type {
        let st = parse_simple_type(
            global,
            &simple_type.simple_derivation[0],
            SimpleTypeFinal::default(),
        )?;
        global.mc.add_simple_type(st)
    } else {
        XS_ANY_SIMPLE_TYPE
    };
    let base_type = &global.mc.types[base];
    let base_type = if let TypeDef::SimpleType(st) = &base_type.def {
        if st.r#final.contains(SimpleTypeFinal::RESTRICTION) {
            return Err(format!("{} cannot be restricted.", base));
        }
        st
    } else {
        return Err("Cannot derive simple type from a complex type.".into());
    };
    let mut st = SimpleType {
        r#final,
        variety: base_type.variety.clone(),
        ..Default::default()
    };
    st.facets.fixed = base_type.facets.fixed;
    st.facets.white_space = base_type.facets.white_space;
    st.facets.min_length = base_type.facets.min_length;
    st.facets.max_length = base_type.facets.max_length;
    let parser = if let SimpleTypeVariety::Atomic(parser) = &st.variety {
        Some(parser)
    } else {
        None
    };
    let mut patterns = Vec::new();
    for c in &r.simple_restriction_model.choice {
        match c {
            local::SimpleRestrictionModel::Choice(c) => parse_facet(
                global,
                parser,
                &base_type.facets,
                c,
                &mut patterns,
                &mut st.facets,
            )?,
            local::SimpleRestrictionModel::Any { .. } => {}
        }
    }
    st.facets.patterns = Pattern::derive(&patterns, &base_type.facets.patterns)
        .map_err(|_| format!("Cannot parse pattern: {:?}.", patterns))?;
    if st.facets.max_inclusive.is_some() && st.facets.max_exclusive.is_some() {
        return Err("It is an error for both 'maxInclusive' and 'maxExclusive' to be specified on the same type definition.".into());
    }
    if st.facets.min_inclusive.is_some() && st.facets.min_exclusive.is_some() {
        return Err("It is an error for both 'minInclusive' and 'minExclusive' to be specified on the same type definition.".into());
    }
    if let Some(min_inclusive) = &st.facets.min_inclusive {
        if let Some(max_exclusive) = &st.facets.max_exclusive {
            if min_inclusive >= max_exclusive {
                return Err(format!(
                    "minInclusive must be smaller than maxExclusive. {} vs {}",
                    max_exclusive, min_inclusive
                ));
            }
        } else if let Some(max_inclusive) = &st.facets.max_inclusive {
            if min_inclusive > max_inclusive {
                return Err(format!(
                    "minInclusive must be smaller than or equal to maxInclusive. {} vs {}",
                    max_inclusive, min_inclusive
                ));
            }
        }
    } else if let Some(min_exclusive) = &st.facets.min_exclusive {
        if let Some(max_exclusive) = &st.facets.max_exclusive {
            if min_exclusive >= max_exclusive {
                return Err(format!(
                    "minExclusive must be smaller than or equal to maxExclusive. {} vs {}",
                    max_exclusive, min_exclusive
                ));
            }
        } else if let Some(max_inclusive) = &st.facets.max_inclusive {
            if min_exclusive >= max_inclusive {
                return Err(format!(
                    "minExclusive must be smaller than maxInclusive. {} vs {}",
                    max_inclusive, min_exclusive
                ));
            }
        }
    }
    if let Some(fraction_digits) = st.facets.fraction_digits {
        if fraction_digits > 0 && base != XS_DECIMAL {
            return Err("The facet fraction digits is not allowed on this type.".into());
        }
    }
    if let Some(max) = st.facets.max_length {
        if st.facets.min_length > max {
            return Err(format!(
                "max_length ({}) must be larger than min_length({}).",
                max, st.facets.min_length
            ));
        }
    }
    st.facets.inherit(&base_type.facets)?;
    Ok(Type {
        name: None,
        base,
        def: TypeDef::SimpleType(st),
    })
}

fn parse_simple_type_list(
    global: &mut Global,
    l: &e::List,
    r#final: SimpleTypeFinal,
) -> Result<Type> {
    let item_type = if let Some(item_type) = &l.item_type {
        let id = get_type(global, item_type)?;
        let base_type = &global.mc.types[id];
        if let TypeDef::SimpleType(st) = &base_type.def {
            if st.r#final.contains(SimpleTypeFinal::LIST) {
                return Err(format!("{} cannot be placed in a list.", item_type));
            }
        }
        id
    } else if let Some(item_type) = &l.simple_type {
        let st = parse_simple_type(
            global,
            &item_type.simple_derivation[0],
            SimpleTypeFinal::default(),
        )?;
        global.mc.add_simple_type(st)
    } else {
        return Err("Simple type has no item_type.".into());
    };
    let facets = Facets::default();
    Ok(Type {
        name: None,
        base: XS_ANY_SIMPLE_TYPE,
        def: TypeDef::SimpleType(SimpleType {
            r#final: r#final | SimpleTypeFinal::LIST,
            variety: SimpleTypeVariety::List(List { item_type }),
            facets,
        }),
    })
}

fn parse_simple_type_union(
    global: &mut Global,
    u: &e::Union,
    r#final: SimpleTypeFinal,
) -> Result<Type> {
    let mut union_types = Vec::with_capacity(
        u.member_types.as_ref().map(|v| v.len()).unwrap_or_default() + u.simple_type.len(),
    );
    for member_type in u.member_types.iter().flatten() {
        let id = get_type(global, member_type)?;
        let base_type = &global.mc.types[id];
        if let TypeDef::SimpleType(st) = &base_type.def {
            if st.r#final.contains(SimpleTypeFinal::UNION) {
                return Err(format!("{} cannot be placed in a union.", member_type));
            }
        }
        union_types.push(id);
    }
    for st in &u.simple_type {
        let st = parse_simple_type(global, &st.simple_derivation[0], SimpleTypeFinal::default())?;
        union_types.push(global.mc.add_simple_type(st));
    }
    let facets = Facets::default();
    Ok(Type {
        name: None,
        base: XS_ANY_SIMPLE_TYPE,
        def: TypeDef::SimpleType(SimpleType {
            r#final,
            variety: SimpleTypeVariety::Union(union_types),
            facets,
        }),
    })
}

fn parse_simple_type(
    global: &mut Global,
    sd: &g::SimpleDerivation,
    r#final: SimpleTypeFinal,
) -> Result<Type> {
    match sd {
        g::SimpleDerivation::Restriction(r) => parse_simple_type_restriction(global, r, r#final),
        g::SimpleDerivation::List(l) => parse_simple_type_list(global, l, r#final),
        g::SimpleDerivation::Union(u) => parse_simple_type_union(global, u, r#final),
    }
}

// set a different default namespace when calling f
fn with_default_namespace<R>(
    global: &mut Global,
    ns: &Option<String>,
    f: impl Fn(&mut QNameCollector) -> Result<R>,
) -> Result<R> {
    let nm = global.qnames.start_namespace_context();
    let ns = ns.as_deref().unwrap_or("");
    global
        .qnames
        .define_namespace(Prefix(""), Namespace(ns))
        .map_err(|e| {
            global.qnames.leave_namespace_context(nm);
            e.to_string()
        })?;
    global.qnames.finish_namespace_context();
    let r = f(global.qnames);
    global.qnames.leave_namespace_context(nm);
    r
}

fn parse_key(global: &mut Global, key: &local::Keyref1) -> Result<(Selector, Vec<Field>)> {
    let selector =
        with_default_namespace(global, &key.selector.xpath_default_namespace, |qnames| {
            parse_selector_xpath(qnames, &key.selector.xpath)
        })?;
    if key.field.is_empty() {
        return Err("No fields defined for key.".into());
    }
    let mut fields = Vec::with_capacity(key.field.len());
    for field in &key.field {
        let field = with_default_namespace(global, &field.xpath_default_namespace, |qnames| {
            parse_field_xpath(qnames, &field.xpath)
        })?;
        fields.push(field);
    }
    Ok((selector, fields))
}

fn parse_keybase(
    global: &mut Global,
    kb: &ct::Keybase,
    is_unique: bool,
) -> Result<IdentityConstraintId> {
    let category = if is_unique {
        IdentityConstraintCategory::Unique
    } else {
        IdentityConstraintCategory::Key
    };
    match (&kb.r#ref, &kb.name, &kb.sequence) {
        (None, Some(name), Some(sequence)) => {
            let (selector, fields) = parse_key(global, sequence)?;
            let name = global.qname(name);
            global.mc.add_identity_constraint(IdentityConstraint {
                name,
                category,
                selector,
                selector_xpath: sequence.selector.xpath.clone(),
                fields,
                fields_xpath: sequence.field.iter().map(|f| f.xpath.clone()).collect(),
            })
        }
        (Some(r), _, _) => {
            let r = global.add(r);
            global.mc.get_identity_constraint(r, category)
        }
        _ => Err("keybase is not complete".into()),
    }
}

fn parse_keyref(global: &mut Global, kr: &e::Keyref) -> Result<IdentityConstraintId> {
    match (&kr.r#ref, &kr.name, &kr.sequence, &kr.refer) {
        (None, Some(name), Some(sequence), Some(refer)) => {
            let (selector, fields) = parse_key(global, sequence)?;
            let name = global.qname(name);
            let refer = global.add(refer);
            let refer = global.mc.get_identity_constraint_for_keyref(refer)?;
            global.mc.add_identity_constraint(IdentityConstraint {
                name,
                category: IdentityConstraintCategory::Keyref(refer),
                selector,
                selector_xpath: sequence.selector.xpath.clone(),
                fields,
                fields_xpath: sequence.field.iter().map(|f| f.xpath.clone()).collect(),
            })
        }
        (Some(r), _, _, _) => {
            let r = global.add(r);
            global.mc.get_identity_constraint(
                r,
                IdentityConstraintCategory::Keyref(IdentityConstraintId::default()),
            )
        }
        _ => Err("keyref is not complete".into()),
    }
}

fn parse_identity_constraint(
    global: &mut Global,
    ic: &g::IdentityConstraint,
) -> Result<IdentityConstraintId> {
    match ic {
        g::IdentityConstraint::Unique(keybase) => parse_keybase(global, keybase, true),
        g::IdentityConstraint::Key(keybase) => parse_keybase(global, keybase, false),
        g::IdentityConstraint::Keyref(keyref) => parse_keyref(global, keyref),
    }
}

fn parse_identity_constraints(
    global: &mut Global,
    ics: &[g::IdentityConstraint],
) -> Result<Vec<IdentityConstraintId>> {
    let mut ic = Vec::with_capacity(ics.len());
    for i in ics {
        ic.push(parse_identity_constraint(global, i)?);
    }
    Ok(ic)
}

fn parse_local_element(
    global: &mut Global,
    e: &ct::LocalElement,
    parent: Option<&str>,
) -> Result<Particle> {
    let identity_constraints = parse_identity_constraints(global, &e.identity_constraint)?;
    let p = if let Some(r#ref) = &e.r#ref {
        let qname = global.add(r#ref);
        // check that name and type are not there
        if let Some(name) = &e.name {
            return Err(format!(
                "Element has name {} and ref {} defined simultaneously.",
                name, r#ref
            ));
        }
        if let Some(r#type) = &e.r#type {
            return Err(format!(
                "Element has type {} and ref {} defined simultaneously.",
                r#type, r#ref
            ));
        }
        let term;
        if let Some(choice) = global.mc.substitution_groups.get(&qname) {
            // The element is abstract, so instead of referencing that,
            // reference the group that is a choice between one of the
            // instances of the abstract element.
            term = Term::Group(*choice);
        } else if let Some(pos) = global.mc.elements.iter().position(|ed| ed.tag == qname) {
            term = Term::TopLevelElement(pos);
        } else {
            return Err(format!("Cannot find {}.", r#ref));
        }
        // WORKAROUND: <not/> should go in a box to avoid a
        // recursive type. Here we put it in a vec to avoid that.
        let max_occurs = if e.max_occurs == MaxOccurs::Bounded(1) && parent == Some("not") {
            MaxOccurs::Unbounded
        } else {
            e.max_occurs
        };
        check_min_max(e.min_occurs, max_occurs)?;
        Particle {
            term,
            min_occurs: e.min_occurs,
            max_occurs,
        }
    } else if let Some(r#type) = &e.r#type {
        let name = e.name.as_ref().unwrap();
        let tag = global.resolve_element_name(name, &e.form)?;
        let typeid = global.add(r#type);
        check_min_max(e.min_occurs, e.max_occurs)?;
        Particle {
            term: Term::LocalElement(ElementDeclaration {
                tag,
                content: global.mc.get_type_id(typeid, r#type)?,
                identity_constraints,
                r#default: e.r#default.clone(),
                fixed: e.fixed.clone(),
            }),
            min_occurs: e.min_occurs,
            max_occurs: e.max_occurs,
        }
    } else {
        let name = if let Some(name) = &e.name {
            name.clone()
        } else {
            return Err("Local element has no name.".to_string());
        };
        let tag = global.resolve_element_name(&name, &e.form)?;
        let term;
        match &e.choice {
            Some(local::Element3::SimpleType(st)) => {
                let st = parse_simple_type(
                    global,
                    &st.simple_derivation[0],
                    SimpleTypeFinal::default(),
                )?;
                term = Term::LocalElement(ElementDeclaration {
                    tag,
                    content: global.mc.add_simple_type(st),
                    identity_constraints,
                    r#default: e.r#default.clone(),
                    fixed: e.fixed.clone(),
                });
            }
            Some(local::Element3::ComplexType(ct)) => {
                let mut ct = parse_complex_type_model(
                    global,
                    None,
                    &ct.complex_type_model,
                    ct.mixed.unwrap_or(false),
                    false,
                    ComplexTypeFinal::default(),
                )?;
                ct.name = None;
                term = Term::LocalElement(ElementDeclaration {
                    tag,
                    content: global.mc.add_complex_type(ct),
                    identity_constraints,
                    r#default: e.r#default.clone(),
                    fixed: e.fixed.clone(),
                });
            }
            None => {
                // element derived from xs:anyType
                term = Term::LocalElement(ElementDeclaration {
                    tag,
                    content: TypeId::any_type(),
                    identity_constraints,
                    r#default: e.r#default.clone(),
                    fixed: e.fixed.clone(),
                });
            }
        }
        check_min_max(e.min_occurs, e.max_occurs)?;
        Particle {
            term,
            min_occurs: e.min_occurs,
            max_occurs: e.max_occurs,
        }
    };
    Ok(p)
}

fn check_min_max(min: usize, max: MaxOccurs) -> Result<()> {
    if let MaxOccurs::Bounded(max) = max {
        if max < min {
            return Err(format!("Max ({}) is less than min ({}).", max, min));
        }
    }
    Ok(())
}

fn parse_process_contents(s: &str) -> Result<ProcessContents> {
    match s {
        "strict" => Ok(ProcessContents::Strict),
        "lax" => Ok(ProcessContents::Lax),
        "skip" => Ok(ProcessContents::Skip),
        o => Err(format!("Unknown value for processContents '{}'.", o)),
    }
}

fn parse_namespace_constraints(
    target_namespace: &str,
    namespace: &Option<String>,
    not_namespace: &Option<Vec<String>>,
) -> Result<NamespaceConstraints> {
    let mut namespaces = Vec::new();
    let variety = if let Some(namespace) = namespace {
        if namespace == "##any" {
            NamespaceConstraintsVariety::Any
        } else if namespace == "##other" {
            namespaces.push(String::new());
            namespaces.push(target_namespace.to_string());
            NamespaceConstraintsVariety::Not
        } else {
            namespaces = namespace.split_whitespace().map(String::from).collect();
            NamespaceConstraintsVariety::Enumeration
        }
    } else if let Some(not_namespace) = not_namespace {
        namespaces = not_namespace.clone();
        NamespaceConstraintsVariety::Not
    } else {
        NamespaceConstraintsVariety::Any
    };
    for ns in &mut namespaces {
        if ns == "##targetNamespace" {
            ns.clear();
            ns.push_str(target_namespace);
        }
        if ns == "##local" {
            ns.clear();
        }
    }
    let disallowed = Vec::new();
    Ok(NamespaceConstraints {
        variety,
        namespaces,
        disallowed,
    })
}

fn parse_particle(
    global: &mut Global,
    parent: Option<&str>,
    p: &g::NestedParticle,
) -> Result<Option<Particle>> {
    let particle = match p {
        g::NestedParticle::Element(e) => parse_local_element(global, e, parent)?,
        g::NestedParticle::Group(g) => {
            let name = g.r#ref.to_ref();
            let name = name.local_name().as_str();
            // WORKAROUND: simpleDerivation should go in a box to avoid a
            // recursive type. Here we put it in a vec to avoid that.
            let max_occurs = if name == "simpleDerivation" && g.max_occurs == MaxOccurs::Bounded(1)
            {
                MaxOccurs::Unbounded
            } else {
                g.max_occurs
            };
            let r#type = global.qname(name);
            check_min_max(g.min_occurs, max_occurs)?;
            Particle {
                term: Term::Group(global.mc.get_group(r#type)?),
                min_occurs: g.min_occurs,
                max_occurs,
            }
        }
        g::NestedParticle::Choice(c) => {
            if let Some(group) =
                nested_particles2group(global, None, &c.nested_particle, Compositor::Choice)?
            {
                let id = global.mc.add_group(group);
                check_min_max(c.min_occurs, c.max_occurs)?;
                Particle {
                    term: Term::Group(id),
                    min_occurs: c.min_occurs,
                    max_occurs: c.max_occurs,
                }
            } else {
                return Ok(None);
            }
        }
        g::NestedParticle::Sequence(s) => {
            if let Some(group) =
                nested_particles2group(global, None, &s.nested_particle, Compositor::Sequence)?
            {
                let id = global.mc.add_group(group);
                check_min_max(s.min_occurs, s.max_occurs)?;
                Particle {
                    term: Term::Group(id),
                    min_occurs: s.min_occurs,
                    max_occurs: s.max_occurs,
                }
            } else {
                return Ok(None);
            }
        }
        g::NestedParticle::Any(a) => any2particle(global, a)?,
    };
    Ok(Some(particle))
}

fn any2particle(global: &mut Global, a: &e::Any) -> Result<Particle> {
    let mut wildcard = Wildcard::any_type();
    wildcard.namespace_constraints =
        parse_namespace_constraints(&global.namespace, &a.namespace, &a.not_namespace)?;
    wildcard.process_contents = parse_process_contents(&a.process_contents)?;
    check_min_max(a.min_occurs, a.max_occurs)?;
    let wildcard = global.mc.add_wildcard(wildcard);
    Ok(Particle {
        term: Term::Any(wildcard),
        min_occurs: a.min_occurs,
        max_occurs: a.max_occurs,
    })
}

fn nested_particles2group(
    global: &mut Global,
    id: Option<QNameId>,
    nested_particle: &[g::NestedParticle],
    compositor: Compositor,
) -> Result<Option<ModelGroup>> {
    let particles = parse_particles(global, None, nested_particle)?;
    if particles.is_empty() {
        return Ok(None);
    }
    Ok(Some(ModelGroup {
        name: id,
        compositor,
        particles,
        mixed: false,
    }))
}

fn parsed2group(global: &mut Global, group: &ct::NamedGroup) -> Result<()> {
    let id = global.qname(&group.name);
    let group = match &group.choice {
        local::Group3::Sequence(s) => {
            nested_particles2group(global, Some(id), &s.nested_particle, Compositor::Sequence)?
        }
        local::Group3::Choice(c) => {
            nested_particles2group(global, Some(id), &c.nested_particle, Compositor::Choice)?
        }
        local::Group3::All(a) => Some(all_particles2group(global, Some(id), &a.choice)?),
    };
    if let Some(group) = group {
        global.mc.define_group(group)?;
    }
    Ok(())
}

fn all_particles2group(
    global: &mut Global,
    name: Option<QNameId>,
    all_particles: &[local::AllModel],
) -> Result<ModelGroup> {
    let mut particles = Vec::new();
    for a in all_particles {
        particles.push(match a {
            local::AllModel::Element(e) => parse_local_element(global, e, None)?,
            local::AllModel::Any(a) => any2particle(global, a)?,
            local::AllModel::Group(g) => {
                let name = g.r#ref.to_ref();
                let name = name.local_name().as_str();
                let r#type = global.qname(name);
                check_min_max(
                    g.min_occurs.unwrap_or(1),
                    g.max_occurs.unwrap_or(MaxOccurs::Bounded(1)),
                )?;
                Particle {
                    term: Term::Group(global.mc.get_group(r#type)?),
                    min_occurs: g.min_occurs.unwrap_or(1),
                    max_occurs: g.max_occurs.unwrap_or(MaxOccurs::Bounded(1)),
                }
            }
        });
    }
    Ok(ModelGroup {
        name,
        compositor: Compositor::All,
        particles,
        mixed: false,
    })
}

fn collect_declarations(
    prefixes: &HashMap<String, String>,
    parts: &BTreeMap<String, Vec<e::Schema>>,
    qnames: &mut QNameCollector,
    model_collector: &mut ModelCollector,
) -> Result<()> {
    for (ns, schema) in parts {
        let prefix = Prefix(prefixes.get(ns).unwrap());
        let ns = Namespace(ns);
        for s in schema.iter().flat_map(|s| &s.sequence2) {
            match &s.schema_top {
                g::SchemaTop::Redefinable(g::Redefinable::ComplexType(ct)) => {
                    let qname = qnames.add_qname(prefix, ns, NCName(&ct.name));
                    model_collector.declare_complex_type(qname);
                }
                g::SchemaTop::Redefinable(g::Redefinable::SimpleType(st)) => {
                    let qname = qnames.add_qname(prefix, ns, NCName(&st.name));
                    let dep = get_required_type(st).map(|q| qnames.add_qname_by_ref(q.as_ref()));
                    model_collector.declare_simple_type(qname, dep);
                }
                g::SchemaTop::Redefinable(g::Redefinable::Group(g)) => {
                    let qname = qnames.add_qname(prefix, ns, NCName(&g.name));
                    model_collector.declare_group(qname);
                }
                g::SchemaTop::Attribute(a) => {
                    let qname = qnames.add_qname(prefix, ns, NCName(&a.name));
                    model_collector.declare_attribute(qname, XS_ANY_SIMPLE_TYPE)?;
                }
                g::SchemaTop::Element(_)
                | g::SchemaTop::Notation(_)
                | g::SchemaTop::Redefinable(g::Redefinable::AttributeGroup(_)) => {}
            }
        }
    }
    if !model_collector.sort_types_by_dependency() {
        return Err("There must be a circular dependency.".into());
    }
    // declare elements after types have been declared
    for (ns, schema) in parts {
        let prefix = Prefix(prefixes.get(ns).unwrap());
        let ns = Namespace(ns);
        for s in schema.iter().flat_map(|s| &s.sequence2) {
            if let g::SchemaTop::Element(e) = &s.schema_top {
                let qname = qnames.add_qname(prefix, ns, NCName(&e.name));
                for sg in e.substitution_group.iter().flatten() {
                    let sg = sg.to_ref();
                    let sg = qnames.add_qname(sg.prefix(), sg.namespace(), sg.local_name());
                    model_collector.declare_substitution_group(sg);
                }
                if e.r#abstract {
                    model_collector.declare_substitution_group(qname);
                } else {
                    let r#type = e.r#type.as_ref().map(|t| {
                        let t = t.to_ref();
                        qnames.add_qname(t.prefix(), t.namespace(), t.local_name())
                    });
                    model_collector.declare_element(
                        qname,
                        r#type,
                        e.r#default.clone(),
                        e.fixed.clone(),
                    )?;
                }
            }
        }
    }
    Ok(())
}

fn form_default(form: &str) -> Result<Form> {
    FromStr::from_str(form)
}

fn with_attribute_group_schema<R>(
    global: &mut Global,
    g: &QName,
    f: &mut dyn FnMut(&mut Global, &ct::NamedAttributeGroup) -> Result<R>,
) -> Result<R> {
    let r = g.to_ref();
    let (schema, g) = if let Some(r) = global
        .schemas
        .get(r.namespace().0)
        .iter()
        .flat_map(|a| a.iter())
        .find_map(|schema| {
            for g in &schema.sequence2 {
                if let g::SchemaTop::Redefinable(g::Redefinable::AttributeGroup(g)) = &g.schema_top
                {
                    if g.name == r.local_name().0 {
                        return Some((schema, g));
                    }
                }
            }
            None
        }) {
        r
    } else {
        return Err(format!("Attribute group '{}' was not found.", r));
    };
    with_schema(global, schema, &mut |global| f(global, g))
}

fn with_type_schema<R>(
    global: &mut Global,
    ns: Namespace,
    type_name: &str,
    f: &mut dyn Fn(&mut Global) -> Result<R>,
) -> Result<R> {
    // find a schema that defines a type with the given name
    let schema = if let Some(schema) =
        global
            .schemas
            .get(ns.0)
            .iter()
            .flat_map(|a| a.iter())
            .find(|schema| {
                for s in &schema.sequence2 {
                    if let g::SchemaTop::Redefinable(g::Redefinable::ComplexType(ct)) =
                        &s.schema_top
                    {
                        if ct.name == type_name {
                            return true;
                        }
                    } else if let g::SchemaTop::Redefinable(g::Redefinable::SimpleType(st)) =
                        &s.schema_top
                    {
                        if st.name == type_name {
                            return true;
                        }
                    }
                }
                false
            }) {
        schema
    } else {
        return Err(format!("No type Q{{{}}}{} found.", ns, type_name));
    };
    with_schema(global, schema, &mut |global| f(global))
}

fn with_schema<R>(
    global: &mut Global,
    schema: &e::Schema,
    f: &mut dyn FnMut(&mut Global) -> Result<R>,
) -> Result<R> {
    if std::ptr::eq(global.schema, schema) {
        return f(global);
    }
    let namespace = schema.target_namespace.clone().unwrap_or_default();
    let prefix = if let Some(prefix) = global.prefixes.get(&namespace) {
        prefix.to_string()
    } else {
        String::new()
    };
    let mut tmp_global = Global {
        attribute_form_default: form_default(&schema.attribute_form_default)?,
        element_form_default: form_default(&schema.element_form_default)?,
        namespace,
        prefix,
        prefixes: global.prefixes,
        schema,
        schemas: global.schemas,
        qnames: global.qnames,
        qname_parser: global.qname_parser,
        mc: ModelCollector::default(),
    };
    std::mem::swap(&mut global.mc, &mut tmp_global.mc);
    let r = f(&mut tmp_global);
    std::mem::swap(&mut global.mc, &mut tmp_global.mc);
    r
}

fn parsed2groups<'a>(
    prefix: &str,
    qnames: &'a mut QNameCollector,
    qname_parser: &'a QNameParser,
    schema: &'a e::Schema,
    prefixes: &'a HashMap<String, String>,
    schemas: &'a BTreeMap<String, Vec<e::Schema>>,
    model_collector: ModelCollector,
) -> Result<Global<'a>> {
    let ns = if let Some(target_namespace) = &schema.target_namespace {
        target_namespace.clone()
    } else {
        String::new()
    };
    let mut global = Global {
        prefix: prefix.to_string(),
        namespace: ns,
        attribute_form_default: form_default(&schema.attribute_form_default)?,
        element_form_default: form_default(&schema.element_form_default)?,
        qnames,
        qname_parser,
        schema,
        prefixes,
        schemas,
        mc: model_collector,
    };
    for s in &schema.sequence2 {
        if let g::SchemaTop::Redefinable(g::Redefinable::Group(g)) = &s.schema_top {
            parsed2group(&mut global, g)?;
        }
    }
    Ok(global)
}

fn add_or_replace_att(a: Attribute, atts: &mut Vec<Attribute>) {
    if let Some(old) = atts.iter_mut().find(|att| att.name == a.name) {
        *old = a;
        return;
    }
    atts.push(a);
}

fn parsed2attr(global: &mut Global, att: &ct::Attribute, atts: &mut Vec<Attribute>) -> Result<()> {
    let (qname, r#type, fixed) = if let Some(name) = &att.name {
        (
            global.resolve_attribute_name(name, &att.form)?,
            None,
            att.fixed.clone(),
        )
    } else if let Some(r) = att.r#ref.as_ref() {
        let name = global.add(r);
        if let Some(ad) = global.mc.attributes.get(&name) {
            (
                name,
                Some(ad.content),
                att.fixed
                    .as_ref()
                    .or(ad.fixed.as_ref())
                    .map(|f| f.to_owned()),
            )
        } else {
            return Err(format!("Attribute {} is not defined.", r));
        }
    } else {
        return Err("Attribute is missing either ref or name.".to_string());
    };
    if fixed.is_some() && att.default.is_some() {
        return Err("Cannot use fixed and default attribute at the same time.".to_string());
    }
    let prohibited = att.r#use == "prohibited";
    if prohibited {
        atts.retain(|att| att.name != qname);
        return Ok(());
    }
    let r#type = if let Some(r#type) = &att.r#type {
        let qname = global.add(r#type);
        global.mc.get_type_id(qname, r#type)?
    } else if let Some(st) = &att.simple_type {
        let st = parse_simple_type(global, &st.simple_derivation[0], SimpleTypeFinal::default())?;
        global.mc.add_simple_type(st)
    } else if let Some(r#type) = r#type {
        r#type
    } else {
        XS_STRING
    };
    let required = att.r#use == "required";
    if required && att.default.is_some() {
        return Err("If there is a default, 'use' must be 'optional'.".to_string());
    }
    let a = Attribute {
        name: qname,
        required,
        ad: AttributeDeclaration {
            content: r#type,
            fixed,
            r#default: att.default.clone(),
        },
    };
    add_or_replace_att(a, atts);
    Ok(())
}

fn parsed2attrs(
    global: &mut Global,
    attr_decls: &g::AttrDecls,
    atts: &mut Vec<Attribute>,
    depth: usize,
) -> Result<Option<Wildcard>> {
    if depth > 100 {
        return Err("Nesting too deep in attributeGroup.".to_string());
    }
    let mut attribute_wildcard: Option<Wildcard> = None;
    for a in &attr_decls.choice {
        match a {
            local::AttrDecls::Attribute(a) => {
                parsed2attr(global, a, atts)?;
            }
            local::AttrDecls::AttributeGroup(g) => {
                let aw = with_attribute_group_schema(global, &g.r#ref, &mut |global, g| {
                    parsed2attrs(global, &g.attr_decls, atts, depth + 1)
                })?;
                if let Some(aw) = aw {
                    attribute_wildcard = Some(if let Some(w) = attribute_wildcard {
                        w.intersect(aw)
                    } else {
                        aw
                    });
                }
            }
        }
    }
    if let Some(any) = &attr_decls.any_attribute {
        attribute_wildcard = Some(parse_any_attribute(&global.namespace, any)?);
    }
    Ok(attribute_wildcard)
}

fn parse_any_attribute(namespace: &str, any: &e::AnyAttribute) -> Result<Wildcard> {
    let mut wildcard = Wildcard::any_type();
    wildcard.namespace_constraints =
        parse_namespace_constraints(namespace, &any.namespace, &any.not_namespace)?;
    wildcard.process_contents = parse_process_contents(&any.process_contents)?;
    Ok(wildcard)
}

fn parse_simple_type_final(s: &Option<String>) -> Result<SimpleTypeFinal> {
    let mut f = SimpleTypeFinal::default();
    let s = if let Some(s) = s {
        s
    } else {
        return Ok(f);
    };
    if s == "#all" {
        return Ok(SimpleTypeFinal::all());
    }
    for s in s.split_whitespace() {
        match s {
            "extension" => f |= SimpleTypeFinal::EXTENSION,
            "list" => f |= SimpleTypeFinal::LIST,
            "restriction" => f |= SimpleTypeFinal::RESTRICTION,
            "union" => f |= SimpleTypeFinal::UNION,
            _ => return Err(format!("{} is not valid value for final.", s)),
        }
    }
    Ok(f)
}

fn parse_complex_type_final(s: &Option<String>) -> Result<ComplexTypeFinal> {
    let mut f = ComplexTypeFinal::default();
    let s = if let Some(s) = s {
        s
    } else {
        return Ok(f);
    };
    if s == "#all" {
        return Ok(ComplexTypeFinal::all());
    }
    for s in s.split_whitespace() {
        match s {
            "extension" => f |= ComplexTypeFinal::EXTENSION,
            "restriction" => f |= ComplexTypeFinal::RESTRICTION,
            _ => return Err(format!("{} is not valid value for final.", s)),
        }
    }
    Ok(f)
}

fn parsed2type(global: &mut Global, complex_type: &ct::TopLevelComplexType) -> Result<TypeId> {
    let ct = parse_complex_type_model(
        global,
        Some(&complex_type.name),
        &complex_type.complex_type_model,
        complex_type.mixed.unwrap_or(false),
        complex_type.r#abstract,
        parse_complex_type_final(&complex_type.r#final)?,
    )?;
    global.mc.define_complex_type(ct)
}

fn parsed2types(global: &mut Global) -> Result<()> {
    for s in &global.schema.sequence2 {
        if let g::SchemaTop::Redefinable(g::Redefinable::SimpleType(st)) = &s.schema_top {
            if st.simple_derivation.is_empty() {
                return Err(format!("Simple type {} has no definition.", st.name));
            }
            let r#final = parse_simple_type_final(&st.r#final)?;
            let mut parsed = parse_simple_type(global, &st.simple_derivation[0], r#final)?;
            parsed.name = Some(global.qname(&st.name));
            global.mc.define_simple_type(parsed)?;
        }
        if let g::SchemaTop::Redefinable(g::Redefinable::ComplexType(ct)) = &s.schema_top {
            parsed2type(global, ct)?;
        }
    }
    Ok(())
}

fn parse_type_def_particle(
    global: &mut Global,
    name: Option<&str>,
    type_def_particle: &g::TypeDefParticle,
    particles: &mut Vec<Particle>,
) -> Result<()> {
    match type_def_particle {
        g::TypeDefParticle::Choice(c) => {
            if let Some(group) =
                nested_particles2group(global, None, &c.nested_particle, Compositor::Choice)?
            {
                let id = global.mc.add_group(group);
                check_min_max(c.min_occurs, c.max_occurs)?;
                particles.push(Particle {
                    term: Term::Group(id),
                    min_occurs: c.min_occurs,
                    max_occurs: c.max_occurs,
                });
            }
        }
        g::TypeDefParticle::Group(g) => {
            let r#ref = g.r#ref.to_ref();
            let name = r#ref.local_name().as_str();
            let r#type = global.qname(name);
            check_min_max(g.min_occurs, g.max_occurs)?;
            particles.push(Particle {
                term: Term::Group(global.mc.get_group(r#type)?),
                min_occurs: g.min_occurs,
                max_occurs: g.max_occurs,
            });
        }
        g::TypeDefParticle::All(a) => {
            let group = all_particles2group(global, None, &a.choice)?;
            let g = global.mc.add_group(group);
            check_min_max(a.min_occurs, a.max_occurs)?;
            if a.min_occurs > 1 {
                return Err("In xs:all, minOccurs should be less than 2.".to_string());
            }
            if a.max_occurs > MaxOccurs::Bounded(1) {
                return Err("In xs:all, maxOccurs should be less than 2.".to_string());
            }
            particles.push(Particle {
                term: Term::Group(g),
                min_occurs: a.min_occurs,
                max_occurs: a.max_occurs,
            });
        }
        g::TypeDefParticle::Sequence(s) => {
            if s.min_occurs == 1 && s.max_occurs == MaxOccurs::Bounded(1) {
                let mut p = parse_particles(global, name, &s.nested_particle)?;
                particles.append(&mut p);
            } else if let Some(group) =
                nested_particles2group(global, None, &s.nested_particle, Compositor::Sequence)?
            {
                let id = global.mc.add_group(group);
                check_min_max(s.min_occurs, s.max_occurs)?;
                particles.push(Particle {
                    term: Term::Group(id),
                    min_occurs: s.min_occurs,
                    max_occurs: s.max_occurs,
                })
            }
        }
    }
    Ok(())
}

/// Return true if the group is not needed outside of the element.
pub(crate) fn is_only_used_directly_in_element(
    types: &Types,
    id: ModelGroupId,
    group: &ModelGroup,
) -> bool {
    if group.name.is_some() || group.compositor != Compositor::Sequence {
        return false;
    }
    types
        .particles()
        .filter(|(particle, _)| {
            particle.min_occurs == 1 && particle.max_occurs == MaxOccurs::Bounded(1)
        })
        .any(|(p, _)| p.term == Term::Group(id))
}

/// Get the particles for the ComplexType
/// If the particle has a sequence that occurs only once, use it directly.
pub(crate) fn get_particles(groups: &ModelGroups, ct: &ComplexType) -> Vec<Particle> {
    if let ContentType::ComplexType { particle, .. } = &ct.content_type {
        if particle.min_occurs == 1 && particle.max_occurs == MaxOccurs::Bounded(1) {
            if let Term::Group(groupid) = &particle.term {
                let group = &groups[*groupid];
                if group.compositor == Compositor::Sequence {
                    group.particles.clone()
                } else {
                    vec![particle.clone()]
                }
            } else {
                vec![particle.clone()]
            }
        } else {
            vec![particle.clone()]
        }
    } else {
        Vec::new()
    }
}

fn parse_complex_content(
    global: &mut Global,
    complex_content: &local::ComplexContent1,
    mixed: bool,
) -> Result<Type> {
    let mut attribute_uses;
    let mut attribute_wildcard;
    let mut particles;
    let base_type_definition;
    let derivation_method;
    match complex_content {
        local::ComplexContent1::Restriction(r) => {
            derivation_method = DerivationMethod::Restriction;
            let (baseid, base) = get_complex_type(global, &r.base)?;
            if base.r#final.contains(ComplexTypeFinal::RESTRICTION) {
                return Err(format!("{} cannot be restricted.", r.base));
            }
            base_type_definition = baseid;
            attribute_uses = base.attribute_uses;
            let wildcard = parsed2attrs(global, &r.attr_decls, &mut attribute_uses, 0)?;
            attribute_wildcard = wildcard.map(|w| global.mc.add_wildcard(w));
            particles = vec![];
            match &r.choice {
                Some(local::ComplexRestrictionType1::Sequence(s)) => {
                    parse_type_def_particle(global, None, &s.type_def_particle, &mut particles)?;
                }
                None => {}
            }
        }
        local::ComplexContent1::Extension(e) => {
            derivation_method = DerivationMethod::Extension;
            let (baseid, base) = get_complex_type(global, &e.base)?;
            if base.r#final.contains(ComplexTypeFinal::EXTENSION) {
                return Err(format!("{} cannot be extended.", e.base));
            }
            base_type_definition = baseid;
            particles = get_particles(&global.mc.groups, &base);
            attribute_uses = base.attribute_uses;
            attribute_wildcard = base.attribute_wildcard;
            let aw = parsed2attrs(global, &e.attr_decls, &mut attribute_uses, 0)?;
            let aw = aw.map(|w| global.mc.add_wildcard(w));
            if attribute_wildcard.is_none() {
                attribute_wildcard = aw;
            }
            if let Some(type_def_particle) = &e.type_def_particle {
                parse_type_def_particle(global, None, type_def_particle, &mut particles)?;
            }
        }
    };
    Ok(Type {
        name: None,
        base: base_type_definition,
        def: TypeDef::ComplexType(ComplexType {
            r#final: ComplexTypeFinal::default(),
            derivation_method,
            r#abstract: false,
            attribute_uses,
            attribute_wildcard,
            content_type: particles_to_content_type(global, particles, mixed),
        }),
    })
}

fn get_type(global: &mut Global, qname: &QName) -> Result<TypeId> {
    let qnameid = global.add(qname);
    if let Some((id, _)) = global.mc.get_type(qnameid)? {
        return Ok(id);
    }
    if global.mc.types_under_construction.contains(&qnameid) {
        return Err(format!(
            "Detected cyclic dependency when parsing {}.",
            qname
        ));
    }
    global.mc.types_under_construction.insert(qnameid);
    let qname = qname.to_ref();
    let ns = qname.namespace();
    let name = qname.local_name().as_str();
    with_type_schema(global, ns, name, &mut |global| {
        for s in &global.schema.sequence2 {
            if let g::SchemaTop::Redefinable(g::Redefinable::ComplexType(ct)) = &s.schema_top {
                if ct.name == name {
                    return parsed2type(global, ct);
                }
            } else if let g::SchemaTop::Redefinable(g::Redefinable::SimpleType(st)) = &s.schema_top
            {
                if st.name == name {
                    let r#final = parse_simple_type_final(&st.r#final)?;
                    let mut st = parse_simple_type(global, &st.simple_derivation[0], r#final)?;
                    st.name = Some(qnameid);
                    return global.mc.define_simple_type(st);
                }
            }
        }
        Err(format!("Type {} was not found.", qname))
    })
}

fn get_complex_type(global: &mut Global, qname: &QName) -> Result<(TypeId, ComplexType)> {
    let id = get_type(global, qname)?;
    let t = &global.mc.types[id];
    match &t.def {
        TypeDef::ComplexType(ct) => Ok((id, ct.clone())),
        TypeDef::SimpleType(_) => Err(format!(
            "Found simple type instead of complex type for {}.",
            qname
        )),
    }
}

fn parse_complex_type_model(
    global: &mut Global,
    name: Option<&str>,
    complex_type_model: &g::ComplexTypeModel,
    mixed: bool,
    r#abstract: bool,
    r#final: ComplexTypeFinal,
) -> Result<Type> {
    let qname = name.map(|name| global.qname(name));
    match complex_type_model {
        g::ComplexTypeModel::SimpleContent(st) => {
            let derivation_method;
            let base_type_definition;
            let mut attribute_uses = Vec::new();
            let mut attribute_wildcard = None;
            match &st.choice {
                local::SimpleContent1::Restriction(r) => {
                    derivation_method = DerivationMethod::Restriction;
                    let baseid = get_type(global, &r.base)?;
                    let base = &global.mc.types[baseid];
                    base_type_definition = baseid;
                    if let TypeDef::ComplexType(base) = &base.def {
                        if base.r#final.contains(ComplexTypeFinal::RESTRICTION) {
                            return Err(format!("{} cannot be restricted.", r.base));
                        }
                        attribute_uses = base.attribute_uses.clone();
                    }
                    let wildcard = parsed2attrs(global, &r.attr_decls, &mut attribute_uses, 0)?;
                    attribute_wildcard = wildcard.map(|w| global.mc.add_wildcard(w));
                }
                local::SimpleContent1::Extension(e) => {
                    derivation_method = DerivationMethod::Extension;
                    let baseid = get_type(global, &e.base)?;
                    let base = &global.mc.types[baseid];
                    base_type_definition = baseid;
                    if let TypeDef::ComplexType(base) = &base.def {
                        if base.r#final.contains(ComplexTypeFinal::EXTENSION) {
                            return Err(format!("{} cannot be extended.", e.base));
                        }
                        attribute_uses = base.attribute_uses.clone();
                        attribute_wildcard = base.attribute_wildcard;
                    }
                    let aw = parsed2attrs(global, &e.attr_decls, &mut attribute_uses, 0)?;
                    let aw = aw.map(|w| global.mc.add_wildcard(w));
                    if attribute_wildcard.is_none() {
                        attribute_wildcard = aw;
                    }
                }
            }
            Ok(Type {
                name: qname,
                base: base_type_definition,
                def: TypeDef::ComplexType(ComplexType {
                    r#final,
                    derivation_method,
                    r#abstract,
                    attribute_uses,
                    attribute_wildcard,
                    content_type: ContentType::SimpleType(base_type_definition),
                }),
            })
        }
        g::ComplexTypeModel::ComplexContent(c) => {
            let mut ct = parse_complex_content(global, &c.choice, c.mixed.unwrap_or(mixed))?;
            ct.name = qname;
            Ok(ct)
        }
        g::ComplexTypeModel::Sequence(s) => {
            let mut attribute_uses = Vec::new();
            let attribute_wildcard = parsed2attrs(global, &s.attr_decls, &mut attribute_uses, 0)?;
            let attribute_wildcard = attribute_wildcard.map(|w| global.mc.add_wildcard(w));
            let mut particles = vec![];
            if let Some(particle) = &s.type_def_particle {
                parse_type_def_particle(global, name, particle, &mut particles)?;
            }
            let content_type = particles_to_content_type(global, particles, mixed);
            Ok(Type {
                name: qname,
                base: TypeId::any_type(),
                def: TypeDef::ComplexType(ComplexType {
                    r#final,
                    derivation_method: DerivationMethod::Restriction,
                    r#abstract,
                    attribute_uses,
                    attribute_wildcard,
                    content_type,
                }),
            })
        }
    }
}

fn particles_to_content_type(
    global: &mut Global,
    particles: Vec<Particle>,
    mixed: bool,
) -> ContentType {
    if particles.is_empty() && !mixed {
        return ContentType::Empty;
    }
    let particle = if particles.len() == 1 {
        particles[0].clone()
    } else {
        let group = global.mc.add_group(ModelGroup {
            name: None,
            particles,
            compositor: Compositor::Sequence,
            mixed,
        });
        Particle {
            term: Term::Group(group),
            min_occurs: 1,
            max_occurs: MaxOccurs::Bounded(1),
        }
    };
    ContentType::ComplexType { mixed, particle }
}

fn parsed2attribute(global: &mut Global, att: &ct::TopLevelAttribute) -> Result<()> {
    let qname = global.qname(&att.name);
    let r#type = if let Some(r#type) = &att.r#type {
        let qname = global.add(r#type);
        global.mc.get_type_id(qname, r#type)?
    } else if let Some(st) = &att.simple_type {
        let st = parse_simple_type(global, &st.simple_derivation[0], SimpleTypeFinal::default())?;
        global.mc.add_simple_type(st)
    } else {
        XS_STRING
    };
    if att.default.is_some() && att.fixed.is_some() {
        return Err("Cannot use fixed and default attribute at the same time.".to_string());
    }
    global
        .mc
        .define_attribute(qname, r#type, &att.fixed, &att.r#default);
    Ok(())
}

fn parsed2element(global: &mut Global, element: &ct::TopLevelElement) -> Result<()> {
    let name = global.qname(&element.name);
    let r#abstract = element.r#abstract;
    if r#abstract {
        for sg in element.substitution_group.iter().flatten() {
            let sg_id = global.add(sg);
            global
                .mc
                .add_substitution_group_group(format!("{}", sg), sg_id, name)?;
        }
        return Ok(());
    }
    let mut first_sg = None;
    for sg in element.substitution_group.iter().flatten() {
        let sg_id = global.add(sg);
        global
            .mc
            .add_substitution_group_element(format!("{}", sg), sg_id, name)?;
        if first_sg.is_none() {
            first_sg = Some(sg_id);
        }
    }
    if global.mc.substitution_groups.contains_key(&name) {
        global
            .mc
            .add_substitution_group_element(element.name.clone(), name, name)?;
    }
    let ics = parse_identity_constraints(global, &element.identity_constraint)?;
    if let Some(r#type) = &element.r#type {
        let r#type = global.add(r#type);
        if let Some((r#type, _)) = global.mc.get_type(r#type)? {
            global.mc.define_element(name, r#type, ics);
        }
        return Ok(());
    }
    let type_def = if let Some(type_def) = &element.choice {
        type_def
    } else {
        // if there is a substitutionGroup, take the same type as that
        if let Some(sg) = first_sg.and_then(|sg| global.mc.get_element_pos(sg)) {
            let pos = global.mc.get_element_pos(name).unwrap();
            global.mc.substitution_group_types.push((pos, sg));
        }
        global.mc.define_element(name, TypeId::any_type(), ics);
        return Ok(());
    };
    let r#type = match type_def {
        local::Element3::ComplexType(ct) => {
            let mut complex_type = parse_complex_type_model(
                global,
                Some(&element.name),
                &ct.complex_type_model,
                ct.mixed.unwrap_or(false),
                r#abstract,
                ComplexTypeFinal::default(),
            )?;
            complex_type.name = None;
            global.mc.add_complex_type(complex_type)
        }
        local::Element3::SimpleType(st) => {
            let t =
                parse_simple_type(global, &st.simple_derivation[0], SimpleTypeFinal::default())?;
            global.mc.add_simple_type(t)
        }
    };
    global.mc.define_element(name, r#type, ics);
    Ok(())
}

fn parsed2attributes(global: &mut Global, schema: &e::Schema) -> Result<()> {
    for s in &schema.sequence2 {
        if let g::SchemaTop::Attribute(a) = &s.schema_top {
            parsed2attribute(global, a)?;
        }
    }
    Ok(())
}

fn parsed2elements(global: &mut Global, schema: &e::Schema) -> Result<()> {
    for s in &schema.sequence2 {
        if let g::SchemaTop::Element(e) = &s.schema_top {
            parsed2element(global, e)?;
        }
    }
    Ok(())
}

fn parse_atomic(
    parser: Option<&PrimitiveParSer>,
    v: &str,
    facets: &Facets,
    qname_parser: &QNameParser,
) -> Result<AtomicValue> {
    if let Some(parser) = parser {
        if let Ok(v) = parser.parse(v, facets, qname_parser) {
            Ok(v)
        } else {
            Err(format!("Cannot parse facet value '{}'.", v))
        }
    } else {
        Ok(AtomicValue::String(v.into()))
    }
}

fn get_simple_types(types: &Types) -> Vec<SimpleType> {
    let mut simple_types = Vec::with_capacity(types.len());
    for (_id, t) in types.iter() {
        simple_types.push(match &t.def {
            TypeDef::ComplexType(ec) => {
                if let ContentType::SimpleType(st) = &ec.content_type {
                    if let TypeDef::SimpleType(st) = &types[*st].def {
                        st.clone()
                    } else {
                        SimpleType::default()
                    }
                } else {
                    SimpleType::default()
                }
            }
            TypeDef::SimpleType(ec) => ec.clone(),
        });
    }
    simple_types
}

fn cascade_mixed_group(
    mc: &mut ModelCollector,
    groupid: ModelGroupId,
    mixed: bool,
    stack: &mut Vec<ModelGroupId>,
) -> bool {
    let group = &mut mc.groups[groupid];
    let mut changed = group.mixed != mixed;
    group.mixed = mixed;
    let mut to_change = Vec::new();
    for p in &group.particles {
        if let Term::Group(g) = &p.term {
            to_change.push(*g);
        }
    }
    for g in to_change {
        if !stack.contains(&groupid) {
            stack.push(groupid);
            changed |= cascade_mixed_group(mc, g, mixed, stack);
            stack.pop();
        }
    }
    changed
}

// ModelGroup and ContentType can be mixed. Any group used in a mixed type
// should also be mixed.
fn cascade_mixed_types(mc: &mut ModelCollector) {
    let mut something_changed = true;
    let mut stack = Vec::new();
    while something_changed {
        let mut to_change = Vec::new();
        for (particle, mixed) in mc.types.particles() {
            if let Term::Group(group) = &particle.term {
                to_change.push((*group, mixed));
            }
        }
        for (group, mixed) in to_change {
            something_changed |= cascade_mixed_group(mc, group, mixed, &mut stack);
        }
        // do not loop for now
        something_changed = false;
    }
}

fn parsed2mods(schemas: Schemas) -> Result<PrefixesAndSchemaComponents> {
    let Schemas {
        prefixes,
        schemas,
        mut qnames,
        namespace_definitions,
    } = schemas;
    let mut model_collector = ModelCollector::new(&mut qnames);
    collect_declarations(&prefixes, &schemas, &mut qnames, &mut model_collector)?;
    let mut nds = Vec::new();
    qnames.start_namespace_context();
    for (prefix, ns) in namespace_definitions {
        if let Ok(nd) = qnames.define_namespace(Prefix(&prefix), Namespace(&ns)) {
            nds.push(nd);
        }
    }
    qnames.finish_namespace_context();
    let mut qname_parser = QNameParser::new(qnames.qname_collection());
    qname_parser.push_namespace_definitions(&nds);
    // first convert all the groups
    for (ns, part) in &schemas {
        if let Some(prefix) = prefixes.get(ns) {
            for part in part {
                let mut global = parsed2groups(
                    prefix,
                    &mut qnames,
                    &qname_parser,
                    part,
                    &prefixes,
                    &schemas,
                    model_collector,
                )?;
                parsed2types(&mut global)?;
                parsed2attributes(&mut global, part)?;
                parsed2elements(&mut global, part)?;
                model_collector = global.mc;
            }
        }
    }
    cascade_mixed_types(&mut model_collector);
    // fix all the attribute types
    for t in model_collector.types.type_ids() {
        let r#type = &mut model_collector.types[t];
        if let TypeDef::ComplexType(ct) = &mut r#type.def {
            for a in &mut ct.attribute_uses {
                if let Some(at) = model_collector.attributes.get(&a.name) {
                    a.ad = at.clone();
                }
            }
        }
    }
    // check default and fixed values of elements and attributes
    let simple_types = get_simple_types(&model_collector.types);
    let mut dummy = |_, _| {};
    for e in &model_collector.elements {
        if let Some(r#default) = &e.r#default {
            parse_all(
                &simple_types,
                e.content,
                r#default,
                &mut dummy,
                &qname_parser,
            )
            .map_err(|_| format!("Value '{}' does not match type.", r#default))?;
        }
        if let Some(fixed) = &e.fixed {
            parse_all(&simple_types, e.content, fixed, &mut dummy, &qname_parser)
                .map_err(|_| format!("Value '{}' does not match type.", fixed))?;
        }
    }
    for a in model_collector.attributes.values() {
        if let Some(r#default) = &a.r#default {
            parse_all(
                &simple_types,
                a.content,
                r#default,
                &mut dummy,
                &qname_parser,
            )
            .map_err(|_| format!("Value '{}' does not match type.", r#default))?;
        }
        if let Some(fixed) = &a.fixed {
            parse_all(&simple_types, a.content, fixed, &mut dummy, &qname_parser)
                .map_err(|_| format!("Value '{}' does not match type.", fixed))?;
        }
    }
    for (a, b) in model_collector.substitution_group_types {
        model_collector.elements[a].content = model_collector.elements[b].content;
    }
    let qnames = qnames.collect();
    // set_atomic_types(&mut model_collector.types, &nds, &qnames)?;
    if model_collector
        .identity_constraints
        .iter()
        .any(|i| i.1.fields.is_empty())
    {
        return Err("Not all IC are defined.".into());
    }
    // remove_unused_groups(&mut model_collector);
    Ok(PrefixesAndSchemaComponents {
        prefixes,
        components: SchemaComponents {
            elements: model_collector.elements,
            attributes: model_collector.attributes.into(),
            qnames,
            types: model_collector.types,
            groups: model_collector.groups,
            wildcards: Arc::new(model_collector.wildcards),
            identity_constraints: Arc::new(model_collector.identity_constraints),
            simple_types: simple_types.into(),
        },
    })
}
