//! Idenity constraints xs:key, xs:keyref, and xs:unique are written in
//! a subset of XPath 2.0. This module parses the xpath expressions into
//! structures for the Schema Components.
//!
//!

use nom::{
    bytes::complete::tag,
    character::complete::{char, space0},
    sequence::tuple,
    IResult,
};
use xust_tree::qnames::{NCName, Prefix, QNameCollector, QNameId};
use xust_xml::ncname;

type Result<T> = std::result::Result<T, String>;

#[derive(Debug, Clone, PartialEq, Default)]
pub(crate) struct Selector(pub Vec<SelectorPath>);

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct SelectorPath {
    // true if the path starts with './/'
    pub initial_double_slash: bool,
    pub steps: Vec<NameTest>,
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum NameTest {
    QName(QNameId),
    Any,
    Namespace(QNameId),
}

#[derive(Debug, Clone, PartialEq, Default)]
pub(crate) struct Field(pub Vec<FieldPath>);

#[derive(Debug, Clone, PartialEq)]
pub(crate) struct FieldPath {
    /// true if the path starts with './/'
    pub initial_double_slash: bool,
    pub is_attribute: bool,
    pub steps: Vec<NameTest>,
}

pub(crate) fn parse_selector_xpath(qnames: &mut QNameCollector, xpath: &str) -> Result<Selector> {
    match selector(qnames, xpath) {
        Ok(("", selector)) => Ok(selector),
        _ => Err(format!("Error parsing selector/@xpath '{}'", xpath)),
    }
}

// [1]   	Selector	   ::=   	Path ( '|' Path )*
fn selector<'a>(qnames: &mut QNameCollector, xpath: &'a str) -> IResult<&'a str, Selector> {
    let (i, path) = selector_path(qnames, xpath)?;
    let mut i = space0(i)?.0;
    let mut paths = vec![path];
    while let Ok((j, _)) = tuple::<_, _, (), _>((char('|'), space0))(i) {
        let (j, path) = selector_path(qnames, j)?;
        paths.push(path);
        i = space0(j)?.0;
    }
    Ok((i, Selector(paths)))
}
fn initial_double_slash(i: &str) -> (&str, bool) {
    if let Ok((i, _)) = tuple::<_, _, (), _>((char('.'), space0, tag("//"), space0))(i) {
        (i, true)
    } else {
        (i, false)
    }
}
// [2]   	Path	   ::=   	('.' '//')? Step ( '/' Step )*
fn selector_path<'a>(qnames: &mut QNameCollector, i: &'a str) -> IResult<&'a str, SelectorPath> {
    let (i, (initial_double_slash, steps)) = selector_steps(qnames, i)?;
    Ok((
        i,
        SelectorPath {
            initial_double_slash,
            steps,
        },
    ))
}
fn selector_steps<'a>(
    qnames: &mut QNameCollector,
    i: &'a str,
) -> IResult<&'a str, (bool, Vec<NameTest>)> {
    let (i, initial_double_slash) = initial_double_slash(i);
    let mut steps = Vec::with_capacity(1);
    let (i, step) = step(qnames, i)?;
    if let Some(step) = step {
        steps.push(step);
    }
    let mut i = space0(i)?.0;
    while let Ok((j, _)) = tuple::<_, _, (), _>((char('/'), space0))(i) {
        let (j, step) = self::step(qnames, j)?;
        if let Some(step) = step {
            steps.push(step);
        }
        i = space0(j)?.0;
    }
    Ok((i, (initial_double_slash, steps)))
}
// [3]   	Step	   ::=   	'.' | NameTest
fn step<'a>(qnames: &mut QNameCollector, i: &'a str) -> IResult<&'a str, Option<NameTest>> {
    if let Ok((i, _)) = char::<_, ()>('.')(i) {
        let i = space0(i)?.0;
        return Ok((i, None));
    }
    let (i, name_test) = name_test(qnames, i)?;
    Ok((i, Some(name_test)))
}
// [4]   	NameTest	   ::=   	QName | '*' | NCName ':*'
fn name_test<'a>(qnames: &mut QNameCollector, i: &'a str) -> IResult<&'a str, NameTest> {
    if let Ok((i, _)) = char::<_, ()>('*')(i) {
        let i = space0(i)?.0;
        return Ok((i, NameTest::Any));
    }
    let (i, prefix) = if let Ok((i, (prefix, _))) = tuple((ncname, char(':')))(i) {
        if let Ok((i, _)) = char::<_, ()>('*')(i) {
            return if let Some(ns) = qnames.get_namespace(Prefix(prefix)) {
                Ok((
                    i,
                    NameTest::Namespace(QNameId::from_namespace(ns.namespace)),
                ))
            } else {
                Err(nom::Err::Error(nom::error::make_error(
                    i,
                    nom::error::ErrorKind::Tag,
                )))
            };
        }
        (i, Prefix(prefix))
    } else {
        (i, Prefix(""))
    };
    let (i, local_name) = ncname(i)?;
    let i = space0(i)?.0;
    let qname = qnames
        .add_qname_by_prefix(prefix, NCName(local_name))
        .map_err(|_| nom::Err::Error(nom::error::make_error(i, nom::error::ErrorKind::Tag)))?;
    Ok((i, NameTest::QName(qname)))
}

pub(crate) fn parse_field_xpath(qnames: &mut QNameCollector, xpath: &str) -> Result<Field> {
    match field(qnames, xpath) {
        Ok(("", field)) => Ok(field),
        Ok((left, _)) => Err(format!(
            "key parsing not complete '{}' left from '{}'",
            left, xpath
        )),
        Err(e) => Err(format!("Error parsing field/@xpath '{}': {:?}", xpath, e)),
    }
}
// [1]   	Field	   ::=   	Path ( '|' Path )*
fn field<'a>(qnames: &mut QNameCollector, xpath: &'a str) -> IResult<&'a str, Field> {
    let (i, path) = field_path(qnames, xpath)?;
    let mut i = space0(i)?.0;
    let mut paths = vec![path];
    while let Ok((j, _)) = tuple::<_, _, (), _>((char('|'), space0))(i) {
        let (j, path) = field_path(qnames, j)?;
        paths.push(path);
        i = space0(j)?.0;
    }
    Ok((i, Field(paths)))
}
// [7]   	Path	   ::=   	('.' '//')? ( Step '/' )* ( Step | '@' NameTest )
fn field_path<'a>(qnames: &mut QNameCollector, i: &'a str) -> IResult<&'a str, FieldPath> {
    let (i, (initial_double_slash, mut steps)) = field_steps(qnames, i)?;
    let (i, is_attribute) = if let Ok((j, _)) = tuple::<_, _, (), _>((char('@'), space0))(i) {
        let (j, name_test) = name_test(qnames, j)?;
        steps.push(name_test);
        (j, true)
    } else {
        let (i, name_test) = step(qnames, i)?;
        if let Some(name_test) = name_test {
            steps.push(name_test);
        }
        (i, false)
    };
    let i = space0(i)?.0;
    Ok((
        i,
        FieldPath {
            initial_double_slash,
            is_attribute,
            steps,
        },
    ))
}
fn field_steps<'a>(
    qnames: &mut QNameCollector,
    i: &'a str,
) -> IResult<&'a str, (bool, Vec<NameTest>)> {
    let (i, initial_double_slash) = initial_double_slash(i);
    let mut steps = Vec::new();
    let mut i = space0(i)?.0;
    while let Ok((j, step)) = self::step(qnames, i) {
        if let Ok((j, _)) = tuple::<_, _, (), _>((space0, char('/'), space0))(j) {
            if let Some(step) = step {
                steps.push(step);
            }
            i = space0(j)?.0;
        } else {
            break;
        }
    }
    Ok((i, (initial_double_slash, steps)))
}

#[cfg(test)]
struct TestContext {
    qnames: QNameCollector,
}

#[cfg(test)]
impl TestContext {
    fn qname(&mut self, qname: &str) -> QNameId {
        self.qnames.add_local_qname(NCName(qname))
    }
}

#[cfg(test)]
fn test_context() -> TestContext {
    TestContext {
        qnames: QNameCollector::default(),
    }
}

#[cfg(test)]
fn selector_xpath_test(context: &mut TestContext, xpath: &str, reference: &Selector) {
    let selector = parse_selector_xpath(&mut context.qnames, xpath).unwrap();
    assert_eq!(selector, *reference);
}

#[cfg(test)]
fn field_xpath_test(context: &mut TestContext, xpath: &str, reference: &Field) {
    let field = parse_field_xpath(&mut context.qnames, xpath).unwrap();
    assert_eq!(field, *reference);
}

#[test]
fn test_selector_1() {
    let mut context = test_context();
    let reference = Selector(vec![SelectorPath {
        initial_double_slash: true,
        steps: vec![NameTest::QName(context.qname("vehicle"))],
    }]);
    selector_xpath_test(&mut context, ".//vehicle", &reference);
}

#[test]
fn test_field_1() {
    let mut context = test_context();
    let reference = Field(vec![FieldPath {
        initial_double_slash: false,
        is_attribute: true,
        steps: vec![NameTest::QName(context.qname("plateNumber"))],
    }]);
    field_xpath_test(&mut context, "@plateNumber", &reference);
}
