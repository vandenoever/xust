use crate::{
    atomic::Atomic,
    error::{ErrorCollector, XsdError},
    xsd_validator::XsdValidator,
    TypeId,
};
use std::{
    cmp::Ordering,
    fmt::{Display, Formatter},
    sync::Arc,
};
use xust_tree::{
    node::{self, NodeKind},
    qnames::{QName, QNameRef},
    tree::Tree,
};

type Node = node::Node<Arc<Tree<Atomic>>>;
pub type Element = node::Element<Arc<Tree<Atomic>>>;

pub struct ParseEnv<T> {
    pub names: T,
    #[allow(dead_code)]
    pub errors: ErrorCollector<XsdError>,
    pub validate: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub enum MaxOccurs {
    Bounded(usize),
    Unbounded,
}

impl PartialOrd for MaxOccurs {
    fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
        match (self, rhs) {
            (MaxOccurs::Bounded(a), MaxOccurs::Bounded(b)) => a.partial_cmp(b),
            (MaxOccurs::Unbounded, MaxOccurs::Bounded(_)) => Some(Ordering::Less),
            (MaxOccurs::Bounded(_), MaxOccurs::Unbounded) => Some(Ordering::Greater),
            _ => Some(Ordering::Equal),
        }
    }
}
impl Display for MaxOccurs {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            MaxOccurs::Unbounded => f.write_str("unbounded"),
            MaxOccurs::Bounded(v) => write!(f, "{}", v),
        }
    }
}

// return the first node that is a text node
// other nodes except elements are ignored
pub fn first_child_if_text(parent: &Element) -> Result<Option<Node>> {
    let mut node = parent.first_child();
    while let Some(n) = node {
        if n.node_kind() == NodeKind::Element {
            return Ok(None);
        }
        if n.node_kind() == NodeKind::Text {
            return Ok(Some(n));
        }
        node = n.next_sibling();
    }
    Ok(None)
}

// return the first node that is an element
// other nodes are ignored except for text nodes with anything but whitespace
pub fn first_child_element(parent: &Element) -> Result<Option<Element>> {
    let mut node = parent.first_child();
    while let Some(n) = node {
        if let Some(element) = n.element() {
            return Ok(Some(element));
        }
        check_no_text(&n);
        node = n.next_sibling();
    }
    Ok(None)
}

pub fn next_element_sibling(mut node: Option<Node>) -> Option<Element> {
    while let Some(n) = node {
        if let Some(element) = n.element() {
            return Some(element);
        }
        node = n.next_sibling();
    }
    None
}

pub fn next_element(element: &Element) -> Option<Element> {
    let mut node = element.next_sibling();
    while let Some(n) = node {
        if let Some(element) = n.element() {
            return Some(element);
        }
        node = n.next_sibling();
    }
    None
}

fn qn(element: &Element) -> String {
    let n = element.node_name().to_ref();
    format!("{}:{}", n.prefix(), n.local_name())
}

type Result<T> = std::result::Result<T, XsdError>;
pub type EResult<'a, T> = Result<(Option<Element>, Option<T>)>;
pub type CtResult<'a, T> = Result<(Option<Element>, T)>;

fn bt() -> eyre::Report {
    eyre::eyre!("")
}

pub fn check_name(qname: QNameRef, element: &Element) -> bool {
    //println!("{:?} {:?}", element.node_name(), qname);
    element.node_name() == qname
}

fn check_no_text(node: &Node) {
    if node.node_kind() == NodeKind::Text {}
}

pub fn check_done(element: Option<Element>) -> Result<()> {
    if let Some(element) = element {
        return Err(XsdError::UnexpectedElement(qn(&element), bt()));
    }
    Ok(())
}

pub trait AttributeParser {
    fn parse_attribute(qname: QNameRef, element: &Element, def: Option<&str>) -> Result<Self>
    where
        Self: Sized,
    {
        if let Some(att) = element.attributes().find(|a| a.node_name() == qname) {
            Self::parse_str(&att.string_value(), element)
        } else if let Some(att) = def {
            Self::parse_str(att, element)
        } else {
            Err(XsdError::MissingAttribute(qname.to_owned(), bt()))
        }
    }
    fn parse_str(att: &str, element: &Element) -> Result<Self>
    where
        Self: Sized;
}

impl<T: AttributeParser> AttributeParser for Option<T> {
    fn parse_attribute(qname: QNameRef, element: &Element, _def: Option<&str>) -> Result<Self> {
        if let Some(att) = element.attributes().find(|a| a.node_name() == qname) {
            Ok(Some(T::parse_str(&att.string_value(), element)?))
        } else {
            Ok(None)
        }
    }
    fn parse_str(_att: &str, _element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        unreachable!()
    }
}

impl<T: AttributeParser> AttributeParser for Vec<T> {
    fn parse_str(att: &str, element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        let mut v = Vec::new();
        for s in att.split_whitespace() {
            v.push(T::parse_str(s, element)?);
        }
        Ok(v)
    }
}

impl AttributeParser for String {
    fn parse_str(att: &str, _element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        Ok(att.to_string())
    }
}

impl AttributeParser for QName {
    fn parse_str(att: &str, element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        if let Some(qname) = element.node().resolve_qname(att) {
            Ok(qname)
        } else {
            Err(XsdError::UnresolvedQName(att.to_string(), bt()))
        }
    }
}

impl AttributeParser for bool {
    fn parse_str(att: &str, _element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        match att {
            "true" | "1" => Ok(true),
            "false" | "0" => Ok(false),
            v => Err(XsdError::ParseBoolError(v.into())),
        }
    }
}

impl AttributeParser for usize {
    fn parse_str(att: &str, _element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        Ok(att.parse::<usize>()?)
    }
}

impl AttributeParser for u8 {
    fn parse_str(att: &str, _element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        Ok(att.parse::<u8>()?)
    }
}

impl AttributeParser for i64 {
    fn parse_str(att: &str, _element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        Ok(att.parse::<i64>()?)
    }
}

impl AttributeParser for f64 {
    fn parse_str(att: &str, _element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        att.parse::<f64>()
            .map_err(|e| XsdError::ParseBoolError(format!("{}", e)))
    }
}

impl AttributeParser for MaxOccurs {
    fn parse_str(att: &str, _element: &Element) -> Result<Self>
    where
        Self: Sized,
    {
        if att == "unbounded" {
            Ok(MaxOccurs::Unbounded)
        } else {
            Ok(MaxOccurs::Bounded(att.parse::<usize>()?))
        }
    }
}

pub trait EParser<'a, P, T>: FnMut(&mut P, Element) -> EResult<'a, T> {
    fn parse(&mut self, env: &'a mut P, element: Element) -> EResult<'a, T>;
}

impl<'a, P, T, F> EParser<'a, P, T> for F
where
    F: FnMut(&mut P, Element) -> EResult<'a, T>,
{
    fn parse(&mut self, env: &'a mut P, element: Element) -> EResult<'a, T> {
        self(env, element)
    }
}

pub fn name_and_type<'a, P, T, F>(
    mut name: impl FnMut(&mut P) -> QNameRef,
    mut f: F,
) -> impl FnMut(&mut P, Element) -> EResult<'a, T>
where
    F: FnMut(&mut P, Element) -> CtResult<'a, T>,
{
    move |env, element| {
        Ok(if check_name(name(env), &element) {
            let (e, value) = f(env, element)?;
            (e, Some(value))
        } else {
            (Some(element), None)
        })
    }
}

pub fn opt<'a, P, T, F>(
    mut f: F,
) -> impl FnMut(&mut P, Option<Element>, &mut bool) -> EResult<'a, T>
where
    F: EParser<'a, P, T>,
{
    move |env, element, at_start| {
        Ok(if let Some(e) = element {
            let (e, v) = f(env, e)?;
            if v.is_some() {
                *at_start = false;
            }
            (e, v)
        } else {
            (None, None)
        })
    }
}

pub fn req<'a, P, T, F>(mut f: F) -> impl FnMut(&mut P, Option<Element>) -> CtResult<'a, T>
where
    F: EParser<'a, P, T>,
{
    move |env, element| {
        if let Some(e) = element {
            match f(env, e)? {
                (e, Some(value)) => Ok((e, value)),
                (_, None) => Err(XsdError::MissingElement(bt())),
            }
        } else {
            Err(XsdError::MissingElement(bt()))
        }
    }
}

pub fn req_mixed<'a, P, T, F>(
    mut f: F,
) -> impl FnMut(&mut P, Option<Element>) -> CtResult<'a, (T, String)>
where
    F: EParser<'a, P, T>,
{
    move |env, element| {
        if let Some(e) = element {
            match f(env, e.clone())? {
                (e2, Some(value)) => {
                    let s = mixed_text(e);
                    Ok((e2, (value, s)))
                }
                (_, None) => Err(XsdError::MissingElement(bt())),
            }
        } else {
            Err(XsdError::MissingElement(bt()))
        }
    }
}

pub fn req_or_default<'a, P, T: Default, F>(
    mut f: F,
) -> impl FnMut(&mut P, Option<Element>) -> CtResult<'a, T>
where
    F: EParser<'a, P, T>,
{
    move |env, element| {
        if let Some(e) = element {
            match f(env, e)? {
                (e, Some(value)) => Ok((e, value)),
                (_, None) => Err(XsdError::MissingElement(bt())),
            }
        } else {
            Ok((None, Default::default()))
        }
    }
}

pub fn req_or_default_mixed<'a, P, T: Default, F>(
    mut f: F,
) -> impl FnMut(&mut P, Option<Element>) -> CtResult<'a, (T, String)>
where
    F: EParser<'a, P, T>,
{
    move |env, element| {
        if let Some(e) = element {
            match f(env, e.clone())? {
                (e2, Some(value)) => {
                    let s = mixed_text(e);
                    Ok((e2, (value, s)))
                }
                (_, None) => Err(XsdError::MissingElement(bt())),
            }
        } else {
            Ok((None, Default::default()))
        }
    }
}

pub fn many0<'a, P, T, F>(mut f: F) -> impl FnMut(&mut P, Option<Element>) -> CtResult<'a, Vec<T>>
where
    F: EParser<'a, P, T>,
{
    move |env, mut element| {
        let mut acc = Vec::new();
        while let Some(e) = element {
            match f(env, e)? {
                (e, Some(value)) => {
                    acc.push(value);
                    element = e;
                }
                (e, None) => {
                    element = e;
                    break;
                }
            }
        }
        Ok((element, acc))
    }
}

pub fn many0_mixed<'a, P, T, F>(
    mut f: F,
) -> impl FnMut(&mut P, Option<Element>) -> CtResult<'a, Vec<(T, String)>>
where
    F: EParser<'a, P, T>,
{
    move |env, mut element| {
        let mut acc = Vec::new();
        while let Some(e) = element {
            match f(env, e.clone())? {
                (e2, Some(value)) => {
                    let s = mixed_text(e);
                    acc.push((value, s));
                    element = e2;
                }
                (e, None) => {
                    element = e;
                    break;
                }
            }
        }
        Ok((element, acc))
    }
}

pub fn mixed_text(e: Element) -> String {
    let mut s = String::new();
    let mut node = e.next_sibling();
    while let Some(n) = node {
        if n.node_kind() == NodeKind::Element {
            return s;
        }
        if n.node_kind() == NodeKind::Text {
            s.push_str(&n.string_value());
        }
        node = n.next_sibling();
    }
    s
}

pub type Unit = Element;
pub fn unit<'a, P>(_p: &mut P, e: Element) -> EResult<'a, Unit> {
    Ok((next_element(&e), Some(e.clone())))
}

pub fn bytes_to_tree(
    xml: Vec<u8>,
    validator: Option<&XsdValidator>,
) -> std::result::Result<Arc<Tree<Atomic>>, Box<dyn std::error::Error>> {
    let xml = xust_xml::read::decode_bytes(xml)?.1;
    let tree = if let Some(validator) = validator {
        validator.validate_to_tree(&xml, None)?
    } else {
        xust_xml::read::parse_xml(&xml, None, None)?
    };
    Ok(Arc::new(tree))
}

pub fn check_element_type(validate: bool, expected: TypeId, element: &Element) -> Result<()> {
    use xust_tree::typed_value::XS_UNTYPED;
    let type_id = element.node().type_id().unwrap();
    if type_id != expected {
        if validate {
            return Err(XsdError::UnexpectedElement(
                format!(
                    "Unexpected type {:?} found instead of {:?}.",
                    type_id, expected
                ),
                eyre::eyre!(""),
            ));
        } else if type_id != XS_UNTYPED {
            return Err(XsdError::UnexpectedElement(
                format!(
                    "Unexpected type {:?} found instead of {:?}.",
                    type_id, XS_UNTYPED
                ),
                eyre::eyre!(""),
            ));
        }
    }
    Ok(())
}

///// simpleType definitions
pub mod xsd {
    pub mod ct {
        pub type String = std::string::String;
        pub type DateTime = String;
        pub type Decimal = String;
        pub type NonNegativeInteger = String;
        pub type GYear = String;
        pub type AnyUri = String;
        use super::super::{next_element, CtResult, Element};

        pub fn string<'a, P>(_env: &mut P, e: Element) -> CtResult<'a, String> {
            Ok((next_element(&e), e.string_value().to_string()))
        }
        pub fn date_time<'a, P>(_env: &mut P, e: Element) -> CtResult<'a, String> {
            Ok((next_element(&e), e.string_value().to_string()))
        }
        pub fn decimal<'a, P>(_env: &mut P, e: Element) -> CtResult<'a, String> {
            Ok((next_element(&e), e.string_value().to_string()))
        }
        pub fn non_negative_integer<'a, P>(_env: &mut P, e: Element) -> CtResult<'a, String> {
            Ok((next_element(&e), e.string_value().to_string()))
        }
        pub fn g_year<'a, P>(_env: &mut P, e: Element) -> CtResult<'a, String> {
            Ok((next_element(&e), e.string_value().to_string()))
        }
        pub fn any_uri<'a, P>(_env: &mut P, e: Element) -> CtResult<'a, String> {
            Ok((next_element(&e), e.string_value().to_string()))
        }
    }
}
