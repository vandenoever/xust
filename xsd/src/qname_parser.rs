use xust_tree::qnames::{
    NCName, NamespaceDefinition, Prefix, QName, QNameCollection, QNameId,
    INITIAL_NAMESPACE_DEFINITIONS,
};
use xust_xml::ncname;

pub struct QNameParser {
    depth: usize,
    qnames: QNameCollection,
    namespace_definitions: Vec<Vec<NamespaceDefinition>>,
}

impl QNameParser {
    pub fn new(qnames: QNameCollection) -> Self {
        Self {
            depth: 0,
            qnames,
            namespace_definitions: vec![INITIAL_NAMESPACE_DEFINITIONS.into()],
        }
    }
    #[allow(dead_code)]
    pub(crate) fn qnames(&self) -> &QNameCollection {
        &self.qnames
    }
    pub fn push_namespace_definitions(&mut self, namespace_definitions: &[NamespaceDefinition]) {
        self.depth += 1;
        if self.depth >= self.namespace_definitions.len() {
            self.namespace_definitions.push(Vec::new());
        } else {
            self.namespace_definitions[self.depth].clear();
        }
        self.namespace_definitions[self.depth].extend_from_slice(namespace_definitions)
    }
    pub fn pop_namespace_definitions(&mut self) {
        self.depth -= 1;
    }
    pub fn parse_qnameid(&self, name: &str) -> Option<QNameId> {
        if let Some((p, l)) = parse_qname(name) {
            self.qnames
                .qnameid_from_prefix(p, l, &self.namespace_definitions[self.depth])
        } else {
            None
        }
    }
    pub fn parse_qname(&self, name: &str) -> Option<QName> {
        if let Some((p, l)) = parse_qname(name) {
            self.qnames
                .qname_from_prefix(p, l, &self.namespace_definitions[self.depth])
        } else {
            None
        }
    }
    pub fn namespace_definitions(&self) -> &[NamespaceDefinition] {
        &self.namespace_definitions[self.depth]
    }
}

pub fn parse_qname(name: &str) -> Option<(Prefix, NCName)> {
    let (p, l) = if let Some((p, l)) = name.find(':').map(|p| name.split_at(p)) {
        if p.is_empty() {
            return None;
        }
        (p, &l[1..])
    } else {
        ("", name)
    };
    if let Ok(("", _)) = ncname(l) {
    } else {
        return None;
    };
    Some((Prefix(p), NCName(l)))
}

impl Default for QNameParser {
    fn default() -> Self {
        Self::new(QNameCollection::default())
    }
}
