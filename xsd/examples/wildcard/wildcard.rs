#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
xust_xsd::use_xsd!();
use xust_tree::qnames::{NCName, Namespace, Prefix, QNameCollection};

type P = xust_xsd::parse::ParseEnv<QNames>;

pub struct QNames {
    qnames: QNameCollection,
    local: LocalQNames,
    w: WQNames,
}
pub fn qnames(qnames: QNameCollection) -> QNames {
    QNames {
        local: LocalQNames::new(&qnames),
        w: WQNames::new(&qnames),
        qnames,
    }
}
ns!(WQNames "http://example.com/wildcard" =>
    wildcard => "wildcard"
    wildchild => "wildchild"
);
ns!(LocalQNames "" =>
);
pub mod w {
    pub mod local {
        xust_xsd::use_xsd!();
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
    }
    pub mod e {
        xust_xsd::use_xsd!();

        element!(w wildcard Wildcard 52 -
            wildchild(Unit): v - - unit
        );
    }
}
