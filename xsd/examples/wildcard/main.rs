use std::path::PathBuf;
use wildcard::w::e::Wildcard;
use xust_xsd::{load_validator, write::serialize, Deserialize};

/**
 * This is a wildcard example that shows how to use code that was generated
 * by xsd2rs.
 *
 * The code in `example.rs` was generated with
 *   xsd2rs -o wildcard.rs wildcard.xsd
 */
mod wildcard;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input_file = PathBuf::from("xsd/examples/wildcard/wildcard.xml");
    let xml = std::fs::read(&input_file)?;
    let validator = load_validator(&["xsd/examples/wildcard/wildcard.xsd".into()], None)?;
    let (qnames, mut instance) = Wildcard::from_bytes(xml, Some(&validator))
        .map_err(|e| format!("Could not parse {}: {}", input_file.display(), e))?;
    instance.wildchild.truncate(2);
    let xml = serialize(&instance, &qnames);
    println!("{:#?}", instance);
    println!("{}", xml);
    Ok(())
}
