use simple::default::e::Simple;
use std::path::PathBuf;
use xust_xsd::{load_validator, write::serialize, Deserialize};

/**
 * This is a simple example that shows how to use code that was generated
 * by xsd2rs.
 *
 * The code in `example.rs` was generated with
 *   xsd2rs -o simple.rs simple.xsd
 */
mod simple;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input_file = PathBuf::from("xsd/examples/simple/simple.xml");
    let xml = std::fs::read(&input_file)?;
    let validator = load_validator(&["xsd/examples/simple/simple.xsd".into()], None)?;
    let (qnames, mut instance) = Simple::from_bytes(xml, Some(&validator))
        .map_err(|e| format!("Could not parse {}: {}", input_file.display(), e))?;
    instance.greeting = "Hello world!".to_string();
    instance.happy = Some(true);
    instance.score = Some(10);
    let xml = serialize(&instance, &qnames);
    println!("{:#?}", instance);
    println!("{}", xml);
    Ok(())
}
