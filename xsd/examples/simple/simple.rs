#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
xust_xsd::use_xsd!();
use xust_tree::qnames::{NCName, Namespace, Prefix, QNameCollection};

type P = xust_xsd::parse::ParseEnv<QNames>;

pub struct QNames {
    qnames: QNameCollection,
    local: LocalQNames,
    default: DefaultQNames,
}
pub fn qnames(qnames: QNameCollection) -> QNames {
    QNames {
        local: LocalQNames::new(&qnames),
        default: DefaultQNames::new(&qnames),
        qnames,
    }
}
ns!(DefaultQNames "http://example.com/simple" =>
    simple => "simple"
);
ns!(LocalQNames "" =>
    greeting => "greeting"
    happy => "happy"
    score => "score"
);
pub mod default {
    pub mod local {
        xust_xsd::use_xsd!();
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
    }
    pub mod e {
        xust_xsd::use_xsd!();

        element!(default simple Simple 52 -
                attributes {
                    local greeting greeting r - String,
                    local happy happy o - bool,
                    local score score o - u8
                }
        );
    }
}
