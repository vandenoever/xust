use nested::default::e::Nested;
use std::path::PathBuf;
use xust_xsd::{load_validator, write::serialize, Deserialize};

/**
 * This is a nested example that shows how to use code that was generated
 * by xsd2rs.
 *
 * The code in `example.rs` was generated with
 *   xsd2rs -o nested.rs nested.xsd
 */
mod nested;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input_file = PathBuf::from("xsd/examples/nested/nested.xml");
    let xml = std::fs::read(&input_file)?;
    let validator = load_validator(&["xsd/examples/nested/nested.xsd".into()], None)?;
    let (qnames, instance) = Nested::from_bytes(xml, Some(&validator))
        .map_err(|e| format!("Could not parse {}: {}", input_file.display(), e))?;
    let xml = serialize(&instance, &qnames);
    println!("{:#?}", instance);
    println!("{}", xml);
    Ok(())
}
