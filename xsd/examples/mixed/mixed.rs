#![allow(clippy::large_enum_variant)]
#![allow(dead_code)]
#![allow(unused_imports)]
xust_xsd::use_xsd!();
use xust_tree::qnames::{NCName, Namespace, Prefix, QNameCollection};

type P = xust_xsd::parse::ParseEnv<QNames>;

pub struct QNames {
    qnames: QNameCollection,
    local: LocalQNames,
    default: DefaultQNames,
}
pub fn qnames(qnames: QNameCollection) -> QNames {
    QNames {
        local: LocalQNames::new(&qnames),
        default: DefaultQNames::new(&qnames),
        qnames,
    }
}
ns!(DefaultQNames "http://example.com/mixed" =>
    first_child => "firstChild"
    mixed => "mixed"
    second_child => "secondChild"
    third_child => "thirdChild"
);
ns!(LocalQNames "" =>
    greeting => "greeting"
    happy => "happy"
);
pub mod default {
    pub mod local {
        xust_xsd::use_xsd!();
    }
    pub mod g {
        xust_xsd::use_xsd!();
    }
    pub mod ct {
        xust_xsd::use_xsd!();
        complexType!(child1 Child1 52 -
        attributes {
            local greeting greeting r - String
        });
        complexType!(child2 Child2 53 -);
    }
    pub mod e {
        xust_xsd::use_xsd!();

        element!(default mixed Mixed 54 m
            attributes {
                local greeting greeting r - String
            }
            first_child(super::ct::Child1): r default first_child super::ct::child1,
            second_child(super::ct::Child2): o default second_child super::ct::child2,
            third_child(ThirdChild): v - - third_child
        );
        element!(default third_child ThirdChild 55 -
                attributes {
                    local happy happy r - bool
                }
        );
    }
}
