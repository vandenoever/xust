use mixed::default::e::Mixed;
use std::path::PathBuf;
use xust_xsd::{load_validator, write::serialize, Deserialize};

/**
 * This is a mixed example that shows how to use code that was generated
 * by xsd2rs.
 *
 * The code in `example.rs` was generated with
 *   xsd2rs -o mixed.rs mixed.xsd
 *
 * An element with mixed=true has tuples of (element, string) as children
 * and a string as the first child. That way, text and elements can be
 * alternated in a simple way that only introduces one new name, `first_text`
 * for the first String.
 */
mod mixed;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let input_file = PathBuf::from("xsd/examples/mixed/mixed.xml");
    let xml = std::fs::read(&input_file)?;
    let validator = load_validator(&["xsd/examples/mixed/mixed.xsd".into()], None)?;
    let (qnames, mut instance) = Mixed::from_bytes(xml, Some(&validator))
        .map_err(|e| format!("Could not parse {}: {}", input_file.display(), e))?;
    instance.first_text.push('!');
    let xml = serialize(&instance, &qnames);
    println!("{:#?}", instance);
    println!("{}", xml);
    Ok(())
}
