#![allow(clippy::unnecessary_wraps)]
use crate::{
    deep_equal::DeepEqual,
    eval::{add_atomized_item, atomize_sequence, context::Context, to_boolean},
    xdm::{Array, Item, Map, Sequence},
    Ref,
};
use regex::{Regex, RegexBuilder};
use std::convert::TryFrom;
use std::fmt::Write;
use url::Url;
use xust_grammar::resolve::FunctionResolver;
use xust_tree::{
    node::{Element, Node, NodeKind},
    qnames::{NCName, Namespace, QName},
};
use xust_xsd::{
    atomic::{atomic_to_integer, round_f64, Atomic},
    types::{built_ins::*, primitive_base_type::*, TypeId},
    xpath_error::{Error, Result},
};

const ARRAY: Namespace = Namespace("http://www.w3.org/2005/xpath-functions/array");
const FN: Namespace = Namespace("http://www.w3.org/2005/xpath-functions");
const MAP: Namespace = Namespace("http://www.w3.org/2005/xpath-functions/map");
const MATH: Namespace = Namespace("http://www.w3.org/2005/xpath-functions/math");
const XS: Namespace = Namespace("http://www.w3.org/2001/XMLSchema");

pub type FunctionDefinition<T> = (
    Namespace<'static>,
    &'static str,
    usize,
    usize,
    fn(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>>,
);

pub struct FunctionDefinitions<T: Ref>(Vec<FunctionDefinition<T>>);

impl<T: Ref> FunctionDefinitions<T> {
    pub(crate) fn get(&self, pos: usize) -> &FunctionDefinition<T> {
        &self.0[pos]
    }
    pub fn add(&mut self, f: &[FunctionDefinition<T>]) {
        for f in f {
            self.0.push(*f);
        }
    }
}

impl<T: Ref> FunctionResolver for FunctionDefinitions<T> {
    fn find(&self, namespace: Namespace, local_name: NCName, arity: usize) -> Option<usize> {
        self.0.iter().position(|f| {
            f.2 <= arity && arity <= f.3 && local_name == NCName(f.1) && namespace == f.0
        })
    }
    fn len(&self) -> usize {
        self.0.len()
    }
}

fn fd<'a, T: Ref>() -> &'a [FunctionDefinition<T>] {
    &[
        (ARRAY, "get", 2, 2, array_get),
        (ARRAY, "size", 1, 1, array_size),
        (FN, "QName", 2, 2, qname),
        (FN, "abs", 1, 1, abs),
        (
            FN,
            "adjust-dateTime-to-timezone",
            1,
            2,
            adjust_date_time_to_timezone,
        ),
        (FN, "avg", 1, 1, avg),
        (FN, "base-uri", 0, 0, base_uri),
        (FN, "base-uri", 1, 1, base_uri_1),
        (FN, "boolean", 1, 1, xs_boolean),
        (FN, "ceiling", 1, 1, ceiling),
        (FN, "codepoints-to-string", 1, 1, codepoints_to_string),
        (FN, "collection", 0, 0, collection),
        (FN, "collection", 1, 1, collection_1),
        (FN, "concat", 2, 100000, concat),
        (FN, "contains", 2, 2, contains),
        (FN, "contains", 3, 3, contains_3),
        (FN, "count", 1, 1, count),
        (FN, "data", 0, 0, data),
        (FN, "data", 1, 1, data_1),
        (FN, "doc", 1, 1, doc),
        (FN, "document-uri", 0, 0, document_uri),
        (FN, "document-uri", 1, 1, document_uri_1),
        (FN, "deep-equal", 2, 2, deep_equal),
        (FN, "deep-equal", 3, 3, deep_equal_3),
        (FN, "distinct-values", 1, 2, distinct_values),
        (FN, "encode-for-uri", 1, 1, encode_for_uri),
        (FN, "format-dateTime", 2, 2, format_date_time),
        (FN, "format-dateTime", 5, 5, format_date_time_5),
        (FN, "format-integer", 2, 2, format_integer),
        (FN, "format-integer", 3, 3, format_integer_3),
        (FN, "format-number", 2, 2, format_number),
        (FN, "format-number", 3, 3, format_number_3),
        (FN, "head", 1, 1, head),
        (FN, "empty", 1, 1, empty),
        (FN, "ends-with", 2, 2, ends_with),
        (FN, "ends-with", 3, 3, ends_with_3),
        (FN, "error", 0, 0, error),
        (FN, "error", 1, 1, error_1),
        (FN, "error", 2, 3, error_2),
        (FN, "exists", 1, 1, exists),
        (FN, "exactly-one", 1, 1, exactly_one),
        (FN, "false", 0, 0, r#false),
        (FN, "floor", 1, 1, floor),
        (FN, "fold-left", 3, 3, fold_left),
        (FN, "function-lookup", 2, 2, function_lookup),
        (FN, "in-scope-prefixes", 1, 1, in_scope_prefixes),
        (FN, "id", 1, 1, id),
        (FN, "id", 2, 2, id_2),
        (FN, "idref", 1, 1, idref),
        (FN, "idref", 2, 2, idref_2),
        (FN, "iri-to-uri", 1, 1, iri_to_uri),
        (FN, "json-to-xml", 1, 2, json_to_xml),
        (FN, "json-doc", 1, 2, json_doc),
        (FN, "lang", 1, 1, lang),
        (FN, "lang", 2, 2, lang_2),
        (FN, "last", 0, 0, last),
        (FN, "load-xquery-module", 1, 2, load_xquery_module),
        (FN, "local-name", 0, 1, local_name),
        (FN, "local-name-from-QName", 1, 1, local_name_from_qname),
        (FN, "lower-case", 1, 1, lower_case),
        (FN, "upper-case", 1, 1, upper_case),
        (FN, "matches", 2, 2, matches),
        (FN, "matches", 3, 3, matches_3),
        (FN, "max", 1, 1, max),
        (FN, "max", 2, 2, max_2),
        (FN, "min", 1, 1, min),
        (FN, "min", 2, 2, min_2),
        (FN, "name", 0, 0, name),
        (FN, "name", 1, 1, name_1),
        (FN, "namespace-uri", 0, 0, namespace_uri),
        (FN, "namespace-uri", 1, 1, namespace_uri_1),
        (
            FN,
            "namespace-uri-for-prefix",
            2,
            2,
            namespace_uri_for_prefix,
        ),
        (
            FN,
            "namespace-uri-from-QName",
            1,
            1,
            namespace_uri_from_qname,
        ),
        (FN, "nilled", 0, 0, nilled),
        (FN, "nilled", 1, 1, nilled_1),
        (FN, "node-name", 0, 0, node_name),
        (FN, "node-name", 1, 1, node_name_1),
        (FN, "normalize-space", 0, 0, normalize_space),
        (FN, "normalize-space", 1, 1, normalize_space_1),
        (FN, "not", 1, 1, not),
        (FN, "number", 0, 0, number),
        (FN, "number", 1, 1, number_1),
        (FN, "parse-json", 1, 2, parse_json),
        (FN, "parse-xml", 1, 1, parse_xml),
        (FN, "path", 0, 0, path),
        (FN, "path", 1, 1, path_1),
        (FN, "position", 0, 0, position),
        (FN, "random-number-generator", 0, 0, random_number_generator),
        (
            FN,
            "random-number-generator",
            1,
            1,
            random_number_generator_1,
        ),
        (FN, "replace", 3, 4, replace),
        (FN, "reverse", 1, 1, reverse),
        (FN, "root", 0, 0, root),
        (FN, "root", 1, 1, root_1),
        (FN, "round", 1, 1, round),
        (FN, "round", 2, 2, round_2),
        (FN, "serialize", 1, 1, serialize),
        (FN, "serialize", 2, 2, serialize_2),
        (FN, "sort", 1, 1, sort),
        (FN, "sort", 2, 2, sort_2),
        (FN, "sort", 3, 3, sort_3),
        (FN, "starts-with", 2, 3, starts_with),
        (FN, "string", 0, 0, string),
        (FN, "string", 1, 1, string_1),
        (FN, "string-join", 1, 1, string_join),
        (FN, "string-join", 2, 2, string_join_2),
        (FN, "string-length", 0, 0, string_length),
        (FN, "string-length", 1, 1, string_length_1),
        (FN, "string-to-codepoints", 1, 1, string_to_codepoints),
        (FN, "subsequence", 2, 3, subsequence),
        (FN, "substring", 2, 2, substring),
        (FN, "substring", 3, 3, substring_3),
        (FN, "substring-before", 2, 2, substring_before),
        (FN, "substring-before", 3, 3, substring_before_3),
        (FN, "substring-after", 2, 2, substring_after),
        (FN, "substring-after", 3, 3, substring_after_3),
        (FN, "sum", 1, 1, sum),
        (FN, "sum", 2, 2, sum_2),
        (FN, "tail", 1, 1, tail),
        (FN, "tokenize", 1, 1, tokenize),
        (FN, "tokenize", 2, 2, tokenize_2),
        (FN, "tokenize", 3, 3, tokenize_3),
        (FN, "translate", 3, 3, translate),
        (FN, "trace", 1, 2, trace),
        (FN, "true", 0, 0, r#true),
        (FN, "unordered", 1, 1, unordered),
        (FN, "unparsed-text", 1, 1, unparsed_text),
        (FN, "unparsed-text", 2, 2, unparsed_text_2),
        (FN, "unparsed-text-lines", 1, 2, unparsed_text_lines),
        (FN, "year-from-date", 1, 1, year_from_date),
        (MAP, "contains", 2, 2, contains),
        (MAP, "entry", 2, 2, entry),
        (MAP, "for-each", 2, 2, for_each),
        (MAP, "get", 2, 2, map_get),
        (MAP, "keys", 1, 1, keys),
        (MAP, "merge", 1, 1, merge),
        (MAP, "merge", 2, 2, merge_2),
        (MAP, "put", 3, 3, put),
        (MAP, "remove", 2, 2, remove),
        (MAP, "size", 1, 1, map_size),
        (MATH, "pi", 0, 0, pi),
        (MATH, "exp", 1, 1, exp),
        (MATH, "exp10", 1, 1, exp10),
        (MATH, "log", 1, 1, log),
        (MATH, "log10", 1, 1, log10),
        (MATH, "pow", 2, 2, pow),
        (MATH, "sqrt", 1, 1, sqrt),
        (MATH, "sin", 1, 1, sin),
        (MATH, "cos", 1, 1, cos),
        (MATH, "tan", 1, 1, tan),
        (MATH, "asin", 1, 1, asin),
        (MATH, "acos", 1, 1, acos),
        (MATH, "atan", 1, 1, atan),
        (MATH, "atan2", 2, 2, atan2),
        (XS, "ID", 1, 1, xs_id),
        (XS, "IDREF", 1, 1, xs_idref),
        (XS, "ENTITY", 1, 1, xs_entity),
        (XS, "NCName", 1, 1, xs_ncname),
        (XS, "Name", 1, 1, xs_name),
        (XS, "QName", 1, 1, xs_qname),
        (XS, "anyURI", 1, 1, xs_any_uri),
        (XS, "base64Binary", 1, 1, xs_base64_binary),
        (XS, "boolean", 1, 1, xs_boolean),
        (XS, "byte", 1, 1, xs_byte),
        (XS, "date", 1, 1, xs_date),
        (XS, "dateTime", 1, 1, xs_date_time),
        (XS, "dayTimeDuration", 1, 1, xs_day_time_duration),
        (XS, "decimal", 1, 1, xs_decimal),
        (XS, "double", 1, 1, xs_double),
        (XS, "duration", 1, 1, xs_duration),
        (XS, "error", 1, 1, xs_error),
        (XS, "float", 1, 1, xs_float),
        (XS, "gDay", 1, 1, xs_g_day),
        (XS, "gMonth", 1, 1, xs_g_month),
        (XS, "gMonthDay", 1, 1, xs_g_month_day),
        (XS, "gYear", 1, 1, xs_g_year),
        (XS, "gYearMonth", 1, 1, xs_g_year_month),
        (XS, "hexBinary", 1, 1, xs_hex_binary),
        (XS, "int", 1, 1, xs_int),
        (XS, "integer", 1, 1, xs_integer),
        (XS, "language", 1, 1, xs_language),
        (XS, "long", 1, 1, xs_long),
        (XS, "short", 1, 1, xs_short),
        (XS, "string", 1, 1, xs_string),
        (XS, "token", 1, 1, xs_token),
        (XS, "time", 1, 1, xs_time),
        (XS, "untypedAtomic", 1, 1, xs_untyped_atomic),
        (XS, "yearMonthDuration", 1, 1, xs_year_month_duration),
    ]
}

pub fn function_definitions<T: Ref>() -> FunctionDefinitions<T> {
    FunctionDefinitions(fd().into())
}

fn context_item<'a, A, T: Ref>(
    context: &'a Context<T>,
    f: fn(item: &'a Item<T>) -> Result<A>,
) -> Result<A> {
    if let Some(item) = &context.item {
        f(item.item)
    } else {
        Err(Error::xpdy(2))
    }
}

pub fn arg<'a, A, T: Ref>(
    arg: &'a Sequence<T>,
    f: fn(item: &'a Item<T>) -> Result<A>,
) -> Result<A> {
    if arg.len() == 1 {
        Ok(f(arg.get(0))?)
    } else {
        Err(Error::xpty(4))
    }
}

fn optional_arg<'a, A, T: Ref>(
    arg: &'a Sequence<T>,
    f: fn(item: &'a Item<T>) -> Result<A>,
) -> Result<Option<A>> {
    if arg.is_empty() {
        Ok(None)
    } else if arg.len() == 1 {
        Ok(Some(f(arg.get(0))?))
    } else {
        Err(Error::xpty(4))
    }
}

fn item_as_item<T: Ref>(item: &Item<T>) -> Result<&Item<T>> {
    Ok(item)
}

fn item_as_node<T: Ref>(item: &Item<T>) -> Result<&Node<T>> {
    if let Item::Node(node) = item {
        Ok(node)
    } else {
        Err(Error::xpty(4))
    }
}

fn item_as_element<T: Ref>(item: &Item<T>) -> Result<Element<T>> {
    if let Item::Node(node) = item {
        if let Some(element) = node.element() {
            return Ok(element);
        }
    }
    Err(Error::xpty(4))
}

fn item_as_atomic<T: Ref>(item: &Item<T>) -> Result<&Atomic> {
    if let Item::Atomic(atomic) = item {
        Ok(atomic)
    } else {
        Err(Error::xpty(4))
    }
}

pub fn item_as_integer<T: Ref>(item: &Item<T>) -> Result<i64> {
    if let Item::Atomic(a) = item {
        if let Some(v) = a.as_i64() {
            return Ok(v);
        }
    }
    Err(Error::xpty(4))
}

fn item_as_double<T: Ref>(item: &Item<T>) -> Result<f64> {
    if let Item::Atomic(a) = item {
        if let Some(v) = a.as_double() {
            return Ok(v);
        }
    }
    Err(Error::xpty(4))
}

fn item_as_string<T: Ref>(item: &Item<T>) -> Result<&str> {
    if let Item::Atomic(a) = item {
        if let Some(v) = a.as_str() {
            return Ok(v);
        }
    }
    Err(Error::xpty(4))
}

fn item_as_array<T: Ref>(item: &Item<T>) -> Result<&Array<T>> {
    if let Item::Array(array) = item {
        Ok(array)
    } else {
        Err(Error::xpty(4))
    }
}

fn item_as_qname<T: Ref>(item: &Item<T>) -> Result<&QName> {
    if let Item::Atomic(a) = item {
        if let Some(qname) = a.as_qname() {
            return Ok(qname);
        }
    }
    Err(Error::xpty(4))
}

// 2.1 fn:node-name
// fn:node-name() as xs:QName?
// fn:node-name($arg as node()?) as xs:QName?
fn node_name<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    node_name_impl(node)
}
fn node_name_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = match optional_arg(&args[0], item_as_node)? {
        Some(node) => node,
        None => return Ok(Sequence::empty()),
    };
    node_name_impl(node)
}
fn node_name_impl<T: Ref>(node: &Node<T>) -> Result<Sequence<T>> {
    Ok(if let Some(qname) = node.node_name() {
        Sequence::qname(qname.to_owned())
    } else {
        Sequence::empty()
    })
}

// 2.2 fn:nilled
// fn:nilled() as xs:boolean?
// fn:nilled($arg as node()?) as xs:boolean?
fn nilled<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    nilled_impl(node)
}
fn nilled_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = match optional_arg(&args[0], item_as_node)? {
        Some(node) => node,
        None => return Ok(Sequence::empty()),
    };
    nilled_impl(node)
}
fn nilled_impl<T: Ref>(node: &Node<T>) -> Result<Sequence<T>> {
    Ok(if node.element().is_none() {
        Sequence::empty()
    } else {
        Sequence::boolean(node.nilled())
    })
}

// 2.3 fn:string
// fn:string() as xs:string
// fn:string($arg as item()?) as xs:string
fn string<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let arg = context_item(context, item_as_item)?;
    Ok(Sequence::string(string_impl(arg)?))
}
fn string_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_item)? {
        Some(arg) => arg,
        None => return Ok(Sequence::string(String::new())),
    };
    Ok(Sequence::string(string_impl(arg)?))
}
fn string_impl<T: Ref>(arg: &Item<T>) -> Result<String> {
    Ok(match arg {
        Item::Array(_) | Item::Map(_) => return Err(Error::foty(14)),
        item => item.to_string(),
    })
}

// 2.4 fn:data
// fn:data() as xs:anyAtomicType*
// fn:data($arg as item()*) as xs:anyAtomicType*
fn data<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let arg = context_item(context, item_as_item)?;
    let mut v = Vec::new();
    add_atomized_item(arg.clone(), &mut v, context)?;
    let v = v.into_iter().map(Item::Atomic).collect();
    Ok(Sequence::many(v))
}
fn data_1<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    Ok(atomize_sequence(&args[0], context)?
        .into_iter()
        .map(Item::Atomic)
        .collect())
}

// 2.5 fn:base-uri
// fn:base-uri() as xs:anyURI?
// fn:base-uri($arg as node()?) as xs:anyURI?
fn base_uri<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    base_uri_impl(node)
}
fn base_uri_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = match optional_arg(&args[0], item_as_node)? {
        Some(node) => node,
        None => return Ok(Sequence::empty()),
    };
    base_uri_impl(node)
}
fn base_uri_impl<T: Ref>(_arg: &Node<T>) -> Result<Sequence<T>> {
    Err(Error::xxxx(1))
}

// 2.6 fn:document-uri
// fn:document-uri() as xs:anyURI?
// fn:document-uri($arg as node()?) as xs:anyURI?
fn document_uri<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    document_uri_impl(node)
}
fn document_uri_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = match optional_arg(&args[0], item_as_node)? {
        Some(node) => node,
        None => return Ok(Sequence::empty()),
    };
    document_uri_impl(node)
}
fn document_uri_impl<T: Ref>(arg: &Node<T>) -> Result<Sequence<T>> {
    if arg.node_kind() != NodeKind::Document {
        return Ok(Sequence::empty());
    }
    Err(Error::xxxx(2))
}

// 3.1.1 fn:error
// fn:error() as none
// fn:error($code as xs:QName?) as none
// fn:error($code as xs:QName?, $description as xs:string) as none
// fn:error($code as xs:QName?,
//          $description as xs:string,
//          $error-object as item()*) as none
fn error<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Err(Error::foer(0))
}
fn error_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _code = optional_arg(&args[0], item_as_qname)?;
    Err(Error::foer(0))
}
fn error_2<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _code = optional_arg(&args[0], item_as_qname)?;
    let _description = arg(&args[1], item_as_string)?;
    Err(Error::foer(0))
}

// 3.2.1 fn:trace
// fn:trace($value as item()*) as item()*
// fn:trace($value as item()*, $label as xs:string) as item()*
fn trace<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Ok(args[0].clone())
}

// 4.4.1 fn:abs
// fn:abs($arg as xs:numeric?) as xs:numeric?
fn abs<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_atomic)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::atomic(arg.abs()?))
}

// 4.4.2 fn:ceiling
// fn:ceiling($arg as xs:numeric?) as xs:numeric?
fn ceiling<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_atomic)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::atomic(arg.ceil()?))
}

// 4.4.3 fn:floor
// fn:floor($arg as xs:numeric?) as xs:numeric?
fn floor<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_atomic)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::atomic(arg.floor()?))
}

// 4.4.4 fn:round
// fn:round($arg as xs:numeric?) as xs:numeric?
// fn:round($arg as xs:numeric?, $precision as xs:integer) as xs:numeric?
fn round<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_atomic)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::atomic(arg.round()?))
}
fn round_2<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let a = match optional_arg(&args[0], item_as_atomic)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    let precision = arg(&args[1], item_as_integer)?;
    Ok(Sequence::atomic(a.round_2(precision)?))
}

// 4.5.1 fn:number
// fn:number() as xs:double
// fn:number($arg as xs:anyAtomicType?) as xs:double
fn number<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let arg = context_item(context, item_as_atomic)?;
    constructor(
        XS_DOUBLE,
        DOUBLE,
        &[Sequence::one(Item::Atomic(arg.clone()))],
        context,
    )
}
fn number_1<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_DOUBLE, DOUBLE, args, context)
}

// 4.6.1 fn:format-integer
// fn:format-integer($value as xs:integer?, $picture as xs:string) as xs:string
// fn:format-integer($value as xs:integer?,
//                   $picture as xs:string,
//                   $lang as xs:string?) as xs:string
fn format_integer<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _value = match optional_arg(&args[0], item_as_integer)? {
        Some(value) => value,
        None => return Ok(Sequence::string(String::new())),
    };
    let _picture = arg(&args[1], item_as_string)?;
    Err(Error::xxxx(3))
}
fn format_integer_3<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _value = match optional_arg(&args[0], item_as_integer)? {
        Some(value) => value,
        None => return Ok(Sequence::string(String::new())),
    };
    let _picture = arg(&args[1], item_as_string)?;
    let _lang = optional_arg(&args[2], item_as_string)?;
    Err(Error::xxxx(4))
}

// 4.7.2 fn:format-number
// fn:format-number($value as xs:numeric?, $picture as xs:string) as xs:string
// fn:format-number($value as xs:numeric?,
//                  $picture as xs:string,
//                  $decimal-format-name as xs:string?) as xs:string
fn format_number<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _value = match optional_arg(&args[0], item_as_atomic)? {
        Some(value) => value,
        None => return Ok(Sequence::string(String::new())),
    };
    let _picture = arg(&args[1], item_as_string)?;
    Err(Error::xxxx(5))
}
fn format_number_3<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _value = match optional_arg(&args[0], item_as_atomic)? {
        Some(value) => value,
        None => return Ok(Sequence::string(String::new())),
    };
    let _picture = arg(&args[1], item_as_string)?;
    let _lang = optional_arg(&args[2], item_as_string)?;
    Err(Error::xxxx(6))
}

// 4.8.1 math:pi
// math:pi() as xs:double
fn pi<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Ok(Sequence::one(Item::Atomic(Atomic::double(
        std::f64::consts::PI,
    ))))
}

fn math_one_double_arg<T: Ref>(args: &[Sequence<T>], f: fn(f64) -> f64) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_double)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::double(f(arg)))
}

// 4.8.2 math:exp
// math:exp($arg as xs:double?) as xs:double?
fn exp<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::exp)
}

// 4.8.3 math:exp10
// math:exp10($arg as xs:double?) as xs:double?
fn exp10<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, |arg| 10_f64.powf(arg))
}

// 4.8.4 math:log
// math:log($arg as xs:double?) as xs:double?
fn log<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::ln)
}

// 4.8.5 math:log10
// math:log10($arg as xs:double?) as xs:double?
fn log10<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::log10)
}

// 4.8.6 math:pow
// math:pow($x as xs:double?, $y as xs:numeric) as xs:double?
fn pow<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let x = match optional_arg(&args[0], item_as_double)? {
        Some(x) => x,
        None => return Ok(Sequence::empty()),
    };
    let y = arg(&args[1], item_as_double)?;
    Ok(Sequence::double(x.powf(y)))
}

// 4.8.7 math:sqrt
// math:sqrt($arg as xs:double?) as xs:double?
fn sqrt<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::sqrt)
}

// 4.8.8 math:sin
// math:sin($arg as xs:double?) as xs:double?
fn sin<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::sin)
}

// 4.8.9 math:cos
// math:cos($arg as xs:double?) as xs:double?
fn cos<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::cos)
}

// 4.8.10 math:tan
// math:tan($arg as xs:double?) as xs:double?
fn tan<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::tan)
}

// 4.8.11 math:asin
// math:asin($arg as xs:double?) as xs:double?
fn asin<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::asin)
}

// 4.8.12 math:acos
// math:acos($arg as xs:double?) as xs:double?
fn acos<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::acos)
}

// 4.8.13 math:atan
// math:atan($arg as xs:double?) as xs:double?
fn atan<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    math_one_double_arg(args, f64::atan)
}

// 4.8.14 math:atan2
// math:atan2($arg as xs:double?) as xs:double?
fn atan2<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let x = match optional_arg(&args[0], item_as_double)? {
        Some(x) => x,
        None => return Ok(Sequence::empty()),
    };
    let y = arg(&args[1], item_as_double)?;
    Ok(Sequence::double(x.atan2(y)))
}

// 4.9.1 fn:random-number-generator
// fn:random-number-generator() as map(xs:string, item())
// fn:random-number-generator($seed as xs:anyAtomicType?) as map(xs:string, item())
fn random_number_generator<T: Ref>(
    _args: &[Sequence<T>],
    _context: &Context<T>,
) -> Result<Sequence<T>> {
    Err(Error::xxxx(7))
}
fn random_number_generator_1<T: Ref>(
    args: &[Sequence<T>],
    _context: &Context<T>,
) -> Result<Sequence<T>> {
    let _seed = optional_arg(&args[0], item_as_atomic)?;
    Err(Error::xxxx(8))
}

// 5.2.1 fn:codepoints-to-string
// fn:codepoints-to-string($arg as xs:integer*) as xs:string
fn codepoints_to_string<T: Ref>(
    args: &[Sequence<T>],
    _context: &Context<T>,
) -> Result<Sequence<T>> {
    let mut s = String::with_capacity(args.len());
    for arg in &args[0] {
        let i = cast_item_to_atomic(arg, atomic_to_integer)?;
        let i = u32::try_from(i).map_err(|_| Error::foch(1))?;
        let c = char::try_from(i).map_err(|_| Error::foch(1))?;
        s.push(c);
    }
    Ok(Sequence::one(Item::Atomic(Atomic::string(s))))
}

// 5.2.2 fn:string-to-codepoints
// fn:string-to-codepoints($arg as xs:string?) as xs:integer*
fn string_to_codepoints<T: Ref>(
    args: &[Sequence<T>],
    _context: &Context<T>,
) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_string)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::many(
        arg.chars()
            .map(|c| Item::Atomic(Atomic::integer(c as i64)))
            .collect(),
    ))
}

// 5.4.1 fn:concat
// fn:concat($arg1 as xs:anyAtomicType?,
//           $arg2 as xs:anyAtomicType?,
//           ... ) as xs:string
pub fn concat<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    concat_impl(args)
}
pub fn concat_impl<T: Ref>(args: &[Sequence<T>]) -> Result<Sequence<T>> {
    let mut s = String::new();
    for seq in args {
        if seq.len() > 1 {
            return Err(Error::xpty(4));
        }
        for i in seq {
            write!(&mut s, "{}", i).unwrap();
        }
    }
    Ok(Sequence::one(Item::Atomic(Atomic::string(s))))
}

// 5.4.2 fn:string-join
// fn:string-join($arg1 as xs:anyAtomicType*) as xs:string
// fn:string-join($arg1 as xs:anyAtomicType*, $arg2 as xs:string) as xs:string
fn string_join<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = atomize_sequence(&args[0], context)?;
    string_join_impl(arg1, "")
}
fn string_join_2<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = atomize_sequence(&args[0], context)?;
    let arg2 = arg(&args[1], item_as_string)?;
    string_join_impl(arg1, arg2)
}
fn string_join_impl<T: Ref>(arg1: Vec<Atomic>, arg2: &str) -> Result<Sequence<T>> {
    let mut s = String::new();
    let mut arg1 = arg1.iter();
    if let Some(first) = arg1.next() {
        write!(&mut s, "{}", first).unwrap();
    }
    for arg in arg1 {
        s.push_str(arg2);
        write!(&mut s, "{}", arg).unwrap();
    }
    Ok(Sequence::string(s))
}

// 5.4.3 fn:substring
// fn:substring($sourceString as xs:string?, $start as xs:double) as xs:string
// fn:substring($sourceString as xs:string?,
//              $start as xs:double,
//              $length as xs:double) as xs:string
fn substring<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let source_string = match optional_arg(&args[0], item_as_string)? {
        Some(arg) => arg,
        None => return Ok(Sequence::string(String::new())),
    };
    let start = arg(&args[1], item_as_double)?;
    let start = round_f64(start) as i64;
    if start > 1 {
        Ok(Sequence::string(
            source_string.chars().skip((start - 1) as usize).collect(),
        ))
    } else {
        Ok(Sequence::string(source_string.to_string()))
    }
}
fn substring_3<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let source_string = match optional_arg(&args[0], item_as_string)? {
        Some(arg) => arg,
        None => return Ok(Sequence::string(String::new())),
    };
    let start = arg(&args[1], item_as_double)?;
    let length = arg(&args[2], item_as_double)?;
    let start = round_f64(start) as i64;
    let length = round_f64(length) as i64;
    if start > 1 {
        Ok(Sequence::string(
            source_string
                .chars()
                .skip((start - 1) as usize)
                .take(length as usize)
                .collect(),
        ))
    } else {
        Ok(Sequence::string(
            source_string
                .chars()
                .take((start + length - 1) as usize)
                .collect(),
        ))
    }
}

// 5.4.4 fn:string-length
// fn:string-length() as xs:integer
// fn:string-length($arg as xs:string?) as xs:integer
fn string_length<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let arg = context_item(context, item_as_item)?;
    let arg = string_impl(arg)?;
    Ok(Sequence::integer(arg.chars().count() as i64))
}
fn string_length_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_string)? {
        Some(arg) => arg,
        None => return Ok(Sequence::integer(0)),
    };
    Ok(Sequence::integer(arg.chars().count() as i64))
}

// 5.4.5 fn:normalize-space
// fn:normalize-space() as xs:string
// fn:normalize-space($arg as xs:string?) as xs:string
fn normalize_space<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let arg = context_item(context, item_as_item)?;
    let arg = string_impl(arg)?;
    normalize_space_impl(&arg)
}
fn normalize_space_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_string)? {
        Some(arg) => arg,
        None => return Ok(Sequence::string(String::new())),
    };
    normalize_space_impl(arg)
}
fn normalize_space_impl<T: Ref>(s: &str) -> Result<Sequence<T>> {
    Ok(Sequence::string(s.trim().to_string()))
}

// 5.4.7 fn:upper-case
// fn:upper-case($arg as xs:string?) as xs:string
fn upper_case<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_string)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::string(arg.to_uppercase()))
}

// 5.4.8 fn:lower-case
// fn:lower-case($arg as xs:string?) as xs:string
fn lower_case<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_string)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::string(arg.to_lowercase()))
}

// 5.4.9 fn:translate
// fn:translate($arg as xs:string?,
//              $mapString as xs:string,
//              $transString as xs:string) as xs:string
fn translate<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg_ = match optional_arg(&args[0], item_as_string)? {
        Some(arg) => arg,
        None => return Ok(Sequence::string(String::new())),
    };
    let map_string = arg(&args[1], item_as_string)?;
    let trans_string = arg(&args[2], item_as_string)?;
    if map_string.is_empty() {
        return Ok(Sequence::string(arg_.to_string()));
    }
    let translated = arg_
        .chars()
        .filter_map(|c| {
            if let Some(pos) = map_string.chars().position(|t| t == c) {
                trans_string.chars().nth(pos)
            } else {
                Some(c)
            }
        })
        .collect();
    Ok(Sequence::string(translated))
}

// 5.5.1 fn:contains
// fn:contains($arg1 as xs:string?, $arg2 as xs:string?) as xs:boolean
// fn:contains($arg1 as xs:string?,
//             $arg2 as xs:string?,
//             $collation as xs:string) as xs:boolean
fn contains<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = optional_arg(&args[0], item_as_string)?;
    let arg2 = optional_arg(&args[1], item_as_string)?;
    let (arg1, arg2) = match (arg1, arg2) {
        (Some(arg1), Some(arg2)) => (arg1, arg2),
        _ => return Ok(Sequence::empty()),
    };
    contains_impl(arg1, arg2, None)
}
fn contains_3<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = optional_arg(&args[0], item_as_string)?;
    let arg2 = optional_arg(&args[1], item_as_string)?;
    let (arg1, arg2) = match (arg1, arg2) {
        (Some(arg1), Some(arg2)) => (arg1, arg2),
        _ => return Ok(Sequence::empty()),
    };
    let collation = arg(&args[2], item_as_string)?;
    contains_impl(arg1, arg2, Some(collation))
}
fn contains_impl<T: Ref>(arg1: &str, arg2: &str, _collation: Option<&str>) -> Result<Sequence<T>> {
    Ok(Sequence::boolean(arg1.contains(arg2)))
}

// 5.5.3 fn:ends-with
// fn:ends-with($arg1 as xs:string?, $arg2 as xs:string?) as xs:boolean
// fn:ends-with($arg1 as xs:string?,
//              $arg2 as xs:string?,
//              $collation as xs:string) as xs:boolean
fn ends_with<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = arg(&args[0], item_as_string)?;
    let arg2 = arg(&args[1], item_as_string)?;
    Ok(Sequence::boolean(arg1.ends_with(arg2)))
}
fn ends_with_3<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = arg(&args[0], item_as_string)?;
    let arg2 = arg(&args[1], item_as_string)?;
    let _collation = optional_arg(&args[2], item_as_string)?.unwrap_or("");
    Ok(Sequence::boolean(arg1.ends_with(arg2)))
}

// 5.5.4 fn:substring-before
// fn:substring-before($arg1 as xs:string?, $arg2 as xs:string?) as xs:string
// fn:substring-before($arg1 as xs:string?,
//                    $arg2 as xs:string?,
//                    $collation as xs:string) as xs:string
fn substring_before<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    let arg2 = optional_arg(&args[1], item_as_string)?.unwrap_or("");
    substring_before_impl(arg1, arg2)
}
fn substring_before_3<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    let arg2 = optional_arg(&args[1], item_as_string)?.unwrap_or("");
    let _collation = arg(&args[2], item_as_string)?;
    substring_before_impl(arg1, arg2)
}
fn substring_before_impl<T: Ref>(arg1: &str, arg2: &str) -> Result<Sequence<T>> {
    let s = if arg2.is_empty() {
        ""
    } else if let Some(pos) = arg1.find(arg2) {
        &arg1[..pos]
    } else {
        ""
    };
    Ok(Sequence::string(s.to_string()))
}

// 5.5.5 fn:substring-after
// fn:substring-after($arg1 as xs:string?, $arg2 as xs:string?) as xs:string
// fn:substring-after($arg1 as xs:string?,
//                    $arg2 as xs:string?,
//                    $collation as xs:string) as xs:string
fn substring_after<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    let arg2 = optional_arg(&args[1], item_as_string)?.unwrap_or("");
    substring_after_impl(arg1, arg2)
}
fn substring_after_3<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg1 = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    let arg2 = optional_arg(&args[1], item_as_string)?.unwrap_or("");
    let _collation = arg(&args[2], item_as_string)?;
    substring_after_impl(arg1, arg2)
}
fn substring_after_impl<T: Ref>(arg1: &str, arg2: &str) -> Result<Sequence<T>> {
    let s = if arg2.is_empty() {
        arg1
    } else if let Some(pos) = arg1.find(arg2) {
        &arg1[pos + arg2.len()..]
    } else {
        ""
    };
    Ok(Sequence::string(s.to_string()))
}

// 5.6.3 fn:matches
// fn:matches($input as xs:string?, $pattern as xs:string) as xs:boolean
// fn:matches($input as xs:string?,
//            $pattern as xs:string,
//            $flags as xs:string) as xs:boolean
fn matches<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let input = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    let pattern = arg(&args[1], item_as_string)?;
    matches_impl(input, pattern, "")
}
fn matches_3<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let input = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    let pattern = arg(&args[1], item_as_string)?;
    let flags = arg(&args[2], item_as_string)?;
    matches_impl(input, pattern, flags)
}
fn matches_impl<T: Ref>(input: &str, pattern: &str, flags: &str) -> Result<Sequence<T>> {
    let re = build_regex(pattern, flags)?;
    Ok(Sequence::boolean(re.is_match(input)))
}
fn build_regex(pattern: &str, flags: &str) -> Result<Regex> {
    let mut builder = if flags.contains('q') {
        let pattern = regex::escape(pattern);
        RegexBuilder::new(&pattern)
    } else {
        RegexBuilder::new(pattern)
    };
    builder.dot_matches_new_line(flags.contains('s'));
    builder.multi_line(flags.contains('m'));
    builder.case_insensitive(flags.contains('i'));
    builder.ignore_whitespace(flags.contains('x'));
    builder.build().map_err(|_| Error::forx(2))
}

// 5.6.6 fn:matches
// fn:replace($input       as xs:string?,
//            $pattern     as xs:string,
//            $replacement as xs:string) as xs:string
// fn:replace($input       as xs:string?,
//            $pattern     as xs:string,
//            $replacement as xs:string,
//            $flags       as xs:string) as xs:string
fn replace<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    unimplemented()
}

// 5.6.5 fn:tokenize
// fn:tokenize($input as xs:string?) as xs:string*
// fn:tokenize($input as xs:string?, $pattern as xs:string) as xs:string*
// fn:tokenize($input as xs:string?,
//             $pattern as xs:string,
//             $flags as xs:string) as xs:string*
fn tokenize<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let input = match optional_arg(&args[0], item_as_string)? {
        Some(input) => input,
        None => return Ok(Sequence::empty()),
    };
    tokenize_impl(input)
}
fn tokenize_2<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Err(Error::xxxx(9))
}
fn tokenize_3<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Err(Error::xxxx(10))
}
fn tokenize_impl<T: Ref>(input: &str) -> Result<Sequence<T>> {
    if input.is_empty() {
        return Ok(Sequence::empty());
    }
    Ok(Sequence::many(
        input
            .split(' ')
            .map(|s| Item::Atomic(Atomic::string(s.to_string())))
            .collect(),
    ))
}

// 6.2 fn:encode-for-uri
// fn:encode-for-uri($uri-part as xs:string?) as xs:string
fn encode_for_uri<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let uri_part = match optional_arg(&args[0], item_as_string)? {
        Some(uri_part) => uri_part,
        None => return Ok(Sequence::string(String::new())),
    };
    use percent_encoding::{utf8_percent_encode, AsciiSet, NON_ALPHANUMERIC};
    const FRAGMENT: &AsciiSet = &NON_ALPHANUMERIC
        .remove(b'-')
        .remove(b'_')
        .remove(b'.')
        .remove(b'~');
    Ok(Sequence::string(
        utf8_percent_encode(uri_part, FRAGMENT).to_string(),
    ))
}

// 6.3 fn:iri-to-uri
// fn:iri-to-uri($iri as xs:string?) as xs:string
fn iri_to_uri<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _iri = match optional_arg(&args[0], item_as_string)? {
        Some(iri) => iri,
        None => return Ok(Sequence::string(String::new())),
    };
    Err(Error::xxxx(11))
}

// 7.1.1 fn:true
// fn:true() as xs:boolean
fn r#true<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Ok(Sequence::r#true())
}

// 7.1.1 fn:false
// fn:false() as xs:boolean
fn r#false<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Ok(Sequence::r#false())
}

// 7.3.2 fn:not
fn not<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let b = to_boolean(&args[0])?;
    Ok(Sequence::boolean(!b))
}

// 10.1.2 fn:QName
// fn:QName($paramURI as xs:string?, $paramQName as xs:string) as xs:QName
fn qname<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let param_uri = Namespace(optional_arg(&args[0], item_as_string)?.unwrap_or(""));
    let param_qname = arg(&args[1], item_as_string)?;
    let qname = xust_grammar::qname_parser::create_qname(context.qnames(), param_uri, param_qname)?;
    Ok(Sequence::qname(qname))
}

// 10.2.3 fn:local-name-from-QName
// fn:local-name-from-QName($arg as xs:QName?) as xs:NCName?
fn local_name_from_qname<T: Ref>(
    args: &[Sequence<T>],
    _context: &Context<T>,
) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_qname)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty_string()),
    };
    Ok(Sequence::one(Item::Atomic(Atomic::any_uri(
        arg.to_ref().local_name().to_string(),
    ))))
}

// 10.2.4 fn:namespace-uri-from-QName
// fn:namespace-uri-from-QName($arg as xs:QName?) as xs:anyURI?
fn namespace_uri_from_qname<T: Ref>(
    args: &[Sequence<T>],
    _context: &Context<T>,
) -> Result<Sequence<T>> {
    let arg = match optional_arg(&args[0], item_as_qname)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty_string()),
    };
    Ok(Sequence::one(Item::Atomic(Atomic::any_uri(
        arg.to_ref().namespace().to_string(),
    ))))
}

// 10.2.6 fn:in-scope-prefixes
// fn:in-scope-prefixes($element as element()) as xs:string*
fn in_scope_prefixes<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _element = arg(&args[0], item_as_element)?;
    Err(Error::xxxx(12))
}

// 13.1 fn:name
// fn:name() as xs:string
// fn:name($arg as node()?) as xs:string
fn name<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    name_impl(node)
}
fn name_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = match optional_arg(&args[0], item_as_node)? {
        Some(node) => node,
        None => return Ok(Sequence::string(String::new())),
    };
    name_impl(node)
}
fn name_impl<T: Ref>(node: &Node<T>) -> Result<Sequence<T>> {
    if let Some(qname) = node.node_name() {
        let qname = qname.to_ref();
        let prefix = qname.prefix();
        let name = if prefix.is_empty() {
            qname.local_name().to_string()
        } else {
            format!("{}:{}", prefix, qname.local_name())
        };
        Ok(Sequence::string(name))
    } else {
        Ok(Sequence::empty_string())
    }
}

// 13.3 fn:namespace-uri
// fn:namespace-uri() as xs:anyURI
// fn:namespace-uri($arg as node()?) as xs:anyURI
fn namespace_uri<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    namespace_uri_impl(node)
}
fn namespace_uri_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = match optional_arg(&args[0], item_as_node)? {
        Some(node) => node,
        None => return Ok(Sequence::any_uri(String::new())),
    };
    namespace_uri_impl(node)
}
fn namespace_uri_impl<T: Ref>(node: &Node<T>) -> Result<Sequence<T>> {
    Ok(Sequence::any_uri(
        if let Some(namespace) = node
            .node_name()
            .map(|qname| qname.to_ref().namespace().to_string())
        {
            namespace
        } else {
            String::new()
        },
    ))
}

// 13.4 fn:lang
// fn:lang($testlang as xs:string?) as xs:boolean
// fn:lang($testlang as xs:string?, $node as node()) as xs:boolean
fn lang<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let testlang = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    let node = context_item(context, item_as_node)?;
    lang_impl(testlang, node)
}
fn lang_2<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let testlang = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    let node = arg(&args[1], item_as_node)?;
    lang_impl(testlang, node)
}
fn lang_impl<T: Ref>(_testlang: &str, _node: &Node<T>) -> Result<Sequence<T>> {
    Err(Error::xxxx(13))
}

// 13.5 fn:root
// fn:root() as node()
// fn:root($arg as node()?) as node()?
fn root<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    Ok(Sequence::one(Item::Node(node.root_node())))
}
fn root_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = match optional_arg(&args[0], item_as_node)? {
        Some(node) => node,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::one(Item::Node(node.root_node())))
}

// 13.6 fn:path
// fn:path() as xs:string?
// fn:path($arg as node()?) as xs:string?
fn path<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    path_impl(node)
}
fn path_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = match optional_arg(&args[0], item_as_node)? {
        Some(node) => node,
        None => return Ok(Sequence::empty()),
    };
    path_impl(node)
}
fn path_impl<T: Ref>(node: &Node<T>) -> Result<Sequence<T>> {
    Ok(Sequence::string(
        if node.node_kind() == NodeKind::Document {
            "/".to_string()
        } else {
            let mut path = String::new();
            let nodes: Vec<_> = node.ancestors_or_self().collect();
            for n in nodes.iter().rev() {
                if let Some(name) = n.node_name() {
                    let name = name.to_ref();
                    path.push_str("/Q{");
                    path.push_str(name.namespace().as_str());
                    path.push('}');
                    path.push_str(name.local_name().as_str());
                    path.push_str("[1]"); // TODO
                }
            }
            path
        },
    ))
}

// 14.1.1 fn:empty
// fn:empty($arg as item()*) as xs:boolean
fn empty<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Ok(Sequence::boolean(args[0].is_empty()))
}

// 14.1.2 fn:exists
// fn:exists($arg as item()*) as xs:boolean
fn exists<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Ok(Sequence::one(Item::Atomic(Atomic::boolean(
        !args[0].is_empty(),
    ))))
}

// 14.1.3 fn:head
// fn:head($arg as item()*) as item()?
fn head<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = &args[0];
    Ok(if arg.is_empty() {
        Sequence::empty()
    } else {
        Sequence::one(arg.get(0).clone())
    })
}

// 14.1.4 fn:tail
// fn:tail($arg as item()*) as item()?
fn tail<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = &args[0];
    Ok(if arg.is_empty() {
        Sequence::empty()
    } else {
        let mut v = Vec::with_capacity(arg.len() - 1);
        for i in 1..arg.len() {
            v.push(arg.get(i).clone());
        }
        Sequence::many(v)
    })
}

// 14.1.9 fn:unordered
// fn:unordered($sourceSeq as item()*) as item()*
fn unordered<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Ok(args[0].clone())
}

// 14.2.1 fn:distinct-values
// fn:distinct-values($arg as xs:anyAtomicType*) as xs:anyAtomicType*
// fn:distinct-values($arg as xs:anyAtomicType*,
//                    $collation as xs:string) as xs:anyAtomicType*
fn distinct_values<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    if args[0].is_empty() {
        Ok(Sequence::empty())
    } else {
        let mut collation = "";
        if args.len() > 1 {
            collation = optional_arg(&args[1], item_as_string)?.unwrap_or("");
        }
        let mut dv: Vec<Item<_>> = Vec::with_capacity(args[0].len());
        for i in &args[0] {
            if let Item::Atomic(_) = i {
                let mut contains = false;
                for v in &dv {
                    if v.deep_equal(i, collation)? {
                        contains = true;
                        break;
                    }
                }
                if !contains {
                    dv.push(i.clone());
                }
            } else {
                return Err(Error::xpty(4));
            }
        }
        Ok(Sequence::many(dv))
    }
}

// 14.2.3 fn:deep-equal
// fn:deep-equal($parameter1 as item()*, $parameter2 as item()*) as xs:boolean
// fn:deep-equal($parameter1 as item()*,
//               $parameter2 as item()*,
//               $collation as xs:string) as xs:boolean
fn deep_equal<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let parameter1 = &args[0];
    let parameter2 = &args[1];
    Ok(Sequence::boolean(parameter1.deep_equal(parameter2, "")?))
}
fn deep_equal_3<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let parameter1 = &args[0];
    let parameter2 = &args[1];
    let collation = arg(&args[2], item_as_string)?;
    Ok(Sequence::boolean(
        parameter1.deep_equal(parameter2, collation)?,
    ))
}

// 14.3.3 fn:exactly-one
// fn:exactly-one($arg as item()*) as item()
fn exactly_one<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    if args[0].len() == 1 {
        Ok(Sequence::one(args[0].get(0).clone()))
    } else {
        Err(Error::forg(5))
    }
}

// 14.4.1 fn:count
// fn:count($arg as item()*) as xs:integer
fn count<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Ok(Sequence::integer(args[0].len() as i64))
}

// 14.4.2 fn:avg
// fn:avg($arg as xs:anyAtomicType*) as xs:anyAtomicType?
fn avg<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let arg = &args[0];
    if arg.is_empty() {
        Ok(Sequence::empty())
    } else if arg.len() == 1 {
        Ok(Sequence::one(arg.get(0).to_owned()))
    } else {
        let sum = sum_seq(arg)?;
        Ok(Sequence::atomic(Atomic::double(sum / arg.len() as f64)))
    }
}

// 14.4.3 fn:max
// fn:max($arg as xs:anyAtomicType*) as xs:anyAtomicType?
// fn:max($arg as xs:anyAtomicType*, $collation as xs:string) as xs:anyAtomicType?
fn max<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    /*
    let args = &args[0];
    if args.is_empty() {
        Ok(Sequence::empty())
    } else {
        let iter = args.iter();
        let v = iter.next().unwrap();
        let mut v = if let Item::Atomic(a) = v {
            a
        } else {
            return Err(Error::xpty(4));
        };
        for i in iter {
            if let Item::Atomic(a) = i {
                if a > v {
                    v = a;
                }
            }
        }
        Ok(Sequence::one(Item::Atomic(v.clone())))
    }
    */
    unimplemented()
}
fn max_2<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    max(args, context)
}

// 14.4.4 fn:max
// fn:min($arg as xs:anyAtomicType*) as xs:anyAtomicType?
// fn:min($arg as xs:anyAtomicType*, $collation as xs:string) as xs:anyAtomicType?
fn min<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    unimplemented()
}
fn min_2<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    max(args, context)
}

// 14.4.5 fn:sum
// fn:sum($arg as xs:anyAtomicType*) as xs:anyAtomicType
// fn:sum($arg as xs:anyAtomicType*,
//        $zero as xs:anyAtomicType?) as xs:anyAtomicType?
fn sum<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    sum_impl(&args[0], Some(&Atomic::integer(0)))
}
fn sum_2<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let zero = optional_arg(&args[1], item_as_atomic)?;
    sum_impl(&args[0], zero)
}
fn sum_impl<T: Ref>(arg: &Sequence<T>, zero: Option<&Atomic>) -> Result<Sequence<T>> {
    if arg.is_empty() {
        if let Some(zero) = zero {
            Ok(Sequence::one(Item::Atomic(zero.to_owned())))
        } else {
            Ok(Sequence::empty())
        }
    } else if arg.len() == 1 {
        Ok(Sequence::one(arg.get(0).to_owned()))
    } else {
        Ok(Sequence::atomic(Atomic::double(sum_seq(arg)?)))
    }
}
fn sum_seq<T: Ref>(arg: &Sequence<T>) -> Result<f64> {
    let mut sum = 0.;
    for i in arg {
        if let Item::Atomic(a) = i {
            if let Some(v) = a.as_double() {
                sum += v;
                continue;
            }
        }
        return Err(Error::xpty(4));
    }
    Ok(sum)
}

// 14.5.1 fn:id
// fn:id($arg as xs:string*) as element()*
// fn:id($arg as xs:string*, $node as node()) as element()*
fn id<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    id_impl(args, node)
}
fn id_2<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = arg(&args[1], item_as_node)?;
    id_impl(args, node)
}
fn id_impl<T: Ref>(_args: &[Sequence<T>], node: &Node<T>) -> Result<Sequence<T>> {
    if node.root_node().node_kind() != NodeKind::Document {
        return Err(Error::fodc(1));
    }
    // TODO
    Ok(Sequence::empty())
}

// 14.5.3 fn:id
// fn:idref($arg as xs:string*) as element()*
// fn:idref($arg as xs:string*, $node as node()) as node()*
fn idref<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let node = context_item(context, item_as_node)?;
    idref_impl(args, node)
}
fn idref_2<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let node = arg(&args[1], item_as_node)?;
    idref_impl(args, node)
}
fn idref_impl<T: Ref>(_args: &[Sequence<T>], node: &Node<T>) -> Result<Sequence<T>> {
    if node.root_node().node_kind() != NodeKind::Document {
        return Err(Error::fodc(1));
    }
    // TODO
    Ok(Sequence::empty())
}

// 14.6.1 fn:doc
// fn:doc($uri as xs:string?) as document-node()?
fn doc<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let uri = match optional_arg(&args[0], item_as_string)? {
        Some(uri) => uri,
        None => return Ok(Sequence::empty()),
    };
    // todo: add loaded document to loaded documents so it has the same id
    let uri = match Url::parse(uri) {
        Ok(uri) => uri,
        Err(url::ParseError::RelativeUrlWithoutBase) => {
            if let Some(base) = context.base_uri() {
                match base.join(uri) {
                    Ok(uri) => uri,
                    Err(_) => return Err(Error::fodc(5)),
                }
            } else {
                return Err(Error::fodc(5));
            }
        }
        Err(_) => return Err(Error::fodc(5)),
    };
    if let Ok(path) = uri.to_file_path() {
        if !path.exists() {
            return Err(Error::fodc(2));
        }
        match context.read_xml(&path) {
            Ok(doc) => Ok(Sequence::one(Item::Node(Node::root(doc)))),
            Err(_) => Err(Error::fodc(2)),
        }
    } else {
        unimplemented()
    }
}

// 14.6.3 fn:collection
// fn:collection() as item()*
// fn:collection($arg as xs:string?) as item()*
fn collection<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Err(Error::fodc(2))
}
fn collection_1<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _arg = optional_arg(&args[0], item_as_string)?;
    Err(Error::fodc(2))
}

// 14.6.5 fn:unparsed-text
// fn:unparsed-text($href as xs:string?) as xs:string?
// fn:unparsed-text($href as xs:string?, $encoding as xs:string) as xs:string?
fn unparsed_text<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _href = match optional_arg(&args[0], item_as_string)? {
        Some(href) => href,
        None => return Ok(Sequence::empty()),
    };
    Err(Error::fodc(2))
}
fn unparsed_text_2<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    unparsed_text(args, context)
}

// 14.7.1 fn:parse-xml
// fn:parse-xml($arg as xs:string?) as document-node(element(*))?
fn parse_xml<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let _arg = match optional_arg(&args[0], item_as_string)? {
        Some(arg) => arg,
        None => return Ok(Sequence::empty()),
    };
    Err(Error::xxxx(14))
}

fn unimplemented<T: Ref>() -> Result<Sequence<T>> {
    Ok(Sequence::one(Item::Atomic(Atomic::string(String::from(
        "unimplemented!()",
    )))))
}

// 14.7.3 fn:serialize
// fn:serialize($arg as item()*) as xs:string
// fn:serialize($arg as item()*, $params as item()?) as xs:string
fn serialize<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let mut s = String::new();
    for item in &args[0] {
        match item {
            Item::Node(node) => {
                if node.node_kind() == NodeKind::Attribute {
                    return Err(Error::senr(1));
                }
                let v = xust_xml::write::node_to_string(node, true, false);
                match v {
                    Ok(v) => s.push_str(&v),
                    Err(_) => {
                        return Err(Error::xxxx(15));
                    }
                }
            }
            _ => {
                return unimplemented();
            }
        }
    }
    Ok(Sequence::string(s))
}
fn serialize_2<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    Err(Error::xxxx(16))
}

// 15.1 fn:position
// fn:position() as xs:integer
fn position<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    if let Some(item) = &context.item {
        Ok(Sequence::integer(item.position as i64))
    } else {
        Err(Error::xpdy(2))
    }
}

// 15.2 fn:last
// fn:last() as xs:integer
fn last<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    if let Some(item) = &context.item {
        Ok(Sequence::integer(item.size as i64))
    } else {
        Err(Error::xpdy(2))
    }
}

// 16.1.1 fn:function-lookup
// fn:function-lookup($name as xs:QName, $arity as xs:integer) as function(*)?
fn function_lookup<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let name = arg(&args[0], item_as_qname)?;
    let arity = arg(&args[1], item_as_integer)?;
    Ok(if let Some(f) = context.find_function(name, arity) {
        Sequence::one(Item::Function(f))
    } else {
        // TODO: look in user functions
        Sequence::empty()
    })
}

// 16.2.3 fn:fold-left
// fn:fold-left($seq  as item()*,
//              $zero as item()*,
//              $f    as function(item()*, item()) as item()*) as item()*
fn fold_left<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    unimplemented()
}

// 16.2.6 fn:sort
// fn:sort($input as item()*) as item()*
// fn:sort($input as item()*, $collation as xs:string?) as item()*
// fn:sort($input as item()*,
//         $collation as xs:string?,
//         $key as function(item()) as xs:anyAtomicType*) as item()*
fn sort<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let input = arg(&args[0], item_as_item)?;
    sort_impl(input, "", context)
}
fn sort_2<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let input = arg(&args[0], item_as_item)?;
    let collation = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    sort_impl(input, collation, context)
}
fn sort_3<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    let input = arg(&args[0], item_as_item)?;
    let collation = optional_arg(&args[0], item_as_string)?.unwrap_or("");
    sort_impl(input, collation, context)
}
fn sort_impl<T: Ref>(
    _input: &Item<T>,
    _collation: &str,
    _context: &Context<T>,
) -> Result<Sequence<T>> {
    unimplemented()
}

// 16.3.1 fn:load-xquery-module
// fn:load-xquery-module($module-uri as xs:string) as map(*)
// fn:load-xquery-module($module-uri as xs:string, $options as map(*)) as map(*)
fn load_xquery_module<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let module_uri = arg(&args[0], item_as_string)?;
    if module_uri.is_empty() {
        return Err(Error::foqm(1));
    }
    unimplemented()
}

// 17.1.2 map:merge
// map:merge($maps as map(*)*) as map(*)
// map:merge($maps as map(*)*, $options as map(*)) as map(*)
fn merge<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let maps = &args[0];
    merge_impl(maps, &Map::empty())
}
fn merge_2<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let maps = &args[0];
    merge_impl(maps, &Map::empty())
}
fn merge_impl<T: Ref>(maps: &Sequence<T>, _options: &Map<T>) -> Result<Sequence<T>> {
    if maps.is_empty() {
        return Ok(Sequence::map(Map::empty()));
    }
    let map = Map::empty();
    for _m in maps {}
    Ok(Sequence::map(map))
}

// 17.1.9 map:entry
// map:entry($key as xs:anyAtomicType, $value as item()*) as map(*)
fn entry<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let key = arg(&args[0], item_as_atomic)?.clone();
    let value = args[1].clone();
    Ok(Sequence::map(Map::entry(key, value)))
}

// 17.1.11 map:for-each
// map:for-each($map as map(*),
//              $action as function(xs:anyAtomicType, item()*) as item()*
//             ) as item()*
fn for_each<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    unimplemented()
}

// 17.3.1 array:size
// array:size($array as array(*)) as xs:integer
fn array_size<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let array = arg(&args[0], item_as_array)?;
    Ok(Sequence::integer(array.len() as i64))
}

// 17.3.2 array:get
// array:get($array as array(*), $position as xs:integer) as item()*
fn array_get<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    let array = arg(&args[0], item_as_array)?;
    let position = arg(&args[1], item_as_integer)?;
    array_get_impl(array, position)
}
pub fn array_get_impl<T: Ref>(array: &Array<T>, position: i64) -> Result<Sequence<T>> {
    if position < 1 {
        return Err(Error::foay(1));
    }
    let position = position as usize;
    if position > array.len() {
        return Err(Error::foay(1));
    }
    Ok(array[position - 1].clone())
}

// 18.4 xs:error
// xs:error($arg as xs:anyAtomicType?) as xs:error?
fn xs_error<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    if args[0].is_empty() {
        return Ok(Sequence::empty());
    }
    Err(Error::forg(1))
}

fn xs_cast<T: Ref>(
    args: &[Sequence<T>],
    f: impl Fn(&Atomic) -> Result<Atomic>,
) -> Result<Sequence<T>> {
    assert_eq!(args.len(), 1);
    let atomic = match optional_arg(&args[0], item_as_atomic)? {
        Some(atomic) => atomic,
        None => return Ok(Sequence::empty()),
    };
    Ok(Sequence::atomic(f(atomic)?))
}

fn cast_item_to_atomic<A, T: Ref>(item: &Item<T>, f: fn(&Atomic) -> Result<A>) -> Result<A> {
    if let Item::Atomic(atomic) = item {
        f(atomic)
    } else {
        Err(Error::xpdy(4))
    }
}
/*
/// get a primitive as defined in
/// 19.1 Casting from primitive types to primitive types
pub(crate) fn get_cast_primitive_type(types: &Types, mut r#type: TypeId) -> TypeId {
    while r#type > XS_ANY_ATOMIC_TYPE {
        r#type = types.get_base_type(r#type);
    }
    r#type
}
*/

/// See 3.18.5 Constructor Functions
/// <https://www.w3.org/TR/xquery-31/#dt-constructor-function>
/// and 19.1 Casting from primitive types to primitive types
/// <https://www.w3.org/TR/xpath-functions/#casting-from-primitive-to-primitive>
///
/// First check if the `Value` for the desired type can be created from the
/// given Atomic.
///
/// This function only parses single values. TODO: create a function for parsing
/// lists or expand this function.
fn constructor<T: Ref>(
    target_type: TypeId,
    _target_primitive_type: PrimitiveBaseType,
    args: &[Sequence<T>],
    context: &Context<T>,
) -> Result<Sequence<T>> {
    assert_eq!(args.len(), 1);
    let src = match optional_arg(&args[0], item_as_atomic)? {
        Some(atomic) => atomic,
        None => return Ok(Sequence::empty()),
    };
    let mut items = Vec::with_capacity(1);
    if let Some(s) = src.as_str() {
        context
            .types()
            .parse(
                target_type,
                s,
                &mut |value, _| items.push(Item::Atomic(value)),
                context.qname_parser(),
            )
            .map_err(|_| Error::fodt(1))?;
    } else {
        items.push(Item::Atomic(context.types().cast(
            target_type,
            src,
            context.qname_parser(),
        )?));
    };
    Ok(Sequence::many(items))
}
/*
fn constructor<T: Ref>(
    target_type: TypeId,
    args: &[Sequence<T>],
    context: &Context<T>,
) -> Result<Sequence<T>> {
    constructor(target_type, target_type, args, context)
}
fn constructor_derived<T: Ref>(
    target_type: TypeId,
    args: &[Sequence<T>],
    context: &Context<T>,
) -> Result<Sequence<T>> {
    let primitive_type = get_cast_primitive_type(context.types(), target_type);
    constructor(target_type, primitive_type, args, context)
}
*/

// untypedAtomic
fn xs_untyped_atomic<T: Ref>(args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    xs_cast(args, |atomic| Ok(atomic.clone()))
}
// dateTime
fn xs_date_time<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_DATE_TIME, DATE_TIME, args, context)
}

// dateTimeStamp
// date
fn xs_date<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_DATE, DATE, args, context)
}

// time
fn xs_time<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_TIME, TIME, args, context)
}

// duration
fn xs_duration<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_DURATION, DURATION, args, context)
}

// yearMonthDuration
fn xs_year_month_duration<T: Ref>(
    args: &[Sequence<T>],
    context: &Context<T>,
) -> Result<Sequence<T>> {
    constructor(XS_YEAR_MONTH_DURATION, DURATION, args, context)
}

// dayTimeDuration
fn xs_day_time_duration<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_DAY_TIME_DURATION, DURATION, args, context)
}

// float
fn xs_float<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_FLOAT, FLOAT, args, context)
}

// double
fn xs_double<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_DOUBLE, DOUBLE, args, context)
}

// decimal
fn xs_decimal<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_DECIMAL, DECIMAL, args, context)
}

// integer
fn xs_integer<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_INTEGER, INTEGER, args, context)
}

// nonPositiveInteger
// negativeInteger
// long
fn xs_long<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_LONG, INTEGER, args, context)
}

// int
fn xs_int<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_INT, INTEGER, args, context)
}

// short
fn xs_short<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_SHORT, INTEGER, args, context)
}

// byte
fn xs_byte<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_BYTE, INTEGER, args, context)
}

// nonNegativeInteger
// unsignedLong
// unsignedInt
// unsignedShort
// unsignedByte
// positiveInteger
// gYearMonth
fn xs_g_year_month<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_G_YEAR_MONTH, G_YEAR_MONTH, args, context)
}

// gYear
fn xs_g_year<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_G_YEAR, G_YEAR, args, context)
}

// gMonthDay
fn xs_g_month_day<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_G_MONTH_DAY, G_MONTH_DAY, args, context)
}

// gDay
fn xs_g_day<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_G_DAY, G_DAY, args, context)
}

// gMonth
fn xs_g_month<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_G_MONTH, G_MONTH, args, context)
}

// string
fn xs_string<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_STRING, STRING, args, context)
}

// normalizeString
// token
fn xs_token<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_TOKEN, STRING, args, context)
}
// language
// NMTOKEN
// Name
fn xs_name<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_NAME, STRING, args, context)
}

// NCName
fn xs_ncname<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_NCNAME, STRING, args, context)
}

// ID
fn xs_id<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_ID, STRING, args, context)
}

// IDREF
fn xs_idref<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_IDREF, STRING, args, context)
}

// ENTITY
fn xs_entity<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_ENTITY, STRING, args, context)
}

// boolean
fn xs_boolean<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_BOOLEAN, BOOLEAN, args, context)
}

// base64Binary
fn xs_base64_binary<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_BASE64_BINARY, BASE64_BINARY, args, context)
}

// hexBinary
fn xs_hex_binary<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_HEX_BINARY, HEX_BINARY, args, context)
}

// anyURI
fn xs_any_uri<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    constructor(XS_ANY_URI, ANY_URI, args, context)
}

// QName
fn xs_qname<T: Ref>(args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    xs_cast(args, |atomic| {
        if atomic.as_qname().is_some() {
            return Ok(atomic.clone());
        }
        let s = if let Some(s) = atomic.as_str() {
            s
        } else {
            return Err(Error::xpty(4));
        };
        if let Some(qname) = context.qname_parser().parse_qname(s) {
            return Ok(Atomic::qname(qname));
        }
        Err(Error::forg(1))
    })
}

// NOTATION

macro_rules! tmpfn {
    ($( $n:ident )*) => { $(
        fn $n<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
            unimplemented()
        } )*
    }
}

tmpfn!(
json_to_xml json_doc unparsed_text_lines put reverse
keys local_name map_get map_size namespace_uri_for_prefix
parse_json remove starts_with subsequence year_from_date xs_language
adjust_date_time_to_timezone format_date_time format_date_time_5
);
