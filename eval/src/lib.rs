pub mod deep_equal;
pub mod eval;
pub mod r#fn;
pub mod xdm;
use std::fmt::Debug;
use std::ops::Deref;
use xust_tree::node::Tree;
use xust_xsd::atomic::Atomic;

/// limited version of `xust_tree::node::Ref` that is restricted to
/// Atomic typed values
pub trait Ref: xust_tree::node::Ref<Tree = Self::AtomicTree> {
    type AtomicTree: Tree<TypedValue = Atomic> + ?Sized;
}

impl<T, R> Ref for R
where
    T: Tree<TypedValue = Atomic> + ?Sized,
    R: Deref<Target = T> + Clone + Debug,
{
    type AtomicTree = T;
}
