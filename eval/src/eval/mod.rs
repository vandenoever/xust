mod construct;
pub mod context;
mod flwor;

use crate::{
    r#fn::{arg, array_get_impl, concat_impl, item_as_integer},
    xdm::{
        chute::{Chute, ChuteItem},
        Array, Item, Map, Sequence, SequenceIter,
    },
    Ref,
};
use construct::{
    comp_construct_attribute, comp_construct_element, comp_construct_pi, construct_comment,
    construct_document, construct_element, construct_pi, construct_text,
};
use context::Context;
use flwor::eval_flwor;
use std::fmt::Write;
use xust_grammar::{
    ArrowFunctionCall, Axis, AxisStep, Binary, BinaryOp, Comp, ElementTest, Expr, ExprIterator,
    ExprRef, Function, FunctionCall, If, ItemType, KindTest, NodeTest, OccurrenceIndicator,
    Postfix, PostfixItem, SequenceType, VarRef,
};
use xust_tree::{
    node::{Element, IterTypedValue, Node, NodeIterator, NodeKind, TypedValueIterator},
    qnames::{NCName, Namespace},
};
use xust_xsd::atomic::{
    self, eq_literal, greater_literal, greater_or_equal_literal, less_literal,
    less_or_equal_literal, not_eq_literal, Atomic,
};
use xust_xsd::{
    atomic::{safe_add, safe_div, safe_idiv, safe_mod, safe_mul, safe_sub},
    xpath_error::{Error, Result},
};

pub fn eval_xquery<T: Ref>(context: &mut Context<T>) -> Result<Sequence<T>> {
    let query = context.query().clone();
    let mut seq = Sequence::empty();
    eval_expr(
        &query.query_body(),
        &mut seq.chute(context.tree_builder()),
        context,
    )?;
    Ok(seq)
}

fn eval_expr<T: Ref>(
    expr: &Expr,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    match expr {
        Expr::Integer(v) => chute.push(Atomic::integer(*v).into()),
        Expr::Decimal(i) => chute.push(Atomic::decimal(*i).into()),
        Expr::Double(i) => chute.push(Atomic::double(*i).into()),
        Expr::FunctionCall(fc) => eval_function_call(fc, chute, context),
        Expr::ArrowFunctionCall(fc) => eval_arrow_function_call(fc, chute, context),
        Expr::String(s) => chute.push(Atomic::string(s.str()).into()),
        Expr::ContextItemExpr => match &context.item {
            Some(c) => chute.push(c.item.clone().into()),
            None => Err(Error::xpdy(2)),
        },
        Expr::AxisStep(axis_step) => eval_axis_step(axis_step, chute, context),
        Expr::Negate(expr) => eval_negate(expr.expr(), chute, context),
        Expr::BinaryOp(op) => eval_binary_op(op, chute, context),
        Expr::VarRef(var_ref) => eval_var_ref(var_ref, chute, context),
        Expr::SquareArrayConstructor(expr) => eval_square_array(expr, chute, context),
        Expr::CurlyArrayConstructor(expr) => eval_curly_array(expr, chute, context),
        Expr::Empty => Ok(()),
        Expr::Postfix(expr) => eval_postfix(expr, chute, context),
        Expr::CompTextConstructor(expr) => construct_text(expr, chute, context),
        Expr::CompElemConstructor(expr) => comp_construct_element(expr, chute, context),
        Expr::CompAttrConstructor(expr) => comp_construct_attribute(expr, chute, context),
        Expr::CompPIConstructor(expr) => comp_construct_pi(expr, chute, context),
        Expr::CompCommentConstructor(expr) => eval_comp_comment_constructor(expr, chute, context),
        Expr::CompDocConstructor(c) => construct_document(c, chute, context),
        Expr::DirElemConstructor(c) => construct_element(c, chute, context),
        Expr::DirCommentConstructor(c) => construct_comment(c.comment(), chute),
        Expr::DirPIConstructor(c) => construct_pi(c, chute),
        Expr::StringConstructor(expr) => eval_string_constructor(expr, chute, context),
        Expr::MapConstructor => eval_map_constructor(chute, context),
        Expr::OrderedExpr(_ordering_mode, expr) => eval_exprs(expr, chute, context),
        Expr::NamedFunctionRef { id, arity } => eval_function_ref(*id, *arity, chute, context),
        Expr::FLWORExpr(flwor) => eval_flwor(flwor, chute, context),
        Expr::If(i) => eval_if(i, chute, context),
        Expr::Function(_)
        | Expr::ContextItem
        | Expr::VarDecl
        | Expr::OrderByClause(_)
        | Expr::WindowClause(_)
        | Expr::SequenceType(_)
        | Expr::DirAttribute(_)
        | Expr::QName(_)
        | Expr::SingleType { .. }
        | Expr::QuantifiedExpr(_)
        | Expr::ArgumentPlaceholder
        | Expr::UnaryLookup => Err(Error::unimplemented_msg(format!("{:?}", expr))), // e => panic!("{:#?}", e),
    }
}
fn eval_expr_seq<T: Ref>(expr: &Expr, context: &mut Context<T>) -> Result<Sequence<T>> {
    let mut seq = Sequence::empty();
    eval_expr(expr, &mut seq.chute(context.tree_builder()), context)?;
    Ok(seq)
}

fn eval_exprs<T: Ref>(
    a: &ExprIterator,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    for e in a {
        eval_expr(&e, chute, context)?;
    }
    Ok(())
}

fn eval_var_ref<T: Ref>(
    var_ref: &VarRef,
    chute: &mut dyn Chute<T>,
    context: &Context<T>,
) -> Result<()> {
    let varid = var_ref.varid();
    if let Some(var) = context.get_var(varid) {
        chute.push(var.clone().into())
    } else {
        Err(Error::xqst(8))
    }
}

fn eval_args<T: Ref>(a: &ExprIterator, context: &mut Context<T>) -> Result<Vec<Sequence<T>>> {
    let size = a.into_iter().count();
    let mut v = Vec::with_capacity(size);
    for e in a {
        let mut seq = Sequence::empty();
        eval_expr(&e, &mut seq.chute(context.tree_builder()), context)?;
        v.push(seq);
    }
    Ok(v)
}

// each member expressions results in one array member
fn eval_square_array<T: Ref>(
    a: &ExprIterator,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    let v = eval_args(a, context)?;
    chute.push(Array::from_vec(v).into())
}
// sequences resulting from evaluating member expressions are collated into
// one sequence
fn eval_curly_array<T: Ref>(
    a: &ExprIterator,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    let mut v = Vec::new();
    for e in a {
        let mut seq = Sequence::empty();
        eval_expr(&e, &mut seq.chute(context.tree_builder()), context)?;
        v.push(seq);
    }
    chute.push(Array::from_vec(v).into())
}
fn expr_to_string<T: Ref>(
    a: &ExprIterator,
    separator: &str,
    context: &mut Context<T>,
) -> Result<String> {
    let mut s = String::new();
    let mut first = true;
    for e in a {
        let mut seq = Sequence::empty();
        eval_expr(&e, &mut seq.chute(context.tree_builder()), context)?;
        for i in &seq {
            if !first {
                s.push_str(separator);
            }
            match i {
                Item::Array(_) | Item::Map(_) => {
                    return Err(Error::foty(13));
                }
                _ => write!(&mut s, "{}", i).unwrap(),
            }
            first = false;
        }
    }
    Ok(s)
}
fn eval_string_constructor<T: Ref>(
    a: &ExprIterator,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    chute.push(expr_to_string(a, "", context)?.into())
}
fn eval_map_constructor<T: Ref>(chute: &mut dyn Chute<T>, _context: &Context<T>) -> Result<()> {
    chute.push(Item::Map(Map::empty()).into())
}
fn eval_if<T: Ref>(r#if: &If, chute: &mut dyn Chute<T>, context: &mut Context<T>) -> Result<()> {
    let s = eval_expr_seq(&r#if.r#if(), context)?;
    if s.is_true() {
        eval_expr(&r#if.r#then(), chute, context)
    } else {
        eval_expr(&r#if.r#else(), chute, context)
    }
}
fn eval_function_ref<T: Ref>(
    id: usize,
    _arity: u32,
    chute: &mut dyn Chute<T>,
    _context: &Context<T>,
) -> Result<()> {
    chute.push(Item::Function(id).into())
}
fn eval_postfix<T: Ref>(
    a: &Postfix,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    // evaluate the expression and then apply the postfix expressions
    let mut seq = Sequence::empty();
    eval_expr(&a.expr(), &mut seq.chute(context.tree_builder()), context)?;
    for e in a.into_iter() {
        match e {
            PostfixItem::Lookup(expr) => seq = eval_lookup(seq, expr, context),
            PostfixItem::LookupInteger(pos) => seq = eval_lookup_integer(&seq, pos)?,
            PostfixItem::LookupNCName(step) => seq = eval_ncname_filter(seq, step, context)?,
            PostfixItem::LookupWildcard => seq = eval_lookup_wildcard(&seq)?,
            PostfixItem::Predicate(exprs) => eval_node_filters(&mut seq, exprs, context)?,
            PostfixItem::FunctionArgs(args) => {
                seq = eval_dynamic_function_call(seq, args, context)?
            }
        }
    }
    chute.push(seq.into())
}

fn eval_comp_comment_constructor<T: Ref>(
    a: &ExprIterator,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    // todo pass the iterator to the construction function to save on allocations
    let comment = expr_to_string(a, "", context)?;
    construct_comment(&comment, chute)
}

fn eval_binary_op<T: Ref>(
    op: &Binary,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    let first = eval_expr_seq(&op.first(), context)?;
    if op.op() == BinaryOp::Treat {
        if let Expr::SequenceType(st) = op.second() {
            return chute.push(eval_treat(first, st, context)?.into());
        } else {
            return Err(Error::implementation_error());
        }
    } else if op.op() == BinaryOp::Instanceof {
        if let Expr::SequenceType(st) = op.second() {
            return chute.push(Atomic::boolean(eval_instanceof(&first, &st, context)?).into());
        } else {
            return Err(Error::implementation_error());
        }
    } else if op.op() == BinaryOp::Step {
        return eval_step(first, op.second(), chute, context);
    } else if op.op() == BinaryOp::SimpleMap {
        return eval_simple_map(op.second(), first, chute, context);
    }
    let second = eval_expr_seq(&op.second(), context)?;
    fn bool<'a, T: Ref>(b: bool) -> ChuteItem<'a, T> {
        b.into()
    }
    fn opt_bool<'a, T: Ref>(b: Option<bool>) -> ChuteItem<'a, T> {
        match b {
            Some(b) => b.into(),
            None => ChuteItem::None,
        }
    }
    fn opt_atom<'a, T: Ref>(b: Option<Atomic>) -> ChuteItem<'a, T> {
        match b {
            Some(b) => b.into(),
            None => ChuteItem::None,
        }
    }
    chute.push(match op.op() {
        BinaryOp::Comparison(cmp) => match cmp {
            Comp::Equal => general_comparison(eq_literal, &first, &second, context).map(bool),
            Comp::NotEqual => {
                general_comparison(not_eq_literal, &first, &second, context).map(bool)
            }
            Comp::Less => general_comparison(less_literal, &first, &second, context).map(bool),
            Comp::LessOrEqual => {
                general_comparison(less_or_equal_literal, &first, &second, context).map(bool)
            }
            Comp::Greater => {
                general_comparison(greater_literal, &first, &second, context).map(bool)
            }
            Comp::GreaterOrEqual => {
                general_comparison(greater_or_equal_literal, &first, &second, context).map(bool)
            }
            Comp::Eq => value_comparison(eq_literal, &first, &second, context).map(opt_bool),
            Comp::Ne => value_comparison(not_eq_literal, &first, &second, context).map(opt_bool),
            Comp::Lt => value_comparison(less_literal, &first, &second, context).map(opt_bool),
            Comp::Le => {
                value_comparison(less_or_equal_literal, &first, &second, context).map(opt_bool)
            }
            Comp::Gt => value_comparison(greater_literal, &first, &second, context).map(opt_bool),
            Comp::Ge => {
                value_comparison(greater_or_equal_literal, &first, &second, context).map(opt_bool)
            }
            Comp::Is => is(&first, &second).map(opt_bool),
            Comp::Follows => follows(&first, &second).map(opt_bool),
            Comp::Precedes => precedes(&first, &second).map(opt_bool),
        },
        BinaryOp::And => and(&first, &second).map(bool),
        BinaryOp::Or => or(&first, &second).map(bool),
        BinaryOp::Add => add(&first, &second, context).map(opt_atom),
        BinaryOp::Substract => substract(&first, &second, context).map(opt_atom),
        BinaryOp::Div => div(&first, &second, context).map(opt_atom),
        BinaryOp::IDiv => idiv(&first, &second, context).map(opt_atom),
        BinaryOp::Multiply => multiply(&first, &second, context).map(opt_atom),
        BinaryOp::Mod => modulo(&first, &second, context).map(opt_atom),
        BinaryOp::Union => return union(first, second, chute),
        BinaryOp::Range => return range(&first, &second, chute),
        BinaryOp::Comma => return comma(first, second, chute),
        BinaryOp::StringConcat => string_concat(first, second).map(|s| s.into()),
        BinaryOp::Except => Ok(except(first, second)?.into()),
        //e => panic!(format!("{:#?} {:?} {:?}", e, first, second)),
        _e => Err(Error::unimplemented()),
    }?)
}

fn union<T: Ref>(a: Sequence<T>, b: Sequence<T>, chute: &mut dyn Chute<T>) -> Result<()> {
    if !a.only_nodes() || !b.only_nodes() {
        Err(Error::xpty(4))
    } else if a.is_empty() {
        chute.push(b.into())
    } else if b.is_empty() {
        chute.push(a.into())
    } else {
        Err(Error::unimplemented())
    }
}

fn range<T: Ref>(a: &Sequence<T>, b: &Sequence<T>, chute: &mut dyn Chute<T>) -> Result<()> {
    if a.is_empty() || b.is_empty() {
        return Ok(());
    }
    if a.len() != 1 || b.len() != 1 {
        return Err(Error::xpty(4));
    }
    let (a, b) = match (a.get(0), b.get(0)) {
        (Item::Atomic(a), Item::Atomic(b)) => (a, b),
        _ => return Err(Error::xpty(4)),
    };
    let (a, b) = match (a.as_i64(), b.as_i64()) {
        (Some(a), Some(b)) => (a, b),
        _ => return Err(Error::xpty(4)),
    };
    if a > b {
        return Ok(());
    }
    if b - a > 300000000 {
        // limit the amount of memory allocated since out sequence is
        // instantiated and not dynamic
        return Err(Error::xpdy(130));
    }
    (a..=b).try_for_each(|i| chute.push(Atomic::integer(i).into()))
}

fn comma<T: Ref>(a: Sequence<T>, b: Sequence<T>, chute: &mut dyn Chute<T>) -> Result<()> {
    chute.push(a.into())?;
    chute.push(b.into())
}

fn string_concat<T: Ref>(a: Sequence<T>, b: Sequence<T>) -> Result<Sequence<T>> {
    concat_impl(&[a, b])
}

fn except<T: Ref>(mut a: Sequence<T>, b: Sequence<T>) -> Result<Sequence<T>> {
    a.retain_with_err(|item| {
        if let Item::Node(a) = item {
            for b in &b {
                if let Item::Node(b) = b {
                    if a == b {
                        return Ok(false);
                    }
                } else {
                    return Err(Error::xpty(4));
                }
            }
            Ok(true)
        } else {
            Err(Error::xpty(4))
        }
    })?;
    Ok(a)
}

fn eval_instanceof<T: Ref>(
    s: &Sequence<T>,
    st: &SequenceType,
    context: &Context<T>,
) -> Result<bool> {
    let len = s.len();
    let right_occurrence = match st.occurrence_indicator() {
        OccurrenceIndicator::Zero => len == 0,
        OccurrenceIndicator::One => len == 1,
        OccurrenceIndicator::ZeroOrOne => len < 2,
        OccurrenceIndicator::ZeroOrMore => true,
        OccurrenceIndicator::OneOrMore => len >= 1,
    };
    if !right_occurrence {
        return Ok(false);
    }
    for item in s {
        if !check_type(item, st, context)? {
            return Ok(false);
        }
    }
    Ok(true)
}

fn eval_treat<T: Ref>(
    s: Sequence<T>,
    st: SequenceType,
    context: &Context<T>,
) -> Result<Sequence<T>> {
    if eval_instanceof(&s, &st, context)? {
        Ok(s)
    } else {
        Err(Error::xpdy(50))
    }
}

fn kind_test<T: Ref>(node: &Node<T>, kt: &KindTest) -> bool {
    match kt {
        KindTest::DocumentTestNone => node.node_kind() == NodeKind::Document,
        KindTest::DocumentTestElementTest(dt) => document_test_element_test(node, dt),
        KindTest::ElementTest(et) => element_test(node, et),
        KindTest::AttributeTest(at) => element_test(node, at),
        KindTest::SchemaElementTest(_) => todo!(),
        KindTest::SchemaAttributeTest(_) => todo!(),
        KindTest::CommentTest => node.node_kind() == NodeKind::Comment,
        KindTest::AnyKindTest => true, // node()
        KindTest::TextTest => node.node_kind() == NodeKind::Text,
        KindTest::PITestNone => node.node_kind() == NodeKind::ProcessingInstruction,
        KindTest::PITestNCName { .. } => node.node_kind() == NodeKind::ProcessingInstruction,
        KindTest::PITestString { .. } => node.node_kind() == NodeKind::ProcessingInstruction,
        KindTest::NamespaceNodeTest => false,
    }
}

fn check_type<T: Ref>(item: &Item<T>, st: &SequenceType, context: &Context<T>) -> Result<bool> {
    let _xs = Namespace("http://www.w3.org/2001/XMLSchema");
    Ok(match st.item_type() {
        ItemType::KindTest(kt) => {
            if let Item::Node(node) = item {
                kind_test(node, &kt)
            } else {
                false
            }
        }
        ItemType::ArrayTest(at) => {
            if let Item::Array(a) = item {
                if let Some(sequence_type) = at.sequence_type() {
                    // all entries in the array should match
                    a.into_iter().fold(Ok(true), |acc, seq| {
                        match acc {
                            Ok(true) => {}
                            o => return o,
                        }
                        eval_instanceof(seq, &sequence_type, context)
                    })?
                } else {
                    true
                }
            } else {
                false
            }
        }
        ItemType::AtomicOrUnionType(v) => {
            if let Item::Atomic(l) = item {
                context.types().derives_from(l.type_id(), v.type_id())
            } else {
                false
            }
        }
        ItemType::MapTest => matches!(item, Item::Map(_)),
        ItemType::AnyMapTest => matches!(item, Item::Map(_)),
        ItemType::Item => true,
        ItemType::FunctionTest => true,
    })
}

fn root_element<T: Ref>(node: &Node<T>) -> Option<Element<T>> {
    node.children().find_map(|e| e.element())
}

fn document_test_element_test<T: Ref>(node: &Node<T>, dt: &ElementTest) -> bool {
    if let Some(element) = root_element(node) {
        node.node_kind() == NodeKind::Document
            && if let Some(name) = dt.name() {
                element.node_name() == name
            } else {
                true
            }
            && if dt.type_name().is_some() {
                unimplemented!()
            } else {
                true
            }
    } else {
        false
    }
}

fn element_test<T: Ref>(node: &Node<T>, dt: &ElementTest) -> bool {
    if let Some(element) = node.element() {
        (if let Some(name) = dt.name() {
            element.node_name() == name
        } else {
            true
        }) && if let Some(type_name) = dt.type_name() {
            let type_name = type_name.to_ref();
            if type_name.namespace() == Namespace("http://www.w3.org/2001/XMLSchema") {
                let local_name = type_name.local_name();
                if local_name == NCName("anyType") || local_name == NCName("untyped") {
                    return true;
                }
            }
            true
        } else {
            true
        }
    } else {
        false
    }
}

fn eval_function_call<T: Ref>(
    fc: &FunctionCall,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    if let Some(f) = fc.user_function() {
        eval_user_function(f, fc.args(), chute, context)
    } else {
        let args = eval_args(&fc.args(), context)?;
        chute.push(context.call_function(fc.id(), &args)?.into())
    }
}

fn eval_user_function<T: Ref>(
    f: Function,
    args: ExprIterator,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    let mut new_context = context.function_context(f.stack_size());
    if new_context.depth() > 100 {
        // 'stack overflow'
        return Err(Error::xxxx(0));
    }
    for (pos, a) in args.enumerate() {
        let mut seq = Sequence::empty();
        eval_expr(&a, &mut seq.chute(context.tree_builder()), context)?;
        new_context.set_var(pos as u16, seq);
    }
    eval_expr(&f.expr(), chute, &mut new_context)
}

fn eval_arrow_function_call<T: Ref>(
    fc: &ArrowFunctionCall,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    if let Some(f) = fc.user_function() {
        eval_user_function(f, fc.args(), chute, context)
    } else {
        let args = eval_args(&fc.args(), context)?;
        if let Some(id) = fc.id() {
            chute.push(context.call_function(id, &args)?.into())
        } else {
            Err(Error::unimplemented())
        }
    }
}

fn eval_dynamic_function_call<T: Ref>(
    function: Sequence<T>,
    args: ExprIterator,
    context: &mut Context<T>,
) -> Result<Sequence<T>> {
    if function.len() != 1 {
        return Err(Error::xpty(4));
    }
    match function.get(0) {
        Item::Array(a) => array_get(a, args, context),
        Item::Function(f) => {
            if let Some(f) = args.user_function(*f) {
                let mut seq = Sequence::empty();
                eval_user_function(f, args, &mut seq.chute(context.tree_builder()), context)?;
                Ok(seq)
            } else {
                let args = eval_args(&args, context)?;
                context.call_function(*f, &args)
            }
        }
        _ => Err(Error::xpty(4)),
    }
}

// concat all
fn eval_lookup_wildcard<T: Ref>(a: &Sequence<T>) -> Result<Sequence<T>> {
    let mut seq = Vec::new();
    for item in a {
        match item {
            Item::Array(a) => {
                for s in a {
                    for i in s {
                        seq.push(i.clone());
                    }
                }
            }
            Item::Map(_) => return Err(Error::xxxx(100)),
            _ => return Err(Error::xpty(4)),
        }
    }
    Ok(Sequence::many(seq))
}
fn eval_lookup_integer<T: Ref>(a: &Sequence<T>, pos: u32) -> Result<Sequence<T>> {
    if pos < 1 {
        return Err(Error::foay(1));
    }
    let mut seq = Vec::new();
    for item in a {
        match item {
            Item::Array(a) => {
                if pos as usize > a.len() {
                    return Err(Error::foay(1));
                }
                for i in a[pos as usize - 1].iter() {
                    seq.push(i.clone());
                }
            }
            Item::Map(_) => return Err(Error::xxxx(101)),
            _ => return Err(Error::xpty(4)),
        }
    }
    Ok(Sequence::many(seq))
}

fn eval_axis_step<T: Ref>(
    axis_step: &AxisStep,
    chute: &mut dyn Chute<T>,
    context: &Context<T>,
) -> Result<()> {
    // this takes the start from the context item
    let context_item = context.item.as_ref().ok_or(Error::xpdy(2))?;
    let context_node = if let Item::Node(node) = &context_item.item {
        node
    } else {
        return Err(Error::xpty(20));
    };
    let it = walk_nodes(context_node, axis_step.axis())?;
    let test = axis_step.node_test();
    let e: Sequence<T> = it
        .filter(|i| match test {
            NodeTest::QName(ref qn) => i.node_name().map(|i| &i == qn).unwrap_or(false),
            NodeTest::KindTest(ref kt) => kind_test(i, kt),
            NodeTest::Wildcard => i.node_kind() == NodeKind::Element,
            _ => true,
        })
        .collect();
    // TODO filter on predicates
    chute.push(e.into())
}

fn array_get<T: Ref>(
    array: &Array<T>,
    mut args: ExprIterator,
    context: &mut Context<T>,
) -> Result<Sequence<T>> {
    let position_expr = if let Some(position_expr) = args.next() {
        position_expr
    } else {
        return Err(Error::xpty(4));
    };
    if args.next().is_some() {
        return Err(Error::xpty(4));
    }
    let mut seq = Sequence::empty();
    eval_expr(
        &position_expr,
        &mut seq.chute(context.tree_builder()),
        context,
    )?;
    let position = arg(&seq, item_as_integer)?;
    array_get_impl(array, position)
}

fn eval_node_filters<T: Ref>(
    seq: &mut Sequence<T>,
    iter: ExprIterator,
    context: &mut Context<T>,
) -> Result<()> {
    for expr in iter {
        match expr {
            Expr::AxisStep(step) => eval_node_filter(seq, &step, context)?,
            Expr::Integer(i) => eval_postfix_integer(seq, i, context),
            expr => eval_predicate(seq, expr, context)?,
        }
    }
    Ok(())
}

fn eq_numeric_singleton<T: Ref>(seq: &Sequence<T>, position: usize) -> bool {
    if seq.len() != 1 {
        return false;
    }
    if let Item::Atomic(v) = seq.get(0) {
        v.eq_numeric_singleton(position)
    } else {
        false
    }
}

fn eval_predicate<T: Ref>(
    seq: &mut Sequence<T>,
    expr: Expr,
    context: &mut Context<T>,
) -> Result<()> {
    let mut position = 0;
    seq.retain_with_err(|item| {
        position += 1;
        let r =
            context.with_context_item(item, position, |context| eval_expr_seq(&expr, context))?;
        Ok(if eq_numeric_singleton(&r, position) {
            true
        } else {
            to_boolean(&r)?
        })
    })
}

fn eval_postfix_integer<T: Ref>(seq: &mut Sequence<T>, i: i64, _context: &Context<T>) {
    let mut pos = 0;
    seq.retain(|_| {
        pos += 1;
        pos == i
    });
}

fn eval_node_filter<T: Ref>(
    seq: &mut Sequence<T>,
    axis_step: &AxisStep,
    _context: &Context<T>,
) -> Result<()> {
    seq.retain_with_err(|item| {
        if let Item::Node(i) = item {
            // return true if there is any node
            Ok(walk_nodes(i, axis_step.axis())?.next().is_some())
        } else {
            Err(Error::xpty(20))
        }
    })
}

fn eval_ncname_filter<T: Ref>(
    seq: Sequence<T>,
    ncname: NCName,
    _context: &Context<T>,
) -> Result<Sequence<T>> {
    let mut s = Vec::new();
    for i in seq {
        if let Item::Node(i) = i {
            if i.node_name()
                .map(|qname| qname.to_ref().local_name() == ncname)
                .unwrap_or(false)
            {
                s.push(Item::Node(i));
            }
        } else {
            return Err(Error::xpty(20));
        }
    }
    Ok(Sequence::many(s))
}

fn eval_lookup<T: Ref>(seq: Sequence<T>, _expr: ExprRef, _context: &Context<T>) -> Sequence<T> {
    let s = Vec::new();
    for _i in &seq {}
    Sequence::many(s)
}

fn eval_step<T: Ref>(
    s: Sequence<T>,
    expr: Expr,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    for (pos, item) in s.into_iter().enumerate() {
        if !item.is_node() {
            return Err(Error::xpty(19));
        };
        context.with_context_item(&item, pos + 1, |context| eval_expr(&expr, chute, context))?;
    }
    Ok(())
}

fn eval_simple_map<T: Ref>(
    expr: Expr,
    s: Sequence<T>,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    for (pos, item) in s.into_iter().enumerate() {
        context.with_context_item(&item, pos + 1, |context| eval_expr(&expr, chute, context))?;
    }
    Ok(())
}

fn walk_nodes<T: Ref>(node: &Node<T>, axis: Axis) -> Result<NodeIterator<T>> {
    Ok(match axis {
        Axis::Ancestor => node.ancestors(),
        Axis::AncestorOrSelf => node.ancestors_or_self(),
        Axis::Attribute => node.attributes(),
        Axis::Child => node.children(),
        Axis::Descendant => node.descendant(),
        Axis::DescendantOrSelf => node.descendant_or_self(),
        Axis::Following => node.following(),
        Axis::FollowingSibling => node.following_siblings(),
        Axis::Namespace => return Err(Error::unimplemented()),
        Axis::Parent => node.parent_iter(),
        Axis::Preceding => node.preceding(),
        Axis::PrecedingSibling => node.preceding_siblings(),
        Axis::Self_ => node.self_iter(),
    })
}

fn eval_negate<T: Ref>(e: Expr, chute: &mut dyn Chute<T>, context: &mut Context<T>) -> Result<()> {
    let s = eval_expr_seq(&e, context)?;
    match s.len() {
        0 => Ok(()),
        1 => {
            if let Item::Atomic(v) = s.get(0) {
                chute.push(Item::Atomic(v.negate()?).into())
            } else {
                Err(Error::xpty(4))
            }
        }
        _ => Err(Error::xpty(4)),
    }
}

enum AtomIterator<'a, T: Ref> {
    Iter(SequenceIter<'a, T>),
    IterIter(SequenceIter<'a, T>, TypedValueIterator<'a, T>),
    Seq(std::vec::IntoIter<Atomic>),
}

impl<'a, T: Ref> AtomIterator<'a, T> {
    fn new(seq: &'a Sequence<T>, context: &Context<T>) -> Result<Self> {
        // if there are are any arrays, convert to vec
        let has_array = seq.iter().any(|item| matches!(item, Item::Array(_)));
        Ok(if has_array {
            Self::Seq(atomize_sequence(seq, context)?.into_iter())
        } else {
            Self::Iter(seq.iter())
        })
    }
}

fn typed_value_to_atom(v: IterTypedValue<Atomic>) -> Atomic {
    match v {
        IterTypedValue::String(_, s) => Atomic::string(s),
        IterTypedValue::TypedValue(atomic) => atomic.clone(),
    }
}

impl<'a, T: Ref> Iterator for AtomIterator<'a, T> {
    type Item = Result<Atomic>;
    fn next(&mut self) -> Option<Self::Item> {
        match self {
            AtomIterator::Iter(iter) => {
                if let Some(item) = iter.next() {
                    match item {
                        Item::Atomic(l) => return Some(Ok(l.clone())),
                        Item::Node(l) => {
                            // take the iterator out, so it can be replaced
                            let mut new_iter = SequenceIter::default();
                            std::mem::swap(iter, &mut new_iter);
                            let mut ti = l.typed_value();
                            match ti.next() {
                                None => {
                                    *self = AtomIterator::Iter(new_iter);
                                    return self.next();
                                }
                                Some(tv) => {
                                    *self = AtomIterator::IterIter(new_iter, ti);
                                    return Some(Ok(typed_value_to_atom(tv)));
                                }
                            }
                        }
                        // no arrays should be encountered, if there's an array
                        // the AtomIterator::Seq should have been taken
                        Item::Array(_) => return Some(Err(Error::implementation_error())),
                        Item::Function(_) | Item::Map(_) => return Some(Err(Error::foty(13))),
                    }
                }
            }
            AtomIterator::IterIter(iter, typed_iter) => {
                if let Some(tv) = typed_iter.next() {
                    return Some(Ok(typed_value_to_atom(tv)));
                } else {
                    // take the iterator out, so it can be replaced
                    let mut new_iter = SequenceIter::default();
                    std::mem::swap(iter, &mut new_iter);
                    *self = AtomIterator::Iter(new_iter);
                    return self.next();
                }
            }
            AtomIterator::Seq(seq) => {
                return seq.next().map(Ok);
            }
        }
        None
    }
}

fn atomize_array<T: Ref>(a: Array<T>, v: &mut Vec<Atomic>, context: &Context<T>) -> Result<()> {
    for s in a.into_iter() {
        for i in s {
            let i: Item<_> = i;
            add_atomized_item(i, v, context)?;
        }
    }
    Ok(())
}

pub(super) fn add_atomized_item<T: Ref>(
    i: Item<T>,
    v: &mut Vec<Atomic>,
    context: &Context<T>,
) -> Result<()> {
    match i {
        Item::Atomic(l) => v.push(l),
        Item::Node(l) => {
            for l in l.typed_value() {
                let atomic = typed_value_to_atom(l);
                // println!("wow! {:?} {:?}", l.node_kind(), _l);
                // return Err(Error::unimplemented());
                v.push(atomic);
            }
            // return Err(Error::foty(12));
        }
        Item::Array(a) => {
            atomize_array(a, v, context)?;
        }
        Item::Function(_) | Item::Map(_) => return Err(Error::foty(13)),
    }
    Ok(())
}

pub fn atomize_sequence<T: Ref>(a: &Sequence<T>, context: &Context<T>) -> Result<Vec<Atomic>> {
    let mut v = Vec::with_capacity(a.len());
    for i in a {
        add_atomized_item(i.clone(), &mut v, context)?;
    }
    Ok(v)
}

fn get_atoms<T: Ref>(
    a: &Sequence<T>,
    b: &Sequence<T>,
    context: &Context<T>,
) -> Result<Option<(Atomic, Atomic)>> {
    let mut a_iter = AtomIterator::new(a, context)?;
    let mut b_iter = AtomIterator::new(b, context)?;
    let a = if let Some(a) = a_iter.next() {
        a?
    } else {
        return Ok(None);
    };
    let b = if let Some(b) = b_iter.next() {
        b?
    } else {
        return Ok(None);
    };
    if a_iter.next().is_some() || b_iter.next().is_some() {
        return Err(Error::xpty(4));
    }
    Ok(Some((a, b)))
}

fn is<T: Ref>(a: &Sequence<T>, b: &Sequence<T>) -> Result<Option<bool>> {
    if a.is_empty() || b.is_empty() {
        return Ok(None);
    }
    if a.len() > 1 || b.len() > 1 {
        return Err(Error::xpty(4));
    }
    if let (Item::Node(a), Item::Node(b)) = (a.get(0), b.get(0)) {
        Ok(Some(a.is(b)))
    } else {
        Err(Error::xpty(4))
    }
}

fn follows<T: Ref>(a: &Sequence<T>, b: &Sequence<T>) -> Result<Option<bool>> {
    if a.is_empty() || b.is_empty() {
        return Ok(None);
    }
    if a.len() > 1 || b.len() > 1 {
        return Err(Error::xpty(4));
    }
    if let (Item::Node(a), Item::Node(b)) = (a.get(0), b.get(0)) {
        Ok(Some(a.follows(b)))
    } else {
        Err(Error::xpty(4))
    }
}

fn precedes<T: Ref>(a: &Sequence<T>, b: &Sequence<T>) -> Result<Option<bool>> {
    if a.is_empty() || b.is_empty() {
        return Ok(None);
    }
    if a.len() > 1 || b.len() > 1 {
        return Err(Error::xpty(4));
    }
    if let (Item::Node(a), Item::Node(b)) = (a.get(0), b.get(0)) {
        Ok(Some(a.precedes(b)))
    } else {
        Err(Error::xpty(4))
    }
}

fn value_comparison<T: Ref>(
    f: fn(&Atomic, &Atomic) -> Result<bool>,
    a: &Sequence<T>,
    b: &Sequence<T>,
    context: &Context<T>,
) -> Result<Option<bool>> {
    if let Some((a, b)) = get_atoms(a, b, context)? {
        Ok(Some(atomic::value_comparison(f, a, b)?))
    } else {
        Ok(None)
    }
}

// xpath eq operator 'eq'
pub fn eq<T: Ref>(a: &Sequence<T>, b: &Sequence<T>, context: &Context<T>) -> Result<Sequence<T>> {
    Ok(
        if let Some(b) = value_comparison(eq_literal, a, b, context)? {
            Sequence::boolean(b)
        } else {
            Sequence::empty()
        },
    )
}

fn general_comparison<T: Ref>(
    f: fn(&Atomic, &Atomic) -> Result<bool>,
    a: &Sequence<T>,
    b: &Sequence<T>,
    context: &Context<T>,
) -> Result<bool> {
    let a = atomize_sequence(a, context)?;
    let b = atomize_sequence(b, context)?;
    for a in a {
        for b in &b {
            if f(&a, b)? {
                return Ok(true);
            }
        }
    }
    Ok(false)
}

pub(super) fn to_boolean<T: Ref>(a: &Sequence<T>) -> Result<bool> {
    if a.is_empty() {
        return Ok(false);
    }
    let i = &a.get(0);
    if i.is_node() {
        return Ok(true);
    }
    if a.len() > 1 {
        return Err(Error::forg(6));
    }
    if let Item::Atomic(l) = i {
        if let Ok(b) = l.to_boolean() {
            Ok(b)
        } else {
            println!("{:?}", l.to_boolean());
            Err(Error::forg(6))
        }
    } else {
        Err(Error::forg(6))
    }
}
macro_rules! op_fn {
    ($n:ident $op:ident) => {
        fn $n<T: Ref>(
            a: &Sequence<T>,
            b: &Sequence<T>,
            context: &Context<T>,
        ) -> Result<Option<Atomic>> {
            if let Some((a, b)) = get_atoms(a, b, context)? {
                Ok(Some($op(a, b)?))
            } else {
                Ok(None)
            }
        }
    };
}

op_fn!(add safe_add);
op_fn!(substract safe_sub);
op_fn!(div safe_div);
op_fn!(idiv safe_idiv);
op_fn!(multiply safe_mul);
op_fn!(modulo safe_mod);

fn or<T: Ref>(a: &Sequence<T>, b: &Sequence<T>) -> Result<bool> {
    Ok(to_boolean(a)? || to_boolean(b)?)
}

fn and<T: Ref>(a: &Sequence<T>, b: &Sequence<T>) -> Result<bool> {
    Ok(to_boolean(a)? && to_boolean(b)?)
}
