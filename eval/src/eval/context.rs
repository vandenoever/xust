use crate::{
    r#fn::FunctionDefinitions,
    xdm::{Item, Sequence},
    Ref,
};
use std::path::Path;
use std::rc::Rc;
use std::sync::Arc;
use url::Url;
use xust_grammar::XQuery;
use xust_tree::{
    qname_stream::{self, ItemStream, QNameTreeStream},
    qnames::{QName, QNameCollection},
};
use xust_xml::read::{decode_bytes, normalize_xml};
use xust_xsd::{
    atomic::Atomic,
    qname_parser::QNameParser,
    types::Types,
    xpath_error::{self as error, Result},
    xsd_validator::XsdValidator,
};

type Tree = xust_tree::tree::Tree<Atomic>;

// todo: replace with reference to the module info in the AST
#[derive(Debug, Default, Clone)]
pub struct Module {
    pub base_uri: Option<Url>,
}

pub struct ContextInit<T: Ref> {
    xml_reader: Rc<dyn Fn(&Path, Option<XsdValidator>) -> Result<T>>,
    parse_xml: fn(String) -> Result<T>,
    pub qnames: QNameCollection,
    pub(crate) tree_builder_builder: Rc<dyn Fn(QNameCollection) -> Box<dyn TreeBuilder<T>>>,
    fd: Rc<FunctionDefinitions<T>>,
}

impl<T: Ref> ContextInit<T> {
    fn for_query(&self, xquery: &XQuery) -> Self {
        Self {
            xml_reader: self.xml_reader.clone(),
            parse_xml: self.parse_xml,
            qnames: xquery.qnames().clone(),
            tree_builder_builder: self.tree_builder_builder.clone(),
            fd: self.fd.clone(),
        }
    }
    pub fn parse_xml(&self, xml: String) -> Result<T> {
        (self.parse_xml)(xml)
    }
    pub fn fd(&self) -> &FunctionDefinitions<T> {
        &self.fd
    }
}

impl<T: Ref> Clone for ContextInit<T> {
    fn clone(&self) -> Self {
        Self {
            xml_reader: self.xml_reader.clone(),
            parse_xml: self.parse_xml,
            qnames: self.qnames.clone(),
            tree_builder_builder: self.tree_builder_builder.clone(),
            fd: self.fd.clone(),
        }
    }
}

pub struct GlobalContext<T: Ref> {
    query: XQuery,
    // indicate which variables have been set
    set_vars: Vec<bool>,
    // global variable values
    vars: Vec<Sequence<T>>,
    pub module: Module,
    init: ContextInit<T>,
    qname_parser: QNameParser,
}

impl<T: Ref> GlobalContext<T> {
    pub fn new(init: &ContextInit<T>, query: XQuery) -> Self {
        let init = init.for_query(&query);
        let var_names = query.var_names();
        let mut set_vars = Vec::new();
        set_vars.resize(var_names.len(), false);
        let mut vars = Vec::new();
        vars.resize(var_names.len(), Sequence::empty());
        let mut qname_parser = QNameParser::new(query.qnames().clone());
        qname_parser.push_namespace_definitions(query.namespace_definitions());
        Self {
            query,
            set_vars,
            vars,
            module: Module::default(),
            init,
            qname_parser,
        }
    }
    pub fn set_base_uri(&mut self, base_uri: Option<Url>) {
        self.module.base_uri = base_uri;
    }
    pub fn set_var(&mut self, name: QName, value: Sequence<T>) {
        if let Some(pos) = self
            .query
            .var_names()
            .iter()
            .position(|n| self.init.qnames.get(*n) == name)
        {
            self.set_var_pos(pos as u16, value);
        }
    }
    fn set_var_pos(&mut self, varid: u16, value: Sequence<T>) {
        let varid = varid as usize;
        self.set_vars[varid] = true;
        self.vars[varid] = value;
    }
}

#[derive(Debug)]
pub struct ContextItem<'a, T: Ref> {
    pub item: &'a Item<T>,
    pub position: usize,
    pub size: usize,
}

pub struct Context<'a, T: Ref> {
    depth: usize,
    pub(crate) item: Option<ContextItem<'a, T>>,
    vars: Vec<Sequence<T>>,
    global: Rc<GlobalContext<T>>,
}

impl<'a, T: Ref> Context<'a, T> {
    pub fn set_context_item(&mut self, item: &'a Item<T>, position: usize) {
        self.item = Some(ContextItem {
            item,
            position,
            size: 1,
        });
    }
    pub fn set_only_item(&mut self, item: &'a Item<T>) {
        self.item = Some(ContextItem {
            item,
            position: 1,
            size: 1,
        });
    }
    pub fn base_uri(&self) -> Option<&Url> {
        self.global.module.base_uri.as_ref()
    }
    pub(crate) fn with_context_item<R>(
        &mut self,
        item: &Item<T>,
        position: usize,
        mut f: impl FnMut(&mut Context<T>) -> Result<R>,
    ) -> Result<R> {
        let item = Some(ContextItem {
            item,
            position,
            size: 1,
        });
        let mut clone = Context {
            depth: self.depth + 1,
            item,
            vars: Vec::new(),
            global: Rc::clone(&self.global),
        };
        std::mem::swap(&mut self.vars, &mut clone.vars);
        let r = f(&mut clone);
        std::mem::swap(&mut self.vars, &mut clone.vars);
        r
    }
    pub fn new(global: GlobalContext<T>) -> Result<Self> {
        let global = evaluate_global_variables(global)?;
        Ok(Self::inner_new(global.query.main_stack_size(), global))
    }
    fn inner_new(stack_size: usize, global: GlobalContext<T>) -> Self {
        let mut vars = Vec::new();
        vars.resize(stack_size, Sequence::empty());
        Self {
            depth: 0,
            item: None,
            vars,
            global: Rc::new(global),
        }
    }
    pub(crate) fn depth(&self) -> usize {
        self.depth
    }
    pub(crate) fn find_function(&self, name: &QName, arity: i64) -> Option<usize> {
        use xust_grammar::FunctionResolver;
        let name = name.to_ref();
        let ns = name.namespace();
        let name = name.local_name();
        self.global.init.fd.find(ns, name, arity as usize)
    }
    pub(crate) fn function_context(&self, stack_size: u16) -> Self {
        let mut vars = Vec::new();
        vars.resize(stack_size as usize, Sequence::empty());
        Self {
            depth: self.depth + 1,
            item: None,
            vars,
            global: self.global.clone(),
        }
    }
    pub fn call_function(&self, id: usize, args: &[Sequence<T>]) -> Result<Sequence<T>> {
        self.global.init.fd.get(id).4(args, self)
    }
    pub fn qnames(&self) -> &QNameCollection {
        &self.global.init.qnames
    }
    pub(crate) fn take_var(&mut self, varid: u16) -> Sequence<T> {
        std::mem::take(&mut self.vars[varid as usize])
    }
    pub(crate) fn var_mut(&mut self, varid: u16) -> &mut Sequence<T> {
        &mut self.vars[varid as usize]
    }
    pub(crate) fn set_var(&mut self, varid: u16, value: Sequence<T>) {
        self.vars[varid as usize] = value;
    }
    pub(crate) fn get_var(&self, varid: u16) -> Option<&Sequence<T>> {
        if varid < self.global.vars.len() as u16 {
            self.global.vars.get(varid as usize)
        } else {
            self.vars.get(varid as usize - self.global.vars.len())
        }
    }
    pub fn read_xml(&self, path: &Path) -> Result<T> {
        (self.global.init.xml_reader)(path, None)
    }
    pub(crate) fn tree_builder(&self) -> Box<dyn TreeBuilder<T>> {
        (self.global.init.tree_builder_builder)(self.global.init.qnames.clone())
    }
    pub fn query(&self) -> &XQuery {
        &self.global.query
    }
    pub fn types(&self) -> &Types {
        self.global.query.types()
    }
    pub fn qname_parser(&self) -> &QNameParser {
        &self.global.qname_parser
    }
}

fn parse_xml(xml: String) -> Result<Tree> {
    let xml = normalize_xml(xml);
    xust_xml::read::parse_xml(&xml, None, None).map_err(|_| error::Error::fodc(2))
}

fn parse_xml_rc(xml: String) -> Result<Rc<Tree>> {
    parse_xml(xml).map(Rc::new)
}

fn parse_xml_arc(xml: String) -> Result<Arc<Tree>> {
    parse_xml(xml).map(Arc::new)
}

fn read_xml(path: &Path, validator: Option<XsdValidator>) -> Result<Tree> {
    if let Some(validator) = validator {
        let bytes = std::fs::read(path).map_err(|_| error::Error::fodc(2))?;
        let xml = decode_bytes(bytes).map_err(|_| error::Error::fodc(2))?.1;
        validator
            .validate_to_tree(&xml, None)
            .map_err(|_| error::Error::fodc(2))
    } else {
        xust_xml::read::parse_xml_from_file(path, None, None).map_err(|_| error::Error::fodc(2))
    }
}

pub fn read_xml_rc(path: &Path, validator: Option<XsdValidator>) -> Result<Rc<Tree>> {
    read_xml(path, validator).map(Rc::new)
}

pub fn read_xml_arc(path: &Path, validator: Option<XsdValidator>) -> Result<Arc<Tree>> {
    read_xml(path, validator).map(Arc::new)
}

pub trait TreeBuilder<T: Ref> {
    fn take_tree(&mut self) -> qname_stream::Result<T>;
    fn reset(&mut self) {}
    fn stream(&mut self) -> Option<&mut dyn ItemStream<T>>;
}

struct RcTreeBuilder(Option<QNameTreeStream<Rc<Tree>, Tree>>, QNameCollection);

impl TreeBuilder<Rc<Tree>> for RcTreeBuilder {
    fn take_tree(&mut self) -> qname_stream::Result<Rc<Tree>> {
        if let Some(builder) = self.0.take() {
            Ok(Rc::new(builder.build()?))
        } else {
            todo!()
        }
    }
    fn reset(&mut self) {
        self.0 = Some(QNameTreeStream::tree_builder(self.1.clone()))
    }
    fn stream(&mut self) -> Option<&mut dyn ItemStream<Rc<Tree>>> {
        if let Some(is) = &mut self.0 {
            Some(is)
        } else {
            None
        }
    }
}

struct ArcTreeBuilder(Option<QNameTreeStream<Arc<Tree>, Tree>>, QNameCollection);

impl TreeBuilder<Arc<Tree>> for ArcTreeBuilder {
    fn take_tree(&mut self) -> qname_stream::Result<Arc<Tree>> {
        if let Some(builder) = self.0.take() {
            Ok(Arc::new(builder.build()?))
        } else {
            todo!()
        }
    }
    fn reset(&mut self) {
        self.0 = Some(QNameTreeStream::tree_builder(self.1.clone()))
    }
    fn stream(&mut self) -> Option<&mut dyn ItemStream<Arc<Tree>>> {
        if let Some(is) = &mut self.0 {
            Some(is)
        } else {
            None
        }
    }
}

pub fn default_tree_context_init(
    qnames: QNameCollection,
    fd: FunctionDefinitions<Rc<Tree>>,
) -> ContextInit<Rc<Tree>> {
    let xml_reader = Rc::new(read_xml_rc);
    ContextInit {
        qnames,
        tree_builder_builder: Rc::new(|qnames| Box::new(RcTreeBuilder(None, qnames))),
        xml_reader,
        parse_xml: parse_xml_rc,
        fd: Rc::new(fd),
    }
}

pub fn arc_tree_context_init(
    qnames: QNameCollection,
    fd: FunctionDefinitions<Arc<Tree>>,
) -> ContextInit<std::sync::Arc<Tree>> {
    let xml_reader = Rc::new(read_xml_arc);
    ContextInit {
        qnames,
        tree_builder_builder: Rc::new(|qnames| Box::new(ArcTreeBuilder(None, qnames))),
        xml_reader,
        parse_xml: parse_xml_arc,
        fd: Rc::new(fd),
    }
}

/**
 * Global variables should have a value at the evaluation of the main
 * expression. Initially, there is a boolean for each global variable.
 */
fn evaluate_global_variables<T: Ref>(mut context: GlobalContext<T>) -> Result<GlobalContext<T>> {
    assert_eq!(
        context.query.global_variables().count(),
        context.set_vars.len()
    );
    let query = context.query.clone();
    for (i, gv) in query.global_variables().enumerate() {
        if !context.set_vars[i] {
            if let Some(expr) = gv.expr() {
                let mut seq = Sequence::empty();
                let mut c = Context::inner_new(query.main_stack_size(), context);
                let tree_builder = c.tree_builder();
                super::eval_expr(&expr, &mut seq.chute(tree_builder), &mut c)?;
                if let Ok(c) = Rc::try_unwrap(c.global) {
                    context = c;
                } else {
                    panic!()
                }
                context.vars[i] = seq;
                context.set_vars[i] = true;
            }
        }
    }
    let all_defined = true;
    if all_defined {
        Ok(context)
    } else {
        Err(error::Error::xpst(8))
    }
}
