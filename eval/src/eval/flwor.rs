use crate::{
    eval::{eval_expr, Context},
    xdm::{chute::Chute, Sequence},
    Ref,
};
use xust_grammar::ast::proxy::{Clause, ClauseIterator, Expr, FLWORExpr, ForBinding};
use xust_xsd::xpath_error::Result;

pub fn eval_flwor<T: Ref>(
    flwor: &FLWORExpr<'_>,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    let it = flwor.clauses();
    let return_clause = flwor.return_clause();
    eval_flwor_inner(it, &return_clause, chute, context, 1)?;
    Ok(())
}

fn eval_flwor_inner<'a, T: Ref>(
    mut it: ClauseIterator<'a>,
    return_clause: &Expr<'a>,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
    pos: usize,
) -> Result<usize> {
    while let Some(clause) = it.next() {
        match clause {
            Clause::For(f) => return eval_for(it, return_clause, f, chute, context, pos),
            Clause::Let(l) => {
                let mut seq = context.take_var(l.varid());
                eval_expr(&l.expr(), &mut seq.chute(context.tree_builder()), context)?;
                context.set_var(l.varid(), seq);
            }
            Clause::Window(_) => {}
            Clause::Where(expr) => {
                let mut seq = Sequence::empty();
                eval_expr(&expr, &mut seq.chute(context.tree_builder()), context)?;
                if seq.is_false() {
                    return Ok(pos);
                }
            }
            Clause::Count(varid) => {
                context.set_var(varid, Sequence::integer(pos as i64));
            }
            Clause::Other => {}
        }
    }
    eval_expr(return_clause, chute, context)?;
    Ok(pos + 1)
}

fn eval_for<'a, T: Ref>(
    it: ClauseIterator<'a>,
    return_clause: &Expr<'a>,
    f: ForBinding<'a>,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
    mut pos: usize,
) -> Result<usize> {
    let mut seq = Sequence::empty();
    eval_expr(&f.expr(), &mut seq.chute(context.tree_builder()), context)?;
    for i in seq.into_iter() {
        let var = context.var_mut(f.varid());
        var.clear();
        var.push(i);
        pos = eval_flwor_inner(it.clone(), return_clause, chute, context, pos)?;
    }
    Ok(pos)
}
