use super::{
    atomize_sequence, context::Context, eval_expr, eval_expr_seq, eval_exprs, expr_to_string,
};
use crate::{
    xdm::chute::{Chute, ChuteItem},
    Ref,
};
use std::fmt::Write;
use xust_grammar::{
    CompConstructor, CompPIConstructor, DirElemConstructor, DirPIConstructor, Expr, ExprIterator,
    NameOrExpression,
};
use xust_tree::{
    qname_stream::Item as TreeItem,
    qnames::{NCName, Namespace, Prefix, QName},
};
use xust_xsd::xpath_error::{Error, Result};

pub(super) fn construct_document<T: Ref>(
    expr: &ExprIterator,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    chute.push(ChuteItem::TreeItem(TreeItem::StartDocument))?;
    eval_exprs(expr, chute, context)?;
    chute.push(ChuteItem::TreeItem(TreeItem::EndDocument))
}

pub(super) fn construct_element<T: Ref>(
    dec: &DirElemConstructor,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    // QName already exists in DirElemConstructor (XQuery AST) so start
    // the TreeBuilder from the QNameCollection of the AST
    let qname = dec.qname();
    chute.push(ChuteItem::TreeItem(TreeItem::StartElement { qname }))?;
    construct_element_inner(dec, chute, context)?;
    chute.push(ChuteItem::TreeItem(TreeItem::EndElement))
}
fn construct_element_inner<T: Ref>(
    dec: &DirElemConstructor,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    for expr in dec.contents() {
        match expr {
            Expr::DirAttribute(dac) => {
                let qname = dac.qname();
                chute.push(ChuteItem::TreeItem(TreeItem::StartAttribute { qname }))?;
                eval_exprs(&dac.contents(), chute, context)?;
                chute.push(ChuteItem::TreeItem(TreeItem::EndAttribute))?;
            }
            Expr::String(s) => chute.push(ChuteItem::TreeItem(TreeItem::Text(s.str())))?,
            Expr::CompCommentConstructor(expr) => {
                chute.push(ChuteItem::TreeItem(TreeItem::StartComment))?;
                eval_exprs(&expr, chute, context)?;
                chute.push(ChuteItem::TreeItem(TreeItem::EndComment))?;
            }
            Expr::CompTextConstructor(expr) => {
                construct_text(&expr, chute, context)?;
            }
            Expr::DirCommentConstructor(dcc) => {
                construct_comment(dcc.comment(), chute)?;
            }
            Expr::DirElemConstructor(dec) => {
                construct_element(&dec, chute, context)?;
            }
            Expr::DirPIConstructor(_) => {}
            expr => {
                eval_expr(&expr, chute, context)?;
            }
        }
    }
    Ok(())
}

// check if attribute is allowed according to
// 3.9.3.2 Computed Attribute Constructors
fn qname_allowed_for_attribute(qname: &QName) -> bool {
    let qname = qname.to_ref();
    let prefix = qname.prefix();
    if prefix == Prefix("xmlns") {
        return false;
    }
    let local_name = qname.local_name();
    if prefix == Prefix("") && local_name == NCName("xmlns") {
        return false;
    }
    let namespace = qname.namespace();
    if namespace == Namespace("http://www.w3.org/2000/xmlns/") {
        return false;
    }
    if namespace == Namespace("http://www.w3.org/XML/1998/namespace") {
        prefix == Prefix("xml")
    } else {
        true
    }
}

fn expression_to_string<T: Ref>(expr: Option<Expr>, context: &mut Context<T>) -> Result<String> {
    let mut s = String::new();
    if let Some(expr) = expr {
        let r = eval_expr_seq(&expr, context)?;
        let seq = atomize_sequence(&r, context)?;
        for i in &seq {
            write!(&mut s, "{}", i).unwrap();
        }
    }
    Ok(s)
}

fn comp_qname<T: Ref>(a: &CompConstructor, context: &mut Context<T>) -> Result<QName> {
    match a.qname() {
        NameOrExpression::Expression(qname_expr) => {
            let r = eval_expr_seq(&qname_expr, context)?;
            let r = atomize_sequence(&r, context)?;
            if r.len() != 1 {
                return Err(Error::xpty(4));
            }
            let item = &r[0];
            if let Some(s) = item.as_str() {
                context
                    .qname_parser()
                    .parse_qname(s)
                    .ok_or_else(|| Error::xpty(4))
            } else if let Some(q) = item.as_qname() {
                Ok(q.clone())
            } else {
                return Err(Error::xpty(4));
            }
        }
        NameOrExpression::Name(qname) => Ok(qname.to_owned()),
    }
}

pub(super) fn comp_construct_element<T: Ref>(
    a: &CompConstructor,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    let qname = comp_qname(a, context)?;
    chute.push(ChuteItem::TreeItem(TreeItem::StartElement {
        qname: qname.as_ref(),
    }))?;
    if let Some(expr) = a.expr() {
        eval_expr(&expr, chute, context)?;
    }
    chute.push(ChuteItem::TreeItem(TreeItem::EndElement))
}

pub(super) fn comp_construct_attribute<T: Ref>(
    a: &CompConstructor,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    let qname = comp_qname(a, context)?;
    if !qname_allowed_for_attribute(&qname) {
        return Err(Error::xqdy(44));
    }
    chute.push(ChuteItem::TreeItem(TreeItem::StartAttribute {
        qname: qname.as_ref(),
    }))?;
    if let Some(expr) = a.expr() {
        eval_expr(&expr, chute, context)?;
    }
    chute.push(ChuteItem::TreeItem(TreeItem::EndAttribute))
}

pub(super) fn comp_construct_pi<T: Ref>(
    a: &CompPIConstructor,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    let target = match a.target() {
        NameOrExpression::Expression(qname_expr) => {
            let r = eval_expr_seq(&qname_expr, context)?;
            let r = atomize_sequence(&r, context)?;
            if r.len() != 1 {
                return Err(Error::xpty(4));
            }
            let item = &r[0];
            if let Some(s) = item.as_str() {
                s.to_string()
            } else {
                return Err(Error::xpty(4));
            }
        }
        NameOrExpression::Name(target) => target.to_string(),
    };
    // TODO move this check down the chute
    if target.to_lowercase() == "xml" {
        return Err(Error::xqdy(64));
    }
    // TODO move this check down the chute
    let content = expression_to_string(a.content(), context)?;
    if content.contains("?>") {
        return Err(Error::xqdy(26));
    }
    chute.push(ChuteItem::TreeItem(TreeItem::StartProcessingInstruction {
        target: context
            .qnames()
            .qname(Prefix(""), Namespace(""), NCName(&target))
            .as_ref(),
    }))?;
    if let Some(expr) = a.content() {
        eval_expr(&expr, chute, context)?;
    }
    chute.push(ChuteItem::TreeItem(TreeItem::EndProcessingInstruction))
}

pub(super) fn construct_text<T: Ref>(
    expr: &ExprIterator,
    chute: &mut dyn Chute<T>,
    context: &mut Context<T>,
) -> Result<()> {
    chute.push(ChuteItem::TreeItem(TreeItem::StartText))?;
    let string = expr_to_string(expr, " ", context)?;
    chute.push(ChuteItem::TreeItem(TreeItem::String(&string)))?;
    chute.push(ChuteItem::TreeItem(TreeItem::EndText))
}

pub(super) fn construct_comment<T: Ref>(comment: &str, chute: &mut dyn Chute<T>) -> Result<()> {
    chute.push(ChuteItem::TreeItem(TreeItem::Comment(comment)))
}

/*
pub(super) fn construct_comment_from_iter<'a, T: TreeRef>(
    comment: impl Iterator<Item = Result<&'a str>>,
) -> Result<Sequence<T>> {
    let tree = TreeBuilder::comment_from_iter(comment)?;
    let node = Node::root(Rc::new(tree));
    Ok(Sequence::one(Item::Node(node)))
}
*/

pub(super) fn construct_pi<T: Ref>(c: &DirPIConstructor, chute: &mut dyn Chute<T>) -> Result<()> {
    chute.push(ChuteItem::TreeItem(TreeItem::ProcessingInstruction {
        target: c.target().as_ref(),
        content: c.content(),
    }))
}
