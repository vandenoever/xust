use super::{Array, Atomic, Item, Sequence};
use crate::eval::context::TreeBuilder;
use crate::Ref;
use xust_tree::{
    node::{Node, NodeKind},
    qname_stream::{self, Item as TreeItem, ItemStream},
    stream::Status,
};
use xust_xsd::xpath_error::{Error, Result};

#[derive(Debug)]
pub enum ChuteItem<'a, T: Ref> {
    Item(Item<T>),
    TreeItem(TreeItem<'a, T>),
    Sequence(Sequence<T>),
    None,
}

impl<'a, T: Ref> From<Item<T>> for ChuteItem<'a, T> {
    fn from(item: Item<T>) -> Self {
        ChuteItem::Item(item)
    }
}

impl<'a, T: Ref> From<TreeItem<'a, T>> for ChuteItem<'a, T> {
    fn from(item: TreeItem<'a, T>) -> Self {
        ChuteItem::TreeItem(item)
    }
}

impl<'a, T: Ref> From<Sequence<T>> for ChuteItem<'a, T> {
    fn from(item: Sequence<T>) -> Self {
        ChuteItem::Sequence(item)
    }
}

impl<'a, T: Ref, I: Into<ChuteItem<'a, T>>> From<Option<I>> for ChuteItem<'a, T> {
    fn from(item: Option<I>) -> Self {
        match item {
            Some(item) => item.into(),
            None => ChuteItem::None,
        }
    }
}

impl<'a, T: Ref> From<Atomic> for ChuteItem<'a, T> {
    fn from(item: Atomic) -> Self {
        ChuteItem::Item(Item::Atomic(item))
    }
}

impl<'a, T: Ref> From<Node<T>> for ChuteItem<'a, T> {
    fn from(item: Node<T>) -> Self {
        ChuteItem::Item(Item::Node(item))
    }
}

impl<'a, T: Ref> From<Array<T>> for ChuteItem<'a, T> {
    fn from(item: Array<T>) -> Self {
        ChuteItem::Item(Item::Array(item))
    }
}

impl<'a, T: Ref> From<String> for ChuteItem<'a, T> {
    fn from(item: String) -> Self {
        ChuteItem::Item(Item::Atomic(Atomic::string(item)))
    }
}

impl<'a, T: Ref> From<bool> for ChuteItem<'a, T> {
    fn from(item: bool) -> Self {
        ChuteItem::Item(Item::Atomic(Atomic::boolean(item)))
    }
}

pub trait Chute<T: Ref> {
    fn push(&mut self, item: ChuteItem<T>) -> Result<()>;
    fn write_str(&mut self, s: &str) -> Result<()>;
}

pub struct SequenceChute<'a, T: Ref> {
    seq: &'a mut Sequence<T>,
    tree_builder: Box<dyn TreeBuilder<T>>,
}

fn map_err(_err: qname_stream::Error) -> Error {
    Error::unimplemented()
}

impl<'a, T: Ref> SequenceChute<'a, T> {
    pub(crate) fn new(seq: &'a mut Sequence<T>, tree_builder: Box<dyn TreeBuilder<T>>) -> Self {
        Self { seq, tree_builder }
    }
    fn handle_tree_item(&mut self, item: TreeItem<T>) -> Result<()> {
        let tree_builder = self.tree_builder.stream().unwrap();
        tree_builder.next(item).map_err(map_err)?;
        /*
        match tree_builder.next(item) {
            Err(s::Error::EmptyTextAsRoot) => return Ok(()),
            Err(e) => return Err(map_err(e)),
            Ok(()) => {}
        }
        */
        if tree_builder.status() != Status::Open {
            let tree = self.tree_builder.take_tree().map_err(map_err)?;
            let node = Node::root(tree);
            let empty_text = node.node_kind() == NodeKind::Text && node.string_value().is_empty();
            if !empty_text {
                self.seq.push(Item::Node(node));
            }
        }
        Ok(())
    }
}

impl<'a, T: Ref> Chute<T> for SequenceChute<'a, T> {
    fn push(&mut self, item: ChuteItem<T>) -> Result<()> {
        if let Some(b) = self.tree_builder.stream() {
            match item {
                ChuteItem::Item(item) => add_item_to_tree(b, item)?,
                ChuteItem::TreeItem(item) => self.handle_tree_item(item)?,
                ChuteItem::Sequence(seq) => seq
                    .into_iter()
                    .try_for_each(|item| add_item_to_tree(b, item))?,
                ChuteItem::None => {}
            }
        } else {
            match item {
                ChuteItem::Item(item) => self.seq.push(item),
                ChuteItem::TreeItem(item) => {
                    self.tree_builder.reset();
                    self.handle_tree_item(item)?;
                }
                ChuteItem::Sequence(mut seq) => self.seq.append(&mut seq),
                ChuteItem::None => {}
            }
        }
        Ok(())
    }
    fn write_str(&mut self, s: &str) -> Result<()> {
        if let Some(b) = self.tree_builder.stream() {
            b.write_str(s).map_err(map_err)
        } else {
            self.seq.push(Item::Atomic(Atomic::string(s)));
            Ok(())
        }
    }
}

fn add_item_to_tree<T: Ref>(b: &mut dyn ItemStream<T>, item: Item<T>) -> Result<()> {
    match item {
        Item::Array(_) => Err(Error::xxxx(202)),
        Item::Map(_) => Err(Error::xxxx(203)),
        Item::Node(node) => b.next(TreeItem::Node(node)).map_err(map_err),
        item => {
            let s = item.to_string();
            b.next(TreeItem::String(&s)).map_err(map_err)
        }
    }
}

/*
pub struct ChuteFn<'a> {
    f: &'a mut dyn FnMut(ChuteItem) -> Result<()>,
}

impl<'a> From<&'a mut dyn FnMut(ChuteItem) -> Result<()>> for ChuteFn<'a> {
    fn from(f: &'a mut dyn FnMut(ChuteItem) -> Result<()>) -> Self {
        Self { f }
    }
}

impl<'a> Chute for ChuteFn<'a> {
    fn push(&mut self, item: ChuteItem) -> Result<()> {
        (self.f)(item)
    }
    fn write_str(&mut self, s: &str) -> Result<()> {
        (self.f)(
    }
}
*/
