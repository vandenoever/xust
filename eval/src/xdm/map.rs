use super::{Atomic, Sequence};
use crate::Ref;

#[derive(Debug, Clone, Default)]
pub struct Map<T: Ref> {
    #[allow(dead_code)]
    v: Vec<(Atomic, Sequence<T>)>,
}

impl<T: Ref> Map<T> {
    pub fn empty() -> Self {
        Self { v: Vec::new() }
    }
    pub fn entry(key: Atomic, value: Sequence<T>) -> Self {
        Self {
            v: vec![(key, value)],
        }
    }
}
