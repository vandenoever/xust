mod num;

use crate::deep_equal::DeepEqual;
use chrono::{Date, DateTime, Duration, FixedOffset, NaiveDate, NaiveDateTime, NaiveTime};
use num::SafeOps;
use rust_decimal::prelude::{FromPrimitive, ToPrimitive};
use rust_decimal::Decimal;
use std::borrow::Cow;
use std::fmt::Display;
use std::str::FromStr;
use xust_grammar::error::{Error, Result};
use xust_tree::{
    qnames::{NCName, QName, QNameCollection},
    typed_value::{TypedValue, Value},
};
use xust_xsd::{
    primitive_par_ser::write::*,
    types::{built_ins::*, primitive_base_type::*, TypeId},
};

#[derive(Clone, Debug)]
pub struct Atomic {
    value: AtomicValue,
    r#type: TypeId,
}

impl Atomic {
    pub fn untyped_atomic(value: impl Into<Box<str>>) -> Self {
        Self {
            value: AtomicValue::String(value.into()),
            r#type: XS_UNTYPED_ATOMIC,
        }
    }
    pub fn string(value: impl Into<Box<str>>) -> Self {
        Self {
            value: AtomicValue::String(value.into()),
            r#type: XS_STRING,
        }
    }
    pub fn boolean(value: bool) -> Self {
        Self {
            value: AtomicValue::Boolean(value),
            r#type: XS_BOOLEAN,
        }
    }
    pub fn integer(value: i64) -> Self {
        /*
        use std::convert::TryFrom;
        let value = if let Ok(v) = i64::try_from(value) {
            AtomicValue::Integer(v)
        } else {
            AtomicValue::UnsignedLong(value)
        };
        */
        Self {
            value: AtomicValue::Integer(value),
            r#type: XS_INTEGER,
        }
    }
    pub fn decimal(value: Decimal) -> Self {
        Self {
            value: AtomicValue::Decimal(value),
            r#type: XS_DECIMAL,
        }
    }
    pub fn float(value: f32) -> Self {
        Self {
            value: AtomicValue::Float(value),
            r#type: XS_FLOAT,
        }
    }
    pub fn double(value: f64) -> Self {
        Self {
            value: AtomicValue::Double(value),
            r#type: XS_DOUBLE,
        }
    }
    pub fn duration(value: Duration) -> Self {
        Self {
            value: AtomicValue::Duration(value),
            r#type: XS_DURATION,
        }
    }
    pub fn date_time(value: NaiveDateTime) -> Self {
        Self {
            value: AtomicValue::DateTime(value),
            r#type: XS_DATE_TIME,
        }
    }
    pub fn date_time_tz(value: DateTime<FixedOffset>) -> Self {
        Self {
            value: AtomicValue::DateTimeTZ(value),
            r#type: XS_DATE_TIME,
        }
    }
    pub fn time(value: NaiveTime) -> Self {
        Self {
            value: AtomicValue::Time(value),
            r#type: XS_TIME,
        }
    }
    pub fn time_tz(value: DateTime<FixedOffset>) -> Self {
        Self {
            value: AtomicValue::TimeTZ(value),
            r#type: XS_TIME,
        }
    }
    pub fn date(value: NaiveDate) -> Self {
        Self {
            value: AtomicValue::Date(value),
            r#type: XS_DATE,
        }
    }
    pub fn date_tz(value: Date<FixedOffset>) -> Self {
        Self {
            value: AtomicValue::DateTZ(value),
            r#type: XS_DATE,
        }
    }
    pub fn g_year_month(value: NaiveDate) -> Self {
        Self {
            value: AtomicValue::GYearMonth(value),
            r#type: XS_G_YEAR_MONTH,
        }
    }
    pub fn g_year_month_tz(value: Date<FixedOffset>) -> Self {
        Self {
            value: AtomicValue::GYearMonthTZ(value),
            r#type: XS_G_YEAR_MONTH,
        }
    }
    pub fn g_year(value: NaiveDate) -> Self {
        Self {
            value: AtomicValue::GYear(value),
            r#type: XS_G_YEAR,
        }
    }
    pub fn g_year_tz(value: Date<FixedOffset>) -> Self {
        Self {
            value: AtomicValue::GYearTZ(value),
            r#type: XS_G_YEAR,
        }
    }
    pub fn g_month_day(value: NaiveDate) -> Self {
        Self {
            value: AtomicValue::GMonthDay(value),
            r#type: XS_G_MONTH_DAY,
        }
    }
    pub fn g_month_day_tz(value: Date<FixedOffset>) -> Self {
        Self {
            value: AtomicValue::GMonthDayTZ(value),
            r#type: XS_G_MONTH_DAY,
        }
    }
    pub fn g_day(value: NaiveDate) -> Self {
        Self {
            value: AtomicValue::GDay(value),
            r#type: XS_G_DAY,
        }
    }
    pub fn g_day_tz(value: Date<FixedOffset>) -> Self {
        Self {
            value: AtomicValue::GDayTZ(value),
            r#type: XS_G_DAY,
        }
    }
    pub fn g_month(value: NaiveDate) -> Self {
        Self {
            value: AtomicValue::GMonth(value),
            r#type: XS_G_MONTH,
        }
    }
    pub fn g_month_tz(value: Date<FixedOffset>) -> Self {
        Self {
            value: AtomicValue::GMonthTZ(value),
            r#type: XS_G_MONTH,
        }
    }
    pub fn hex_binary(value: impl Into<Box<[u8]>>) -> Self {
        Self {
            value: AtomicValue::HexBinary(value.into()),
            r#type: XS_HEX_BINARY,
        }
    }
    pub fn base64_binary(value: impl Into<Box<[u8]>>) -> Self {
        Self {
            value: AtomicValue::Base64Binary(value.into()),
            r#type: XS_BASE64_BINARY,
        }
    }
    pub fn any_uri(value: impl Into<Box<str>>) -> Self {
        Self {
            value: AtomicValue::String(value.into()),
            r#type: XS_ANY_URI,
        }
    }
    pub fn qname(value: QName) -> Self {
        Self {
            value: AtomicValue::QName(value),
            r#type: XS_QNAME,
        }
    }
    pub fn notation(value: impl Into<Box<str>>) -> Self {
        Self {
            value: AtomicValue::NOTATION(value.into()),
            r#type: XS_NOTATION,
        }
    }
    pub fn token(value: impl Into<Box<str>>) -> Self {
        Self {
            value: AtomicValue::String(value.into()),
            r#type: XS_TOKEN,
        }
    }
    pub fn ncname(value: impl Into<Box<str>>) -> Self {
        Self {
            value: AtomicValue::String(value.into()),
            r#type: XS_NCNAME,
        }
    }
    pub fn type_id(&self) -> TypeId {
        self.r#type
    }
    pub fn is_true(&self) -> bool {
        if let AtomicValue::Boolean(b) = self.value {
            b
        } else {
            false
        }
    }
    pub fn is_false(&self) -> bool {
        if let AtomicValue::Boolean(b) = self.value {
            !b
        } else {
            false
        }
    }
    pub fn negate(&self) -> Result<Self> {
        Ok(match &self.value {
            AtomicValue::Integer(v) => {
                // let v = if *v == 0 { 0 } else { -v };
                Atomic::integer(*v)
            }
            AtomicValue::Decimal(v) => Atomic::decimal(-v),
            AtomicValue::Double(v) => Atomic::double(-v),
            AtomicValue::Float(v) => Atomic::float(-v),
            _e => return Err(Error::xpty(4)),
        })
    }
    pub fn abs(&self) -> Result<Self> {
        Ok(match self.value {
            AtomicValue::Double(v) => Atomic::double(v.abs()),
            AtomicValue::Integer(v) => Atomic::integer(v.abs()),
            AtomicValue::Decimal(v) => Atomic::decimal(v.abs()),
            AtomicValue::Float(v) => Atomic::float(v.abs()),
            _ => return Err(Error::xpty(4)),
        })
    }
    pub fn ceil(&self) -> Result<Self> {
        Ok(match self.value {
            AtomicValue::Double(v) => Atomic::double(v.ceil()),
            AtomicValue::Integer(v) => Atomic::integer(v),
            AtomicValue::Decimal(v) => Atomic::decimal(v.ceil()),
            AtomicValue::Float(v) => Atomic::float(v.ceil()),
            _ => return Err(Error::xpty(4)),
        })
    }
    pub fn floor(&self) -> Result<Self> {
        Ok(match self.value {
            AtomicValue::Double(v) => Atomic::double(v.floor()),
            AtomicValue::Integer(v) => Atomic::integer(v),
            AtomicValue::Decimal(v) => Atomic::decimal(v.floor()),
            AtomicValue::Float(v) => Atomic::float(v.floor()),
            _ => return Err(Error::xpty(4)),
        })
    }
    pub fn round(&self) -> Result<Self> {
        Ok(match self.value {
            AtomicValue::Double(v) => Atomic::double(round_f64(v)),
            AtomicValue::Integer(v) => Atomic::integer(v),
            // AtomicValue::Decimal(v) => Atomic::decimal(round_f64(v)),
            AtomicValue::Float(v) => Atomic::float(round_f32(v)),
            _ => return Err(Error::xpty(4)),
        })
    }
    pub fn round_2(&self, precision: i64) -> Result<Self> {
        Ok(match self.value {
            AtomicValue::Double(v) => {
                let n = 10_f64.powi(precision as i32);
                Atomic::double(round_f64(n * v) / n)
            }
            AtomicValue::Integer(v) => Atomic::integer(v),
            /*
            AtomicValue::Decimal(v) => {
                let n = 10_f64.powi(precision as i32);
                Atomic::double(round_f64(n * v) / n)
            }
            */
            AtomicValue::Float(v) => {
                let n = 10_f32.powi(precision as i32);
                Atomic::float(round_f32(n * v) / n)
            }
            _ => return Err(Error::xpty(4)),
        })
    }
    pub fn as_i64(&self) -> Option<i64> {
        match &self.value {
            AtomicValue::Integer(s) => Some(*s),
            _ => None,
        }
    }
    pub fn as_double(&self) -> Option<f64> {
        Some(match self.value {
            AtomicValue::Double(i) => i,
            AtomicValue::Float(i) => i as f64,
            AtomicValue::Integer(i) => i as f64,
            //            AtomicValue::Decimal(i) => i,
            _ => return None,
        })
    }
    pub fn as_str(&self) -> Option<&str> {
        match &self.value {
            AtomicValue::String(s) => Some(&s),
            _ => None,
        }
    }
    pub fn as_qname(&self) -> Option<&QName> {
        match &self.value {
            AtomicValue::QName(q) => Some(q),
            _ => None,
        }
    }
    pub fn eq_numeric_singleton(&self, v: usize) -> bool {
        match self.value {
            AtomicValue::Integer(i) => v as i64 == i,
            AtomicValue::Double(i) => v as f64 == i,
            AtomicValue::Float(i) => v as f32 == i,
            // AtomicValue::Decimal(i) => v == i,
            _ => false,
        }
    }
    // Convert atomic to boolean as described in fn:boolean.
    // This is different from xs:boolean.
    pub fn to_boolean(&self) -> Result<bool> {
        Ok(match &self.value {
            AtomicValue::String(s) => !s.is_empty(),
            AtomicValue::Boolean(b) => *b,
            AtomicValue::Integer(i) => *i != 0, // && !i.is_nan(),
            AtomicValue::Decimal(d) => !d.is_zero(),
            AtomicValue::Float(d) => *d != 0. && !d.is_nan(),
            AtomicValue::Double(d) => *d != 0. && !d.is_nan(),
            _ => return Err(Error::xpty(4)),
        })
    }
    /*
    fn try_untyped_to_double(self) -> Result<Atomic> {
        if self.r#type != XS_UNTYPED_ATOMIC {
            return Ok(self);
        }
        if let AtomicValue::String(s) = self.value {
            if let Ok(value) = parse_primitive::double(s.as_ref()) {
                return Ok(Atomic {
                    value: AtomicValue::Double(value),
                    r#type: XS_DOUBLE,
                });
            }
        }
        Err(Error::forg(1))
    }
    */
}
pub fn safe_add(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_add(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_add(b)?),
        PromotedNumericPair::Decimal(a, b) => Atomic::decimal(a.safe_add(b)?),
        PromotedNumericPair::Integer(a, b) => Atomic::integer(a.safe_add(b)?),
    })
}
pub fn safe_sub(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_sub(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_sub(b)?),
        PromotedNumericPair::Decimal(a, b) => {
            Atomic::decimal(a.checked_sub(b).ok_or(Error::foar(1))?)
        }
        PromotedNumericPair::Integer(a, b) => Atomic::integer(a.safe_sub(b)?),
    })
}
pub fn safe_div(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_div(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_div(b)?),
        PromotedNumericPair::Decimal(a, b) => {
            Atomic::decimal(a.checked_div(b).ok_or(Error::foar(1))?)
        }
        PromotedNumericPair::Integer(a, b) => Atomic::integer(a.safe_div(b)?),
    })
}
pub fn safe_idiv(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::integer(a.safe_idiv(b)? as i64),
        PromotedNumericPair::Float(a, b) => Atomic::integer(a.safe_idiv(b)? as i64),
        PromotedNumericPair::Decimal(a, b) => Atomic::integer(d2i64(a.safe_idiv(b)?)?),
        PromotedNumericPair::Integer(a, b) => Atomic::integer(a.safe_idiv(b)?),
    })
}
pub fn safe_mul(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_mul(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_mul(b)?),
        PromotedNumericPair::Decimal(a, b) => Atomic::decimal(a.safe_mul(b)?),
        PromotedNumericPair::Integer(a, b) => Atomic::integer(a.safe_mul(b)?),
    })
}
pub fn safe_mod(a: Atomic, b: Atomic) -> Result<Atomic> {
    Ok(match numeric_type_promotion(&a.value, &b.value)? {
        PromotedNumericPair::Double(a, b) => Atomic::double(a.safe_mod(b)?),
        PromotedNumericPair::Float(a, b) => Atomic::float(a.safe_mod(b)?),
        PromotedNumericPair::Decimal(a, b) => Atomic::decimal(a.safe_mod(b)?),
        PromotedNumericPair::Integer(a, b) => Atomic::integer(a.safe_mod(b)?),
    })
}

// round negative numbers that are equally near to integers upwards
pub(crate) fn round_f64(v: f64) -> f64 {
    let r = v.round();
    if v < 0. && v - r == 0.5 {
        r + 1.
    } else {
        r
    }
}
fn round_f32(v: f32) -> f32 {
    let r = v.round();
    if v < 0. && v - r == 0.5 {
        r + 1.
    } else {
        r
    }
}

/// Convert the source `Atomic` to `Value` such that the parser gets a string
/// to parse or the result `Value` that would come out of the parsing.
/// Follows the table in 19.1 Casting from primitive types to primitive types.
/// Any information loss, e.g. when going from dateTime to date should occur
/// here, even if the `Value` can hold that information.
pub(crate) fn cast_to_value(source: &Atomic, target: PrimitiveBaseType) -> Result<Value> {
    if let AtomicValue::String(s) = &source.value {
        return Ok(Value::String(Cow::Borrowed(s.as_ref())));
    }
    let v = match target {
        STRING | UNTYPED_ATOMIC | ANY_URI => {
            if let AtomicValue::String(s) | AtomicValue::NOTATION(s) = &source.value {
                Value::String(s.as_ref().into())
            } else {
                Value::String(format!("{}", source).into())
            }
        }
        FLOAT | DOUBLE => match &source.value {
            AtomicValue::Float(v) => Value::F64(*v as f64),
            AtomicValue::Double(v) => Value::F64(*v),
            AtomicValue::Decimal(v) => Value::F64(d2f64(*v)?),
            AtomicValue::Integer(v) => Value::F64(*v as f64),
            AtomicValue::Boolean(v) => Value::F64(if *v { 1. } else { 0. }),
            _ => return Err(Error::fodt(1)),
        },
        DECIMAL => match &source.value {
            AtomicValue::Float(v) => Value::Decimal(f322d(*v)?),
            AtomicValue::Double(v) => Value::Decimal(f642d(*v)?),
            AtomicValue::Decimal(v) => Value::Decimal(*v),
            AtomicValue::Integer(v) => Value::Decimal(Decimal::new(*v, 0)),
            AtomicValue::Boolean(v) => Value::Decimal(Decimal::new(if *v { 1 } else { 0 }, 0)),
            _ => return Err(Error::fodt(1)),
        },
        INTEGER => match &source.value {
            AtomicValue::Float(v) => Value::I64(*v as i64),
            AtomicValue::Double(v) => Value::I64(*v as i64),
            AtomicValue::Decimal(v) => Value::I64(d2i64(*v)?),
            AtomicValue::Integer(v) => Value::I64(*v),
            AtomicValue::Boolean(v) => Value::I64(if *v { 1 } else { 0 }),
            _ => return Err(Error::fodt(1)),
        },
        BOOLEAN => match &source.value {
            AtomicValue::Float(v) => Value::Boolean(*v != 0.),
            AtomicValue::Double(v) => Value::Boolean(*v != 0.),
            AtomicValue::Decimal(v) => Value::Boolean(!v.is_zero()),
            AtomicValue::Integer(v) => Value::Boolean(*v != 0),
            AtomicValue::Boolean(v) => Value::Boolean(*v),
            _ => return Err(Error::fodt(1)),
        },
        DURATION => match &source.value {
            AtomicValue::Duration(v) => Value::Duration(*v),
            _ => return Err(Error::fodt(1)),
        },
        HEX_BINARY | BASE64_BINARY => match &source.value {
            AtomicValue::HexBinary(v) | AtomicValue::Base64Binary(v) => {
                Value::Binary(Cow::Borrowed(v))
            }
            _ => return Err(Error::fodt(1)),
        },
        _ => panic!(), //return Err(Error::xxxx(101)), // TODO
    };
    Ok(v)
}

pub(crate) fn atomic_to_integer(atomic: &Atomic) -> Result<i64> {
    Ok(match &atomic.value {
        AtomicValue::Boolean(b) => {
            if *b {
                1
            } else {
                0
            }
        }
        AtomicValue::String(s) => i64::from_str(s).map_err(|_e| Error::forg(1))?,
        //        AtomicValue::Decimal(d) => d as i64,
        AtomicValue::Integer(i) => *i,
        AtomicValue::Float(f) => *f as i64,
        AtomicValue::Double(d) => *d as i64,
        _ => return Err(Error::xpty(4)),
    })
}

pub(crate) fn typed_value_to_atom(typed_value: TypedValue) -> Result<Atomic> {
    match typed_value.value {
        Value::String(s) => Ok(Atomic::untyped_atomic(s)),
        _ => Err(xust_grammar::error::Error::unimplemented()),
    }
}

pub(crate) fn parsed_to_atomic(
    r#type: TypeId,
    primitive_type: PrimitiveBaseType,
    value: Value,
    qnames: &QNameCollection,
) -> Result<Atomic> {
    // the type must be a primitive value
    let value = match (primitive_type, value) {
        (STRING, Value::String(v)) => AtomicValue::String(v.into()),
        (BOOLEAN, Value::Boolean(v)) => AtomicValue::Boolean(v),
        (DECIMAL, Value::Decimal(v)) => AtomicValue::Decimal(v),
        (INTEGER, Value::I64(v)) => AtomicValue::Integer(v),
        (FLOAT, Value::F64(v)) => AtomicValue::Float(v as f32),
        (DOUBLE, Value::F64(v)) => AtomicValue::Double(v),
        (DURATION, Value::Duration(v)) => AtomicValue::Duration(v),
        (DATE_TIME, Value::DateTime(v)) => AtomicValue::DateTime(v),
        (DATE_TIME, Value::DateTimeTZ(v)) => AtomicValue::DateTimeTZ(v),
        (TIME, Value::DateTime(v)) => AtomicValue::Time(v.time()),
        (TIME, Value::DateTimeTZ(v)) => AtomicValue::TimeTZ(v),
        (DATE, Value::DateTime(v)) => AtomicValue::Date(v.date()),
        (DATE, Value::DateTimeTZ(v)) => AtomicValue::DateTZ(v.date()),
        (G_YEAR_MONTH, Value::DateTime(v)) => AtomicValue::GYearMonth(v.date()),
        (G_YEAR_MONTH, Value::DateTimeTZ(v)) => AtomicValue::GYearMonthTZ(v.date()),
        (G_YEAR, Value::DateTime(v)) => AtomicValue::GYear(v.date()),
        (G_YEAR, Value::DateTimeTZ(v)) => AtomicValue::GYearTZ(v.date()),
        (G_MONTH_DAY, Value::DateTime(v)) => AtomicValue::GMonthDay(v.date()),
        (G_MONTH_DAY, Value::DateTimeTZ(v)) => AtomicValue::GMonthDayTZ(v.date()),
        (G_DAY, Value::DateTime(v)) => AtomicValue::GDay(v.date()),
        (G_DAY, Value::DateTimeTZ(v)) => AtomicValue::GDayTZ(v.date()),
        (G_MONTH, Value::DateTime(v)) => AtomicValue::GMonth(v.date()),
        (G_MONTH, Value::DateTimeTZ(v)) => AtomicValue::GMonthTZ(v.date()),
        (HEX_BINARY, Value::Binary(v)) => AtomicValue::HexBinary(v.into()),
        (BASE64_BINARY, Value::Binary(v)) => AtomicValue::Base64Binary(v.into()),
        (QNAME, Value::QName(v)) => AtomicValue::QName(qnames.get(v).to_owned()),
        (NOTATION, Value::String(v)) => AtomicValue::NOTATION(v.into()),
        _ => return Err(Error::xxxx(71)),
    };
    Ok(Atomic { r#type, value })
}

impl DeepEqual for Atomic {
    fn deep_equal(&self, other: &Atomic, _collation: &str) -> Result<bool> {
        Ok(match (&self.value, &other.value) {
            (AtomicValue::Double(a), AtomicValue::Double(b)) => {
                if a.is_nan() && b.is_nan() {
                    true
                } else {
                    a == b
                }
            }
            (AtomicValue::Float(a), AtomicValue::Float(b)) => {
                if a.is_nan() && b.is_nan() {
                    true
                } else {
                    a == b
                }
            }
            (_, _) => eq_literal(self, other)?,
        })
    }
}

impl Atomic {
    pub fn local_name(&self) -> NCName {
        // use type id to get local_name from type register
        todo!()
    }
}

#[derive(Debug)]
enum PromotedNumericPair {
    Double(f64, f64),
    Float(f32, f32),
    Decimal(Decimal, Decimal),
    Integer(i64, i64),
}

fn same_primitive(a: &AtomicValue, b: &AtomicValue) -> bool {
    if std::mem::discriminant(a) == std::mem::discriminant(b) {
        return true;
    }
    match (a, b) {
        (AtomicValue::Decimal(_), AtomicValue::Decimal(_)) => true,
        (AtomicValue::Decimal(_), AtomicValue::Integer(_)) => true,
        (AtomicValue::Integer(_), AtomicValue::Decimal(_)) => true,
        (AtomicValue::Integer(_), AtomicValue::Integer(_)) => true,
        _ => false,
    }
}

// promote values so that they are of equal numeric type
fn numeric_type_promotion(a: &AtomicValue, b: &AtomicValue) -> Result<PromotedNumericPair> {
    Ok(match (a, b) {
        (AtomicValue::Double(a), b) => PromotedNumericPair::Double(*a, b.promote_to_double()?),
        (a, AtomicValue::Double(b)) => PromotedNumericPair::Double(a.promote_to_double()?, *b),
        (AtomicValue::Float(a), b) => PromotedNumericPair::Float(*a, b.promote_to_float()?),
        (a, AtomicValue::Float(b)) => PromotedNumericPair::Float(a.promote_to_float()?, *b),
        (AtomicValue::Decimal(a), b) => PromotedNumericPair::Decimal(*a, b.promote_to_decimal()?),
        (a, AtomicValue::Decimal(b)) => PromotedNumericPair::Decimal(a.promote_to_decimal()?, *b),
        (AtomicValue::Integer(a), AtomicValue::Integer(b)) => PromotedNumericPair::Integer(*a, *b),
        _ => return Err(Error::xpty(4)),
    })
}

// fn promoted_primitive_pair(a: &AtomicValue, b: &AtomicValue)

impl AtomicValue {
    fn promote_to_double(&self) -> Result<f64> {
        Ok(match self {
            AtomicValue::Double(v) => *v,
            AtomicValue::Float(v) => *v as f64,
            AtomicValue::Decimal(v) => v.to_f64().ok_or(Error::xpty(4))?,
            AtomicValue::Integer(v) => *v as f64,
            _ => return Err(Error::xpty(4)),
        })
    }
    fn promote_to_float(&self) -> Result<f32> {
        Ok(match self {
            AtomicValue::Float(v) => *v,
            AtomicValue::Decimal(v) => v.to_f32().ok_or(Error::xpty(4))?,
            AtomicValue::Integer(v) => *v as f32,
            _ => return Err(Error::xpty(4)),
        })
    }
    fn promote_to_decimal(&self) -> Result<Decimal> {
        Ok(match self {
            AtomicValue::Decimal(v) => *v,
            AtomicValue::Integer(v) => Decimal::new(*v, 0),
            _ => return Err(Error::xpty(4)),
        })
    }
}

fn write_float<T: num_traits::float::FloatCore + Display + Default>(
    v: &T,
    f: &mut std::fmt::Formatter,
) -> std::fmt::Result {
    if v.is_infinite() {
        if v.is_sign_negative() {
            write!(f, "-")?;
        }
        write!(f, "INF")
    } else if *v == T::default() && v.is_sign_negative() {
        write!(f, "-0")
    } else {
        write!(f, "{}", v)
    }
}

impl std::fmt::Display for Atomic {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match &self.value {
            AtomicValue::Integer(v) => write(v, f),
            AtomicValue::Decimal(v) => write(v, f),
            AtomicValue::Double(v) => write_float(v, f),
            AtomicValue::Float(v) => write_float(v, f),
            AtomicValue::String(v) => write(v, f),
            AtomicValue::Boolean(v) => write(v, f),
            AtomicValue::QName(v) => write_qname(v, f),
            AtomicValue::Time(v) => write(v, f),
            AtomicValue::TimeTZ(v) => write_time_tz(v, f),
            AtomicValue::DateTime(v) => write(v, f),
            AtomicValue::DateTimeTZ(v) => write_date_time_tz(v, f),
            AtomicValue::Date(v) => write(v, f),
            AtomicValue::DateTZ(v) => write_date_tz(v, f),
            AtomicValue::GYear(v) => write(v, f),
            e => unimplemented!("{:#?}", e),
        }
    }
}

pub(crate) fn value_comparison(
    f: fn(&Atomic, &Atomic) -> Result<bool>,
    mut a: Atomic,
    mut b: Atomic,
) -> Result<bool> {
    if a.r#type == XS_UNTYPED_ATOMIC {
        a.r#type = XS_STRING
    };
    if b.r#type == XS_UNTYPED_ATOMIC {
        b.r#type = XS_STRING
    };
    f(&a, &b)
}

macro_rules! compare_fn {
    ($n:ident $f:ident) => {
        pub(crate) fn $n(a: &Atomic, b: &Atomic) -> Result<bool> {
            $f(&a.value, &b.value)
        }
    };
}

fn d2f64(d: Decimal) -> Result<f64> {
    d.to_f64().ok_or(Error::xpty(4))
}

fn d2i64(d: Decimal) -> Result<i64> {
    d.to_i64().ok_or(Error::xpty(4))
}

fn f322d(v: f32) -> Result<Decimal> {
    Decimal::from_f32(v).ok_or(Error::fodt(1))
}

fn f642d(v: f64) -> Result<Decimal> {
    Decimal::from_f64(v).ok_or(Error::fodt(1))
}

pub fn eq_literal(a: &Atomic, b: &Atomic) -> Result<bool> {
    let (a, b) = (&a.value, &b.value);
    if same_primitive(a, b) {
        return Ok(a == b);
    }
    Ok(match numeric_type_promotion(a, b)? {
        PromotedNumericPair::Double(a, b) => a == b,
        PromotedNumericPair::Float(a, b) => a == b,
        PromotedNumericPair::Decimal(a, b) => a == b,
        PromotedNumericPair::Integer(a, b) => a == b,
    })
}

fn greater_atomic(a: &AtomicValue, b: &AtomicValue) -> Result<bool> {
    if same_primitive(a, b) {
        return Ok(a > b);
    }
    Ok(match numeric_type_promotion(a, b)? {
        PromotedNumericPair::Double(a, b) => a > b,
        PromotedNumericPair::Float(a, b) => a > b,
        PromotedNumericPair::Decimal(a, b) => a > b,
        PromotedNumericPair::Integer(a, b) => a > b,
    })
}

fn greater_or_equal_atomic(a: &AtomicValue, b: &AtomicValue) -> Result<bool> {
    if same_primitive(a, b) {
        return Ok(a >= b);
    }
    Ok(match numeric_type_promotion(a, b)? {
        PromotedNumericPair::Double(a, b) => a >= b,
        PromotedNumericPair::Float(a, b) => a >= b,
        PromotedNumericPair::Decimal(a, b) => a >= b,
        PromotedNumericPair::Integer(a, b) => a >= b,
    })
}

pub(crate) fn not_eq_literal(a: &Atomic, b: &Atomic) -> Result<bool> {
    Ok(!eq_literal(a, b)?)
}

pub(crate) fn less_literal(a: &Atomic, b: &Atomic) -> Result<bool> {
    greater_literal(b, a)
}

pub(crate) fn less_or_equal_literal(a: &Atomic, b: &Atomic) -> Result<bool> {
    greater_or_equal_literal(b, a)
}

compare_fn!(greater_literal greater_atomic);
compare_fn!(greater_or_equal_literal greater_or_equal_atomic);
