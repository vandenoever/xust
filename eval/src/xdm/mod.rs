#![allow(clippy::upper_case_acronyms)]
//pub mod atomic;
pub mod chute;
pub mod map;

use crate::eval::context::TreeBuilder;
use crate::Ref;
use chute::SequenceChute;
pub use map::Map;
use std::iter::FromIterator;
use std::ops::Index;
use xust_tree::{node::Node, qnames::QName};
use xust_xsd::atomic::Atomic;
use xust_xsd::xpath_error::Result;

#[derive(Debug, Clone)]
pub struct EQName {
    prefix: Option<String>,
    namespace: Option<String>,
    name: String,
}

impl EQName {
    pub fn new(prefix: Option<String>, namespace: Option<String>, name: String) -> EQName {
        EQName {
            prefix,
            namespace,
            name,
        }
    }
    pub fn prefix(&self) -> Option<&str> {
        self.prefix.as_deref()
    }
    pub fn namespace(&self) -> Option<&str> {
        self.namespace.as_deref()
    }
    pub fn name(&self) -> &str {
        &self.name
    }
}

/// all possible items in a sequence
#[derive(Debug)]
pub enum Item<T: Ref> {
    Atomic(Atomic),
    Node(Node<T>),
    Array(Array<T>),
    Map(Map<T>),
    Function(usize),
}

impl<T: Ref> Clone for Item<T> {
    fn clone(&self) -> Self {
        match self {
            Item::Atomic(a) => Item::Atomic(a.clone()),
            Item::Node(n) => Item::Node(n.clone()),
            Item::Array(a) => Item::Array(a.clone()),
            Item::Map(m) => Item::Map(m.clone()),
            Item::Function(f) => Item::Function(*f),
        }
    }
}

impl<T: Ref> Item<T> {
    pub fn is_node(&self) -> bool {
        matches!(self, Item::Node(_))
    }
}

impl<T: Ref> std::fmt::Display for Item<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Item::Atomic(l) => write!(f, "{}", l),
            Item::Node(n) => write!(f, "{}", n.string_value()),
            Item::Array(_) => panic!("An array does not have a string value."),
            Item::Map(_) => panic!("A map does not have a string value."),
            Item::Function(_) => {
                write!(f, "Function")
            }
        }
    }
}

#[derive(Debug)]
pub enum Sequence<T: Ref> {
    Vec(Vec<Item<T>>),
}

impl<T: Ref> Default for Sequence<T> {
    fn default() -> Self {
        Self::empty()
    }
}

impl<T: Ref> Clone for Sequence<T> {
    fn clone(&self) -> Self {
        match self {
            Sequence::Vec(v) => Sequence::Vec(v.clone()),
        }
    }
}

impl<T: Ref> Sequence<T> {
    pub fn empty() -> Sequence<T> {
        Sequence::Vec(Vec::new())
    }
    pub fn one(item: Item<T>) -> Sequence<T> {
        Sequence::Vec(vec![item])
    }
    pub fn many(seq: Vec<Item<T>>) -> Sequence<T> {
        Sequence::Vec(seq)
    }
    pub fn atomic(atomic: Atomic) -> Sequence<T> {
        Sequence::Vec(vec![Item::Atomic(atomic)])
    }
    pub fn is_empty(&self) -> bool {
        match self {
            Sequence::Vec(seq) => seq.is_empty(),
        }
    }
    pub fn len(&self) -> usize {
        match self {
            Sequence::Vec(seq) => seq.len(),
        }
    }
    pub fn get(&self, index: usize) -> &Item<T> {
        match self {
            Sequence::Vec(seq) => seq.get(index).unwrap(),
        }
    }
    pub fn into_vec(self) -> Vec<Item<T>> {
        match self {
            Sequence::Vec(seq) => seq,
        }
    }
    pub fn append(&mut self, s: &mut Sequence<T>) {
        match (self, s) {
            (Sequence::Vec(a), Sequence::Vec(ref mut b)) => a.append(b),
        }
    }
    pub fn clear(&mut self) {
        match self {
            Sequence::Vec(a) => a.clear(),
        }
    }
    pub fn push(&mut self, item: Item<T>) {
        match self {
            Sequence::Vec(a) => a.push(item),
        }
    }
    pub fn chute(&mut self, tree_builder: Box<dyn TreeBuilder<T>>) -> SequenceChute<T> {
        SequenceChute::new(self, tree_builder)
    }
    // is the sequence length 1 and contains xs:boolean value true
    pub fn is_true(&self) -> bool {
        if self.len() == 1 {
            if let Item::Atomic(a) = self.get(0) {
                return a.is_true();
            }
        }
        false
    }
    pub fn is_false(&self) -> bool {
        if self.len() == 1 {
            if let Item::Atomic(a) = self.get(0) {
                return a.is_false();
            }
        }
        false
    }
    pub fn literal(l: Atomic) -> Self {
        Self::one(Item::Atomic(l))
    }
    pub fn boolean(b: bool) -> Self {
        Self::one(Item::Atomic(Atomic::boolean(b)))
    }
    pub fn r#true() -> Self {
        Self::one(Item::Atomic(Atomic::boolean(true)))
    }
    pub fn r#false() -> Self {
        Self::one(Item::Atomic(Atomic::boolean(false)))
    }
    pub fn integer(i: i64) -> Self {
        Self::one(Item::Atomic(Atomic::integer(i)))
    }
    pub fn double(d: f64) -> Self {
        Self::one(Item::Atomic(Atomic::double(d)))
    }
    pub fn string(s: String) -> Self {
        Self::one(Item::Atomic(Atomic::string(s)))
    }
    pub fn any_uri(s: String) -> Self {
        Self::one(Item::Atomic(Atomic::any_uri(s)))
    }
    pub fn qname(qname: QName) -> Self {
        Self::one(Item::Atomic(Atomic::qname(qname)))
    }
    pub fn empty_string() -> Self {
        Self::one(Item::Atomic(Atomic::string(String::new())))
    }
    pub fn map(map: Map<T>) -> Self {
        Self::one(Item::Map(map))
    }
    pub fn only_nodes(&self) -> bool {
        self.iter().all(|i| i.is_node())
    }
    pub fn retain<F>(&mut self, f: F)
    where
        F: FnMut(&Item<T>) -> bool,
    {
        match self {
            Sequence::Vec(v) => v.retain(f),
        }
    }
    /// retain elements according to the function
    pub fn retain_with_err<F>(&mut self, mut f: F) -> Result<()>
    where
        F: FnMut(&Item<T>) -> Result<bool>,
    {
        match self {
            Sequence::Vec(v) => {
                let mut result = Ok(());
                v.retain(|item| match f(item) {
                    Ok(b) => b,
                    Err(e) => {
                        result = Err(e);
                        false
                    }
                });
                result
            }
        }
    }
    pub fn iter(&self) -> SequenceIter<T> {
        let state = match self {
            Sequence::Vec(v) => SequenceIterState::Vec(v.iter()),
        };
        SequenceIter { state }
    }
}

enum SequenceIterState<'a, T: Ref> {
    Vec(std::slice::Iter<'a, Item<T>>),
}

pub struct SequenceIter<'a, T: Ref> {
    state: SequenceIterState<'a, T>,
}

impl<'a, T: Ref> Default for SequenceIter<'a, T> {
    fn default() -> Self {
        SequenceIter {
            state: SequenceIterState::Vec([].iter()),
        }
    }
}

impl<'a, T: Ref> Iterator for SequenceIter<'a, T> {
    type Item = &'a Item<T>;
    fn next(&mut self) -> Option<Self::Item> {
        match &mut self.state {
            SequenceIterState::Vec(v) => v.next(),
        }
    }
}

impl<'a, T: Ref> IntoIterator for &'a Sequence<T> {
    type Item = &'a Item<T>;
    type IntoIter = SequenceIter<'a, T>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<T: Ref> IntoIterator for Sequence<T> {
    type Item = Item<T>;
    type IntoIter = std::vec::IntoIter<Item<T>>;
    fn into_iter(self) -> Self::IntoIter {
        match self {
            Sequence::Vec(v) => v.into_iter(),
        }
    }
}

impl<T: Ref> FromIterator<Item<T>> for Sequence<T> {
    fn from_iter<I: IntoIterator<Item = Item<T>>>(iter: I) -> Self {
        Sequence::Vec(iter.into_iter().collect())
    }
}

impl<T: Ref> FromIterator<Node<T>> for Sequence<T> {
    fn from_iter<I: IntoIterator<Item = Node<T>>>(iter: I) -> Self {
        Sequence::Vec(iter.into_iter().map(Item::Node).collect())
    }
}

pub enum SequenceItemIter<'a, T: Ref> {
    Once(std::iter::Once<Item<T>>),
    S(SequenceIter<'a, T>),
}

#[derive(Debug, Default)]
pub struct Array<T: Ref> {
    array: Vec<Sequence<T>>,
}

impl<T: Ref> Clone for Array<T> {
    fn clone(&self) -> Self {
        Self {
            array: self.array.clone(),
        }
    }
}

impl<T: Ref> Array<T> {
    pub fn from_vec(array: Vec<Sequence<T>>) -> Array<T> {
        Array { array }
    }
    pub fn from_sequence(s: Sequence<T>) -> Array<T> {
        Array { array: vec![s] }
    }
    pub fn len(&self) -> usize {
        self.array.len()
    }
    pub fn is_empty(&self) -> bool {
        self.array.is_empty()
    }
}

impl<T: Ref> Index<usize> for Array<T> {
    type Output = Sequence<T>;
    fn index(&self, pos: usize) -> &Self::Output {
        &self.array[pos]
    }
}

impl<T: Ref> IntoIterator for Array<T> {
    type Item = Sequence<T>;
    type IntoIter = std::vec::IntoIter<Sequence<T>>;
    fn into_iter(self) -> Self::IntoIter {
        self.array.into_iter()
    }
}

impl<'a, T: Ref> IntoIterator for &'a Array<T> {
    type Item = &'a Sequence<T>;
    type IntoIter = std::slice::Iter<'a, Sequence<T>>;
    fn into_iter(self) -> Self::IntoIter {
        self.array.iter()
    }
}

#[cfg(test)]
macro_rules! print_sizes {
    ( $( $t:ty )* ) => {
        $( println!("sizeof {}\t{}", stringify!($t), size_of::<$t>()); )*
    }
}
#[test]
fn print_a_sizes() {
    use std::mem::size_of;
    use std::rc::Rc;
    use xust_tree::tree::Tree;
    println!("===");
    print_sizes!(Item<Rc<Tree<Atomic>>> Atomic Array<Rc<Tree<Atomic>>> Node<Rc<Tree<Atomic>>> Map<Rc<Tree<Atomic>>> QName Vec<u8> String
                 xust_tree::qnames::QNameCollection chrono::NaiveDateTime
                 chrono::DateTime<chrono::FixedOffset> chrono::NaiveDate);
    println!("===");
    assert!(size_of::<Atomic>() <= 32);
    assert!(size_of::<Item<Rc<Tree<Atomic>>>>() <= 40);
}
