use crate::xdm::{Item, Sequence};
use crate::Ref;
use xust_tree::node::Node;
use xust_xsd::{atomic::deep_equal, xpath_error::Result};

pub trait DeepEqual {
    fn deep_equal(&self, other: &Self, collation: &str) -> Result<bool>;
}

impl<T: Ref> DeepEqual for Sequence<T> {
    fn deep_equal(&self, other: &Sequence<T>, collation: &str) -> Result<bool> {
        if self.len() != other.len() {
            return Ok(false);
        }
        for p in self.into_iter().zip(other.into_iter()) {
            if !p.0.deep_equal(p.1, collation)? {
                return Ok(false);
            }
        }
        Ok(true)
    }
}

impl<T: Ref> DeepEqual for Item<T> {
    fn deep_equal(&self, other: &Item<T>, collation: &str) -> Result<bool> {
        match (self, other) {
            (Item::Node(a), Item::Node(b)) => a.deep_equal(b, collation),
            (Item::Atomic(a), Item::Atomic(b)) => deep_equal(a, b, collation),
            _ => Ok(false),
        }
    }
}

impl<T: Ref> DeepEqual for Node<T> {
    #[allow(clippy::only_used_in_recursion)]
    // TODO: use the collation argument
    fn deep_equal(&self, other: &Node<T>, collation: &str) -> Result<bool> {
        if self.node_kind() != other.node_kind() {
            return Ok(false);
        }
        if let (Some(a), Some(b)) = (self.node_name(), other.node_name()) {
            if a != b {
                return Ok(false);
            }
        }
        let mut a = self.attributes();
        let mut b = other.attributes();
        loop {
            let (a, b) = match (a.next(), b.next()) {
                (None, None) => break,
                (Some(a), Some(b)) => (a, b),
                _ => return Ok(false),
            };
            if !a.deep_equal(&b, collation)? {
                return Ok(false);
            }
        }
        let mut a = self.children();
        let mut b = other.children();
        loop {
            let (a, b) = match (a.next(), b.next()) {
                (None, None) => return Ok(true),
                (Some(a), Some(b)) => (a, b),
                _ => return Ok(false),
            };
            if !a.deep_equal(&b, collation)? {
                return Ok(false);
            }
        }
    }
}
