use crate::{
    qnames::{NamespaceDefinition, QNameCollection, QNameId},
    typed_value::TypeId,
};

pub trait Builder<O> {
    fn init_document(&mut self);
    fn init_element(
        &mut self,
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    );
    fn init_top_attribute(&mut self, qname: QNameId, r#type: TypeId, content: &str);
    fn init_top_text(&mut self, content: &str);
    fn init_top_comment(&mut self, content: &str);
    fn init_top_processing_instruction(&mut self, target: QNameId, content: &str);
    fn end_comment(&mut self);
    fn end_top_text(&mut self);
    fn end_processing_instruction(&mut self);
    fn push_str(&mut self, s: &str);
    fn push(&mut self, c: char);
    fn start_element(
        &mut self,
        parent: usize,
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    );
    fn start_attribute(&mut self, qname: QNameId, r#type: TypeId, value: &str);
    // call this method when the element open tag is complete and before adding
    // any children
    fn process_element_open_tag(&mut self) -> usize;
    fn start_comment(&mut self, node_id: usize);
    fn add_comment(&mut self, node_id: usize, content: &str);
    fn start_processing_instruction(&mut self, node_id: usize, target: QNameId);
    fn end_element(&mut self, node_id: usize);
    fn end_document(&mut self);
    fn build(self: Box<Self>, qnames: QNameCollection) -> O;
    fn has_errors(&self) -> bool;
    fn errors(&self, qnames: QNameCollection) -> Vec<String>;
}

pub trait TypedBuilder<O, T>: Builder<O> {
    fn push_typed_element(&mut self, element: usize, typed_value: T, last: bool);
    fn push_typed_attribute(&mut self, typed_value: T, last: bool);
}
