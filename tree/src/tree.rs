#![allow(clippy::upper_case_acronyms)]
pub mod builder;
pub(crate) mod iterator;

use crate::{
    node::{NodeIteratorInit, NodeKind, TypedValueIteratorInit},
    qnames::{NCName, NamespaceDefinition, Prefix, QName, QNameCollection, QNameId, QNameRef},
    stream::Item,
    string_collection::{StringCollection, StringId},
    typed_value::{TypeId, TypeResolver, DEFAULT_TYPE_RESOLVER, XS_UNTYPED_ATOMIC},
};
use std::borrow::Cow;
use std::fmt::Debug;
use std::sync::atomic::{AtomicUsize, Ordering};

static GLOBAL_TREE_COUNT: AtomicUsize = AtomicUsize::new(0);

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd)]
pub struct TreeId(usize);

impl TreeId {
    fn new() -> Self {
        let old_tree_count = GLOBAL_TREE_COUNT.fetch_add(1, Ordering::SeqCst);
        Self(old_tree_count)
    }
}

pub struct Tree<T> {
    id: TreeId,
    nodes: Box<[usize]>,
    strings: StringCollection,
    qnames: QNameCollection,
    namespace_definitions: Vec<Box<[NamespaceDefinition]>>,
    typed_values: Vec<T>,
    type_resolver: Option<Box<dyn TypeResolver>>,
}

impl<T> PartialEq for Tree<T> {
    fn eq(&self, t: &Self) -> bool {
        // trees are immutable so same id means same tree
        if self.id == t.id {
            return true;
        }
        unimplemented!()
    }
}

fn some(pos: usize) -> Option<usize> {
    if pos == 0 {
        None
    } else {
        Some(pos)
    }
}

impl<T> Tree<T> {
    fn node_kind(&self, pos: usize) -> NodeKindInternal {
        NodeKindInternal(self.nodes[pos])
    }
    fn pos(&self, pos: usize, offset: usize) -> usize {
        self.nodes[pos + offset]
    }
    fn following(&self, mut pos: usize) -> Option<usize> {
        use crate::node::Tree;
        let kind = self.node_kind(pos);
        if kind == ATTRIBUTE {
            if let Some(parent) = self.parent(pos) {
                if let Some(first_child) = self.first_child(parent) {
                    return Some(first_child);
                } else {
                    return Some(parent);
                }
            }
        }
        loop {
            if let Some(next_sibling) = self.next_sibling(pos) {
                return Some(next_sibling);
            } else if let Some(parent) = self.parent(pos) {
                pos = parent;
            } else {
                return None;
            }
        }
    }
    fn preceding(&self, mut pos: usize) -> Option<usize> {
        use crate::node::Tree;
        let kind = self.node_kind(pos);
        if kind == ATTRIBUTE {
            return self.parent(pos);
        }
        if let Some(previous_sibling) = self.previous_sibling(pos) {
            pos = previous_sibling;
            while let Some(last_child) = self.last_child(pos) {
                pos = last_child;
            }
            return Some(pos);
        }
        self.parent(pos)
    }
    fn qname(&self, qname_pos: usize) -> QNameRef {
        self.qnames.get(QNameId::from_usize(self.nodes[qname_pos]))
    }
    fn get_typed_range(&self, pos: usize) -> TypedValueIteratorInit {
        let (start, end) = usize_to_typed_range(pos);
        TypedValueIteratorInit::List { start, end }
    }
    fn str(&self, offset: usize) -> &str {
        let string = self.nodes[offset];
        self.strings.get(StringId { id: string as u32 })
    }
    fn concatenate_descendant_text(&self, pos: usize) -> Cow<str> {
        use crate::node::Tree;
        let init = self.descendant_init(pos);
        let mut pos = init.start;
        let mut string = Cow::Borrowed("");
        while pos != init.end {
            if self.node_kind(pos) == TEXT {
                let ns = self.str(pos + TEXT_CONTENT);
                match &mut string {
                    Cow::Owned(s) => s.push_str(ns),
                    Cow::Borrowed(s) => {
                        if s.is_empty() {
                            string = Cow::Borrowed(ns);
                        } else {
                            let mut str = String::new();
                            str.reserve(s.len() + ns.len());
                            str.push_str(s);
                            str.push_str(ns);
                            string = Cow::Owned(str);
                        }
                    }
                }
            }
            pos = self.descendant_iter(pos);
        }
        string
    }
    fn type_resolver(&self) -> &dyn TypeResolver {
        self.type_resolver
            .as_deref()
            .unwrap_or(&DEFAULT_TYPE_RESOLVER)
    }
    fn get_type_name(&self, pos: usize) -> QNameRef {
        let t = TypeId::from_usize(self.nodes[pos]);
        let t = self.type_resolver().get_type_name(t);
        self.qnames.get(t)
    }
}

fn usize_to_typed_range(u: usize) -> (usize, usize) {
    (u as u32 as usize, u >> 32)
}

fn typed_range_to_usize(pos: usize, end: usize) -> usize {
    assert_eq!(pos >> 32, 0);
    assert_eq!(end >> 32, 0);
    pos + (end << 32)
}

#[test]
fn test_usize_to_typed_range() {
    let (pos, len) = usize_to_typed_range(55834574865);
    assert_eq!(pos, 17);
    assert_eq!(len, 13);
}

#[test]
fn test_to_tree_type_id() {
    let u = typed_range_to_usize(17, 13);
    assert_eq!(u, 55834574865);
}

impl<T> crate::node::Tree for Tree<T> {
    type TypedValue = T;
    fn id(&self) -> TreeId {
        self.id
    }
    fn kind(&self, pos: usize) -> NodeKind {
        NodeKind::from(self.node_kind(pos))
    }
    fn qname(&self, pos: usize) -> QNameId {
        let offset = match self.node_kind(pos) {
            ELEMENT => ELEMENT_QNAME,
            ATTRIBUTE => ATTRIBUTE_QNAME,
            PROCESSING_INSTRUCTION => PROCESSING_INSTRUCTION_TARGET,
            o => panic!("No QName for node of type {:?}.", o),
        };
        QNameId::from_usize(self.nodes[pos + offset])
    }
    fn node_name(&self, pos: usize) -> Option<QNameRef> {
        // document: nill
        // element: node-name
        // attribute: node-name
        // pi: target
        // comment: nill
        // text: nill
        let kind = self.node_kind(pos);
        if kind == ELEMENT {
            Some(self.qname(pos + ELEMENT_QNAME))
        } else if kind == ATTRIBUTE {
            Some(self.qname(pos + ATTRIBUTE_QNAME))
        } else if kind == PROCESSING_INSTRUCTION {
            Some(self.qname(pos + PROCESSING_INSTRUCTION_TARGET))
        } else {
            None
        }
    }
    fn string_value(&self, pos: usize) -> Cow<str> {
        // document: string-value
        // element: string-value
        // attribute: value
        // pi: content
        // comment: content
        // text: content
        let kind = self.node_kind(pos);
        if kind == ELEMENT || kind == DOCUMENT {
            self.concatenate_descendant_text(pos)
        } else {
            Cow::Borrowed(if kind == ATTRIBUTE {
                self.str(pos + ATTRIBUTE_CONTENT)
            } else if kind == PROCESSING_INSTRUCTION {
                self.str(pos + PROCESSING_INSTRUCTION_CONTENT)
            } else if kind == COMMENT {
                self.str(pos + COMMENT_CONTENT)
            } else if kind == TEXT {
                self.str(pos + TEXT_CONTENT)
            } else {
                panic!()
            })
        }
    }
    fn type_id(&self, pos: usize) -> Option<TypeId> {
        match self.node_kind(pos) {
            ELEMENT => Some(TypeId::from_usize(self.nodes[pos + ELEMENT_TYPE])),
            ATTRIBUTE => Some(TypeId::from_usize(self.nodes[pos + ATTRIBUTE_TYPE])),
            TEXT => todo!(),
            _ => None,
        }
    }
    fn type_name(&self, pos: usize) -> Option<QNameRef> {
        match self.node_kind(pos) {
            ELEMENT => Some(self.get_type_name(pos + ELEMENT_TYPE)),
            ATTRIBUTE => Some(self.get_type_name(pos + ATTRIBUTE_TYPE)),
            TEXT => Some(self.qnames.untyped_atomic()),
            _ => None,
        }
    }
    fn parent(&self, pos: usize) -> Option<usize> {
        if pos == 0 {
            return None;
        }
        Some(self.pos(pos, NODE_PARENT))
    }
    fn previous_sibling(&self, pos: usize) -> Option<usize> {
        let kind = self.node_kind(pos);
        if kind == ELEMENT || kind == PROCESSING_INSTRUCTION || kind == COMMENT || kind == TEXT {
            some(self.pos(pos, NODE_PREV))
        } else {
            None
        }
    }
    fn next_sibling(&self, pos: usize) -> Option<usize> {
        let kind = self.node_kind(pos);
        if kind == ELEMENT || kind == PROCESSING_INSTRUCTION || kind == COMMENT || kind == TEXT {
            some(self.pos(pos, NODE_NEXT))
        } else {
            None
        }
    }
    fn first_child(&self, pos: usize) -> Option<usize> {
        match self.node_kind(pos) {
            ELEMENT => some(self.pos(pos, ELEMENT_FIRST)),
            DOCUMENT => some(self.pos(pos, DOCUMENT_FIRST)),
            _ => None,
        }
    }
    fn last_child(&self, pos: usize) -> Option<usize> {
        match self.node_kind(pos) {
            ELEMENT => some(self.pos(pos, ELEMENT_LAST)),
            DOCUMENT => some(self.pos(pos, DOCUMENT_LAST)),
            _ => None,
        }
    }
    fn descendant_init(&self, pos: usize) -> NodeIteratorInit {
        if let Some(first_child) = self.first_child(pos) {
            let end = self.following(pos).unwrap_or(self.nodes.len());
            NodeIteratorInit::new(first_child, end)
        } else {
            NodeIteratorInit::empty()
        }
    }
    fn descendant_iter(&self, pos: usize) -> usize {
        let node_kind = self.node_kind(pos);
        let node_size = NODE_SIZES[node_kind.0];
        let mut new_pos = pos + node_size;
        // skip attributes
        if node_kind == ELEMENT {
            let n_attributes = self.pos(pos, ELEMENT_NUMBER_OF_ATTRIBUTES);
            new_pos += n_attributes * ATTRIBUTE_NODE_SIZE;
        }
        new_pos
    }
    fn descendant_or_self_init(&self, pos: usize) -> NodeIteratorInit {
        let end = if self.node_kind(pos) == ATTRIBUTE {
            pos + ATTRIBUTE_NODE_SIZE
        } else if let Some(end) = self.following(pos) {
            end
        } else {
            self.nodes.len()
        };
        NodeIteratorInit::new(pos, end)
    }
    fn descendant_or_self_iter(&self, pos: usize) -> usize {
        self.descendant_iter(pos)
    }
    fn following_init(&self, pos: usize) -> NodeIteratorInit {
        if let Some(following) = self.following(pos) {
            let end = self.nodes.len();
            NodeIteratorInit::new(following, end)
        } else {
            NodeIteratorInit::empty()
        }
    }
    fn following_iter(&self, pos: usize) -> usize {
        self.descendant_iter(pos)
    }
    fn following_siblings_init(&self, pos: usize) -> NodeIteratorInit {
        if self.node_kind(pos) != ATTRIBUTE {
            if let Some(next_sibling) = self.next_sibling(pos) {
                return NodeIteratorInit::new(next_sibling, 0);
            }
        }
        NodeIteratorInit::empty()
    }
    fn following_siblings_iter(&self, pos: usize) -> usize {
        self.nodes[pos + NODE_NEXT]
    }
    fn preceding_siblings_init(&self, pos: usize) -> NodeIteratorInit {
        if self.node_kind(pos) != ATTRIBUTE {
            if let Some(previous_sibling) = self.previous_sibling(pos) {
                return NodeIteratorInit::new(previous_sibling, 0);
            }
        }
        NodeIteratorInit::empty()
    }
    fn preceding_siblings_iter(&self, pos: usize) -> usize {
        self.nodes[pos + NODE_PREV]
    }
    fn self_init(&self, pos: usize) -> NodeIteratorInit {
        NodeIteratorInit::new(pos, usize::MAX)
    }
    fn self_iter(&self, _pos: usize) -> usize {
        usize::MAX
    }
    fn parent_init(&self, pos: usize) -> NodeIteratorInit {
        NodeIteratorInit::new(self.parent(pos).unwrap_or(usize::MAX), usize::MAX)
    }
    fn parent_iter(&self, _pos: usize) -> usize {
        usize::MAX
    }
    fn preceding_init(&self, pos: usize) -> NodeIteratorInit {
        if self.node_kind(pos) != ATTRIBUTE {
            if let Some(preceding) = self.preceding(pos) {
                return NodeIteratorInit::new(preceding, usize::MAX);
            }
        }
        NodeIteratorInit::empty()
    }
    fn preceding_iter(&self, pos: usize) -> usize {
        self.preceding(pos).unwrap_or(usize::MAX)
    }
    fn ancestors_init(&self, pos: usize) -> NodeIteratorInit {
        if let Some(parent) = self.parent(pos) {
            NodeIteratorInit::new(parent, usize::MAX)
        } else {
            NodeIteratorInit::empty()
        }
    }
    fn ancestors_iter(&self, pos: usize) -> usize {
        self.parent(pos).unwrap_or(usize::MAX)
    }
    fn ancestors_or_self_init(&self, pos: usize) -> NodeIteratorInit {
        NodeIteratorInit::new(pos, usize::MAX)
    }
    fn ancestors_or_self_iter(&self, pos: usize) -> usize {
        self.parent(pos).unwrap_or(usize::MAX)
    }
    fn children_init(&self, pos: usize) -> NodeIteratorInit {
        match self.node_kind(pos) {
            ELEMENT => NodeIteratorInit::new(self.nodes[pos + ELEMENT_FIRST], 0),
            DOCUMENT => NodeIteratorInit::new(self.nodes[pos + DOCUMENT_FIRST], 0),
            _ => NodeIteratorInit::empty(),
        }
    }
    fn children_iter(&self, pos: usize) -> usize {
        self.nodes[pos + NODE_NEXT]
    }
    fn attributes_init(&self, pos: usize) -> NodeIteratorInit {
        if self.node_kind(pos) == ELEMENT {
            let start = pos + ELEMENT_NODE_SIZE;
            let n = self.nodes[pos + ELEMENT_NUMBER_OF_ATTRIBUTES];
            let end = start + n * ATTRIBUTE_NODE_SIZE;
            NodeIteratorInit::new(start, end)
        } else {
            NodeIteratorInit::empty()
        }
    }
    fn attributes_iter(&self, pos: usize) -> usize {
        pos + ATTRIBUTE_NODE_SIZE
    }
    fn item_iter(&self, pos: usize) -> Box<dyn Iterator<Item = Item> + '_> {
        Box::new(iterator::TreeIterator::new(self, pos))
    }
    fn typed_value_init(&self, pos: usize) -> TypedValueIteratorInit {
        let kind = self.node_kind(pos);
        if kind == ELEMENT {
            let v = self.nodes[pos + ELEMENT_SIMPLE_CONTENT];
            if v == NO_TYPED_VALUE {
                TypedValueIteratorInit::String(self.string_value(pos), XS_UNTYPED_ATOMIC)
            } else {
                self.get_typed_range(v)
            }
        } else if kind == ATTRIBUTE {
            let v = self.nodes[pos + ATTRIBUTE_CONTENT];
            if let Some(v) = usize_to_typed_value_pos(v) {
                self.get_typed_range(v)
            } else {
                let t = TypeId::from_usize(self.nodes[pos + ATTRIBUTE_TYPE]);
                TypedValueIteratorInit::String(Cow::Borrowed(self.str(pos + ATTRIBUTE_CONTENT)), t)
            }
        } else if kind == TEXT {
            TypedValueIteratorInit::String(
                Cow::Borrowed(self.str(pos + TEXT_CONTENT)),
                XS_UNTYPED_ATOMIC,
            )
        } else {
            TypedValueIteratorInit::Empty
        }
    }
    fn get_typed_value(&self, pos: usize) -> &T {
        &self.typed_values[pos]
    }

    fn qnames(&self) -> &QNameCollection {
        &self.qnames
    }
    fn attribute(&self, pos: usize, qname: QNameId) -> Option<&str> {
        let kind = self.node_kind(pos);
        if kind != ELEMENT {
            return None;
        }
        let mut i = pos + ELEMENT_NODE_SIZE;
        let n = self.nodes[pos + ELEMENT_NUMBER_OF_ATTRIBUTES];
        let end = i + n * ATTRIBUTE_NODE_SIZE;
        while i < end {
            if qname == QNameId::from_usize(self.nodes[i + ATTRIBUTE_QNAME]) {
                let string = self.nodes[i + ATTRIBUTE_CONTENT];
                return Some(self.strings.get(StringId { id: string as u32 }));
            }
            i += ATTRIBUTE_NODE_SIZE;
        }
        None
    }
    fn namespaces(&self, pos: usize) -> &[NamespaceDefinition] {
        assert!(self.nodes[pos] == ELEMENT.0);
        &self.namespace_definitions[self.nodes[pos + ELEMENT_NAMESPACES]]
    }
    fn resolve_qname(&self, node: usize, qname: &str) -> Option<QName> {
        assert!(self.nodes[node] == ELEMENT.0);
        let ns = &self.namespace_definitions[self.nodes[node + ELEMENT_NAMESPACES]];
        let (prefix, local_name) = if let Some(pos) = qname.find(':') {
            (&qname[..pos], &qname[pos + 1..])
        } else {
            ("", qname)
        };
        self.qnames
            .qname_from_prefix(Prefix(prefix), NCName(local_name), ns)
    }
}

use std::fmt;
impl<T> fmt::Debug for Tree<T> {
    fn fmt(&self, _f: &mut fmt::Formatter) -> fmt::Result {
        Ok(())
    }
}

#[derive(Debug, PartialEq, Eq)]
struct NodeKindInternal(usize);

const DOCUMENT: NodeKindInternal = NodeKindInternal(0);
const ELEMENT: NodeKindInternal = NodeKindInternal(1);
const ATTRIBUTE: NodeKindInternal = NodeKindInternal(2);
//const NAMESPACE: NodeKindInternal = NodeKindInternal(3);
const PROCESSING_INSTRUCTION: NodeKindInternal = NodeKindInternal(4);
const COMMENT: NodeKindInternal = NodeKindInternal(5);
const TEXT: NodeKindInternal = NodeKindInternal(6);

impl From<NodeKindInternal> for NodeKind {
    fn from(v: NodeKindInternal) -> Self {
        NodeKind::from_usize(v.0)
    }
}

const DOCUMENT_NODE_SIZE: usize = 3;
const DOCUMENT_FIRST: usize = 1;
const DOCUMENT_LAST: usize = 2;
const ELEMENT_NODE_SIZE: usize = 11;
const NODE_PARENT: usize = 1;
const NODE_PREV: usize = 2;
const NODE_NEXT: usize = 3;
const ELEMENT_FIRST: usize = 4;
const ELEMENT_LAST: usize = 5;
const ELEMENT_QNAME: usize = 6;
const ELEMENT_NAMESPACES: usize = 7;
const ELEMENT_NUMBER_OF_ATTRIBUTES: usize = 8;
const ELEMENT_TYPE: usize = 9;
const ELEMENT_SIMPLE_CONTENT: usize = 10;
const ATTRIBUTE_NODE_SIZE: usize = 5;
const ATTRIBUTE_QNAME: usize = 2;
const ATTRIBUTE_CONTENT: usize = 3;
const ATTRIBUTE_TYPE: usize = 4;
const NAMESPACE_NODE_SIZE: usize = 1;
const PROCESSING_INSTRUCTION_NODE_SIZE: usize = 6;
const PROCESSING_INSTRUCTION_TARGET: usize = 4;
const PROCESSING_INSTRUCTION_CONTENT: usize = 5;
const COMMENT_NODE_SIZE: usize = 5;
const COMMENT_CONTENT: usize = 4;
const TEXT_NODE_SIZE: usize = 5;
const TEXT_CONTENT: usize = 4;

const NODE_SIZES: [usize; 7] = [
    DOCUMENT_NODE_SIZE,
    ELEMENT_NODE_SIZE,
    ATTRIBUTE_NODE_SIZE,
    NAMESPACE_NODE_SIZE,
    PROCESSING_INSTRUCTION_NODE_SIZE,
    COMMENT_NODE_SIZE,
    TEXT_NODE_SIZE,
];

/// iterate forward over all nodes
struct AllNodeIterator<'a, T> {
    tree: &'a Tree<T>,
    current: usize,
    end: usize,
}

impl<'a, T> AllNodeIterator<'a, T> {
    fn new(tree: &'a Tree<T>, pos: usize) -> Self {
        let end = if tree.node_kind(pos) == ATTRIBUTE {
            pos + ATTRIBUTE_NODE_SIZE
        } else if let Some(end) = tree.following(pos) {
            end
        } else {
            tree.nodes.len()
        };
        Self {
            tree,
            current: pos,
            end,
        }
    }
}

impl<'a, T> Iterator for AllNodeIterator<'a, T> {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        let pos = self.current;
        if pos < self.end {
            let node_kind = self.tree.node_kind(pos);
            let node_size = NODE_SIZES[node_kind.0];
            self.current += node_size;
            Some(pos)
        } else {
            None
        }
    }
}

// nodes
//
// Each node has a type. Depending on the type, the other properties are read.

// structures
//
// Document
// - children
// - base-uri
// - document-uri
// - unparsed-entity-public-id
// - unparsed-entity-system-id
// 1: first-child
// 2: last-child
//
// Element
// 1: parent
// 2: prev
// 3: next
// 4: first-child
// 5: last-child
// 6: qname
// 7: ns set
// 8: number of attributes
//
// Attribute
// 1: parent
// 2: qname
// 3: value
//
// Namespace
// TODO
//
// Processing Instruction
// 1: parent
// 2: prev
// 3: next
// 4: target
// 5: content
//
// Comment
// 1: parent
// 2: prev
// 3: next
// 4: content
//
// Text
// 1: parent
// 2: prev
// 3: next
// 4: content
//
//
//

const NO_TYPED_VALUE: usize = !(1 << 63);

fn is_type_id(v: usize) -> bool {
    (v & (1 << 63)) != 0
}

fn get_typed_value_pos(v: usize) -> usize {
    v & !(1 << 63)
}

fn usize_to_typed_value_pos(v: usize) -> Option<usize> {
    if is_type_id(v) {
        Some(get_typed_value_pos(v))
    } else {
        None
    }
}

// test for the axes
//
#[cfg(test)]
fn test_dom1() -> Tree<()> {
    // <a><b/><c d=" D " i="I I">E<!--F--></c></a>
    let qnames = crate::qnames::QNameCollector::default();
    let name_a = qnames.add_local_qname(NCName("a"));
    let name_b = qnames.add_local_qname(NCName("b"));
    let name_c = qnames.add_local_qname(NCName("c"));
    let name_d = qnames.add_local_qname(NCName("d"));
    let name_i = qnames.add_local_qname(NCName("i"));
    let mut b = builder::DocumentBuilder::new();
    let untyped_atomic = crate::typed_value::DEFAULT_TYPE_RESOLVER.xs_untyped_atomic();
    let untyped = crate::typed_value::DEFAULT_TYPE_RESOLVER.xs_untyped();
    let mut e = b.add_element(name_a, untyped, &[]).add_children();
    e.add_element(name_b, untyped, &[]);
    let mut c = e.add_element(name_c, untyped, &[]);
    c.add_attribute_from_qname(name_d, untyped_atomic, " D ");
    c.add_attribute_from_qname(name_i, untyped_atomic, "I I");
    let mut c = c.add_children();
    c.push_str("E");
    c.add_comment("F");
    drop(c);
    drop(e);

    // let string = crate::write::to_string(&tree).unwrap();
    // println!("{}", string);
    b.build(qnames.collect())
}
#[test]
fn test_ancestors() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.ancestors().count(), 0);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.ancestors().count(), 2);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.ancestors().count(), 3);
}
#[test]
fn test_ancestors_or_self() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.ancestors_or_self().count(), 1);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.ancestors_or_self().count(), 3);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.ancestors_or_self().count(), 4);
}
#[test]
fn test_self() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.self_iter().count(), 1);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.self_iter().count(), 1);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.self_iter().count(), 1);
}
#[test]
fn test_parent() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.parent_iter().count(), 0);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.parent_iter().count(), 1);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.parent_iter().count(), 1);
}
#[test]
fn test_children() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.children().count(), 1);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.children().count(), 2);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.children().count(), 0);
}
#[test]
fn test_descendant() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.descendant().count(), 5);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.descendant().count(), 2);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.descendant().count(), 0);
}
#[test]
fn test_descendant_or_self() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.descendant_or_self().count(), 6);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.descendant_or_self().count(), 3);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.descendant_or_self().count(), 1);
}
#[test]
fn test_attributes() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.attributes().count(), 0);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.attributes().count(), 2);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.attributes().count(), 0);
}
#[test]
fn test_typed_attributes() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.attributes().count(), 0);
    let c = root.first_child().unwrap().last_child().unwrap();
    let mut atts = c.attributes();
    let att1 = atts.next().unwrap();
    let att2 = atts.next().unwrap();
    println!("{}", att1.string_value());
    println!("{}", att2.string_value());
}
#[test]
fn test_following() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.following().count(), 0);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.following().count(), 0);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.following().count(), 2);
}
#[test]
fn test_following_siblings() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.following_siblings().count(), 0);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.following_siblings().count(), 0);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.following_siblings().count(), 0);
}
#[test]
fn test_preceding() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.preceding().count(), 0);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.preceding().count(), 3);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.preceding().count(), 0);
}
#[test]
fn test_preceding_siblings() {
    let tree = test_dom1();
    let root = crate::node::Node::root(&tree);
    assert_eq!(root.preceding_siblings().count(), 0);
    let c = root.first_child().unwrap().last_child().unwrap();
    assert_eq!(c.preceding_siblings().count(), 1);
    let att = c.attributes().next().unwrap();
    assert_eq!(att.preceding_siblings().count(), 0);
}
