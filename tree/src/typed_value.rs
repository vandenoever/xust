use crate::qnames::QNameId;

// TypeId holds the QNameId for the type.
// This is used in Tree::type_name()
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct TypeId(usize);

// TODO: These constants should not exist.
// The values in them are not universal, but unique to the xust_xsd
// implementation.
pub const XS_UNTYPED: TypeId = TypeId(46);
pub const XS_UNTYPED_ATOMIC: TypeId = TypeId(47);

impl TypeId {
    pub fn to_usize(&self) -> usize {
        self.0
    }
    pub const fn from_usize(v: usize) -> Self {
        Self(v)
    }
}

pub trait TypeResolver: Sync + Send {
    fn xs_untyped(&self) -> TypeId;
    fn xs_untyped_atomic(&self) -> TypeId;
    fn get_type_name(&self, r#type: TypeId) -> QNameId;
}

pub(crate) struct DefaultTypeResolver {}

impl TypeResolver for DefaultTypeResolver {
    fn xs_untyped(&self) -> TypeId {
        XS_UNTYPED
    }
    fn xs_untyped_atomic(&self) -> TypeId {
        XS_UNTYPED_ATOMIC
    }
    fn get_type_name(&self, r#type: TypeId) -> QNameId {
        if r#type == XS_UNTYPED {
            return crate::qnames::XSD_UNTYPED_QNAME;
        }
        if r#type == XS_UNTYPED_ATOMIC {
            return crate::qnames::XSD_UNTYPED_ATOMIC_QNAME;
        }
        panic!("The DefaultTypeResolver only knows untyped types.");
    }
}

pub(crate) const DEFAULT_TYPE_RESOLVER: DefaultTypeResolver = DefaultTypeResolver {};
