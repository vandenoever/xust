use crate::{
    builder::Builder,
    qnames::{NamespaceDefinition, QNameCollection, QNameId},
    typed_value::TypeId,
};
use std::sync::atomic::{AtomicUsize, Ordering};

/// Items to build a tree.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Item<'a> {
    StartDocument,
    EndDocument,
    StartElement {
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &'a [NamespaceDefinition],
    },
    EndElement,
    StartAttribute {
        qname: QNameId,
        r#type: TypeId,
    },
    EndAttribute,
    StartText,
    EndText,
    StartComment,
    EndComment,
    // an xs:QName with the value of the target property in the local-name and an empty namespace URI and empty prefix
    StartProcessingInstruction {
        target: QNameId,
    },
    EndProcessingInstruction,
    Attribute {
        qname: QNameId,
        r#type: TypeId,
        value: &'a str,
    },
    Text(&'a str),
    Comment(&'a str),
    ProcessingInstruction {
        target: QNameId,
        content: &'a str,
    },
    String(&'a str),
    Char(char),
}

#[derive(Debug, PartialEq, Eq)]
pub enum Status {
    Open,
    Done,
    Error,
}

pub trait ItemStream {
    fn next(&mut self, item: Item);
    fn write_str(&mut self, s: &str);
    fn status(&self) -> Status;
}

pub trait ItemStreamBuilder<T>: ItemStream {
    fn build(self, qnames: QNameCollection) -> T;
}

fn unexpected(item: Item) {
    panic!("unexpected item {:?}", item)
}

static GLOBAL_ID_COUNT: AtomicUsize = AtomicUsize::new(0);

#[derive(PartialEq, Eq)]
pub struct Id(usize);

impl Id {
    fn new() -> Self {
        let old_tree_count = GLOBAL_ID_COUNT.fetch_add(1, Ordering::SeqCst);
        Self(old_tree_count)
    }
}

pub struct IteratorToTree<T> {
    id: Id,
    debug_output: bool,
    builder: Box<dyn Builder<T>>,
    state: fn(&mut IteratorToTree<T>, item: Item),
    stack: Vec<usize>,
}

impl<T> IteratorToTree<T> {
    pub fn new(builder: Box<dyn Builder<T>>) -> Self {
        Self {
            id: Id::new(),
            debug_output: false,
            builder,
            state: IteratorToTree::next_init,
            stack: Vec::new(),
        }
    }
    pub fn builder(&self) -> &dyn Builder<T> {
        &*self.builder
    }
    fn next_init(&mut self, item: Item) {
        match item {
            Item::StartDocument => {
                self.builder.init_document();
                self.stack.push(0);
                self.state = IteratorToTree::next_content;
            }
            Item::StartElement {
                qname,
                r#type,
                namespace_definitions,
            } => {
                self.builder
                    .init_element(qname, r#type, namespace_definitions);
                self.state = IteratorToTree::next_attributes;
            }
            Item::StartAttribute { qname, r#type } => {
                self.builder.init_top_attribute(qname, r#type, "");
                self.state = IteratorToTree::next_single_node;
            }
            Item::StartText => {
                self.builder.init_top_text("");
                self.state = IteratorToTree::next_single_node;
            }
            Item::StartComment => {
                self.builder.init_top_comment("");
                self.state = IteratorToTree::next_single_node;
            }
            Item::StartProcessingInstruction { target } => {
                self.builder.init_top_processing_instruction(target, "");
                self.state = IteratorToTree::next_single_node;
            }
            Item::Attribute {
                qname,
                r#type,
                value,
            } => {
                self.builder.init_top_attribute(qname, r#type, value);
                self.state = IteratorToTree::next_end;
            }
            Item::Text(content) => {
                self.builder.init_top_text(content);
                self.builder.end_top_text();
                self.state = IteratorToTree::next_end;
            }
            Item::Comment(content) => {
                self.builder.init_top_comment(content);
                self.builder.end_comment();
                self.state = IteratorToTree::next_end;
            }
            Item::ProcessingInstruction { target, content } => {
                self.builder
                    .init_top_processing_instruction(target, content);
                self.builder.end_processing_instruction();
                self.state = IteratorToTree::next_end;
            }
            e => unexpected(e),
        }
    }
    fn current_node(&self) -> usize {
        *self.stack.last().unwrap()
    }
    fn start_element(
        &mut self,
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    ) {
        let parent = self.current_node();
        self.builder
            .start_element(parent, qname, r#type, namespace_definitions);
        self.state = IteratorToTree::next_attributes;
    }
    fn next_single_node(&mut self, item: Item) {
        match item {
            Item::String(s) => {
                self.builder.push_str(s);
            }
            Item::Char(c) => {
                self.builder.push(c);
            }
            Item::EndAttribute => {
                // If there is only one attribute, make sure it has all the
                // text that was pushed to it.
                let id = self.builder.process_element_open_tag();
                self.stack.push(id);
                self.state = IteratorToTree::next_end;
            }
            Item::EndText => {
                self.builder.end_top_text();
                self.state = IteratorToTree::next_end;
            }
            Item::EndComment => {
                self.builder.end_comment();
                self.state = IteratorToTree::next_end;
            }
            Item::EndProcessingInstruction => {
                self.builder.end_processing_instruction();
                self.state = IteratorToTree::next_end;
            }
            e => unexpected(e),
        }
    }
    fn next_attributes(&mut self, item: Item) {
        match item {
            Item::StartAttribute { qname, r#type } => {
                self.builder.start_attribute(qname, r#type, "");
                self.state = IteratorToTree::next_attribute;
            }
            Item::Attribute {
                qname,
                r#type,
                value,
            } => {
                self.builder.start_attribute(qname, r#type, value);
            }
            item => {
                let id = self.builder.process_element_open_tag();
                self.stack.push(id);
                self.state = IteratorToTree::next_content;
                self.next_content(item)
            }
        }
    }
    fn next_attribute(&mut self, item: Item) {
        match item {
            Item::String(s) => {
                self.builder.push_str(s);
            }
            Item::Char(c) => {
                self.builder.push(c);
            }
            Item::EndAttribute => {
                self.state = IteratorToTree::next_attributes;
            }
            e => unexpected(e),
        }
    }
    fn next_content(&mut self, item: Item) {
        match item {
            Item::StartElement {
                qname,
                r#type,
                namespace_definitions,
            } => {
                self.start_element(qname, r#type, namespace_definitions);
            }
            Item::StartText => {
                self.state = IteratorToTree::next_text;
            }
            Item::Text(content) => self.builder.push_str(content),
            Item::StartComment => {
                let parent = self.current_node();
                self.builder.start_comment(parent);
                self.state = IteratorToTree::next_comment;
            }
            Item::Comment(content) => {
                let parent = self.current_node();
                self.builder.add_comment(parent, content);
            }
            Item::StartProcessingInstruction { target } => {
                let parent = self.current_node();
                self.builder.start_processing_instruction(parent, target);
                self.state = IteratorToTree::next_processing_instruction;
            }
            Item::ProcessingInstruction { target, content } => {
                let parent = self.current_node();
                self.builder.start_processing_instruction(parent, target);
                self.builder.push_str(content);
                self.builder.end_processing_instruction();
            }
            Item::EndElement => {
                let node = self.stack.pop().unwrap();
                self.builder.end_element(node);
                if self.stack.is_empty() {
                    self.state = IteratorToTree::next_end;
                }
            }
            Item::EndDocument => {
                self.builder.end_document();
                self.state = IteratorToTree::next_end;
            }
            e => unexpected(e),
        }
    }
    fn next_text(&mut self, item: Item) {
        match item {
            Item::String(s) => {
                self.builder.push_str(s);
            }
            Item::Char(c) => {
                self.builder.push(c);
            }
            Item::EndText => {
                self.state = IteratorToTree::next_content;
            }
            e => unexpected(e),
        }
    }
    fn next_comment(&mut self, item: Item) {
        match item {
            Item::String(s) => {
                self.builder.push_str(s);
            }
            Item::Char(c) => {
                self.builder.push(c);
            }
            Item::EndComment => {
                self.builder.end_comment();
                self.state = IteratorToTree::next_content;
            }
            e => unexpected(e),
        }
    }
    fn next_processing_instruction(&mut self, item: Item) {
        match item {
            Item::String(s) => {
                self.builder.push_str(s);
            }
            Item::Char(c) => {
                self.builder.push(c);
            }
            Item::EndProcessingInstruction => {
                self.builder.end_processing_instruction();
                self.state = IteratorToTree::next_content;
            }
            e => unexpected(e),
        }
    }
    fn next_end(&mut self, _item: Item) {
        panic!("Tree was already complete.")
    }
}

impl<T> ItemStream for IteratorToTree<T> {
    fn next(&mut self, item: Item) {
        if self.debug_output {
            println!("({})  > IT {:#?}", self.id.0, item);
        }
        (self.state)(self, item)
    }
    fn write_str(&mut self, s: &str) {
        (self.state)(self, Item::String(s))
    }
    fn status(&self) -> Status {
        if self.state as usize == IteratorToTree::<T>::next_end as usize {
            Status::Done
        } else if self.builder.has_errors() {
            Status::Error
        } else {
            Status::Open
        }
    }
}

impl<T> ItemStreamBuilder<T> for IteratorToTree<T> {
    fn build(self, qnames: QNameCollection) -> T {
        if self.state as usize == IteratorToTree::<T>::next_end as usize {
            self.builder.build(qnames)
        } else {
            panic!("The tree is not complete. {}", self.stack.len())
        }
    }
}

#[cfg(test)]
use crate::qnames::{NCName, Namespace, QNameCollector, XML_NAMESPACE, XML_PREFIX};

#[cfg(test)]
fn test_qnames() -> QNameCollection {
    let qnames = QNameCollector::default();
    qnames.add_local_qname(NCName("a"));
    qnames.add_local_qname(NCName("b"));
    qnames.add_local_qname(NCName("c"));
    qnames.collect()
}

#[cfg(test)]
fn test_build(input: &[Item], qnames: QNameCollection) -> crate::tree::Tree<()> {
    let iter = input.iter();
    let mut builder = IteratorToTree::new(Box::new(crate::tree::builder::TreeBuilder::empty()));
    for item in iter {
        builder.next(*item);
    }
    builder.build(qnames)
}

#[cfg(test)]
fn test_compare<T>(tree: crate::tree::Tree<T>, result: &[Item]) -> Option<String> {
    use crate::node::Node;
    let root = Node::root(&tree);
    let iter = root.item_iter();
    let mut result = result.iter();
    let mut a_len = 0;
    for a in iter {
        a_len += 1;
        if let Some(b) = result.next() {
            if a != *b {
                return Some(format!("at {} {:?} != {:?}", a_len, a, b));
            }
        } else {
            return Some(format!(
                "stream is too long (>{}) redundant {:?}",
                a_len - 1,
                a
            ));
        }
    }
    result
        .next()
        .map(|b| format!("stream is too short (<{}) missing {:?}", a_len + 1, b))
}

#[cfg(test)]
fn test_builder(input: &[Item], qnames: QNameCollection) -> Option<String> {
    let tree = test_build(input, qnames);
    test_compare(tree, input)
}

#[cfg(test)]
fn test_builder_simplify(
    input: &[Item],
    result: &[Item],
    qnames: QNameCollection,
) -> Option<String> {
    let tree = test_build(input, qnames);
    test_compare(tree, result)
}
/*
#[test]
fn test_empty() {
    assert_eq!(test_builder(&[]), Err(Error::Incomplete))
}
*/
#[cfg(test)]
const UNTYPED: TypeId = crate::typed_value::XS_UNTYPED;

#[test]
fn test_empty_document() {
    assert_eq!(
        test_builder(&[Item::StartDocument, Item::EndDocument], test_qnames()),
        None
    );
}

#[cfg(test)]
const DEFAULT_NS: &[NamespaceDefinition] = &[NamespaceDefinition {
    prefix: XML_PREFIX,
    namespace: XML_NAMESPACE,
}];

#[test]
fn test_simple_document() {
    let qnames = test_qnames();
    let qname = qnames.find_qname(Namespace(""), NCName("a")).unwrap();
    assert_eq!(
        test_builder(
            &[
                Item::StartDocument,
                Item::StartElement {
                    qname,
                    r#type: UNTYPED,
                    namespace_definitions: DEFAULT_NS
                },
                Item::Text("b"),
                Item::EndElement,
                Item::EndDocument
            ],
            qnames
        ),
        None
    );
}
/*
#[test]
fn test_incomplete_document() {
    assert_eq!(test_builder(&[Item::StartDocument]), Err(Error::Incomplete));
}

#[test]
fn test_incomplete_element() {
    assert_eq!(
        test_builder(&[Item::StartElement {
            prefix: Prefix(""),
            local_name: NCName("a")
        }]),
        Err(Error::Incomplete)
    );
}
*/
#[test]
fn test_element() {
    let qnames = test_qnames();
    let qname = qnames.find_qname(Namespace(""), NCName("a")).unwrap();
    assert_eq!(
        test_builder(
            &[
                Item::StartElement {
                    qname,
                    r#type: UNTYPED,
                    namespace_definitions: DEFAULT_NS
                },
                Item::EndElement
            ],
            qnames
        ),
        None
    );
}

#[test]
fn test_nested_element() {
    let qnames = test_qnames();
    let a = qnames.find_qname(Namespace(""), NCName("a")).unwrap();
    let b = qnames.find_qname(Namespace(""), NCName("b")).unwrap();
    assert_eq!(
        test_builder(
            &[
                Item::StartElement {
                    qname: a,
                    r#type: UNTYPED,
                    namespace_definitions: DEFAULT_NS
                },
                Item::StartElement {
                    qname: b,
                    r#type: UNTYPED,
                    namespace_definitions: DEFAULT_NS
                },
                Item::EndElement,
                Item::EndElement,
            ],
            qnames
        ),
        None
    );
}

#[test]
fn test_two_nested_elements() {
    let qnames = test_qnames();
    let a = qnames.find_qname(Namespace(""), NCName("a")).unwrap();
    let b = qnames.find_qname(Namespace(""), NCName("b")).unwrap();
    let c = qnames.find_qname(Namespace(""), NCName("c")).unwrap();
    assert_eq!(
        test_builder(
            &[
                Item::StartElement {
                    qname: a,
                    r#type: UNTYPED,
                    namespace_definitions: DEFAULT_NS
                },
                Item::StartElement {
                    qname: b,
                    r#type: UNTYPED,
                    namespace_definitions: DEFAULT_NS
                },
                Item::EndElement,
                Item::StartElement {
                    qname: c,
                    r#type: UNTYPED,
                    namespace_definitions: DEFAULT_NS
                },
                Item::EndElement,
                Item::EndElement
            ],
            qnames
        ),
        None
    );
}
/*
#[test]
fn test_empty_text() {
    assert_eq!(
        test_builder_simplify(&[Item::StartText, Item::EndText], &[Item::Text("")]),
        Err(Error::EmptyTextAsRoot)
    );
}
*/

#[cfg(test)]
use crate::typed_value::{TypeResolver, DEFAULT_TYPE_RESOLVER};

#[test]
fn test_empty_attribute() {
    let qnames = test_qnames();
    let qname = qnames.find_qname(Namespace(""), NCName("a")).unwrap();
    let untyped_atomic = DEFAULT_TYPE_RESOLVER.xs_untyped_atomic();
    assert_eq!(
        test_builder_simplify(
            &[
                Item::StartAttribute {
                    qname,
                    r#type: untyped_atomic
                },
                Item::EndAttribute
            ],
            &[Item::Attribute {
                qname,
                r#type: untyped_atomic,
                value: ""
            }],
            qnames
        ),
        None
    );
}

#[test]
fn test_simple_attribute() {
    let qnames = test_qnames();
    let qname = qnames.find_qname(Namespace(""), NCName("a")).unwrap();
    let untyped_atomic = DEFAULT_TYPE_RESOLVER.xs_untyped_atomic();
    assert_eq!(
        test_builder_simplify(
            &[
                Item::StartAttribute {
                    qname,
                    r#type: untyped_atomic
                },
                Item::String("b"),
                Item::EndAttribute
            ],
            &[Item::Attribute {
                qname,
                r#type: untyped_atomic,
                value: "b"
            }],
            qnames
        ),
        None
    );
}

// TODO: give error or define new namespace e.g. xmlns:a_1 etc
// 'element {QName("urn:a", "a:a")} {attribute {QName("urn:b","a:a")}{}}'
