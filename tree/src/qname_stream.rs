use crate::{
    node::{Node, Ref},
    qnames::{
        self, NCName, Namespace, NamespaceMarker, Prefix, QNameCollection, QNameCollector, QNameId,
        QNameRef,
    },
    stream as s,
    tree::builder::{self, TreeBuilder},
    typed_value::{XS_UNTYPED, XS_UNTYPED_ATOMIC},
};
use std::fmt::{self, Debug};

#[non_exhaustive]
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Error {
    TreeIsDone,
    Incomplete,
    UndefinedPrefix,
    InvalidPrefix,
    UnexpectedItem(String),
    AttributeAlreadyDefined,
    InvalidComment,
    InvalidProcessingInstruction,
    // empty text as root not is not allowed
    // normally adjacent text nodes are merged and empty text nodes ignored,
    // but there has to be a root node.
    EmptyTextAsRoot,
    InvalidNamespaceDefinition,
    ValidationError,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for Error {}

impl From<builder::Error> for Error {
    fn from(e: builder::Error) -> Self {
        match e {
            builder::Error::UndefinedPrefix => Error::UndefinedPrefix,
            builder::Error::EmptyTextAsRoot => Error::EmptyTextAsRoot,
            builder::Error::InvalidComment => Error::InvalidComment,
            builder::Error::InvalidProcessingInstruction => Error::InvalidProcessingInstruction,
            builder::Error::AttributeAlreadyDefined => Error::AttributeAlreadyDefined,
            builder::Error::InvalidNamespaceDefinition => Error::InvalidNamespaceDefinition,
        }
    }
}

fn unexpected<T: Ref>(item: Item<T>) -> Result<()> {
    Err(Error::UnexpectedItem(format!("{:?}", item)))
}

pub type Result<T> = std::result::Result<T, Error>;

/// Items to build a tree.
pub enum Item<'a, T: Ref> {
    StartDocument,
    EndDocument,
    Namespace {
        prefix: Prefix<'a>,
        local_name: NCName<'a>,
        value: &'a str,
    },
    StartElement {
        qname: QNameRef<'a>,
    },
    EndElement,
    StartAttribute {
        qname: QNameRef<'a>,
    },
    EndAttribute,
    StartText,
    EndText,
    StartComment,
    EndComment,
    // an xs:QName with the value of the target property in the local-name and an empty namespace URI and empty prefix
    StartProcessingInstruction {
        target: QNameRef<'a>,
    },
    EndProcessingInstruction,
    Text(&'a str),
    Comment(&'a str),
    ProcessingInstruction {
        target: QNameRef<'a>,
        content: &'a str,
    },
    String(&'a str),
    Char(char),
    Node(Node<T>),
}

impl<'a, T: Ref> Debug for Item<'a, T> {
    fn fmt(&self, _f: &mut fmt::Formatter) -> fmt::Result {
        Ok(())
    }
}

pub trait ItemStream<T: Ref> {
    fn next(&mut self, item: Item<T>) -> Result<()>;
    fn write_str(&mut self, s: &str) -> Result<()>;
    fn status(&self) -> s::Status;
}

//    fn build(self) -> Result<O>;

type State<T, O, S> = fn(&mut QNameStream<T, O, S>, item: Item<T>) -> Result<()>;

pub struct QNameStream<T: Ref, O, S: s::ItemStream> {
    target: S,
    ns_stack: Vec<NamespaceMarker>,
    qnames: QNameCollector,
    state: State<T, O, S>,
    attributes: Vec<QNameId>,
    defining_namespaces: bool,
}

fn map_err(err: qnames::Error) -> Error {
    match err {
        qnames::Error::AlreadyDefinedPrefix => Error::AttributeAlreadyDefined,
        qnames::Error::UndefinedPrefix => Error::UndefinedPrefix,
        qnames::Error::InvalidNamespaceDefinition => Error::InvalidNamespaceDefinition,
        e => unimplemented!("{}", e),
    }
}

pub type QNameTreeStream<T, O> = QNameStream<T, O, s::IteratorToTree<O>>;

impl<T: Ref, TV: 'static>
    QNameStream<T, crate::tree::Tree<TV>, s::IteratorToTree<crate::tree::Tree<TV>>>
{
    pub fn tree_builder(qnames: QNameCollection) -> Self {
        Self::new(
            qnames,
            s::IteratorToTree::new(Box::new(TreeBuilder::empty())),
        )
    }
}

impl<T: Ref, O, S: s::ItemStreamBuilder<O>> QNameStream<T, O, S> {
    pub fn new(qnames: QNameCollection, target: S) -> Self {
        let qnames = QNameCollector::from_qname_collection(qnames);
        Self {
            target,
            ns_stack: Vec::new(),
            qnames,
            state: QNameStream::next_init,
            attributes: Vec::new(),
            defining_namespaces: false,
        }
    }
    fn start_element(&mut self, qname: QNameRef) -> Result<()> {
        self.ensure_new_namespaces();
        self.qnames.finish_namespace_context();
        self.defining_namespaces = false;
        let qname = self.qnames.add_qname_by_ref(qname);
        if qname.prefix_is_xmlns() {
            return Err(Error::InvalidPrefix);
        }
        let r#type = XS_UNTYPED;
        let namespace_definitions = self.qnames.namespace_context();
        self.target.next(s::Item::StartElement {
            qname,
            r#type,
            namespace_definitions,
        });
        self.attributes.clear();
        // start collecting attributes in the buffer
        self.state = QNameStream::next_attributes;
        Ok(())
    }
    fn start_attribute(&mut self, qname: QNameRef) -> Result<()> {
        let qname = self.qnames.add_qname_by_ref(qname);
        if self.attributes.contains(&qname) {
            return Err(Error::AttributeAlreadyDefined);
        }
        self.attributes.push(qname);
        let r#type = XS_UNTYPED_ATOMIC;
        self.target.next(s::Item::StartAttribute { qname, r#type });
        Ok(())
    }
    fn ensure_new_namespaces(&mut self) {
        if !self.defining_namespaces {
            self.ns_stack.push(self.qnames.start_namespace_context());
            self.defining_namespaces = true;
        }
    }
    fn define_namespace(&mut self, prefix: Prefix, local_name: NCName, value: &str) -> Result<()> {
        if prefix.is_empty() && local_name.0 == "xmlns" {
            self.ensure_new_namespaces();
            self.qnames
                .define_namespace(prefix, Namespace(value))
                .map_err(map_err)?;
        } else if prefix.0 == "xmlns" {
            self.ensure_new_namespaces();
            self.qnames
                .define_namespace(Prefix(local_name.0), Namespace(value))
                .map_err(map_err)?;
        }
        self.state = QNameStream::next_namespace;
        Ok(())
    }
    fn next_init(&mut self, item: Item<T>) -> Result<()> {
        match item {
            Item::StartDocument => {
                self.ensure_new_namespaces();
                self.qnames.finish_namespace_context();
                self.defining_namespaces = false;
                self.target.next(s::Item::StartDocument);
                self.state = QNameStream::next_content;
            }
            Item::Namespace {
                prefix,
                local_name,
                value,
            } => {
                self.define_namespace(prefix, local_name, value)?;
            }
            Item::StartElement { qname } => {
                self.start_element(qname)?;
            }
            Item::StartAttribute { qname } => {
                self.start_attribute(qname)?;
                self.state = QNameStream::next_single_node;
            }
            Item::StartText => {
                self.target.next(s::Item::StartText);
                self.state = QNameStream::next_single_node;
            }
            Item::StartComment => {
                self.target.next(s::Item::StartComment);
                self.state = QNameStream::next_single_node;
            }
            Item::StartProcessingInstruction { target } => {
                let target = self.qnames.add_qname_by_ref(target);
                self.target
                    .next(s::Item::StartProcessingInstruction { target });
                self.state = QNameStream::next_single_node;
            }
            Item::Text(content) => {
                self.target.next(s::Item::Text(content));
                self.state = QNameStream::next_end;
            }
            Item::Comment(content) => {
                self.target.next(s::Item::Comment(content));
                self.state = QNameStream::next_end;
            }
            Item::ProcessingInstruction { target, content } => {
                let target = self.qnames.add_qname_by_ref(target);
                self.target
                    .next(s::Item::ProcessingInstruction { target, content });
                self.state = QNameStream::next_end;
            }
            e => panic!("{:?}", e),
        }
        Ok(())
    }
    fn next_single_node(&mut self, item: Item<T>) -> Result<()> {
        match item {
            Item::String(s) => {
                self.target.write_str(s);
            }
            Item::Char(c) => {
                self.target.next(s::Item::Char(c));
            }
            Item::EndAttribute => {
                self.target.next(s::Item::EndAttribute);
                self.state = QNameStream::next_end;
            }
            Item::EndText => {
                self.target.next(s::Item::EndText);
                self.state = QNameStream::next_end;
            }
            Item::EndComment => {
                self.target.next(s::Item::EndComment);
                self.state = QNameStream::next_end;
            }
            Item::EndProcessingInstruction => {
                self.target.next(s::Item::EndProcessingInstruction);
                self.state = QNameStream::next_end;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_namespace(&mut self, item: Item<T>) -> Result<()> {
        match item {
            Item::Namespace {
                prefix,
                local_name,
                value,
            } => {
                self.define_namespace(prefix, local_name, value)?;
            }
            Item::StartElement { qname } => {
                self.start_element(qname)?;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_attributes(&mut self, item: Item<T>) -> Result<()> {
        match item {
            Item::StartAttribute { qname } => {
                self.start_attribute(qname)?;
                self.state = QNameStream::next_attribute;
            }
            item => {
                self.state = QNameStream::next_content;
                return self.next_content(item);
            }
        }
        Ok(())
    }
    fn next_attribute(&mut self, item: Item<T>) -> Result<()> {
        match item {
            Item::String(s) => {
                self.target.next(s::Item::String(s));
            }
            Item::Char(c) => {
                self.target.next(s::Item::Char(c));
            }
            Item::EndAttribute => {
                self.target.next(s::Item::EndAttribute);
                self.state = QNameStream::next_attributes;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_content(&mut self, item: Item<T>) -> Result<()> {
        match item {
            Item::Namespace {
                prefix,
                local_name,
                value,
            } => {
                self.define_namespace(prefix, local_name, value)?;
            }
            Item::StartElement { qname } => {
                self.start_element(qname)?;
            }
            Item::StartText => {
                self.target.next(s::Item::StartText);
                self.state = QNameStream::next_text;
            }
            Item::Text(content) => self.target.next(s::Item::Text(content)),
            Item::StartComment => {
                self.target.next(s::Item::StartComment);
                self.state = QNameStream::next_comment;
            }
            Item::Comment(content) => self.target.next(s::Item::Comment(content)),
            Item::StartProcessingInstruction { target } => {
                let target = self.qnames.add_qname_by_ref(target);
                self.target
                    .next(s::Item::StartProcessingInstruction { target });
                self.state = QNameStream::next_processing_instruction;
            }
            Item::ProcessingInstruction { target, content } => {
                let target = self.qnames.add_qname_by_ref(target);
                self.target
                    .next(s::Item::ProcessingInstruction { target, content });
            }
            Item::EndElement => {
                let marker = self.ns_stack.pop().unwrap();
                self.qnames.leave_namespace_context(marker);
                self.target.next(s::Item::EndElement);
                if self.ns_stack.is_empty() {
                    self.state = QNameStream::next_end;
                }
            }
            Item::EndDocument => {
                self.target.next(s::Item::EndDocument);
                self.state = QNameStream::next_end;
            }
            Item::String(content) => {
                self.target.next(s::Item::Text(content));
            }
            Item::Char(content) => {
                self.target.next(s::Item::StartText);
                self.target.next(s::Item::Char(content));
                self.target.next(s::Item::EndText);
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_text(&mut self, item: Item<T>) -> Result<()> {
        match item {
            Item::String(s) => {
                self.target.write_str(s);
            }
            Item::Char(c) => {
                self.target.next(s::Item::Char(c));
            }
            Item::EndText => {
                self.target.next(s::Item::EndText);
                self.state = QNameStream::next_content;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_comment(&mut self, item: Item<T>) -> Result<()> {
        match item {
            Item::String(s) => {
                self.target.write_str(s);
            }
            Item::Char(c) => {
                self.target.next(s::Item::Char(c));
            }
            Item::EndComment => {
                self.target.next(s::Item::EndComment);
                self.state = QNameStream::next_content;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_processing_instruction(&mut self, item: Item<T>) -> Result<()> {
        match item {
            Item::String(s) => {
                self.target.write_str(s);
            }
            Item::Char(c) => {
                self.target.next(s::Item::Char(c));
            }
            Item::EndProcessingInstruction => {
                self.target.next(s::Item::EndProcessingInstruction);
                self.state = QNameStream::next_content;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_end(&mut self, _item: Item<T>) -> Result<()> {
        Err(Error::TreeIsDone)
    }
    pub fn build(self) -> Result<O> {
        Ok(self.target.build(self.qnames.collect()))
    }
}

impl<T: Ref, O, S: s::ItemStream> ItemStream<T> for QNameStream<T, O, S> {
    fn next(&mut self, item: Item<T>) -> Result<()> {
        (self.state)(self, item)
    }
    fn write_str(&mut self, s: &str) -> Result<()> {
        self.next(Item::String(s))
    }
    fn status(&self) -> s::Status {
        self.target.status()
    }
}
