// this struct should become a group of structs with the same main api
// but differen performance trade offs.
//
// The parameters on which it can vary:
//   - sorting or no sorting
//   - u32 or usize
//   - different pointers:
//     - one number that points to a struct with two numbers that point to str
//     - two numbers (start and end)
//     - additional pointer to the store packed in the pointer
//  Even the largest of these is ony equal in stack size as std::string::String

use std::cmp::Ordering;
use std::ops::Index;

#[derive(Clone, Debug)]
struct StringRef {
    start: u32,
    end: u32,
    index: u32,
}

// Within a StringCollection the strings are sorted, so sorting by StringId
// is just like sorting by the underlying strings.
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Hash)]
pub struct StringId {
    pub id: u32,
}
impl From<StringId> for usize {
    fn from(v: StringId) -> Self {
        v.id as usize
    }
}

struct StringSorter {
    buffer: String,
    refs: Vec<StringRef>,
}
fn slice<'a>(buffer: &'a str, r: &StringRef) -> &'a str {
    &buffer[r.start as usize..r.end as usize]
}
impl StringSorter {
    fn from_string_collection(collection: StringCollection) -> Self {
        let buffer = collection.buffer;
        let refs = collection
            .starts
            .windows(2)
            .enumerate()
            .map(|(i, w)| StringRef {
                index: i as u32,
                start: w[0],
                end: w[1],
            })
            .collect();
        Self { buffer, refs }
    }
    /// Sort the references by the strings that they point to.
    fn sort(&mut self) {
        let buffer = &self.buffer;
        self.refs.sort_unstable_by_key(|s| slice(buffer, s));
    }
    /// Remove duplicate strings from the refs array and create an array that
    /// translates from the old order to the new order.
    fn deduplicate_and_translate(&mut self) -> (Box<[StringId]>, usize) {
        let buffer = &self.buffer;
        let refs = &mut self.refs;
        if refs.is_empty() {
            return (Vec::new().into_boxed_slice(), 0);
        }
        let refs_len = refs.len();
        let mut translation = vec![StringId { id: 0 }; refs_len];
        translation[refs[0].index as usize] = StringId { id: 0 };
        let mut to = 0;
        let mut prev_str = slice(buffer, &refs[0]);
        let mut new_len = prev_str.len();
        for i in 1..refs_len {
            let r = refs[i].clone();
            let str = slice(buffer, &r);
            if str != prev_str {
                to += 1;
                refs[to].start = r.start;
                refs[to].end = r.end;
                new_len += str.len();
                prev_str = str;
            }
            translation[r.index as usize] = StringId { id: to as u32 };
        }
        refs.truncate(to + 1);
        (translation.into_boxed_slice(), new_len)
    }
    /// Create a new string buffer that contains each string only once
    /// in sorted order.
    fn create_new_buffer(&mut self, new_len: usize) -> String {
        let buffer = &self.buffer;
        let refs = &mut self.refs;
        let mut new_buf = String::with_capacity(new_len);
        for r in refs.iter_mut() {
            let start = new_buf.len() as u32;
            new_buf.push_str(slice(buffer, r));
            r.start = start;
        }
        new_buf
    }
    fn get_starts(mut refs: Vec<StringRef>, last: u32) -> Vec<u32> {
        refs.push(StringRef {
            start: last,
            end: 0,
            index: 0,
        });
        refs.iter().map(|r| r.start).collect()
    }
    /// Collect the strings in a new StringCollection.
    /// Also return an array that translates from the old order to the new order.
    fn collect_sort(mut self) -> (Box<[StringId]>, StringCollection) {
        self.sort();
        let (translation, new_len) = self.deduplicate_and_translate();
        let new_buffer = self.create_new_buffer(new_len);
        let starts = StringSorter::get_starts(self.refs, new_buffer.len() as u32);
        let collection = StringCollection {
            buffer: new_buffer,
            starts,
            sorted: true,
        };
        (translation, collection)
    }
}

#[derive(Clone, Debug, Default)]
pub struct StringCollection {
    buffer: String,
    starts: Vec<u32>,
    sorted: bool,
}

impl StringCollection {
    /// Creates a new empty StringCollection with a particular capacity.
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            buffer: String::with_capacity(capacity),
            starts: vec![0],
            sorted: false,
        }
    }
    /// Add a string to the StringBuffer and return an identifier for it.
    pub fn add_string(&mut self, string: &str) -> StringId {
        self.buffer.push_str(string);
        let id = self.starts.len() as u32 - 1;
        self.starts.push(self.buffer.len() as u32);
        StringId { id }
    }
    pub fn len(&self) -> usize {
        self.starts.len()
    }
    pub fn is_empty(&self) -> bool {
        self.starts.is_empty()
    }
    pub fn str(&self) -> &str {
        &self.buffer
    }
    /// Add string data, but do not create a string from it yet.
    /// This allows to create a string incrementally.
    pub fn push_str(&mut self, s: &str) {
        self.buffer.push_str(s);
    }
    pub fn push(&mut self, c: char) {
        self.buffer.push(c);
    }
    // Extend the string returned by `add_string` to the full length of the
    // buffer.
    // This is e.g. used to merge adjacent text nodes in a DOM.
    pub fn extend_last_string(&mut self) {
        *self.starts.last_mut().unwrap() = self.buffer.len() as u32;
    }
    // the string that is currently being pushed
    pub fn current_string(&self) -> &str {
        let start = *self.starts.last().unwrap();
        &self.buffer[start as usize..]
    }
    pub fn get(&self, i: StringId) -> &str {
        let start = self.starts[i.id as usize] as usize;
        let end = self.starts[(i.id + 1) as usize] as usize;
        &self.buffer[start..end]
    }
    pub fn find(&self, s: &str) -> Option<StringId> {
        // use as_bytes to avoid checking character boundaries
        let s = s.as_bytes();
        if self.sorted {
            match binary_search_by_index(self.starts.len() - 1, |i| {
                self.get(StringId { id: i as u32 }).as_bytes().cmp(s)
            }) {
                Ok(pos) => Some(StringId { id: pos as u32 }),
                Err(_) => None,
            }
        } else {
            let buffer = self.buffer.as_bytes();
            for (id, w) in self.starts.windows(2).enumerate() {
                let w = &buffer[w[0] as usize..w[1] as usize];
                if w == s {
                    return Some(StringId { id: id as u32 });
                }
            }
            None
        }
    }
    pub fn sort_and_deduplicate(self) -> (Box<[StringId]>, StringCollection) {
        let s = StringSorter::from_string_collection(self);
        s.collect_sort()
    }
}

fn binary_search_by_index<F>(len: usize, mut f: F) -> Result<usize, usize>
where
    F: FnMut(usize) -> Ordering,
{
    if len == 0 {
        return Err(0);
    }
    let mut l = 0;
    let mut r = len - 1;
    loop {
        if l > r {
            return Err(l);
        }
        let m = (l + r) >> 1;
        match f(m) {
            Ordering::Less => {
                l = m + 1;
            }
            Ordering::Greater => {
                r = m - 1;
            }
            Ordering::Equal => return Ok(m),
        }
    }
}

impl Index<StringId> for Vec<StringId> {
    type Output = StringId;

    fn index(&self, id: StringId) -> &StringId {
        &self[id.id as usize]
    }
}

#[test]
fn test_string_collector() {
    let mut c = StringCollection::with_capacity(1000);
    let refs = [
        c.add_string("xy"),
        c.add_string("1234"),
        c.add_string("xy"),
        c.add_string("abc"),
    ];
    assert_eq!(
        refs,
        [
            StringId { id: 0 },
            StringId { id: 1 },
            StringId { id: 2 },
            StringId { id: 3 },
        ]
    );
    let (translation, c) = c.sort_and_deduplicate();
    assert_eq!(
        &translation[..],
        &[
            StringId { id: 2 },
            StringId { id: 0 },
            StringId { id: 2 },
            StringId { id: 1 },
        ]
    );
    assert_eq!(c.get(StringId { id: 0 }), "1234");
    assert_eq!(c.get(StringId { id: 1 }), "abc");
    assert_eq!(c.get(StringId { id: 2 }), "xy");
}
