use crate::{
    node::Tree,
    qnames::{self, NCName, Namespace, NamespaceMarker, Prefix, QNameCollector, QNameId},
    stream as s,
    tree::builder,
    typed_value::{XS_UNTYPED, XS_UNTYPED_ATOMIC},
};

#[non_exhaustive]
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Error {
    TreeIsDone,
    Incomplete,
    UndefinedPrefix,
    InvalidPrefix,
    UnexpectedItem(String),
    AttributeAlreadyDefined,
    InvalidComment,
    InvalidProcessingInstruction,
    // empty text as root not is not allowed
    // normally adjacent text nodes are merged and empty text nodes ignored,
    // but there has to be a root node.
    EmptyTextAsRoot,
    InvalidNamespaceDefinition,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for Error {}

impl From<builder::Error> for Error {
    fn from(e: builder::Error) -> Self {
        match e {
            builder::Error::UndefinedPrefix => Error::UndefinedPrefix,
            builder::Error::EmptyTextAsRoot => Error::EmptyTextAsRoot,
            builder::Error::InvalidComment => Error::InvalidComment,
            builder::Error::InvalidProcessingInstruction => Error::InvalidProcessingInstruction,
            builder::Error::AttributeAlreadyDefined => Error::AttributeAlreadyDefined,
            builder::Error::InvalidNamespaceDefinition => Error::InvalidNamespaceDefinition,
        }
    }
}

fn unexpected(item: Item) -> Result<()> {
    Err(Error::UnexpectedItem(format!("{:?}", item)))
}

pub type Result<T> = std::result::Result<T, Error>;

/// Items to build a tree.
#[derive(Debug)]
pub enum Item<'a> {
    StartDocument,
    EndDocument,
    DefaultNamespace(&'a str),
    Namespace {
        prefix: Prefix<'a>,
        namespace: &'a str,
    },
    StartElement {
        prefix: Prefix<'a>,
        local_name: NCName<'a>,
    },
    EndElement,
    StartAttribute {
        prefix: Prefix<'a>,
        local_name: NCName<'a>,
    },
    EndAttribute,
    StartText,
    EndText,
    StartComment,
    EndComment,
    // an xs:QName with the value of the target property in the local-name and an empty namespace URI and empty prefix
    StartProcessingInstruction {
        target: NCName<'a>,
    },
    EndProcessingInstruction,
    Text(&'a str),
    Comment(&'a str),
    ProcessingInstruction {
        target: NCName<'a>,
        content: &'a str,
    },
    String(&'a str),
    Char(char),
}

pub trait ItemStream {
    fn next(&mut self, item: Item) -> Result<()>;
    fn write_str(&mut self, s: &str) -> Result<()>;
    fn status(&self) -> s::Status;
}

pub trait ItemStreamBuilder<T>: ItemStream {
    fn build(self) -> Result<T>;
}

pub struct StringNamesToQNames<T: Tree, R, S: s::ItemStream> {
    target: S,
    ns_stack: Vec<NamespaceMarker>,
    qnames: QNameCollector,
    state: fn(&mut StringNamesToQNames<T, R, S>, item: Item) -> Result<()>,
    attributes: Vec<QNameId>,
    defining_namespaces: bool,
    last_comment_char: char,
}

impl<T: Tree, R: Tree> StringNamesToQNames<T, R, super::stream::IteratorToTree<R>> {
    pub fn has_errors(&self) -> bool {
        self.target.builder().has_errors()
    }
    pub fn errors(self) -> Vec<String> {
        let qnames = self.qnames.collect();
        self.target.builder().errors(qnames)
    }
}

fn map_err(err: qnames::Error) -> Error {
    match err {
        qnames::Error::AlreadyDefinedPrefix => Error::AttributeAlreadyDefined,
        qnames::Error::UndefinedPrefix => Error::UndefinedPrefix,
        qnames::Error::InvalidNamespaceDefinition => Error::InvalidNamespaceDefinition,
        e => unimplemented!("{}", e),
    }
}

impl<T: Tree, R, S: s::ItemStream> StringNamesToQNames<T, R, S> {
    pub fn new(target: S) -> Self {
        Self::from_qname_collector(target, QNameCollector::default())
    }
    pub fn from_qname_collector(target: S, qnames: QNameCollector) -> Self {
        Self {
            target,
            ns_stack: Vec::new(),
            qnames,
            state: StringNamesToQNames::next_init,
            attributes: Vec::new(),
            defining_namespaces: false,
            last_comment_char: ' ',
        }
    }
    pub fn target(&self) -> &S {
        &self.target
    }
    fn start_element(&mut self, prefix: Prefix, local_name: NCName) -> Result<()> {
        self.ensure_new_namespaces();
        self.qnames.finish_namespace_context();
        self.defining_namespaces = false;
        let qname = self
            .qnames
            .add_qname_by_prefix(prefix, local_name)
            .map_err(map_err)?;
        if qname.prefix_is_xmlns() {
            return Err(Error::InvalidPrefix);
        }
        let r#type = XS_UNTYPED;
        let namespace_definitions = self.qnames.namespace_context();
        self.target.next(s::Item::StartElement {
            qname,
            r#type,
            namespace_definitions,
        });
        self.attributes.clear();
        // start collecting attributes in the buffer
        self.state = StringNamesToQNames::next_attributes;
        Ok(())
    }
    fn start_attribute(&mut self, prefix: Prefix, local_name: NCName) -> Result<()> {
        let qname = self
            .qnames
            .add_attribute_qname(prefix, local_name)
            .map_err(map_err)?;
        if self.attributes.contains(&qname) {
            return Err(Error::AttributeAlreadyDefined);
        }
        self.attributes.push(qname);
        let r#type = XS_UNTYPED_ATOMIC;
        self.target.next(s::Item::StartAttribute { qname, r#type });
        Ok(())
    }
    fn ensure_new_namespaces(&mut self) {
        if !self.defining_namespaces {
            self.ns_stack.push(self.qnames.start_namespace_context());
            self.defining_namespaces = true;
        }
        self.state = StringNamesToQNames::next_namespace;
    }
    fn define_namespace(&mut self, prefix: Prefix, namespace: &str) -> Result<()> {
        self.ensure_new_namespaces();
        self.qnames
            .define_namespace(prefix, Namespace(namespace))
            .map_err(map_err)?;
        Ok(())
    }
    fn define_default_namespace(&mut self, namespace: &str) -> Result<()> {
        if namespace == "http://www.w3.org/2000/xmlns/" {
            return Err(Error::InvalidNamespaceDefinition);
        }
        self.ensure_new_namespaces();
        self.qnames
            .define_namespace(Prefix(""), Namespace(namespace))
            .map_err(map_err)?;
        Ok(())
    }
    fn next_init(&mut self, item: Item) -> Result<()> {
        match item {
            Item::StartDocument => {
                self.target.next(s::Item::StartDocument);
                self.ensure_new_namespaces();
                self.qnames.finish_namespace_context();
                self.defining_namespaces = false;
                self.state = StringNamesToQNames::next_content;
            }
            Item::DefaultNamespace(namespace) => {
                self.define_default_namespace(namespace)?;
            }
            Item::Namespace { prefix, namespace } => {
                self.define_namespace(prefix, namespace)?;
            }
            Item::StartElement { prefix, local_name } => {
                self.start_element(prefix, local_name)?;
            }
            Item::StartAttribute { prefix, local_name } => {
                self.start_attribute(prefix, local_name)?;
                self.state = StringNamesToQNames::next_single_node;
            }
            Item::StartText => {
                self.state = StringNamesToQNames::next_single_node;
            }
            Item::StartComment => {
                self.last_comment_char = ' ';
                self.state = StringNamesToQNames::next_single_node;
            }
            Item::StartProcessingInstruction { target } => {
                let target = self.qnames.add_local_qname(target);
                self.target
                    .next(s::Item::StartProcessingInstruction { target });
                self.state = StringNamesToQNames::next_single_node;
            }
            Item::Text(content) => {
                self.target.next(s::Item::Text(content));
                self.state = StringNamesToQNames::next_end;
            }
            Item::Comment(content) => {
                self.check_comment(content)?;
                self.target.next(s::Item::Comment(content));
                self.state = StringNamesToQNames::next_end;
            }
            Item::ProcessingInstruction { target, content } => {
                let target = self.qnames.add_local_qname(target);
                self.target
                    .next(s::Item::ProcessingInstruction { target, content });
                self.state = StringNamesToQNames::next_end;
            }
            e => panic!("{:?}", e),
        }
        Ok(())
    }
    fn next_single_node(&mut self, item: Item) -> Result<()> {
        match item {
            Item::String(s) => {
                self.target.write_str(s);
            }
            Item::Char(c) => {
                self.target.next(s::Item::Char(c));
            }
            Item::EndAttribute => {
                self.target.next(s::Item::EndAttribute);
                self.state = StringNamesToQNames::next_end;
            }
            Item::EndText => {
                self.target.next(s::Item::EndText);
                self.state = StringNamesToQNames::next_end;
            }
            Item::EndComment => {
                if self.last_comment_char == '-' {
                    return Err(Error::InvalidComment);
                }
                self.target.next(s::Item::EndComment);
                self.state = StringNamesToQNames::next_end;
            }
            Item::EndProcessingInstruction => {
                self.target.next(s::Item::EndProcessingInstruction);
                self.state = StringNamesToQNames::next_end;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_namespace(&mut self, item: Item) -> Result<()> {
        match item {
            Item::DefaultNamespace(namespace) => {
                self.define_default_namespace(namespace)?;
            }
            Item::Namespace { prefix, namespace } => {
                self.define_namespace(prefix, namespace)?;
            }
            Item::StartElement { prefix, local_name } => {
                self.start_element(prefix, local_name)?;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_attributes(&mut self, item: Item) -> Result<()> {
        match item {
            Item::StartAttribute { prefix, local_name } => {
                self.start_attribute(prefix, local_name)?;
                self.state = StringNamesToQNames::next_attribute;
            }
            item => {
                self.state = StringNamesToQNames::next_content;
                return self.next_content(item);
            }
        }
        Ok(())
    }
    fn next_attribute(&mut self, item: Item) -> Result<()> {
        match item {
            Item::String(s) => {
                self.target.next(s::Item::String(s));
            }
            Item::Char(c) => {
                self.target.next(s::Item::Char(c));
            }
            Item::EndAttribute => {
                self.target.next(s::Item::EndAttribute);
                self.state = StringNamesToQNames::next_attributes;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_content(&mut self, item: Item) -> Result<()> {
        match item {
            Item::DefaultNamespace(namespace) => {
                self.define_default_namespace(namespace)?;
            }
            Item::Namespace { prefix, namespace } => {
                self.define_namespace(prefix, namespace)?;
            }
            Item::StartElement { prefix, local_name } => {
                self.start_element(prefix, local_name)?;
            }
            Item::StartText => {
                self.target.next(s::Item::StartText);
                self.state = StringNamesToQNames::next_text;
            }
            Item::Text(content) => self.target.next(s::Item::Text(content)),
            Item::StartComment => {
                self.last_comment_char = ' ';
                self.target.next(s::Item::StartComment);
                self.state = StringNamesToQNames::next_comment;
            }
            Item::Comment(content) => {
                self.check_comment(content)?;
                self.target.next(s::Item::Comment(content));
            }
            Item::StartProcessingInstruction { target } => {
                let target = self.qnames.add_local_qname(target);
                self.target
                    .next(s::Item::StartProcessingInstruction { target });
                self.state = StringNamesToQNames::next_processing_instruction;
            }
            Item::ProcessingInstruction { target, content } => {
                let target = self.qnames.add_local_qname(target);
                self.target
                    .next(s::Item::ProcessingInstruction { target, content });
            }
            Item::EndElement => {
                let marker = self.ns_stack.pop().unwrap();
                self.qnames.leave_namespace_context(marker);
                self.target.next(s::Item::EndElement);
                if self.ns_stack.is_empty() {
                    self.state = StringNamesToQNames::next_end;
                }
            }
            Item::EndDocument => {
                self.target.next(s::Item::EndDocument);
                self.state = StringNamesToQNames::next_end;
            }
            Item::String(content) => {
                self.target.next(s::Item::Text(content));
            }
            Item::Char(content) => {
                self.target.next(s::Item::StartText);
                self.target.next(s::Item::Char(content));
                self.target.next(s::Item::EndText);
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_text(&mut self, item: Item) -> Result<()> {
        match item {
            Item::String(s) => {
                self.target.write_str(s);
            }
            Item::Char(c) => {
                self.target.next(s::Item::Char(c));
            }
            Item::EndText => {
                self.target.next(s::Item::EndText);
                self.state = StringNamesToQNames::next_content;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_comment(&mut self, item: Item) -> Result<()> {
        match item {
            Item::String(s) => {
                self.check_comment(s)?;
                self.target.write_str(s);
            }
            Item::Char(c) => {
                if self.last_comment_char == '-' && c == '-' {
                    return Err(Error::InvalidComment);
                }
                self.last_comment_char = '-';
                self.target.next(s::Item::Char(c));
            }
            Item::EndComment => {
                if self.last_comment_char == '-' {
                    return Err(Error::InvalidComment);
                }
                self.target.next(s::Item::EndComment);
                self.state = StringNamesToQNames::next_content;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_processing_instruction(&mut self, item: Item) -> Result<()> {
        match item {
            Item::String(s) => {
                self.target.write_str(s);
            }
            Item::Char(c) => {
                self.target.next(s::Item::Char(c));
            }
            Item::EndProcessingInstruction => {
                self.target.next(s::Item::EndProcessingInstruction);
                self.state = StringNamesToQNames::next_content;
            }
            e => return unexpected(e),
        }
        Ok(())
    }
    fn next_end(&mut self, _item: Item) -> Result<()> {
        Err(Error::TreeIsDone)
    }
    fn check_comment(&mut self, comment: &str) -> Result<()> {
        if comment.ends_with('-') {
            self.last_comment_char = '-';
        }
        if comment.contains("--") {
            Err(Error::InvalidComment)
        } else {
            Ok(())
        }
    }
}

impl<T: Tree, R, S: s::ItemStream> ItemStream for StringNamesToQNames<T, R, S> {
    fn next(&mut self, item: Item) -> Result<()> {
        (self.state)(self, item)
    }
    fn write_str(&mut self, s: &str) -> Result<()> {
        self.next(Item::String(s))
    }
    fn status(&self) -> s::Status {
        self.target.status()
    }
}

impl<T: Tree, R, S: s::ItemStreamBuilder<R>> ItemStreamBuilder<R> for StringNamesToQNames<T, R, S> {
    fn build(self) -> Result<R> {
        Ok(self.target.build(self.qnames.collect()))
    }
}
