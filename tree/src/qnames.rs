#![allow(clippy::upper_case_acronyms)]
use crate::string_collection::{StringCollection, StringId};
use std::cmp::Ordering;
use std::sync::{Arc, Mutex};

// TODO
// - the prefix xml must not be bound to any namespace URI other than http://www.w3.org/XML/1998/namespace
// - no prefix other than xml may be bound to this namespace URI
// - The prefix xmlns must not be bound to any namespace URI, and no prefix may be bound to the namespace URI http://www.w3.org/2000/xmlns/.

#[non_exhaustive]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Error {
    UndefinedPrefix,
    AlreadyDefinedPrefix,
    InvalidAttributeName,
    InvalidNamespaceDefinition,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for Error {}

type Result<T> = std::result::Result<T, Error>;

const EMPTY_PREFIX: PrefixId = PrefixId(0);
const XMLNS_PREFIX: PrefixId = PrefixId(1);
pub const XML_PREFIX: PrefixId = PrefixId(2);

pub const EMPTY_NAMESPACE: StringId = StringId { id: 0 };
pub const XMLNS_NAMESPACE: StringId = StringId { id: 1 };
pub const XML_NAMESPACE: StringId = StringId { id: 2 };

const XMLNS_NCNAME: StringId = StringId { id: 3 };

const XSD_NAMESPACE: StringId = StringId { id: 4 };
const XSD_UNTYPED: StringId = StringId { id: 5 };
pub(crate) const XSD_UNTYPED_QNAME: QNameId = QNameId::to(XSD_NAMESPACE.id as u16, XSD_UNTYPED.id);
const XSD_UNTYPED_ATOMIC: StringId = StringId { id: 6 };
pub(crate) const XSD_UNTYPED_ATOMIC_QNAME: QNameId =
    QNameId::to(XSD_NAMESPACE.id as u16, XSD_UNTYPED_ATOMIC.id);

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Prefix<'a>(pub &'a str);

impl<'a> Prefix<'a> {
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    pub fn as_str(&self) -> &'a str {
        self.0
    }
}

impl<'a> std::fmt::Display for Prefix<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Namespace<'a>(pub &'a str);

impl<'a> Namespace<'a> {
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    pub fn as_str(&self) -> &'a str {
        self.0
    }
}

impl<'a> std::fmt::Display for Namespace<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct NCName<'a>(pub &'a str);

impl<'a> NCName<'a> {
    pub fn as_str(&self) -> &'a str {
        self.0
    }
    pub fn as_prefix(&self) -> Prefix<'a> {
        Prefix(self.0)
    }
}

impl<'a> std::fmt::Display for NCName<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Clone, Copy)]
pub struct QNameId(usize);

impl QNameId {
    fn add_prefix(&self, prefix: PrefixId) -> QNameId {
        QNameId::top(prefix.0, self.namespace(), self.local_name())
    }
    fn remove_prefix(&self) -> usize {
        self.0 & !0xFFFF
    }
    pub fn from_usize(value: usize) -> Self {
        Self(value)
    }
    fn local_name(&self) -> u32 {
        (self.0 >> 16) as u32
    }
    fn namespace(&self) -> u16 {
        (self.0 >> 48) as u16
    }
    fn prefix(&self) -> PrefixId {
        PrefixId(self.0 as u16)
    }
    pub fn prefix_is_xmlns(&self) -> bool {
        self.prefix() == XMLNS_PREFIX
    }
    pub fn to_usize(&self) -> usize {
        self.0
    }
    // create a QNameId in which only the namespace is relevant
    pub fn from_namespace(ns: StringId) -> Self {
        Self::top(0, ns.id as u16, 0)
    }
    pub fn compare_namespace(&self, ns: QNameId) -> bool {
        self.namespace() == ns.namespace()
    }
    const fn to(namespace: u16, local_name: u32) -> Self {
        Self((namespace as usize).wrapping_shl(48) + (local_name as usize).wrapping_shl(16))
    }
    fn top(prefix: u16, namespace: u16, local_name: u32) -> Self {
        Self(
            (namespace as usize).wrapping_shl(48)
                + (local_name as usize).wrapping_shl(16)
                + (prefix as usize),
        )
    }
}

impl PartialEq for QNameId {
    fn eq(&self, b: &Self) -> bool {
        self.remove_prefix() == b.remove_prefix()
    }
}

impl Eq for QNameId {}
impl PartialOrd for QNameId {
    fn partial_cmp(&self, b: &Self) -> Option<Ordering> {
        Some(self.cmp(b))
    }
}
impl Ord for QNameId {
    fn cmp(&self, b: &Self) -> Ordering {
        self.remove_prefix().cmp(&b.remove_prefix())
    }
}

impl std::fmt::Debug for QNameId {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "prefix: {} ns: {} local_name: {}",
            self.prefix().0,
            self.namespace(),
            self.local_name()
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct PrefixId(u16);

#[derive(Clone, Copy)]
pub struct QNameRef<'a> {
    qnames: &'a QNameCollection,
    id: QNameId,
}

impl<'a> QNameRef<'a> {
    pub fn to_ref(&self) -> Ref {
        self.qnames.to_ref(self.id)
    }
    pub fn to_owned(&self) -> QName {
        QName {
            qnames: self.qnames.to_owned(),
            id: self.id,
        }
    }
    pub fn qname_collection(&self) -> &QNameCollection {
        self.qnames
    }
}

impl<'a> PartialEq for QNameRef<'a> {
    fn eq(&self, b: &Self) -> bool {
        if Arc::ptr_eq(&self.qnames.0, &b.qnames.0) {
            return self.id == b.id;
        }
        self.to_ref().eq(&b.to_ref())
    }
}
impl<'a> PartialEq<QName> for QNameRef<'a> {
    fn eq(&self, b: &QName) -> bool {
        if Arc::ptr_eq(&self.qnames.0, &b.qnames.0) {
            return self.id == b.id;
        }
        self.to_ref().eq(&b.to_ref())
    }
}

impl<'a> std::fmt::Debug for QNameRef<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        self.to_ref().fmt(f)
    }
}

impl<'a> std::fmt::Display for QNameRef<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        std::fmt::Debug::fmt(self, f)
    }
}

#[derive(Clone)]
pub struct QName {
    qnames: QNameCollection,
    id: QNameId,
}

impl QName {
    pub fn to_ref(&self) -> Ref {
        self.qnames.to_ref(self.id)
    }
    pub fn as_ref(&self) -> QNameRef {
        QNameRef {
            qnames: &self.qnames,
            id: self.id,
        }
    }
    pub fn qname_collection(&self) -> &QNameCollection {
        &self.qnames
    }
    pub fn change_namespace(&self, ns: Namespace) -> QName {
        let r = self.to_ref();
        self.qnames.qname(r.prefix(), ns, r.local_name())
    }
}

impl std::fmt::Debug for QName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        self.to_ref().fmt(f)
    }
}

impl std::fmt::Display for QName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        std::fmt::Debug::fmt(self, f)
    }
}

impl PartialEq for QName {
    fn eq(&self, b: &Self) -> bool {
        if Arc::ptr_eq(&self.qnames.0, &b.qnames.0) {
            return self.id == b.id;
        }
        self.to_ref().eq(&b.to_ref())
    }
}
impl Eq for QName {}
impl PartialOrd for QName {
    fn partial_cmp(&self, b: &Self) -> Option<Ordering> {
        Some(self.cmp(b))
    }
}
impl Ord for QName {
    fn cmp(&self, b: &Self) -> Ordering {
        if Arc::ptr_eq(&self.qnames.0, &b.qnames.0) {
            return self.id.cmp(&b.id);
        }
        self.to_ref().cmp(&b.to_ref())
    }
}
impl<'a> PartialEq<QNameRef<'a>> for QName {
    fn eq(&self, b: &QNameRef<'a>) -> bool {
        if Arc::ptr_eq(&self.qnames.0, &b.qnames.0) {
            return true;
        }
        self.to_ref().eq(&b.to_ref())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct NamespaceMarker(usize);

#[derive(Debug, Clone)]
pub struct QNameCollection(Arc<Mutex<Arc<QNameCollectionInner>>>);

#[derive(Clone)] // TODO: remove Clone
pub struct Ref {
    qnames: Arc<QNameCollectionInner>,
    id: QNameId,
}

impl Ref {
    pub fn namespace(&self) -> Namespace {
        self.qnames.namespace(self.id)
    }
    pub fn local_name(&self) -> NCName {
        self.qnames.local_name(self.id)
    }
    pub fn prefix(&self) -> Prefix {
        self.qnames.prefix(self.id)
    }
}

impl PartialEq for Ref {
    fn eq(&self, b: &Self) -> bool {
        self.local_name() == b.local_name() && self.namespace() == b.namespace()
    }
}
impl Eq for Ref {}
impl PartialOrd for Ref {
    fn partial_cmp(&self, b: &Self) -> Option<Ordering> {
        Some(self.cmp(b))
    }
}
impl Ord for Ref {
    fn cmp(&self, b: &Self) -> Ordering {
        let ord = self.namespace().0.cmp(b.namespace().0);
        if ord == Ordering::Equal {
            self.local_name().0.cmp(b.local_name().0)
        } else {
            ord
        }
    }
}

impl std::fmt::Debug for Ref {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(f, "Q{{{}}}{}", self.namespace(), self.local_name())
    }
}

impl std::fmt::Display for Ref {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        std::fmt::Debug::fmt(self, f)
    }
}

#[derive(Debug, Clone)]
struct QNameCollectionInner {
    strings: StringCollection,
    prefixes: StringCollection,
}

impl QNameCollection {
    pub fn to_ref(&self, name: QNameId) -> Ref {
        Ref {
            qnames: self.0.lock().unwrap().clone(),
            id: name,
        }
    }
    pub fn get(&self, name: QNameId) -> QNameRef {
        QNameRef {
            qnames: self,
            id: name,
        }
    }
    // Add a qname to the QNameCollection.
    pub fn qname(&self, prefix: Prefix, namespace: Namespace, local_name: NCName) -> QName {
        let id = self.add_qname(prefix, namespace, local_name);
        QName {
            qnames: self.clone(),
            id,
        }
    }
    fn with_mut<R>(&self, f: impl Fn(&mut QNameCollectionInner) -> R) -> R {
        let mut r = self.0.lock().unwrap();
        let r = Arc::make_mut(&mut r);
        f(r)
    }
    pub fn add_qname(&self, prefix: Prefix, namespace: Namespace, local_name: NCName) -> QNameId {
        self.with_mut(|r| r.add_qname(prefix, namespace, local_name))
    }
    pub fn add_qname_by_ref(&mut self, qname: QNameRef) -> QNameId {
        if Arc::ptr_eq(&qname.qnames.0, &self.0) {
            return qname.id;
        }
        let qname = qname.to_ref();
        self.add_qname(qname.prefix(), qname.namespace(), qname.local_name())
    }
    pub(crate) fn find_qname(&self, namespace: Namespace, local_name: NCName) -> Option<QNameId> {
        self.0.lock().unwrap().find_qname(namespace, local_name)
    }
    // return xs:untypedAtomic
    pub(crate) fn untyped_atomic(&self) -> QNameRef {
        QNameRef {
            qnames: self,
            id: XSD_UNTYPED_ATOMIC_QNAME,
        }
    }
    // return None if the prefix is not defined
    pub fn qnameid_from_prefix(
        &self,
        prefix: Prefix,
        local_name: NCName,
        namespaces: &[NamespaceDefinition],
    ) -> Option<QNameId> {
        self.with_mut(|r| r.qname_from_prefix(prefix, local_name, namespaces))
    }
    pub fn qname_from_prefix(
        &self,
        prefix: Prefix,
        local_name: NCName,
        namespaces: &[NamespaceDefinition],
    ) -> Option<QName> {
        self.qnameid_from_prefix(prefix, local_name, namespaces)
            .map(|id| QName {
                qnames: self.clone(),
                id,
            })
    }
    // Return a `Ref` from which prefix and namespace strings can be read.
    pub fn namespace_ref(&self, nd: NamespaceDefinition) -> Ref {
        self.to_ref(QNameId::top(nd.prefix.0, nd.namespace.id as u16, 0))
    }
    fn add_local_qname(&self, local_name: NCName) -> QNameId {
        self.add_qname_by_namespace(EMPTY_NAMESPACE, local_name)
    }
    fn add_qname_by_namespace(&self, namespace: StringId, local_name: NCName) -> QNameId {
        self.with_mut(|r| r.add(namespace, local_name))
    }
}

impl Default for QNameCollection {
    fn default() -> Self {
        QNameCollection(Arc::new(Mutex::new(Arc::new(
            QNameCollectionInner::default(),
        ))))
    }
}

impl QNameCollectionInner {
    fn prefix(&self, id: QNameId) -> Prefix {
        self.prefix_id(id.prefix())
    }
    fn prefix_id(&self, id: PrefixId) -> Prefix {
        Prefix(self.prefixes.get(StringId { id: id.0 as u32 }))
    }
    fn namespace(&self, id: QNameId) -> Namespace {
        Namespace(self.strings.get(StringId {
            id: id.namespace() as u32,
        }))
    }
    fn local_name(&self, id: QNameId) -> NCName {
        NCName(self.strings.get(StringId {
            id: id.local_name(),
        }))
    }
    /// Find a QName by namespace and localname.
    pub(crate) fn find_qname(&self, namespace: Namespace, local_name: NCName) -> Option<QNameId> {
        let strings = &self.strings;
        if let Some(ln) = strings.find(local_name.0) {
            if let Some(ns) = strings.find(namespace.0) {
                return Some(QNameId::to(ns.id as u16, ln.id));
            }
        }
        None
    }
    fn add_prefix_and_namespace(
        &mut self,
        prefix: Prefix,
        namespace: Namespace,
    ) -> (PrefixId, StringId) {
        (self.get_or_add_prefix(prefix), self.string_id(namespace.0))
    }
    // return None if the prefix is not defined
    fn qname_from_prefix(
        &mut self,
        prefix: Prefix,
        local_name: NCName,
        namespaces: &[NamespaceDefinition],
    ) -> Option<QNameId> {
        if let Some(prefix_id) = self.prefixes.find(prefix.0) {
            let prefix_id = PrefixId(prefix_id.id as u16);
            if let Some(nd) = namespaces.iter().find(|nd| nd.prefix == prefix_id) {
                let local_name_id = self.string_id(local_name.0);
                Some(QNameId::top(
                    prefix_id.0,
                    nd.namespace.id as u16,
                    local_name_id.id,
                ))
            } else {
                /*
                eprintln!(
                    "Cannot find {}:{} {} {:?}",
                    prefix.0,
                    local_name.0,
                    namespaces.len(),
                    namespaces
                );
                */
                None
            }
        } else {
            None
        }
    }
    fn get_or_add_prefix(&mut self, prefix: Prefix) -> PrefixId {
        PrefixId(
            self.prefixes
                .find(prefix.0)
                .unwrap_or_else(|| self.prefixes.add_string(prefix.0))
                .id as u16,
        )
    }
    fn string_id(&mut self, s: &str) -> StringId {
        if let Some(id) = self.strings.find(s) {
            id
        } else {
            self.strings.add_string(s)
        }
    }
    fn add(&mut self, namespace: StringId, local_name: NCName) -> QNameId {
        let local_name_id = self.string_id(local_name.0);
        QNameId::to(namespace.id as u16, local_name_id.id)
    }
    fn add_qname(&mut self, prefix: Prefix, namespace: Namespace, local_name: NCName) -> QNameId {
        let prefix = self.get_or_add_prefix(prefix);
        let namespace = self.string_id(namespace.0);
        self.add(namespace, local_name).add_prefix(prefix)
    }
}

impl Default for QNameCollectionInner {
    fn default() -> Self {
        let mut c = Self {
            strings: StringCollection::with_capacity(1024),
            prefixes: StringCollection::with_capacity(16),
        };
        let empty = c.add_prefix_and_namespace(Prefix(""), Namespace(""));
        let xmlns =
            c.add_prefix_and_namespace(Prefix("xmlns"), Namespace("http://www.w3.org/2000/xmlns/"));
        let xml = c.add_prefix_and_namespace(
            Prefix("xml"),
            Namespace("http://www.w3.org/XML/1998/namespace"),
        );
        assert_eq!(empty.0, EMPTY_PREFIX);
        assert_eq!(empty.1, EMPTY_NAMESPACE);
        assert_eq!(xmlns.0, XMLNS_PREFIX);
        assert_eq!(xmlns.1, XMLNS_NAMESPACE);
        assert_eq!(xml.0, XML_PREFIX);
        assert_eq!(xml.1, XML_NAMESPACE);
        let xmlns_xmlns = c.string_id("xmlns");
        assert_eq!(xmlns_xmlns, XMLNS_NCNAME);
        let xsd = c.string_id("http://www.w3.org/2001/XMLSchema");
        let untyped = c.string_id("untyped");
        let untyped_atomic = c.string_id("untypedAtomic");
        assert_eq!(xsd, XSD_NAMESPACE);
        assert_eq!(untyped, XSD_UNTYPED);
        assert_eq!(untyped_atomic, XSD_UNTYPED_ATOMIC);
        c
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct NamespaceDefinition {
    pub prefix: PrefixId,
    pub namespace: StringId,
}

#[derive(Clone, Debug)]
pub struct QNameCollector {
    inner: QNameCollection,
    namespace_definitions: Vec<Vec<NamespaceDefinition>>,
    defining_namespaces: bool,
}

pub const INITIAL_NAMESPACE_DEFINITIONS: [NamespaceDefinition; 3] = [
    NamespaceDefinition {
        prefix: EMPTY_PREFIX,
        namespace: EMPTY_NAMESPACE,
    },
    NamespaceDefinition {
        prefix: XMLNS_PREFIX,
        namespace: XMLNS_NAMESPACE,
    },
    NamespaceDefinition {
        prefix: XML_PREFIX,
        namespace: XML_NAMESPACE,
    },
];

impl Default for QNameCollector {
    fn default() -> Self {
        Self::from_qname_collection(QNameCollection::default())
    }
}

impl QNameCollector {
    pub fn to_ref(&self, name: QNameId) -> Ref {
        self.inner.to_ref(name)
    }
    pub fn no_namespaces() -> Self {
        Self {
            inner: QNameCollection::default(),
            namespace_definitions: vec![vec![]],
            defining_namespaces: false,
        }
    }
    pub fn from_qname_collection(qname_collection: QNameCollection) -> Self {
        Self {
            inner: qname_collection,
            namespace_definitions: vec![INITIAL_NAMESPACE_DEFINITIONS.into()],
            defining_namespaces: false,
        }
    }
    pub fn get_namespace(&self, prefix: Prefix) -> Option<NamespaceDefinition> {
        let prefix_id = self.inner.0.lock().unwrap().prefixes.find(prefix.0);
        let ns = prefix_id.and_then(|prefix| {
            // look in the latest non-empty vec
            self.namespace_definitions
                .iter()
                .rev()
                .find(|ns| !ns.is_empty())
                .and_then(|ns| {
                    ns.iter()
                        .find(|nd| nd.prefix.0 == prefix.id as u16)
                        .cloned()
                })
        });
        // if non-empty prefix has empty namespace, it is not defined/undefined
        if !prefix.is_empty() {
            if let Some(ns) = ns {
                if ns.namespace == EMPTY_NAMESPACE {
                    return None;
                }
            }
        }
        ns
    }
    pub fn empty_prefix(&self) -> PrefixId {
        EMPTY_PREFIX
    }
    pub fn define_namespace(
        &mut self,
        prefix: Prefix,
        namespace: Namespace,
    ) -> Result<NamespaceDefinition> {
        let (prefix, namespace) = {
            self.inner
                .with_mut(|r| r.add_prefix_and_namespace(prefix, namespace))
        };
        self.define_namespace_from_ids(prefix, namespace)
    }
    fn define_namespace_from_ids(
        &mut self,
        prefix: PrefixId,
        namespace: StringId,
    ) -> Result<NamespaceDefinition> {
        if (prefix == XML_PREFIX && namespace != XML_NAMESPACE)
            || (prefix != XML_PREFIX && namespace == XML_NAMESPACE)
            || prefix == XMLNS_PREFIX
            || namespace == XMLNS_NAMESPACE
        {
            return Err(Error::InvalidNamespaceDefinition);
        }
        if prefix == XML_PREFIX && namespace != XML_NAMESPACE {
            return Err(Error::InvalidNamespaceDefinition);
        }
        let in_scope_ns = self.namespace_definitions.last_mut().unwrap();
        if in_scope_ns
            .iter()
            .any(|v| v.prefix == prefix && v.namespace != namespace)
        {
            return Err(Error::AlreadyDefinedPrefix);
        }
        Ok(self.define_namespace_from_ids_unchecked(prefix, namespace))
    }
    fn define_namespace_from_ids_unchecked(
        &mut self,
        prefix: PrefixId,
        namespace: StringId,
    ) -> NamespaceDefinition {
        let nd = NamespaceDefinition { prefix, namespace };
        let in_scope_ns = self.namespace_definitions.last_mut().unwrap();
        in_scope_ns.push(nd);
        nd
    }
    pub fn namespace_context(&self) -> &[NamespaceDefinition] {
        // look in the latest non-empty vec
        self.namespace_definitions
            .iter()
            .rev()
            .find(|ns| !ns.is_empty())
            .unwrap()
    }
    pub fn start_namespace_context(&mut self) -> NamespaceMarker {
        assert!(!self.defining_namespaces);
        self.defining_namespaces = true;
        self.namespace_definitions.push(Vec::new());
        NamespaceMarker(self.namespace_definitions.len())
    }
    // call this function after the namespaces have been defined but before
    // resolving any prefixes like the qname of the element
    pub fn finish_namespace_context(&mut self) {
        assert!(self.defining_namespaces);
        self.defining_namespaces = false;
        // if this vec is not empty, add the prefixes that have not been
        // redefined
        let nd_len = self.namespace_definitions.len();
        if nd_len > 1 && !self.namespace_definitions[nd_len - 1].is_empty() {
            let (a, b) = self.namespace_definitions.split_at_mut(nd_len - 1);
            let mut pos = nd_len - 2;
            while pos > 0 && a[pos].is_empty() {
                pos -= 1;
            }
            let a = &a[pos];
            let b = &mut b[0];
            // since a is/should be sorted, this could be probably be faster
            for i in a {
                if !b.iter().any(|nd| nd.prefix == i.prefix) {
                    b.push(*i);
                }
            }
            b.sort();
        }
    }
    // return the namespace context for this element if it is different from
    // the parent context. if it is not different, an empty Vec is returned
    pub fn leave_namespace_context(&mut self, marker: NamespaceMarker) -> Vec<NamespaceDefinition> {
        assert_eq!(marker.0, self.namespace_definitions.len());
        self.namespace_definitions.pop().unwrap()
    }
    pub fn add_qname(&self, prefix: Prefix, namespace: Namespace, local_name: NCName) -> QNameId {
        self.inner.add_qname(prefix, namespace, local_name)
    }
    // Add the QName to this collection.
    pub fn add_qname_by_ref(&mut self, qname: QNameRef) -> QNameId {
        if Arc::ptr_eq(&qname.qnames.0, &self.inner.0) {
            return qname.id;
        }
        let qname = qname.to_ref();
        self.add_qname(qname.prefix(), qname.namespace(), qname.local_name())
    }
    pub fn add_local_qname(&self, local_name: NCName) -> QNameId {
        self.inner.add_local_qname(local_name)
    }
    pub fn add_qname_by_namespace(&self, namespace: StringId, local_name: NCName) -> QNameId {
        self.inner.add_qname_by_namespace(namespace, local_name)
    }
    pub fn add_qname_by_prefix(&self, prefix: Prefix, local_name: NCName) -> Result<QNameId> {
        let nd = self.get_namespace(prefix).ok_or(Error::UndefinedPrefix)?;
        if nd.prefix != EMPTY_PREFIX && nd.namespace == EMPTY_NAMESPACE {
            return Err(Error::UndefinedPrefix);
        }
        Ok(self
            .add_qname_by_namespace(nd.namespace, local_name)
            .add_prefix(nd.prefix))
    }
    // if the prefix is empty, return the empty namespace
    pub(crate) fn add_attribute_qname(
        &self,
        prefix: Prefix,
        local_name: NCName,
    ) -> Result<QNameId> {
        // An attribute with the empty prefix, has the empty namespace.
        let nd = if prefix.is_empty() {
            NamespaceDefinition {
                prefix: EMPTY_PREFIX,
                namespace: EMPTY_NAMESPACE,
            }
        } else {
            self.get_namespace(prefix).ok_or(Error::UndefinedPrefix)?
        };
        Ok(self
            .add_qname_by_namespace(nd.namespace, local_name)
            .add_prefix(nd.prefix))
    }
    pub fn qname_collection(&self) -> QNameCollection {
        self.inner.clone()
    }
    pub fn collect(self) -> QNameCollection {
        self.inner
    }
    // return xs:untyped
    pub fn untyped(&self) -> QNameId {
        QNameId::to(XSD_NAMESPACE.id as u16, XSD_UNTYPED.id)
    }
    // return xs:untypedAtomic
    pub fn untyped_atomic(&self) -> QNameId {
        QNameId::to(XSD_NAMESPACE.id as u16, XSD_UNTYPED_ATOMIC.id)
    }
}

#[test]
fn test_qnameid_to_usize() {
    let ns = 100;
    let local_name = 70000;
    let prefix = 130;
    let qname = QNameId::to(ns, local_name);
    assert_eq!(ns, qname.namespace());
    assert_eq!(local_name, qname.local_name());
    let qname = QNameId::top(prefix, ns, local_name);
    assert_eq!(prefix, qname.prefix().0);
    assert_eq!(ns, qname.namespace());
    assert_eq!(local_name, qname.local_name());
}
