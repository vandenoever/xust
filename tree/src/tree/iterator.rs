use crate::{
    qnames::QNameId,
    stream::Item,
    string_collection::StringId,
    tree::{
        AllNodeIterator, NodeKind, Tree, ATTRIBUTE_CONTENT, ATTRIBUTE_QNAME, ATTRIBUTE_TYPE,
        COMMENT_CONTENT, DOCUMENT, ELEMENT_NAMESPACES, ELEMENT_QNAME, ELEMENT_TYPE, NODE_PARENT,
        PROCESSING_INSTRUCTION_CONTENT, PROCESSING_INSTRUCTION_TARGET, TEXT_CONTENT,
    },
    typed_value::TypeId,
};
use std::iter::Peekable;

pub(crate) struct TreeIterator<'a, T> {
    tree: &'a Tree<T>,
    node: Option<(State, usize)>,
    //namespaces: Option<TopNamespaceIterator<'a>>,
    is_document: bool,
    done: bool,
    parent_stack: Vec<usize>,
    iter: Peekable<AllNodeIterator<'a, T>>,
}

#[derive(Clone, Copy, PartialEq)]
enum State {
    Start,
    End,
}

impl<'a, T> TreeIterator<'a, T> {
    pub fn new(tree: &'a Tree<T>, pos: usize) -> Self {
        let mut iter = AllNodeIterator::new(tree, pos);
        iter.next();
        let node_kind = tree.nodes[pos];
        let mut ti = Self {
            tree,
            node: Some((State::Start, pos)),
            done: false,
            iter: iter.peekable(),
            is_document: node_kind == DOCUMENT.0,
            parent_stack: Vec::with_capacity(16),
        };
        if ti.document_or_element(pos) {
            //ti.namespaces = Some(root.tree.top_namespaces());
            ti.parent_stack.push(pos);
        }
        ti
    }
    fn nodes(&self, pos: usize) -> usize {
        self.tree.nodes[pos]
    }
    fn string(&self, pos: usize) -> &'a str {
        let string_id = self.nodes(pos);
        self.tree.strings.get(StringId {
            id: string_id as u32,
        })
    }
    fn node_kind(&self, node: usize) -> NodeKind {
        NodeKind::from_usize(self.nodes(node))
    }
    fn document_or_element(&self, node: usize) -> bool {
        let node_kind = self.node_kind(node);
        node_kind == NodeKind::Document || node_kind == NodeKind::Element
    }
    fn node_to_item(&self, state: State, node: usize) -> Item<'a> {
        if state == State::End {
            return if self.parent_stack.is_empty() && self.is_document && self.done {
                Item::EndDocument
            } else {
                Item::EndElement
            };
        }
        let node_kind = self.node_kind(node);
        match node_kind {
            NodeKind::Document => Item::StartDocument,
            NodeKind::Element => Item::StartElement {
                qname: QNameId::from_usize(self.nodes(node + ELEMENT_QNAME)),
                r#type: TypeId::from_usize(self.nodes(node + ELEMENT_TYPE)),
                namespace_definitions: &self.tree.namespace_definitions
                    [self.nodes(node + ELEMENT_NAMESPACES)],
            },
            NodeKind::Text => Item::Text(self.string(node + TEXT_CONTENT)),
            NodeKind::Comment => Item::Comment(self.string(node + COMMENT_CONTENT)),
            NodeKind::Attribute => Item::Attribute {
                qname: QNameId::from_usize(self.nodes(node + ATTRIBUTE_QNAME)),
                r#type: TypeId::from_usize(self.nodes(node + ATTRIBUTE_TYPE)),
                value: self.string(node + ATTRIBUTE_CONTENT),
            },
            NodeKind::Namespace => unimplemented!(),
            NodeKind::ProcessingInstruction => Item::ProcessingInstruction {
                target: QNameId::from_usize(self.nodes(node + PROCESSING_INSTRUCTION_TARGET)),
                content: self.string(node + PROCESSING_INSTRUCTION_CONTENT),
            },
        }
    }
    fn next_node(&mut self) -> Option<(State, usize)> {
        // peek the next node
        if let Some(next) = self.iter.peek().copied() {
            let parent = *self.parent_stack.last().unwrap();
            let parent_of_next = self.nodes(next + NODE_PARENT);
            if parent_of_next < parent {
                // leaving parent
                self.parent_stack.pop();
                Some((State::End, parent))
            } else {
                if self.document_or_element(next) {
                    // this node has to close at some point
                    self.parent_stack.push(next);
                }
                self.iter.next();
                Some((State::Start, next))
            }
        } else if self.parent_stack.pop().is_some() {
            Some((State::End, 0))
        } else {
            self.done = true;
            None
        }
    }
}

impl<'a, T> Iterator for TreeIterator<'a, T> {
    type Item = Item<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        /*
        if let Some(ref mut ns) = self.namespaces {
            if let Some((prefix, namespace)) = ns.next() {
                return Some(Item::NamespaceDefinition { prefix, namespace });
            } else {
                self.namespaces = None;
            }
        }
        */
        if let Some((state, node)) = self.node.take() {
            self.node = self.next_node();
            Some(self.node_to_item(state, node))
        } else {
            None
        }
    }
}
