#![allow(clippy::upper_case_acronyms)]
use super::*;
use crate::{
    builder::{Builder, TypedBuilder},
    qnames::{NamespaceDefinition, QNameId, XML_NAMESPACE, XML_PREFIX},
    string_collection::{StringCollection, StringId},
    typed_value::TypeId,
};

#[non_exhaustive]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Error {
    UndefinedPrefix,
    AttributeAlreadyDefined,
    InvalidComment,
    InvalidProcessingInstruction,
    // empty text as root not is not allowed
    // normally adjacent text nodes are merged and empty text nodes ignored,
    // but there has to be a root node.
    EmptyTextAsRoot,
    InvalidNamespaceDefinition,
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for Error {}

pub struct TreeBuilder<T> {
    nodes: Vec<usize>,
    strings: StringCollection,
    namespace_definitions: Vec<Box<[NamespaceDefinition]>>,
    ns_stack: Vec<usize>,
    typed_values: Vec<T>,
    current_element_tag: usize,
}

impl<T> TreeBuilder<T> {
    fn build(mut self, qnames: QNameCollection) -> Tree<T> {
        assert!(!self.nodes.is_empty());
        let deduplicate_and_sort_values = false;
        let strings = if deduplicate_and_sort_values {
            let (translation, strings) = self.strings.sort_and_deduplicate();
            fn tr(n: &mut usize, translation: &[StringId]) {
                *n = translation[*n].id as usize;
            }
            let mut n = 0;
            while n < self.nodes.len() {
                let node_kind = self.nodes[n];
                match node_kind {
                    3 => tr(&mut self.nodes[n + ATTRIBUTE_CONTENT], &translation),
                    4 => tr(
                        &mut self.nodes[n + PROCESSING_INSTRUCTION_CONTENT],
                        &translation,
                    ),

                    5 => tr(&mut self.nodes[n + COMMENT_CONTENT], &translation),

                    6 => tr(&mut self.nodes[n + TEXT_CONTENT], &translation),

                    _ => {}
                }
                n += NODE_SIZES[node_kind];
            }
            strings
        } else {
            self.strings
        };
        Tree {
            id: TreeId::new(),
            strings,
            qnames,
            nodes: self.nodes.into_boxed_slice(),
            namespace_definitions: self.namespace_definitions,
            typed_values: self.typed_values,
            type_resolver: None,
        }
    }
}

impl<T> Builder<Tree<T>> for TreeBuilder<T> {
    fn init_document(&mut self) {
        self.nodes.reserve(1024);
        assert_eq!(self.nodes.len(), 0);
        self.nodes.push(DOCUMENT.0);
        self.nodes.push(0);
        self.nodes.push(0);
        assert_eq!(self.nodes.len(), DOCUMENT_NODE_SIZE);
    }
    fn init_element(
        &mut self,
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    ) {
        assert_eq!(self.namespace_definitions.len(), 1);
        assert_eq!(self.nodes.len(), 0);
        self.namespace_definitions
            .push(namespace_definitions.into());
        self.nodes.reserve(1024);
        self.nodes.push(ELEMENT.0);
        self.nodes.push(0); // parent
        self.nodes.push(0); // prev
        self.nodes.push(0); // next
        self.nodes.push(0); // first-child
        self.nodes.push(0); // last-child
        self.nodes.push(qname.to_usize());
        self.nodes.push(0); // in-scope namespaces
        self.nodes.push(0); // number of attributes
        self.nodes.push(r#type.to_usize()); // element type
        self.nodes.push(NO_TYPED_VALUE); // element simple content
        assert_eq!(self.nodes.len(), ELEMENT_NODE_SIZE);
    }
    fn init_top_attribute(&mut self, qname: QNameId, r#type: TypeId, content: &str) {
        self.nodes.reserve(ATTRIBUTE_NODE_SIZE);
        let s = self.strings.add_string(content);
        assert_eq!(self.nodes.len(), 0);
        self.nodes.push(ATTRIBUTE.0);
        self.nodes.push(0);
        self.nodes.push(qname.to_usize());
        self.nodes.push(s.into());
        self.nodes.push(r#type.to_usize()); // attribute type
        assert_eq!(self.nodes.len(), ATTRIBUTE_NODE_SIZE);
    }
    fn init_top_text(&mut self, content: &str) {
        self.nodes.reserve(TEXT_NODE_SIZE);
        let s = self.strings.add_string(content);
        assert_eq!(self.nodes.len(), 0);
        self.nodes.push(TEXT.0);
        self.nodes.push(0);
        self.nodes.push(0);
        self.nodes.push(0);
        self.nodes.push(s.into());
        assert_eq!(self.nodes.len(), TEXT_NODE_SIZE);
    }
    fn init_top_comment(&mut self, content: &str) {
        self.nodes.reserve(COMMENT_NODE_SIZE);
        self.strings.push_str(content);
        let s = self.strings.add_string("");
        assert_eq!(self.nodes.len(), 0);
        self.nodes.push(COMMENT.0);
        self.nodes.push(0);
        self.nodes.push(0);
        self.nodes.push(0);
        self.nodes.push(s.into());
        assert_eq!(self.nodes.len(), COMMENT_NODE_SIZE);
    }
    fn init_top_processing_instruction(&mut self, target: QNameId, content: &str) {
        self.nodes.reserve(PROCESSING_INSTRUCTION_NODE_SIZE);
        let content = self.strings.add_string(content);
        self.nodes.push(PROCESSING_INSTRUCTION.0);
        self.nodes.push(0);
        self.nodes.push(0);
        self.nodes.push(0);
        self.nodes.push(target.to_usize());
        self.nodes.push(content.into());
        assert_eq!(self.nodes.len(), PROCESSING_INSTRUCTION_NODE_SIZE);
    }
    /// add text to the content of the current node
    /// if that node is a document node or element node, a text node will
    /// be created
    fn push_str(&mut self, s: &str) {
        self.strings.push_str(s);
    }
    /// add text to the content of the current node
    /// if that node is a document node or element node, a text node will
    /// be created
    fn push(&mut self, c: char) {
        self.strings.push(c);
    }
    fn end_document(&mut self) {
        self.end_text(0);
    }
    fn start_element(
        &mut self,
        parent: usize,
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    ) {
        self.current_element_tag =
            self.start_element_from_qname(parent, qname, r#type, namespace_definitions);
    }
    fn start_processing_instruction(&mut self, node_id: usize, target: QNameId) {
        self.end_text(node_id);
        self.add_node(node_id, PROCESSING_INSTRUCTION);
        let content = self.string("");
        self.nodes.push(target.to_usize());
        self.nodes.push(content.into());
    }
    fn end_processing_instruction(&mut self) {
        self.strings.extend_last_string();
    }
    fn start_comment(&mut self, node_id: usize) {
        self.end_text(node_id);
        self.add_node(node_id, COMMENT);
        let content = self.string("");
        self.nodes.push(content.into());
    }
    fn add_comment(&mut self, node_id: usize, content: &str) {
        self.start_comment(node_id);
        self.push_str(content);
        self.end_comment()
    }
    fn end_comment(&mut self) {
        self.strings.extend_last_string();
    }
    fn end_top_text(&mut self) {}
    fn start_attribute(&mut self, qname: QNameId, r#type: TypeId, value: &str) {
        let parent = self.current_element_tag;
        self.strings.extend_last_string();
        // this is a regular attribute
        let string = self.string(value);
        self.nodes[parent + ELEMENT_NUMBER_OF_ATTRIBUTES] += 1;
        self.nodes.push(ATTRIBUTE.0);
        self.nodes.push(parent);
        self.nodes.push(qname.to_usize());
        self.nodes.push(string.into());
        self.nodes.push(r#type.to_usize()); // attribute type
    }
    // call this method when the element open tag is complete and before adding
    // any children
    fn process_element_open_tag(&mut self) -> usize {
        self.strings.extend_last_string();
        self.current_element_tag
        // check for disallowed attributes
    }
    fn end_element(&mut self, node_id: usize) {
        self.ns_stack.pop();
        self.end_text(node_id);
    }
    fn build(self: Box<Self>, qnames: QNameCollection) -> Tree<T> {
        (*self).build(qnames)
    }
    fn has_errors(&self) -> bool {
        false
    }
    fn errors(&self, _qnames: QNameCollection) -> Vec<String> {
        Vec::new()
    }
}

impl<T> TypedBuilder<Tree<T>, T> for TreeBuilder<T> {
    fn push_typed_element(&mut self, element: usize, typed_value: T, _last: bool) {
        self.end_text(element);
        let value_pos = &mut self.nodes[element + ELEMENT_SIMPLE_CONTENT];
        let end = self.typed_values.len();
        let start = if *value_pos == NO_TYPED_VALUE {
            // this is the first data for this element
            end
        } else {
            usize_to_typed_range(*value_pos).0
        };
        *value_pos = typed_range_to_usize(start, end + 1);
        self.typed_values.push(typed_value);
    }
    fn push_typed_attribute(&mut self, typed_value: T, _last: bool) {
        let len = self.nodes.len();
        let value_pos = &mut self.nodes[len - 2];
        let end = self.typed_values.len();
        let start = if *value_pos == NO_TYPED_VALUE {
            // this is the first data for this element
            end
        } else {
            usize_to_typed_range(*value_pos).0
        };
        *value_pos = typed_range_to_usize(start, end);
        self.typed_values.push(typed_value);
    }
}

impl<T> TreeBuilder<T> {
    pub fn empty() -> Self {
        // every element must have a definition for xml to
        // "http://www.w3.org/XML/1998/namespace"
        let nd = NamespaceDefinition {
            prefix: XML_PREFIX,
            namespace: XML_NAMESPACE,
        };
        Self {
            nodes: Vec::new(),
            strings: StringCollection::with_capacity(16),
            namespace_definitions: vec![Box::new([nd])],
            ns_stack: Vec::new(),
            typed_values: Vec::new(),
            current_element_tag: 0,
        }
    }
    fn start_element_from_qname(
        &mut self,
        parent: usize,
        name: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    ) -> usize {
        self.end_text(parent);
        let nd = self.get_namespace_definitions(namespace_definitions);
        self.ns_stack.push(nd);
        let this = self.add_node(parent, ELEMENT);
        self.nodes.push(0); // first-child
        self.nodes.push(0); // last-child
        self.nodes.push(name.to_usize());
        self.nodes.push(nd); // in-scope namespaces
        self.nodes.push(0); // number of attributes
        self.nodes.push(r#type.to_usize()); // element type
        self.nodes.push(NO_TYPED_VALUE); // element simple content
        this
    }
    // add element and return the parent id
    fn nest_element_from_qname(
        &mut self,
        parent: usize,
        name: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    ) -> ElementAttributeBuilder<T> {
        self.current_element_tag =
            self.start_element_from_qname(parent, name, r#type, namespace_definitions);
        ElementAttributeBuilder { builder: self }
    }
    fn end_text(&mut self, node_id: usize) {
        // text node is only created if there is text
        if !self.strings.current_string().is_empty() {
            self.add_node(node_id, TEXT);
            let content = self.string("");
            self.nodes.push(content.into());
        }
    }
    fn string(&mut self, string: &str) -> StringId {
        self.strings.add_string(string)
    }
    // return position of previous sibling or 0 if there was no previous sibling
    fn add_child(&mut self, parent: usize, child: usize) -> usize {
        if self.nodes.is_empty() {
            return 0;
        }
        let parent_kind = self.nodes[parent];
        if parent_kind == DOCUMENT.0 {
            if self.nodes[parent + DOCUMENT_FIRST] == 0 {
                self.nodes[parent + DOCUMENT_FIRST] = child;
                self.nodes[parent + DOCUMENT_LAST] = child;
                0
            } else {
                let prev = self.nodes[parent + DOCUMENT_LAST];
                self.nodes[prev + NODE_NEXT] = child;
                self.nodes[parent + DOCUMENT_LAST] = child;
                prev
            }
        } else {
            assert_eq!(self.nodes[parent], ELEMENT.0);
            if self.nodes[parent + ELEMENT_FIRST] == 0 {
                self.nodes[parent + ELEMENT_FIRST] = child;
                self.nodes[parent + ELEMENT_LAST] = child;
                0
            } else {
                let prev = self.nodes[parent + ELEMENT_LAST];
                self.nodes[prev + NODE_NEXT] = child;
                self.nodes[parent + ELEMENT_LAST] = child;
                prev
            }
        }
    }
    fn add_node(&mut self, parent: usize, kind: NodeKindInternal) -> usize {
        let this = self.nodes.len();
        let prev = self.add_child(parent, this);
        self.nodes.push(kind.0);
        self.nodes.push(parent); // parent
        self.nodes.push(prev);
        self.nodes.push(0); // next
        this
    }
    fn get_namespace_definitions(
        &mut self,
        namespace_definitions: &[NamespaceDefinition],
    ) -> usize {
        if namespace_definitions.is_empty() {
            self.ns_stack.last().copied().unwrap_or(0)
        } else if let Some(index) = self
            .namespace_definitions
            .iter()
            .position(|nd| &nd[..] == namespace_definitions)
        {
            index
        } else {
            let index = self.namespace_definitions.len();
            self.namespace_definitions
                .push(namespace_definitions.into());
            index
        }
    }
}

pub struct DocumentBuilder<T> {
    builder: TreeBuilder<T>,
}

impl<T> DocumentBuilder<T> {
    pub fn new() -> Self {
        let mut builder = TreeBuilder::empty();
        builder.init_document();
        Self { builder }
    }
    pub fn add_element(
        &mut self,
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    ) -> ElementAttributeBuilder<T> {
        self.builder
            .nest_element_from_qname(0, qname, r#type, namespace_definitions)
    }
    pub fn start_comment(&mut self) {
        self.builder.start_comment(0);
    }
    pub fn add_comment(&mut self, content: &str) {
        self.builder.add_comment(0, content)
    }
    pub fn end_comment(&mut self) {
        self.builder.end_comment()
    }
    pub fn push_str(&mut self, content: &str) {
        self.builder.push_str(content)
    }
    pub fn push(&mut self, content: char) {
        self.builder.push(content)
    }
    pub fn build(mut self, qnames: QNameCollection) -> Tree<T> {
        self.builder.end_document();
        self.builder.build(qnames)
    }
}

pub struct ElementAttributeBuilder<'a, T> {
    builder: &'a mut TreeBuilder<T>,
}

impl<'a, T> ElementAttributeBuilder<'a, T> {
    pub fn add_attribute_from_qname(&mut self, name: QNameId, r#type: TypeId, value: &str) {
        self.builder.start_attribute(name, r#type, value);
    }
    pub fn push(&mut self, content: char) {
        self.builder.strings.push(content);
    }
    pub fn add_children(self) -> ElementChildBuilder<'a, T> {
        let this = self.builder.process_element_open_tag();
        ElementChildBuilder::new(this, self.builder)
    }
}

pub struct ElementChildBuilder<'a, T> {
    this: usize,
    builder: &'a mut TreeBuilder<T>,
}

impl<'a, T> ElementChildBuilder<'a, T> {
    fn new(this: usize, builder: &'a mut TreeBuilder<T>) -> Self {
        Self { this, builder }
    }
    pub fn push_str(&mut self, content: &str) {
        self.builder.push_str(content)
    }
    pub fn add_comment(&mut self, content: &str) {
        self.builder.start_comment(self.this);
        self.builder.strings.push_str(content);
        self.builder.end_comment();
    }
    pub fn add_element(
        &mut self,
        qname: QNameId,
        r#type: TypeId,
        namespace_definitions: &[NamespaceDefinition],
    ) -> ElementAttributeBuilder<T> {
        self.builder
            .nest_element_from_qname(self.this, qname, r#type, namespace_definitions)
    }
}

impl<'a, T> Drop for ElementChildBuilder<'a, T> {
    fn drop(&mut self) {
        self.builder.end_element(self.this)
    }
}

#[test]
fn test_empty_document() {
    let qnames = crate::qnames::QNameCollector::default();
    let mut tree = TreeBuilder::<()>::empty();
    tree.init_document();
    let tree = tree.build(qnames.collect());
    let root_node = crate::node::Node::root(&tree);
    assert_eq!(root_node.node_kind(), NodeKind::Document);
    assert_eq!(root_node.children().count(), 0);
    assert_eq!(root_node.parent(), None);
    assert_eq!(root_node.first_child(), None);
    assert_eq!(root_node.last_child(), None);
    eprintln!("{:?}", tree.nodes);
}

#[cfg(test)]
const UNTYPED: TypeId = crate::typed_value::XS_UNTYPED;

#[test]
fn test_document() {
    let qnames = crate::qnames::QNameCollector::default();
    let name = qnames.add_local_qname(NCName("a"));
    let mut builder = DocumentBuilder::<()>::new();
    builder.add_element(name, UNTYPED, &[]);
    let tree = builder.build(qnames.collect());
    let root_node = crate::node::Node::root(&tree);
    assert_eq!(root_node.node_kind(), NodeKind::Document);
    assert_eq!(root_node.children().count(), 1);
    assert_eq!(root_node.parent(), None);
    let first_child = root_node.first_child().unwrap();
    let last_child = root_node.last_child().unwrap();
    assert_eq!(first_child, last_child);
    assert_eq!(first_child.parent(), Some(root_node));
    for _a in first_child.attributes() {}
    eprintln!("{:?}", tree.nodes);
    eprintln!("{:?}", tree.strings);
    eprintln!("{:?}", tree.qnames);
    eprintln!(
        "{:?}",
        first_child.node_name().unwrap().to_ref().local_name()
    );
}

#[test]
fn test_next() {
    let qnames = crate::qnames::QNameCollector::default();
    let aname = qnames.add_local_qname(NCName("a"));
    let bname = qnames.add_local_qname(NCName("b"));
    let mut builder = DocumentBuilder::<()>::new();
    let eb = builder.add_element(aname, UNTYPED, &[]);
    eb.add_children().add_element(bname, UNTYPED, &[]);
    let tree = builder.build(qnames.collect());
    let root_node = crate::node::Node::root(&tree);
    assert_eq!(root_node.node_kind(), NodeKind::Document);
    assert_eq!(root_node.children().count(), 1);
    assert_eq!(root_node.parent(), None);
    let a = root_node.first_child().unwrap();
    let b = a.first_child().unwrap();
    eprintln!("{:?}", tree.nodes);
    eprintln!("{:?}", tree.strings);
    eprintln!("{:?}", tree.qnames);
    eprintln!("{:?}", a.node_name().unwrap().to_ref().local_name());
    eprintln!("{:?}", b.node_name().unwrap().to_ref().local_name());
}
