use crate::{
    qnames::{
        self, NCName, Namespace, NamespaceDefinition, QName, QNameCollection, QNameId, QNameRef,
    },
    stream::Item,
    tree::TreeId,
    typed_value::TypeId,
};
use std::borrow::Cow;
use std::fmt::{self, Debug};
use std::iter::Peekable;
use std::ops::Deref;

pub struct NodeIteratorInit {
    pub start: usize,
    pub end: usize,
}

impl NodeIteratorInit {
    pub fn new(start: usize, end: usize) -> Self {
        Self { start, end }
    }
    pub fn empty() -> Self {
        Self { start: 0, end: 0 }
    }
}

/**
 * A DOM Tree for XML documents.
 *
 * This trait is modelled after the [XQuery and XPath Data Model 3.0](https://www.w3.org/TR/xpath-datamodel-30/).
 */
pub trait Tree {
    type TypedValue;
    fn id(&self) -> TreeId;
    fn kind(&self, pos: usize) -> NodeKind;
    fn qname(&self, pos: usize) -> QNameId;
    fn node_name(&self, pos: usize) -> Option<QNameRef>;
    fn string_value(&self, pos: usize) -> Cow<str>;
    fn type_id(&self, pos: usize) -> Option<TypeId>;
    fn type_name(&self, pos: usize) -> Option<QNameRef>;
    fn parent(&self, pos: usize) -> Option<usize>;
    fn previous_sibling(&self, pos: usize) -> Option<usize>;
    fn next_sibling(&self, pos: usize) -> Option<usize>;
    fn first_child(&self, pos: usize) -> Option<usize>;
    fn last_child(&self, pos: usize) -> Option<usize>;

    fn descendant_init(&self, pos: usize) -> NodeIteratorInit;
    fn descendant_iter(&self, pos: usize) -> usize;
    fn descendant_or_self_init(&self, pos: usize) -> NodeIteratorInit;
    fn descendant_or_self_iter(&self, pos: usize) -> usize;
    fn following_init(&self, pos: usize) -> NodeIteratorInit;
    fn following_iter(&self, pos: usize) -> usize;
    fn following_siblings_init(&self, pos: usize) -> NodeIteratorInit;
    fn following_siblings_iter(&self, pos: usize) -> usize;
    fn preceding_siblings_init(&self, pos: usize) -> NodeIteratorInit;
    fn preceding_siblings_iter(&self, pos: usize) -> usize;
    fn self_init(&self, pos: usize) -> NodeIteratorInit;
    fn self_iter(&self, pos: usize) -> usize;
    fn parent_init(&self, pos: usize) -> NodeIteratorInit;
    fn parent_iter(&self, pos: usize) -> usize;
    fn preceding_init(&self, pos: usize) -> NodeIteratorInit;
    fn preceding_iter(&self, pos: usize) -> usize;
    fn ancestors_init(&self, pos: usize) -> NodeIteratorInit;
    fn ancestors_iter(&self, pos: usize) -> usize;
    fn ancestors_or_self_init(&self, pos: usize) -> NodeIteratorInit;
    fn ancestors_or_self_iter(&self, pos: usize) -> usize;
    fn children_init(&self, pos: usize) -> NodeIteratorInit;
    fn children_iter(&self, pos: usize) -> usize;
    fn attributes_init(&self, pos: usize) -> NodeIteratorInit;
    fn attributes_iter(&self, pos: usize) -> usize;

    fn item_iter<'a>(&'a self, pos: usize) -> Box<dyn Iterator<Item = Item> + 'a>;
    fn typed_value_init(&self, pos: usize) -> TypedValueIteratorInit;
    fn get_typed_value(&self, pos: usize) -> &Self::TypedValue;

    fn qnames(&self) -> &QNameCollection;
    fn attribute(&self, pos: usize, qname: QNameId) -> Option<&str>;
    fn namespaces(&self, pos: usize) -> &[NamespaceDefinition];
    fn resolve_qname(&self, pos: usize, qname: &str) -> Option<QName>;
}

pub trait Ref: Deref<Target = Self::Tree> + Clone + Debug {
    type Tree: Tree + ?Sized;
}
impl<T, R> Ref for R
where
    T: Tree + ?Sized,
    R: Deref<Target = T> + Clone + Debug,
{
    type Tree = T;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum NodeKind {
    Document = 0,
    Element = 1,
    Attribute = 2,
    Namespace = 3,
    ProcessingInstruction = 4,
    Comment = 5,
    Text = 6,
}

const NODE_KIND_DOCUMENT: &str = "document";
const NODE_KIND_ELEMENT: &str = "element";
const NODE_KIND_ATTRIBUTE: &str = "attribute";
const NODE_KIND_NAMESPACE: &str = "namespace";
const NODE_KIND_PROCESSING_INSTRUCTION: &str = "processing-instruction";
const NODE_KIND_COMMENT: &str = "comment";
const NODE_KIND_TEXT: &str = "text";

const NODE_KINDS: [NodeKind; 7] = [
    NodeKind::Document,
    NodeKind::Element,
    NodeKind::Attribute,
    NodeKind::Namespace,
    NodeKind::ProcessingInstruction,
    NodeKind::Comment,
    NodeKind::Text,
];

const NODE_KIND_STRINGS: [&str; 7] = [
    NODE_KIND_DOCUMENT,
    NODE_KIND_ELEMENT,
    NODE_KIND_ATTRIBUTE,
    NODE_KIND_NAMESPACE,
    NODE_KIND_PROCESSING_INSTRUCTION,
    NODE_KIND_COMMENT,
    NODE_KIND_TEXT,
];

impl NodeKind {
    pub(crate) fn node_kind(v: NodeKind) -> &'static str {
        match v {
            NodeKind::Document => NODE_KIND_STRINGS[v as usize],
            NodeKind::Element => NODE_KIND_STRINGS[v as usize],
            NodeKind::Attribute => NODE_KIND_STRINGS[v as usize],
            NodeKind::Namespace => NODE_KIND_STRINGS[v as usize],
            NodeKind::ProcessingInstruction => NODE_KIND_STRINGS[v as usize],
            NodeKind::Comment => NODE_KIND_STRINGS[v as usize],
            NodeKind::Text => NODE_KIND_STRINGS[v as usize],
        }
    }
    pub(crate) fn from_usize(v: usize) -> Self {
        NODE_KINDS[v]
    }
}

impl From<NodeKind> for &'static str {
    fn from(v: NodeKind) -> Self {
        NodeKind::node_kind(v)
    }
}

/**
 * A node in an XDM.
 *
 * This struct is an implementation of an [XDM
 * node](https://www.w3.org/TR/xpath-datamodel-30/#dt-node).
 */
pub struct Node<T: Ref> {
    tree: T,
    pos: usize,
}

impl<T: Ref> Clone for Node<T> {
    fn clone(&self) -> Self {
        Self {
            tree: self.tree.clone(),
            pos: self.pos,
        }
    }
}

impl<T: Ref> PartialEq for Node<T> {
    fn eq(&self, o: &Self) -> bool {
        let a = self.tree.id();
        let b = self.tree.id();
        self.pos == o.pos && a == b
    }
}

impl<T: Ref> Node<T> {
    pub fn tree(&self) -> &T {
        &self.tree
    }
    /**
     * Create a root node from a tree.
     */
    pub fn root(tree: T) -> Self {
        Node { tree, pos: 0 }
    }
    /**
     * Get the root node of the tree
     * of this node.
     */
    pub fn root_node(&self) -> Self {
        Node {
            tree: self.tree.clone(),
            pos: 0,
        }
    }

    // helper functions
    fn node(&self, pos: usize) -> Self {
        Self {
            tree: self.tree.clone(),
            pos,
        }
    }
    pub fn previous_sibling(&self) -> Option<Node<T>> {
        self.tree
            .previous_sibling(self.pos)
            .map(|pos| self.node(pos))
    }
    pub fn next_sibling(&self) -> Option<Node<T>> {
        self.tree.next_sibling(self.pos).map(|pos| self.node(pos))
    }
    pub fn first_child(&self) -> Option<Node<T>> {
        self.tree.first_child(self.pos).map(|pos| self.node(pos))
    }
    pub fn last_child(&self) -> Option<Node<T>> {
        self.tree.last_child(self.pos).map(|pos| self.node(pos))
    }
    fn iter(
        &self,
        f: fn(&T::Tree, usize) -> NodeIteratorInit,
        i: fn(&T::Tree, usize) -> usize,
    ) -> NodeIterator<T> {
        let iter = f(&self.tree, self.pos);
        NodeIterator::new(self.tree.clone(), iter, i)
    }
    pub fn descendant(&self) -> NodeIterator<T> {
        self.iter(T::Tree::descendant_init, T::Tree::descendant_iter)
    }
    pub fn descendant_or_self(&self) -> NodeIterator<T> {
        self.iter(
            T::Tree::descendant_or_self_init,
            T::Tree::descendant_or_self_iter,
        )
    }
    // start from a node that is not a descendant of this node
    pub fn following(&self) -> NodeIterator<T> {
        self.iter(T::Tree::following_init, T::Tree::following_iter)
    }
    pub fn following_siblings(&self) -> NodeIterator<T> {
        self.iter(
            T::Tree::following_siblings_init,
            T::Tree::following_siblings_iter,
        )
    }
    pub fn preceding_siblings(&self) -> NodeIterator<T> {
        self.iter(
            T::Tree::preceding_siblings_init,
            T::Tree::preceding_siblings_iter,
        )
    }
    pub fn self_iter(&self) -> NodeIterator<T> {
        self.iter(T::Tree::self_init, T::Tree::self_iter)
    }
    pub fn parent_iter(&self) -> NodeIterator<T> {
        self.iter(T::Tree::parent_init, T::Tree::parent_iter)
    }
    pub fn preceding(&self) -> NodeIterator<T> {
        self.iter(T::Tree::preceding_init, T::Tree::preceding_iter)
    }
    pub fn ancestors(&self) -> NodeIterator<T> {
        self.iter(T::Tree::ancestors_init, T::Tree::ancestors_iter)
    }
    pub fn ancestors_or_self(&self) -> NodeIterator<T> {
        self.iter(
            T::Tree::ancestors_or_self_init,
            T::Tree::ancestors_or_self_iter,
        )
    }
    pub fn element(&self) -> Option<Element<T>> {
        if self.node_kind() == NodeKind::Element {
            Some(Element((*self).clone()))
        } else {
            None
        }
    }
    pub fn element_ref(&self) -> Option<ElementRef<T::Tree>> {
        if self.node_kind() == NodeKind::Element {
            Some(ElementRef {
                tree: &self.tree,
                pos: self.pos,
            })
        } else {
            None
        }
    }
    // XPath 'is' operator
    pub fn is(&self, o: &Node<T>) -> bool {
        self.pos == o.pos && self.tree.id() == o.tree.id()
    }
    // XPath '>>' operator
    pub fn follows(&self, o: &Node<T>) -> bool {
        let a = self.tree.id();
        let b = o.tree.id();
        if a == b {
            self.pos > o.pos
        } else {
            a > b
        }
    }
    // XPath '<<' operator
    pub fn precedes(&self, o: &Node<T>) -> bool {
        let a = self.tree.id();
        let b = o.tree.id();
        if a == b {
            self.pos < o.pos
        } else {
            a < b
        }
    }
    pub fn qnames(&self) -> &QNameCollection {
        self.tree.qnames()
    }
    pub fn find_qname(&self, namespace: Namespace, local_name: NCName) -> Option<QNameId> {
        self.tree.qnames().find_qname(namespace, local_name)
    }
    pub fn attribute(&self, qname: QNameId) -> Option<&str> {
        self.tree.attribute(self.pos, qname)
    }
    pub fn namespaces(&self) -> NamespaceIterator {
        NamespaceIterator {
            qnames: self.tree.qnames(),
            iter: self.tree.namespaces(self.pos).iter(),
        }
    }
    pub fn resolve_qname(&self, qname: &str) -> Option<QName> {
        self.tree.resolve_qname(self.pos, qname)
    }
    pub fn item_iter(&self) -> ItemIterator {
        ItemIterator {
            iter: self.tree.item_iter(self.pos),
            qnames: self.tree.qnames(),
        }
    }

    // xdm
    pub fn attributes(&self) -> NodeIterator<T> {
        self.iter(T::Tree::attributes_init, T::Tree::attributes_iter)
    }
    pub fn base_uri(&self) -> Option<&str> {
        None
    }
    pub fn children(&self) -> NodeIterator<T> {
        self.iter(T::Tree::children_init, T::Tree::children_iter)
    }
    pub fn document_uri(&self) -> Option<&str> {
        None
    }
    pub fn nilled(&self) -> bool {
        false
    }
    pub fn node_kind(&self) -> NodeKind {
        self.tree.kind(self.pos)
    }
    /*
    pub fn node_prefix(&self) -> Option<Prefix> {
        self.tree.node_prefix(self.pos)
    }
    */
    pub fn node_name(&self) -> Option<QNameRef> {
        self.tree.node_name(self.pos)
    }
    pub fn parent(&self) -> Option<Node<T>> {
        self.tree.parent(self.pos).map(|pos| self.node(pos))
    }
    pub fn string_value(&self) -> Cow<str> {
        self.tree.string_value(self.pos)
    }
    pub fn type_id(&self) -> Option<TypeId> {
        self.tree.type_id(self.pos)
    }
    pub fn type_name(&self) -> Option<QNameRef> {
        self.tree.type_name(self.pos)
    }
    pub fn typed_value(&self) -> TypedValueIterator<T> {
        let init = self.tree.typed_value_init(self.pos);
        #[cfg(debug_assertions)]
        if let TypedValueIteratorInit::List { start, end } = &init {
            assert!(
                start < end,
                "start ({}) should be smaller than end ({}) in TypedValueIteratorInit::List.",
                start,
                end
            );
        }
        TypedValueIterator::new(init, &self.tree)
    }
}
#[derive(Debug)]
pub enum TypedValueIteratorInit<'a> {
    Empty,
    String(Cow<'a, str>, TypeId),
    List { start: usize, end: usize },
}
pub enum IterTypedValue<'a, TV> {
    String(TypeId, Cow<'a, str>),
    TypedValue(&'a TV),
}
pub struct TypedValueIterator<'a, T: Ref> {
    state: TypedValueIteratorInit<'a>,
    tree: &'a T,
}

impl<'a, T: Ref> TypedValueIterator<'a, T> {
    fn new(state: TypedValueIteratorInit<'a>, tree: &'a T) -> Self {
        Self { state, tree }
    }
}

impl<'a, T: Ref> Iterator for TypedValueIterator<'a, T> {
    type Item = IterTypedValue<'a, <T::Target as Tree>::TypedValue>;
    fn next(&mut self) -> Option<Self::Item> {
        match &mut self.state {
            TypedValueIteratorInit::Empty => None,
            TypedValueIteratorInit::String(s, t) => {
                let mut o = Cow::Borrowed("");
                std::mem::swap(&mut o, s);
                let v = IterTypedValue::String(*t, o);
                self.state = TypedValueIteratorInit::Empty;
                Some(v)
            }
            TypedValueIteratorInit::List { start, end } => {
                let v = self.tree.get_typed_value(*start);
                *start += 1;
                if start == end {
                    self.state = TypedValueIteratorInit::Empty;
                }
                Some(IterTypedValue::TypedValue(v))
            }
        }
    }
}

impl<T: Ref> Debug for Node<T> {
    fn fmt(&self, _f: &mut fmt::Formatter) -> fmt::Result {
        Ok(())
    }
}

// more convenient interface
#[derive(Debug, Clone)]
pub struct Element<T: Ref>(Node<T>);

impl<T: Ref> Element<T> {
    pub fn node(&self) -> &Node<T> {
        &self.0
    }
    pub fn previous_sibling(&self) -> Option<Node<T>> {
        self.0.previous_sibling()
    }
    pub fn next_sibling(&self) -> Option<Node<T>> {
        self.0.next_sibling()
    }
    pub fn first_child(&self) -> Option<Node<T>> {
        self.0.first_child()
    }
    pub fn last_child(&self) -> Option<Node<T>> {
        self.0.last_child()
    }
    pub fn descendant(&self) -> NodeIterator<T> {
        self.0.descendant()
    }
    pub fn has_name(&self, name: QNameId) -> bool {
        self.0.tree.qname(self.0.pos) == name
    }

    // xdm
    pub fn attributes(&self) -> impl Iterator<Item = Attribute<T>> {
        self.0.attributes().map(Attribute)
    }
    pub fn base_uri(&self) -> Option<&str> {
        self.0.base_uri()
    }
    pub fn children(&self) -> NodeIterator<T> {
        self.0.children()
    }
    pub fn document_uri(&self) -> Option<&str> {
        self.0.document_uri()
    }
    pub fn node_kind(&self) -> &str {
        NodeKind::node_kind(self.0.node_kind())
    }
    pub fn node_name(&self) -> QNameRef {
        self.0.node_name().unwrap()
    }
    pub fn qnames(&self) -> &QNameCollection {
        self.0.tree.qnames()
    }
    /*
    pub fn prefix(&self) -> Prefix {
        self.0.tree.qnames().to_ref(self.0.tree.qname(self.0.pos))
    }
    pub fn namespace(&self) -> Namespace {
        self.0
            .tree
            .qnames()
            .namespace(self.0.tree.qname(self.0.pos))
    }
    pub fn local_name(&self) -> NCName {
        self.0
            .tree
            .qnames()
            .local_name(self.0.tree.qname(self.0.pos))
    }*/
    pub fn parent(&self) -> Option<Node<T>> {
        self.0.parent()
    }
    pub fn string_value(&self) -> Cow<str> {
        self.0.string_value()
    }
    pub fn namespaces(&self) -> NamespaceIterator {
        self.0.namespaces()
    }
    pub fn new_namespaces(&self) -> NewNamespaceIterator {
        let parent = if let Some(parent) = self.0.tree.parent(self.0.pos) {
            self.0.tree.namespaces(parent)
        } else {
            &[]
        };
        NewNamespaceIterator {
            qnames: self.0.tree.qnames(),
            iter: self.0.tree.namespaces(self.0.pos).iter(),
            parent: parent.iter().peekable(),
        }
    }
}

#[derive(Clone)]
pub struct Attribute<T: Ref>(Node<T>);

impl<T: Ref> Attribute<T> {
    pub fn node(&self) -> &Node<T> {
        &self.0
    }
    pub fn previous_sibling(&self) -> Option<Node<T>> {
        None
    }
    pub fn next_sibling(&self) -> Option<Node<T>> {
        None
    }
    pub fn first_child(&self) -> Option<Node<T>> {
        None
    }
    pub fn last_child(&self) -> Option<Node<T>> {
        None
    }
    pub fn descendant(&self) -> NodeIterator<T> {
        NodeIterator::empty(self.0.tree.clone())
    }

    // xdm
    pub fn attributes(&self) -> NodeIterator<T> {
        self.0.attributes()
    }
    pub fn base_uri(&self) -> Option<&str> {
        self.0.base_uri()
    }
    pub fn children(&self) -> NodeIterator<T> {
        NodeIterator::empty(self.0.tree.clone())
    }
    pub fn document_uri(&self) -> Option<&str> {
        self.0.document_uri()
    }
    pub fn node_kind(&self) -> &str {
        NodeKind::node_kind(self.0.node_kind())
    }
    pub fn node_name(&self) -> QNameRef {
        self.0.node_name().unwrap()
    }
    /*
    pub fn prefix(&self) -> Prefix {
        self.0.tree.qnames().prefix(self.0.tree.qname(self.0.pos))
    }
    pub fn namespace(&self) -> Namespace {
        self.0
            .tree
            .qnames()
            .namespace(self.0.tree.qname(self.0.pos))
    }
    pub fn local_name(&self) -> NCName {
        self.0
            .tree
            .qnames()
            .local_name(self.0.tree.qname(self.0.pos))
    }
    */
    pub fn parent(&self) -> Option<Node<T>> {
        self.0.parent()
    }
    pub fn string_value(&self) -> Cow<str> {
        self.0.string_value()
    }
}

// hopefully temporary struct lifetime inherited by attributes
pub struct ElementRef<'a, T: Tree + ?Sized> {
    tree: &'a T,
    pos: usize,
}

impl<'a, T: Tree + ?Sized> ElementRef<'a, T> {
    pub fn node_name(&self) -> QNameRef {
        self.tree.node_name(self.pos).unwrap()
    }
    /*
    pub fn namespace(&self) -> Namespace {
        self.tree.qnames().namespace(self.tree.qname(self.pos))
    }
    pub fn prefix(&self) -> Prefix {
        self.tree.qnames().prefix(self.tree.qname(self.pos))
    }
    pub fn local_name(&self) -> NCName {
        self.tree.qnames().local_name(self.tree.qname(self.pos))
    }
    */
    pub fn attributes(&self) -> AttributeRefIterator<'a, T> {
        AttributeRefIterator::new(self.tree, self.pos)
    }
    pub fn namespaces(&self) -> NamespaceIterator {
        NamespaceIterator {
            qnames: self.tree.qnames(),
            iter: self.tree.namespaces(self.pos).iter(),
        }
    }
    pub fn new_namespaces(&self) -> NewNamespaceIterator {
        let parent = if let Some(parent) = self.tree.parent(self.pos) {
            if self.tree.kind(parent) == NodeKind::Element {
                self.tree.namespaces(parent)
            } else {
                &[]
            }
        } else {
            &[]
        };
        NewNamespaceIterator {
            qnames: self.tree.qnames(),
            iter: self.tree.namespaces(self.pos).iter(),
            parent: parent.iter().peekable(),
        }
    }
}

pub struct AttributeRef<'a, T: Tree + ?Sized> {
    tree: &'a T,
    pos: usize,
}

impl<'a, T: Tree + ?Sized> AttributeRef<'a, T> {
    pub fn node_name(&self) -> QNameRef {
        self.tree.node_name(self.pos).unwrap()
    }
    /*
    pub fn namespace(&self) -> Namespace<'a> {
        self.tree.qnames().namespace(self.tree.qname(self.pos))
    }
    pub fn prefix(&self) -> Prefix<'a> {
        self.tree.qnames().prefix(self.tree.qname(self.pos))
    }
    pub fn local_name(&self) -> NCName<'a> {
        self.tree.qnames().local_name(self.tree.qname(self.pos))
    }*/
    pub fn string_value(&self) -> &'a str {
        match self.tree.string_value(self.pos) {
            Cow::Borrowed(s) => s,
            _ => panic!(),
        }
    }
}

pub struct AttributeRefIterator<'a, T: Tree + ?Sized> {
    tree: &'a T,
    current: usize,
    end: usize,
}

impl<'a, T: Tree + ?Sized> AttributeRefIterator<'a, T> {
    fn new(tree: &'a T, pos: usize) -> Self {
        let iter = tree.attributes_init(pos);
        Self {
            tree,
            current: iter.start,
            end: iter.end,
        }
    }
}

impl<'a, T: Tree + ?Sized> Iterator for AttributeRefIterator<'a, T> {
    type Item = AttributeRef<'a, T>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.current == self.end {
            None
        } else {
            let n = AttributeRef {
                tree: self.tree,
                pos: self.current,
            };
            self.current = self.tree.attributes_iter(self.current);
            Some(n)
        }
    }
}

pub struct NodeIterator<T: Ref> {
    tree: T,
    step: fn(&T::Tree, usize) -> usize,
    current: usize,
    end: usize,
}

impl<T: Ref> NodeIterator<T> {
    fn empty(tree: T) -> Self {
        Self {
            tree,
            step: T::Tree::descendant_iter,
            current: 0,
            end: 0,
        }
    }
    fn new(tree: T, init: NodeIteratorInit, step: fn(&T::Tree, usize) -> usize) -> Self {
        Self {
            tree,
            step,
            current: init.start,
            end: init.end,
        }
    }
}

impl<T: Ref> Iterator for NodeIterator<T> {
    type Item = Node<T>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.current == self.end {
            None
        } else {
            let next = (self.step)(&self.tree, self.current);
            let n = Node {
                tree: self.tree.clone(),
                pos: self.current,
            };
            self.current = next;
            Some(n)
        }
    }
}

// iterator for all namespaces for an element
pub struct NamespaceIterator<'a> {
    qnames: &'a QNameCollection,
    iter: std::slice::Iter<'a, NamespaceDefinition>,
}

impl<'a> Iterator for NamespaceIterator<'a> {
    type Item = qnames::Ref;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(nd) = self.iter.next() {
            Some(self.qnames.namespace_ref(*nd))
        } else {
            None
        }
    }
}

// iterator over the new namespaces for an element
// that is: all namespace definitions that are not in the parent element
pub struct NewNamespaceIterator<'a> {
    qnames: &'a QNameCollection,
    parent: Peekable<std::slice::Iter<'a, NamespaceDefinition>>,
    iter: std::slice::Iter<'a, NamespaceDefinition>,
}

impl<'a> Iterator for NewNamespaceIterator<'a> {
    type Item = qnames::Ref;
    fn next(&mut self) -> Option<Self::Item> {
        'outer: for nd in &mut self.iter {
            while let Some(parent_nd) = self.parent.peek() {
                let parent_nd = *parent_nd;
                if parent_nd > nd {
                    // nd is not present in the parent
                    break;
                }
                self.parent.next();
                if parent_nd == nd {
                    // skip nd
                    continue 'outer;
                }
            }
            // parent namespaces are exhausted, what remains is new
            return Some(self.qnames.namespace_ref(*nd));
        }
        None
    }
}

pub struct ItemIterator<'a> {
    iter: Box<dyn Iterator<Item = Item<'a>> + 'a>,
    qnames: &'a QNameCollection,
}

impl<'a> ItemIterator<'a> {
    pub fn qnames(&self) -> &QNameCollection {
        self.qnames
    }
}

impl<'a> Iterator for ItemIterator<'a> {
    type Item = Item<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
