use std::collections::HashMap;
use xust_eval::{
    eval::context::Context,
    r#fn::{FunctionDefinition, FunctionDefinitions},
    xdm::Sequence,
    Ref,
};
use xust_tree::qnames::Namespace;
use xust_xsd::xpath_error::Result;

pub const VALIDATE: Namespace = Namespace("http://basex.org/modules/validate");

fn fd<'a, T: Ref>() -> &'a [FunctionDefinition<T>] {
    &[
        (VALIDATE, "xsd", 1, 3, xsd),
        (VALIDATE, "xsd-info", 1, 3, xsd_info),
    ]
}

pub fn add<T: Ref>(_ns: &mut HashMap<String, String>, f: &mut FunctionDefinitions<T>) {
    f.add(fd());
}

/// validate:xsd($input as item()) as empty-sequence()
/// validate:xsd($input as item(), $schema as item()?) as empty-sequence()
/// validate:xsd($input as item(), $schema as item()?, $features as map(*)) as empty-sequence()
fn xsd<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

/// validate:xsd-info($input as item()) as xs:string*
/// validate:xsd-info($input as item(), $schema as item()?) as xs:string*
/// validate:xsd-info($input as item(), $schema as item()?, $features as map(*)) as xs:string*
fn xsd_info<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}
