use std::collections::HashMap;
use xust_eval::{
    eval::context::Context,
    r#fn::{FunctionDefinition, FunctionDefinitions},
    xdm::Sequence,
    Ref,
};
use xust_tree::qnames::Namespace;
use xust_xsd::xpath_error::Result;

pub const HTML: Namespace = Namespace("http://basex.org/modules/html");

fn fd<'a, T: Ref>() -> &'a [FunctionDefinition<T>] {
    &[(HTML, "parse", 1, 2, parse)]
}

pub fn add<T: Ref>(_ns: &mut HashMap<String, String>, f: &mut FunctionDefinitions<T>) {
    f.add(fd());
}

/// html:parse($input as xs:anyAtomicType) as document-node()
/// html:parse($input as xs:anyAtomicType, $options as map(*)?) as document-node()
fn parse<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}
