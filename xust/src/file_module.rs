use std::collections::HashMap;
use xust_eval::{
    eval::context::Context,
    r#fn::{FunctionDefinition, FunctionDefinitions},
    xdm::Sequence,
    Ref,
};
use xust_tree::qnames::Namespace;
use xust_xsd::xpath_error::Result;

pub const FILE: Namespace = Namespace("http://expath.org/ns/file");

fn fd<'a, T: Ref>() -> &'a [FunctionDefinition<T>] {
    &[
        (FILE, "base-dir", 0, 0, base_dir),
        (FILE, "copy", 2, 2, copy),
        (FILE, "create-dir", 1, 1, create_dir),
        (FILE, "delete", 1, 2, delete),
        (FILE, "exists", 1, 1, exists),
        (FILE, "is-dir", 1, 1, is_dir),
        (FILE, "is-file", 1, 1, is_file),
        (FILE, "list", 1, 3, list),
        (FILE, "parent", 1, 1, parent),
        (FILE, "read-text-lines", 1, 5, read_text_lines),
        (FILE, "resolve-path", 1, 2, resolve_path),
        (FILE, "size", 1, 1, size),
        (FILE, "write", 2, 3, write),
    ]
}

pub fn add<T: Ref>(_ns: &mut HashMap<String, String>, f: &mut FunctionDefinitions<T>) {
    f.add(fd());
}

/// file:base-dir() as xs:string?
fn base_dir<T: Ref>(_args: &[Sequence<T>], context: &Context<T>) -> Result<Sequence<T>> {
    if let Some(path) = context.base_uri().and_then(|u| u.to_file_path().ok()) {
        if let Some(dir) = path.parent() {
            return Ok(Sequence::string(format!("{}", dir.display())));
        }
    }
    Ok(Sequence::empty())
}

/// file:copy($source as xs:string, $target as xs:string) as empty-sequence()
fn copy<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

/// file:delete($path as xs:string) as empty-sequence()
/// file:delete($path as xs:string, $recursive as xs:boolean) as empty-sequence()
fn delete<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

/// file:exists($path as xs:string) as xs:boolean
fn exists<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

fn list<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

/// file:create-dir($dir as xs:string) as empty-sequence()
fn create_dir<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

/// file:parent($path as xs:string) as xs:string?
fn parent<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

fn is_dir<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

fn is_file<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

// file:resolve-path($path as xs:string) as xs:string
// file:resolve-path($path as xs:string, $base as xs:string) as xs:string
fn resolve_path<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

// file:write($path as xs:string, $items as item()*) as empty-sequence()
// file:write($path as xs:string, $items as item()*, $params as item()) as empty-sequence()
fn write<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

/// file:size($path as xs:string) as xs:integer
fn size<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}

/// file:read-text-lines($path as xs:string) as xs:string*
/// file:read-text-lines($path as xs:string, $encoding as xs:string) as xs:string*
/// file:read-text-lines($path as xs:string, $encoding as xs:string, $fallback as xs:boolean) as xs:string*
/// file:read-text-lines($path as xs:string, $encoding as xs:string, $fallback as xs:boolean, $offset as xs:integer) as xs:string*
/// file:read-text-lines($path as xs:string, $encoding as xs:string, $fallback as xs:boolean, $offset as xs:integer, $length as xs:integer) as xs:string*
fn read_text_lines<T: Ref>(_args: &[Sequence<T>], _context: &Context<T>) -> Result<Sequence<T>> {
    todo!()
}
