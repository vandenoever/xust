mod file_module;
mod html_module;
mod validate_module;

use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::Arc;
use xust_eval::{
    eval::{
        context::{default_tree_context_init, Context, GlobalContext},
        eval_xquery,
    },
    r#fn::function_definitions,
    xdm::Item,
};
use xust_grammar::{parse, ParseInit};
use xust_tree::qnames::Namespace;
use xust_xml::write::node_to_string;

struct Arguments {
    help: bool,
    inline_query: Option<String>,
    query_file: Option<PathBuf>,
    output_file: Option<PathBuf>,
}

fn parse_arguments() -> Result<Arguments, Box<dyn std::error::Error>> {
    let mut args = pico_args::Arguments::from_env();
    let help = args.contains(["-h", "--help"]);
    let inline_query = args.opt_value_from_str("--qs")?;
    let query_file = args.opt_value_from_str(["-q", "--query"])?;
    let output_file = args.opt_value_from_str(["-o", "--output-file"])?;
    if !args.finish().is_empty() {
        return Err("Too many files provided.".to_string().into());
    }
    Ok(Arguments {
        help,
        inline_query,
        query_file,
        output_file,
    })
}

fn print_help() {
    println!(
        "Usage: {} [OPTION]... CATALOG",
        std::env::args().next().unwrap()
    );
    println!("  -h, --help");
    println!("      --qs QUERY");
    println!("      -q|--query FILE");
    println!("      -o|--output-file FILE");
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = parse_arguments().map_err(|e| {
        print_help();
        e
    })?;
    if args.help {
        print_help();
        return Ok(());
    }
    let mut base_uri: Option<url::Url> = std::env::current_dir()
        .map_err(|_| ())
        .and_then(url::Url::from_directory_path)
        .ok();
    let (query, module_base) = if let Some(inline_query) = args.inline_query {
        (inline_query, None)
    } else if let Some(query_file) = args.query_file {
        let parent = query_file.parent();
        if let Ok(query_file) = query_file.canonicalize() {
            base_uri = url::Url::from_file_path(query_file).ok();
        }
        (
            std::fs::read_to_string(&query_file)?,
            parent.map(|p| format!("{}", p.display())),
        )
    } else {
        print_help();
        return Err("No query was provided.")?;
    };
    let mut namespaces = HashMap::new();
    namespaces.insert("file".to_string(), file_module::FILE.to_string());
    namespaces.insert(
        "validate".to_string(),
        "http://basex.org/modules/validate".to_string(),
    );
    namespaces.insert(
        "err".to_string(),
        "http://www.w3.org/2005/xqt-errors".to_string(),
    );
    let mut parse_init = ParseInit::default();
    let default_reader = parse_init.reader.clone();
    parse_init.reader = Arc::new(move |uri, base, locations| {
        if Namespace(uri) == file_module::FILE
            || Namespace(uri) == html_module::HTML
            || Namespace(uri) == validate_module::VALIDATE
        {
            return Ok((format!("module namespace template = \"{}\";", uri), None));
        }
        match (default_reader)(uri, base, locations) {
            Ok(r) => Ok(r),
            err => {
                eprintln!("No location for '{}'", uri);
                err
            }
        }
    });
    parse_init.base_url = module_base;
    let mut function_definitions = function_definitions();
    file_module::add(&mut namespaces, &mut function_definitions);
    html_module::add(&mut namespaces, &mut function_definitions);
    validate_module::add(&mut namespaces, &mut function_definitions);
    parse_init.fd = &function_definitions;
    parse_init.namespaces = &namespaces;
    let xquery = parse(&query, parse_init)?;
    let context_init = default_tree_context_init(xquery.qnames().clone(), function_definitions);
    let mut context = GlobalContext::new(&context_init, xquery);
    context.set_base_uri(base_uri);
    let mut context = Context::new(context)?;
    let r = eval_xquery(&mut context)?;
    let mut out: Option<String> = None;
    if r.len() == 1 {
        if let Item::Node(node) = r.get(0) {
            out = Some(node_to_string(node, false, true)?);
        }
    }
    let out: String = out.unwrap_or_else(|| format!("{:#?}", r));
    if let Some(output_file) = args.output_file {
        std::fs::write(output_file, out)?;
    } else {
        println!("{}", out);
    }
    Ok(())
}
