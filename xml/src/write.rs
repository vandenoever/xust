pub mod stream;

use crate::{error::Result, XmlVersion};
use lazy_static::lazy_static;
use regex::Regex;
use std::{fmt::Write, usize};
use xust_tree::{
    node::{Node, Ref},
    qnames::{NamespaceDefinition, QName, QNameCollection, QNameId, INITIAL_NAMESPACE_DEFINITIONS},
    stream::Item,
};

pub(crate) enum XmlEncoding {
    Utf8,
    #[allow(dead_code)]
    Utf16,
}

pub(crate) enum NormalizationForm {
    None,
    #[allow(dead_code)]
    NFC,
}

pub struct XmlOutputParameters {
    pub version: XmlVersion,
    encoding: XmlEncoding,
    #[allow(dead_code)]
    indent: bool,
    #[allow(dead_code)]
    suppress_indentation: Vec<QName>,
    #[allow(dead_code)]
    cdata_section_elements: Vec<QName>,
    omit_xml_declaration: bool,
    standalone: bool,
    #[allow(dead_code)]
    doctype_system: String,
    #[allow(dead_code)]
    doctype_public: String,
    #[allow(dead_code)]
    undeclare_prefixes: bool,
    #[allow(dead_code)]
    normalization_form: NormalizationForm,
    #[allow(dead_code)]
    normalize_empty_elements: bool,
}

impl Default for XmlOutputParameters {
    fn default() -> Self {
        Self {
            version: XmlVersion::Xml10,
            encoding: XmlEncoding::Utf8,
            indent: false,
            suppress_indentation: Vec::new(),
            cdata_section_elements: Vec::new(),
            omit_xml_declaration: false,
            standalone: false,
            doctype_system: String::new(),
            doctype_public: String::new(),
            undeclare_prefixes: false,
            normalization_form: NormalizationForm::None,
            normalize_empty_elements: false,
        }
    }
}

pub(crate) struct XmlWriter<W: Write> {
    write: W,
    config: XmlOutputParameters,
    starting_element: bool,
    element_still_empty: bool,
    buffer: String,
    ns: NamespaceStack,
    qnames: QNameCollection,
}

impl<W: Write> XmlWriter<W> {
    pub fn new(write: W, config: XmlOutputParameters, qnames: QNameCollection) -> Self {
        Self {
            write,
            config,
            starting_element: false,
            element_still_empty: false,
            buffer: String::new(),
            ns: NamespaceStack::new(),
            qnames,
        }
    }
    pub fn into_inner(self) -> W {
        self.write
    }
}

struct NamespaceStackItem {
    qname: QNameId,
    ns_len: usize,
}

#[derive(Default)]
struct NamespaceStack {
    stack: Vec<NamespaceStackItem>,
    ns: Vec<NamespaceDefinition>,
}

impl NamespaceStack {
    fn new() -> Self {
        let qname = QNameId::from_usize(0);
        let ns: Vec<_> = INITIAL_NAMESPACE_DEFINITIONS.into();
        Self {
            stack: vec![NamespaceStackItem {
                qname,
                ns_len: ns.len(),
            }],
            ns,
        }
    }
    fn push(&mut self, qname: QNameId, ns: &[NamespaceDefinition]) {
        self.stack.push(NamespaceStackItem {
            qname,
            ns_len: ns.len(),
        });
        self.ns.extend(ns);
    }
    fn pop(&mut self) -> QNameId {
        let item = self.stack.pop().unwrap();
        self.ns.truncate(self.ns.len() - item.ns_len);
        item.qname
    }
    fn ns(&self) -> &[NamespaceDefinition] {
        let ns_len = self.stack.last().unwrap().ns_len;
        &self.ns[self.ns.len() - ns_len..]
    }
}

pub fn tree_to_string<T: Ref>(tree: T, config: XmlOutputParameters) -> Result<String> {
    let node = Node::root(tree);
    to_string(&node, config)
}

pub fn node_to_string<T: Ref>(
    node: &Node<T>,
    omit_xml_declaration: bool,
    normalize_empty_elements: bool,
) -> Result<String> {
    let config = XmlOutputParameters {
        omit_xml_declaration,
        normalize_empty_elements,
        ..Default::default()
    };
    to_string(node, config)
}

fn to_string<T: Ref>(node: &Node<T>, config: XmlOutputParameters) -> Result<String> {
    let mut w = XmlWriter::new(String::new(), config, node.qnames().clone());
    for i in node.item_iter() {
        write_item(&mut w, i)?;
    }
    Ok(w.into_inner())
}

fn write_item<W: Write>(w: &mut XmlWriter<W>, item: Item) -> Result<()> {
    if w.starting_element {
        match item {
            Item::Attribute { qname, value, .. } => {
                write_attribute_qname(w, qname)?;
                write_attribute_value(&mut w.write, value)?;
                w.write.write_str("\"")?;
            }
            Item::StartAttribute { qname, .. } => {
                write_attribute_qname(w, qname)?;
            }
            Item::String(s) => w.buffer.push_str(s),
            Item::Char(c) => w.buffer.push(c),
            Item::EndAttribute => {
                write_attribute_value(&mut w.write, &w.buffer)?;
                w.write.write_str("\"")?;
                w.buffer.clear();
            }
            _ => {
                w.starting_element = false;
            }
        }
    }
    if w.starting_element {
        return Ok(());
    }
    match item {
        Item::StartDocument => {
            if !w.config.omit_xml_declaration {
                write_document_start(w)?;
            }
        }
        Item::StartElement {
            qname,
            namespace_definitions,
            ..
        } => {
            ensure_element_tag_closed(w)?;
            write_element_start(w, qname, namespace_definitions)?;
            w.starting_element = true;
        }
        Item::StartAttribute { .. } => panic!(),
        Item::StartProcessingInstruction { .. } => panic!(),
        Item::StartComment => {}
        Item::StartText => {}
        Item::Attribute { .. } => panic!(),
        Item::ProcessingInstruction { target, content } => {
            ensure_element_tag_closed(w)?;
            write_processing_instruction(w, target, content)?;
        }
        Item::Comment(content) => {
            ensure_element_tag_closed(w)?;
            write_comment(&mut w.write, content)?;
        }
        Item::Text(text) => {
            ensure_element_tag_closed(w)?;
            write_text(&mut w.write, text)?;
        }
        Item::String(s) => w.buffer.push_str(s),
        Item::Char(c) => w.buffer.push(c),
        Item::EndElement => {
            let qname = w.ns.pop();
            write_element_end(w, qname)?;
        }
        Item::EndProcessingInstruction => panic!(),
        Item::EndComment => {
            write_comment(&mut w.write, &w.buffer)?;
            w.buffer.clear();
        }
        Item::EndText => {
            write_text(&mut w.write, &w.buffer)?;
            w.buffer.clear();
        }
        _ => {}
    }
    Ok(())
}

fn write_document_start<W: Write>(w: &mut XmlWriter<W>) -> Result<()> {
    Ok(writeln!(
        &mut w.write,
        "<?xml version=\"1.{}\" encoding=\"{}\"{}?>",
        match w.config.version {
            XmlVersion::Xml10 => "0",
            XmlVersion::Xml11 => "1",
        },
        match w.config.encoding {
            XmlEncoding::Utf8 => "UTF-8",
            XmlEncoding::Utf16 => "UTF-16",
        },
        if w.config.standalone {
            " standalone=\"yes\""
        } else {
            ""
        }
    )?)
}

fn write_processing_instruction<W: Write>(
    w: &mut XmlWriter<W>,
    target: QNameId,
    content: &str,
) -> Result<()> {
    write!(
        &mut w.write,
        "<?{}",
        w.qnames.to_ref(target).local_name().as_str()
    )?;
    if !content.is_empty() {
        write!(&mut w.write, " {}", content)?;
    }
    w.write.write_str("?>")?;
    Ok(())
}

fn write_comment<W: Write>(w: &mut W, content: &str) -> Result<()> {
    w.write_str("<!--")?;
    w.write_str(content)?;
    w.write_str("-->")?;
    Ok(())
}

// handle all attributes and write the element start
// return the first non-attribute item
fn write_element_start<W: Write>(
    w: &mut XmlWriter<W>,
    qname: QNameId,
    namespaces: &[NamespaceDefinition],
) -> Result<()> {
    write_qname(w, qname)?;
    let parent_ns = w.ns.ns();
    for ns in namespaces {
        if !parent_ns.contains(ns) {
            let ns = w.qnames.namespace_ref(*ns);
            let prefix = ns.prefix();
            if w.config.version == XmlVersion::Xml11 || !prefix.is_empty() {
                if prefix.is_empty() {
                    write!(&mut w.write, " xmlns=\"{}\"", ns.namespace())?;
                } else {
                    write!(&mut w.write, " xmlns:{}=\"{}\"", prefix, ns.namespace())?;
                }
            }
        }
    }
    w.ns.push(qname, namespaces);
    w.element_still_empty = true;
    Ok(())
}

fn ensure_element_tag_closed<W: Write>(w: &mut XmlWriter<W>) -> Result<()> {
    if w.element_still_empty {
        w.element_still_empty = false;
        w.write.write_str(">")?;
    }
    Ok(())
}

fn write_element_end<W: Write>(w: &mut XmlWriter<W>, qname: QNameId) -> Result<()> {
    if w.element_still_empty {
        w.write.write_str("/>")?;
        w.element_still_empty = false;
    } else {
        let qname = w.qnames.to_ref(qname);
        let prefix = qname.prefix();
        if prefix.is_empty() {
            write!(&mut w.write, "</{}>", qname.local_name())?;
        } else {
            write!(&mut w.write, "</{}:{}>", prefix, qname.local_name())?;
        }
    }
    Ok(())
}

fn write_qname<W: Write>(w: &mut XmlWriter<W>, qname: QNameId) -> Result<()> {
    let qname = w.qnames.to_ref(qname);
    let prefix = qname.prefix();
    if prefix.is_empty() {
        write!(&mut w.write, "<{}", qname.local_name())?;
    } else {
        write!(&mut w.write, "<{}:{}", prefix, qname.local_name())?;
    }
    Ok(())
}

fn write_attribute_qname<W: Write>(w: &mut XmlWriter<W>, qname: QNameId) -> Result<()> {
    let qname = w.qnames.to_ref(qname);
    let prefix = qname.prefix();
    if prefix.is_empty() {
        write!(&mut w.write, " {}=\"", qname.local_name())?;
    } else {
        write!(&mut w.write, " {}:{}=\"", prefix, qname.local_name())?;
    }
    Ok(())
}

fn write_text<W: Write>(w: &mut W, mut v: &str) -> Result<()> {
    while let Some(m) = NON_XML_TEXT_REGEX.find(v) {
        write!(w, "{}", &v[..m.start()])?;
        let s = m.as_str();
        if s == "]]>" {
            w.write_str("]]&gt;")?;
        } else {
            for c in m.as_str().chars() {
                match c {
                    '<' => w.write_str("&lt;"),
                    '&' => w.write_str("&amp;"),
                    c => write!(w, "&#x{};", c as usize),
                }?;
            }
        }
        v = &v[m.end()..];
    }
    write!(w, "{}", v)?;
    Ok(())
}

lazy_static! {
    pub(crate) static ref NON_XML_TEXT_REGEX: Regex =
        Regex::new("<|&|]]>|[^\\x09\\x0A\\x0D\\x20-\\x7E\\x85\\xA0-\\uD7FF\\uE000-\\uFFFD\\U00010000-\\U0010FFFF]")
            .unwrap();
}

lazy_static! {
    pub(crate) static ref NON_XML_ATTR_REGEX: Regex =
        Regex::new("[<&\"]|[^\\x09\\x0A\\x0D\\x20-\\x7E\\x85\\xA0-\\uD7FF\\uE000-\\uFFFD\\U00010000-\\U0010FFFF]")
            .unwrap();
}

fn write_attribute_value<W: Write>(w: &mut W, mut value: &str) -> Result<()> {
    while let Some(m) = NON_XML_ATTR_REGEX.find(value) {
        write!(w, "{}", &value[..m.start()])?;
        for c in m.as_str().chars() {
            match c {
                '<' => w.write_str("&lt;"),
                '&' => w.write_str("&amp;"),
                '"' => w.write_str("&quot;"),
                c => write!(w, "&#x{};", c as usize),
            }?;
        }
        value = &value[m.end()..];
    }
    write!(w, "{}", value)?;
    Ok(())
}
