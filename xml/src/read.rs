use crate::{
    dtd::{
        check_character_validity,
        entities::{Entities, UnparsedEntities},
        is_name_char, is_name_start_char, is_unrestricted_xml_1_1_char, is_xml_1_0_char, parse_dtd,
        validator::{ElementContentType, Validator},
        Origin, PIOrComment,
    },
    error::{Result, XmlParserError},
    XmlVersion,
};
use encoding_rs::Encoding;
use encoding_rs_io::DecodeReaderBytesBuilder;
use lazy_static::lazy_static;
use regex::{bytes, Regex};
use std::{
    ffi::OsStr,
    io::Read,
    iter::Peekable,
    path::{Component, Path, PathBuf},
    sync::Arc,
};
use xmlparser::{ElementEnd, Token, Tokenizer};
use xust_tree::{
    qnames::{NCName, Prefix, QNameCollector},
    stream::{IteratorToTree, Status},
    string_stream::{self, Item, ItemStream, ItemStreamBuilder, StringNamesToQNames},
    tree::Tree,
    TreeBuilder,
};

type Iter<'a> = Peekable<Tokenizer<'a>>;

/**
 * Parse xml from a byte array.
 */
pub fn parse_xml_from_bytes<T: 'static>(
    bytes: Vec<u8>,
    config: Option<&ParserConfig>,
    qnames: Option<QNameCollector>,
) -> Result<Tree<T>> {
    let text = decode_bytes(bytes)?;
    parse_xml(&text.1, config, qnames)
}

/**
 * Read an XML file.
 */
pub fn parse_xml_from_file<T: 'static>(
    path: &Path,
    config: Option<&ParserConfig>,
    qnames: Option<QNameCollector>,
) -> Result<Tree<T>> {
    let r = if let Some(config) = config {
        (config.entity_loader)(config.base_url.as_deref(), path, XmlVersion::Xml10)
    } else {
        default_entity_loader(
            config.and_then(|c| c.base_url.as_deref()),
            path,
            XmlVersion::Xml10,
        )
    }?;
    let default_config = ParserConfig {
        base_url: r.base,
        ..Default::default()
    };
    let config = config.unwrap_or(&default_config);
    parse_xml(&r.text, Some(config), qnames)
        .map_err(|e| err!("Could not read {}: {}", path.display(), e))
}

static ENCODING_REGEX: &str = "^<\\?xml(?:\\s+version\\s*=\\s*['\"]1\\.([10])['\"])?(?:\\s+encoding\\s*=\\s*['\"]([^'\"]*)['\"])?";

fn decode(mut bytes: Vec<u8>, enc: &'static Encoding, skip: usize) -> Result<String> {
    if enc == encoding_rs::UTF_8 {
        if skip > 0 {
            bytes.drain(0..skip);
        }
        Ok(String::from_utf8(bytes)?)
    } else {
        let mut xml = String::new();
        DecodeReaderBytesBuilder::new()
            .encoding(Some(enc))
            .build(&bytes[skip..])
            .read_to_string(&mut xml)?;
        Ok(xml)
    }
}

fn find_encoding(bytes: Vec<u8>) -> Result<(Option<&'static Encoding>, String)> {
    lazy_static! {
        // match version and encoding of TextDecl and XmlDecl
        static ref REGEX: bytes::Regex = bytes::Regex::new(ENCODING_REGEX).unwrap();
    }
    let xml: String;
    let mut bom_encoding = None;
    if let Some((bom, size)) = Encoding::for_bom(&bytes) {
        xml = decode(bytes, bom, size)?;
        bom_encoding = Some(bom);
    } else if let Some(enc) = REGEX
        .captures(&bytes)
        .and_then(|c| c.get(2).map(|c| c.as_bytes()))
    {
        if enc == b"iso-8859-1" {
            // encoding_rs does not translate NEL (0x85) property from iso-8859-1,
            // so we handle iso-8859-1 ourselves
            xml = bytes.iter().map(|b| *b as char).collect();
        } else if let Some(enc) = Encoding::for_label_no_replacement(enc) {
            xml = decode(bytes, enc, 0)?;
        } else {
            return Err(err!("Unknown encoding {}.", String::from_utf8_lossy(enc)));
        }
    } else {
        xml = String::from_utf8(bytes)?;
    }
    Ok((bom_encoding, xml))
}

fn to_abs_path(base_url: Option<&Path>, path: &Path) -> Result<PathBuf> {
    let path = PathBuf::from(path);
    if !path.is_relative() {
        return Err(err!("Path {} is absolute.", path.display()));
    }
    if path
        .components()
        .any(|c| c == Component::Normal(OsStr::new("..")))
    {
        return Err(err!("Path {} has '..'.", path.display()));
    }
    let path = if let Some(base_url) = &base_url {
        if base_url.is_file() {
            base_url.parent().unwrap().join(path)
        } else {
            base_url.join(path)
        }
    } else {
        path
    };
    Ok(path)
}

#[derive(Default)]
pub struct NormalizedXml(pub(crate) String);

impl NormalizedXml {
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    pub fn len(&self) -> usize {
        self.0.len()
    }
    pub fn take(self) -> String {
        self.0
    }
}

pub fn decode_bytes(bytes: Vec<u8>) -> Result<(Option<String>, NormalizedXml)> {
    bytes_to_string(XmlVersion::Xml10, bytes)
}

fn bytes_to_string(version: XmlVersion, bytes: Vec<u8>) -> Result<(Option<String>, NormalizedXml)> {
    let (bom_encoding, text) = find_encoding(bytes)?;
    let (encoding, version) = find_xml_version(&text, version, bom_encoding)?;
    Ok((
        encoding,
        NormalizedXml(normalize_end_of_lines(text, version)),
    ))
}

pub fn normalize_xml(text: String) -> NormalizedXml {
    NormalizedXml(normalize_end_of_lines(text, XmlVersion::Xml10))
}

pub fn default_entity_loader(
    base_url: Option<&Path>,
    path: &Path,
    parent_version: XmlVersion,
) -> Result<EntityLoaderResult> {
    let path = to_abs_path(base_url, path)?;
    let bytes = std::fs::read(&path).map_err(|e| err!("Cannot read {}: {}", path.display(), e))?;
    let (encoding, text) = bytes_to_string(parent_version, bytes)?;
    Ok(EntityLoaderResult {
        text,
        encoding,
        base: Some(path),
    })
}

fn find_xml_version(
    xml: &str,
    mut version: XmlVersion,
    bom_encoding: Option<&'static Encoding>,
) -> Result<(Option<String>, XmlVersion)> {
    lazy_static! {
        // match version and encoding of TextDecl and XmlDecl
        static ref REGEX: Regex = Regex::new(ENCODING_REGEX).unwrap();
    }
    let enc = if let Some(c) = REGEX.captures(xml) {
        if let Some(v) = c.get(1).map(|v| {
            if v.as_str() == "1" {
                XmlVersion::Xml11
            } else {
                XmlVersion::Xml10
            }
        }) {
            version = v;
        }
        c.get(2).map(|c| c.as_str())
    } else {
        None
    };
    let encoding;
    if let Some(bom_encoding) = bom_encoding {
        if let Some(prolog_enc) = enc {
            let e1 = Encoding::for_label(prolog_enc.as_bytes());
            let e2 = Encoding::for_label_no_replacement(prolog_enc.as_bytes());
            if bom_encoding.name() != prolog_enc
                && e1 != Some(bom_encoding)
                && e2 != Some(bom_encoding)
            {
                return Err(err!(
                    "Two different encodings: '{}' != '{}'",
                    bom_encoding.name(),
                    prolog_enc
                ));
            }
        }
        encoding = Some(bom_encoding.name().to_string());
    } else {
        encoding = enc.map(|e| e.to_string())
    }
    Ok((encoding, version))
}

// implementation of 2.11 End-of-Line Handling
fn normalize_end_of_lines(xml: String, version: XmlVersion) -> String {
    let mut saw_newline = false;
    if version == XmlVersion::Xml10 {
        // replace \r\n, and \r with \n
        let mut bytes = xml.into_bytes();
        bytes.retain_mut(|b| {
            if *b == b'\n' && saw_newline {
                saw_newline = false;
                return false;
            }
            if *b == b'\r' {
                saw_newline = true;
                *b = b'\n';
            }
            true
        });
        let s;
        unsafe {
            // this is always defined behaviour because only
            // \r and \n were replaced or removed
            s = String::from_utf8_unchecked(bytes);
        };
        s
    } else {
        // replace \r\n, \r\85, \85, \2028 and \r with \n
        if !xml
            .chars()
            .any(|c| c == '\r' || c == 0x85 as char || c == '\u{2028}')
        {
            return xml;
        }
        let mut s = String::with_capacity(xml.len());
        for mut c in xml.chars() {
            let mut keep_char = true;
            if saw_newline {
                s.push(0xA as char);
                keep_char = c != 0xA as char && c != 0x85 as char;
            }
            if c == 0x85 as char || c == '\u{2028}' {
                c = 0xA as char;
            }
            saw_newline = c == 0xD as char;
            if !saw_newline && keep_char {
                s.push(c);
            }
        }
        if saw_newline {
            s.push(0xA as char);
        }
        s
    }
}

pub struct EntityLoaderResult {
    pub text: NormalizedXml,
    pub encoding: Option<String>,
    pub base: Option<PathBuf>,
}

pub type EntityLoader = dyn Fn(Option<&Path>, &Path, XmlVersion) -> Result<EntityLoaderResult>;

#[derive(Clone)]
pub struct ParserConfig {
    pub base_url: Option<PathBuf>,
    pub entity_loader: Arc<EntityLoader>,
    pub encoding: Option<String>,
    // if there is no DTD, fail
    pub require_dtd: bool,
    // if there is a DTD, validate the document
    pub dtd_validation: bool,
    pub strip_comments: bool,
    pub normalize_attribute_whitespace: bool,
}

impl Default for ParserConfig {
    fn default() -> Self {
        Self {
            base_url: None,
            entity_loader: Arc::new(default_entity_loader),
            encoding: None,
            require_dtd: false,
            dtd_validation: true,
            strip_comments: false,
            normalize_attribute_whitespace: true,
        }
    }
}

pub fn parse_xml<T: 'static>(
    xml: &NormalizedXml,
    config: Option<&ParserConfig>,
    qnames: Option<QNameCollector>,
) -> Result<Tree<T>> {
    let qnames = qnames.unwrap_or_default();
    let default_config = ParserConfig::default();
    let config = config.unwrap_or(&default_config);
    let mut builder: StringNamesToQNames<Tree<T>, _, _> = StringNamesToQNames::from_qname_collector(
        IteratorToTree::new(Box::new(TreeBuilder::empty())),
        qnames,
    );
    parse_to_stream(xml, config, &mut builder)?;
    let tree = builder.build()?;
    Ok(tree)
}

#[derive(PartialEq)]
enum AttType<'a> {
    DefaultNamespace,
    Namespace {
        prefix: Prefix<'a>,
    },
    Attribute {
        prefix: Prefix<'a>,
        local_name: NCName<'a>,
    },
}

struct Att<'a> {
    att_type: AttType<'a>,
    name: &'a str,
    value: &'a str,
}

pub fn parse_to_stream(
    xml: &NormalizedXml,
    config: &ParserConfig,
    out: &mut dyn ItemStream,
) -> Result<()> {
    parse_prolog(&xml.0, config, out)
}

// return true if the character sequence makes up a valid character reference
fn check_char_ref(mut chars: std::str::Chars) -> bool {
    match chars.next() {
        None => false,
        Some('x') => chars.all(|c| c.is_ascii_hexdigit()),
        Some(c) => c.is_ascii_digit() && chars.all(|c| c.is_ascii_digit()),
    }
}

// look for incomplete/invalid entity references
// if one is found, its position is returned
// do not use on CDATA sections because there plain & is allowed
fn has_invalid_entity_references(xml: &str) -> Option<usize> {
    // look for incomplete entity references
    // xmlparser does not catch all of them
    let mut start = 0;
    while let Some(pos) = xml[start..].find('&') {
        let pos = start + pos;
        if let Some(end) = xml[pos..].find(';') {
            let mut range = xml[pos + 1..pos + end].chars();
            match range.next() {
                Some('#') => {
                    if !check_char_ref(range) {
                        return Some(pos);
                    }
                }
                Some(c) => {
                    if !is_name_start_char(c) || !range.all(is_name_char) {
                        return Some(pos);
                    }
                }
                None => return Some(pos),
            }
        } else {
            return Some(pos);
        }
        start = pos + 1;
    }
    None
}
fn parse_prolog(xml: &str, config: &ParserConfig, out: &mut dyn ItemStream) -> Result<()> {
    let loader = if config.dtd_validation {
        Some(&*config.entity_loader)
    } else {
        None
    };
    let (left, mut dtd) = parse_dtd(xml, config.base_url.as_deref(), loader)?;
    if config.require_dtd && dtd.validator.is_none() {
        return Err(err!("There is no DTD."));
    }
    if !config.dtd_validation {
        dtd.validator = None;
    }
    let tokens = Tokenizer::from(left).peekable();
    out.next(Item::StartDocument)?;
    let mut atts = Vec::new();
    let parser = Parser {
        tokens,
        entities: &dtd.entities,
        unparsed_entities: &mut dtd.unparsed_entities,
        xml_version: dtd.xml_version,
        standalone: dtd.standalone,
        validator: &mut dtd.validator,
        atts: &mut atts,
        buffer: String::new(),
        config,
    };
    parse_pis_and_comments(out, &parser, &dtd.pis_and_comments)?;
    parse_content(out, parser)
}

struct Parser<'a> {
    tokens: Iter<'a>,
    entities: &'a Entities,
    unparsed_entities: &'a UnparsedEntities,
    xml_version: XmlVersion,
    standalone: bool,
    validator: &'a mut Option<Validator>,
    atts: &'a mut Vec<Att<'a>>,
    buffer: String,
    config: &'a ParserConfig,
}

type Out<'a> = &'a mut dyn ItemStream;

fn parse_pis_and_comments(out: Out, parser: &Parser, pac: &[PIOrComment]) -> Result<()> {
    for pac in pac {
        match pac {
            PIOrComment::PI { target, content } => {
                add_pi(out, target, content)?;
            }
            PIOrComment::Comment(comment) => {
                add_comment(out, parser, comment)?;
            }
        }
    }
    Ok(())
}

fn add_pi(out: Out, target: &str, content: &str) -> Result<()> {
    if "xml".eq_ignore_ascii_case(target) {
        return Err(err!("PI target may not be 'xml'"));
    }
    out.next(Item::ProcessingInstruction {
        target: NCName(target),
        content,
    })?;
    Ok(())
}

fn add_comment(out: Out, parser: &Parser, text: &str) -> Result<()> {
    check_character_validity(parser.xml_version, text)?;
    if !parser.config.strip_comments {
        out.next(Item::Comment(text))?;
    }
    Ok(())
}

fn parse_content(out: Out, mut parser: Parser) -> Result<()> {
    let mut got_element = false;
    while let Some(token) = parser.tokens.next() {
        let token = token.map_err(XmlParserError)?;
        match token {
            Token::ElementStart {
                prefix,
                local,
                span,
            } => {
                let ect = if let Some(validator) = parser.validator {
                    validator.start_element(&span[1..])?
                } else {
                    ElementContentType::Mixed
                };
                let tag = (prefix.as_str(), local.as_str());
                if span.start() + 2 == local.start() {
                    return Err(err!("Bad QName syntax: colon at start"));
                }
                got_element = true;
                if !parse_element(out, &mut parser, tag, 0, &prefix, &local, ect)? {
                    return Err(err!("unexpected end of document."));
                }
            }
            Token::ProcessingInstruction {
                target, content, ..
            } => {
                add_pi(out, &target, content.map(|c| c.as_str()).unwrap_or(""))?;
            }
            Token::Comment { text, .. } => {
                add_comment(out, &parser, &text)?;
            }
            Token::Text { text } => {
                // TODO: check that it's whitespace
                let ect = ElementContentType::Any;
                if let Some((_ent, _rest)) = replace_entities(out, &mut parser, text.as_str(), ect)?
                {
                    // no support yet for entities outside the document element
                    unimplemented!()
                }
            }
            Token::Attribute { span, .. } => {
                return Err(err!("Unexpected attribute {}.", span));
            }
            Token::Cdata { text, .. } => {
                return Err(err!("Unexpected CDATA text {}.", text));
            }
            _ => return Err(err!("unexpected xml event {:?}", token)),
        }
    }
    if !got_element {
        return Err(err!("Document has no document element."));
    }
    if let Some(validator) = parser.validator {
        if !validator.is_done() {
            return Err(err!("Document is not complete."));
        }
    }
    out.next(Item::EndDocument)?;
    Ok(())
}

fn unescape_att<'a>(
    out: Out,
    parser: &mut Parser<'a>,
    v: &'a str,
    countdown: &mut usize,
    state: AttState,
    external: bool,
) -> Result<AttState> {
    let (r, state) = unescape_and_normalize_att(out, parser, v, state, external)?;
    if let Some((ent, rest)) = r {
        *countdown -= 1;
        if *countdown == 0 {
            return Err(err!("too many entity replacements"));
        }
        let state = unescape_att(out, parser, ent, countdown, state, external)?;
        unescape_att(out, parser, rest, countdown, state, external)
    } else {
        Ok(state)
    }
}

fn unescape_att_cdata<'a>(
    out: Out,
    parser: &mut Parser<'a>,
    v: &'a str,
    countdown: &mut usize,
    external: bool,
) -> Result<()> {
    if let Some((ent, rest)) = unescape_and_normalize_att_cdata(out, parser, v, external)? {
        *countdown -= 1;
        if *countdown == 0 {
            return Err(err!("too many entity replacements"));
        }
        unescape_att_cdata(out, parser, ent, countdown, external)?;
        unescape_att_cdata(out, parser, rest, countdown, external)?;
    }
    Ok(())
}

struct StringSink(String);

impl ItemStream for StringSink {
    fn next(&mut self, item: Item) -> std::result::Result<(), string_stream::Error> {
        match item {
            Item::Char(c) => self.0.push(c),
            _ => panic!("DO NOT USE next with this event {:?}", item),
        }
        Ok(())
    }
    fn write_str(&mut self, s: &str) -> std::result::Result<(), string_stream::Error> {
        self.0.push_str(s);
        Ok(())
    }
    fn status(&self) -> Status {
        panic!("DO NOT USE");
    }
}

fn handle_attribute_value<'a>(out: Out, parser: &mut Parser<'a>, att: &Att<'a>) -> Result<()> {
    let (normalize_whitespace, external) = if let Some(validator) = parser.validator {
        validator.start_attribute(att.name)?
    } else {
        (true, false)
    };
    if normalize_whitespace {
        unescape_att(
            out,
            parser,
            att.value,
            &mut 1000,
            AttState::NoCharacterYet,
            external,
        )?;
    } else {
        unescape_att_cdata(out, parser, att.value, &mut 1000, external)?;
    }
    if let Some(validator) = parser.validator {
        validator.end_attribute(parser.unparsed_entities)?;
    }
    Ok(())
}

fn parse_element<'a>(
    out: Out,
    parser: &mut Parser<'a>,
    tag: (&'a str, &'a str),
    recursion: usize,
    prefix: &str,
    local_name: &str,
    ect: ElementContentType,
) -> Result<bool> {
    parser.atts.clear();
    let mut atts = Vec::new();
    std::mem::swap(&mut atts, parser.atts);
    while let Some(Ok(Token::Attribute { .. })) = parser.tokens.peek() {
        match parser.tokens.next().unwrap().unwrap() {
            Token::Attribute {
                prefix,
                local,
                value,
                span,
            } => {
                let name = &span.as_str()[..local.end() - span.start()];
                let att_type = if prefix.as_str() == "xmlns" {
                    AttType::Namespace {
                        prefix: Prefix(local.as_str()),
                    }
                } else if prefix.is_empty() && local.as_str() == "xmlns" {
                    AttType::DefaultNamespace
                } else {
                    AttType::Attribute {
                        prefix: Prefix(prefix.as_str()),
                        local_name: NCName(local.as_str()),
                    }
                };
                atts.push(Att {
                    att_type,
                    name,
                    value: value.as_str(),
                });
            }
            token => return Err(err!("Unexpected token {:?}", token)),
        }
    }
    // define namespaces
    let mut tmp = String::new(); // temporarily use buffer from parser
    std::mem::swap(&mut tmp, &mut parser.buffer);
    let mut nsout = StringSink(tmp);
    for att in atts.iter() {
        if let AttType::DefaultNamespace = &att.att_type {
            nsout.0.clear();
            handle_attribute_value(&mut nsout, parser, att)?;
            out.next(Item::DefaultNamespace(&nsout.0))?;
        } else if let AttType::Namespace { prefix } = &att.att_type {
            nsout.0.clear();
            handle_attribute_value(&mut nsout, parser, att)?;
            if parser.xml_version == XmlVersion::Xml10 && nsout.0.is_empty() {
                return Err(err!("Empty ns not allowed in XML 1.0"));
            }
            out.next(Item::Namespace {
                prefix: *prefix,
                namespace: &nsout.0,
            })?;
        }
    }
    std::mem::swap(&mut nsout.0, &mut parser.buffer);
    out.next(Item::StartElement {
        prefix: Prefix(prefix),
        local_name: NCName(local_name),
    })?;
    if out.status() != Status::Open {
        return Err(err!("Error while parsing."));
    }
    for att in atts.iter() {
        if let AttType::Attribute { prefix, local_name } = &att.att_type {
            out.next(Item::StartAttribute {
                prefix: *prefix,
                local_name: *local_name,
            })?;
            handle_attribute_value(out, parser, att)?;
            out.next(Item::EndAttribute)?;
        }
    }
    std::mem::swap(&mut atts, parser.atts);
    if let Some(validator) = parser.validator {
        // set the default and fixed attributes that were not yet set
        for (name, value) in validator.end_attributes()? {
            if let Some((p, l)) = name.find(':').map(|p| name.split_at(p)) {
                out.next(Item::StartAttribute {
                    prefix: Prefix(p),
                    local_name: NCName(&l[1..]),
                })?;
            } else {
                out.next(Item::StartAttribute {
                    prefix: Prefix(""),
                    local_name: NCName(name),
                })?;
            }
            out.write_str(value)?;
            out.next(Item::EndAttribute)?;
        }
    }
    parse_element_content(out, parser, tag, recursion + 1, true, ect)
}
fn parse_element_content<'a>(
    out: Out,
    parser: &mut Parser<'a>,
    tag: (&'a str, &'a str),
    recursion: usize,
    must_close: bool,
    ect: ElementContentType,
) -> Result<bool> {
    if recursion > 1000 {
        return Err(err!("Too many nested calls"));
    }
    while let Some(token) = parser.tokens.next() {
        let token = token.map_err(XmlParserError)?;
        if let Token::ElementEnd { .. } = &token {
        } else if ect == ElementContentType::Empty {
            return Err(err!("No content allowed in this element."));
        }
        match token {
            Token::ElementStart {
                prefix,
                local,
                span,
            } => {
                let ect = if let Some(validator) = parser.validator {
                    validator.start_element(&span[1..])?
                } else {
                    ElementContentType::Mixed
                };
                let tag = (prefix.as_str(), local.as_str());
                parse_element(out, parser, tag, recursion + 1, &prefix, &local, ect)?;
            }
            Token::ElementEnd { end, .. } => match end {
                ElementEnd::Open => {}
                ElementEnd::Empty => {
                    end_element(out, parser)?;
                    return Ok(true);
                }
                ElementEnd::Close(prefix, local) => {
                    return if tag.0 != prefix.as_str() || tag.1 != local.as_str() {
                        Err(err!(
                            "Mismatching open and close tags {}:{} {}:{}.",
                            tag.0,
                            tag.1,
                            prefix,
                            local
                        ))
                    } else {
                        end_element(out, parser)?;
                        Ok(true)
                    };
                }
            },
            Token::Text { text } => {
                if let Some((ent, rest)) = replace_entities(out, parser, text.as_str(), ect)? {
                    parse_fragment(out, parser, ent, tag, ect, recursion + 1)?;
                    parse_fragment(out, parser, rest, tag, ect, recursion + 1)?;
                }
            }
            Token::Cdata { text, .. } => {
                if ect == ElementContentType::Children {
                    return Err(err!("No cdata allowed in this element."));
                }
                write_str(out, parser, &text)?;
            }
            Token::Comment { text, .. } => {
                add_comment(out, parser, &text)?;
            }
            Token::ProcessingInstruction {
                target, content, ..
            } => {
                add_pi(out, &target, content.map(|c| c.as_str()).unwrap_or(""))?;
            }
            token => return Err(err!("Unexpected token {:?}", token)),
        }
    }
    // no more tokens
    if must_close {
        Err(err!("ELEMENT {} was not closed", tag.1))
    } else {
        Ok(true)
    }
}
fn end_element(out: Out, parser: &mut Parser) -> Result<()> {
    if let Some(validator) = parser.validator {
        validator.end_element()?;
    }
    out.next(Item::EndElement)?;
    Ok(())
}
fn parse_fragment<'a>(
    out: Out,
    parser: &mut Parser<'a>,
    fragment: &'a str,
    tag: (&'a str, &'a str),
    ect: ElementContentType,
    recursion: usize,
) -> Result<bool> {
    if fragment.starts_with("<?xml") {
        return Err(err!(
            "XML declaration allowed only at the start of the document"
        ));
    }
    let mut tokens = Tokenizer::from_fragment(fragment, 0..fragment.len()).peekable();
    if let Some(Ok(Token::Declaration { .. })) = tokens.peek() {
        tokens.next();
    }
    std::mem::swap(&mut tokens, &mut parser.tokens);
    let r = parse_element_content(out, parser, tag, recursion + 1, false, ect)?;
    std::mem::swap(&mut tokens, &mut parser.tokens);
    Ok(r)
}

lazy_static! {
    pub(crate) static ref LT_AMP_REGEX: Regex = Regex::new("<|&").unwrap();
}

fn replace_entity<'a>(
    out: Out,
    parser: &mut Parser<'a>,
    s: &str,
    state: AttState,
    ect: ElementContentType,
) -> Result<(Option<&'a str>, AttState)> {
    let s = &s[1..s.len() - 1];
    let c = if let Some(s) = s.strip_prefix("#x") {
        let c: u32 = u32::from_str_radix(s, 16)?;
        std::char::from_u32(c).ok_or_else(|| err!("Invalid char {}", c))?
    } else if let Some(s) = s.strip_prefix('#') {
        let c: u32 = s.parse()?;
        std::char::from_u32(c).unwrap()
    } else {
        match s {
            "lt" => '<',
            "gt" => '>',
            "amp" => '&',
            "apos" => '\'',
            "quot" => '"',
            _ => {
                if let Some(r) = parser.entities.get(s) {
                    let (e, origin) = r.map_err(|e| err!("{}", e))?;
                    if parser.standalone && origin == Origin::External {
                        return Err(err!("External entity in standalone document."));
                    }
                    if state != AttState::NoAttribute || LT_AMP_REGEX.is_match(e) {
                        return Ok((Some(e), state));
                    } else {
                        write_str(out, parser, e)?;
                        return Ok((None, state));
                    }
                } else {
                    return Err(err!("Unrecognized entity '{}'.", s));
                }
            }
        }
    };
    if ect == ElementContentType::Children {
        return Err(err!("No characters allowed in this element."));
    }
    if (parser.xml_version == XmlVersion::Xml11 && !is_unrestricted_xml_1_1_char(c))
        || (parser.xml_version == XmlVersion::Xml10 && !is_xml_1_0_char(c))
    {
        return Err(err!("Non-xml character {:?}", c));
    }
    if state == AttState::NoAttribute {
        write_char(out, parser, c)?;
        return Ok((None, AttState::NoAttribute));
    }
    if c == ' ' {
        return Ok((
            None,
            if state == AttState::NoCharacterYet {
                AttState::NoCharacterYet
            } else {
                AttState::SpaceTodo
            },
        ));
    }
    if state == AttState::SpaceTodo {
        write_char(out, parser, ' ')?;
    }
    write_char(out, parser, c)?;
    Ok((None, AttState::Other))
}

fn write_str(out: Out, parser: &mut Parser, str: &str) -> Result<()> {
    if let Some(validator) = parser.validator {
        validator.add_text(str)?;
    }
    out.write_str(str)?;
    Ok(())
}

fn write_char(out: Out, parser: &mut Parser, c: char) -> Result<()> {
    if let Some(validator) = parser.validator {
        validator.add_char(c)?;
    }
    out.next(Item::Char(c))?;
    Ok(())
}

fn unescape_and_normalize_whitespace() -> &'static Regex {
    lazy_static! {
        static ref REGEX: Regex = Regex::new("[\r\n\t ]+|&[^;]+;").unwrap();
    }
    &REGEX
}

fn unescape() -> &'static Regex {
    lazy_static! {
        static ref REGEX: Regex = Regex::new("&[^;]+;").unwrap();
    }
    &REGEX
}

// replace entities
fn replace_entities<'a>(
    out: Out,
    parser: &mut Parser<'a>,
    mut input: &'a str,
    ect: ElementContentType,
) -> Result<Option<(&'a str, &'a str)>> {
    check_text(parser.xml_version, input)?;
    lazy_static! {
        static ref REGEX: Regex = Regex::new("&[^;]+;").unwrap();
    }
    while let Some(m) = REGEX.find(input) {
        write_str(out, parser, &input[..m.start()])?;
        input = &input[m.end()..];
        if let Some(ent) = replace_entity(out, parser, m.as_str(), AttState::NoAttribute, ect)?.0 {
            return Ok(Some((ent, input)));
        }
    }
    write_str(out, parser, input)?;
    Ok(None)
}

#[derive(PartialEq, Debug)]
enum AttState {
    NoCharacterYet,
    SpaceTodo,
    Other,
    NoAttribute,
}

// replace entities and normalize consecutive whitespace to single space
// this may come across a system entity
fn unescape_and_normalize_att<'a>(
    out: Out,
    parser: &mut Parser<'a>,
    mut input: &'a str,
    mut state: AttState,
    external: bool,
) -> Result<(Option<(&'a str, &'a str)>, AttState)> {
    check_text(parser.xml_version, input)?;
    let regex = if parser.config.normalize_attribute_whitespace {
        unescape_and_normalize_whitespace()
    } else {
        unescape()
    };
    while let Some(m) = regex.find(input) {
        if m.as_str().starts_with('&') {
            if m.start() > 0 {
                if state == AttState::SpaceTodo {
                    write_char(out, parser, ' ')?;
                }
                state = AttState::Other
            }
            write_str(out, parser, &input[..m.start()])?;
            let (ent, new_state) =
                replace_entity(out, parser, m.as_str(), state, ElementContentType::Mixed)?;
            if let Some(ent) = ent {
                input = &input[m.end()..];
                return Ok((Some((ent, input)), new_state));
            }
            state = new_state;
        } else if m.start() > 0 {
            if state == AttState::SpaceTodo {
                write_char(out, parser, ' ')?;
            }
            write_str(out, parser, &input[..m.start()])?;
            state = AttState::SpaceTodo;
        } else if state == AttState::Other {
            state = AttState::SpaceTodo;
        } else if external && parser.standalone {
            return Err(err!("Normalizing externally defined attribute value."));
        }
        input = &input[m.end()..];
    }
    if !input.is_empty() {
        if state == AttState::SpaceTodo {
            write_char(out, parser, ' ')?;
        }
        state = AttState::Other;
        write_str(out, parser, input)?;
    }
    Ok((None, state))
}

// replace entities and normalize whitespace to space
// this may come across a system entity
fn unescape_and_normalize_att_cdata<'a>(
    out: Out,
    parser: &mut Parser<'a>,
    mut input: &'a str,
    external: bool,
) -> Result<Option<(&'a str, &'a str)>> {
    check_text(parser.xml_version, input)?;
    let regex = unescape_and_normalize_whitespace();
    while let Some(m) = regex.find(input) {
        write_str(out, parser, &input[..m.start()])?;
        if m.as_str().starts_with('&') {
            let (ent, state) = replace_entity(
                out,
                parser,
                m.as_str(),
                AttState::Other,
                ElementContentType::Mixed,
            )?;
            if let Some(ent) = ent {
                input = &input[m.end()..];
                return Ok(Some((ent, input)));
            }
            if state == AttState::SpaceTodo {
                write_char(out, parser, ' ')?;
            }
        } else {
            for c in m.as_str().chars() {
                if external && parser.standalone && c != ' ' {
                    return Err(err!(
                        "Had to normalize an attribute in a standalone document."
                    ));
                }
                write_char(out, parser, ' ')?;
            }
        }
        input = &input[m.end()..];
    }
    write_str(out, parser, input)?;
    Ok(None)
}

fn check_text(xml_version: XmlVersion, input: &str) -> Result<()> {
    if has_invalid_entity_references(input).is_some() {
        return Err(err!("Found an invalid entity reference."));
    }
    if input.contains('<') {
        return Err(err!("Lone < is not allowed."));
    }
    check_character_validity(xml_version, input)
}

#[test]
fn test_unescape() {
    fn test(s: &str) -> String {
        let xml = format!("<a>{}</a>", s);
        let tree = parse_test(&xml).unwrap();
        let root = xust_tree::node::Node::root(&tree);
        let txt = root.first_child().unwrap().first_child().unwrap();
        txt.string_value().to_string()
    }
    assert_eq!(test("&lt;"), "<");
    assert_eq!(test("&lt;e"), "<e");
    assert_eq!(test("&lt;e>"), "<e>");
    assert_eq!(test("&lt;e>]]&gt;"), "<e>]]>");
    assert_eq!(test("&lt;e>]]&gt;&lt;/e&gt;"), "<e>]]></e>");
}

#[cfg(test)]
fn parse_test(s: &str) -> Result<Tree<()>> {
    let s = normalize_xml(s.to_string());
    parse_xml(&s, None, None)
}

#[test]
fn test_parse_empty() {
    assert!(parse_test("").is_err());
    //Error::SXXP(3)
}
#[test]
fn test_parse_undefined_element_prefix() {
    assert!(parse_test("<a:a/>").is_err());
    assert!(parse_test("<a><b:b/></a>").is_err());
    assert!(parse_test("<a><b:b xmlns:b='1'/><b:b/></a>").is_err());
}
#[test]
fn test_parse_undefined_attribute_prefix() {
    assert!(parse_test("<a a:a=''/>").is_err());
    assert!(parse_test("<a><b a:a=''/></a>").is_err());
    assert!(parse_test("<a><b:b xmlns:b='1'/><b b:b=''/></a>").is_err());
}
#[test]
fn test_parse_unclosed_tag() {
    assert!(parse_test("<a>").is_err());
    assert!(parse_test("<a><a/>").is_err());
}
#[test]
fn test_parse_unclosed_quote() {
    assert!(parse_test("<a a='>").is_err());
}
#[test]
fn test_parse_double_attribute() {
    assert!(parse_test("<a a='' a=''/>").is_err());
    assert!(parse_test("<a><b a='' a=''/></a>").is_err());
}
#[test]
fn test_parse_double_attribute_with_ns() {
    assert!(parse_test("<a xmlns:a='1' xmlns:b='1' a:a='' b:a=''/>").is_err());
    assert!(parse_test("<a xmlns:a='1' xmlns:b='1'><b a:a='' b:a=''/></a>").is_err());
}
#[test]
fn test_parse_unknown_entity() {
    assert!(parse_test("<a>&abc;</a>").is_err());
    assert!(parse_test("<a><b>&abc;</b></a>").is_err());
    assert!(parse_test("<a a='&abc;'/>").is_err());
}
#[test]
fn test_parse_wrong_close_tag() {
    assert!(parse_test("<a></b>").is_err());
    assert!(parse_test("<a><b></a></a>").is_err());
}
#[test]
fn test_parse_double_namespace() {
    assert!(parse_test("<a xmlns:a='1' xmlns:a='2'/>").is_err());
    assert!(parse_test("<a><b xmlns:a='1' xmlns:a='2'/></a>").is_err());
}
#[test]
fn test_parse_namespaces() {
    parse_test("<a xmlns:a='1' xmlns:b='2'/>").unwrap();
    parse_test("<a><b xmlns:a='1' xmlns:b='2'/></a>").unwrap();
}
#[test]
fn test_reserved_namespaces() {
    assert!(parse_test("<a xmlns:xmlns='1'/>").is_err());
    assert!(parse_test("<a xmlns:xml='1'/>").is_err());
}
#[test]
fn test_text_before() {
    assert!(parse_test("a<a/>").is_err());
}
#[test]
fn test_text_after() {
    assert!(parse_test("<a/>a").is_err());
}

#[test]
fn test_text_weird() {
    assert!(parse_test("<xmlns:a/>").is_err());
}
/*
#[test]
fn test_text_weird2() {
    assert!(parse_test("<xml:a/>").is_err());
}
*/
#[test]
fn test_xsl_stylesheet_pi() {
    parse_test("<?xml-stylesheet href=\"test.xsl\" type=\"text/xsl\" ?>\n<a/>").unwrap();
}
