mod dtd;
#[macro_use]
pub mod error;
pub mod read;
pub mod write;
pub use dtd::{is_name_char, name, ncname};

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum XmlVersion {
    Xml10,
    Xml11,
}
