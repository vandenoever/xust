use super::{XmlOutputParameters, XmlWriter};
use std::fmt::Write;
use xust_tree::{
    node::{Element, NodeKind, Ref},
    qnames::QNameCollection,
    stream::{Item, ItemStream, Status},
    typed_value::XS_UNTYPED_ATOMIC,
};

pub struct StreamWriter<W: Write> {
    w: XmlWriter<W>,
    error: Option<crate::error::Error>,
}

impl<W: Write> StreamWriter<W> {
    pub fn new(writer: W, config: XmlOutputParameters, qnames: QNameCollection) -> Self {
        let w = XmlWriter::new(writer, config, qnames);
        Self { w, error: None }
    }
    pub fn write_element<T: Ref>(&mut self, element: &Element<T>) {
        let qname = self.w.qnames.add_qname_by_ref(element.node_name());
        self.next(Item::StartElement {
            qname,
            r#type: element.node().type_id().unwrap(),
            namespace_definitions: &[],
        });
        for a in element.attributes() {
            let qname = self.w.qnames.add_qname_by_ref(a.node_name());
            self.next(Item::Attribute {
                qname,
                r#type: XS_UNTYPED_ATOMIC,
                value: &a.string_value(),
            });
        }
        for c in element.children() {
            if let Some(element) = c.element() {
                self.write_element(&element);
            } else if c.node_kind() == NodeKind::Text {
                self.write_str(&c.string_value());
            }
        }
        self.next(Item::EndElement);
    }
}

impl<W: Write> ItemStream for StreamWriter<W> {
    fn next(&mut self, item: Item) {
        if let Err(e) = super::write_item(&mut self.w, item) {
            self.error = Some(e);
        }
    }
    fn write_str(&mut self, s: &str) {
        self.next(Item::String(s));
    }
    fn status(&self) -> Status {
        if self.error.is_some() {
            Status::Error
        } else {
            Status::Done
        }
    }
}
