#![allow(clippy::upper_case_acronyms)]
pub(crate) mod entities;
mod fsm;
pub mod validator;

use crate::{
    err,
    error::Result,
    read::{EntityLoader, EntityLoaderResult},
    XmlVersion,
};
use entities::{Entities, UnparsedEntities};
use lazy_static::lazy_static;
use nom::{
    branch::alt,
    bytes::complete::{tag, take_till, take_till1, take_until, take_while},
    character::complete::{
        alpha1, char, digit1, hex_digit1, multispace0, multispace1, one_of, satisfy,
    },
    combinator::{map, map_res, opt, recognize},
    error::ErrorKind,
    multi::{fold_many0, many0, separated_list1},
    sequence::{delimited, pair, preceded, terminated, tuple},
    IResult, InputTakeAtPosition,
};
use regex::Regex;
use std::borrow::Cow;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use validator::Validator;

#[derive(Debug)]
pub struct DtdElements {
    elements: Vec<ElementDecl>,
    attributes: Vec<AttlistDecl>,
}

impl DtdElements {
    fn new() -> Self {
        Self {
            elements: Vec::new(),
            attributes: Vec::new(),
        }
    }
}

enum PE {
    Local(EntityValue, bool, Origin),
    System(ExternalId),
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub(crate) enum Origin {
    Internal,
    External,
}

enum GE {
    System(ExternalId),
    Entity(EntityValue, bool, Origin),
    Notation(Name, Origin),
}

pub(crate) struct Builder<'a> {
    document_element: Name,
    base: Option<PathBuf>,
    entity_loader: Option<&'a EntityLoader>,
    pe: Vec<(Name, PE)>,
    ge: Vec<(Name, GE)>,
    notations: Vec<(Name, ExternalId)>,
    dtd: DtdElements,
    xml_version: XmlVersion,
}

impl<'a> Builder<'a> {
    fn add(&mut self, md: MarkupDecl, origin: Origin, base: Option<&Path>) -> Result<()> {
        let internal = origin == Origin::Internal;
        match md {
            MarkupDecl::ElementDecl(ed) => {
                let ed = expand_character_refs(Cow::Borrowed(ed))?;
                check_bracket_symmetry(&ed)?;
                let ed = if internal {
                    ed
                } else {
                    expand_pe(ed, self, true)?
                };
                let ed = element_decl(&ed, internal).map_err(|e| err!("'{}' {}", ed, e))?;
                if !ed.0.is_empty() {
                    return Err(err!("Parsing of element leaves data."));
                }
                self.dtd.elements.push(ed.1)
            }
            MarkupDecl::AttlistDecl(ad) => {
                let ad = expand_character_refs(Cow::Borrowed(ad))?;
                let ad = if internal {
                    ad
                } else {
                    expand_pe(ad, self, true)?
                };
                let (left, mut ad) =
                    attlist_decl(&ad, internal).map_err(|e| err!("'{}' {:?}", ad, e))?;
                if !left.is_empty() {
                    return Err(err!("Parsing of attlist leaves data."));
                }
                // expand entities
                for att in &mut ad.atts {
                    let d = std::mem::replace(&mut att.default_decl, DefaultDecl::Implied);
                    att.default_decl = match d {
                        DefaultDecl::Implied => DefaultDecl::Implied,
                        DefaultDecl::Required => DefaultDecl::Required,
                        DefaultDecl::Fixed(v) => DefaultDecl::Fixed(AttValue(expand_ge(
                            v.0,
                            self,
                            self.ge.len(),
                            !internal,
                            0,
                        )?)),
                        DefaultDecl::Default(v) => DefaultDecl::Default(AttValue(expand_ge(
                            v.0,
                            self,
                            self.ge.len(),
                            !internal,
                            0,
                        )?)),
                    };
                }
                self.dtd.attributes.push(ad)
            }
            MarkupDecl::PEDecl(pe) => {
                if !self.pe.iter().any(|(n, _)| *n == pe.name) {
                    match pe.def {
                        PEDef::EntityValue(v) => {
                            if internal && v.0.contains('%') {
                                return Err(err!(
                                    "% is not allowed in interal entity {}",
                                    pe.name.0
                                ));
                            }
                            check_character_validity(self.xml_version, &v.0)?;
                            let v = expand_character_refs(Cow::Owned(v.0))?.to_string();
                            check_entity_references(&v)?;
                            check_parameter_references(&v)?;
                            check_bracket_symmetry(&v)?;
                            self.add_pe(pe.name, EntityValue(v), origin);
                        }
                        PEDef::ExternalId(ext) => {
                            let ext = rebase_ext(ext, base);
                            self.pe.push((pe.name, PE::System(ext)));
                        }
                    }
                }
            }
            MarkupDecl::GEDecl(ge) => {
                if !self.ge.iter().any(|(n, _)| *n == ge.name) {
                    match ge.def {
                        EntityDef::EntityValue(v) => {
                            let mut v: Cow<str> = Cow::Owned(v.0);
                            if internal {
                                if v.contains('%') {
                                    return Err(err!("% is not allowed in an interal entity"));
                                }
                            } else {
                                v = expand_pe(v, self, false)?;
                            }
                            check_character_validity(self.xml_version, &v)?;
                            let v = expand_character_refs(v)?.to_string();
                            check_entity_references(&v)?;
                            self.add_ge(
                                ge.name,
                                EntityValue(v),
                                if internal {
                                    Origin::Internal
                                } else {
                                    Origin::External
                                },
                            );
                        }
                        EntityDef::External(ext, ndata) => {
                            self.add_ext_ge(ge.name, ext, ndata);
                        }
                    }
                }
            }
            MarkupDecl::NotationDecl(nd, external_id) => {
                self.notations.push((Name(nd), external_id));
            }
            MarkupDecl::Comment(c) => {
                check_character_validity(self.xml_version, c)?;
            }
            MarkupDecl::PI { content, .. } => {
                check_character_validity(self.xml_version, content)?;
            }
            MarkupDecl::PEReference(pe_ref) => {
                let (pe, origin, pe_base) = get_pe(&pe_ref, self, false)?;
                if origin == Origin::Internal {
                    let (left, int_subset) = int_subset(&pe).map_err(|e| err!("{}", e))?;
                    if !left.is_empty() {
                        return Err(err!(
                            "Could not parse pe {} completely. left: '{}'.",
                            pe_ref,
                            left
                        ));
                    }
                    for d in int_subset {
                        self.add(d, origin, pe_base.as_deref())?;
                    }
                } else {
                    ext_subset(self, &pe, pe_base.as_deref()).map_err(|e| err!("{}", e))?;
                }
            }
        }
        Ok(())
    }
    fn add_pe(&mut self, name: Name, value: EntityValue, origin: Origin) {
        self.pe.push((name, PE::Local(value, false, origin)));
    }
    fn add_ge(&mut self, name: Name, value: EntityValue, origin: Origin) {
        self.ge.push((name, GE::Entity(value, false, origin)));
    }
    fn add_ext_ge(&mut self, name: Name, ext: ExternalId, ndata: Option<Name>) {
        if let Some(ndata) = ndata {
            self.ge.push((name, GE::Notation(ndata, Origin::External)));
        } else {
            self.ge.push((name, GE::System(ext)));
        }
    }
    fn load_ext(&mut self, ext: &ExternalId) -> Result<Option<(XmlVersion, EntityLoaderResult)>> {
        load_ext(self.entity_loader, &self.base, self.xml_version, ext)
    }
    fn parse_ext(&mut self, ext: &ExternalId) -> Result<()> {
        let load_result = self.load_ext(ext)?;
        if let Some((_, r)) = load_result {
            ext_subset(self, r.text.as_str(), r.base.as_deref()).map_err(|e| err!("{}", e))?;
        }
        Ok(())
    }
    pub fn from_xml<'b>(
        mut text: &'b str,
        base: Option<PathBuf>,
        entity_loader: Option<&'a EntityLoader>,
    ) -> Result<BuilderNewResult<'b, 'a>> {
        // remove bom
        if text.starts_with('\u{feff}') {
            text = &text[3..];
        }
        let (left, prolog) = prolog(text).map_err(|e| err!("{}", e))?;
        let (xml_version, standalone, dtd) = {
            let xml_version = prolog
                .xml_decl
                .as_ref()
                .map(|x| x.version_info)
                .unwrap_or(XmlVersion::Xml10);
            let standalone = prolog.xml_decl.map(|x| x.standalone).unwrap_or(false);
            check_chars(xml_version, &prolog.pis_and_comments)?;
            (xml_version, standalone, prolog.doctypedecl)
        };
        if left.starts_with("<?") {
            return Err(err!("Invalid PI"));
        }
        if left.starts_with("<!--") {
            return Err(err!("Invalid comment"));
        }
        let builder = if let Some(dtd) = dtd {
            let mut builder = Self {
                document_element: dtd.name,
                base,
                entity_loader,
                pe: Vec::new(),
                ge: Vec::new(),
                notations: Vec::new(),
                dtd: DtdElements::new(),
                xml_version,
            };
            for d in dtd.int_subset {
                builder.add(d, Origin::Internal, None)?;
            }
            if let Some(external_id) = dtd.external_id {
                builder.parse_ext(&external_id)?;
            }
            Some(builder)
        } else {
            None
        };
        Ok(BuilderNewResult {
            left,
            xml_version,
            standalone,
            pis_and_comments: prolog.pis_and_comments,
            builder,
        })
    }
    fn build(
        mut self,
        create_validator: bool,
        standalone: bool,
    ) -> Result<(Entities, UnparsedEntities, Option<Validator>)> {
        // check all notations used in entities
        for ge in &self.ge {
            if let GE::Notation(notation, _) = &ge.1 {
                if let Some(_ndata) = self.notations.iter().find(|n| n.0 == *notation) {
                } else if self.entity_loader.is_some() {
                    return Err(err!("NOTATION {} is not defined.", notation.0));
                }
            }
        }
        let mut entities = HashMap::new();
        let mut unparsed_entities = HashMap::new();
        let mut system_entities = HashMap::new();
        for e in self.ge.into_iter() {
            match e.1 {
                GE::System(ext) => {
                    let v = match load_ext(self.entity_loader, &self.base, self.xml_version, &ext) {
                        Ok(Some(a)) => Some(Ok((a.1.text.take(), Origin::External))),
                        Err(e) => Some(Err(e.to_string())),
                        Ok(None) => None,
                    };
                    if let Some(v) = v {
                        system_entities.insert((e.0).0, v);
                    }
                }
                GE::Entity(ge, _, origin) => {
                    entities.insert((e.0).0, (ge.0, origin));
                }
                GE::Notation(name, origin) => {
                    unparsed_entities.insert(e.0 .0, (name.0, origin));
                }
            }
        }
        let unparsed_entities = UnparsedEntities::new(unparsed_entities);
        let validator = if create_validator {
            self.dtd
                .elements
                .sort_unstable_by(|a, b| a.name.cmp(&b.name));
            self.dtd
                .attributes
                .sort_unstable_by(|a, b| a.name.cmp(&b.name));
            for s in self.dtd.elements.windows(2) {
                let (a, b) = (&s[0], &s[1]);
                if a.name == b.name {
                    return Err(err!("Multiple definitions for element {}.", a.name.0));
                }
            }
            let r = fsm::build(&self.document_element, self.dtd, standalone)?;
            r.check_notation_attributes(&self.notations)?;
            r.check_default_attributes()?;
            Some(r)
        } else {
            None
        };
        Ok((
            Entities::new(entities, system_entities),
            unparsed_entities,
            validator,
        ))
    }
}
fn load_ext(
    entity_loader: Option<&EntityLoader>,
    base: &Option<PathBuf>,
    xml_version: XmlVersion,
    ext: &ExternalId,
) -> Result<Option<(XmlVersion, EntityLoaderResult)>> {
    let file = match ext {
        ExternalId::System(s) => s,
        ExternalId::Public { system, public } => {
            if public == "-//W3C//DTD XHTML 1.0 Strict//EN" {
                "dtds/xhtml1-strict.dtd"
            } else {
                system
            }
        }
    };
    let dtd = match entity_loader {
        None => None,
        Some(entity_loader) => {
            let path = PathBuf::from(&file);
            let mut result = entity_loader(base.as_deref(), &path, xml_version)?;
            let version_info = if let Ok((i, td)) = text_decl(result.text.as_str()) {
                if xml_version == XmlVersion::Xml10 && td.version_info == Some(XmlVersion::Xml11) {
                    return Err(err!("External PE has later version number"));
                }
                let text_decl_len = result.text.len() - i.len();
                result.text.0.replace_range(..text_decl_len, "");
                td.version_info
            } else {
                None
            };
            result.base = Some(path);
            let version_info = version_info.unwrap_or(XmlVersion::Xml10);
            check_character_validity(xml_version, result.text.as_str())?;
            Some((version_info, result))
        }
    };
    Ok(dtd)
}

fn rebase_ext(ext: ExternalId, base: Option<&Path>) -> ExternalId {
    match ext {
        ExternalId::System(s) => ExternalId::System(rebase(s, base)),
        ExternalId::Public { public, system } => ExternalId::Public {
            public,
            system: rebase(system, base),
        },
    }
}

fn rebase(ext: String, base: Option<&Path>) -> String {
    if let Some(ext) = base
        .and_then(|b| b.parent())
        .map(|b| b.join(&ext))
        .and_then(|e| e.to_str().map(|s| s.to_string()))
    {
        ext
    } else {
        ext
    }
}

pub(crate) fn check_character_validity(xml_version: XmlVersion, input: &str) -> Result<()> {
    let f = if xml_version == XmlVersion::Xml10 {
        is_xml_1_0_char
    } else {
        is_xml_1_1_char
    };
    if !input.chars().all(f) {
        let c = input.chars().find(|c| !f(*c)).unwrap();
        return Err(err!("Found an invalid character {:#X}.", c as u32));
    }
    Ok(())
}

fn check_chars(xml_version: XmlVersion, v: &[PIOrComment]) -> Result<()> {
    for v in v {
        match v {
            PIOrComment::Comment(c) => check_character_validity(xml_version, c)?,
            PIOrComment::PI { content, .. } => check_character_validity(xml_version, content)?,
        }
    }
    Ok(())
}

fn find_at(s: &str, pos: usize, needle: &str) -> Option<usize> {
    s[pos..].find(needle).map(|p| p + pos)
}

fn expand_character_refs(val: Cow<str>) -> Result<Cow<str>> {
    let mut p = val.find("&#");
    if p.is_none() {
        return Ok(val);
    }
    let mut val = val.to_string();
    while let Some(pos) = p {
        let v = &val[pos + 2..];
        let (i, c, end) = if let Some(stripped) = v.strip_prefix('x') {
            if let Ok((i, n)) = hex_digit1::<_, ()>(stripped) {
                (i, u32::from_str_radix(n, 16)?, pos + 4 + n.len())
            } else {
                return Err(err!("invalid hex char ref"));
            }
        } else if let Ok((i, n)) = digit1::<_, ()>(v) {
            (i, u32::from_str(n)?, pos + 3 + n.len())
        } else {
            return Err(err!("invalid char ref"));
        };
        if !i.starts_with(';') {
            return Err(err!("missing ;"));
        }
        let c = if let Some(c) = std::char::from_u32(c) {
            c
        } else {
            return Err(err!("Invalid char {}", c));
        };
        val.replace_range(pos..end, &format!("{}", c));
        p = find_at(&val, pos + c.len_utf8(), "&#");
    }
    Ok(Cow::Owned(val))
}

fn get_pe(name: &str, b: &mut Builder, expand: bool) -> Result<(String, Origin, Option<PathBuf>)> {
    if let Some(pos) = b.pe.iter().position(|(n, _)| n.0 == name) {
        let mut this_pe = std::mem::replace(
            &mut b.pe[pos].1,
            PE::Local(EntityValue(String::new()), false, Origin::Internal),
        );
        let r;
        match this_pe {
            PE::System(ext) => {
                let (value, base) = if let Some((_version, r)) = b.load_ext(&ext)? {
                    (r.text, r.base)
                } else {
                    (Default::default(), None)
                };
                let value = if expand {
                    expand_pe(Cow::Owned(value.take()), b, false)?.to_string()
                } else {
                    value.take()
                };
                r = (value.clone(), Origin::External, base);
                this_pe = PE::Local(EntityValue(value), true, Origin::External);
            }
            PE::Local(ref p, expanded, origin) => {
                if !expanded {
                    let value = expand_pe(Cow::Borrowed(&p.0), b, false)?.to_string();
                    r = (value.clone(), Origin::Internal, None);
                    this_pe = PE::Local(EntityValue(value), true, origin);
                } else {
                    r = (p.0.clone(), origin, None);
                }
            }
        }
        b.pe[pos].1 = this_pe;
        Ok(r)
    } else {
        Err(err!("missing definition '{}'", name))
    }
}

fn expand_pe<'a>(val: Cow<'a, str>, b: &mut Builder, pad: bool) -> Result<Cow<'a, str>> {
    lazy_static! {
        static ref REGEX: Regex = Regex::new("\"[^\"]+\"|%").unwrap();
    }
    let mut p = REGEX.find(&val);
    if p.is_none() {
        return Ok(val);
    }
    let mut val = val.to_string();
    while let Some(m) = p {
        let pos = if m.as_str().len() > 1 {
            // do not expand inside quoted text
            m.end()
        } else {
            let pos = m.start();
            let name = get_reference(&val[pos + 1..])?;
            let end = name.len();
            let replacement = get_pe(name, b, false)?.0;
            if pad {
                val.replace_range(pos..pos + 1, " ");
                val.replace_range(pos + end + 1..pos + end + 2, " ");
                val.replace_range(pos + 1..pos + end + 1, &replacement);
            } else {
                val.replace_range(pos..pos + end + 2, &replacement);
            }
            pos
        };
        p = REGEX.find_at(&val, pos);
    }
    Ok(Cow::Owned(val))
}

fn get_ge<'a>(
    name: &str,
    b: &'a mut Builder,
    ge_end: usize,
    external_allowed: bool,
    depth: usize,
) -> Result<&'a str> {
    if let Some(pos) = b.ge[..ge_end].iter().position(|(n, _)| n.0 == name) {
        if let GE::System(ext) = &b.ge[pos].1 {
            if !external_allowed {
                return Err(err!("No external ge allowed here."));
            }
            let ext = ext.clone();
            let value = b.load_ext(&ext)?.map(|v| v.1.text).unwrap_or_default();
            b.ge[pos].1 = GE::Entity(EntityValue(value.take()), true, Origin::External);
        }
        let mut this_ge = std::mem::replace(
            &mut b.ge[pos].1,
            GE::Entity(EntityValue(String::new()), false, Origin::External),
        );
        if let GE::Entity(ref g, expanded, origin) = this_ge {
            if !external_allowed && origin == Origin::External {
                return Err(err!("No external ge allowed here."));
            }
            if !expanded {
                let value = expand_ge(g.0.clone(), b, pos, true, depth)?;
                this_ge = GE::Entity(EntityValue(value), true, origin);
            }
        }
        b.ge[pos].1 = this_ge;
        Ok(match &b.ge[pos].1 {
            GE::System(_ext) => panic!(),
            GE::Notation(_, _) => {
                return Err(err!("WFC: Parsed Entity {}", name));
            }
            GE::Entity(g, _, _) => &g.0,
        })
    } else {
        Err(err!("missing definition '{}'", name))
    }
}

// expand enitites in DefaultDecl::Fixed and DefaultDecl::Default
fn expand_ge(
    mut val: String,
    b: &mut Builder,
    ge_end: usize,
    external_allowed: bool,
    depth: usize,
) -> Result<String> {
    if depth > 100 {
        return Err(err!("Too many entity expansions"));
    }
    let mut p = 0;
    while let Some(pos) = val[p..].find('&') {
        let pos = p + pos;
        let name = get_reference(&val[pos + 1..])?;
        match get_ge(name, b, ge_end, external_allowed, depth + 1) {
            Ok(replacement) => {
                let end = pos + name.len() + 2;
                val.replace_range(pos..end, replacement);
                if replacement.len() < 3 {
                    // replacement cannot contain a new entity ref
                    p = pos + replacement.len();
                } else {
                    p = pos;
                }
            }
            Err(e) => {
                return Err(err!("Unknown entity {} {:?}", name, e));
            }
        }
    }
    Ok(val)
}

fn get_reference(ref_start: &str) -> Result<&str> {
    let name = if let Ok((i, name)) = ncname(ref_start) {
        if !i.starts_with(';') {
            return Err(err!("missing ';'"));
        }
        name
    } else {
        return Err(err!("Invalid reference '{}'", ref_start));
    };
    Ok(name)
}

fn check_entity_references(val: &str) -> Result<()> {
    let mut p = 0;
    while let Some(pos) = val[p..].find('&') {
        let pos = p + pos;
        let ref_start = &val[pos + 1..];
        if !ref_start.starts_with('#') {
            get_reference(ref_start)?;
        }
        p = pos + 1;
    }
    Ok(())
}

fn check_parameter_references(val: &str) -> Result<()> {
    let mut p = 0;
    while let Some(pos) = val[p..].find('%') {
        p += pos + 1;
        get_reference(&val[p..])?;
    }
    Ok(())
}

fn check_bracket_symmetry(s: &str) -> Result<()> {
    let mut open = 0;
    for c in s.chars() {
        if c == '(' {
            open += 1;
        } else if c == ')' {
            if open == 0 {
                return Err(err!("Brackets are not symmetric"));
            }
            open -= 1;
        }
    }
    if open != 0 {
        Err(err!("Brackets are not symmetric"))
    } else {
        Ok(())
    }
}

type B<'a> = Builder<'a>;

#[derive(Debug)]
struct DocTypeDecl<'a> {
    name: Name,
    external_id: Option<ExternalId>,
    int_subset: Vec<MarkupDecl<'a>>,
}

#[derive(Debug, Clone)]
pub enum ExternalId {
    System(String),
    Public { public: String, system: String },
}

#[derive(Debug)]
struct ExtSubset<'a> {
    #[allow(dead_code)]
    text_decl: Option<TextDecl>,
    #[allow(dead_code)]
    decl: Vec<MarkupDecl<'a>>,
}

#[derive(Debug)]
pub(crate) struct EncodingDecl(String);

#[derive(Debug)]
pub(crate) struct XmlDecl {
    version_info: XmlVersion,
    #[allow(dead_code)]
    pub encoding_decl: Option<EncodingDecl>,
    standalone: bool,
}

#[derive(Debug)]
struct TextDecl {
    version_info: Option<XmlVersion>,
    #[allow(dead_code)]
    encoding_decl: EncodingDecl,
}

#[derive(Debug, Clone)]
pub(crate) enum PIOrComment<'a> {
    PI { target: &'a str, content: &'a str },
    Comment(&'a str),
}

#[derive(Debug)]
enum MarkupDecl<'a> {
    ElementDecl(&'a str),
    AttlistDecl(&'a str),
    GEDecl(GEDecl),
    PEDecl(PEDecl),
    NotationDecl(String, ExternalId),
    PEReference(String),
    PI {
        #[allow(dead_code)]
        target: &'a str,
        content: &'a str,
    },
    Comment(&'a str),
}

#[derive(Debug)]
struct ElementDecl {
    name: Name,
    element_content: ElementContent,
    internal: bool,
}

#[derive(Debug)]
enum ElementContent {
    Empty,
    Any,
    Mixed(Vec<Name>),
    Children(Cp), // at this level, only seq and choice are allowed, but
                  // it's easier to work with the same type at all levels
}

#[derive(Debug)]
enum OccurrenceIndicator {
    One,        // ''
    ZeroOrOne,  // ?
    ZeroOrMore, // *
    OneOrMore,  // +
}

#[derive(Debug)]
enum NameChoiceOrSeq {
    Name(Name),
    Choice(Vec<Cp>),
    Seq(Vec<Cp>),
}

#[derive(Debug)]
struct Cp {
    // content particle
    child: NameChoiceOrSeq,
    occurrence_indicator: OccurrenceIndicator,
}

impl PartialEq for &Cp {
    fn eq(&self, _o: &&Cp) -> bool {
        // compare pointers, not content
        unimplemented!()
    }
}

#[derive(Debug)]
struct AttlistDecl {
    name: Name,
    atts: Vec<AttDef>,
}

#[derive(Debug)]
pub(crate) struct AttDef {
    name: Name,
    att_type: AttType,
    default_decl: DefaultDecl,
    internal: bool,
}

#[derive(Debug, PartialEq)]
pub(crate) enum AttType {
    CData,
    Id,
    IdRef,
    IdRefs,
    Entity,
    Entities,
    Nmtoken,
    Nmtokens,
    NotationType(Vec<Name>),
    Enumeration(Vec<Nmtoken>),
}

#[derive(Debug, PartialEq)]
pub(crate) enum DefaultDecl {
    Required,
    Implied,
    Fixed(AttValue),
    Default(AttValue),
}

#[derive(Debug)]
struct GEDecl {
    name: Name,
    def: EntityDef,
}

#[derive(Debug)]
enum EntityDef {
    EntityValue(EntityValue),
    External(ExternalId, Option<Name>),
}

#[derive(Debug)]
struct PEDecl {
    name: Name,
    def: PEDef,
}

#[derive(Debug)]
enum PEDef {
    EntityValue(EntityValue),
    ExternalId(ExternalId),
}

#[derive(Debug)]
struct EntityValue(String);

#[derive(Debug, PartialEq)]
pub(crate) struct AttValue(String);

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Name(String);

#[derive(Debug, PartialEq)]
pub(crate) struct Nmtoken(String);

// [2]   	Char	   ::=   	#x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
pub fn is_xml_1_0_char(c: char) -> bool {
    c == 0x9 as char
        || c == 0xA as char
        || c == 0xD as char
        || (c >= 0x20 as char && c <= '\u{D7FF}')
        || ('\u{E000}'..='\u{FFFD}').contains(&c)
        || ('\u{10000}'..='\u{10FFFF}').contains(&c)
}
pub fn is_xml_1_1_char(c: char) -> bool {
    (c >= 0x9 as char && c <= 0xA as char)
        || c == 0xD as char
        || (c >= 0x20 as char && c <= 0x7E as char)
        || c == 0x85 as char
        || (c >= 0xA0 as char && c <= '\u{D7FF}')
        || ('\u{E000}'..='\u{FFFD}').contains(&c)
        || ('\u{10000}'..='\u{10FFFF}').contains(&c)
}
pub fn is_unrestricted_xml_1_1_char(c: char) -> bool {
    (c >= 0x1 as char && c <= '\u{D7FF}')
        || ('\u{E000}'..='\u{FFFD}').contains(&c)
        || ('\u{10000}'..='\u{10FFFF}').contains(&c)
}

// [5] Name	   ::=   	NameStartChar (NameChar)*
/**
 * This function is exported for use in xust_xsd.
 */
pub fn name(i: &str) -> IResult<&str, &str> {
    recognize(pair(satisfy(is_name_start_char), take_while(is_name_char)))(i)
}

// [9] EntityValue	   ::=   	'"' ([^%&"] | PEReference | Reference)* '"'
//			|  "'" ([^%&'] | PEReference | Reference)* "'"
fn entity_value(i: &str) -> IResult<&str, &str> {
    alt((
        delimited(char('"'), take_while(|c| c != '"'), char('"')),
        delimited(char('\''), take_while(|c| c != '\''), char('\'')),
    ))(i)
}

// [10]   	AttValue	   ::=   	'"' ([^<&"] | Reference)* '"'
//			|  "'" ([^<&'] | Reference)* "'"
fn att_value(i: &str) -> IResult<&str, &str> {
    alt((
        delimited(char('"'), take_while(|c| c != '"'), char('"')),
        delimited(char('\''), take_while(|c| c != '\''), char('\'')),
    ))(i)
}

// [11]   	SystemLiteral	   ::=   	('"' [^"]* '"') | ("'" [^']* "'")
fn system_literal(i: &str) -> IResult<&str, &str> {
    alt((
        delimited(char('"'), take_while(|c| c != '"'), char('"')),
        delimited(char('\''), take_while(|c| c != '\''), char('\'')),
    ))(i)
}
// [12]   	PubidLiteral	   ::=   	'"' PubidChar* '"' | "'" (PubidChar - "'")* "'"
// [13]   	PubidChar	   ::=   	#x20 | #xD | #xA | [a-zA-Z0-9] | [-'()+,./:=?;!*#@$_%]
fn pubid_literal(i: &str) -> IResult<&str, &str> {
    alt((
        delimited(char('"'), take_while(is_pubid_char), char('"')),
        delimited(
            char('\''),
            take_while(is_pubid_char_no_single_quote),
            char('\''),
        ),
    ))(i)
}
fn is_pubid_char(c: char) -> bool {
    c == ' '
        || c == 0xd as char
        || c == 0xa as char
        || c.is_ascii_alphanumeric()
        || "-'()+,./:=?;!*#@$_%".contains(c)
}

fn is_pubid_char_no_single_quote(c: char) -> bool {
    is_pubid_char(c) && c != '\''
}

// [15]   	Comment	   ::=   	'<!--' ((Char - '-') | ('-' (Char - '-')))* '-->'
fn comment(i: &str) -> IResult<&str, &str> {
    delimited(
        tag("<!--"),
        recognize(tuple((
            take_till(|c| c == '-'),
            fold_many0(pair(char('-'), take_till1(|c| c == '-')), || (), |_, _| ()),
        ))),
        tag("-->"),
    )(i)
}

// [16]   	PI	   ::=   	'<?' PITarget (S (Char* - (Char* '?>' Char*)))? '?>'
fn pi(i: &str) -> IResult<&str, (&str, &str)> {
    map(
        tuple((
            tag("<?"),
            pi_target,
            opt(tuple((multispace1, take_until("?>")))),
            tag("?>"),
        )),
        |t| (t.1, t.2.map(|t| t.1).unwrap_or("")),
    )(i)
}

// [17]   	PITarget	   ::=   	Name - (('X' | 'x') ('M' | 'm') ('L' | 'l'))
fn pi_target(i: &str) -> IResult<&str, &str> {
    // The specification allows colons in PITarget, but xmlstarlet/libxml2 and
    // test rmt-ns10-042 from the XML Test Suite disallow it.
    map_res(ncname, |s: &str| {
        if "xml".eq_ignore_ascii_case(s) {
            Err(nom::Err::Error((i, ErrorKind::IsNot)))
        } else {
            Ok(s)
        }
    })(i)
}

struct Prolog<'a> {
    xml_decl: Option<XmlDecl>,
    doctypedecl: Option<DocTypeDecl<'a>>,
    pis_and_comments: Vec<PIOrComment<'a>>,
}

// [22]   	prolog	   ::=   	XMLDecl Misc* (doctypedecl Misc*)?
// in XML 1.0, the xml declaration is optional
fn prolog(i: &str) -> IResult<&str, Prolog> {
    let (i, xml_decl) = if let Ok((i, xml_decl)) = xml_decl(i) {
        (i, Some(xml_decl))
    } else {
        (i, None)
    };
    let (i, mut misc1) = miscs(i)?;
    let (i, doctypedecl) = if i.starts_with("<!") {
        let (i, doctypedecl) = doctypedecl(i)?;
        (i, Some(doctypedecl))
    } else {
        (i, None)
    };
    let (i, mut misc2) = miscs(i)?;
    misc1.append(&mut misc2);
    Ok((
        i,
        Prolog {
            xml_decl,
            doctypedecl,
            pis_and_comments: misc1,
        },
    ))
}

// [23]   	XMLDecl	   ::=   	'<?xml' VersionInfo EncodingDecl? SDDecl? S? '?>'
pub(crate) fn xml_decl(i: &str) -> IResult<&str, XmlDecl> {
    map(
        tuple((
            tag("<?xml"),
            version_info,
            opt(encoding_decl),
            opt(sd_decl),
            multispace0,
            tag("?>"),
        )),
        |t| XmlDecl {
            version_info: t.1,
            encoding_decl: t.2.map(|t| EncodingDecl(String::from(t))),
            standalone: t.3.unwrap_or_default(),
        },
    )(i)
}

// [24]   	VersionInfo	   ::=   	S 'version' Eq ("'" VersionNum "'" | '"' VersionNum '"')
fn version_info(i: &str) -> IResult<&str, XmlVersion> {
    let i = tuple((multispace1, tag("version"), eq))(i)?.0;
    let single_quote = i.starts_with('\'');
    let i = if single_quote {
        &i[1..]
    } else {
        char('"')(i)?.0
    };
    let i = tag("1.")(i)?.0;
    let (i, version) = map_res(digit1, |digit| {
        if let Ok(v) = u8::from_str(digit) {
            if v == 1 {
                Ok(XmlVersion::Xml11)
            } else {
                Ok(XmlVersion::Xml10)
            }
        } else {
            Err(nom::Err::Error((i, ErrorKind::Digit)))
        }
    })(i)?;
    let i = if single_quote {
        char('\'')(i)?.0
    } else {
        char('"')(i)?.0
    };
    Ok((i, version))
}

// [25]   	Eq	   ::=   	S? '=' S?
fn eq(i: &str) -> IResult<&str, ()> {
    map(tuple((multispace0, char('='), multispace0)), |_| ())(i)
}

// [27]   	Misc	   ::=   	Comment | PI | S
fn misc(i: &str) -> IResult<&str, Option<PIOrComment>> {
    alt((
        map(comment, |c| Some(PIOrComment::Comment(c))),
        map(pi, |(target, content)| {
            Some(PIOrComment::PI { target, content })
        }),
        map(multispace1, |_| None),
    ))(i)
}
fn miscs(i: &str) -> IResult<&str, Vec<PIOrComment>> {
    fold_many0(misc, Vec::new, |mut acc: Vec<_>, v| {
        if let Some(v) = v {
            acc.push(v);
        }
        acc
    })(i)
}

// [28]   	doctypedecl	   ::=   	'<!DOCTYPE' S Name (S ExternalId)? S? ('[' intSubset ']' S?)? '>'
fn doctypedecl(i: &str) -> IResult<&str, DocTypeDecl> {
    map(
        tuple((
            tag("<!DOCTYPE"),
            multispace1,
            name,
            opt(preceded(multispace1, external_id)),
            multispace0,
            opt(delimited(
                char('['),
                int_subset,
                pair(char(']'), multispace0),
            )),
            char('>'),
        )),
        |t| DocTypeDecl {
            name: Name(String::from(t.2)),
            external_id: t.3,
            int_subset: t.5.unwrap_or_default(),
        },
    )(i)
}
/*
// [28a]   	DeclSep	   ::=   	PEReference | S
fn decl_sep(i: &str) -> IResult<&str, Option<&str>> {
    alt((map(pe_reference, Some), map(multispace1, |_| None)))(i)
}
*/

// [28b]   	intSubset	   ::=   	(markupdecl | DeclSep)*
fn int_subset(i: &str) -> IResult<&str, Vec<MarkupDecl>> {
    preceded(multispace0, many0(terminated(markupdecl, multispace0)))(i)
}

// [29]   	markupdecl	   ::=   	elementdecl | AttlistDecl | EntityDecl | NotationDecl | PI | Comment
fn markupdecl(i: &str) -> IResult<&str, MarkupDecl> {
    alt((
        map(element_decl_raw, MarkupDecl::ElementDecl),
        map(attlist_decl_raw, MarkupDecl::AttlistDecl),
        map(pe_reference, |p| MarkupDecl::PEReference(p.to_string())),
        map(ge_decl, MarkupDecl::GEDecl),
        map(pe_decl, MarkupDecl::PEDecl),
        map(notation_decl, |(n, v)| {
            MarkupDecl::NotationDecl(n.to_string(), v)
        }),
        map(pi, |(target, content)| MarkupDecl::PI { target, content }),
        map(comment, MarkupDecl::Comment),
    ))(i)
}

// [30]   	extSubset	   ::=   	TextDecl? extSubsetDecl
fn ext_subset(b: &mut B, mut i: &str, base: Option<&Path>) -> Result<()> {
    while !i.is_empty() {
        i = ext_subset_decl(b, i, base)
            .map_err(|_| err!("Unparsed text at end of file: '{}'", i))?;
    }
    Ok(())
}

// [31]   	extSubsetDecl	   ::=   	( markupdecl | conditionalSect | DeclSep)*
fn ext_subset_decl<'a>(b: &mut B, i: &'a str, base: Option<&Path>) -> Result<&'a str> {
    if let Ok((i, md)) = markupdecl(i) {
        b.add(md, Origin::External, base)?;
        Ok(i)
    } else if let Ok((i, pe_ref)) = pe_reference(i) {
        let pe_ref = get_pe(pe_ref, b, false)?.0;
        ext_subset(b, &pe_ref, base)?;
        Ok(i)
    } else if let Ok((i, ())) = conditional_sect(b, i, base) {
        Ok(i)
    } else {
        Ok(multispace1::<_, ()>(i).map_err(|e| err!("{}", e))?.0)
    }
}

// [32]   	SDDecl	   ::=   	S 'standalone' Eq (("'" ('yes' | 'no') "'") | ('"' ('yes' | 'no') '"'))
fn sd_decl(i: &str) -> IResult<&str, bool> {
    preceded(
        tuple((multispace1, tag("standalone"), eq)),
        alt((
            map(alt((tag("'yes'"), tag("\"yes\""))), |_| true),
            map(alt((tag("'no'"), tag("\"no\""))), |_| false),
        )),
    )(i)
}

// [45]   	elementdecl	   ::=   	'<!ELEMENT' S Name S contentspec S? '>'
fn element_decl(i: &str, internal: bool) -> IResult<&str, ElementDecl> {
    map(
        tuple((
            tag("<!ELEMENT"),
            multispace1,
            name,
            multispace1,
            content_spec,
            multispace0,
            char('>'),
        )),
        |t| ElementDecl {
            name: Name(String::from(t.2)),
            element_content: t.4,
            internal,
        },
    )(i)
}
fn element_decl_raw(i: &str) -> IResult<&str, &str> {
    recognize(tuple((
        tag("<!ELEMENT"),
        take_till1(|c| c == '>'),
        char('>'),
    )))(i)
}

// [46]   	contentspec	   ::=   	'EMPTY' | 'ANY' | Mixed | children
fn content_spec(i: &str) -> IResult<&str, ElementContent> {
    alt((
        map(tag("EMPTY"), |_| ElementContent::Empty),
        map(tag("ANY"), |_| ElementContent::Any),
        map(mixed, ElementContent::Mixed),
        map(children, ElementContent::Children),
    ))(i)
}

// [47]   	children	   ::=   	(choice | seq) ('?' | '*' | '+')?
fn children(i: &str) -> IResult<&str, Cp> {
    map(pair(choice_or_seq, occurrence_indicator), |t| Cp {
        child: t.0,
        occurrence_indicator: t.1,
    })(i)
}
fn choice_or_seq(i: &str) -> IResult<&str, NameChoiceOrSeq> {
    let r = alt((
        map(choice, NameChoiceOrSeq::Choice),
        map(seq, NameChoiceOrSeq::Seq),
    ))(i)?;
    Ok(r)
}
fn occurrence_indicator(i: &str) -> IResult<&str, OccurrenceIndicator> {
    map(
        opt(alt((
            map(char('?'), |_| OccurrenceIndicator::ZeroOrOne),
            map(char('*'), |_| OccurrenceIndicator::ZeroOrMore),
            map(char('+'), |_| OccurrenceIndicator::OneOrMore),
        ))),
        |o| o.unwrap_or(OccurrenceIndicator::One),
    )(i)
}

// [48]   	cp	   ::=   	(Name | choice | seq) ('?' | '*' | '+')?
fn cp(i: &str) -> IResult<&str, Cp> {
    map(
        pair(
            alt((
                map(name, |name| NameChoiceOrSeq::Name(Name(String::from(name)))),
                map(choice, NameChoiceOrSeq::Choice),
                map(seq, NameChoiceOrSeq::Seq),
            )),
            occurrence_indicator,
        ),
        |t| Cp {
            child: t.0,
            occurrence_indicator: t.1,
        },
    )(i)
}

fn vec_cp(i: &str, sep: char) -> IResult<&str, Vec<Cp>> {
    delimited(
        pair(char('('), multispace0),
        separated_list1(tuple((multispace0, char(sep), multispace0)), cp),
        pair(multispace0, char(')')),
    )(i)
}

// [49]   	choice	   ::=   	'(' S? cp ( S? '|' S? cp )+ S? ')'
fn choice(i: &str) -> IResult<&str, Vec<Cp>> {
    vec_cp(i, '|')
}

// [50]   	seq	   ::=   	'(' S? cp ( S? ',' S? cp )* S? ')'
fn seq(i: &str) -> IResult<&str, Vec<Cp>> {
    vec_cp(i, ',')
}

// [51]   	Mixed	   ::=   	'(' S? '#PCDATA' (S? '|' S? Name)* S? ')*'
//			| '(' S? '#PCDATA' S? ')'
fn mixed(i: &str) -> IResult<&str, Vec<Name>> {
    preceded(
        tuple((char('('), multispace0, tag("#PCDATA"), multispace0)),
        alt((
            terminated(
                many0(map(
                    tuple((char('|'), multispace0, name, multispace0)),
                    |t| Name(String::from(t.2)),
                )),
                tag(")*"),
            ),
            map(char(')'), |_| Vec::new()),
        )),
    )(i)
}

// [52]   	AttlistDecl	   ::=   	'<!ATTLIST' S Name AttDef* S? '>'
fn attlist_decl(i: &str, internal: bool) -> IResult<&str, AttlistDecl> {
    map(
        tuple((
            tag("<!ATTLIST"),
            multispace1,
            name,
            many0(|i| att_def(i, internal)),
            multispace0,
            char('>'),
        )),
        |t| AttlistDecl {
            name: Name(String::from(t.2)),
            atts: t.3,
        },
    )(i)
}
fn attlist_decl_raw(i: &str) -> IResult<&str, &str> {
    recognize(tuple((
        tag("<!ATTLIST"),
        take_till1(|c| c == '>'),
        char('>'),
    )))(i)
}

// [53]   	AttDef	   ::=   	S Name S AttType S DefaultDecl
fn att_def(i: &str, internal: bool) -> IResult<&str, AttDef> {
    map(
        tuple((
            multispace1,
            name,
            multispace1,
            att_type,
            multispace1,
            default_decl,
        )),
        |t| AttDef {
            name: Name(String::from(t.1)),
            att_type: t.3,
            default_decl: t.5,
            internal,
        },
    )(i)
}

// [54]   	AttType	   ::=   	StringType | TokenizedType | EnumeratedType
fn att_type(i: &str) -> IResult<&str, AttType> {
    alt((
        map(tag("CDATA"), |_| AttType::CData),
        map(tag("IDREFS"), |_| AttType::IdRefs),
        map(tag("IDREF"), |_| AttType::IdRef),
        map(tag("ID"), |_| AttType::Id),
        map(tag("ENTITY"), |_| AttType::Entity),
        map(tag("ENTITIES"), |_| AttType::Entities),
        map(tag("NMTOKENS"), |_| AttType::Nmtokens),
        map(tag("NMTOKEN"), |_| AttType::Nmtoken),
        notation_type,
        enumeration,
    ))(i)
}

// [58]   	NotationType	   ::=   	'NOTATION' S '(' S? Name (S? '|' S? Name)* S? ')'
fn notation_type(i: &str) -> IResult<&str, AttType> {
    map(
        delimited(
            tuple((tag("NOTATION"), multispace1, char('('), multispace0)),
            separated_list1(
                tuple((multispace0, char('|'), multispace0)),
                map(ncname, |v| Name(String::from(v))),
            ),
            pair(multispace0, char(')')),
        ),
        AttType::NotationType,
    )(i)
}

// [59]   	Enumeration	   ::=   	'(' S? Nmtoken (S? '|' S? Nmtoken)* S? ')'
fn enumeration(i: &str) -> IResult<&str, AttType> {
    map(
        delimited(
            pair(char('('), multispace0),
            separated_list1(
                tuple((multispace0, char('|'), multispace0)),
                map(nmtoken, |v| Nmtoken(String::from(v))),
            ),
            pair(multispace0, char(')')),
        ),
        AttType::Enumeration,
    )(i)
}

// [60]   	DefaultDecl	   ::=   	'#REQUIRED' | '#IMPLIED'
//			| (('#FIXED' S)? AttValue)
fn default_decl(i: &str) -> IResult<&str, DefaultDecl> {
    alt((
        map(tag("#REQUIRED"), |_| DefaultDecl::Required),
        map(tag("#IMPLIED"), |_| DefaultDecl::Implied),
        map(tuple((tag("#FIXED"), multispace1, att_value)), |t| {
            DefaultDecl::Fixed(AttValue(String::from(t.2)))
        }),
        map(att_value, |v| {
            DefaultDecl::Default(AttValue(String::from(v)))
        }),
    ))(i)
}

// [61] conditionalSect	   ::=   	includeSect | ignoreSect
fn conditional_sect<'a>(b: &mut B, i: &'a str, base: Option<&Path>) -> Result<(&'a str, ())> {
    let i = tag::<_, _, ()>("<![")(i).map_err(|e| err!("{}", e))?.0;
    let mut i = multispace0::<_, ()>(i).map_err(|e| err!("{}", e))?.0;
    let include = if let Ok((j, _)) = tag::<_, _, ()>("INCLUDE")(i) {
        i = j;
        true
    } else if let Ok((j, _)) = tag::<_, _, ()>("IGNORE")(i) {
        i = j;
        false
    } else {
        let (j, pe) = pe_reference(i).map_err(|e| err!("{}", e))?;
        let pe = get_pe(pe, b, false)?.0;
        let pe = pe.trim();
        i = j;
        if pe == "INCLUDE" {
            true
        } else if pe == "IGNORE" {
            false
        } else {
            return Err(err!(
                "Conditional expression should start with INCLUDE or IGNORE, not '{}'.",
                pe
            ));
        }
    };
    if include {
        include_sect(b, i, base)
    } else {
        ignore_sect(i).map_err(|e| err!("{}", e))
    }
}
// [62] includeSect	   ::=   	'<![' S? 'INCLUDE' S? '[' extSubsetDecl ']]>'
fn include_sect<'a>(b: &mut B, i: &'a str, base: Option<&Path>) -> Result<(&'a str, ())> {
    let mut i = tuple::<_, _, (), _>((multispace0, char('[')))(i)
        .map_err(|e| err!("{}", e))?
        .0;
    while let Ok(j) = ext_subset_decl(b, i, base) {
        i = j;
    }
    let i = tag::<_, _, ()>("]]>")(i).map_err(|e| err!("{}", e))?.0;
    Ok((i, ()))
}
// [63]   	ignoreSect	   ::=   	'<![' S? 'IGNORE' S? '[' ignoreSectContents* ']]>'
fn ignore_sect(i: &str) -> IResult<&str, ()> {
    preceded(tuple((multispace0, char('['))), ignore_sect_contents)(i)
}
// [64]   	ignoreSectContents	   ::=   	Ignore ('<![' ignoreSectContents ']]>' Ignore)*
fn ignore_sect_contents(mut i: &str) -> IResult<&str, ()> {
    let mut open = 1;
    while open > 0 {
        let j = take_until("]]>")(i)?.0;
        let k = &i[..i.len() - j.len()];
        if let Some(start) = k.find("<![") {
            i = &i[start + 3..];
            open += 1;
        } else {
            open -= 1;
            i = &j[3..];
        }
    }
    Ok((i, ()))
}

// [69]   	PEReference	   ::=   	'%' Name ';'
fn pe_reference(i: &str) -> IResult<&str, &str> {
    delimited(char('%'), ncname, char(';'))(i)
}

// [71]   	GEDecl	   ::=   	'<!ENTITY' S Name S EntityDef S? '>'
fn ge_decl(i: &str) -> IResult<&str, GEDecl> {
    map(
        tuple((
            tag("<!ENTITY"),
            multispace1,
            ncname,
            multispace1,
            entity_def,
            multispace0,
            char('>'),
        )),
        |t| GEDecl {
            name: Name(String::from(t.2)),
            def: t.4,
        },
    )(i)
}

// [72]   	PEDecl	   ::=   	'<!ENTITY' S '%' S Name S PEDef S? '>'
fn pe_decl(i: &str) -> IResult<&str, PEDecl> {
    map(
        tuple((
            tag("<!ENTITY"),
            multispace1,
            char('%'),
            multispace1,
            ncname,
            multispace1,
            pe_def,
            multispace0,
            char('>'),
        )),
        |t| PEDecl {
            name: Name(String::from(t.4)),
            def: t.6,
        },
    )(i)
}

// [73]   	EntityDef	   ::=   	EntityValue | (ExternalId NDataDecl?)
fn entity_def(i: &str) -> IResult<&str, EntityDef> {
    alt((
        map(entity_value, |v| {
            EntityDef::EntityValue(EntityValue(String::from(v)))
        }),
        map(pair(external_id, opt(n_data_decl)), |t| {
            EntityDef::External(t.0, t.1.map(|v| Name(String::from(v))))
        }),
    ))(i)
}

// [74]   	PEDef	   ::=   	EntityValue | ExternalId
fn pe_def(i: &str) -> IResult<&str, PEDef> {
    alt((
        map(entity_value, |v| {
            PEDef::EntityValue(EntityValue(String::from(v)))
        }),
        map(external_id, PEDef::ExternalId),
    ))(i)
}

// [75]   	ExternalId	   ::=   	'SYSTEM' S SystemLiteral
//			| 'PUBLIC' S PubidLiteral S SystemLiteral
fn external_id(i: &str) -> IResult<&str, ExternalId> {
    alt((
        map(tuple((tag("SYSTEM"), multispace1, system_literal)), |t| {
            ExternalId::System(String::from(t.2))
        }),
        map(
            tuple((
                tag("PUBLIC"),
                multispace1,
                pubid_literal,
                multispace1,
                system_literal,
            )),
            |t| ExternalId::Public {
                public: String::from(t.2),
                system: String::from(t.4),
            },
        ),
    ))(i)
}

// [76]   	NDataDecl	   ::=   	S 'NDATA' S Name
fn n_data_decl(i: &str) -> IResult<&str, &str> {
    preceded(tuple((multispace1, tag("NDATA"), multispace1)), ncname)(i)
}

// [77]   	TextDecl	   ::=   	'<?xml' VersionInfo? EncodingDecl S? '?>
fn text_decl(i: &str) -> IResult<&str, TextDecl> {
    map(
        tuple((
            tag("<?xml"),
            opt(version_info),
            encoding_decl,
            multispace0,
            tag("?>"),
        )),
        |t| TextDecl {
            version_info: t.1,
            encoding_decl: EncodingDecl(String::from(t.2)),
        },
    )(i)
}

// [80]   	EncodingDecl	   ::=   	S 'encoding' Eq ('"' EncName '"' | "'" EncName "'" )
fn encoding_decl(i: &str) -> IResult<&str, &str> {
    map(
        tuple((
            multispace1,
            tag("encoding"),
            eq,
            one_of("\"'"),
            enc_name,
            one_of("\"'"),
        )),
        |t| t.4,
    )(i)
}
// [81]   	EncName	   ::=   	[A-Za-z] ([A-Za-z0-9._] | '-')*
fn enc_name(i: &str) -> IResult<&str, &str> {
    recognize(tuple((
        alpha1,
        take_while(|c: char| c.is_ascii_alphanumeric() || c == '.' || c == '_' || c == '-'),
    )))(i)
}

// [82]   	NotationDecl	   ::=   	'<!NOTATION' S Name S (ExternalID | PublicID) S? '>'
fn notation_decl(i: &str) -> IResult<&str, (&str, ExternalId)> {
    map(
        tuple((
            tag("<!NOTATION"),
            multispace1,
            ncname,
            multispace1,
            alt((
                external_id,
                map(public_id, |system| ExternalId::System(system.into())),
            )),
            multispace0,
            char('>'),
        )),
        |t| (t.2, t.4),
    )(i)
}

// [83]   	PublicID	   ::=   	'PUBLIC' S PubidLiteral
fn public_id(i: &str) -> IResult<&str, &str> {
    preceded(tuple((tag("PUBLIC"), multispace1)), pubid_literal)(i)
}

fn is_ncname_start_char(c: char) -> bool {
    c.is_ascii_alphabetic()
        || c == '_'
        || (c >= 0xC0 as char && c <= '\u{2FF}' && c != 0xD7 as char && c != 0xF7 as char)
        || (('\u{370}'..='\u{1FFF}').contains(&c) && c != '\u{37E}')
        || ('\u{200C}'..='\u{200D}').contains(&c)
        || ('\u{2070}'..='\u{218F}').contains(&c)
        || ('\u{2C00}'..='\u{2FEF}').contains(&c)
        || ('\u{3001}'..='\u{D7FF}').contains(&c)
        || ('\u{F900}'..='\u{FDCF}').contains(&c)
        || ('\u{FDF0}'..='\u{FFFD}').contains(&c)
        || ('\u{10000}'..='\u{EFFFF}').contains(&c)
}

pub fn is_name_start_char(c: char) -> bool {
    c == ':' || is_ncname_start_char(c)
}

fn is_ncname_char(c: char) -> bool {
    is_ncname_start_char(c)
        || c == '-'
        || c == '.'
        || c.is_ascii_digit()
        || c == 0xb7 as char
        || ('\u{300}'..='\u{36F}').contains(&c)
        || c == '\u{203F}'
        || c == '\u{2040}'
}

/**
 * This function is exported for use in xust_xsd.
 */
pub fn is_name_char(c: char) -> bool {
    c == ':' || is_ncname_char(c)
}

/**
 * This function is exported for use in xust_xsd.
 */
pub fn ncname(i: &str) -> IResult<&str, &str> {
    recognize(pair(
        satisfy(is_ncname_start_char),
        take_while(is_ncname_char),
    ))(i)
}

fn nmtoken(i: &str) -> IResult<&str, &str> {
    i.split_at_position1_complete(|item| !is_name_char(item), ErrorKind::AlphaNumeric)
}

pub struct BuilderNewResult<'a, 'b> {
    left: &'a str,
    xml_version: XmlVersion,
    standalone: bool,
    pis_and_comments: Vec<PIOrComment<'a>>,
    builder: Option<Builder<'b>>,
}

pub struct Dtd<'a> {
    pub(crate) xml_version: XmlVersion,
    pub(crate) standalone: bool,
    pub(crate) entities: Entities,
    pub(crate) unparsed_entities: UnparsedEntities,
    pub(crate) validator: Option<Validator>,
    pub(crate) pis_and_comments: Vec<PIOrComment<'a>>,
}

pub fn parse_dtd<'a>(
    dtd: &'a str,
    base: Option<&Path>,
    entity_loader: Option<&EntityLoader>,
) -> Result<(&'a str, Dtd<'a>)> {
    let r: BuilderNewResult = Builder::from_xml(dtd, base.map(PathBuf::from), entity_loader)?;
    let (entities, unparsed_entities, validator) = if let Some(builder) = r.builder {
        builder.build(entity_loader.is_some(), r.standalone)?
    } else {
        (Entities::default(), UnparsedEntities::default(), None)
    };
    Ok((
        r.left,
        Dtd {
            xml_version: r.xml_version,
            standalone: r.standalone,
            entities,
            unparsed_entities,
            validator,
            pis_and_comments: r.pis_and_comments,
        },
    ))
}
