use std::{
    fmt::{Display, Formatter},
    num::ParseIntError,
    string::FromUtf8Error,
};

#[derive(thiserror::Error, Debug)]
pub enum Error {
    String(String),
    // wrapper
    StringStream(#[from] xust_tree::string_stream::Error),
    IoError(#[from] std::io::Error),
    FmtError(#[from] std::fmt::Error),
    Utf8Error(#[from] FromUtf8Error),
    ParseIntError(#[from] ParseIntError),
    XmlParserError(#[from] XmlParserError),
}

#[derive(thiserror::Error, Debug)]
#[error(transparent)]
pub struct XmlParserError(pub(crate) xmlparser::Error);

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub type Result<T> = std::result::Result<T, Error>;

#[macro_export]
macro_rules! err {
    ($($arg:tt)*) => {{
        $crate::error::Error::String(format!($($arg)*))
    }};
}
