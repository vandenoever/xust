#![allow(clippy::upper_case_acronyms)]
use super::validator::{self, Validator};
use super::{
    AttType, Cp, DefaultDecl, DtdElements, ElementContent, ElementDecl, Name, NameChoiceOrSeq,
    OccurrenceIndicator,
};
use crate::{err, error::Result};

// Module that converts each element contents to a Deterministic Finite State
// Automaton (DFA). To achieve this, it is first converted to a
// Non-deterministic Finite Automaton (NFA), that is subsequently converted to a
// DFA.

#[derive(Default)]
struct NFA<'a> {
    nodes: Vec<Node<'a>>,
}

impl<'a> NFA<'a> {
    fn new() -> Self {
        Self {
            nodes: vec![Node::end_node()],
        }
    }
    fn add_node(&mut self) -> usize {
        let node_id = self.nodes.len();
        self.nodes.push(Node::new());
        node_id
    }
    fn add_edge(&mut self, source: usize, destination: usize, label: Label<'a>) {
        self.nodes[source].edges.push(Edge { label, destination });
    }
}

struct Node<'a> {
    edges: Vec<Edge<'a>>,
    end_node: bool,
}

impl<'a> Node<'a> {
    fn end_node() -> Self {
        Self {
            edges: Vec::new(),
            end_node: true,
        }
    }
    fn new() -> Self {
        Self {
            edges: Vec::new(),
            end_node: false,
        }
    }
}

#[derive(PartialEq, Clone, Copy)]
enum Label<'a> {
    Epsilon,
    Name(&'a Name),
}

struct Edge<'a> {
    label: Label<'a>,
    destination: usize,
}

pub(crate) mod dfa {
    pub enum DFA {
        Empty,
        Any,
        Mixed(Vec<usize>),
        Children { start_node: usize, nodes: Vec<Node> },
    }
    pub struct Node {
        pub end_node: bool,
        pub edges: Vec<Edge>,
    }
    #[derive(Clone, Copy)]
    pub struct Edge {
        pub destination: usize,
        pub dfa: usize,
    }
}

pub fn build(document_element: &Name, dtd: DtdElements, standalone: bool) -> Result<Validator> {
    let document_element_position = if let Some(position) = dtd
        .elements
        .iter()
        .position(|e| e.name.0 == document_element.0)
    {
        position
    } else {
        let names: Vec<_> = dtd.elements.iter().map(|e| &e.name.0).collect();
        return Err(err!(
            "No declaration for {} element, only for {:?}.",
            document_element.0,
            names
        ));
    };
    let mut dfas = Vec::with_capacity(dtd.elements.len() + 1);
    // root node
    for e in dtd.elements.iter() {
        let dfa = match &e.element_content {
            ElementContent::Any => dfa::DFA::Any,
            ElementContent::Empty => dfa::DFA::Empty,
            ElementContent::Children(c) => {
                let mut nfa = NFA::new();
                let start_node = translate_particle(&mut nfa, c, 0);
                convert_to_dfa(nfa, start_node, &dtd.elements)?
            }
            // mixed content is a choice with characters and child elements
            // there are no occurrence indicators
            // so the state machine has only one state
            // and a list of accepted children
            ElementContent::Mixed(c) => create_mixed_dfa(c, &dtd.elements)?,
        };
        dfas.push(validator::Element {
            name: e.name.clone(),
            dfa,
            atts: Vec::new(),
            internal: e.internal,
        });
    }
    dfas.push(validator::Element {
        name: Name(String::new()),
        dfa: dfa::DFA::Children {
            start_node: 0,
            nodes: vec![
                dfa::Node {
                    end_node: false,
                    edges: vec![dfa::Edge {
                        destination: 1,
                        dfa: document_element_position,
                    }],
                },
                dfa::Node {
                    end_node: true,
                    edges: Vec::new(),
                },
            ],
        },
        atts: Vec::new(),
        internal: false,
    });
    for a in dtd.attributes {
        if let Some(e) = dfas.iter_mut().find(|e| e.name == a.name) {
            for a in a.atts {
                if let AttType::Enumeration(v) = &a.att_type {
                    if let Some(v) = has_duplicate_value(v) {
                        return Err(err!("Duplicate enumeration value '{}'.", v.0));
                    }
                }
                if let AttType::NotationType(v) = &a.att_type {
                    if let Some(v) = has_duplicate_value(v) {
                        return Err(err!("Duplicate enumeration value '{}'.", v.0));
                    }
                }
                if !e.atts.iter().any(|ea| ea.name == a.name) {
                    if a.att_type == AttType::Id {
                        if let DefaultDecl::Implied | DefaultDecl::Required = a.default_decl {
                        } else {
                            return Err(err!("Attribute with type ID must have a declared default of #IMPLIED or #REQUIRED, not {:?}", a.default_decl));
                        }
                    }
                    e.atts.push(a);
                }
            }
            if e.atts.iter().filter(|a| a.att_type == AttType::Id).count() > 1 {
                return Err(err!("Too many attributes have type ID on element."));
            }
        }
    }
    Ok(Validator::new(dfas, standalone))
}

fn has_duplicate_value<T: PartialEq>(v: &[T]) -> Option<&T> {
    for i in 1..v.len() {
        let r = &v[i - 1];
        if v[i..].contains(r) {
            return Some(r);
        }
    }
    None
}

fn check_for_duplicates(c: &[Name]) -> Result<()> {
    if let Some(v) = has_duplicate_value(c) {
        return Err(err!("'{}' appears twice.", v.0));
    }
    Ok(())
}

fn translate_particle_1<'a>(nfa: &mut NFA<'a>, cp: &'a Cp, next: usize) -> usize {
    let node = nfa.add_node();
    match &cp.child {
        NameChoiceOrSeq::Name(name) => {
            nfa.add_edge(node, next, Label::Name(name));
        }
        NameChoiceOrSeq::Seq(seq) => {
            let mut n = next;
            for cp in seq.iter().rev() {
                n = translate_particle(nfa, cp, n);
            }
            nfa.add_edge(node, n, Label::Epsilon);
        }
        NameChoiceOrSeq::Choice(choice) => {
            for cp in choice {
                let n = translate_particle(nfa, cp, next);
                nfa.add_edge(node, n, Label::Epsilon);
            }
        }
    }
    node
}

fn translate_particle<'a>(nfa: &mut NFA<'a>, cp: &'a Cp, next: usize) -> usize {
    match cp.occurrence_indicator {
        OccurrenceIndicator::One => translate_particle_1(nfa, cp, next),
        OccurrenceIndicator::ZeroOrMore => {
            let node1 = nfa.add_node();
            let sub = translate_particle_1(nfa, cp, node1);
            nfa.add_edge(node1, sub, Label::Epsilon);
            nfa.add_edge(node1, next, Label::Epsilon);
            node1
        }
        OccurrenceIndicator::OneOrMore => {
            let node1 = nfa.add_node();
            let node2 = nfa.add_node();
            let sub = translate_particle_1(nfa, cp, node2);
            nfa.add_edge(node1, sub, Label::Epsilon);
            nfa.add_edge(node2, sub, Label::Epsilon);
            nfa.add_edge(node2, next, Label::Epsilon);
            node1
        }
        OccurrenceIndicator::ZeroOrOne => {
            let node1 = nfa.add_node();
            let sub = translate_particle_1(nfa, cp, next);
            nfa.add_edge(node1, sub, Label::Epsilon);
            nfa.add_edge(node1, next, Label::Epsilon);
            node1
        }
    }
}

fn convert_to_dfa(mut nfa: NFA, start_node: usize, elements: &[ElementDecl]) -> Result<dfa::DFA> {
    /* First find all the useful nodes, ie those pointed to by a
    non-epsilon edge. */
    let mut useful = Vec::with_capacity(nfa.nodes.len());
    for node in &mut nfa.nodes {
        for edge in &node.edges {
            if edge.label != Label::Epsilon {
                useful.push(edge.destination);
            }
        }
    }
    useful.push(start_node); // add start_node
    useful.sort_unstable();
    useful.dedup();
    /* Now add to each useful node all the non-epsilon edges of
    the nodes in its epsilon-closure. */
    for node in &useful {
        for edge in 0..nfa.nodes[*node].edges.len() {
            let edge = &nfa.nodes[*node].edges[edge];
            if edge.label == Label::Epsilon {
                let destination = edge.destination;
                add_epsilon_closure(&mut nfa, *node, destination);
            }
        }
    }
    /* Now remove all useless nodes and epsilon edges from useful nodes */
    // retain only the useful nodes
    let mut pos = 0;
    nfa.nodes.retain(|_| {
        let retain = useful.contains(&pos);
        pos += 1;
        retain
    });
    // translate the edges
    for node in &mut nfa.nodes {
        pos = 0;
        for i in 0..node.edges.len() {
            if node.edges[i].label != Label::Epsilon {
                let destination = node.edges[i].destination;
                if let Some(new_destination) = useful.iter().position(|n| *n == destination) {
                    node.edges[pos].destination = new_destination;
                    node.edges[pos].label = node.edges[i].label;
                    pos += 1;
                }
            }
        }
        node.edges.truncate(pos);
    }
    let start_node = useful.iter().position(|n| *n == start_node).unwrap();
    /*
    println!("{:?} {}", useful, start_node);
    print_nfa(&nfa, start_node);
    */
    // convert to dfa
    let mut dfa_nodes = Vec::with_capacity(nfa.nodes.len());
    for n in nfa.nodes {
        let mut edges = Vec::with_capacity(n.edges.len());
        for e in n.edges {
            if let Label::Name(name) = e.label {
                if let Some(dfa) = elements.iter().position(|e| e.name == *name) {
                    edges.push(dfa::Edge {
                        destination: e.destination,
                        dfa,
                    })
                } else {
                    // element is not defined, leave out this axis
                    // TODO: leave it out earlier in the build process
                }
            } else {
                return Err(err!("Edge has non-name label."));
            }
        }
        dfa_nodes.push(dfa::Node {
            end_node: n.end_node,
            edges,
        });
    }
    Ok(dfa::DFA::Children {
        start_node,
        nodes: dfa_nodes,
    })
}

fn create_mixed_dfa(members: &[Name], elements: &[ElementDecl]) -> Result<dfa::DFA> {
    check_for_duplicates(members)?;
    let mut names = Vec::with_capacity(members.len());
    for name in members {
        if let Some(dfa) = elements.iter().position(|e| e.name == *name) {
            names.push(dfa);
        } else {
            return Err(err!("Element {} is not defined.", name.0));
        }
    }
    Ok(dfa::DFA::Mixed(names))
}

#[allow(dead_code)]
fn print_nfa(nfa: &NFA, start_node: usize) {
    println!("start: {}\tnodes: {}", start_node, nfa.nodes.len());
    for (pos, node) in nfa.nodes.iter().enumerate() {
        print!("{}", pos);
        if node.end_node {
            print!("*")
        }
        for edge in &node.edges {
            print!("\t{} ", edge.destination);
            match edge.label {
                Label::Epsilon => print!("(e)"),
                Label::Name(n) => print!("{}", n.0),
            }
        }
        println!();
    }
}

fn add_epsilon_closure(nfa: &mut NFA, base: usize, node: usize) {
    if nfa.nodes[node].end_node {
        nfa.nodes[base].end_node = true;
    }
    let mut edges = std::mem::take(&mut nfa.nodes[node].edges);
    for edge in &edges {
        if edge.label == Label::Epsilon {
            add_epsilon_closure(nfa, base, edge.destination);
        } else if !nfa.nodes[base]
            .edges
            .iter()
            .any(|e| e.destination == edge.destination && e.label == edge.label)
        {
            nfa.add_edge(base, edge.destination, edge.label);
        }
    }
    std::mem::swap(&mut nfa.nodes[node].edges, &mut edges);
}
