use super::{
    entities::UnparsedEntities, fsm::dfa::*, ncname, nmtoken, AttDef, AttType, DefaultDecl,
    ExternalId, Name,
};
use crate::{err, error::Result};

pub(crate) struct Element {
    pub name: Name,
    pub dfa: DFA,
    pub atts: Vec<super::AttDef>,
    pub internal: bool,
}

pub struct Validator {
    elements: Vec<Element>,
    // text buffer for receiving attribute values
    buffer: String,
    attribute: Option<usize>,
    // every attributes that was seen is set to true
    // the index corresponds to the index of the attribute in the elementdefinition
    defined_attributes: Vec<bool>,
    stack: Vec<(usize, usize)>, // dfa_id, node_id
    ids: Vec<String>,
    idrefs: Vec<String>,
    standalone: bool,
}

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum ElementContentType {
    Any,
    Empty,
    Children,
    Mixed,
}

impl Validator {
    pub(crate) fn new(mut elements: Vec<Element>, standalone: bool) -> Self {
        normalize_attribute_defaults(&mut elements);
        let mut v = Self {
            elements,
            buffer: String::new(),
            attribute: None,
            defined_attributes: Vec::new(),
            stack: Vec::new(),
            ids: Vec::new(),
            idrefs: Vec::new(),
            standalone,
        };
        v.reset();
        v
    }
    pub fn reset(&mut self) {
        self.stack.clear();
        let start_element = self.elements.len() - 1;
        if let DFA::Children { start_node, .. } = &self.elements[start_element].dfa {
            self.stack.push((start_element, *start_node));
        }
        self.ids.clear();
        self.idrefs.clear();
        self.attribute = None;
    }
    pub fn start_element(&mut self, name: &str) -> Result<ElementContentType> {
        let element = if let Some(element) = self.elements.iter().position(|e| name == e.name.0) {
            element
        } else {
            return Err(err!("Undefined element."));
        };
        // check if element is allowed to start
        if let Some((element_id, node_id)) = self.stack.last().cloned() {
            match &self.elements[element_id].dfa {
                DFA::Empty => return Err(err!("element should be empty")),
                DFA::Any => {}
                DFA::Mixed(elements) => {
                    let matches = elements.contains(&element);
                    if !matches {
                        return Err(err!("unexpected start tag '{}'", name));
                    }
                }
                DFA::Children { nodes, .. } => {
                    if let Some(edge) = nodes[node_id].edges.iter().find(|e| e.dfa == element) {
                        // change dfa according to element
                        self.stack.last_mut().unwrap().1 = edge.destination;
                    } else {
                        return Err(err!("unexpected start tag '{}'", name));
                    }
                }
            }
        } else {
            return Err(err!("No more events expected."));
        }
        let e = &self.elements[element];
        // put new element on the stack
        let start_node = if let DFA::Children { start_node, .. } = &e.dfa {
            *start_node
        } else {
            0
        };
        let ect = match &self.elements[element].dfa {
            DFA::Any => ElementContentType::Any,
            DFA::Empty => ElementContentType::Empty,
            DFA::Children { .. } => ElementContentType::Children,
            DFA::Mixed(_) => ElementContentType::Mixed,
        };
        self.stack.push((element, start_node));
        self.defined_attributes.clear();
        self.defined_attributes.resize(e.atts.len(), false);
        Ok(ect)
    }
    pub(crate) fn end_attributes(&self) -> Result<impl Iterator<Item = (&str, &str)>> {
        if let Some((element_id, _)) = self.stack.last() {
            let atts = &self.elements[*element_id].atts;
            for (i, a) in atts.iter().enumerate() {
                if !self.defined_attributes[i] {
                    if a.default_decl == DefaultDecl::Required {
                        return Err(err!("Missing required attribute."));
                    }
                    if let DefaultDecl::Default(_) | DefaultDecl::Fixed(_) = &a.default_decl {
                        if self.standalone && !a.internal {
                            return Err(err!("Missing default attribute in standalone document."));
                        }
                    }
                }
            }
            Ok(atts.iter().zip(&self.defined_attributes).filter_map(|b| {
                if !b.1 {
                    if let DefaultDecl::Default(v) | DefaultDecl::Fixed(v) = &b.0.default_decl {
                        return Some((b.0.name.0.as_str(), v.0.as_str()));
                    }
                }
                None
            }))
        } else {
            Err(err!("No more events expected."))
        }
    }
    pub fn end_element(&mut self) -> Result<()> {
        if let Some((element_id, node_id)) = self.stack.last() {
            if let DFA::Children { nodes, .. } = &self.elements[*element_id].dfa {
                if !nodes[*node_id].end_node {
                    return Err(err!("Unexpected element end."));
                }
            }
            self.stack.pop();
            if self.stack.len() == 1 {
                self.end_checks()?;
            }
            Ok(())
        } else {
            Err(err!("No more events expected."))
        }
    }
    fn end_checks(&mut self) -> Result<()> {
        if let Some(dead_id) = self.idrefs.iter().find(|idref| !self.ids.contains(idref)) {
            return Err(err!("There is no id {}.", dead_id));
        }
        Ok(())
    }
    pub fn add_char(&mut self, c: char) -> Result<()> {
        if self.attribute.is_some() {
            self.buffer.push(c);
            Ok(())
        } else if let Some((element, _)) = self.stack.last() {
            let element = &self.elements[*element];
            if let DFA::Mixed { .. } | DFA::Any = &element.dfa {
                // any text is allowed
                Ok(())
            } else if !element.internal && self.standalone {
                Err(err!("No text allowed in this element."))
            } else if c == ' ' || c == '\t' || c == '\r' || c == '\n' {
                // whitespace is allowed
                Ok(())
            } else {
                Err(err!(
                    "No text ({}) allowed in element {}.",
                    c,
                    &element.name.0
                ))
            }
        } else {
            Err(err!("No more events expected."))
        }
    }
    pub fn add_text(&mut self, text: &str) -> Result<()> {
        if text.is_empty() {
            return Ok(());
        }
        if self.attribute.is_some() {
            self.buffer.push_str(text);
            Ok(())
        } else if let Some((element, _)) = self.stack.last() {
            let element = &self.elements[*element];
            if let DFA::Mixed { .. } | DFA::Any = &element.dfa {
                // any text is allowed
                Ok(())
            } else if !element.internal && self.standalone {
                Err(err!("No text allowed in this element."))
            } else if let Ok(("", _)) = nom::character::complete::multispace0::<_, ()>(text) {
                // whitespace is allowed
                Ok(())
            } else {
                Err(err!(
                    "No text ({}) allowed in element {}.",
                    text,
                    &element.name.0
                ))
            }
        } else {
            Err(err!("No more events expected."))
        }
    }
    // return true if attribute value whitespace should be normalized
    pub fn start_attribute(&mut self, name: &str) -> Result<(bool, bool)> {
        self.buffer.clear();
        if let Some((element, _)) = self.stack.last() {
            let e = &self.elements[*element];
            self.attribute = e.atts.iter().position(|a| a.name.0 == name);
            if let Some(a) = self.attribute {
                self.defined_attributes[a] = true;
                Ok((e.atts[a].att_type != AttType::CData, !e.atts[a].internal))
            } else {
                Err(err!(
                    "Attribute {} is not defined for element {}.",
                    name,
                    e.name.0
                ))
            }
        } else {
            Err(err!("No more events expected."))
        }
    }
    pub(crate) fn end_attribute(&mut self, unparsed_entities: &UnparsedEntities) -> Result<()> {
        let value = &self.buffer;
        if let Some((element, _)) = self.stack.last() {
            if let Some(att) = self.attribute {
                let e = &self.elements[*element];
                let att = &e.atts[att];
                if let DefaultDecl::Fixed(v) = &att.default_decl {
                    if v.0 != *value {
                        return Err(err!(
                            "Attribute {} on element {} should be '{}', not '{}'",
                            att.name.0,
                            e.name.0,
                            v.0,
                            value
                        ));
                    }
                }
                check_attribute_value(&e.name.0, att, value)?;
                if AttType::Entity == att.att_type && unparsed_entities.get(value).is_none() {
                    return Err(err!("entity {} is not defined", value));
                }
                if AttType::Entities == att.att_type {
                    for entity in value.split(' ') {
                        if unparsed_entities.get(entity).is_none() {
                            return Err(err!("entity {} is not defined", value));
                        }
                    }
                }
                if AttType::Id == att.att_type {
                    if self.ids.iter().any(|v| v == value) {
                        return Err(err!("id {} occurs more than once", value));
                    }
                    self.ids.push(value.to_string());
                }
                if AttType::IdRef == att.att_type {
                    self.idrefs.push(value.to_string());
                }
                if AttType::IdRefs == att.att_type {
                    for idref in value.split(' ') {
                        self.idrefs.push(idref.to_string());
                    }
                }
            }
            Ok(())
        } else {
            Err(err!("No more events expected."))
        }
    }
    pub fn is_done(&self) -> bool {
        self.stack.len() == 1
    }
    pub(crate) fn check_notation_attributes(&self, notations: &[(Name, ExternalId)]) -> Result<()> {
        // check all notations used in attribute types
        for att in self.elements.iter().flat_map(|e| &e.atts) {
            if let AttType::NotationType(n) = &att.att_type {
                // check that notation is defined
                for an in n {
                    if !notations.iter().any(|n| n.0 == *an) {
                        return Err(err!("No notation '{}'.", an.0));
                    }
                }
            }
        }
        Ok(())
    }
    pub(crate) fn check_default_attributes(&self) -> Result<()> {
        for att in self.elements.iter().flat_map(|e| &e.atts) {
            if let DefaultDecl::Default(v) = &att.default_decl {
                check_attribute_value("", att, &v.0)?;
            }
        }
        Ok(())
    }
}

fn check_attribute_value(ename: &str, att: &AttDef, value: &str) -> Result<()> {
    if let AttType::Id | AttType::IdRef | AttType::Entity = &att.att_type {
        if let Ok(("", _)) = ncname(value) {
        } else {
            return Err(err!(
                "Attribute {} on element {} should be a valid id, not '{}'.",
                att.name.0,
                ename,
                value
            ));
        }
    }
    if let AttType::IdRefs | AttType::Entities = &att.att_type {
        for idref in value.split(' ') {
            if let Ok(("", _)) = ncname(idref) {
            } else {
                return Err(err!(
                    "Attribute {} on element {} is not syntactially valid: '{}'.",
                    att.name.0,
                    ename,
                    value
                ));
            }
        }
    }
    if let AttType::NotationType(n) = &att.att_type {
        if !n.iter().any(|n| n.0 == *value) {
            return Err(err!("notation {} is none of {:?}", value, n));
        }
    }
    if let AttType::Enumeration(n) = &att.att_type {
        if !n.iter().any(|n| n.0 == *value) {
            return Err(err!("enmeration {} is none of {:?}", value, n));
        }
    }
    if AttType::Nmtoken == att.att_type {
        if let Ok(("", _)) = nmtoken(value) {
        } else {
            return Err(err!(
                "Attribute {} on element {} should be a valid nmtoken, not '{}'.",
                att.name.0,
                ename,
                value
            ));
        }
    }
    if AttType::Nmtokens == att.att_type {
        for n in value.split(' ') {
            if let Ok(("", _)) = nmtoken(n) {
            } else {
                return Err(err!(
                    "Attribute {} on element {} should contain valid nmtokens, not '{}'.",
                    att.name.0,
                    ename,
                    value
                ));
            }
        }
    }
    Ok(())
}

fn normalize_attribute_defaults(elements: &mut [Element]) {
    for att in elements.iter_mut().flat_map(|e| &mut e.atts) {
        if let DefaultDecl::Default(v) = &mut att.default_decl {
            if att.att_type == AttType::Nmtoken || att.att_type == AttType::Nmtokens {
                let mut saw_char = false;
                v.0.retain(|c| {
                    let ws = c.is_whitespace();
                    let retain = !ws || saw_char;
                    saw_char = !ws;
                    retain
                });
                v.0.truncate(v.0.trim_end().len());
            }
        }
    }
}
