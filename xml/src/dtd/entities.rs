use super::Origin;
use std::collections::HashMap;

#[derive(Default)]
pub(crate) struct Entities {
    // unparsed external entities by id
    entities: HashMap<String, (String, Origin)>,
    system_entities: HashMap<String, Result<(String, Origin), String>>,
}

impl Entities {
    pub fn new(
        entities: HashMap<String, (String, Origin)>,
        system_entities: HashMap<String, Result<(String, Origin), String>>,
    ) -> Self {
        Self {
            entities,
            system_entities,
        }
    }
    pub fn get(&self, id: &str) -> Option<Result<(&str, Origin), &str>> {
        if let Some(v) = self.entities.get(id).map(|(s, o)| (s.as_str(), *o)) {
            Some(Ok(v))
        } else {
            self.system_entities.get(id).map(|ext| match ext.as_ref() {
                Err(e) => Err(e.as_str()),
                Ok((a, b)) => Ok((a.as_str(), *b)),
            })
        }
    }
}

#[derive(Default)]
pub(crate) struct UnparsedEntities {
    // unparsed external entities by id
    entities: HashMap<String, (String, Origin)>,
}

impl UnparsedEntities {
    pub fn new(entities: HashMap<String, (String, Origin)>) -> Self {
        Self { entities }
    }
    pub fn get(&self, id: &str) -> Option<(&str, Origin)> {
        self.entities.get(id).map(|(s, o)| (s.as_str(), *o))
    }
}
