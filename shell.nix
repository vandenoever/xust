with import <nixpkgs> { };

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    rustc
    rust-analyzer
    cargo
    cargo-watch
    clippy
    rustfmt
    libxml2
    xmlstarlet
    lld
    highlight
    reuse
  ];
  # use a faster linker
  RUSTFLAGS = "-C link-arg=-fuse-ld=lld -C target-cpu=native";
}
