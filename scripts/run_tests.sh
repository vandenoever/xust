#! /usr/bin/env bash

# a simple script that runs some tests

function runtest {
    exitcode="$1"
    shift
    exe="$1"
    cmdline="$*"
    shift
    mkdir tmp
    echo "== Running $cmdline =="
    "$exe" "$@"
    foundexit="$?"
    rm -rf tmp
    if [ "$foundexit" -ne "$exitcode" ]; then
        echo "Got wrong exit code for $cmdline: $foundexit != $exitcode"
        exit 1
    fi
}

function run {
    exitcode="$1"
    shift
    exe="$1"
    shift
    runtest "$exitcode" "target/debug/$exe" "$@"
}

function example {
    exitcode="$1"
    shift
    exe="$1"
    shift
    runtest "$exitcode" "target/debug/examples/$exe" "$@"
}

cargo build --all-features --all

run 0 read_a_lot
run 0 xmlcat
run 1 validate
run 0 validate --help
run 0 validate -s xsd/examples/simple/simple.xsd xsd/examples/simple/simple.xml
run 1 validate -s xsd/examples/simple/simple.xsd xsd/examples/nested/nexted.xml
run 0 xsd_compiler --help nothing
run 1 xsd_compiler nothing
run 0 xust --help

example 0 nested
example 0 simple
example 0 wildcard
example 0 mixed

echo All executable tests ran successfully!
