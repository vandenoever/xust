#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

function xsd2rs() {
    was_generated=false
    target_dir=$1
    shift
    if diff "$1" "$target_dir/$1" 2> /dev/null > /dev/null; then
        was_generated=true
    fi
    if [ "$target_dir" = "xsd/src" ]; then
        local internal="--internal"
    fi
    echo "Creating $1:"
    if cargo run --bin xsd2rs -- ${internal:-} --output "$@"; then
        # if new file differs copy it to git if the git version was not changed
        if diff "$1" "$target_dir/$1"; then
            rm "$1"
        else
            if [ "$was_generated" = true ] || (git diff --exit-code "$target_dir/$1" && ! diff  "$1" "$target_dir/$1"); then
                cp "$1" "$target_dir/$1"
            fi
        fi
    else
        exit 1
    fi
}

xsd2rs xsd/src xsd.rs xsd/xsd/www.w3.org/2001/XMLSchema/xsd.xsd xsd/xsd/www.w3.org/2001/xml.xsd
xsd2rs xsd/examples/simple simple.rs xsd/examples/simple/simple.xsd
xsd2rs xsd/examples/nested nested.rs xsd/examples/nested/nested.xsd
xsd2rs xsd/examples/mixed mixed.rs xsd/examples/mixed/mixed.xsd
xsd2rs xsd/examples/wildcard wildcard.rs xsd/examples/wildcard/wildcard.xsd
xsd2rs tests/src catalog_schema.rs qt3tests/catalog-schema.xsd
xsd2rs tests/src gpx.rs xsd/xsd/misc/gpx.xsd
xsd2rs tests/src ts.rs xsd/xsd/misc/AnnotatedTSSchema.xsd xsd/xsd/www.w3.org/XML/2008/06/xlink.xsd xsd/xsd/www.w3.org/2001/xml.xsd
xsd2rs tests/src svg.rs xsd/xsd/misc/SVG.xsd xsd/xsd/www.w3.org/XML/2008/06/xlink.xsd xsd/xsd/www.w3.org/2001/xml.xsd
xsd2rs tests/src kcfg.rs xsd/xsd/misc/kcfg.xsd
