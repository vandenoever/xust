#!/usr/bin/env bash

# This script lists the types that are exposed by the given crates.
# It uses the hyperlinks from the HTML output from `cargo doc`.

function list_types {
    crate=$1
    depth=$2
    prefix=$(for (( i = 0; i < "$depth"; ++i )); do echo -n "\.\./"; done)

    find target/doc/"$crate"/ -mindepth "$depth" -maxdepth "$depth" \
        -type f -name '*.html' \
        -exec xmllint --html --xpath '//a/@href' {} + 2> /dev/null \
        | cut -f 2 -d '"' | grep ^"$prefix" | grep -v ^"$prefix"src/ \
        | grep -v ^"$prefix"settings.html$ \
        | grep -v /index.html$ \
        | cut -c $((1 + depth * 3))- \
        | sed -e 's/.html$//' \
        | sed -e 's#/#::#g' \
        | sed -e 's/::struct\./::/' \
        | sed -e 's/::enum\./::/' \
        | sed -e 's/::trait\./::/' \
        | sed -e 's/::fn\./::/'
}

function list_exposed_types {
    output=$(
        for crate in "$@"; do
            for depth in {1..10}; do
                list_types "$crate" "$depth"
            done | grep -v ^"$crate"::
        done
    )
    echo "$output" | sort | uniq
}

cargo doc
list_exposed_types xust_tree xust_xml xust_xsd
