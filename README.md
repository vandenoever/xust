[code coverage](https://vandenoever.pages.gitlab.gnome.org/xust/coverage/)
[docs](https://vandenoever.pages.gitlab.gnome.org/xust/doc/xust_xml/)

This project implements XQuery 3.1. The goal is a complete implementation.

This means that XML, XML Schema, XPath 3.1 are implemented as well.

The project is currently in early development. The only useful executables
are currently the executables that run the tests.

# Testing

## XML

```bash
wget https://www.w3.org/XML/Test/xmlts20130923.tar.gz
tar xf xmlts20130923.tar.gz
sed -i 's#eduni/namespaces/misc/#eduni/misc/#' xmlconf/xmlconf.xml
cargo run --bin xml_test_suite xmlconf/xmlconf.xml
```

## XSD

```bash
wget https://www.w3.org/XML/2004/xml-schema-test-suite/xmlschema2006-11-06/xsts-2007-06-20.tar.gz
tar xf xsts-2007-06-20.tar.gz
cargo run --bin xsd_test_suite -- --junit-output xsd_junit.xml xmlschema2006-11-06/suite.xml
```

## XQuery

```bash
cargo run --features=eval --bin qt3tests -- qt3tests/catalog.xml
```

Test runs can generate JUnit report format XML files.

# Development

To contribute, please submit merge requests or report issues.

A number of tests will be run for every merge request. These tests can also
be run from the command-line.

To run XQuery tests from short query to long query and stop on the first error, run:

```bash
RUST_BACKTRACE=1 cargo watch -x 'run --features="eval" --bin qt3tests -- --print-console --stop-on-error --sort-by-xquery-length qt3tests/catalog.xml'
```
