{
  description = "Xust: XML for Rust";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=master";
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    naersk.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, utils, naersk }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = naersk.lib."${system}";
        inputs = [ ];
      in rec {
        # `nix build`
        packages.xust = naersk-lib.buildPackage {
          pname = "xust";
          root = ./.;
          nativeBuildInputs = inputs;
        };
        defaultPackage = packages.xust;

        # `nix run`
        apps.xust = utils.lib.mkApp { drv = packages.xust; };
        defaultApp = apps.xust;

        # `nix develop`
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs;
            [ rustc cargo clippy cargo-audit cargo-watch rust-analyzer rustfmt highlight libxml2 ] ++ inputs;
        };
      });
}

